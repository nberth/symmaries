(** SCGS's whole toolchain *)

open Format
open Rutils
open Utils
open OOLang
module RTKB = ReaTKBridge.Supra

let level = Log.Debug3
let logger = Log.mk ~level "SCGS";;

type 'a rlimit =
  | RLimitNone
  | RLimitSoft of 'a
  | RLimitHard of 'a

module OptHelpers = struct
  let interfaces = function
    | "unsafe" -> Ok (`SafeInterfaces false)
    | "safe" -> Ok (`SafeInterfaces true)
    | s -> Error s
  let prop = function
    | "full" | "true" | "yes" -> Ok `Full
    | "constants" | "consts" | "const" -> Ok `Consts
    | "nu" -> Ok `Nu
    | "disable" | "false" | "none" | "no" -> Ok `None
    | s -> Error s
  let effects_inference = function
    | "preamble"
    | "controllable_preamble" -> Ok ScgsCore.Trans.ControllablePreamble
    | "coreach" | "coreach_only" -> Ok ScgsCore.Trans.CoreachOnly
    | s -> Error s
  let semloc_encoding = function
    | "explicit" -> Ok `ExplicitJSHCDR
    | "mixed" | "symbhr" | "expljs" -> Ok `ExplicitJSSymbolicHCDR
    | "symb" | "symbolic" -> Ok `SymbolicJSHCDR
    | s -> Error s
  let heapdom = function
    | "deepalias" | "deep" -> Ok ScgsCore.Features.DeepAlias
    | "connect" | "connecting" -> Ok ScgsCore.Features.Connect
    | "shallowalias" | "shallow" -> Ok ScgsCore.Features.ShallowAlias
    | "noalias" | "dumbalias" | "dumb" -> Ok ScgsCore.Features.DumbAlias
    | "share" | "sharing" -> Ok ScgsCore.Features.Share
    | "fieldshare" | "ashare" -> Ok ScgsCore.Features.FieldShare
    | s -> Error s
  let trans_merge = function
    | "always" | "yes" | "true" -> Ok `Always
    | "disable" | "false" | "none" | "no" | "never" -> Ok `Never
    | "nonloops" | "except_loops" -> Ok `NonLoops
    | "loops" | "loops_only" -> Ok `Loops
    | s -> Error s
  let expr_heuristics: string -> ([`Lazy | `Eager], string) result = function
    | "lazy" -> Ok `Lazy
    | "eager" -> Ok `Eager
    | s -> Error s
  let sumcomp = function
    | "default" -> Ok None
    | "monolithic" | "full" -> Ok (Some RTKB.Monolithic)
    | "fcsplit" | "split" ->
        Ok (Some (RTKB.SplitGuardNFootprint
                    { enforce_equivalence_to_monolithic = false }))
    | s -> Error s
  let sigint = function
    | "int" | "interrupt" -> Ok `Interrupt
    (* | "ign" | "ignore" -> Ok `Ignore *)
    | "skip" -> Ok `Skip
    | s -> Error s
  let timeout =
    let input k x u =
      let s k x = Some (k, x) and ms k x = Some (k, x /. 1000.) in
      let m k x = Some (k, x *. 60.) and h k x = Some (k, x *. 3600.) in
      let f = match u with
        | "ms" -> ms | "s" | "" -> s | "m" -> m | "h" -> h
        | _ -> fun _ _ -> None
      in
      f k x
    in
    let time_parsers = [
      ScgsUtils.IO.mk_parser "none%!" (Some ("", 0.0));
      ScgsUtils.IO.mk_parser "%[^:] %_1[:] %f %s%!" input;
      ScgsUtils.IO.mk_parser "%f %s%!" (input "h");
    ] in
    fun s -> match ScgsUtils.IO.try_parse time_parsers s with
      | None -> Error s
      | Some (Some (_, t)) when t < 0.0 -> Error s
      | Some (Some (_, 0.0)) -> Ok RLimitNone
      | Some (Some (("soft" | "s"), t)) -> Ok (RLimitSoft t)
      | Some (Some (("hard" | "h"), t)) -> Ok (RLimitHard t)
      | Some _ -> Error s
  let memory =
    let input x u =
      let open Int64 in
      let rec b x = Some x and k x = b (mul x 1024L)
      and m x = k (mul x 1024L) and g x = m (mul x 1024L) in
      let f = match u with
        | "B" | "Byte" | "Bytes" | "" -> b
        | "K" | "KB" | "KiB" -> k
        | "M" | "MB" | "MiB" -> m
        | "G" | "GB" | "GiB" -> g
        | _ -> fun _ -> None
      in
      f x
    in
    let mem_parsers = [
      ScgsUtils.IO.mk_parser "none%!" (Some 0L);
      ScgsUtils.IO.mk_parser "%Lu %s%!" input;
    ] in
    fun s -> match ScgsUtils.IO.try_parse mem_parsers s with
      | None -> Error s
      | Some (Some t) when t < 0L -> Error s
      | Some (Some 0L) -> Ok None
      | Some (Some t) -> Ok (Some t)
      | Some _ -> Error s
end

module type IO = sig
  val error: ?cont:(unit -> 'a) -> ('b, formatter, unit, 'a) format4 -> 'b
  val warn: ('b, formatter, unit, unit) format4 -> 'b
  val mk_out_from: string -> formatter IO.out
  val mk_bin_out_from: string -> out_channel IO.out
  val pp_filename: logger:Log.logger -> string pp
end

(** Toolchain bundle, parameterized with some I/O routines *)
module Bundle (IO: IO) = struct
  open IO
  open Sedlex_menhir

  (* --- *)

  module SecSum = ScgsCore.Trans.SecSum
  module SecSums = SecSum.Set (Java.MethodLookup)
  type secsums = SecSums.t

  module type CGWALK = ScgsCallGraphWalk.J
  module type CGWALK_STAGE = ScgsCallGraphWalk.JW

  type state =
      {
        classes: (module Java.TYPING) option;
        lexer_options: Input.LexerOptions.options option;
        typing_options: typing_options;
        trans_options: (module RTKB.OPTIONS);
        meth_options: meth_options StrMap.t;
        meth_stats: ScgsStats.t;
        proc_options: proc_options;
        proc_brittle: bool;            (* true iff in child safe-walk process *)
        synth_algo: string option;
        sumcomp_algo: RTKB.summary_computation option;
        output_irel: bool;
        output_arel: bool;
        output_farel: bool;
        output_tfa: bool;
        output_cfg: bool;
        output_cdrs: bool;
        output_scfg: bool;
        output_ctrlg: bool;
        output_ctrlf: bool;
        output_secsum: bool;
        output_secsum_bin: bool;
        output_guards: bool;
        output_monctrln: bool;
        output_monnode: bool;
        output_monauto: bool;
        output_rmoncfg: bool;
        secsums: secsums;
        cgwalk: cgwalk;
      }
  and typing_options =
      {
        typing_safe_interfaces: bool;
      }
  and meth_options =
      {
        meth_synth: string option;
        meth_sumcomp: RTKB.summary_computation option;    (* not assigned yet *)
        meth_prop: [ `Full | `Consts | `Nu | `None ] option;
        meth_semloc: [ `ExplicitJSHCDR
                     | `ExplicitJSSymbolicHCDR
                     | `SymbolicJSHCDR ] option;
        meth_trans_merge: ScgsCore.SemLoc.trans_merge_option option;
      }
  and proc_options =
      {
        proc_sigint: [ `Interrupt (* | `Ignore *) | `Skip ];
        proc_timeout: float rlimit;
        proc_cuddmax: int64 option;
      }
  and cgwalk =
    | CGNone
    | CGWalk of (module CGWALK)
    | CGStge of (module CGWALK_STAGE)

  let init =
    { classes = None;
      typing_options = { typing_safe_interfaces = true };
      lexer_options = None;
      trans_options = (module RTKB.DefaultOptions);
      meth_options = StrMap.empty;
      meth_stats = StrMap.empty;
      proc_options = { proc_sigint = `Interrupt;
                       proc_timeout = RLimitNone;
                       proc_cuddmax = None; };
      proc_brittle = false;
      synth_algo = None;
      sumcomp_algo = None;
      output_irel = false; output_arel = false; output_farel = false;
      output_tfa = false;
      output_cfg = false; output_cdrs = false; output_scfg = false;
      output_ctrlg = false; output_ctrlf = false;
      output_secsum = false; output_secsum_bin = false; output_guards = false;
      output_monctrln = false; output_monnode = false; output_monauto = false;
      output_rmoncfg = false;
      secsums = SecSums.empty;
      cgwalk = CGNone; }

  (* --- *)

  let set_idents_flavor flavor s =
    { s with lexer_options = Some Input.LexerOptions.{ flavor } }

  (* --- *)

  let set_safe_interfaces typing_safe_interfaces s =
    { s with typing_options = { typing_safe_interfaces } }

  (* --- *)

  let set_heapdom d ({ trans_options } as s) =
    { s with trans_options = (module struct
      include (val trans_options)
      let heapdom = d
    end); }

  let set_track_levels b ({ trans_options } as s) =
    { s with trans_options = (module struct
      include (val trans_options)
      let track_levels = ScgsCore.Features.(match b with
        | `None -> NoFlows
        | `Explicit -> ExplicitFlows
        | `All -> AllFlows)
      let enforce_privacy = track_levels <> NoFlows && enforce_privacy
    end); }

  let set_enforce_privacy b ({ trans_options } as s) =
    { s with trans_options = (module struct
      include (val trans_options)
      let enforce_privacy = b
      let track_levels =
        ScgsCore.Features.(if track_levels = NoFlows && enforce_privacy
          then AllFlows else track_levels)
    end); }

  let set_track_taints b ({ trans_options } as s) =
    { s with trans_options = (module struct
      include (val trans_options)
      let track_taints = b
      let enforce_integrity = enforce_integrity && track_taints
    end); }

  let set_enforce_integrity b ({ trans_options } as s) =
    { s with trans_options = (module struct
      include (val trans_options)
      let enforce_integrity = b
      let track_taints = track_taints || enforce_integrity
    end); }

  let set_track_exceptions b ({ trans_options } as s) =
    { s with trans_options = (module struct
      include (val trans_options)
      let track_excs = b
    end); }

  let set_evil_statics b ({ trans_options } as s) =
    { s with trans_options = (module struct
      include (val trans_options)
      let evil_static_fields = b
    end); }

  let set_sink_statics b ({ trans_options } as s) =
    { s with trans_options = (module struct
      include (val trans_options)
      let sink_static_fields = b
    end); }

  let set_methskip_condition c ({ trans_options } as s) =
    { s with trans_options = (module struct
      include (val trans_options)
      let methskip_condition = c
    end); }

  let set_methskip_policy p ({ trans_options } as s) =
    { s with trans_options = (module struct
      include (val trans_options)
      let methskip_policy = p
    end); }

  let set_effects_inference o ({ trans_options } as s) =
    { s with trans_options = (module struct
      include (val trans_options)
      let effects_inference = o
    end) }

  let set_semloc_encoding o ({ trans_options = tro } as s) =
    { s with trans_options = (module struct
      include (val tro)
      let semloc_encoding = o
    end); }

  let set_trans_merge o ({ trans_options = tro } as s) =
    { s with trans_options = (module struct
      include (val tro)
      let trans_merge = o
    end); }

  let set_prop_in_scfg o ({ trans_options = tro } as s) =
    { s with trans_options = (module struct
      include (val tro)
      let prop_in_scfg = o
    end: RTKB.OPTIONS); }

  let set_expr_heuristics b ({ trans_options } as s) =
    { s with trans_options = (module struct
      include (val trans_options)
      let eager = b == `Eager
    end); }

  (* --- *)

  let no_meth_options =
    {
      meth_synth = None;
      meth_sumcomp = None;
      meth_prop = None;
      meth_semloc = None;
      meth_trans_merge = None;
    }

  let meth_opt f { meth_options } =
    try StrMap.find f meth_options with
      | Not_found -> no_meth_options

  let meth_options_module f { trans_options = tro; meth_options } =
    try
      let mo = StrMap.find f meth_options in
      (module struct
        include (val tro)
        let prop_in_scfg = opt prop_in_scfg mo.meth_prop
        let semloc_encoding = opt semloc_encoding mo.meth_semloc
        let trans_merge = opt trans_merge mo.meth_trans_merge
      end: RTKB.OPTIONS)
    with
      | Not_found -> tro

  module MethOptionsIO = ScgsUtils.IO.Bin (struct
    type t = meth_options StrMap.t
    let functional = false
  end)

  (* --- *)

  let pf = pp_filename ~logger

  let info f =
    if Log.check_level logger Log.Info
    then Log.i logger f
    else eprintf (f^^"@.")

  (* --- *)

  let set_proc_sigint o ({ proc_options } as s) =
    { s with proc_options = { proc_options with proc_sigint = o } }

  let set_proc_timeout o ({ proc_options } as s) =
    { s with proc_options = { proc_options with proc_timeout = o } }

  let set_proc_cuddmaxmem o ({ proc_options } as s) =
    { s with proc_options = { proc_options with proc_cuddmax = o } }

  (* --- *)

  let pp_meth_errors fmt =
    pp_lst ~fempty:"" ~fopen:"@[" ~fclose:"@]" ~fsep:"@\n"
      Method.pp_error fmt

  let pp_meth_warnings fmt =
    pp_lst ~fempty:"" ~fopen:"@[" ~fclose:"@]" ~fsep:"@\n"
      Method.pp_warning fmt

  let pp_secsum_error fmt = function
    | `UnknownVar v
      -> pp fmt "unknown@ variable@ %a@ in@ security@ summary\
               " ScgsCore.Var.print v
    | `IncompatibleSecSumHeap { got; expected }
      -> pp fmt "encountered@ a@ security@ summary@ that@ relies@ on@ the@ \
                symbolic@ abstract@ heap@ `%a',@ which@ is@ incompatible@ \
                with@ `%a'"
        ScgsCore.Features.pp_heapdom got ScgsCore.Features.pp_heapdom expected

  let pp_errors f = function
    | `InMeth (Some meth, errs)
      -> error "@[in@ method@ `%a':@]@\n%a\
              " Base.pp_methname meth pp_meth_errors errs
    | `InMeth (None, errs)
      -> error "@[in@ file@ `%s':@]@\n%a" f pp_meth_errors errs
    | `InSecSum (ssum, err)
      -> error "@[while@ building@ SCFG@ for@ `%s':@ %a:@]@\n%a\
              " f pp_secsum_error err ScgsCore.Trans.SecSum.print ssum
    | `InSCFG x
      -> error "Error@ while@ building@ SCFG@ for@ `%s':@ %t" f x
    | `SynthFailure
      -> error "Synthesis@ failure@ while@ processing@ `%s'" f
    | `ExtractionError
      -> error "Error@ while@ extracting@ guard@ for@ `%s'" f
    | `InSecStubs errs
      -> error "@[in@ security@ summary@ stubs@ given@ in@ file@ \
                 `%s':@]@\n%a\
              " f pp_meth_errors errs
    | `InvalidSecurity s
      -> error "@[in@ file@ `%s':@]@\n%a" f Security.pp_invalid s
    | `UnexpectedStub s
      -> error "@[in@ file@ `%s':@]@\n%a" f SecStub.pp_unexpected_stub s
    | `Generic t
      -> error "@[in@ file@ `%s':@]@\n%t" f t

  let pp_warnings f = function
    | `InMeth (Some meth, warns)
      -> warn "@[in@ method@ `%a':@]@\n%a\
             " Base.pp_methname meth pp_meth_warnings warns
    | `InMeth (None, warns)
      -> warn "@[in@ file@ `%s':@]@\n%a" f pp_meth_warnings warns
    | `InFile (f, warns)
      -> warn "@[in@ file@ `%s':@]@\n%a" f pp_meth_warnings warns

  let pp_warnings' f =
    List.iter (function
      | `InMeth (m, w) -> pp_warnings f (`InMeth (Some m, w))
      | `InFile _ as w -> pp_warnings f w)

  (* --- *)

  let java_typing_params: state -> (module TypStruct.PARAMS) = function
    | { typing_options = { typing_safe_interfaces = true } }
      -> (module Java.SafeTypingParams: TypStruct.PARAMS)
    | { typing_options = { typing_safe_interfaces = false } }
      -> (module Java.UnsafeTypingParams: TypStruct.PARAMS)

  let java_typ_struct: state -> (module Java.TYPING) = function
    | { classes = None } as s
      -> Log.w logger "@[<h>Working@ with@ default@ class@ declarations.@]";
        (module (Java.Heap.Close (struct
          let db = Java.Heap.TypDecls.empty
        end) (val java_typing_params s)))
    | { classes = Some s } -> s

  (* --- *)

  let output_class_stats out s =
    let module Typs: Java.TYPING = (val java_typ_struct s) in
    let module TypsPeeks = Java.FieldAliasPeek (Typs) in
    let open TypsPeeks in
    let degree_stats =
      Typs.degree_stats ~quantiles:(BasicStats.Aggreg.Uniform 0.05) in
    let fmt, close =
      out.Rutils.IO.out_exec
        ~descr:"type@ hierarchy@ statistics"
        ~backup:Rutils.IO.Forget "class_stats"
    in
    ScgsStats.print_class_stats fmt ~degree_stats ~peeks Typs.stats;
    pp_print_newline fmt ();
    close ();
    s

  let output_meth_stats out ({ meth_stats } as s) =
    let fmt, close =
      out.Rutils.IO.out_exec
        ~descr:"method@ handling@ statistics"
        ~backup:Rutils.IO.Forget "meth_stats"
    in
    ScgsStats.print_full fmt meth_stats;
    close ();
    s

  let meth_stats_append f (meth_stats, summarization_stats) s =
    let s = { s with meth_stats =
        ScgsStats.append f meth_stats summarization_stats s.meth_stats } in
    info "@[<h>(%i)@ New@ stats@ for@ `%a'@]" (StrMap.cardinal s.meth_stats) pf f;
    s

  let meth_stats_skipped f (meth_impl_stats, (symb_heap_stats, meth_scfg_gen)) cause =
    meth_stats_append f
      (meth_impl_stats, ScgsCore.Stats.{ symb_heap_stats; meth_scfg_gen;
                                         meth_handling = Skipped cause })
  and meth_stats_done =
    meth_stats_append

  let print_stats ({ meth_stats; classes } as s) =
    let a = classes <> None and b = not (StrMap.is_empty meth_stats) in
    if a || b then
      let module Typs: Java.TYPING = (val java_typ_struct s) in
      Log.i logger "Overall statistics:@\n@[<v>%a%t%a@]"
        (ppa_if a ScgsStats.print_class_stats) Typs.stats
        (ppt_if (a && b) ",@;")
        (ppa_if b ScgsStats.print_overall_stats) meth_stats

  (* --- *)

  let read_classes_from out f
      ({ output_irel; output_arel; output_farel; output_tfa; lexer_options } as s)
      =
    let open Java.Heap in
    Log.i logger "@[<h>Reading@ class@ declarations@ from@ `%a'…@]" pf f;
    let read = Input.Java.Classes.from_file ?options:lexer_options in
    let module Classes: TypDecls.DB = struct
      let db = TypDecls.of_specs (read f)
    end in
    Log.i logger "@[<h>Checking@ class@ declarations…@]";
    let module AllClasses = Close (Classes) (val java_typing_params s) in
    if output_irel then AllClasses.output_irel ~suff:"-irel" out;
    if output_arel then AllClasses.output_arel ~suff:"-arel" out;
    if output_farel then AllClasses.output_farel ~suff:"-farel" out;
    if output_tfa then AllClasses.output_tfa ~suff:"-tfa" out;
    (module AllClasses: Java.TYPING)

  let new_classes out f ({ lexer_options } as s) =
    let open Typ in
    let open TypStruct in
    try
      { s with
        classes = Some (module (val read_classes_from out f s));
        secsums = SecSums.empty;
        cgwalk = CGNone; }
    with
      | ParseError e -> error "%a" pp_parse_error e
      | SyntaxError e -> error "%a" pp_syntax_error e
      | TypeRedeclaration t ->
          error "@[<h>Multiple@ declarations@ of@ type@ %a@]" UserTyp.print t
      | IncompleteTypeDecls t ->
          error "@[<h>Undeclared@ types:@ %a@]" UserTyps.print t
      | IncompleteRecordDecls infos ->
          error "@[<h>Incomplete@ declaration@ of@ types:%a@]" (fun fmt ->
            List.iter begin fun (rt, fts) ->
              pp fmt "@\n@[<h>In@ declaration@ of@ %a:%a@]\
                     " UserTyp.print rt
                (StrMap.print' ~left:"" ~right:"" ~sep:"" ~rev_assoc:true
                   ~aleft:"@\n- @[Type@ " ~assoc:"@ is@ unknown@ for@ field@ "
                   ~aright:"@]" UserTyp.print) fts
            end) infos
      | CyclicSubtypingRelation sst ->
          let pp_typs = pp_lst ~fopen:"@ through@ " ~fclose:"" ~fempty:""
            UserTyp.print in
          error "@[<h>Class@ hierarchy@ has@ cycles;@ e.g,%a@]" (fun fmt ->
            List.iter begin function
              |t::l -> pp fmt "@\n- @[<h>%a@ is@ a@ subtype@ of@ itself%a.\
                                   @]" UserTyp.print t pp_typs l
              | [] -> ()
            end) sst
      | Java.FlatHeap.FieldRedeclaration (u, f, _, _)
      | Java.ClassesOfPrim.FieldRedeclaration (u, f, _, _)
      | Java.Heap.FieldRedeclaration (u, f, _, _) ->
          error "@[<h>Multiple@ declarations@ of@ field@ `%a'@ in@ class@ %a.\
                 @]" pp_field f UserTyp.print u
      | Java.ClassesOfPrim.InconsistentInheritanceOfField (u, f, t, t') ->
          error "@[<h>Inconsistent@ multiple-inheritance@ relation:@ class@ \
                   %a@ inherits@ (at@ least)@ two@ fields@ `%a'@ of@ distinct@ \
                   types@ %a@ and@ %a.\
                 @]" UserTyp.print u pp_field f PrimTyp.print t PrimTyp.print t'
      | Java.Heap.InconsistentInheritanceOfField (u, f, t, t') ->
          error "@[<h>Inconsistent@ multiple-inheritance@ relation:@ class@ \
                   %a@ inherits@ (at@ least)@ two@ fields@ `%a'@ of@ distinct@ \
                   types@ %a@ and@ %a.\
                 @]" UserTyp.print u pp_field f AnyTyp.print t AnyTyp.print t'

  (* --- *)

  let output_classes_bin out_bin s =
    let module Typs: Java.TYPING = (val java_typ_struct s) in
    let oc, close =
      out_bin.Rutils.IO.out_exec
        ~descr:"Class@ declarations@ in@ binary@ format"
        ~backup:Rutils.IO.Forget "classes_bin"
    in
    Marshal.to_channel oc (module Typs: Java.TYPING) [Marshal.Closures];
    close ();
    s

  let input_classes_bin f s =
    Log.i logger "@[<h>Reading@ class@ declarations@ from@ `%a'…@]" pf f;
    let ic = open_in_bin f in
    let module Typs = (val Marshal.from_channel ic: Java.TYPING) in
    close_in ic;
    { s with
      classes = Some (module Typs);
      secsums = SecSums.empty;
      cgwalk = CGNone; }

  (* --- *)

  type body_spec = Raw | Meth

  let read_body_from kind ?lexer_options f =
    let read = match kind with
      | Raw -> Input.RawMeth.from_file
      | Meth -> Input.Method.from_file
    in
    Log.i logger "@[<h>Reading@ method@ from@ `%a'…@]" pf f;
    try Ok (read ?options:lexer_options f) with
      | ParseError e -> error "%a" pp_parse_error e
      | SyntaxError e -> error "%a" pp_syntax_error e
      | Security.Invalid s -> Error (`InvalidSecurity s)

  let compile_method
      (module Opts: RTKB.OPTIONS)
      (module Typs: Java.TYPING)
      secsums bs =
    let module Meth = Method.Compiler (Java.Semantics (Typs)) in
    match Meth.of_impl bs with
      | Error e, warnings ->
          Error (`InMeth e), warnings
      | Ok meth, warnings ->
          Ok (module (struct
            module Meth = Meth
            module SecSums = SecSums
            module MethInfos = CFG.PessimisticMethInfos (Meth)
            module Resolutions = struct
              module FeatureFilters = ScgsCore.Features.MakeFilters (Opts)
              module SecSumForge = SecSum.Forging (FeatureFilters) (Meth.Sem)
                (ScgsCore.Trans.VarTyps)
                (ScgsCore.SymbHeap.ForgeFromFeature (ScgsCore.Base.SCFGEnv))
              include OOLang.Method.Resolutions (Meth) (struct
                type t = SecSum.t
                (* TODO: bring [`All fcall | `Some (fcall, miss)] here to print
                   warnings from where the decision to use third_party is
                   hard-coded, instead of in ScgsCode.Trans. *)
                let fallback _ ?res ec decl = SecSumForge.third_party (C decl)
              end)
            end
            module ResolutionUniverse = struct
              type t = SecSums.callables
              let u = SecSums.callables secsums
              let filter SecSum.{ sign } = sign
            end
            module HeapDef = ScgsCore.SymbHeap.FiniteCompHeapFromFeature
            let meth = meth
          end): ScgsCore.Trans.INPUT), warnings

  let check_method (module I: ScgsCore.Trans.INPUT) =
    match I.Meth.sign I.meth with
      | None -> Ok (module I: ScgsCore.Trans.INPUT)
      | Some sign ->
          let module ArgConsts = Checking.ArgConsts (I.Meth.Sem) in
          match ArgConsts.check sign (I.Meth.body I.meth) with
            | Ok () -> Ok (module I: ScgsCore.Trans.INPUT)
            | Error es -> Error (`Generic (fun fmt -> Checking.pp_errors fmt es))

  let preproc_method
      (module Opts: RTKB.OPTIONS)
      (module I: ScgsCore.Trans.INPUT)
      =
    Ok (module struct
      open I
      module Meth = Meth
      module SecSums = SecSums
      module MethInfos = struct
        include MethInfos
        module CFG = (CFG.Make (Opts) (Meth) (MethInfos)
                        (Resolutions) (ResolutionUniverse))
        module NPPruning = PreProc.NullPointerExceptionPruning (CFG)
        let may_np =
          let decls = Meth.decls meth in
          let cfg = CFG.of_meth meth in
          let may_np' = NPPruning.compute decls (CFG.Fixpoint.setup cfg) in
          fun pc (stm: stm) -> may_np pc stm && Base.PCs.mem pc may_np'
      end
      module Resolutions = Resolutions
      module ResolutionUniverse = ResolutionUniverse
      module HeapDef = HeapDef
      let meth = meth
    end: ScgsCore.Trans.INPUT)

  (* --- *)

  let ( +? ) a b = match a with
    | Error e -> Error e
    | Ok a -> b a

  let ( *? ) a f i = match a i with
    | res, (_, []) -> res
    | res, (meth_name, wrns) -> pp_warnings f (`InMeth (meth_name, wrns)); res

  let ( ++ ) a b = a |> b

  (* --- *)

  let read_secstubs_from ?lexer_options f =
    Log.i logger "@[<h>Reading@ security@ summary@ stubs@ from@ `%a'…@]" pf f;
    try Ok (Input.SecStubs.from_file ?options:lexer_options f) with
      | ParseError e -> error "%a" pp_parse_error e
      | SyntaxError e -> error "%a" pp_syntax_error e
      | Security.Invalid s -> Error (`InvalidSecurity s)
      | SecStub.UnexpectedStubDescriptor s -> Error (`UnexpectedStub s)

  let check_secstubs (module Sem: Semantics.S) secstubs =
    Log.i logger "@[<h>Checking@ security@ summary@ stubs…@]";
    let module Check = SecStub.Checker (Sem) in
    Check.secstubs secstubs +? Check.filter_out_unknowns

  (* --- *)

  module type INPUT = ScgsCore.Trans.INPUT
  module type MODEL = RTKB.REALIB_MODEL
  module type MODEL' = RTKB.REALIB_MODEL_WITH_SECSUM

  type skipped_stats =
      int OOLang.Method.ImplStats.cardinals *
        (int ScgsCore.Stats.SHStats.t *
           (ScgsCore.Stats.time, int) ScgsCore.Stats.SCFGGen.t)
  exception SkippedMethod of (skipped_stats * SecSum.t option * string)

  let pp_cause fmt = function
    | `Timeout t -> pp fmt "Timeout@ exceeded (%gs)" t
    | `Sigint -> pp fmt "Manual@ interrupt@ received"
    | `ForceSkip -> pp fmt "Forced@ skip"
    | `Custom pp -> pp fmt

  let cause_str c = asprintf "@[<h>%a@]" pp_cause c
  let cause_pp c = PPrt.pp2 "@[<h>%a@]" pp_cause c

  let on_forced_meth_skip' sem cause sign stats
      (module FeatureFilters: ScgsCore.Features.FILTERS) =
    let module Sem = (val match sem with
      | `FromTyps (module Typs: Java.TYPING)
        -> (module Java.Semantics (Typs): Semantics.S)
      | `FromInput (module I: INPUT)
        -> (module I.Meth.Sem: Semantics.S))
    in
    match sign with
      | Some sign
        -> Log.w logger "@[@[<h>%a@]:@ forging@ third-party@ summary@ for@ \
                          method@ `%a'.@]\
                       " pp_cause cause OOLang.Sign.Decl.print sign;
          let module SecSumForge =
                SecSum.Forging (FeatureFilters) (Sem)
                  (ScgsCore.Trans.VarTyps)
                  (ScgsCore.SymbHeap.ForgeFromFeature (ScgsCore.Base.SCFGEnv))
          in
          raise (SkippedMethod (stats, Some (SecSumForge.third_party sign),
                                cause_str cause))
      | None
        -> (* XXX: what should happen there? *)
          Log.e logger "@[Unable@ to@ forge@ security@ summary@ \
                          for@ signatureless@ method.@]";
          raise (SkippedMethod (stats, None, cause_str cause))

  let on_forced_meth_skip sem cause (module M: MODEL) =
    RTKB.clear_realib_env' M.env;
    let stats = M.meth_stats, (M.symb_heap_stats, M.scfg_gen_stats) in
    on_forced_meth_skip' sem cause M.sign stats
      (module ScgsCore.Features.MakeFilters (M.SCFG))

  (* --- *)

  let gc x = Gc.major (); x

  let monitors_enabled s =
    s.output_monctrln || s.output_monnode || s.output_monauto || s.output_rmoncfg

  let monitor_contraction_enabled s =
    s.output_monctrln || s.output_monnode || s.output_monauto

  let make_scfg ({ output_cfg; output_cdrs; output_scfg; output_ctrlg;
                   proc_options = { proc_cuddmax } } as s)
      out tro ?synth_algo ?xxx_skip (module I: INPUT) =
    try
      RTKB.make_scfg
        ~output_cfg ~output_cdrs ~output_scfg ~output_ctrlg
        ~anticipate_monitor_contraction: (monitor_contraction_enabled s)
        ~anticipate_rmonitor_construction: s.output_rmoncfg
        ?max_cudd_mem:proc_cuddmax
        out tro ?synth_algo ?xxx_skip (module I)
    with
      | RTKB.SkipSynthesis (stats, cause) ->
          on_forced_meth_skip' (`FromInput (module I: INPUT))
            (`Custom cause) (I.Meth.sign I.meth)
            (I.Meth.ImplStats.stats Cardinals I.meth, stats)
            (module ScgsCore.Features.MakeFilters (val tro))
      | SecSum.MalformedBinding (s, v) ->
          Error (`InSecSum (s, `UnknownVar v))
      | ScgsCore.Trans.IncompatibleSecSumHeap (s, f) ->
          Error (`InSecSum (s, `IncompatibleSecSumHeap f))

  let restore_proc_sigs ({ proc_options = { proc_sigint; proc_timeout };
                           proc_brittle }) x =
    begin match proc_sigint with
      | `Interrupt -> ()
      | `Skip ->
          Sys.(set_signal sigint Signal_default)
    end;
    begin match proc_timeout with
      | RLimitNone -> ()
      | RLimitSoft _ ->
          if not proc_brittle then
            Sys.(set_signal sigvtalrm Signal_default);
          ignore (Unix.(setitimer ITIMER_VIRTUAL
                          { it_interval = 0.; it_value = 0.0 }))
      | RLimitHard _ ->
          if not proc_brittle then
            Sys.(set_signal sigxcpu Signal_default);
          let open Sys_resource in
          let _soft, hard = Resource.(Sys_resource_unix.getrlimit CPU) in
          Resource.(Sys_resource_unix.setrlimit CPU ~soft:Infinity ~hard);
    end;
    x

  let setup_proc_sigs ({ proc_options = { proc_sigint; proc_timeout };
                         proc_brittle } as s) on_skip x =
    let make_handler cause = Sys.Signal_handle (fun _ ->
      restore_proc_sigs s ();
      on_skip cause x)                                (* probably a bad idea… *)
    in
    begin match proc_sigint with
      | `Interrupt -> ()
      | `Skip ->
          Sys.catch_break false;                      (* XXX: not restored!?! *)
          Sys.set_signal Sys.sigint (make_handler `Sigint)
    end;
    begin match proc_timeout with
      | RLimitNone -> ()
      | RLimitSoft t ->
          (* XXX: use virtual until reatk's logger stops using SIGALRM as it
             does now. *)
          if not proc_brittle then
            Sys.(set_signal sigvtalrm (make_handler (`Timeout t)));
          ignore (Unix.(setitimer ITIMER_VIRTUAL
                          { it_interval = 0.; it_value = t }));
      | RLimitHard t ->
          if not proc_brittle then
            Sys.(set_signal sigxcpu (make_handler (`Timeout t)));
          let open Sys_resource in
          let _soft, hard = Resource.(Sys_resource_unix.getrlimit CPU) in
          let t = ceil (Unix.((times ()).tms_utime) +. t) in
          Log.tf' ~level:Log.Warn
            (lazy (let Unix.{ tm_hour; tm_min; tm_sec } = Unix.gmtime t in
                   asprintf "Timeout set at %.5gs (%uh%0.2u:%0.2u)"
                     t tm_hour tm_min tm_sec));
          let t = int_of_float t in
          Resource.(Sys_resource_unix.setrlimit CPU ~soft:(Limit t) ~hard);
    end;
    Ok x

  let make_scfg_n_skip ({ proc_options = { proc_cuddmax } } as s)
      out tro ?synth_algo cause (module I: INPUT) =
    let xxx_skip = PPrt.pp2 "%a" pp_cause cause in
    ignore (RTKB.make_scfg
              ~anticipate_monitor_contraction: (monitor_contraction_enabled s)
              ~anticipate_rmonitor_construction: s.output_rmoncfg
              ?max_cudd_mem:proc_cuddmax
              out tro ?synth_algo ~xxx_skip (module I))

  let synthesize_secsum out f (module I: INPUT)
      ({ synth_algo; sumcomp_algo; output_ctrlf } as s) =
    let sem = `FromInput (module I: INPUT) in
    let synth_algo = opt' synth_algo (meth_opt f s).meth_synth in
    let algo = opt' sumcomp_algo (meth_opt f s).meth_sumcomp in
    let tro = meth_options_module f s in
    match (module I: INPUT)
      ++ setup_proc_sigs s (make_scfg_n_skip s out tro ?synth_algo)
      +? make_scfg s out tro ?synth_algo
      +? setup_proc_sigs s (on_forced_meth_skip sem)
      +? RTKB.compute_secsum ?synth_algo ?algo
      ++ restore_proc_sigs s
    with
      | Error e -> pp_errors f e
      | Ok (module M) -> (module M: MODEL')
      | exception RTKB.SkipSynthesis (stats, cause) ->
          on_forced_meth_skip' (`FromInput (module I: INPUT))
            (`Custom cause) (I.Meth.sign I.meth)
            (I.Meth.ImplStats.stats Cardinals I.meth, stats)
            (module ScgsCore.Features.MakeFilters (val tro))

  let extract_guards out f (module M: MODEL') { output_ctrlf;
                                                output_guards } =
    match (module M: MODEL')
      ++ RTKB.make_guards ~output_ctrlf ~output_guards out
    with
      | Ok m -> m
      | Error e -> pp_errors f e

  let record_secsum ssum ({ secsums } as s) = match ssum with
    | Some ssum -> { s with secsums = SecSums.insert ssum secsums }
    | None -> s

  let extract_secsum out out_bin f (module M: MODEL')
      ({ output_ctrlf; output_secsum; output_secsum_bin; secsums } as s)
      =
    match (module M: MODEL')
      ++ RTKB.extract_secsum_func ~output_ctrlf out
      +? RTKB.clear_realib_env
      +? RTKB.make_secsum ~output_secsum ~output_secsum_bin out out_bin
      ++ gc
    with
      | Ok (stats, ssum)
        -> meth_stats_done f stats s |> record_secsum ssum
      | Error e
        -> pp_errors f e

  let extract_monitor out f ~enable (module M: MODEL')
      ({ output_monctrln; output_monnode; output_monauto; output_rmoncfg } as s)
      =
    match
      if enable && monitors_enabled s
      then (module M: MODEL')
        ++ RTKB.make_monnode ~output_monctrln ~output_monnode ~output_monauto out
        +? RTKB.compute_rmoncfg ~output_rmoncfg out
      else Ok (module M: MODEL')
    with
      | Ok m -> m
      | Error e -> pp_errors f e

  let new_body kind out out_bin f
      ({ lexer_options; synth_algo; sumcomp_algo;
         output_cfg; output_cdrs; output_scfg;
         output_ctrlg; output_ctrlf;
         output_secsum; output_secsum_bin; output_guards;
         secsums } as s)
      =
    let module Typs: Java.TYPING = (val java_typ_struct s) in
    let sem = `FromTyps (module Typs: Java.TYPING) in
    let tro = meth_options_module f s in
    let synth_algo = opt' synth_algo (meth_opt f s).meth_synth in
    let algo = opt' sumcomp_algo (meth_opt f s).meth_sumcomp in
    try
      match
        read_body_from kind ?lexer_options f
        +? compile_method tro (module Typs) secsums *? f
        +? check_method
        +? preproc_method tro
        +? preproc_method tro
        +? make_scfg s out tro ?synth_algo
        +? setup_proc_sigs s (on_forced_meth_skip sem)
        +? RTKB.compute_secsum ?synth_algo ?algo
        ++ restore_proc_sigs s
        +? RTKB.make_guards ~output_ctrlf ~output_guards out
        +? (fun m -> Ok (extract_monitor out f ~enable:true m s))
        +? RTKB.extract_secsum_func ~output_ctrlf out
        +? RTKB.clear_realib_env
        +? RTKB.make_secsum ~output_secsum ~output_secsum_bin out out_bin
        ++ gc
      with
        | Ok (stats, ssum)
          -> meth_stats_done f stats s |> record_secsum ssum
        | Error e
          -> pp_errors f e
    with
      | Sys_error f
        -> error "%s" f
      | SkippedMethod (stats, ssum, cause)
        -> meth_stats_skipped f stats cause s |> record_secsum ssum

  (* --- *)

  let read_secstubs (* out out_bin *) f
      ({ lexer_options; trans_options = tro; secsums } as s)
      =
    let module Typs: Java.TYPING = (val java_typ_struct s) in
    let module Opts = (val tro) in
    let module FF = ScgsCore.Features.MakeFilters (Opts) in
    let module Sem = Java.Semantics (Typs) in
    let module Forging = SecSum.Forging (FF) (Sem)
          (ScgsCore.Trans.VarTyps)
          (ScgsCore.SymbHeap.ForgeFromFeature (ScgsCore.Base.SCFGEnv))
    in
    let module OfStubs = Forging.OfStubs (SecSums) in
    try
      match
        read_secstubs_from ?lexer_options f
        +? check_secstubs (module Sem)
        +? OfStubs.secsums
        ++ gc
      with
        | Ok ssums -> { s with secsums = SecSums.update secsums ssums }
        | Error e -> pp_errors f e
    with
      | Sys_error f -> error "%s" f

  (* --- *)

  let output_secsums ?(suff = "secsums") out ({ secsums } as s) =
    let fmt, close =
      out.Rutils.IO.out_exec
        ~descr:"security@ summaries"
        ~backup:Rutils.IO.Forget suff
    in
    SecSums.print fmt secsums;
    pp_print_newline fmt ();
    close ();
    s

  let output_secsums_bin ?(suff = "secsums_bin") out_bin ({ secsums } as s) =
    let oc, close =
      out_bin.Rutils.IO.out_exec
        ~descr:"security@ summaries@ in@ binary@ format"
        ~backup:Rutils.IO.Forget suff
    in
    SecSums.output_bin oc secsums;
    close ();
    s

  let input_secsum_bin f s =
    Log.i logger "@[<h>Reading@ security@ summary@ from@ `%a'…@]" pf f;
    let ic = open_in_bin f in
    let input = SecSum.input_bin ic in
    close_in ic;
    record_secsum (Some input) s

  let input_secsums_bin f ({ secsums } as s) =
    Log.i logger "@[<h>Reading@ security@ summaries@ from@ `%a'…@]" pf f;
    let ic = open_in_bin f in
    let input = SecSums.input_bin ic in
    close_in ic;
    { s with secsums = SecSums.update secsums input }

  (* --- *)

  let compute_meth_secsum (module Typs: Java.TYPING) sign meth f
      ?(test_convergence = false)
      ?(enable_monitor_extraction = false)
      ({ secsums } as s)
      =
    let out = mk_out_from f and out_bin = mk_bin_out_from f in
    info "@[<h>Considering@ implementation@ given@ in@ `%a'%t@<1>…@]" pf f
      (fun fmt -> if test_convergence then pp fmt ",@ with@ convergence@ test");
    let synth (module I: INPUT) ({ secsums } as s) =
      let module M1 = (val synthesize_secsum out f (module I) s) in
      let postproc s =
        ignore (extract_guards out f (module M1) s);
        ignore (extract_monitor out f (module M1) s
                  ~enable:enable_monitor_extraction);
        s
      in
      match try Some (SecSums.find sign secsums) with Not_found -> None with
        | Some prev_ssum when (test_convergence &&
                                 RTKB.secsum_match (module M1) prev_ssum) ->
            Log.i logger "@[Security@ summary@ for@ `%a'@ has@ converged.@]\
                         " pf f;
            (* pp std_formatter "%a@." SecSum.print prev_ssum; *)
            let s = meth_stats_done f (RTKB.extract_stats (module M1)) s in
            postproc s, true
        | Some _ | None ->
            let s = if not test_convergence then postproc s else s in
            let s = extract_secsum out out_bin f (module M1) s in
            s, false
    in
    let tro = meth_options_module f s in
    match meth
      ++ compile_method tro (module Typs) secsums *? f
      +? check_method
      +? preproc_method tro
      +? preproc_method tro
    with
      | Error e -> pp_errors f e
      | Ok (module M0) -> try synth (module M0) s with
          | SkippedMethod (stats, ssum, cause)
            -> meth_stats_skipped f stats cause s |> record_secsum ssum, true

  let bootstrap_meth_secsum ssum_boot decl f ({ secsums } as s) =
    if SecSums.mem (Sign.Decl.sign decl) secsums then begin
      Log.i logger "@[<h>Not@ bootstrapping@ method@ given@ in@ `%a'@ due@ to@ \
                      existing@ security@ summary.@]" pf f;
      s
    end else begin
      Log.i logger "@[<h>Bootstrapping@ method@ given@ in@ `%a'@<1>…@]" pf f;
      let ssum = ssum_boot decl in
      Log.i logger "@[<h>Bootstrapped@ security@ summary:@]@\n@[<2>  %a@]\
                   " SecSum.print ssum;
      record_secsum (Some ssum) s
    end

  let actual_cause cause proc_options = match cause, proc_options with
    | `Signal i, { proc_timeout = RLimitHard t | RLimitSoft t }
          when i = Sys.sigxcpu || i = Sys.sigvtalrm -> `Timeout t
    | `Signal i, _(* { proc_sigint = `Skip } *)
          when i = Sys.sigint -> `Sigint
    | _ -> `ForceSkip

  let skip_meth (module Typs: Java.TYPING) cause meth f ({ secsums } as s) =
    let out = mk_out_from f in
    Log.i logger "@[<h>Skipping@ implementation@ given@ in@ `%a'@<1>…@]" pf f;
    let tro = meth_options_module f s in
    try match meth
        ++ compile_method tro (module Typs) secsums *? f
        +? check_method
        +? preproc_method tro
        +? preproc_method tro
        +? (make_scfg s out (meth_options_module f s)
              ~xxx_skip:(cause_pp (actual_cause cause s.proc_options)))
      with
        | Error e -> pp_errors f e
        | Ok m -> begin
          let sem = `FromTyps (module Typs: Java.TYPING) in
          let cause = actual_cause cause s.proc_options in
          ignore (on_forced_meth_skip sem cause m); s, true
        end
    with
      | SkippedMethod (stats, ssum, cause)
        -> meth_stats_skipped f stats cause s |> record_secsum ssum, true

  (* --- *)

  let on_cgwalk ~wlk ~stg ({ cgwalk } as s) =
    try match cgwalk with
      | CGNone -> Log.w logger "@[<h>No@ callgraph@ constructed@ yet.@]"; s
      | CGWalk x -> wlk x s
      | CGStge x -> stg x s
    with
      | Sys_error f -> error "%s" f

  (* --- *)

  let output_cgwalk_aux_state_bin oc { meth_options; meth_stats; secsums } =
    SecSums.output_bin oc secsums;
    MethOptionsIO.output_bin oc meth_options;
    ScgsStats.IO.output_bin oc meth_stats

  let input_cgwalk_aux_state_bin ic s =
    let secsums = SecSums.input_bin ic in
    let meth_options = MethOptionsIO.input_bin ic in
    let meth_stats = ScgsStats.IO.input_bin ic in
    { s with meth_options; meth_stats; secsums }

  let cgwalk_bin_open = function
    | `Filename f ->
        let oc = open_out_bin f in oc, fun () -> flush oc; close_out oc
    | `RutilsOut out_bin ->
        out_bin.Rutils.IO.out_exec
          ~descr:"full@ callgraph@ walker@ in@ binary@ format"
          ~backup:Rutils.IO.Forget "cgwalk_bin"

  let output_cgwalk_bin out = on_cgwalk
    ~wlk: begin fun (module CGWalk) s ->
      let oc, close = cgwalk_bin_open out in
      ScgsCallGraphWalk.IO.output_bin oc
        (ScgsCallGraphWalk.IO.Full (module CGWalk));
      output_cgwalk_aux_state_bin oc s;
      close ();
      s
    end
    ~stg: begin fun (module CGWalkStage) s ->
      let oc, close = cgwalk_bin_open out in
      ScgsCallGraphWalk.IO.output_bin oc
        (ScgsCallGraphWalk.IO.Stage (module CGWalkStage));
      output_cgwalk_aux_state_bin oc s;
      close ();
      s
    end

  let input_cgwalk_bin f s =
    Log.i logger "@[<h>Reading@ callgraph@ walker@ from@ `%a'…@]" pf f;
    let ic = open_in_bin f in
    let cgwalk = ScgsCallGraphWalk.IO.(match input_bin ic with
      | Full m -> CGWalk m
      | Stage m -> CGStge m)
    in
    let s = input_cgwalk_aux_state_bin ic { s with cgwalk } in
    close_in ic;
    s

  (* --- *)

  let cgwalk_stage_open = function
    | `Filename f ->
        let oc = open_out_bin f in oc, fun () -> flush oc; close_out oc
    | `RutilsOut out_bin ->
        out_bin.Rutils.IO.out_exec
          ~descr:"callgraph@ walker@ step@ in@ binary@ format"
          ~backup:Rutils.IO.Forget "cgwalk_stage"

  let output_cgwalk_stage out =
    let output_stage (module CGWalkStage: CGWALK_STAGE) s =
      let oc, close = cgwalk_stage_open out in
      CGWalkStage.IO.output_bin oc CGWalkStage.visits;
      output_cgwalk_aux_state_bin oc s;
      close ();
      s
    in
    on_cgwalk
      ~wlk: begin fun (module CGWalk) ->
        let module CGWalkStage = ScgsCallGraphWalk.LockSteps (CGWalk) in
        output_stage (module CGWalkStage)
      end
      ~stg: output_stage

  let input_cgwalk_stage f =
    let input_stage (module CGWalkStage: CGWALK_STAGE) s =
      Log.i logger "@[<h>Reading@ callgraph@ walker@ stage@ from@ `%a'…@]" pf f;
      let ic = open_in_bin f in
      let s = { s with cgwalk = CGStge (module struct
        include CGWalkStage
        let visits = IO.input_bin ic
      end: CGWALK_STAGE) } |> input_cgwalk_aux_state_bin ic in
      close_in ic;
      s
    in
    on_cgwalk
      ~wlk: begin fun (module CGWalk) ->
        let module CGWalkStage = ScgsCallGraphWalk.LockSteps (CGWalk) in
        input_stage (module CGWalkStage)
      end
      ~stg: input_stage

  (* --- *)

  let cg_stage_info = on_cgwalk
    ~wlk: begin fun _ s ->
      info "@[Full@ callgraph@ walk@ registered.@]"; s
    end
    ~stg: begin fun (module CGWalkStage) s ->
      match CGWalkStage.try_visit CGWalkStage.visits with
        | None
          -> info "@[End@ of@ callgraph@ walk@ already@ reached.@]"; s
        | Some (v, _)
          -> info "@[Next@ stage@ of@ callgraph@ walk@ consists@ in@ %a.@]"
            CGWalkStage.(fun fmt -> function
              | WeakTopo.V CG.Entry.{ filename = f }
                -> pp fmt "summarization@ of@ method@ from@ file@ `%a'" pf f
              | WeakTopo.Ch (CG.Entry.{ filename = f }, _)
                -> pp fmt "boostraping@ of@ method@ from@ file@ `%a'" pf f
              | WeakTopo.Ct (CG.Entry.{ filename = f }, _)
                -> pp fmt "summarization@ of@ method@ from@ file@ `%a',@ with@ \
                          convergence@ test" pf f) v;
            s
    end

  let cgwalk_end (module CGWalkStage: CGWALK_STAGE) s =
    { s with cgwalk = CGStge (module (struct
      include CGWalkStage
      let visits = nosteps
    end): CGWALK_STAGE); }

  (* --- *)

  module CGVisits
    (Opts: RTKB.OPTIONS)
    (Typs: Java.TYPING)
    (CGWalkBase: ScgsCallGraphWalk.BASE with module Typs = Typs)
    =
  struct
    open CGWalkBase
    module FF = ScgsCore.Features.MakeFilters (Opts)
    module SecSumForge = SecSum.Forging (FF) (CG.Compiler.Sem)
      (ScgsCore.Trans.VarTyps)
      (ScgsCore.SymbHeap.ForgeFromFeature (ScgsCore.Base.SCFGEnv))

    let meth (CG.Entry.{ filename; impl } as m) =
      compute_meth_secsum (module Typs) (CG.Entry.sign m) impl filename
    and boot (CG.Entry.{ filename; decl }) =
      bootstrap_meth_secsum SecSumForge.bottom decl filename
    let meth_mon = meth ~enable_monitor_extraction:true
    (* let meth_nomon = meth ~enable_monitor_extraction:false *)
    let skip cause CG.Entry.{ filename; impl } =
      skip_meth (module Typs) cause impl filename

    open WeakTopo
    let visit ?skip_next : _ visit -> state -> state * bool = fun v ->
      match v, skip_next with
        |(V m | Ct (m, _)), Some cause -> skip cause m
        | V m, _ -> meth_mon m ~test_convergence:false
        | Ch (m, _), _ -> fun s -> boot m s, true
        | Ct (m, _), _ -> meth_mon m ~test_convergence:true
  end

  let cg_stages ?n ?save ?skip_next
      (module CGWalkStage: CGWALK_STAGE) ({ trans_options } as s) =
    let module Opts = (val trans_options) in
    let module Visits = CGVisits (Opts) (CGWalkStage.Typs) (CGWalkStage) in
    let rebuild visits' s = { s with cgwalk =
        CGStge (module (struct
          include CGWalkStage
          let visits = visits'
        end): CGWALK_STAGE); }
    in
    let rec visits_aux ?skip_next i visits s =
      match CGWalkStage.try_visit visits, i with
        | None, None -> None, s                                 (* do not warn *)
        | None, _
          -> Log.w logger "@[End@ of@ callgraph@ walk@ already@ reached.@]";
            None, s
        | _, Some i when i <= 0 ->
            Some visits, s
        | Some (v, visits'), i
          -> let s, conv = Visits.visit ?skip_next v s in
            let visits' = visits' conv in
            maybe save (lazy (rebuild visits' s));
            visits_aux (app_opt pred i) visits' s
    in
    match visits_aux ?skip_next n CGWalkStage.visits s with
      | None, s -> s
      | Some visits', s -> rebuild visits' s

  let cg_locksteps ?n ?save ?skip_next =
    on_cgwalk
      ~wlk: begin fun (module CGWalk) ->
        let module CGWalkStage = ScgsCallGraphWalk.LockSteps (CGWalk) in
        cg_stages ?n ?save ?skip_next (module CGWalkStage)
      end
      ~stg: (cg_stages ?n ?save ?skip_next)

  let cg_fullwalk =
    on_cgwalk
      ~wlk: begin fun (module CGWalk) ({ trans_options } as s) ->
        let module Visits = (CGVisits (val trans_options)
                               (CGWalk.Typs) (CGWalk)) in
        CGWalk.WeakTopo.fold Visits.visit CGWalk.wtopo s
        |> let module CGWalkStage = ScgsCallGraphWalk.LockSteps (CGWalk) in
           cgwalk_end (module CGWalkStage)
      end
      ~stg: cg_stages

  (* --- *)

  let cg_safe_crawl ?n =
    let tmp = Filename.temp_file "state" ".cgwalk_stage" in
    let tmp' = tmp ^".tmp" in
    let save s =
      ignore (output_cgwalk_stage (`Filename tmp') (Lazy.force s));
      Sys.rename tmp' tmp
    and maybe_restore s =
      if Sys.file_exists tmp then input_cgwalk_stage tmp s else s
    and cleanup () =
      if Sys.file_exists tmp' then Sys.remove tmp';
      Sys.remove tmp
    in
    let rec proceed_with ?skip_next ntries s =
      if ntries >= 42 then
        failwith (asprintf "Max number of retries (%u) exceeded!" ntries);
      let sigmsk = Unix.(sigprocmask SIG_BLOCK Sys.[sigint; sigquit]) in
      let pid = Unix.fork () in
      if pid == 0 then begin                                          (* child *)
        ignore Unix.(sigprocmask SIG_SETMASK sigmsk);
        let s = maybe_restore s in
        let s = { s with proc_brittle = true } in
        ignore (cg_locksteps ?n ~save ?skip_next s);
        exit 0
      end else begin                                                (* parent *)
        let sigint = Sys.(signal sigint Signal_ignore) in
        let sigquit = Sys.(signal sigquit Signal_ignore) in
        let _, status = Unix.waitpid [] pid in
        ignore Unix.(sigprocmask SIG_SETMASK sigmsk);
        Sys.set_signal Sys.sigint sigint;
        Sys.set_signal Sys.sigquit sigquit;
        match status with
          (* | _ when not (Sys.file_exists tmp) -> *)
          (*     failwith (asprintf "Missing temporary state file `%s'!" tmp); *)
          | Unix.WSIGNALED i when List.memq i Sys.[sigbus; sigsegv; sigabrt] ->
              Log.w logger "@[<h>Attempting@ callgraph@ walker@ recovery@ \
                              after@ it@ received@ signal@ %d@<1>…@]" i;
              proceed_with (succ ntries) s
          | Unix.WSIGNALED i when List.memq i Sys.[sigint; sigxcpu; sigvtalrm] ->
              (* sigxcpu: reached resource limit *)
              (* Log.w logger "@[Child@ process@ recieved@ signal@ %d.@ \ *)
              (*                 Skipping@ next@ method@ and@ proceeding@<1>…@]" i; *)
              proceed_with ~skip_next:(`Signal i) 0 s
          | Unix.WSIGNALED i when i <> 0 ->
              Log.w logger "@[<h>Child@ process@ recieved@ signal@ %d.@ \
                              Terminating@<1>…@]" i;
              terminate s
          | Unix.WEXITED 1 ->
              (* XXX: dangerous, temporary workaround until we can make CUDD
                 abort instead of just exit 1 when it runs out of memory. *)
              Log.w logger "@[<h>Child@ process@ exited@ with@ status@ 1.@ \
                              Skipping@ next@ method@ and@ proceeding@<1>…@]";
              proceed_with ~skip_next:`Forced 0 s
          | _ ->
              terminate s
      end
    and terminate s =
      let s = maybe_restore s in
      cleanup ();
      s
    in
    fun s -> save (lazy s); proceed_with 0 s

  (* --- *)

  let values descr lookup ok reset s = match lookup (String.trim s) with
    | Ok v -> ok v
    | Error "reset" -> reset
    | Error s -> Log.w logger "@[<h>Unknown@ specifier@ `%s'@ for@ \
                                method-specific@ %(%).@]" s descr; id

  let synth s o = { o with meth_synth = Some s }
  let synth'  o = { o with meth_synth = None }

  let prop s o = { o with meth_prop = Some s }
  let prop'  o = { o with meth_prop = None }
  let prop = values "propagation@ in@ generted@ SCFG"
    OptHelpers.prop prop prop'

  let semloc s o = { o with meth_semloc = Some s }
  let semloc'  o = { o with meth_semloc = None }
  let semloc = values "algorithm@ for@ encoding@ semantic@ locations"
    OptHelpers.semloc_encoding semloc semloc'

  let trmrg s o = { o with meth_trans_merge = Some s }
  let trmrg'  o = { o with meth_trans_merge = None }
  let trmrg = values "policy@ for@ merging@ transitions"
    OptHelpers.trans_merge trmrg trmrg'

  let read_meth_files_from f =
    Log.i logger "@[Reading@ list@ of@ method@ files@ from@ `%a'…@]" pf f;
    let comment_parsers = [
      ScgsUtils.IO.mk_parser "%_[#] + synth : %s %!" synth;
      ScgsUtils.IO.mk_parser "%_[#] - synth %!" synth';
      ScgsUtils.IO.mk_parser "%_[#] + prop : %[a-zA-Z \t]%!" prop;
      ScgsUtils.IO.mk_parser "%_[#] - prop %!" prop';
      ScgsUtils.IO.mk_parser "%_[#] + semloc : %[a-zA-Z \t]%!" semloc;
      ScgsUtils.IO.mk_parser "%_[#] - semloc %!" semloc';
      ScgsUtils.IO.mk_parser "%_[#] + trans_merge : %[a-zA-Z \t]%!" trmrg;
      ScgsUtils.IO.mk_parser "%_[#] - trans_merge %!" trmrg';
      (* ScgsUtils.IO.mk_parser "%_[#] + full_prop : %B %!" full_prop; *)
      (* ScgsUtils.IO.mk_parser "%_[#] + const_prop : %B %!" const_prop; *)
    ] in
    let reg_opts f = function
      | o, _ as a when o = no_meth_options -> a
      | o, omap -> o, StrMap.add f o omap
    in
    let fold_lines ic = ScgsUtils.IO.fold_lines
      ~trim_comments: false
      ~comment: (fun l -> match ScgsUtils.IO.try_parse comment_parsers l with
        | None -> ign
        | Some x -> app_snd (app_fst x))
      (fun f -> app_pair (List.cons f) (reg_opts f))
      ic ([], (no_meth_options, StrMap.empty))
    in
    try fold_lines (open_in f) |> app_pair List.rev snd with
      | Sys_error f -> error "%s" f

  let read_meth_files_from out f ({ lexer_options; secsums } as s) =
    let meth_files, meth_options = read_meth_files_from f in
    Log.i logger "@[<h>Building@ callgraph…@]";
    let read_meth = read_body_from Meth ?lexer_options in
    let module Typs: Java.TYPING = (val java_typ_struct s) in
    let module Sem = Java.Semantics (Typs) in
    let module CG = CallGraph.Make (Sem) in
    let module CGWalk = ScgsCallGraphWalk.Make (CG) in
    try let res, warnings = CG.from_filenames read_meth meth_files in
        if warnings <> [] then pp_warnings' f warnings;
        match res with
          | Error (`InFile (f, _)) -> error "in file %s" f
          | Error (`InMeth (f, e)) -> pp_errors f (`InMeth (Some f, e))
          | Error (`InMeth' (f, es)) ->
              pp_errors f (`Generic (fun fmt -> Checking.pp_errors fmt es))
          | Ok (cg, miss) ->
              CGWalk.output_cg_dot out cg;
              let has_no_secsum s =
                match Sem.FCall.lookup s (SecSums.callables secsums) with
                  | [], _, _ -> true
                  | _ -> false
              in
              let miss = Sem.FCalls.filter has_no_secsum miss in
              if not (Sem.FCalls.is_empty miss) then
                Log.w logger "@[<h>No@ implementation@ nor@ security@ summary@ \
                                found@ for@ the@ following@ methods:@]@\n%a\
                             " Sem.FCalls.print miss;
              let wtopo = CGWalk.WeakTopo.of_callgraph cg in
              (* CGWalk.WeakTopo.output_dot out wtopo; *)
              { s with
                meth_options;
                cgwalk = CGWalk (module (struct
                  module Typs = Typs
                  include CGWalk
                  let wtopo = wtopo
                end): CGWALK); }
    with
      | Sys_error f -> error "%s" f

end
