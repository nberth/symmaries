open Format
open Rutils
open Utils
open Base
module V = Var                                                       (* alias *)
open OOLang.Base
open OOLang.CDR
open OOLang.Method
open OOLang
open SCFG
open SemLoc
open V

(* --- *)

let level = Log.Debug
let logger = Log.mk ~level "Tr"

(* --- *)

module type COMMON_VARS = sig
  val branch_var: v
  val oracle_var: v
  val branch_mult_label: int -> var
  val branch_mult_typ: string
  val ua_var: v
  val sink_pred_exc_label: var
  val sink_pred_var: v
  val sink_label_of: pc -> var
  val exh_typ: string
  val exh_var: v
  val exh: SCFGEnv.eexp
  val exh_oracle_var: v
  val exh_oracle: SCFGEnv.eexp
  val exh_label_of: int -> var
  val exh_label_none: var
end
module CommonVars (Opts: OPTIONS) : COMMON_VARS = struct
  open Opts
  let branch_var: v = Var "τ"
  let oracle_var: v = Var "ω"
  let branch_mult_label: int -> var = asprintf "T%i"
  let branch_mult_typ = "Branch"
  let ua_var: v = Var "υ"
  let sink_label_of: pc -> var = asprintf "S%a" PC.print
  let sink_pred_exc_label: var = "SExc"
  let sink_pred_var: v = Var "ϕ"
  let exh_typ = "CatchTableEntry"
  let exh_var: v = Var "χ"
  let exh = SCFGEnv.Eexp.var exh_var
  let exh_oracle_var: v = Var "ξ"
  let exh_oracle = SCFGEnv.Eexp.var exh_oracle_var
  let exh_label_none: var = "H."
  let exh_label_of: int -> var = asprintf "H%i"
end

module VarTyps = struct
  let lpc_var: v = Var "pc"
  let lpc = SCFGEnv.Lexp.var lpc_var
  (* let saved_pc_var: v = Prime lpc_var *)
  let ( ~~ ) (v: var) = SCFGEnv.Lexp.var (Level v)
  let ( ?~ ) (v: var) = SCFGEnv.Bexp.var (Taint v)
end

(* --- *)

module SecSum = SecSum.Make (SCFGEnv)

exception IncompatibleSecSumHeap of SecSum.t * Features.heapdom expected

module type INPUT = sig
  module Meth: Method.T
  module SecSums: SecSum.SET
    with module Map = Meth.Sem.FCall.Lookup
  module MethInfos: CFG.METH_INFOS
    with module Meth := Meth
  module Resolutions: Method.RESOLUTIONS
    with module Meth := Meth
    and type e = SecSum.t
  module ResolutionUniverse: sig
    include CFG.RESOLUTION_UNIVERSE
    with type t = Resolutions.universe
    val filter: Resolutions.e -> Sign.decl
  end
  module HeapDef: SymbHeap.FINITE_COMPS_HEAP
  (* with module Env := SCFGEnv *)
  val meth: Meth.t
end

(* --- *)

type effects_inference =
  | ControllablePreamble
  | CoreachOnly

module type EFFECTS_INFERENCE = sig
  val effects_inference: effects_inference
end

module DefaultEffectsInference: EFFECTS_INFERENCE = struct
  let effects_inference = CoreachOnly
end

(* --- *)

module type S = sig
  include SCFG.T with module Env = SCFGEnv
  val scfg: t

  include OPTIONS
  include EFFECTS_INFERENCE
  include COMMON_VARS
  val local_vars: Env.VarSet.t

  (** Predicate holding when in nominal mode, in terms of the SCFG variables *)
  val ua_nom: Env.bexp
  val actual_branching: Env.bexp LocMap.t

  (** Invariant relating relationships between formal arguments *)
  val arg_alias_invariant: Env.bexp
  val arg_alias_invariant_support_size: int

  (** State variables that hold argument-related types and info *)
  val support: Env.VarSet.t

  (** State variables that hold heap-effect summarizations upon method return *)
  val footprint: typ Env.VarMap.t
  val footprint': Env.VarSet.t

  (* (\** Facts accumulated about local variables at custom checkpoints *)
  (*     (initialization…) *\) *)
  (* val checkpoint_asserts: Env.bexp Env.VarMap.t *)

  (** Custom user policy variables *)
  val policies: SecSum.policies

  (** Features handled by the translation *)
  val features: Features.record

  (** Custom variable affinities that can be used to find "good" order for
      variables when constructing (MT)BDD-based representations of SCFGs. *)
  val variable_affinities: Env.VarSet.t list

  (** Output a CDR-annotated CFG in DOT format *)
  val output_cdrs_dot: formatter -> unit

  val heap_stats: int SymbHeap.Stats.cardinals
end

(* --- *)

module UpdateHelpers
  (FeatureFilters: Features.FILTERS)
  (Heuristics: SCFGEnv.HEURISTICS)
  (CommonVars: COMMON_VARS)
  =
struct
  open FeatureFilters
  open SCFGEnv
  open Updates
  open Bexp
  open Lexp
  open CommonVars
  open VarTyps
  open Base
  open Heuristics

  let forall l f = List.fold_left (fun acc v -> f v &&. acc) tt l
  let forall_u l f u = List.fold_left (fun u v -> f v u) u l

  (* --- *)

  let ( ||~ ) a b = if Features.track_levels <> NoFlows then a else b

  (* --- *)

  let rec expr_level: unqual expr -> lexp = function
    | _ when Features.track_levels = NoFlows -> bottom
    | Expr.Lit _ -> bottom
    | Expr.Var v -> Lexp.var (Level v)
    | Expr.Unop (_, e) | Expr.Cast (_, e) -> expr_level e
    | Expr.Binop (e, _, e')
    | Expr.Relop (e, _, e') -> (expr_level e) |.| (expr_level e')

  let rec cond_level: cond -> lexp = function
    | _ when Features.track_levels = NoFlows -> bottom
    | Expr.Cond.Expr e -> expr_level e
    | Expr.Cond.Rel (e, _, e') -> (expr_level e) |.| (expr_level e')

  let ( <~~ ) (v: var) = setl (Level v)
  let ( ~~?% ) (v: var) = Lexp.var (Prime (Ret (Level v)))
  let ( ~~!% ) (v: var) = Lexp.var (Prime (Exc (Level v)))

  (* --- *)

  type texp = bexp
  let rec expr_taint: unqual expr -> texp = function
    | _ when not Features.track_taints -> ff
    | Expr.Lit _ -> ff
    | Expr.Var v -> Bexp.var (Taint v)
    | Expr.Unop (_, e) | Expr.Cast (_, e) -> expr_taint e
    | Expr.Binop (e, _, e')
    | Expr.Relop (e, _, e') -> (expr_taint e) ||. (expr_taint e')

  let rec cond_taint: cond -> texp = function
    | _ when not Features.track_taints -> ff
    | Expr.Cond.Expr e -> expr_taint e
    | Expr.Cond.Rel (e, _, e') -> (expr_taint e) ||. (expr_taint e')

  let ( <?~ ) (v: var) = setb (Taint v)
  let ( ?~?% ) (v: var) = Bexp.var (Prime (Ret (Taint v)))
  let ( ?~!% ) (v: var) = Bexp.var (Prime (Exc (Taint v)))

  (* --- *)

  let set_lpc, save_pc, lpc = match Features.track_levels with
    | NoFlows | ExplicitFlows -> (fun _ -> ign), ign, bottom
    | AllFlows -> setl lpc_var, (* setl saved_pc_var lpc *)ign, lpc
  let saved_pc = (* Lexp.var saved_pc_var *)bottom
  let restore_pc = set_lpc (* saved_pc *)bottom

  (* --- *)

end

(* --- *)

module BodyImpl
  (FeatureFilters: Features.FILTERS)
  (I: INPUT)
  =
struct
  module Meth = I.Meth
  open FeatureFilters
  open Meth
  open Sem
  let excn = excnvar
  let sign = sign I.meth
  let body = body I.meth
  let decls = decls I.meth
  let prms = prms decls
  let refs = refs decls
  let refs = if Features.track_excs then refs else VarMap.remove excn refs
  let args = match sign with Some s -> args s | None -> VarMap.empty
  let arg_refs = VarMap.merge (fun v at rt -> match at, rt with
    | Some _, Some rt -> Some rt
    | _ -> None) args refs
  let ret_some = match sign with Some s -> ret_some s | None -> true
  let ret_refp = match sign with Some s -> ret_refp s | None -> true
  let retdom = match sign with Some s -> ret_refdom s | None -> None
  let catch_table = Body.catch_table body

  (* Sets of variables *)
  let all_refs = VarMap.keys refs
  let all_prms = VarMap.keys prms
  let all_args = VarMap.keys args
  let all_vars = Vars.union all_refs all_prms
  let all_locals = Vars.diff all_vars all_args
  let fold_all_vars f = Vars.fold f all_vars
  let fold_all_args f = Vars.fold f all_args
  let fold_all_locals f = Vars.fold f all_locals
end

(* --- *)

module type APP_OPTIONS = sig
  include OPTIONS
  include EFFECTS_INFERENCE
  module SemLocEncoding: SemLoc.ENCODING
end

module App
  (Opts: APP_OPTIONS)
  (I: INPUT)
  : S =
struct

  module FeaturesRecord = Features.Record
  module FeatureFilters = Features.MakeFilters (Opts)
  module Heuristics = MakeHeuristics (Opts)
  module CommonVars = CommonVars (Opts)
  include Opts
  include CommonVars
  include FeatureFilters

  module Vs = SCFGEnv.VarSet
  module U = SCFGEnv.Updates
  module Body = BodyImpl (FeatureFilters) (I)
  module Sem = I.Meth.Sem

  open SCFGEnv.Bexp
  open VarTyps
  open Body
  open V

  (* --- *)

  (* Features used/required in translation *)
  let features = FeatureFilters.record;;
  Log.i logger "@[Active@ flags:@ %a@]" FeaturesRecord.print_enabled features;;

  (* --- *)

  module CDRs = struct
    module CFG = (CFG.Make (Opts) (I.Meth) (I.MethInfos)
                    (I.Resolutions) (I.ResolutionUniverse))
    include CDR.Make (CFG)
    let cfg = CFG.of_meth I.meth
    let cfg_fp_setup = CFG.Fixpoint.setup cfg
    let cdrs = of_cfg cfg
    let ( ~@? ) f i = if has_regions cdrs then f i else i
  end

  (* --- *)

  include (Opts.SemLocEncoding (FeatureFilters) (SCFGEnv) (Heuristics)
             (Body) (CDRs))
  include SCFGGraph

  open SemLoc

  (* --- *)

  module GenData = struct
    type t =
        {
          (* custom_checkpoints: (SemLoc.t * var) PCMap.t; *)
          actual_branching: Env.Bexp.t LocMap.t;
        }
    let init =
      {
        (* custom_checkpoints = PCMap.empty; *)
        actual_branching = LocMap.empty;
      }
  end

  module Gen = struct
    include MakeGen (GenData)
    open GenData

    (* let register_custom_checkpoint l c g = *)
    (*   let { custom_checkpoints } as d = data g in *)
    (*   new_data g { d with *)
    (*     custom_checkpoints = PCMap.add (pc l) (l, c) custom_checkpoints } *)

    let register_actual_branching l g =
      let { actual_branching } as d = data g in
      let l, loc_predicate = actual_loc l in
      new_data g { actual_branching = LocMap.add l loc_predicate actual_branching }
  end

  module SCFGGen = MakeSCFGGenOverlay (Gen)
  module UH = UpdateHelpers (FeatureFilters) (Heuristics) (CommonVars)
  module H = I.HeapDef (SCFGEnv) (FeatureFilters) (Heuristics) (struct
    module Sem = Meth.Sem
    include Body
    let ( ~@? ) = CDRs.( ~@? )
  end)

  (* --- *)

  open H
  open CDRs
  open Gen
  open SCFGEnv
  open SCFGGen
  open GenLoc
  open Updates
  open UH
  open Bexp
  open Lexp
  open Eexp
  open Heuristics

  (* --- *)

  let secsums = I.ResolutionUniverse.u

  module SecSumBind = SecSum.Binding (H)
  let pc2ssum =
    let module SSumsLookup = I.Resolutions.Direct (struct

      let check_features = List.map begin fun s ->
        let sf = SecSum.features s in
        (match FeaturesRecord.compatible sf features with Ok () -> ()
          | Error f -> raise @@ IncompatibleSecSumHeap (s, {
            got = sf.heapd; expected = features.heapd }));
        let missing = FeaturesRecord.missing_flags sf features in
        if missing <> [] then
          Log.w logger "@[Warning:@ security@ summary@ for@ `%a'@ is@ missing@ \
                          %a@ that@ %s@ required@ for@ the@ current@ \
                          translation@ (it@ was@ generated@ with@ flags@ `%a').\
                        @]"
            SecSum.pp_shortname s
            (FeaturesRecord.print_missing
               ~pref_unique:"feature@ " ~pref_mult:"features@ ") missing
            (if List.tl missing = [] then "is" else "are")
            FeaturesRecord.print sf;
        s
      end

      let merge _pc ?res ec ssum0 ssums =
        let pcsubst = if ssums = [] then ign else begin
          let open Stm.Ops in
          let disp_prms, disp_refs = Sem.FCall.dispatch_support_prms_refs ec in
          let disp_level = begin lpc
              |> forall_u disp_prms
                  (fun i l -> match !^i with | V v -> l |.| ~~v | L _ -> l)
              |> forall_u disp_refs
                  (fun i l -> match !^i with | V r -> l |.| ~~r |.| ~^r | L _ -> l)
          end in
          SecSum.subst_in_exprs (function
            | v when Var.equal lpc_var v -> `Expr (`Lexp disp_level)
            | _ -> `None)
        end in
        let ssum = (ssum0 :: ssums)
          |> check_features
          |> SecSumBind.bind_merge
              Sem.merge_throws (Sem.FCall.argmap ec) all_locals
          |> pcsubst
        in
        Log.d2 logger "@[<h>Preliminary@ security@ summary:@\n  @[%a@]@]\
                      " SecSum.print ssum;
        if SecSum.guard ssum = ff then
          Log.w logger "@[<h>Warning:@ security@ summary@ for@ `%a'@ has@ \
                          an@ unsatisfiable@ guard.@]\
                       " SecSum.pp_shortname ssum;
        ssum

    end) in
    let pc2ssum, missing = SSumsLookup.lookup_all I.meth secsums in
    Sign.Map.iter begin fun _ -> function
      | `All ec ->
          Log.w logger "@[No@ security@ summary@ found@ for@ `%a':@ \
                          considering@ third-party@]\
                       " Sem.FCall.print_typsign ec
      | `Some (ec, missing) ->
          Log.w logger "@[No@ security@ summary@ found@ for@ `%a'@ \
                          in@ case@ target@ object@ is@ one@ of@ %a@ \
                          or@ any@ of@ their@ superclasses:@ \
                          considering@ third-party@]\
                       " Sem.FCall.print_typsign ec Typ.UserTyps.print missing
    end missing;
    pc2ssum

  let policies, pc2ssum =
    let pc_n_secsums = PCMap.bindings pc2ssum in
    let pcs, secsums = List.split pc_n_secsums in
    let policies, secsums = SecSum.unify_policies secsums in
    policies,
    List.fold_left2 (fun m pc ssum ->
      Log.d logger "@[<h>Effective@ security@ summary@ at@ pc = %a:@\n  @[%a@]\
                    @]" PC.print pc SecSum.print ssum;
      PCMap.add pc ssum m)
      PCMap.empty pcs secsums

  let decl_policies =
    Policy.Map.fold (fun p v ->
      decl v (match Policy.dom p with Taint -> Bool | Level -> Level) State)
      policies

  (* --- *)

  module NomSink = struct
    type t =
      | RetU
      | RetP of unqual expr
      | RetR of Stm.imref
    let compare = Stdlib.compare
  end
  module NomSinks = Map.Make (NomSink)
  open NomSink

  let sink_pred_typ = "SinkPred"
  let nom_sinks, sink_exc =
    let open Stm in
    let pp_stm = print_linked ?pad:None ~pp_lvl:Security.print in
    let rec register_sink pc stm ns = NomSinks.(match stm with
      | ReturnU   when not (mem  RetU    ns) -> add  RetU    pc
      | ReturnP e when not (mem (RetP e) ns) -> add (RetP e) pc
      | ReturnR r when not (mem (RetR r) ns) -> add (RetR r) pc
      | _ when not ret_some && not (mem RetU ns) -> add RetU pc
      | Throw r when not track_excs -> throw r pc
      | s when track_excs -> ign                       (* probably throws exc. *)
      | ReturnU as s when ret_some ->
          Log.w logger "@[Unexpected@ statement@ `%a'@ preceding@ method@ sink.\
                        @]" pp_stm s; ign
      | _ -> ign) ns
    and throw r pc =
      let s' =
        if ret_some && ret_refp then ReturnR (R r)
        else if ret_some then ReturnP (Expr.Var r)
        else ReturnU
      in
      Log.w logger "@[Treating@ `%a'@ as@ `%a'.@]" pp_stm (Throw r) pp_stm s';
      register_sink pc s'
    in
    CFG.fold_sink_pred begin fun pc (ns, exc) ->
      let stm = delabel (Method.Body.stm_at_pc body pc) in
      let ns' = register_sink pc stm ns in
      ns', exc || NomSinks.equal PC.equal ns' ns
    end cfg (NomSinks.empty, false)
  let set_exc_sink = unione (sink_pred_var <~ enum (sink_pred_exc_label))
  let sink_pred_label k = sink_label_of (NomSinks.find k nom_sinks)
  let set_sink_pred k = unione (sink_pred_var <~ enum (sink_pred_label k))

  (* --- *)

  (* let register_custom_checkpoint l = register_custom_checkpoint (semloc l) *)
  let register_actual_branching l = register_actual_branching (semloc l)
  let max_br = CFG.fold_nom_branching (fun _ ~out_degree -> max out_degree) cfg 0

  let bin_branch, bin_oracle, decl_branch_vars =
    if max_br <= 2 then
      Bexp.var branch_var,
      Bexp.var oracle_var,
      begin fun k g -> g
        |> decl branch_var Bool k
        |> decl oracle_var Bool k
      end
    else
      !.(Eexp.var branch_var ==.@ enum (branch_mult_label 0)),
      !.(Eexp.var oracle_var ==.@ enum (branch_mult_label 0)),
      begin fun k g -> g
        |> fold_range 0 max_br
            (fun i -> decl_label branch_mult_typ (branch_mult_label i))
        |> decl branch_var (Enum branch_mult_typ) k
        |> decl oracle_var (Enum branch_mult_typ) k
      end

  let nth_branch, nth_oracle =
    if max_br <= 2 then
      (fun i -> if i > 0 then bin_branch else !.bin_branch),
      (fun i -> if i > 0 then bin_oracle else !.bin_oracle)
    else
      (fun i -> Eexp.var branch_var ==.@ enum (branch_mult_label i)),
      (fun i -> Eexp.var oracle_var ==.@ enum (branch_mult_label i))

  (* --- *)

  let chkpt v = Bexp.var (Guard v)

  (* --- *)

  let g0: group = Z
  let g1: group = S Z

  (* --- *)

  module Vm = VarMap

  let var_typs f r i = i
    |>~ f (Level r) SCFG.Level
    |>? f (Taint r) SCFG.Bool

  let ret c = Vm.add (Ret c)
  let exc c = Vm.add (Exc c)

  let add_res_typs s = s
    |> (if ret_some then var_typs ret "" else ign)
    |>! var_typs exc ""

  (* --- *)

  let support = Vs.empty
    |>~ fold_all_args (fun v -> Vs.add (Level v))
    |>? fold_all_args (fun v -> Vs.add (Taint v))
    |> Vs.union (Vs.diff (Vs.union (Vm.keys H.mutable_support)
                            (Vm.keys H.const_support))
                   H.local_support)

  let footprint = add_res_typs H.footprint
  let footprint' = Vm.keys footprint |> Vs.map (fun v -> Prime v)
  let decl_footprint =
    Vm.fold begin fun v t g ->
      ( g |> decl v t (Controllable (Min, g0, S Z))
          |> decl (Prime v) t State)
    end footprint

  let assign_footprint =
    Vm.fold begin fun v -> function
      | Bool -> setb (Prime v) (Bexp.var v)
      | Level -> setl (Prime v) (Lexp.var v)
      | Enum _ -> sete (Prime v) (Eexp.var v)
    end footprint

  (* --- *)

  let decl_non_footprint_state g = g
    |>~+ decl lpc_var Level State
    (* |>~+ ~@?(decl saved_pc_var Level State)           (\* <- stack(|Levels|-1) *\) *)
    |>~+ ~@?(decl ua_var Bool State)
    |> (if not (NomSinks.is_empty nom_sinks) || track_excs && sink_exc
        then decl sink_pred_var (Enum sink_pred_typ) State
        else ign)
    |>~ fold_all_vars (fun v -> decl (Level v) Level State)
    |>? fold_all_vars (fun v -> decl (Taint v) Bool State)
    |> Vm.fold (fun v t g ->
      ( g |> decl v t State                              (* stack(|Levels|-1) *)
          |> decl (Prime v) t State)) H.mutable_support
    |> Vm.fold (fun v t -> decl v t State) H.const_support

  (* --- *)

  let local_vars = local_vars                            (* SemLoc.local_vars *)
    (* |>~+ ~@?(Vs.add saved_pc_var) *)
    |> Vs.add branch_var
    |> fold_all_locals (var_typs (fun v _ -> Vs.add v))
    |> Vs.union H.local_support

  (* --- *)

  let init_bindings = let open SCFGEnv in let open VarMap in init_bindings
    |>~ fold_all_locals (fun v m -> add (Level v) (Lexp bottom) m)
    |>? fold_all_locals (fun v m -> add (Taint v) (Bexp ff) m)
    |>~+ ~@?(add ua_var (Bexp ff))
    (* |>~+ ~@?(add saved_pc_var (Lexp bottom)) *)
    |> (if ret_some || NomSinks.is_empty nom_sinks
        then ign
        else add sink_pred_var (Eexp (enum (sink_pred_label RetU))))
    |> H.acc_init_bindings
    |> ~@?(Vm.fold (fun v -> function
        | Bool -> add (Prime v) (Bexp ff)
        | Level -> add (Prime v) (Lexp bottom)
        | Enum _ -> ign) H.mutable_support)

  (* ;; Log.w logger "Init: %a" (VarMap.print Exp.print) init_bindings;; *)

  (* --- *)

  let arg_alias_invariant = H.arg_alias_invariant
  let arg_alias_invariant_support_size = H.arg_alias_invariant_support_size

  (* --- *)

  let (arg_related_footprint_constraints,
       acc_ret_related_footprint_constraints,
       acc_exc_related_footprint_constraints) =
    let comp v = Prime v in
    H.acc_arg_related_footprint_constraints ~comp tt,
    H.acc_ret_related_footprint_constraints ~comp,
    H.acc_exc_related_footprint_constraints ~comp

  (* --- *)

  let to_prime v = function
    | Bool -> setb (Prime v) (Bexp.var v)
    | Level -> setl (Prime v) (Lexp.var v)
    | Enum _ -> sete (Prime v) (Eexp.var v)

  let of_prime v = function
    | Bool -> setb v (Bexp.var (Prime v))
    | Level -> setl v (Lexp.var (Prime v))
    | Enum _ -> sete v (Eexp.var (Prime v))

  let save_heap = id
    |> ~@?(Vm.fold to_prime H.mutable_support)

  let swap_heap = id
    |> ~@?(Vm.fold to_prime H.mutable_support)
    |> ~@?(Vm.fold of_prime H.mutable_support)

  let restore_heap = id
    |> ~@?(let prime_if_mut v =
             if Vm.mem (Prime v) H.mutable_support then (Prime v) else v in
           H.bulk_upgrade ~from_comp:prime_if_mut ~to_comp:ign)

  (* --- *)

  let ua_nom = match Features.track_levels with
    | NoFlows | ExplicitFlows -> tt
    | AllFlows -> ~@?( (&&.) !.(BVar ua_var) ) tt

  let ( ~. ) e = lite (ua_nom, e, bottom) |.| lpc
  let ( <~~. ) v e = v <~~ (lite (ua_nom, e, ~~v) |.| lpc)

  let ( ?. ) e = bite (ua_nom, e, ff)
  let ( <?~. ) v e = v <?~ (bite (ua_nom, e, ?~v))

  (* --- *)

  let methcall_u ~ctxtrefs =
    let apply_hcomp = H.apply_comp_effect ~lcap:(lite (ua_nom, top, lpc)) in
    fun (v: Var.t) e -> match v, e with
      | Level _, Lexp _ when Features.track_levels = NoFlows -> ign
      | Taint _, Bexp _ when not Features.track_taints -> ign
      | Level "", Lexp l -> setl v l
      | Taint "", Bexp t -> setb v t
      | Level v, Lexp l -> v <~~. l
      | Taint v, Bexp t -> v <?~. t
      | v, e when H.bool_comp v || H.level_comp v -> apply_hcomp ~ctxtrefs v e
      | v, _ -> Log.w logger "@[Warning:@ ignoring@ %a@ in@ security@ summary.@]\
                            " V.print v; ign

  let subst_bound bsubst u =                  (* binds return/thrown variable *)
    let exception Discard in
    let bsubst v = match bsubst v with
      | None -> raise Discard
      | Some v -> v
    in
    let vsubst set v e = match V.subst bsubst v with
      | Some (Level _ | Taint _ as v) -> set v e
      | Some v when H.captured_comp v -> set v e
      | _ -> ign
      | exception Discard -> ign
    in
    (id |> U.foldb (vsubst setb) u
        |> U.foldl (vsubst setl) u
        |> U.folde (vsubst sete) u)

  let assign_res ?res u = match res with
    | None -> u
    | Some v ->
        let u = try repl (id |> (v <~~. findl (Level v) u)) u with
          | Not_found -> u in
        let u = try repl (id |> (v <?~. findb (Taint v) u)) u with
          | Not_found -> u in
        u

  let secsum_refargs ssum =
    Vars.(inter all_refs (of_list (Sign.Decl.args ssum.SecSum.sign)))

  let methcall_ret_u ?res ssum =
    let ctxtrefs = secsum_refargs ssum in
    (* Log.w logger "ctxtrefs = %a" Vars.print ctxtrefs; *)
    (id |> SecSum.fold_ret_bindings (methcall_u ~ctxtrefs) ssum
        |> subst_bound (function
            | "" -> res
            | x -> match res with Some r when r = x -> None | _ -> Some x)
        |> assign_res ?res)

  let methcall_exc_u ?res ssum =
    let ctxtrefs = secsum_refargs ssum in
    (* Log.w logger "ctxtrefs = %a" Vars.print ctxtrefs; *)
    (id |> SecSum.fold_exc_bindings (methcall_u ~ctxtrefs) ssum
        |> subst_bound (function "" -> Some excn | x -> Some x))

  (* --- *)

  let exc_handlers_from ?exctyps l g =
    let handlers, full = Sem.on_exc catch_table ?exctyps (pc l) in
    Log.d logger "Found %i handler(s) for %a (full = %B)\
                 " (List.length handlers) GenLoc.print l full;
    let last_resort, hl = match List.rev handlers with
      | (_, h) :: hl when full -> (jump h l, id), hl
      | hl -> (sink l, id |> set_exc_sink), hl
    in
    last_resort, hl, g

  (* --- *)

  let ndetbranch_start
      ~level ~u_benign ~u_upgrade ~u_cont ~l_upgrade
      ~tau ~oracle ?(u = id) dest =
    let nominal = if track_levels = AllFlows then level <=.~ lpc else tt in
    let upgrade = !.nominal in
    fun (ntau, noracle, tl) ->
      (ntau &&. !.tau, noracle &&. !.oracle,
       (     ua_nom &&. nominal &&. tau, U.compose u_benign  u, dest)
       :: (  ua_nom &&. upgrade &&. tau, U.compose u_upgrade u, l_upgrade dest)
       :: (!.ua_nom &&.         oracle, U.compose u_cont    u, dest)
       :: tl)

  let ndetbranch_cont
      ~tau ~oracle ?(u = id) dest =
    fun (ntau, noracle, tl) ->
      (ntau &&. !.tau, noracle &&. !.oracle,
       ( !.ua_nom &&.      oracle, u, dest)
       :: (ua_nom &&.         tau, u, dest)
       :: tl)

  (* --- *)

  open Stm

  type g = Gen.t

  let rec make_locs g =
    (gen_next [@tailcall]) ~js1 ~js2 g

  and trans l gul' g rem =
    acc_trans l gul' g rem |> fun (g, rem) -> make_locs g rem

  (* --- *)

  and otherwise branch ?u dest ((tau, oracle, _) as acc) =
    branch ~tau ~oracle ?u dest acc |> (fun (_, _, tl) -> tl)

  and cond_goto l e dest g =
    let base_tl = tt, tt, [] in
    let tau = !.bin_branch and oracle = !.bin_oracle in
    let g = g |>>~+ register_actual_branching l in
    match induced_region (pc l) cdrs with
      | Some r ->
          let level = cond_level e in
          let ndetbranch = ndetbranch_start ~level
            ~l_upgrade: (fun l -> l |>~+ enter_hcdr r)
            ~u_benign: id
            ~u_upgrade: (id |>~+ set_lpc (lpc |.| level)
                            |>~+ save_pc
                            |>~+ union save_heap)
            ~u_cont: id
          in
          trans l begin base_tl
              |> ndetbranch ~tau ~oracle (succ l)
              |> otherwise ndetbranch (jump dest l)
          end g
      | None when Stm.target dest = PC.succ (pc l) ->
          trans l [tt, id, succ l] g
      | None -> missing_cdr_infos l

  and switch_branches branch l { swch_cases; swch_default } acc =
    let default_pc = target swch_default in
    let branch_targets, _ = List.fold_left begin fun (map, i) (_, label) ->
      let pc = target label in
      if PC.equal pc default_pc || PCMap.mem pc map
      then map, i
      else PCMap.add pc (i, label) map, i + 1
    end (PCMap.empty, 1) swch_cases in
    PCMap.fold begin fun _ (i, target) acc ->
      let tau = nth_branch i and oracle = nth_oracle i in
      (acc |> branch ~tau ~oracle ?u:None (jump target l))
    end branch_targets acc |> otherwise branch (jump swch_default l)

  and switch l e sw g =
    let base_tl = tt, tt, [] in
    let g = g |>>~+ register_actual_branching l in
    match induced_region (pc l) cdrs with
      | Some r ->
          let level = expr_level e in
          let ndetbranch = ndetbranch_start ~level
            ~l_upgrade: (fun l -> l |>~+ enter_hcdr r)
            ~u_benign: id
            ~u_upgrade: (id |>~+ set_lpc (lpc |.| level)
                            |>~+ save_pc
                            |>~+ union save_heap)
            ~u_cont: id
          in
          trans l (switch_branches ndetbranch l sw base_tl) g
      | None ->
          let dt = Stm.target sw.swch_default in
          if List.for_all (fun (_, l) -> Stm.target l == dt) sw.swch_cases
          then trans l [tt, id, succ l] g
          else missing_cdr_infos l

  and trycatch_excs branch ?u_exc l hl (sink, u_sink) acc =
    let u_sink = match u_exc with | None -> u_sink | Some u -> union u u_sink in
    List.fold_left begin fun acc (i, h) ->
      let exh_hdl = SCFGEnv.Eexp.enum (exh_label_of i) in
      let tau = exh ==.@ exh_hdl and oracle = exh_oracle ==.@ exh_hdl in
      acc |>! branch ~tau ~oracle ?u:u_exc (jump h l)
    end acc hl |> otherwise branch ~u:u_sink sink

  (** No nominal transition if [u_nom=None] *)
  and handle_excs ?u_nom ?u_exc ~nominal ?exctyps ?(vl = bottom) l g =
    assert track_excs;
    let tau, oracle = match u_nom with
      | None -> ff, ff
      | Some _ -> let exh_nom = enum exh_label_none in
                 exh ==.@ exh_nom, exh_oracle ==.@ exh_nom
    and base_tl = tt, tt, [] in
    let sink, hl, g = exc_handlers_from ?exctyps l g in
    if is_second_stage l then begin
      (* Second-stage exception handling (e.g, post-method call, or actually
         executing a throw instruction) is assumed indistinguishable from
         first-stage at same instruction (e.g. null ref access), and thus induce
         the same CDR: we just need to branch one step further here, as we are
         alredy in the appropriate region. *)
      trans l begin base_tl
          |> ndetbranch_cont ~tau ~oracle ?u:u_nom (succ l)
          |> trycatch_excs ndetbranch_cont ?u_exc l hl sink
      end g
    end else begin
      let g = g |>>~+ register_actual_branching l in
      let level = vl |.| lpc in
      match induced_region (pc l) cdrs with
        | Some r ->
            let ndetbranch = ndetbranch_start ~level
              ~l_upgrade: (fun l -> l |>~+ enter_hcdr r)
              ~u_benign: id
              ~u_upgrade: (id |>~+ set_lpc level
                              |>~+ save_pc
                              |>~+ union save_heap)
              ~u_cont: id
            in
            trans l begin base_tl
                |> ndetbranch ~tau ~oracle ?u:u_nom (nominal l)
                |> trycatch_excs ndetbranch ?u_exc l hl sink
            end g
        | None when hl = [] -> (match u_exc, sink with
            | None, _ -> missing_cdr_infos l
            | Some u_exc, (sink, u) -> trans l [tt, union u_exc u, sink] g)
        | None -> missing_cdr_infos l
    end

  and missing_cdr_infos l =
    Log.e logger "CDRS: @[%a@]" (fun fmt -> CDRs.print fmt) cdrs;
    failwith (asprintf "Missing CDR info at PC = %i" (pc l))

  and checkpoint ~group l v u g =
    let invar =
      if implicit_flows && group <> g0 && sign <> None
      then is_lcdr l &&. (lpc <=.~ bottom)
      else tt
    in
    g |> decl (Guard v) Bool (Controllable (Max, group, Z))
      (* |> (if group = g0 then ign else register_custom_checkpoint l (Guard v)) *)
      |> restrict_to_location (chkpt v) l
      |>>~ invar_in_loc l invar
      |> trans l [ua_nom =>. chkpt v, u, succ l]

  and output l lvl (pl, rl) g =
    let invar = tt
      |>>~ (&&.) (forall pl (fun v -> (~~v <=.~ lvl)))
      |>>~ (&&.) (forall rl (fun r -> (~~r <=.~ lvl) &&. (~^r <=.~ lvl)))
      |>>~ (&&.) (lpc <=.~ lvl)
    in
    g |> invar_in_loc l invar
      |> trans l [tt, id, succ l]

  and true_sink l g =
    let c = "" in
    let invar = arg_related_footprint_constraints in
    let invar = NomSinks.fold begin fun s pc ->
      let vl, vt = match s with
        | RetU -> None, None
        | RetP e -> Some (expr_level e |.| lpc), Some (expr_taint e)
        | RetR (R r) -> Some (~~r |.| lpc), Some (?~r)
        | RetR (Rlit _) -> Some lpc, None
      in
      let r, rl, rt = match s with
        | RetU | RetP _ -> None, None, None
        | RetR (R r) -> Some r, Some (~^r), Some (?^r)
        | RetR (Rlit _) -> None, Some lpc, None
      in
      let sinvar = tt
        |>~ (&&.) (match vl with Some l -> (l <=.~ ~~?%c) | None -> tt)
        |>? (&&.) (match vt with Some t -> (t =>. ?~?%c) | None -> tt)
        |> acc_ret_related_footprint_constraints r rl rt
      in
      (&&.) ((EVar sink_pred_var ==.@ enum (sink_label_of pc)) =>. sinvar)
    end nom_sinks invar in
    let invar = if not track_excs || not sink_exc then invar else begin
      let r = excn in
      let einvar = tt
        |>!~ (&&.) (lpc |.| ~~r <=.~ ~~!%c)
        |>!? (&&.) (?~r =>. ?~!%c)
        |>! acc_exc_related_footprint_constraints r (lpc |.| ~^r) (?^r)
      in
      invar &&. ((EVar sink_pred_var ==.@ enum sink_pred_exc_label) =>. einvar)
    end in
    g |> invar_in_loc l invar
      |> make_locs

  and return k l = trans l [tt, id |> set_sink_pred k, sink l]

  and excsink l = trans l [tt, id |> set_exc_sink, sink l]

  and throw l r =
    if track_excs then
      handle_excs
        ~u_exc:(id |>!~ (excn <~~. ~~r)
                   |>!? (excn <?~. ?~r)
                   |>! H.copyr excn r)
        ~nominal:(fun l ->
          failwith @@ asprintf "Nominal after location %a?" GenLoc.print l)
        ~exctyps:(Typ.UserTyps.singleton (Sem.excdom decls r))
        l
    else
      let pc = pc l in
      let k = NomSinks.(fold (fun k pc' k' -> if PC.equal pc pc' then k else k')
                          nom_sinks (choose nom_sinks |> fst)) in
      return k l

  and methcall ?res ec l g =
    let ssum = PCMap.find (pc l) pc2ssum in
    let throws = track_excs && SecSum.throws ssum in
    let guard = if enforce_privacy || enforce_integrity then
        SecSum.guard ssum else tt in
    let u_nom = if guard <> ff then methcall_ret_u ?res ssum else id in
    let u_exc = if throws && guard <> ff then methcall_exc_u ?res ssum else id in
    g |> invar_in_loc l guard
      |> if throws
        then handle_excs ~u_nom ~u_exc ~nominal:succ l
        else trans l [tt, u_nom, succ l]

  and js1 l =
    let fst = is_first_stage l in
    let rs = if fst then hcdr_juncs l else CDR.M.empty in
    if not implicit_flows || CDR.M.is_empty rs
    then instr l
    else junc l rs

  and js2 l =
    instr l

  and junc l rs =
    Log.d logger "@[Building@ junction@ stage@ %a@]" GenLoc.print l;
    let l' = js2_stage l in
    let u_analyze = (id |> setb ua_var tt
                        |> union swap_heap) in
    let analyze_trans, (analyzep, joinp_junc, joinp_sink) =
      CDR.M.fold begin fun r hcdr_cond (at, (ap, jp, sp)) ->
        let pc_r = pc_of_inducing r cdrs in
        let analyzep = ua_nom &&. hcdr_cond in
        ((analyzep, u_analyze, jump_to_pc pc_r l) :: at,
         if is_sink l
         then (ap ||. hcdr_cond, jp, sp ||. hcdr_cond)
         else (ap ||. hcdr_cond, jp ||. hcdr_cond, sp))
      end rs ([], (ff, ff, ff)) in
    let u_join = (id |> setb ua_var ff
                     |> union restore_heap) in
    trans l ((   !.analyzep, id, l')
             :: (!.ua_nom &&. joinp_junc, u_join |> restore_pc, pop_hcdr l')
             :: (!.ua_nom &&. joinp_sink, u_join, pop_hcdr l')
             :: analyze_trans)

  and instr l =
    Log.d logger "@[Building@ instruction@ stage@ %a@]" GenLoc.print l;
    if is_sink l then true_sink l else begin
      let stm = stm l and fst = is_first_stage l and pc = pc l in
      let exctyps, vl = Typ.UserTyps.empty, bottom in
      let exctyps, vl =
        if fst && CFG.is_branching_on_np cfg pc then
          Typ.UserTyps.add Sem.npexctyp exctyps,
          Stm.if_ref_access (fun _ r -> (|.|) ~~r) stm vl
        else exctyps, vl
      in
      let exctyps, vl =
        if fst && CFG.is_branching_on_bad_cast cfg pc then
          Typ.UserTyps.add Sem.ccexctyp exctyps,
          Stm.if_unsafe_cast (fun _ r -> (|.|) (~~r |.| ~^r)) stm vl
        else exctyps, vl
      in
      let exctyps, vl =
        if fst && CFG.is_branching_on_aindx_error cfg pc then
          Typ.UserTyps.add Sem.aindxtyp exctyps,
          (Stm.array_access_idx
             (fun e -> Vars.fold (fun v -> (|.|) ~~v) (Expr.vars e)) stm vl
              |> Stm.if_ref_access (fun _ r -> (|.|) (~~r |.| ~^r)) stm)
        else exctyps, vl
      in
      (* Negative-array-size if handled in ANew case below since it's the only
         statement that may raise it. *)
      if not (Typ.UserTyps.is_empty exctyps)
      then handle_excs ~u_nom:id ~nominal:second_stage ~exctyps ~vl l
      else instr' l stm
    end

  and instr' l =
    let cont u = trans l [tt, u, succ l] in
    let open Stm in
    function
      | Nop ->
          cont id
      | AssignP (v, e) ->
          cont (id |>~ (v <~~. expr_level e)
                   |>? (v <?~. expr_taint e))
      | CastP (v, _, w) ->
          cont (id |>~ (v <~~. ~~w)
                   |>? (v <?~. ?~w))
      | LoadP (v, r, f) ->
          cont (id |>~ (v <~~. (~~r |.| ~^r))
                   |>? (v <?~. (?~r ||. ?^r)))
      | SLoadP (v, _, f) ->                                    (* TODO: static *)
          cont (id |>~ (v <~~. if evil_static_fields then top else bottom)
                   |>? (v <?~. if evil_static_fields then tt else ff))
      | ALoadP (v, a, e) ->
          cont (id |>~ (v <~~. (~~a |.| ~^a |.| expr_level e))
                   |>? (v <?~. (?~a ||. ?^a ||. expr_taint e)))
      | StoreP (r, f, e) ->
          cont (id |> H.storep r f ~.(expr_level e) ?.(expr_taint e))
      | SStoreP (_, f, e) when sink_static_fields ->           (* TODO: static *)
          (fun g -> g
              |>~ invar_in_loc l (~.(expr_level e) <=.~ bottom)
              |>? invar_in_loc l (?.(expr_taint e) =>. ff)
              |> cont id)
      | SStoreP (_, f, e) ->                                   (* TODO: static *)
          cont id
      | AStoreP (a, e, e') ->
          cont (id |> (H.astorep a
                         ~.(expr_level e |.| expr_level e')
                         ?.(expr_taint e ||. expr_taint e')))
      | AssignR (r, R s) | CastR (r, _, s) | CastA (r, _, s) ->
          cont (id |>~ (r <~~. ~~s)
                   |>? (r <?~. ?~s)
                   |> H.copyr r s)
      | LoadR (r, s, f) ->
          cont (id |>~ (r <~~. (~~s |.| ~^s))
                   |>? (r <?~. (?~s ||. ?^s))
                   |> H.loadr r s f)
      | SLoadR (r, c, f) ->                                    (* TODO: static *)
          cont (id |>~ (r <~~. if evil_static_fields then top else bottom)
                   |>? (r <?~. if evil_static_fields then tt else ff)
                   |> H.sloadr r c f)
      | ALoadR (r, a, e) ->
          cont (id |>~ (r <~~. (~~a |.| ~^a |.| expr_level e))
                   |>? (r <?~. (?~a ||. ?^a ||. expr_taint e))
                   |> H.aloadr r a)
      | StoreR (r, f, R s) ->
          cont (id |> H.storer r f ~s ~.(~~s |.| ~^s) ?.(?~s ||. ?^s))
      | StoreR (r, f, Rlit _) ->
          cont (id |> H.storer r f lpc ff)
      | SStoreR (_, f, R s) when sink_static_fields ->         (* TODO: static *)
          (fun g -> g
              |>~ invar_in_loc l (~.(~~s |.| ~^s) <=.~ bottom)
              |>? invar_in_loc l (?.(?~s ||. ?^s) =>. ff)
              |> cont id)
      | SStoreR (_, f, _) ->                                   (* TODO: static *)
          cont id
      | AStoreR (a, e, R r) ->
          cont (id |> (H.astorer a ~r
                         ~.(expr_level e |.| ~~r |.| ~^r)
                         ?.(expr_taint e ||. ?~r ||. ?^r)))
      | AStoreR (a, e, Rlit _) ->
          cont (id |> H.astorer a ~.(expr_level e) ?.(expr_taint e))
      | ALen (v, a) ->
          cont (id |>~ (v <~~. (~~a |.| ~^a))
                   |>? (v <?~. (?~a ||. ?^a)))
      | AssignR (r, Rlit Null) ->
          cont (id |>~ (r <~~. bottom)
                   |>? (r <?~. ff)
                   |> H.null r)
      | AssignR (r, Rlit _) ->
          cont (id |>~ (r <~~. bottom)
                   |>? (r <?~. ff)
                   |> H.newr r lpc ff)
      | New (r, c) ->
          cont (id |>~ (r <~~. bottom)
                   |>? (r <?~. ff)
                   |> H.newr r ~c lpc ff)
      | ANew (_, _, idxs)
            when is_first_stage l && CFG.is_branching_on_negsz_alloc cfg (pc l) ->
          handle_excs ~u_nom:id ~nominal:second_stage l
            ~exctyps:(Typ.UserTyps.singleton Sem.negsztyp)
            ~vl:(Vars.fold (fun v -> (|.|) ~~v) (vars_in_exprs idxs) bottom)
      | ANew (r, c, el) ->
          cont (id |>~ (r <~~. lpc)
                   |>? (r <?~. ff)
                   |> (H.newa r ~c
                         ~.(forall_u el (fun e -> (|.|) (expr_level e)) lpc)
                         ?.(forall_u el (fun e -> (||.) (expr_taint e)) ff)))
      (* |>~ (r <~^ forall_u el (fun e -> (|.|) (expr_level e)) lpc) *)
      (* |>? (r <?^ forall_u el (fun e -> (||.) (expr_taint e)) ff) *)
      | InstOfR (v, r, _) | InstOfA (v, r, _) ->
          cont (id |>~ (v <~~. (~~r |.| ~^r))
                   |>? (v <?~. (?~r ||. ?^r)))    (* Not sure it's appropriate *)
      | Goto dest ->
          trans l [tt, id, jump dest l]
      | CondGoto (e, dest) ->
          cond_goto l e dest
      | Switch (e, sw) ->
          switch l e sw
      | ChkPoint v ->
          checkpoint ~group:g1 l v id
      | OutputPR (lvl, il) ->
          output l (of_raw lvl) (Sem.nolits il)
      | UFCall ec ->
          methcall ec l
      | MFCallP (res, ec) | MFCallR (res, ec) ->
          methcall ~res ec l
      | ReturnU ->
          return RetU l
      | ReturnP e ->
          return (RetP e) l
      | ReturnR r ->
          return (RetR r) l
      | Throw r ->
          throw l r
      | Catch r ->
          cont (id |>!~ (r <~~. ~~excn)
                   |>!? (r <?~. ?~excn)
                   |>! H.copyr r excn)
      | MonEnter _ | MonExit _ ->
          cont id                                      (* TODO: warn for now? *)
      | Label (_, s) ->
          instr' l s

  (* should be called only once *)
  and methstart g = match sign, effects_inference with
    | None, _ ->
        make_locs g
    | Some sign, ControllablePreamble ->
        let g, l = preamble g in
        decl_footprint g |>
        checkpoint ~group:g0 l (SecSum.methname sign) (assign_footprint id)
    | Some sign, CoreachOnly ->
        decl_footprint g |>
        decl (Guard (SecSum.methname sign)) Bool (Controllable (Max, g0, Z)) |>
        make_locs

  (* --- *)

  (* let propagate_initialized_to_checkpoints g = *)
  (*   let GenData.{ custom_checkpoints } = data g in *)
  (*   let module InitializationFP = OOLang.CFG.Initialization (CFG) in *)
  (*   InitializationFP.fold begin fun pc inits (g, checkpoint_asserts) -> *)
  (*     try *)
  (*       let l, c = PCMap.find pc custom_checkpoints in *)
  (*       let uninit_refs = Vars.diff all_local_refs inits in *)
  (*       let uninit_prms = Vars.diff all_local_prms inits in *)
  (*       Log.d2 logger "uninits@%a = %a" SemLoc.print l *)
  (*         Vars.print (Vars.union uninit_refs uninit_prms); *)
  (*       let checkpoint_assert = begin tt *)
  (*           |>~ Vars.fold (fun r -> (&&.) (~~r |.| ~^r ==.~ bottom)) uninit_refs *)
  (*           |>~ Vars.fold (fun v -> (&&.) (~~v ==.~ bottom)) uninit_prms *)
  (*           |>? Vars.fold (fun r -> (&&.) (?~r ||. ?^r ==. ff)) uninit_refs *)
  (*           |>? Vars.fold (fun v -> (&&.) (?~v ==. ff)) uninit_prms *)
  (*       end in *)
  (*       assert_in_loc (of_semloc l) checkpoint_assert g, *)
  (*       Env.VarMap.add c checkpoint_assert checkpoint_asserts *)
  (*     with *)
  (*       | Not_found -> g, checkpoint_asserts *)
  (*   end cfg_fp_setup (g, Env.VarMap.empty) *)

  (* --- *)

  let rec decl_exh_labels = function
    | i when i < 0 -> decl_label exh_typ exh_label_none
    | i -> fun g -> decl_label exh_typ (exh_label_of i) g |> decl_exh_labels (i-1)

  let scfg, (* checkpoint_asserts,  *)actual_branching =
    let g, rem = SCFGGen.init GenData.init in
    let g = begin
      g |> NomSinks.fold begin fun s pc g ->
          g |> decl_label sink_pred_typ (sink_label_of pc)
        end nom_sinks
        |>!(if sink_exc then
            decl_label sink_pred_typ sink_pred_exc_label else ign)
        |> decl_non_footprint_state
        |> decl_policies
        |> set_initial_bindings init_bindings
        |> decl_branch_vars Input
        |>! decl exh_var (Enum exh_typ) Input
        |>! decl exh_oracle_var (Enum exh_typ) Input
        (* |> assert_global arg_alias_invariant *)
        (* |> assert_global (heap_invariant tt) *)
        (* |> invar_global (heap_invariant tt) *)
    end in
    let g = methstart g rem in
    let GenData.{ actual_branching } = data g in
    let g =
      if track_excs
      then decl_exh_labels (List.length catch_table) g
      else g
    in
    (* let g, checkpoint_asserts = propagate_initialized_to_checkpoints g in *)
    Gen.extract g(* , checkpoint_asserts *), actual_branching

  (* --- *)

  let variable_affinities =
    let acc_affinities lst = List.cons (Vs.of_list lst) in
    let primed_of v l = acc_affinities [ v; Prime v ] l
    and primed_ret_of v = acc_affinities [ v; Ret v; Prime (Ret v) ]
    and primed_exc_of v = acc_affinities [ v; Exc v; Prime (Exc v) ]
    and primed_ret_exc_of v = acc_affinities [
      v; Ret v; Prime (Ret v); Exc v; Prime (Exc v) ] in
    let hmutsupp =     (* Too much affinities seem to actually degrade perfs… *)
      (* if Vm.cardinal H.mutable_support <= 32    (\* XXX: <- arbitrary threshold *\) *)
      (* then Vm.keys H.mutable_support *)
      (* else *) Vs.empty
    in
    ([] |> Vs.fold primed_of hmutsupp
        |>~ (if not ret_some then ign else primed_of (Ret (Level "")))
        |>? (if not ret_some then ign else primed_of (Ret (Taint "")))
        |>!~ primed_of (Exc (Level ""))
        |>!? primed_of (Exc (Taint ""))
        |> H.fold_footprint_comps begin function
            | Ret _ | Exc _ as v -> primed_of v
            | v -> let has_ret = Vm.mem (Ret v) H.footprint in
                  let has_exc = Vm.mem (Exc v) H.footprint in
                  if has_ret && has_exc then primed_ret_exc_of v
                  else if has_ret then primed_ret_of v
                  else if has_exc then primed_exc_of v
                  else ign
        end)
  ;;
  Log.d3 logger "Variable affinities: %a\
                " (pp_lst Vs.print) variable_affinities;;

  (* --- *)

  let output_cdrs_dot fmt =
    CDRs.output_dot fmt ~cdr:CDRs.cdrs
      ~cdrx:(CDRs.extend ~fp_setup:CDRs.cfg_fp_setup cfg CDRs.cdrs) cfg

  (* --- *)

  let heap_stats = H.stats

end

(* --- *)
