open Format
open Rutils
open Utils
open OOLang.Base
open OOLang.Typ

type dom = Level | Taint

let pp_dom: dom pp = fun fmt -> function
  | Level -> pp_print_string fmt "level"
  | Taint -> pp_print_string fmt "taint"

type policy =
    {
      clazz: user typname;
      name: var option;
      dom: dom;
      args: argspec list;
    }
and argspec =
  | Formal of (* any typname *  *)var
  | Lit of lit
  | Elim of int

let make clazz name dom args = { clazz; name; dom; args }
let dom { dom } = dom

module T = struct
  type t = policy

  let compare_argspec a b = match a, b with
    (* | Formal (a1, a2), Formal (b1, b2) *)
    (*   -> cmp_2 AnyTyp.compare Var.compare (a1, a2) (b1, b2) *)
    | Formal a, Formal b -> Var.compare a b
    | Lit a, Lit b -> Stdlib.compare a b
    | Elim i, Elim j -> Stdlib.compare i j
    | Formal _, _ -> -1 | _, Formal _ -> 1
    | Lit _, _ -> -1 | _, Lit _ -> 1

  let pp_argspec fmt = function
    (* | Formal (t, v) -> pp_pair ~format:"@[%a@ %a@]" AnyTyp.print Var.print fmt (t, v) *)
    | Formal v -> Var.print fmt v
    | Lit l -> print_lit fmt l
    | Elim i -> pp fmt "'%i" i

  let elimvars args = args
    |> List.filter (function Elim _ -> true | _ -> false)
    |> List.sort_uniq compare_argspec

  let pp_elimvars fmt args =
    pp_lst ~fempty:"" ~fopen:"@[" ~fclose:" ->@]@ " ~fsep:",@ "
      (fun fmt -> function Elim i -> pp fmt "'%i" i | _ -> ())
      fmt (elimvars args)

  let print fmt { clazz; name; dom; args } =
    pp_elimvars fmt args;
    pp fmt "%a%a@ %a@ -> %a"
      UserTyp.print clazz (pp_opt ":%a" Var.print) name
      (pp_lst ~fempty:"()" ~fopen:"(@[" ~fclose:")@]" ~fsep:",@ " pp_argspec) args
      pp_dom dom

  let compare
      { clazz = c1; name = n1; dom = d1; args = a1 }
      { clazz = c2; name = n2; dom = d2; args = a2 }
      =
    cmp_2
      (cmp_2 UserTyp.compare (cmp_opt Var.compare))
      (cmp_2 Stdlib.compare (cmp_lst compare_argspec))
      ((c1, n1), (d1, a1)) ((c2, n2), (d2, a2))

  let equal a b = a == b || compare a b = 0

  let hash = Hashtbl.hash

  open OOLang.Stm
  open OOLang.Stm.Ops
  let subst m ({ args } as p) =
    { p with
      args = List.map (function
        | Lit _ | Elim _ as l -> l
        | Formal v as x -> match !^(VarMap.find v m) with
            | OOLang.Stm.V v -> Formal v
            | OOLang.Stm.L l -> Lit l
            | exception Not_found -> x)
        args }

  let elim vars ({ args } as p) =
    let prev_elim_count =
      List.fold_left (fun i -> function Elim _ -> succ i | _ -> i) 0 args in
    { p with
      args = begin List.fold_left (fun (evars, acc, i) a -> match a with
        | Lit _ | Elim _ -> evars, a :: acc, i
        | Formal v -> try evars, Elim (VarMap.find v evars) :: acc, i with
            | Not_found when Vars.mem v vars ->
                VarMap.add v i evars, Elim i :: acc, succ i
            | Not_found -> evars, a :: acc, i)
          (VarMap.empty, [], prev_elim_count)
          args
      end |> fun (_, acc, _) -> List.rev acc; }

end

module Set = MakeSet (T)
module Map = MakeMap (T)

include T
