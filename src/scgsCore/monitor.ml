open Format
open Rutils
open Utils
open OOLang
open Sign

(* --- *)

let level = Log.Debug
let logger = Log.mk ~level "Mon"

type typ = SCFG.typ
type 'x decl = Typ of typ | Expr of 'x

(* --- *)

module type COMMON = sig
  module Env: SCFG.ENV
  open Env
  type params = typ VarMap.t
  type decls = exp decl VarMap.t
  type updates =
    | U of bindings
    | L of bindings * updates
    | C of bexp * updates * updates
end

(* --- *)

module Common (Env: SCFG.ENV) = struct
  include HashCons.Exprs (Env)
  module Env = Env
  open Env
  open SCFG

  type params = typ VarMap.t
  type decls = exp decl VarMap.t

  let pp_exp' fmt e =
    pp fmt "%a = %a"
      pp_typ
      (match e with | Bexp _ -> Bool | Eexp _ -> Enum "Location" | Lexp _ -> Level)
      pp_exp e

  let pp_dcl fmt = function
    | Typ t -> pp_typ fmt t
    | Expr e -> pp_exp' fmt e

  let print_locs =
    Strings.print' ~empty:"" ~left:"!locations@\n  @[<v>" ~right:";@]"

  let print_vars ~left =
    VarMap.print' ~empty:"" ~left ~right:";@]" ~sep:";@\n" ~assoc:": " pp_typ

  let print_decls ~left =
    VarMap.print' ~empty:"" ~left ~right:";@]" ~sep:";@\n" ~assoc:": " pp_dcl

  (* --- *)

  type policies = Policy.t VarMap.t

  let print_policies ~left =
    VarMap.print' ~empty:"" ~left ~right:";@]" ~sep:";@\n" ~assoc:": "
      Policy.print

  (* --- *)

  let print_bindings: bindings pp =
    VarMap.print' ~empty:"" ~left:"@\n" ~right:";" ~sep:";@\n" ~assoc:" := " pp_exp

  let print_locals: bindings pp =
    VarMap.print' ~empty:"" ~left:"@\n!let@\n  @[" ~right:";@]@\n!in" ~sep:";@\n"
      ~assoc:": " pp_exp'

  (* --- *)

  type guard =
    | G of bexp
    | G' of bindings * guard

  let rec print_guard: guard pp = fun fmt -> function
    | G g -> pp fmt " %a" pp_bexp g
    | G' (l, g) -> pp fmt "%a%a" print_locals l print_guard g

  let rec hashcons_guard ?par : guard -> guard = function
    | G g ->
        let m = new_memo par in
        let g = bexp m g in
        let l = pop_bindings m VarMap.empty in
        if VarMap.is_empty l then G g else G' (l, G g)
    | G' (l, g) ->
        let g = hashcons_guard ?par g in
        G' (l, g)

  (* --- *)

  type guards = guard VarMap.t

  let print_guards ?(pref: PPrt.ufmt = "@\n") =
    VarMap.print' ~empty:"" ~left:(pref^^"!guards@\n  @[") ~sep:"@\n" ~right:";@]"
      ~assoc:" =" print_guard

  let make_guards ~hashcons : bexp VarMap.t -> guards =
    VarMap.map (fun g -> if hashcons then hashcons_guard (G g) else (G g))

  (* --- *)

  type updates =
    | U of bindings
    | L of bindings * updates
    | C of bexp * updates * updates

  let rec print_updates = fun fmt -> function
    | U u -> print_bindings fmt u
    | L (l, u) -> print_locals fmt l; print_updates fmt u
    | C (c, u, v) -> pp fmt "@\n!i@[f %a%a@]@\n!e@[lse%a@]\
                           " Bexp.print c print_updates u print_updates v

  let rec hashcons_updates ?par : updates -> updates = function
    | U u ->
        let m = new_memo par in
        let u = VarMap.map (exp m) u in
        let l = pop_bindings m VarMap.empty in
        if VarMap.is_empty l then U u else L (l, U u)
    | L (l, u) ->
        let u = hashcons_updates ?par u in
        L (l, u)
    | C (c, u, v) ->
        let m = new_memo par in
        let c = bexp m c in
        let u = hashcons_updates ~par:m u in
        let v = hashcons_updates ~par:m v in
        let l = pop_bindings m VarMap.empty in
        if VarMap.is_empty l then C (c, u, v) else L (l, (C (c, u, v)))

end

(* --- *)

module type GUARDS = sig
  module Env: SCFG.ENV
  type t
  val make: ?hashcons: bool -> Policy.t Env.VarMap.t -> Env.bexp Env.VarMap.t -> t
  val print: t pp
end

module Guards (Env: SCFG.ENV) : (GUARDS with module Env = Env) = struct
  include Common (Env)
  open Env

  type t =
      {
        policies: policies;
        guards: guards;
      }

  let make ?(hashcons = false) : policies -> bexp VarMap.t -> t = fun policies m ->
    { policies; guards = make_guards ~hashcons m }

  let print fmt { policies; guards } =
    print_policies ~left:"!policies@\n  @[" fmt policies;
    print_guards ~pref:(if VarMap.is_empty policies then "" else "@\n")
      fmt guards;
    pp_print_newline fmt ()

end

(* --- *)

module type NODE = sig
  include COMMON
  type t
  val make: ?hashcons: bool -> Strings.t -> params -> Policy.t Env.VarMap.t
    -> decls -> Env.bindings -> t
  val print: t pp
end

module Node (Env: SCFG.ENV) : (NODE with module Env = Env) = struct
  open Env
  include Common (Env)

  type t =
      {
        locs: locs;
        params: typ VarMap.t;
        policies: policies;
        init: exp decl VarMap.t;
        locals: exp VarMap.t;
        trans: exp VarMap.t;
      }
  and locs = Strings.t

  let make' locs params policies init locals trans : t =
    { locs; params; policies; init; locals; trans }

  let make ?(hashcons = false) locs params policies init trans : t =
    let trans, locals =
      if not hashcons then trans, VarMap.empty else
        let m = new_memo None in
        let trans = VarMap.map (exp m) trans in
        let locals = pop_bindings m VarMap.empty in
        trans, locals
    in
    make' locs params policies init locals trans

  let print_params = print_vars ~left:"@\n!params@\n  @["
  let print_policies = print_policies ~left:"@\n!policies@\n  @["
  let print_init = print_decls ~left:"@\n!init@\n  @["

  let print fmt { locs; params; policies; init; locals; trans } =
    print_locs fmt locs;
    print_params fmt params;
    print_policies fmt policies;
    print_init fmt init;
    pp fmt "@\n!t@[ransition%a%a@]@.\
           " print_locals locals print_bindings trans

end

(* --- *)

module type AUTOMATON = sig
  include COMMON
  type t
  val make: ?hashcons: bool -> params -> Policy.t Env.VarMap.t
    -> decls -> updates StrMap.t -> t
  val print: t pp
end

module Automaton (Env: SCFG.ENV) : (AUTOMATON with module Env = Env) = struct
  open Env
  include Common (Env)

  type t =
      {
        locs: locs;
        params: typ VarMap.t;
        policies: policies;
        init: exp decl VarMap.t;
        steps: step StrMap.t;
      }
  and locs = Strings.t
  and step = updates

  let make' locs params policies init steps : t =
    { locs; params; policies; init; steps }

  let make ?(hashcons = false) params policies init steps : t =
    let steps = if hashcons then StrMap.map hashcons_updates steps else steps in
    let locs = StrMap.fold (fun l _ -> Strings.add l) steps Strings.empty in
    make' locs params policies init steps

  let print_params = print_vars ~left:"@\n!params@\n  @["
  let print_policies = print_policies ~left:"@\n!policies@\n  @["
  let print_init = print_decls ~left:"@\n!init@\n  @["
  let print_trans =
    StrMap.print' ~empty:"" ~left:"@\n!automaton@\n  @[" ~sep:"@\n" ~right:"@]"
      ~aleft:"!l@[ocation " ~assoc:"" ~aright:"@]" print_updates

  let print fmt { locs; params; policies; init; steps } =
    print_locs fmt locs;
    print_params fmt params;
    print_policies fmt policies;
    print_init fmt init;
    print_trans fmt steps;
    pp_print_newline fmt ()

end
