open Format
open Timing
open Rutils.PPrt
open SCFG.Stats
open BasicStats.Outputs

(* -------------------------------------------------------------------------- *)

type time = Timing.time                                              (* alias *)

(* -------------------------------------------------------------------------- *)

(** Heap abstraction *)

module SHStats = struct
  open SymbHeap.Stats
  type 'n t = 'n cardinals
  let print = print_cardinals
  let map = map_cardinals
  let acc = acc_cardinals
end

(* -------------------------------------------------------------------------- *)

(** SCFG generation *)
module SCFGGen = struct

  type ('t, 'n) t =
      {
        time: 't;
        cards: 'n cardinals;
        degrees: 'n degrees;
        decls: 'n var_decls;
      }

  let print ?(pk = ps) ?k0 ptime pn fmt { time; cards; decls; degrees } =
    pp fmt "@[<2>%a: %a,@]@;%a,@;%a,@;%a"
      (pk ?k0) "scfg_generation_time" ptime time
      (print_cardinals ~pk ?k0 pn) cards
      (print_var_decls ~pk ?k0 pn) decls
      (print_degrees ~pk ?k0 pn) degrees

  let print_scfg_infos fmt
      { cards = { nb_locations; nb_transitions; nb_vars };
        decls = { nb_state_vars; nb_input_vars; nb_contr_vars }; } =
    pp fmt "%i@ locations,@ %i@ transitions,@ @[<hov>and@ %i@ variables@ \
            (state/input/controllable: %i/%i/%i)@]"
      nb_locations nb_transitions nb_vars
      nb_state_vars nb_input_vars nb_contr_vars

  let print_gen_time fmt { time } = pp_secs fmt time

  let map ft fn { time; cards; degrees; decls } =
    { time =                ft time;
      cards = map_cardinals fn cards;
      degrees = map_degrees fn degrees;
      decls = map_var_decls fn decls; }

  let acc ft fn a b =
    { time =                ft a.time    b.time;
      cards = acc_cardinals fn a.cards   b.cards;
      degrees = acc_degrees fn a.degrees b.degrees;
      decls = acc_var_decls fn a.decls   b.decls; }

end

(* -------------------------------------------------------------------------- *)

(** Realib model construction *)
module Constr = struct

  type ('t, 'n) t =
      {
        time: 't;
        suppsize: 'n;
        footsize: 'n;
      }

  let print ?(pk = ps) ?k0 ptime pn fmt { time; suppsize; footsize } =
    let pk = pk ?k0 in
    pp fmt "@[<2>%a: %a@],@;\
            @[<2>%a: %a@],@;\
            @[<2>%a: %a@]"
      pk "model_instantiation_time" ptime time
      pk "support_size" pn suppsize
      pk "footprint_size" pn footsize

  let print_full_time fmt { time } = pp_secs fmt time

end

(* -------------------------------------------------------------------------- *)

(** Synthesis / Summary computation *)
module Synth = struct

  type ('t, 'b) t =
      {
        synth_time: 't;
        triang_time: 't;
        unsat_guard: 'b;
        true_guard: 'b;
        (* unsat_chkpts: int; *)
      }

  let print ?(pk = ps) ?k0 ptime pb fmt { synth_time; triang_time;
                                          unsat_guard; true_guard } =
    let pk = pk ?k0 in
    pp fmt "@[<2>%a: %a@],@;\
            @[<2>%a: %a@],@;\
            @[<2>%a: %a@],@;\
            @[<2>%a: %a@]"
      pk "synthesis_time" ptime synth_time
      pk "triangularization_time" ptime triang_time
      pk "unsat_guard" pb unsat_guard
      pk "true_guard" pb true_guard

  let print_full_time fmt { synth_time; triang_time } =
    pp_secs fmt (synth_time +. triang_time)

end

(* -------------------------------------------------------------------------- *)
(* Full method processing *)

type ('t, 'n, 'b, 'r) t =
    {
      symb_heap_stats: 'n SHStats.t;
      meth_scfg_gen: ('t, 'n) SCFGGen.t;
      meth_handling: ('t, 'n, 'b, 'r) meth_handling;
    }
and ('t, 'n, 'b, 'r) meth_handling =
  | Skipped of 'r
  (* | Constructed of { model: Constr.t } *)
  | Done of { model: ('t, 'n) Constr.t; synth: ('t, 'b) Synth.t; }

let print ?(pk = ps) ?k0 ptime pn pb pr fmt
    { symb_heap_stats; meth_scfg_gen; meth_handling } =
  pp fmt "%a,@;" (SHStats.print ~pk ?k0 pn) symb_heap_stats;
  pp fmt "%a,@;" (SCFGGen.print ~pk ?k0 ptime pn) meth_scfg_gen;
  begin match meth_handling with
    | Skipped cause
      -> pk ?k0 fmt "summarization"; pp fmt ": skipped,@;";
        pk ?k0 fmt "summarization_skipped_cause"; pp fmt ": %a" pr cause
    | Done { model; synth }
      -> pk ?k0 fmt "summarization"; pp fmt ": done,@;";
        pp fmt "%a,@;%a"
          (Constr.print ~pk ?k0 ptime pn) model
          (Synth.print ~pk ?k0 ptime pb) synth
  end(* ; *)
  (* pp fmt "@]" *)

(* -------------------------------------------------------------------------- *)
