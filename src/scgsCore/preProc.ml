(** {1 Basic pre-processing modules for symbolic control flow graphs} *)

open Format
open Rutils
open Utils

let level = Log.Debug3
let logger = Log.mk ~level "RBPP";;

(* -------------------------------------------------------------------------- *)

module type BINDINGS = sig
  module S: SCFG.T
  type e =
    | T : e
    | E : S.Env.exp -> e
  type t = e S.Env.VarMap.t
  type data = t
  val empty: t
  val print: t pp
  val sub: t -> S.Env.subst
  val subst_in_updates: S.Env.updates -> t -> S.Env.updates
  val subst_in_pred: S.Env.bexp -> t -> S.Env.bexp
  include S.FPDATA with type data := data
  include S.FPCDATA with type t := t
end

module MakeBindings (S: SCFG.T) : BINDINGS with module S := S = struct

  open S
  open Env
  open VarMap
  open Updates

  type e =
    | T : e
    | E : exp -> e
  type t = e VarMap.t
  type data = t

  let direction = Graph.Fixpoint.Forward

  let print = VarMap.print'
    ~skip_key:(fun _ -> function T -> true | _ -> false)
    ~skip_val:(fun _ -> function T -> true | _ -> false)
    (fun fmt -> function
      | T -> ()
      | E e -> Exp.print fmt e)

  let depends_on = function
    | T -> fun _ -> false
    | E e -> fun s ->
      try Exp.fold_supp (fun v () -> if VarSet.mem v s then raise Exit; ()) e ();
          false
      with Exit -> true

  let empty = empty

  let equal = equal (fun a b -> match a, b with
    | T, T -> true
    | E a, E b -> Exp.equal a b
    | _ -> false)

  let join: t -> t -> t = merge (fun _ a b -> match a, b with
    | Some (E a), None | None, Some (E a) -> Some (E a)
    | Some (E a), Some (E b) when Exp.compare a b = 0 -> Some (E a)
    | Some _, _ | _, Some _ -> Some T
    | None, None -> None)

  let widening: t -> t -> t = fun a b ->
    let r = merge (fun _ a b -> match a, b with
      | None, Some x -> Some x
      | Some (E a), Some (E b) when Exp.compare a b = 0 -> Some (E a)
      | Some _, Some _ -> Some T
      | Some _, None -> None
      | None, None -> None) a b in
    Log.d2 logger "Widenin@[g: @[%a@\n%a@]@\n-> %a@]" print a print b print r;
    r

  let sub map v =
    try match VarMap.find v map with
      | T -> `None
      | E (Bexp e) -> `Bexp e
      | E (Lexp e) -> `Expr (`Lexp e)
      | E (Eexp e) -> `Expr (`Eexp e)
    with
      | Not_found -> `None

  let analyze ((src, (g, u), dest): loc * trans * loc) s =
    let sub = sub s in
    let s' = begin
      s |> foldb Bexp.(fun v e -> add v (E (Bexp (subst sub e)))) u
        |> foldl Lexp.(fun v e -> add v (E (Lexp (subst sub e)))) u
        |> folde Eexp.(fun v e -> add v (E (Eexp (subst sub e)))) u
    end in
    let modified = VarMap.fold (fun v _ -> VarSet.add v) s' VarSet.empty in
    VarMap.map (fun e -> if depends_on e modified then T else e) s'

  let subst_in_updates u s =
    let sub = sub s in
    u |> mapb (Bexp.subst sub)
      |> mapl (Lexp.subst sub)
      |> mape (Eexp.subst sub)

  let subst_in_pred b s = Bexp.subst (sub s) b

end

(* --- *)

module MakeConstBindings (S: SCFG.T) : BINDINGS with module S := S = struct
  open S
  open Env
  open VarMap
  open Updates

  include MakeBindings (S)

  let analyze ((src, (g, u), dest): loc * trans * loc) s =
    let sub = sub s in
    let subb v e = match Bexp.subst sub e with
      | Bexp.BCnj [] | Bexp.BDsj [] as e -> add v (E (Bexp e)) | _ -> add v T
    and subl v e = match Lexp.subst sub e with
      | Lexp.LBot | Lexp.LTop as e -> add v (E (Lexp e)) | _ -> add v T
    and sube v e = match Eexp.subst sub e with
      | Eexp.ECst _ as e -> add v (E (Eexp e)) | _ -> add v T
    in
    s |> foldb subb u
      |> foldl subl u
      |> folde sube u
end

(* -------------------------------------------------------------------------- *)

module CommonBindings
  (MakeBindings: functor (S: SCFG.T) -> BINDINGS with module S := S)
  (S: SCFG.T)
  =
struct
  open S

  module Bindings = MakeBindings (S)
  module SCFGFp = FixpointChaotic (S)

  let init scfg l =
    if Location.equal (init_loc scfg) l
    then Env.VarMap.map (fun e -> Bindings.E e) (initial_bindings scfg)
    else Bindings.empty
end

(* -------------------------------------------------------------------------- *)

module ConstProp (S: SCFG.T) = struct
  include CommonBindings (MakeConstBindings) (S)
  include SCFGFp.Make (Bindings)

  open S.Env
  open Bindings

  let apply_on scfg =
    let m = apply (init scfg) (SCFGFp.setup scfg) in
    foldres begin fun loc consts g ->
      let subst_in_pred p = subst_in_pred p consts in
      Log.d2 logger "@[consts(%a) = %a@]\
                    " S.Location.print loc print consts;
      let g = S.mapfilter_trans_from begin fun (_src, (g, u), _dest) ->
        let g = subst_in_pred g in
        if g = Bexp.ff then None else Some (g, subst_in_updates u consts)
      end loc g in
      if S.mem_location g loc then begin g
          |> S.refine_invar_of_loc subst_in_pred loc
          |> S.refine_assert_of_loc subst_in_pred loc
      end else g
    end m scfg
end

(* --- *)

module FullProp (S: SCFG.T) = struct
  include CommonBindings (MakeBindings) (S)
  include SCFGFp.Make (Bindings)

  open S.Env
  open Bindings

  let apply_on scfg =
    let m = apply (init scfg) (SCFGFp.setup scfg) in
    foldres begin fun loc exprs g ->
      let subst_in_pred p = subst_in_pred p exprs in
      Log.d2 logger "@[exprs(%a) = %a@]\
                    " S.Location.print loc print exprs;
      let g = S.mapfilter_trans_from begin fun (_src, (g, u), _dest) ->
        let g = subst_in_pred g in
        if g = Bexp.ff then None else Some (g, subst_in_updates u exprs)
      end loc g in
      (* XXX: can't substitute in objective invariants as exprs might depend on
         inputs: *)
      if S.mem_location g loc then begin g
          (* |> S.refine_invar_of_loc (fun i -> subst_in_pred i exprs) loc *)
          |> S.refine_assert_of_loc subst_in_pred loc
      end else g
    end m scfg
end

(* -------------------------------------------------------------------------- *)

module MakeValue (S: SCFG.T) (Val: sig val v: S.Env.Var.t end) = struct

  open S
  open Env
  open VarMap
  open Updates

  type t =
    | T : t
    | B : t
    | E : exp -> t
  type data = t

  let direction = Graph.Fixpoint.Forward

  let print fmt = function
    | T -> pp fmt "@<1>⊤"
    | B -> pp fmt "@<1>⊥"
    | E e -> Exp.print fmt e

  (* let depends_on_v = function *)
  (*   | T -> false *)
  (*   | E e -> *)
  (*       try Exp.fold_supp (fun v () -> if v = Val.v then raise Exit; ()) e (); *)
  (*           false *)
  (*       with Exit -> true *)

  let equal a b = match a, b with
    | T, T | B, B -> true
    | E a, E b -> Exp.equal a b
    | _ -> false

  let join: t -> t -> t = fun a b -> match a, b with
    | B, B -> B
    | E a, E b when Exp.compare a b = 0 -> E a
    | _ -> T

  let widening = join

  let sub e v = if v <> Val.v then `None else match e with
    | Bexp e -> `Bexp e
    | Lexp e -> `Expr (`Lexp e)
    | Eexp e -> `Expr (`Eexp e)

  let subst_in_pred b e = Bexp.subst (sub e) b

  let subst_in_updates u e =
    let sub = sub e in
    (u  |> mapb (Bexp.subst sub)
        |> mapl (Lexp.subst sub)
        |> mape (Eexp.subst sub))

  let analyze ((src, (g, u), dest): loc * trans * loc) = function
    | B -> B
    | T -> (try E (find Val.v u) with Not_found -> T)
    | E e -> let g' = subst_in_pred g e in
            if g' = Bexp.ff then B else
              (try E (Exp.subst (sub e) (find Val.v u)) with Not_found -> E e)

end

(* -------------------------------------------------------------------------- *)

module CommonValue (S: SCFG.T) (Val: sig val v: S.Env.Var.t end) = struct
  open S

  module Value = MakeValue (S) (Val)
  module SCFGFp = Fixpoint (S)

  let init scfg l =
    if not (Location.equal (init_loc scfg) l) then Value.T else
      try Value.E (Env.VarMap.find Val.v (initial_bindings scfg)) with
        | Not_found -> Value.T
end

(* -------------------------------------------------------------------------- *)

module VarProp (S: SCFG.T) (Val: sig val v: S.Env.Var.t end) = struct
  include CommonValue (S) (Val)
  include SCFGFp.Make (Value)

  open S.Env
  open Value

  let apply_on scfg =
    let m = apply (init scfg) (SCFGFp.setup scfg) in
    foldres begin fun loc -> function
      | B -> ign
      | T -> ign
      | E value as e ->
          Log.d2 logger "@[val(%a)@@%a = %a@]\
                    " S.Env.Var.print Val.v S.Location.print loc print e;
          let subst_in_pred p = subst_in_pred p value in
          fun g ->
            let g = S.mapfilter_trans_from begin fun (_src, (g, u), _dest) ->
              let g = subst_in_pred g in
              if g = Bexp.ff then None else Some (g, subst_in_updates u value)
            end loc g in
            if S.mem_location g loc then begin g
                |> S.refine_invar_of_loc subst_in_pred loc
                |> S.refine_assert_of_loc subst_in_pred loc
            end else g
    end m scfg
end

(* -------------------------------------------------------------------------- *)
