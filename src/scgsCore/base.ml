module OOBase = OOLang.Base
module OOMeth = OOLang.Method
module OOSign = OOLang.Sign
module OOTyp = OOLang.Typ

module type VAR_TYPS = sig
  type lexp and texp
  val ( ~~ ): OOBase.var -> lexp
  val ( ?~ ): OOBase.var -> texp
end

module type OBJ_TYPS = sig
  type lexp and texp
  val ( ~^ ): OOBase.var -> lexp
  val ( ?^ ): OOBase.var -> texp
end

module type OPTIONS = sig
  include Features.T
  include Heuristics.OPTIONS
end
module DefaultOptions: OPTIONS = struct
  include Features.Default
  include Heuristics.DefaultOptions
end

(* --- *)

module SCFGEnv = SCFG.MakeEnv (Var)
module MakeHeuristics = Heuristics.Exprs (SCFGEnv)
