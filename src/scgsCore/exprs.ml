open Format
open Rutils
open Utils
open OOLang

(* --- *)

type ('a, 'b, 'x) subst_func = 'a -> [ `None | `Bexp of 'b | `Expr of 'x ]
type 'x ext_cond = Tt | Ff | X of 'x

let exfltr f mk orig x = match f x with
  | Tt -> Tt | Ff -> Ff
  | X x' when x' == x -> X orig
  | X x' -> X (mk x')

module type BEXP_EXTENSION = sig
  type 'b t
  type var
  val print: 'b PPrt.pp -> (bool * 'b t) PPrt.pp
  val compare: ('b -> 'b -> int) -> 'b t -> 'b t -> int
  val hash: ('b -> int) -> 'b t -> int
  val fold_supp: ('b -> 'a -> 'a) -> (var -> 'a -> 'a) -> 'b t -> 'a -> 'a
  val subst_vars: ('b -> 'b ext_cond) -> (var -> var option) -> 'b t -> 'b t ext_cond
  type 'b subexpr
  exception UnexpectedSubexpr of var
  val subst: ('b -> 'b ext_cond) -> (var -> 'b subexpr option) -> 'b t -> 'b t ext_cond
end

(* --- *)

module type EXTENDED_BEXP = sig
  type var
  type 'b ext
  type t =
    | BVar : var -> t
    | BExt : t ext -> t
    | BNot : t -> t
    | BCnj : t list -> t
    | BDsj : t list -> t
    | BIte : t * t * t -> t
    | BEq : t * t -> t
    | BNe : t * t -> t
  include ORDERED_HASHABLE_TYPE with type t := t
  val ext: t ext -> t
  val mk_ite
    : decompose:('a -> ((t * 'a * 'a) as 'x) option)
    -> compare:('a -> 'a -> int)
    -> recur:('x -> 'a)
    -> constr:('x -> 'a)
    -> 'x -> 'a
  val fold_supp: (var -> 'a -> 'a) -> t -> 'a -> 'a
  val subst_vars: (var -> var option) -> t -> t
  val subst_vars': (var -> var option) -> t -> t ext_cond
  type 'b subexpr
  type subst = (var, t, t subexpr) subst_func
  exception UnexpectedBexpr of var * t
  exception UnexpectedExpr of var * t subexpr
  val subst: subst -> t -> t
  val subst': subst -> t -> t ext_cond
  module Constrs: sig
    val tt: t
    val ff: t
    val cst: bool -> t
    val var: var -> t
    val ite: t * t * t -> t
    val conj: t list -> t
    val disj: t list -> t
  end
  include module type of Constrs
  module Ops: sig
    val ( !. ): t -> t
    val ( &&. ): t -> t -> t
    val ( ||. ): t -> t -> t
    val ( =>. ): t -> t -> t
    val ( ==. ): t -> t -> t
    val ( <>. ): t -> t -> t
  end
  val mk_ite'
    : decompose:('a -> ((t * 'a * 'a) as 'x) option)
    -> constr:('x -> 'a)
    -> ?conj:(t list -> t)
    -> ?disj:(t list -> t)
    -> compare:('a -> 'a -> int)
    -> recur:('x -> 'a)
    -> 'x -> 'a
  val mk_ite0
    : ?conj:(t list -> t)
    -> ?disj:(t list -> t)
    -> recur:(t * t * t -> t)
    -> t * t * t -> t
  include module type of Ops
  val simpl: t -> t ext_cond
end

(* --- *)

module type NONBOOL_EXP = sig
  type v
  type 'b l
  type cond
  type t = cond l
  include ORDERED_HASHABLE_TYPE with type t := t
  val var: v -> t
  val ite: cond * t * t -> t
  val fold_supp: (v -> 'a -> 'a) -> t -> 'a -> 'a
  val subst_vars: (v -> v option) -> t -> t
  type subst
  val subst: subst -> t -> t
  val mk_ite0
    : ?conj:(cond list -> cond)
    -> ?disj:(cond list -> cond)
    -> recur:(cond * t * t -> t)
    -> cond * t * t -> t
end

module type NONBOOL_EXP_COND = sig
  type v
  type 'b l
  type 'b t
  val print: 'b pp -> (bool * 'b t) pp
  val compare: ('b -> 'b -> int) -> 'b t -> 'b t -> int
  val hash: ('b -> int) -> 'b t -> int
  val fold_supp: ('b -> 'a -> 'a) -> (v -> 'a -> 'a) -> 'b t -> 'a -> 'a
  val subst_vars: ('b -> 'b ext_cond) -> (v -> v option) -> 'b t -> 'b t ext_cond
  val subst: ('b -> 'b ext_cond) -> (v -> 'b l option) -> 'b t -> 'b t ext_cond
  val eq: 'b l -> 'b l -> 'b t ext_cond
end

(* -------------------------------------------------------------------------- *)

module type LEXP_REPR = sig
  type v
  type _ l =
    | LTop : _ l
    | LBot : _ l
    | LVar : v -> _ l
    | LLub : 'b l * 'b l * 'b l list -> 'b l                                (* ⊔ *)
    | LGlb : 'b l * 'b l * 'b l list -> 'b l                                (* ⊓ *)
    | LIte : 'b * 'b l * 'b l -> 'b l
end

module type LEXP_COND = sig
  include LEXP_REPR
  type 'b t =
    | LLe : 'b l * 'b l -> 'b t
    | LEq : 'b l * 'b l -> 'b t
  include NONBOOL_EXP_COND with type v := v
                           and type 'b l := 'b l
                           and type 'b t := 'b t
  val le: 'b l -> 'b l -> 'b t ext_cond
end

module type LEXP = sig
  include LEXP_REPR
  include NONBOOL_EXP with type v := v and type 'b l := 'b l
  val top: t
  val bottom: t
  val of_raw: Security.t -> t
  module Ops: sig
    val ( |.| ): t -> t -> t
    val ( |^| ): t -> t -> t
    val ( <=.~ ): t -> t -> cond
    val ( >=.~ ): t -> t -> cond
    val ( <.~ ): t -> t -> cond
    val ( >.~ ): t -> t -> cond
    val ( ==.~ ): t -> t -> cond
    val ( <>.~ ): t -> t -> cond
  end
  include module type of Ops
  module Cond: LEXP_COND with type v := v and type 'b l := 'b l
end

module type LEXP0 = sig
  include LEXP_REPR
  type 'b t = 'b l
  module Cond: LEXP_COND with type v := v and type 'b l := 'b l
  module Close
    (B: sig
      include EXTENDED_BEXP with type var = v
      val mk_lcond: t Cond.t ext_cond -> t
      val as_lexpr: var -> t subexpr -> t l
    end)
    :
    (LEXP with type v := v
          and type 'b l = 'b l
          and type cond := B.t
          and type subst = B.subst
          and module Cond = Cond)
end

module Lexp
  (Var: ORDERED_HASHABLE_TYPE)
  :
  (LEXP0 with type v := Var.t) =
struct
  module SL = Security

  module T = struct
    type _ l =
      | LTop : _ l
      | LBot : _ l
      | LVar : Var.t -> _ l
      | LLub : 'b l * 'b l * 'b l list -> 'b l                                (* ⊔ *)
      | LGlb : 'b l * 'b l * 'b l list -> 'b l                                (* ⊓ *)
      | LIte : 'b * 'b l * 'b l -> 'b l
  end
  include T
  type 'b t = 'b l

  let print: 'b pp -> 'b t pp = fun pp_b ->
    let rec pl p fmt = let ppp p' = pp_prec p p' fmt in function
      | LTop -> SL.print fmt SL.Top
      | LBot -> SL.print fmt SL.Bot
      | LVar v -> Var.print fmt v
      | LGlb (a, b, [])
        -> ppp 3 "%a @<2>⊓ %a" (pl 3) a (pl 3) b
      | LGlb (a, b, c :: tl)
        -> ppp 3 "%a @<2>⊓ %a" (pl 3) a (pl 3) (LGlb (b, c, tl))
      | LLub (a, b, [])
        -> ppp 4 "%a @<2>⊔ %a" (pl 4) a (pl 4) b
      | LLub (a, b, c :: tl)
        -> ppp 4 "%a @<2>⊔ %a" (pl 4) a (pl 4) (LLub (b, c, tl))
      | LIte (c, t, e)
        -> ppp 13 "if %a then %a else %a" pp_b c (pl 13) t (pl 13) e
    and plX fmt = pl max_int fmt in
    plX

  let rec compare cmp_b a b = let cmp = compare cmp_b in match a, b with
    | LBot, LBot | LTop, LTop -> 0
    | LVar a, LVar b -> Var.compare a b
    | LLub (a1, b1, tl1), LLub (a2, b2, tl2) ->
        cmp_3 cmp cmp (cmp_lst cmp) (a1, b1, tl1) (a2, b2, tl2)
    | LGlb (a1, b1, tl1), LGlb (a2, b2, tl2) ->
        cmp_3 cmp cmp (cmp_lst cmp) (a1, b1, tl1) (a2, b2, tl2)
    | LIte (c1, t1, e1), LIte (c2, t2, e2) ->
        cmp_3 cmp_b cmp cmp (c1, t1, e1) (c2, t2, e2)
    | LTop, _ -> -1 | _, LTop -> 1
    | LBot, _ -> -1 | _, LBot -> 1
    | LVar _, _ -> -1 | _, LVar _ -> 1
    | LLub _, _ -> -1 | _, LLub _ -> 1
    | LGlb _, _ -> -1 | _, LGlb _ -> 1

  let compare' a b = compare Stdlib.compare a b

  let equal cmp_b a b = a == b || compare cmp_b a b = 0

  let (* rec *) hash hb = let hash = Hashtbl.hash(* hash hb *) in function
    | LTop -> 0
    | LBot -> 1
    | LVar v -> Var.hash v lsl 1
    | LLub (a, b, tl) -> hash_3 hash hash (hash_lst hash) (a, b, tl) lsl 3
    | LGlb (a, b, tl) -> hash_3 hash hash (hash_lst hash) (a, b, tl) lsl 5
    | LIte (c, t, e) -> hash_3 hb hash hash (c, t, e) lsl 7

  (* --- *)

  module Constrs = struct
    let top = LTop
    let bottom = LBot
    let var v = LVar v
    let ite (c, t, e) = LIte (c, t, e)
    let ite' c t e = LIte (c, t, e)
    let of_raw = function
      | SL.Top -> LTop
      | SL.Bot -> LBot

    let sort l = List.sort_uniq compare' l
    let rec lub a b tl =
      let rec nolub acc = function
        | [] -> sort acc
        | LLub (a, b, tl) :: tl' -> nolub (a :: b :: List.rev_append tl acc) tl'
        | LTop :: tl' -> [LTop]
        | LBot :: tl' -> nolub acc tl'
        | x :: tl' -> nolub (x :: acc) tl'
      in
      match a, b, tl with                             (* very basic shortcuts *)
        | LTop, _, _ | _, LTop, _ -> LTop
        | LBot, a, [] | a, LBot, [] -> a
        | LBot, a, c :: tl | a, LBot, c :: tl -> lub a c tl
        | a, b, tl -> match nolub [] (a :: b :: tl) with
            | [] -> LTop                                              (* fail? *)
            | [a] -> a
            | a :: b :: tl -> LLub (a, b, tl)
    let rec glb a b tl =
      let rec noglb acc = function
        | [] -> sort acc
        | LGlb (a, b, tl) :: tl' -> noglb (a :: b :: List.rev_append tl acc) tl'
        | LBot :: tl' -> [LBot]
        | LTop :: tl' -> noglb acc tl'
        | x :: tl' -> noglb (x :: acc) tl'
      in
      match a, b, tl with                      (* again: very basic shortcuts *)
        | LBot, _, _ | _, LBot, _ -> LBot
        | LTop, a, [] | a, LTop, [] -> a
        | LTop, a, c :: tl | a, LTop, c :: tl -> glb a c tl
        | a, b, tl -> match noglb [] (a :: b :: tl) with
            | [] -> LBot                                              (* fail? *)
            | [a] -> a
            | a :: b :: tl -> LGlb (a, b, tl)
  end
  include Constrs

  (* --- *)

  module Ops = struct
    let ( |.| ) a b = lub a b []
    let ( |^| ) a b = glb a b []
  end
  include Ops

  (* --- *)

  let fold_supp fb f =
    let rec aux = function
      | LTop | LBot -> ign
      | LVar v -> f v
      | LLub (a, b, tl)
      | LGlb (a, b, tl) -> fun x -> List.fold_left (fun x a -> aux a x) x (a :: b :: tl)
      | LIte (c, t, e) -> fun x -> x |> fb c |> aux t |> aux e
    in aux

  (* --- *)

  open Cstrs

  let ite'' = function
    | Tt -> fun t _ -> t
    | Ff -> fun _ e -> e
    | X c -> ite' c

  (* TODO: check physical equality of composed constructs *)
  let subst' (sb: 'b -> 'b ext_cond) map' =
    let rec sl = function
      | (LTop | LBot) as x -> x
      | LVar v as x -> map' x v
      | LLub (a, b, tl) as x -> cstr3 sl sl (cstrl sl ign tl) lub x a b tl
      | LGlb (a, b, tl) as x -> cstr3 sl sl (cstrl sl ign tl) glb x a b tl
      | LIte (c, t, e) as x -> s_ite x (X c) t e
    and s_ite x = cstr3 sbx sl sl ite'' x
    and sbx = function | Tt -> Tt | Ff -> Ff | X c -> sb c in
    sl

  let subst_vars sb map =
    subst' sb (fun x v -> match map v with None -> x | Some v -> LVar v)

  let subst sb map =
    subst' sb (fun x v -> match map v with None -> x | Some e -> e)

  (* --- *)

  module Cond = struct
    type 'b t =
      | LLe : 'b l * 'b l -> 'b t
      | LEq : 'b l * 'b l -> 'b t
    let print pp_b fmt = let print' = print pp_b in function
      | true,  LLe (a, b) -> pp fmt "%a @<2>⊑ %a" print' a print' b
      | false, LLe (a, b) -> pp fmt "%a @<2>⊐ %a" print' a print' b
      | true,  LEq (a, b) -> pp fmt "%a @<2>= %a" print' a print' b
      | false, LEq (a, b) -> pp fmt "%a @<2>≠ %a" print' a print' b
    let compare cmp_b a b = let cmp_e = compare cmp_b in match a, b with
      | LLe (a1, b1), LLe (a2, b2) -> cmp_pair cmp_e cmp_e (a1, b1) (a2, b2)
      | LEq (a1, b1), LEq (a2, b2) -> cmp_pair cmp_e cmp_e (a1, b1) (a2, b2)
      | LLe _, _ -> -1 | _, LLe _ -> 1
    let hash hb = function
      | LLe (a, b) -> hash_2 (hash hb) (hash hb) (a, b)
      | LEq (a, b) -> hash_2 (hash hb) (hash hb) (a, b) lsl 7
    let fold_supp fb f = let fe = fold_supp fb f in function
      | LLe (a, b)
      | LEq (a, b) -> fun x -> x |> fe a |> fe b
    let le a b = match a, b with
      | LBot, _ | _, LTop -> Tt
      | LTop, LBot -> Ff
      | LVar v, LVar v' when Var.equal v v' -> Tt
      | a, b -> X (LLe (a, b))
    let eq a b = match a, b with
      | LTop, LTop | LBot, LBot -> Tt
      | LTop, LBot | LBot, LTop -> Ff
      | LVar v, LVar v' when Var.equal v v' -> Tt
      | a, b -> X (LEq (a, b))
    let subst' sub sb map =
      let sl = sub sb map in function
        | LLe (a, b) as x -> cstr2 sl sl le (X x) a b
        | LEq (a, b) as x -> cstr2 sl sl eq (X x) a b
    let subst_vars sb map = subst' subst_vars sb map
    let subst sb map = subst' subst sb map
  end

  (* --- *)

  module Close (B: sig
    include EXTENDED_BEXP with type var = Var.t
    val mk_lcond: t Cond.t ext_cond -> t
    val as_lexpr: var -> t subexpr -> t l
  end) = struct
    include T
    type cond = B.t
    type t = cond l
    let print = print B.print
    let compare: t -> t -> int = compare B.compare
    let equal: t -> t -> bool = equal B.compare
    let hash: t -> int = hash B.hash
    include Constrs
    let rec ite tpl = B.mk_ite
      ~decompose:(function | LIte (c, t, e) -> Some (c, t, e) | _ -> None)
      ~compare ~recur:ite ~constr:Constrs.ite tpl
    let fold_supp f = fold_supp (B.fold_supp f) f
    let le a b = match a, b with
      | a, b when compare a b = 0 -> B.tt
      | a, b -> B.mk_lcond (Cond.le a b)
    let eq a b = match a, b with
      | a, b when compare a b = 0 -> B.tt
      | a, b -> B.mk_lcond (Cond.eq a b)
    module Ops = struct
      include Ops
      let ( <=.~ ): t -> t -> cond = le
      let ( >=.~ ): t -> t -> cond = fun a b -> le b a
      let ( <.~ ): t -> t -> cond = fun a b -> B.( !. ) (a >=.~ b)
      let ( >.~ ): t -> t -> cond = fun a b -> B.( !. ) (a <=.~ b)
      let ( ==.~ ): t -> t -> cond = eq
      let ( <>.~ ): t -> t -> cond = fun a b -> B.( !. ) (eq a b)
    end
    include Ops
    let decompose = function
      | LIte (c, t, e) -> Some (c, t, e)
      | _ -> None
    let constr = Constrs.ite
    let mk_ite0 ?conj ?disj ~recur =
      B.mk_ite' ~decompose ~constr ?conj ?disj ~compare ~recur
    module Cond = Cond
    let subst_vars map = subst_vars (B.subst_vars' map) map
    type subst = B.subst
    let subst map = subst (B.subst' map) (fun v -> match map v with
      | `None -> None
      | `Expr e -> Some (B.as_lexpr v e)
      | `Bexp e -> raise @@ B.UnexpectedBexpr (v, e))
  end
end

(* -------------------------------------------------------------------------- *)

module type EEXP_REPR = sig
  type v
  type label
  type _ l =
    | ECst : label -> _ l
    | EVar : v -> _ l
    | EIte : 'b * 'b l * 'b l -> 'b l
end

module type EEXP_COND = sig
  include EEXP_REPR
  type 'b t =
    | EEq : 'b l * 'b l -> 'b t
  include NONBOOL_EXP_COND with type v := v
                           and type 'b l := 'b l
                           and type 'b t := 'b t
end

module type EEXP = sig
  include EEXP_REPR
  include NONBOOL_EXP with type v := v
                      and type 'b l := 'b l
  val enum: label -> t
  module Ops: sig
    val ( ==.@ ): t -> t -> cond
  end
  include module type of Ops
  module Cond: EEXP_COND with type v := v
                         and type label := label
                         and type 'b l := 'b l
end

module type EEXP0 = sig
  include EEXP_REPR
  type 'b t = 'b l
  module Cond: EEXP_COND with type v := v
                         and type label := label
                         and type 'b l := 'b l
  module Close
    (B: sig
      include EXTENDED_BEXP with type var = v
      val mk_econd: t Cond.t ext_cond -> t
      val as_eexpr: var -> t subexpr -> t l
    end)
    :
    (EEXP with type label = label
          and type v := v
          and type 'b l = 'b l
          and type cond := B.t
          and type subst = B.subst
          and module Cond = Cond)
end

module Eexp
  (Var: ORDERED_HASHABLE_TYPE)
  (Label: ORDERED_HASHABLE_TYPE)
  :
  (EEXP0 with type v := Var.t and type label = Label.t)
  =
struct

  module T = struct
    type label = Label.t
    type _ l =
      | ECst : label -> _ l
      | EVar : Var.t -> _ l
      | EIte : 'b * 'b l * 'b l -> 'b l
  end
  include T
  type 'b t = 'b l

  let print: 'b pp -> 'b t pp = fun pp_b ->
    let rec pe p fmt = let ppp p' = pp_prec p p' fmt in function
      | ECst l -> Label.print fmt l
      | EVar v -> Var.print fmt v
      | EIte (c, t, e) -> ppp 13 "if %a then %a else %a\
                                " pp_b c (pe 13) t (pe 13) e
    and peX fmt = pe max_int fmt in
    peX

  let rec compare cmp_b a b = let cmp = compare cmp_b in match a, b with
    | EVar a, EVar b -> Var.compare a b
    | ECst a, ECst b -> Label.compare a b
    | EIte (c1, t1, e1), EIte (c2, t2, e2) ->
        cmp_3 cmp_b cmp cmp (c1, t1, e1) (c2, t2, e2)
    | EVar _, _ -> -1 | _, EVar _ -> 1
    | ECst _, _ -> -1 | _, ECst _ -> 1

  let equal cmp_b a b = a == b || compare cmp_b a b = 0

  let (* rec *) hash hb = let hash = Hashtbl.hash(* hash hb *) in function
    | ECst l -> Label.hash l
    | EVar v -> Var.hash v lsl 7
    | EIte (c, t, e) -> hash_3 hb hash hash (c, t, e) lsl 17

  (* --- *)

  module Constrs = struct
    let enum l = ECst l
    let var v = EVar v
    let ite (c, t, e) = EIte (c, t, e)
    let ite' c t e = EIte (c, t, e)
  end
  include Constrs

  (* --- *)

  let fold_supp fb f =
    let rec aux = function
      | ECst _ -> ign
      | EVar v -> f v
      | EIte (c, t, e) -> fun x -> x |> fb c |> aux t |> aux e
    in aux

  (* --- *)

  let ite'' = function
    | Tt -> fun t _ -> t
    | Ff -> fun _ e -> e
    | X c -> ite' c

  open Cstrs
  let subst' sb map' =
    let rec se = function
      | ECst _ as x -> x
      | EVar v as x -> map' x v
      | EIte (c, t, e) as x -> s_ite x (X c) t e
    and s_ite x = cstr3 sbx se se ite'' x
    and sbx = function | Tt -> Tt | Ff -> Ff | X c -> sb c in
    se

  let subst_vars sb map =
    subst' sb (fun x v -> match map v with None -> x | Some v -> EVar v)

  let subst sb map =
    subst' sb (fun x v -> match map v with None -> x | Some e -> e)

  (* --- *)

  module Cond = struct
    type 'b t =
      | EEq : 'b l * 'b l -> 'b t
    let print pp_b fmt = let print' = print pp_b in function
      | true,  EEq (a, b) -> pp fmt "%a = %a" print' a print' b
      | false, EEq (a, b) -> pp fmt "%a ≠ %a" print' a print' b
    let compare cmp_b a b = let cmp_e = compare cmp_b in match a, b with
      | EEq (a1, b1), EEq (a2, b2) -> cmp_pair cmp_e cmp_e (a1, b1) (a2, b2)
    let hash hb = function
      | EEq (a, b) -> hash_2 (hash hb) (hash hb) (a, b)
    let fold_supp fb f = let fe = fold_supp fb f in function
      | EEq (a, b) -> fun x -> x |> fe a |> fe b
    let eq a b = match a, b with
      | ECst a, ECst b when Label.equal a b -> Tt
      | ECst a, ECst b -> Ff
      | EVar a, EVar b when Var.equal a b -> Tt
      | a, b -> X (EEq (a, b))
    let subst' sub sb map =
      let se = sub sb map in function
        | EEq (a, b) as x -> cstr2 se se eq (X x) a b
    let subst_vars sb map = subst' subst_vars sb map
    let subst sb map = subst' subst sb map
  end

  (* --- *)

  module Close (B: sig
    include EXTENDED_BEXP with type var = Var.t
    val mk_econd: t Cond.t ext_cond -> t
    val as_eexpr: var -> t subexpr -> t l
  end) = struct
    include T
    type cond = B.t
    type t = cond l
    let print = print B.print
    let compare: t -> t -> int = compare B.compare
    let equal: t -> t -> bool = equal B.compare
    let hash: t -> int = hash B.hash
    include Constrs
    let rec ite tpl = B.mk_ite
      ~decompose:(function | EIte (c, t, e) -> Some (c, t, e) | _ -> None)
      ~compare ~recur:ite ~constr:Constrs.ite tpl
    let fold_supp f = fold_supp (B.fold_supp f) f
    let eq a b = match a, b with
      | a, b when compare a b = 0 -> B.tt
      | a, b -> B.mk_econd (Cond.eq a b)
    module Ops = struct
      let ( ==.@ ): t -> t -> B.t = eq
    end
    include Ops
    let decompose = function
      | EIte (c, t, e) -> Some (c, t, e)
      | _ -> None
    let constr = Constrs.ite
    let mk_ite0 ?conj ?disj ~recur =
      B.mk_ite' ~decompose ~constr ?conj ?disj ~compare ~recur
    module Cond = Cond
    let subst_vars map = subst_vars (B.subst_vars' map) map
    type subst = B.subst
    let subst map = subst (B.subst' map) (fun v -> match map v with
      | `None -> None
      | `Expr e -> Some (B.as_eexpr v e)
      | `Bexp e -> raise @@ B.UnexpectedBexpr (v, e))
  end
end

(* --- *)

(* module Nexpr (Var: ORDERED_HASHABLE_TYPE) = struct *)
(*   module Var = Var *)

(*   type _ t = *)
(*     | NCst : int -> _ t *)
(*     | NVar : Var.t -> _ t *)
(*     | NBop : op * 'b t * 'b t -> 'b t *)
(*     | NIte : 'b * 'b t * 'b t -> 'b t *)
(*   and op = Add *)

(*   let rec print: *)
(*   type b. b pp -> b t pp = fun pp_b fmt -> *)
(*     let print' = print pp_b in function *)
(*       | NCst i -> pp fmt "%i" i *)
(*       | NVar v -> Var.print fmt v *)
(*       | NBop (o, a, b) -> pp fmt "(%a %a %a)\ *)
(*                                 " print' a print_op o print' b *)
(*       | NIte (c, t, e) -> pp fmt "@[if %a then %a else %a@]\ *)
(*                                 " pp_b c print' t print' e *)
(*   and print_op fmt = function *)
(*     | Add -> pp fmt "+" *)

(*   let compare cmp_b a b = match a, b with *)
(*     | NVar a, NVar b -> Var.compare a b *)
(*     | NVar _, _ -> -2 | _, NVar _ -> 2 *)
(*     | _ -> Stdlib.compare a b                                     (\* well… *\) *)

(*   (\* --- *\) *)

(*   module Constrs = struct *)
(*     let zero = NCst 0 *)
(*     let ncst i = NCst i *)
(*     let var v = NVar v *)
(*     let add a b = NBop (Add, a, b) *)
(*     let ite (c, t, e) = NIte (c, t, e) *)
(*   end *)
(*   include Constrs *)

(*   (\* --- *\) *)

(*   module Cond = struct *)
(*     type 'b e = 'b t *)
(*     type 'b t = *)
(*       | NEq : 'b e * 'b e -> 'b t *)
(*       | NLe : 'b e * 'b e -> 'b t *)
(*     let print pp_b fmt = *)
(*       let print' = print pp_b in function *)
(*         | NEq (a, b) -> pp fmt "(%a = %a)" print' a print' b *)
(*         | NLe (a, b) -> pp fmt "(%a ≤ %a)" print' a print' b *)
(*     let compare cmp_b a b = *)
(*       let cmp_e = compare cmp_b in *)
(*       match a, b with *)
(*         | NEq (a1, b1), NEq (a2, b2) *)
(*         | NLe (a1, b1), NLe (a2, b2) -> cmp_pair cmp_e cmp_e (a1, b1) (a2, b2) *)
(*         | NEq _, _ -> -1 *)
(*         | _, NEq _ -> 1 *)
(*   end *)

(*   (\* --- *\) *)

(*   module Close *)
(*     (B: EXTENDED_BEXP with module Var = Var) *)
(*     (X: sig val ext: B.t Cond.t -> B.t end) = *)
(*   struct *)
(*     open X *)
(*     type e = B.t t *)
(*     type t = e *)
(*     let print = print B.print *)
(*     let compare: t -> t -> int = compare B.compare *)
(*     include Constrs *)
(*     module Ops = struct *)
(*       open B.Ops *)
(*       let ( ==.. ): t -> t -> B.t = fun a b -> ext (Cond.NEq (a, b)) *)
(*       let ( <=.. ): t -> t -> B.t = fun a b -> ext (Cond.NLe (a, b)) *)
(*       let ( >=.. ): t -> t -> B.t = fun a b -> ext (Cond.NLe (b, a)) *)
(*       let ( <>.. ): t -> t -> B.t = fun a b -> !. (a ==.. b) *)
(*       let ( >.. ): t -> t -> B.t = fun a b -> !. (a <=.. b) *)
(*       let ( <.. ): t -> t -> B.t = fun a b -> !. (a >=.. b) *)
(*     end *)
(*     include Ops *)
(*   end *)
(* end *)

(* --- *)

module Bexp
  (Var: ORDERED_HASHABLE_TYPE)
  (Ext: BEXP_EXTENSION with type var = Var.t)
  :
  (EXTENDED_BEXP with type var = Var.t
                 and  type 'b ext = 'b Ext.t
                 and  type 'b subexpr = 'b Ext.subexpr)
  =
struct

  type var = Var.t
  type 'b ext = 'b Ext.t

  type t =
    | BVar : var -> t
    | BExt : t ext -> t
    | BNot : t -> t
    | BCnj : t list -> t
    | BDsj : t list -> t
    | BIte : t * t * t -> t
    | BEq : t * t -> t
    | BNe : t * t -> t

  let print: t pp =
    let rec pb p fmt = let ppp p' = pp_prec p p' fmt in function
      | BCnj [] -> pp fmt "tt"
      | BDsj [] -> pp fmt "ff"
      | BVar v -> Var.print fmt v
      | BExt e -> Ext.print pbX fmt (true, e)
      | BNot (BExt e) -> Ext.print pbX fmt (false, e)
      | BNot a
        -> ppp 2 "@<1>¬%a" (pb 2) a
      | BCnj [a] | BDsj [a] -> pb p fmt a
      | BCnj (a :: tl)
        -> ppp 11 "%a @<2>∧ %a" (pb 11) a (pb 11) (BCnj tl)
      | BDsj (a :: tl)
        -> ppp 12 "%a @<2>∨ %a" (pb 12) a (pb 12) (BDsj tl)
      | BIte (c, t, e)
        -> ppp 13 "if %a then %a else %a" (pb 13) c (pb 13) t (pb 13) e
      | BEq (a, b)
        -> ppp 7 "%a @<2>≡ %a" (pb 7) a (pb 7) b
      | BNe (a, b)
        -> ppp 7 "%a @<2>≢ %a" (pb 7) a (pb 7) b
    and pbX fmt = pb max_int fmt in
    pbX

  let rec compare a b = match a, b with
    | BVar a, BVar b -> Var.compare a b
    | BNot a, BNot b -> compare a b
    | BExt a, BExt b -> Ext.compare compare a b
    | BCnj tl1, BCnj tl2
    | BDsj tl1, BDsj tl2
      -> cmp_lst compare tl1 tl2
    | BEq (a1, b1), BEq (a2, b2)
    | BNe (a1, b1), BNe (a2, b2)
      -> cmp_2 compare compare (a1, b1) (a2, b2)
    | BIte (c1, t1, e1), BIte (c2, t2, e2)
      -> cmp_3 compare compare compare (c1, t1, e1) (c2, t2, e2)
    | BVar _, _ -> -1 | _, BVar _ -> 1
    | BNot _, _ -> -1 | _, BNot _ -> 1
    | BExt _, _ -> -1 | _, BExt _ -> 1
    | BCnj _, _ -> -1 | _, BCnj _ -> 1
    | BDsj _, _ -> -1 | _, BDsj _ -> 1
    | BIte _, _ -> -1 | _, BIte _ -> 1
    | BEq _, _ -> -1 | _, BEq _ -> 1

  let equal a b = a == b || compare a b = 0

  let (* rec  *)hash = let hash = Hashtbl.hash in function
    | BVar v -> Var.hash v lsl 1
    | BExt e -> Ext.hash hash e lsl 3
    | BNot b -> hash b lsl 5
    | BCnj l -> hash_lst hash l lsl 7
    | BDsj l -> hash_lst hash l lsl 9
    | BEq (a, b) -> hash_2 hash hash (a, b) lsl 11
    | BNe (a, b) -> hash_2 hash hash (a, b) lsl 13
    | BIte (c, t, e) -> hash_3 hash hash hash (c, t, e) lsl 15

  (* --- *)

  module Ops = struct
    let tt = BCnj []
    let ff = BDsj []
    let sort l = List.sort_uniq compare l
    let rec disj tl =
      let rec nodisj acc = function
        | [] -> sort acc
        | BCnj [] :: tl' -> [tt]
        | BDsj [] :: tl' -> nodisj acc tl'
        | BDsj  l :: tl' -> nodisj (List.rev_append l acc) tl'
        | x :: tl' -> nodisj (x :: acc) tl'
      in
      match nodisj [] tl with
        | [a] -> a
        |  l -> BDsj l
    let rec conj tl =
      let rec noconj acc = function
        | [] -> sort acc
        | BDsj [] :: tl' -> [ff]
        | BCnj [] :: tl' -> noconj acc tl'
        | BCnj  l :: tl' -> noconj (List.rev_append l acc) tl'
        | x :: tl' -> noconj (x :: acc) tl'
      in
      match noconj [] tl with
        | [a] -> a
        |  l -> BCnj l
    let ( !. ) = function
      | BDsj [] -> BCnj []
      | BCnj [] -> BDsj []
      | BNot a -> a
      | a -> BNot a
    let ( &&. ) a b = conj [a; b]
    let ( ||. ) a b = disj [a; b]
    let ( =>. ) a b = (!. a) ||. b
    let ( ==. ) a b = if compare a b = 0 then tt else BEq (a, b)
    let ( <>. ) a b = if compare a b = 0 then ff else BNe (a, b)
  end
  include Ops

  (* --- *)

  let ext e = BExt e

  let bite = function
    | (BCnj [] | BNot (BDsj [])), t, _ -> t
    | (BDsj [] | BNot (BCnj [])), _, e -> e
    | c, (BCnj [] | BNot (BDsj [])), e -> c ||. e
    | c, (BDsj [] | BNot (BCnj [])), e -> !.c &&. e
    | c, t, (BCnj [] | BNot (BDsj [])) -> !.c ||. t
    | c, t, (BDsj [] | BNot (BCnj [])) -> c &&. t
    | c, t, e when compare t e = 0 -> c &&. t
    | c, t, e -> BIte (c, t, e)

  let mk_ite' ~decompose ~constr ?(conj = conj) ?(disj = disj) ~compare ~recur =
    function
      | (BCnj [] | BNot (BDsj [])), t, _ -> t
      | (BDsj [] | BNot (BCnj [])), _, e -> e
      | c, t, e
        -> let eq_c = equal c in
          let eq_t x = compare t x = 0 and eq_e x = compare e x = 0 in
          if eq_t e then t else match decompose t, decompose e with
            | Some (c', t', e'), _ when eq_c c' -> recur (c, t', e)
            | Some (c', t', e'), _ when eq_c !.c' -> recur (c, e', e)
            | _, Some (c', t', e') when eq_c c' -> recur (c, t, e')
            | _, Some (c', t', e') when eq_c !.c' -> recur (c, t, t')
            | Some (c', t', e'), _ when eq_e e' -> recur (conj [c; c'], t', e')
            | Some (c', t', e'), _ when eq_e t' -> recur (conj [c; !.c'], e', t')
            | _, Some (c', t', e') when eq_t t' -> recur (disj [c; c'], t', e')
            | _, Some (c', t', e') when eq_t e' -> recur (disj [c; !.c'], e', t')
            | _ -> match c with
                | BNot c' -> recur (c', e, t)
                | _ -> constr (c, t, e)

  let mk_ite ~decompose ~compare ~recur ~constr = function
    | (BCnj [] | BNot (BDsj [])), t, _ -> t
    | (BDsj [] | BNot (BCnj [])), _, e -> e
    | c, t, e when compare t e = 0 -> t
    | c, t, e -> match decompose t, decompose e with
        | Some (c', t', e'), _ when equal c c' -> recur (c, t', e)
        | Some (c', t', e'), _ when equal c !.c' -> recur (c, e', e)
        | _, Some (c', t', e') when equal c c' -> recur (c, t, e')
        | _, Some (c', t', e') when equal c !.c' -> recur (c, t, t')
        | Some (c', t', e'), _ when compare e e' = 0 -> recur (c &&. c', t', e')
        | Some (c', t', e'), _ when compare e t' = 0 -> recur (c &&. !.c', e', t')
        | _, Some (c', t', e') when compare t t' = 0 -> recur (c ||. c', t', e')
        | _, Some (c', t', e') when compare t e' = 0 -> recur (c ||. !.c', e', t')
        | _ -> match c with
            | BNot c' -> recur (c', e, t)
            | _ -> constr (c, t, e)

  let decompose = function | BIte (c, t, e) -> Some (c, t, e) | _ -> None
  let constr = bite

  let rec ite tpl =
    mk_ite ~decompose ~compare ~recur:ite ~constr tpl

  let mk_ite0 ?conj ?disj ~recur =
    mk_ite' ~decompose ~constr ?conj ?disj ~compare ~recur

  module Constrs = struct
    let tt = tt
    let ff = ff
    let cst = function
      | true -> tt
      | false -> ff
    let var v = BVar v
    let ite = ite
    let ite' c t e = ite (c, t, e)
    let conj = conj
    let disj = disj
  end
  include Constrs

  (* --- *)

  let fold_supp f =
    let rec aux = function
      | BVar v -> f v
      | BExt e -> Ext.fold_supp aux f e
      | BNot e -> aux e
      | BCnj l
      | BDsj l -> fun x -> List.fold_left (fun x a -> aux a x) x l
      | BEq (a, b)
      | BNe (a, b) -> fun x -> x |> aux a |> aux b
      | BIte (c, t, e) -> fun x -> x |> aux c |> aux t |> aux e
    in aux

  (* --- *)

  let simpl = function
    | BCnj [] -> Tt
    | BDsj [] -> Ff
    | b -> X b

  open Cstrs
  let subst_vars map =
    let rec sb = function
      | BVar v as x -> (match map v with None -> x | Some v -> BVar v)
      | BExt e as x -> (match Ext.subst_vars sbx map e with
          | Tt -> tt
          | Ff -> ff
          | X e' when e' == e -> x
          | X e' -> BExt e')
      | BNot e as x -> let e' = sb e in if e' == e then x else !.e'
      | BCnj l as x -> cstrl sb conj x l
      | BDsj l as x -> cstrl sb disj x l
      | BEq (a, b) as x -> cstr2 sb sb ( ==. ) x a b
      | BNe (a, b) as x -> cstr2 sb sb ( <>. ) x a b
      | BIte (c, t, e) as x -> s_ite x c t e
    and sbx b = simpl (sb b)
    and s_ite x = cstr3 sb sb sb ite' x in
    sb

  let subst_vars' map b = subst_vars map b |> simpl

  type 'b subexpr = 'b Ext.subexpr
  type subst = (var, t, t subexpr) subst_func
  exception UnexpectedBexpr of var * t
  exception UnexpectedExpr of var * t subexpr
  let subst (map: subst) =
    let rec sb = function
      | BVar v as x ->
          (match map v with
            | `None -> x
            | `Bexp b -> b
            | `Expr e -> raise @@ UnexpectedExpr (v, e))
      | BExt e as x ->
          let e' =
            try Ext.subst sbx (fun v -> match map v with
              | `None -> None
              | `Bexp b -> raise @@ UnexpectedBexpr (v, b)
              | `Expr e -> Some e) e
            with
              | Ext.UnexpectedSubexpr v -> match map v with
                  | `Expr e -> raise @@ UnexpectedExpr (v, e)
                  | _ -> failwith @@ asprintf
                      "Internal error in handling wrong type of subexpression \
                       for variable %a" Var.print v
          in
          (match e' with
            | Tt -> tt
            | Ff -> ff
            | X e' when e' == e -> x
            | X e' -> BExt e')
      | BNot e as x -> let e' = sb e in if e' == e then x else !.e'
      | BCnj l as x -> cstrl sb conj x l
      | BDsj l as x -> cstrl sb disj x l
      | BEq (a, b) as x -> cstr2 sb sb ( ==. ) x a b
      | BNe (a, b) as x -> cstr2 sb sb ( <>. ) x a b
      | BIte (c, t, e) as x -> s_ite x c t e
    and sbx b = simpl (sb b)
    and s_ite x = cstr3 sb sb sb ite' x in
    sb

  let subst' map b = subst map b |> simpl

end

(* -------------------------------------------------------------------------- *)

module type BOPS = sig
  type bexp
  val ( ||. ): bexp -> bexp -> bexp
  val ( &&. ): bexp -> bexp -> bexp
end

module type CONDITIONALS = sig
  type bexp and lexp and eexp
  val bite: bexp * bexp * bexp -> bexp
  val lite: bexp * lexp * lexp -> lexp
  val eite: bexp * eexp * eexp -> eexp
end

(* -------------------------------------------------------------------------- *)

module type ALL = sig
  module Var: ORDERED_HASHABLE_TYPE
  module Lexp0: LEXP0 with type v := Var.t
  module Eexp0: EEXP0 with type v := Var.t and type label = String.t
  module Cond: sig
    type 'b t =
      | LExt: 'b Lexp0.Cond.t -> 'b t
      | EExt: 'b Eexp0.Cond.t -> 'b t
    include BEXP_EXTENSION with type 'b t := 'b t
  end

  module Bexp: sig
    include EXTENDED_BEXP
    with type var = Var.t
    and type 'b ext = 'b Cond.t
    and type 'b subexpr = [`Lexp of 'b Lexp0.t | `Eexp of 'b Eexp0.t]
    exception UnexpectedLexpr of var * t Lexp0.t
    exception UnexpectedEexpr of var * t Eexp0.t
  end
  type bexp = Bexp.t

  module Lexp: LEXP with type v := Var.t
                    and type 'b l = 'b Lexp0.l
                    and type cond := bexp
                    and type subst = Bexp.subst
                    and module Cond = Lexp0.Cond
  type lexp = Lexp.t

  module Eexp: EEXP with type v := Var.t
                    and type label = String.t
                    and type 'b l = 'b Eexp0.l
                    and type cond := bexp
                    and type subst = Bexp.subst
                    and module Cond = Eexp0.Cond
  type eexp = Eexp.t

  type exp =
    | Bexp of bexp
    | Lexp of lexp
    | Eexp of eexp

  type subst = Bexp.subst

  module Exp: sig
    include ORDERED_HASHABLE_TYPE with type t = exp
    val fold_supp: (Var.t -> 'a -> 'a) -> t -> 'a -> 'a
    val subst_vars: (Var.t -> Var.t option) -> t -> t
    val subst: subst -> t -> t
  end

  module Conditionals: CONDITIONALS
    with type bexp := bexp
    and type lexp := lexp
    and type eexp := eexp

  module BOps: BOPS
    with type bexp := bexp

  val pp_bexp: bexp pp
  val pp_lexp: lexp pp
  val pp_eexp: eexp pp
  val pp_exp: exp pp
end

(* --- *)

module MakeAll
  (Var: ORDERED_HASHABLE_TYPE)
  :
  (ALL with module Var = Var)
  =
struct
  module Var = Var

  (* --- *)

  module Lexp0 = Lexp (Var)
  module Eexp0 = Eexp (Var) (String)
  module Cond = struct
    type var = Var.t
    module LExt = Lexp0.Cond
    module EExt = Eexp0.Cond
    type 'b t =
      | LExt: 'b LExt.t -> 'b t
      | EExt: 'b EExt.t -> 'b t
    let print pp_b fmt (p, e) = match e with
      | LExt e -> LExt.print pp_b fmt (p, e)
      | EExt e -> EExt.print pp_b fmt (p, e)
    let compare cmp_b a b = match a, b with
      | LExt a, LExt b -> LExt.compare cmp_b a b
      | EExt a, EExt b -> EExt.compare cmp_b a b
      | LExt _, _ -> -1 | _, LExt _ -> 1
    let hash hb = function
      | LExt e -> LExt.hash hb e
      | EExt e -> EExt.hash hb e lsl 17
    let fold_supp fb f = function
      | LExt e -> LExt.fold_supp fb f e
      | EExt e -> EExt.fold_supp fb f e
    let subst_vars sb map = function
      | LExt a as x -> exfltr (LExt.subst_vars sb map) (fun a -> LExt a) x a
      | EExt a as x -> exfltr (EExt.subst_vars sb map) (fun a -> EExt a) x a
    type 'b subexpr = [ `Lexp of 'b Lexp0.t | `Eexp of 'b Eexp0.t ]
    exception UnexpectedSubexpr of var
    let subst (sb: 'b -> 'b ext_cond) map = function
      | LExt a as x -> exfltr (LExt.subst sb (fun v -> match map v with
          | None -> None
          | Some `Lexp e -> Some e
          | Some _ -> raise @@ UnexpectedSubexpr v)) (fun a -> LExt a) x a
      | EExt a as x -> exfltr (EExt.subst sb (fun v -> match map v with
          | None -> None
          | Some `Eexp e -> Some e
          | Some _ -> raise @@ UnexpectedSubexpr v)) (fun a -> EExt a) x a
  end
  module Bexp = struct
    include Bexp (Var) (Cond)
    let mk_lcond = function | Tt -> tt | Ff -> ff | X e -> ext (Cond.LExt e)
    let mk_econd = function | Tt -> tt | Ff -> ff | X e -> ext (Cond.EExt e)
    exception UnexpectedLexpr of var * t Lexp0.t
    exception UnexpectedEexpr of var * t Eexp0.t
    let as_lexpr v = function
      | `Lexp e -> e
      | `Eexp e -> raise @@ UnexpectedEexpr (v, e)
    let as_eexpr v = function
      | `Eexp e -> e
      | `Lexp e -> raise @@ UnexpectedLexpr (v, e)
  end
  module Lexp = Lexp0.Close (Bexp)
  module Eexp = Eexp0.Close (Bexp)

  type bexp = Bexp.t
  type lexp = Lexp.t
  type eexp = Eexp.t
  type exp =
    | Bexp of bexp
    | Lexp of lexp
    | Eexp of eexp

  type subst = Bexp.subst

  let pp_lexp = Lexp.print
  and pp_eexp = Eexp.print
  and pp_bexp = Bexp.print
  let pp_exp fmt = function
    | Bexp e -> pp_bexp fmt e
    | Lexp e -> pp_lexp fmt e
    | Eexp e -> pp_eexp fmt e

  module Exp = struct
    type t = exp
    let print = pp_exp
    let compare a b = match a, b with
      | Bexp a, Bexp b -> Bexp.compare a b
      | Lexp a, Lexp b -> Lexp.compare a b
      | Eexp a, Eexp b -> Eexp.compare a b
      | Bexp _, _ -> -1 | _, Bexp _ -> 1
      | Lexp _, _ -> -1 | _, Lexp _ -> 1
    let equal a b = a == b || compare a b = 0
    let hash = function
      | Bexp a -> Bexp.hash a
      | Lexp a -> Lexp.hash a lsl 5
      | Eexp a -> Eexp.hash a lsl 10
    let fold_supp f = function
      | Bexp a -> Bexp.fold_supp f a
      | Lexp a -> Lexp.fold_supp f a
      | Eexp a -> Eexp.fold_supp f a
    let subst_vars s = function
      | Bexp a -> Bexp (Bexp.subst_vars s a)
      | Lexp a -> Lexp (Lexp.subst_vars s a)
      | Eexp a -> Eexp (Eexp.subst_vars s a)
    let subst s = function
      | Bexp a -> Bexp (Bexp.subst s a)
      | Lexp a -> Lexp (Lexp.subst s a)
      | Eexp a -> Eexp (Eexp.subst s a)
  end

  module BOps = struct
    let ( &&. ) = Bexp.( &&. )
    let ( ||. ) = Bexp.( ||. )
  end
  module Conditionals = struct
    let bite = Bexp.ite
    let lite = Lexp.ite
    let eite = Eexp.ite
  end

end

(* -------------------------------------------------------------------------- *)
