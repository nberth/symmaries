open Format
open Rutils
open Utils
open Base
open OOLang
open OOBase
open OOMeth
open OOLang.CDR

(* --- *)

module type T = sig
  type t and stm
  val print: t pp
  val pc: t -> pc
  val is_stm: t -> bool
  val is_sink: t -> bool
  val is_first_stage: t -> bool
  val is_second_stage: t -> bool
  val stm: t -> stm
end

(* --- *)

module type METHOD_IMPL = sig
  module Meth: Method.T
  val body: Meth.Sem.domain body
  val sign: Sign.decl option
end

module type CDRS = sig
  module CFG: CFG.T
  include CDR.S with type cfg := CFG.cfg
                and type cfg_fp_setup := CFG.Fixpoint.setup
  val cdrs: info
  val ( ~@? ): ('a -> 'a) -> 'a -> 'a
end

module type ENCODING =
  functor (FeatureFilters: Features.FILTERS) ->
    functor (Env: SCFG.ENV) (Heuristics: Env.HEURISTICS) ->
      functor (Impl: METHOD_IMPL) ->
        functor (CDRs: CDRS with module CFG.Meth = Impl.Meth) ->
sig

  open Env
  module SCFGGraph: SCFG.T
    with module Env = Env

  module SemLoc: sig
    include T with type stm := Impl.Meth.Sem.domain Body.stm
    val actual_loc: t -> SCFGGraph.loc * bexp
  end
  type semloc = SemLoc.t

  module MakeSCFGGenOverlay:
    functor (Gen: SCFGGraph.GRAPH) ->
  sig

    open SCFGGraph
    open Gen

    module GenLoc: sig
      include T with type stm := Impl.Meth.Sem.domain Body.stm
      val semloc: t -> semloc
      val of_semloc: semloc -> t
      val second_stage: t -> t
      val js2_stage: t -> t
      val sink: t -> t
      val succ: t -> t
      val jump_to_pc: pc -> t -> t
      val jump: _ Stm.branch -> t -> t
      val enter_hcdr: region -> t -> t
      val pop_hcdr: t -> t
      val is_lcdr: t -> bexp
    end
    type genloc = GenLoc.t
    type genlocs

    val local_vars: VarSet.t
    val init_bindings: bindings
    val init: data -> t * genlocs
    val preamble: t -> t * genloc

    val hcdr_juncs: genloc -> bexp CDR.M.t

    val acc_trans
      : genloc
      -> (bexp * updates * genloc) list
      -> t -> genlocs -> t * genlocs

    val gen_next
      : js1: (genloc -> t -> genlocs -> t)
      -> js2: (genloc -> t -> genlocs -> t)
      -> t -> genlocs -> t

    val invar_in_loc: genloc -> bexp -> t -> t
    val assert_in_loc: genloc -> bexp -> t -> t
    val restrict_to_location: bexp -> genloc -> t -> t
  end
end

(* --- *)

type trans_merge_option = [ `Always | `Loops | `NonLoops | `Never ]
module type PARTITIONING_OPTIONS = sig
  val loc_var_name: string
  val trans_merge: trans_merge_option
end
module DefaultPartitioningOptions: PARTITIONING_OPTIONS = struct
  let loc_var_name = "ℓ"
  let trans_merge = `Always
end

module MakeTransMerge
  (Opts: PARTITIONING_OPTIONS)
  (SCFG: SCFG.T)
  (UGrd: SCFG.Env.Updates.GUARDING)
  (Gen: SCFG.GRAPH)
  =
struct
  open SCFG
  open Gen

  let merge_trans = Transition.merge ~uite:UGrd.ite

  let add_transition l t l' =
    if (match Opts.trans_merge with
      | `Never -> false
      | `Always -> true
      | `Loops -> Location.equal l l'
      | `NonLoops -> not (Location.equal l l'))
    then merge_transition ~merge_trans l t l'
    else add_transition l t l'
end

(* --- *)

module type EXPLICIT_JS_N_HCDR_OPTIONS = PARTITIONING_OPTIONS
module DefaultExplicitJSnHCDROptions = DefaultPartitioningOptions

module PartitionedExplicitJSnHCDR (Opts: EXPLICIT_JS_N_HCDR_OPTIONS)
  : ENCODING =
  functor (FeatureFilters: Features.FILTERS) ->
    functor (Env: SCFG.ENV) (Heuristics: Env.HEURISTICS) ->
      functor (Impl: METHOD_IMPL) ->
        functor (CDRs: CDRS with module CFG.Meth = Impl.Meth) ->
struct

  open Env
  open Heuristics

  module SCFGLoc = struct
    open Impl
    type t =
        {
          pc: pc;
          prime: bool;
          js: bool;
          hr: region option;
        }
    let compare: t -> t -> int =
      fun { pc = pc1; prime = p1; js = js1; hr = hr1 }
        { pc = pc2; prime = p2; js = js2; hr = hr2 } ->
        cmp_pair
          (cmp_pair PC.compare Stdlib.compare)
          (cmp_pair Stdlib.compare (cmp_opt CDR.T.compare))
          ((pc1, p1), (js1, hr1)) ((pc2, p2), (js2, hr2))
    let equal a b = compare a b = 0
    let hash { pc } = PC.hash pc
    let init = { pc = 0; prime = false; js = false; hr = None }
    let preamble { pc } = { pc = PC.next_neg pc; prime = false;
                            js = false; hr = None }
    let pc { pc } : pc = pc
    let is_stm { pc } = Body.valid_pc body pc
    let is_sink { pc } = PC.equal (Body.first_invalid_pc body) pc
    let is_first_stage { prime } = not prime
    let is_second_stage { prime } = prime
    let is_js2_stage { js } = js
    let stm { pc } = Body.stm_at_pc body pc
    let second_stage ({ prime } as l) = assert (not prime); { l with prime = true }
    let js2_stage ({ js } as l) = assert (not js); { l with js = true }
    let succ ({ pc } as l) = { l with pc = pc + 1; prime = false; js = false }
    let jump_to_pc pc l = { l with pc; prime = false; js = false }
    let jump br = jump_to_pc (Stm.target br)
    let enter_hcdr r l = { l with hr = Some r }
    let pop_hcdr l = { l with hr = None }
    let is_lcdr { hr } = Bexp.cst (hr = None)
    let sink: t -> t = jump_to_pc (Body.first_invalid_pc body)
    let print' =
      let pp_stm = Stm.print_linked ?pad:None ~pp_lvl:Security.print in
      let names =
        let names, dupls = Body.fold_map_stms begin fun defs pc stm ->
          let stm = asprintf "%a" pp_stm stm in
          let pcs = try pc :: StrMap.find stm defs with Not_found -> [ pc ] in
          StrMap.add stm pcs defs, (stm, ScgsUtils.IO.escape_label stm)
        end ("", "") StrMap.empty body in
        StrMap.iter begin fun stm -> function
          | _::_::_ as pcs ->
              List.iter begin fun pc ->
                let stm, stm' = Seq.ra names pc in
                let stm = asprintf "%s @%a" stm PC.print pc
                and stm' = asprintf "%s @%a" stm' PC.print pc in
                Seq.mutate names pc (stm, stm')
              end pcs
          | _ -> ()
        end dupls;
        names
      in
      fun ~escaped fmt ({ pc; prime; js; hr } as l) ->
        let js = if js then "js2" else "js1" in
        let hr fmt = match hr with
          | None -> pp fmt "P."
          | Some r -> pp fmt "P%a" PC.print CDRs.(pc_of_inducing r cdrs)
        in
        if is_stm l then
          let stm = (if escaped then snd else fst) (Seq.ra names pc) in
          pp fmt "@<1>〈%s, %s, %t@<1>〉" stm js hr
        else begin match sign with
          | Some s when pc = -1 ->
              pp fmt "@<1>〈%a〉" Sign.Decl.print_nnl s
          | _ when is_sink l ->
              pp fmt "@<1>〈sink, %s, %t@<1>〉" js hr
          | _ ->
              pp fmt "@<1>〈%a, %s, %t@<1>〉" PC.print pc js hr
        end;
        if prime then if not escaped then pp_print_char fmt '\'' else pp fmt "\\'"
    let print = print' ~escaped:false
    let print_unique fmt { pc; prime; js; hr } =
      pp fmt "pc%a_%s" PC.print pc (if js then "js2" else "js1");
      (match hr with
        | None -> pp fmt "_p."
        | Some r -> pp fmt "p%a" PC.print CDRs.(pc_of_inducing r cdrs));
      if prime then pp fmt "_a"
  end

  module SCFGGraph = SCFG.MakeGraph (Env) (SCFGLoc)

  module SemLoc = struct
    include SCFGLoc
    let actual_loc l = l, Bexp.tt
  end
  type semloc = SemLoc.t

  module MakeSCFGGenOverlay (Gen: SCFGGraph.GRAPH) = struct
    open SCFGGraph
    include Gen

    module GenLoc = struct
      include SemLoc
      type stm = Impl.Meth.Sem.domain Body.stm
      let semloc = ign
      let of_semloc = ign
    end
    type genloc = GenLoc.t
    type genlocs = genloc list

    open GenLoc

    let local_vars = VarSet.empty
    let init_bindings = VarMap.empty

    let init data =
      Gen.init data (Var.raw Opts.loc_var_name) init, [ init ]

    let preamble g =
      let l = preamble (init_loc g) in new_init l g, l

    let hcdr_juncs l = match (semloc l).hr with
      | Some r when CDR.Ts.mem r CDRs.(juncs (pc l) cdrs)
          -> CDR.M.singleton r Bexp.tt
      | _ -> CDR.M.empty

    module UOps = Updates.MakeUOps (Heuristics)
    module UGrd = UOps.MakeGuarding (Heuristics)
    include MakeTransMerge (Opts) (SCFGGraph) (UGrd) (Gen)

    let acc_trans (l: genloc) gul' g rem =
      List.fold_left begin fun (g, rem) (grd, u, l') ->
        Log.d3 logger "%a ? %a ~> %a\
                      " print l Bexp.print grd print l';
        if grd = Bexp.ff then (g, rem) else
          (add_transition l (grd, u) l' g,
           if mem_location g l' then rem else l' :: rem)
      end (g, rem) gul'

    let gen_next ~js1 ~js2 g = function
      | { js } as l :: rem when js -> (js2 [@tailcall]) l g rem
      | l :: rem -> (js1 [@tailcall]) l g rem
      | [] -> g

  end
end

(* --- *)

type 's rel =
  | Same
  | Enter of 's

(* --- *)

module type SYMBOLIC_HCDR_OPTIONS = sig
  val hcdr_typname: string
  val hcdr_varname: string
  val hcdr_label_none: string
  val hcdr_label_of: pc -> string
end
module DefaultSymbolicHCDROptions: SYMBOLIC_HCDR_OPTIONS = struct
  let hcdr_typname = "HCDR"
  let hcdr_varname = "hr"
  let hcdr_label_none: string = "P."
  let hcdr_label_of: pc -> string = asprintf "P%a" PC.print
end

module MakeSymbolicHCDR (Opts: SYMBOLIC_HCDR_OPTIONS) =
  functor (FeatureFilters: Features.FILTERS) ->
    functor (Env: SCFG.ENV) (Heuristics: Env.HEURISTICS) ->
      functor (UOps: Env.Updates.UOPS) ->
        functor (CDRs: CDRS) ->
struct
  open Opts
  open FeatureFilters
  open Env
  open UOps
  open Bexp
  open Eexp
  open Heuristics
  open CDRs

  let hcdr_var = Var.raw hcdr_varname
  let hcdr: eexp = var hcdr_var
  let hcdr_none = enum hcdr_label_none
  let hcdr_of: pc -> eexp = fun pc -> enum (hcdr_label_of pc)
  let hcdr_typ = SCFG.Enum hcdr_typname

  let print fmt = function
    | None -> pp_print_string fmt hcdr_label_none
    | Some r -> pp_print_string fmt (hcdr_label_of (pc_of_inducing r cdrs))

  let local_vars =
    VarSet.empty |>~+ ~@?(VarSet.add hcdr_var)

  let init_bindings =
    VarMap.empty |>~+ ~@?(VarMap.add hcdr_var (Eexp hcdr_none))

  let decl ~decl_label ~decl g = g
    |>~+ ~@?(decl_label hcdr_typname hcdr_label_none)
    |>~+ ~@?(fold_regions
               (fun r pc -> decl_label hcdr_typname (hcdr_label_of pc))
               cdrs)
    |>~+ ~@?(decl hcdr_var hcdr_typ)

  let reset_hcdr = sete hcdr_var hcdr_none
  let set_hcdr (pc: pc) = sete hcdr_var (hcdr_of pc)

  let is_lcdr = tt
    |>~+ ~@?((&&.) (hcdr ==.@ hcdr_none))

  let guard gen_hcdr = tt
    |>~+ ~@?((&&.) (match gen_hcdr with
        | None -> hcdr ==.@ hcdr_none
        | Some r -> hcdr ==.@ hcdr_of (pc_of_inducing r cdrs)))

  let update rel_hcdr = match rel_hcdr with
    | Same -> ign
    | Enter None -> sete hcdr_var hcdr_none
    | Enter Some r -> sete hcdr_var (hcdr_of (pc_of_inducing r cdrs))

  let juncs pc = CDR.M.empty
    |>~+ ~@?(CDR.Ts.fold
               (fun r -> CDR.M.add r (hcdr ==.@ hcdr_of (pc_of_inducing r cdrs)))
               (juncs pc cdrs))

end

(* --- *)

module type SYMBOLIC_JS_OPTIONS = sig
  val js_typname: string
  val js_varname: string
  val js_name: int -> string
end
module DefaultSymbolicJSOptions: SYMBOLIC_JS_OPTIONS = struct
  let js_typname = "JuncStep"
  let js_varname = "η"
  let js_name: int -> string = asprintf "js%i"
end

module MakeSymbolicJS (Opts: SYMBOLIC_JS_OPTIONS) =
  functor (Env: SCFG.ENV) (Heuristics: Env.HEURISTICS) ->
    functor (UOps: Env.Updates.UOPS) ->
struct
  open Opts
  open Env
  open UOps
  open Bexp
  open Eexp
  open Heuristics

  open Opts
  let js1_label = js_name 1 and js2_label = js_name 2
  let js1 = enum js1_label and js2 = enum js2_label
  let js_typ = SCFG.Enum js_typname
  let js_var = Var.raw js_varname
  let js1_stage: bexp = var js_var ==.@ js1
  let js2_stage: bexp = var js_var ==.@ js2

  let local_vars = VarSet.singleton js_var
  let init_bindings = VarMap.singleton js_var (Eexp js1)

  let decl ~decl_label ~decl g =
    (g  |> decl_label js_typname js1_label
        |> decl_label js_typname js2_label
        |> decl js_var js_typ)

  type t =
    | Js1
    | Js2

  let print fmt = function
    | Js1 -> pp fmt "js1"
    | Js2 -> pp fmt "js2"

  let guard = function
    | Js1 -> js1_stage
    | Js2 -> js2_stage

  let update = function
    | Same -> ign
    | Enter Js1 -> sete js_var js1
    | Enter Js2 -> sete js_var js2
end

(* --- *)

module type EXPLICIT_JS_SYMBOLIC_HCDR_OPTIONS = sig
  include PARTITIONING_OPTIONS
  include SYMBOLIC_HCDR_OPTIONS
end
module DefaultExplicitJSSymbolicHCDROptions: EXPLICIT_JS_SYMBOLIC_HCDR_OPTIONS =
struct
  include DefaultPartitioningOptions
  include DefaultSymbolicHCDROptions
end

module PartitionedSymbolicHCDR (Opts: EXPLICIT_JS_SYMBOLIC_HCDR_OPTIONS)
  : ENCODING =
  functor (FeatureFilters: Features.FILTERS) ->
    functor (Env: SCFG.ENV) (Heuristics: Env.HEURISTICS) ->
      functor (Impl: METHOD_IMPL) ->
        functor (CDRs: CDRS with module CFG.Meth = Impl.Meth) ->
struct

  open FeatureFilters
  open Env
  module UOps = Updates.MakeUOps (Heuristics)
  open Updates
  open UOps
  open Bexp
  open Eexp
  open Heuristics

  module SCFGLoc = struct
    open Impl
    type t =
        {
          pc: pc;
          prime: bool;
          js: bool;
        }
    let compare: t -> t -> int =
      fun { pc = pc1; prime = p1; js = js1 } { pc = pc2; prime = p2; js = js2 } ->
        cmp_triple PC.compare Stdlib.compare Stdlib.compare
          (pc1, p1, js1) (pc2, p2, js2)
    let equal a b = compare a b = 0
    let hash { pc } = PC.hash pc
    let init = { pc = 0; prime = false; js = false }
    let preamble { pc } = { pc = PC.next_neg pc; prime = false; js = false }
    let pc { pc } : pc = pc
    let is_stm { pc } = Body.valid_pc body pc
    let is_sink { pc } = PC.equal (Body.first_invalid_pc body) pc
    let is_first_stage { prime } = not prime
    let is_second_stage { prime } = prime
    let is_js2_stage { js } = js
    let stm { pc } = Body.stm_at_pc body pc
    let second_stage ({ prime } as l) = assert (not prime); { l with prime = true }
    let js2_stage ({ js } as l) = assert (not js); { l with js = true }
    let succ { pc } = { pc = pc + 1; prime = false; js = false }
    let jump_to_pc pc l = { pc; prime = false; js = false }
    let jump br = jump_to_pc (Stm.target br)
    let sink: t -> t = jump_to_pc (Body.first_invalid_pc body)
    let print' =
      let pp_stm = Stm.print_linked ?pad:None ~pp_lvl:Security.print in
      let names =
        let names, dupls = Body.fold_map_stms begin fun defs pc stm ->
          let stm = asprintf "%a" pp_stm stm in
          let pcs = try pc :: StrMap.find stm defs with Not_found -> [ pc ] in
          StrMap.add stm pcs defs, (stm, ScgsUtils.IO.escape_label stm)
        end ("", "") StrMap.empty body in
        StrMap.iter begin fun stm -> function
          | _::_::_ as pcs ->
              List.iter begin fun pc ->
                let stm, stm' = Seq.ra names pc in
                let stm = asprintf "%s @%a" stm PC.print pc
                and stm' = asprintf "%s @%a" stm' PC.print pc in
                Seq.mutate names pc (stm, stm')
              end pcs
          | _ -> ()
        end dupls;
        names
      in
      fun ~escaped fmt ({ pc; prime; js } as l) ->
        let js = if js then "js2" else "js1" in
        if is_stm l then
          let stm = (if escaped then snd else fst) (Seq.ra names pc) in
          pp fmt "@<1>〈%s, %s@<1>〉" stm js
        else begin match sign with
          | Some s when pc = -1 ->
              pp fmt "@<1>〈%a〉" Sign.Decl.print_nnl s
          | _ when is_sink l ->
              pp fmt "@<1>〈sink, %s@<1>〉" js
          | _ ->
              pp fmt "@<1>〈%a, %s@<1>〉" PC.print pc js
        end;
        if prime then if not escaped then pp_print_char fmt '\'' else pp fmt "\\'"
    let print = print' ~escaped:false
    let print_unique fmt { pc; prime; js } =
      pp fmt "pc%a_%s" PC.print pc (if js then "js2" else "js1");
      if prime then pp fmt "_a"
  end

  module SCFGGraph = SCFG.MakeGraph (Env) (SCFGLoc)

  (* --- *)

  module HCDR = (MakeSymbolicHCDR (Opts) (FeatureFilters) (Env)
                   (Heuristics) (UOps) (CDRs))
  open CDRs

  (* --- *)

  module SemLoc = struct
    open SCFGLoc

    type t = { loc: SCFGLoc.t;
               rel_hcdr: region option rel;
               gen_hcdr: region option; }

    let print fmt l =
      pp fmt "(%a, %a)"
        print l.loc HCDR.print l.gen_hcdr

    let pc l = pc l.loc
    let stm l = stm l.loc
    let is_stm l = is_stm l.loc
    let is_sink l = is_sink l.loc
    let is_first_stage l = is_first_stage l.loc
    let is_second_stage l = is_second_stage l.loc

    let of_scfgloc loc = { loc; rel_hcdr = Same; gen_hcdr = None; }

    let init = of_scfgloc init
    let preamble s = of_scfgloc (preamble s)

    let cont l loc = { l with loc; rel_hcdr = Same }
    let jump_to_pc pc l = cont l (jump_to_pc pc l.loc)
    let jump br l = cont l (jump br l.loc)
    let succ l = cont l (succ l.loc)
    let sink l = cont l (sink l.loc)
    let second_stage l = cont l (second_stage l.loc)
    let js2_stage l = cont l (js2_stage l.loc)
    let enter_hcdr r l = { l with rel_hcdr = Enter (Some r) }
    let pop_hcdr l = { l with rel_hcdr = Enter None }

    let is_lcdr _ = HCDR.is_lcdr
    let hcdr_guard l = HCDR.guard l.gen_hcdr
    let hcdr_update l = HCDR.update l.rel_hcdr

    let actual_loc l = l.loc, hcdr_guard l
  end
  type semloc = SemLoc.t

  (* --- *)

  module MakeSCFGGenOverlay (Gen: SCFGGraph.GRAPH) = struct
    open SCFGGraph
    include Gen

    module GenLoc = struct
      include SemLoc
      type stm = Impl.Meth.Sem.domain Body.stm
      let semloc = ign
      let of_semloc = ign
    end
    type genloc = GenLoc.t
    type genlocs = genloc list
    open GenLoc

    (* --- *)

    let local_vars = HCDR.local_vars
    let init_bindings = HCDR.init_bindings

    let init data =
      let init_loc = SCFGLoc.init in
      let g = begin Gen.init data (Var.raw Opts.loc_var_name) init_loc
          |> HCDR.decl ~decl_label ~decl:(fun v t -> decl v t State)
      end in
      g, [ init ]

    let preamble g =
      let init_loc = (* GenLoc.{ l =  *)init_loc g(* ; p = JuncStage.js1_stage } *) in
      let l = preamble init_loc in
      new_init l.loc g, l

    let hcdr_juncs l = HCDR.juncs (pc l)

    (* --- *)

    module UGrd = UOps.MakeGuarding (Heuristics)
    include MakeTransMerge (Opts) (SCFGGraph) (UGrd) (Gen)

    let acc_trans (l: genloc) gul' g (rem: genlocs) : _ * genlocs =
      List.fold_left begin fun (g, rem) (grd, u, l') ->
        Log.d logger "@[%a ? %a ~> %a@]" print l Bexp.print grd SCFGLoc.print l'.loc;
        if grd = Bexp.ff then (g, rem) else begin
          let u' = u |> hcdr_update l' in
          (* let l_new = match l'.rel_js with *)
          (*   | Same -> [] *)
          (*   | Enter Js1 when mem_location g l'.loc -> [] *)
          (*   | Enter Js1 -> [{ l' with gen_js = Js1 }] *)
          (*   | Enter Js2 -> [{ l' with gen_js = Js2 }] *)
          (* in *)
          let l_new = match l'.rel_hcdr with
            | Same when mem_location g l'.loc -> []
            | Same -> [{l' with gen_hcdr = l.gen_hcdr }]
            | Enter x -> [{ l' with gen_hcdr = x }]
          in
          (add_transition l.loc (grd, u') l'.loc g,
           l_new @ rem)
           end
      end (g, rem) gul'

    (* --- *)

    (* open SCFGLoc *)

    let gen_next ~js1 ~js2 g = function
      | { loc = { js } } as l :: rem when js -> (js2 [@tailcall]) l g rem
      | l :: rem -> (js1 [@tailcall]) l g rem
      | [] -> g

    (* --- *)

    let invar_in_loc l p =
      invar_in_loc l.loc p

    let assert_in_loc l p =
      assert_in_loc l.loc p

    let restrict_to_location p l =                             (* hcdr_guard? *)
      restrict_to_location (hcdr_guard l &&. p) l.loc

  end
end

(* --- *)

module type SYMBOLIC_JS_N_HCDR_OPTIONS = sig
  include PARTITIONING_OPTIONS
  include SYMBOLIC_JS_OPTIONS
  include SYMBOLIC_HCDR_OPTIONS
end
module DefaultSymbolicJSnHCDROptions: SYMBOLIC_JS_N_HCDR_OPTIONS = struct
  include DefaultPartitioningOptions
  include DefaultSymbolicJSOptions
  include DefaultSymbolicHCDROptions
end

module PartitionedSymbolicJSnHCDR (Opts: SYMBOLIC_JS_N_HCDR_OPTIONS)
  : ENCODING =
  functor (FeatureFilters: Features.FILTERS) ->
    functor (Env: SCFG.ENV) (Heuristics: Env.HEURISTICS) ->
      functor (Impl: METHOD_IMPL) ->
        functor (CDRs: CDRS with module CFG.Meth = Impl.Meth) ->
struct

  open FeatureFilters
  open Env
  module UOps = Updates.MakeUOps (Heuristics)
  open Updates
  open UOps
  open Bexp
  open Eexp
  open Heuristics

  module SCFGLoc = struct
    open Impl
    type t =
        {
          pc: pc;
          prime: bool;
        }
    let compare: t -> t -> int =
      fun { pc = pc1; prime = p1 } { pc = pc2; prime = p2 } ->
        cmp_pair PC.compare Stdlib.compare (pc1, p1) (pc2, p2)
    let equal a b = compare a b = 0
    let hash { pc; prime } = hash_pair PC.hash Hashtbl.hash (pc, prime)
    let init = { pc = 0; prime = false }
    let preamble { pc } = { pc = PC.next_neg pc; prime = false }
    let pc { pc } = pc
    let is_stm { pc } = Body.valid_pc body pc
    let is_sink { pc } = PC.equal (Body.first_invalid_pc body) pc
    let is_first_stage { prime } = not prime
    let is_second_stage { prime } = prime
    let stm { pc } = Body.stm_at_pc body pc
    let second_stage ({ prime } as l) = assert (not prime); { l with prime = true }
    let succ { pc } = { pc = pc + 1; prime = false }
    let jump_to_pc pc l = { pc; prime = false }
    let jump br = jump_to_pc (Stm.target br)
    let sink l = jump_to_pc (Body.first_invalid_pc body) l
    let print' =
      let pp_stm = Stm.print_linked ?pad:None ~pp_lvl:Security.print in
      let names =
        let names, dupls = Body.fold_map_stms begin fun defs pc stm ->
          let stm = asprintf "%a" pp_stm stm in
          let pcs = try pc :: StrMap.find stm defs with Not_found -> [ pc ] in
          StrMap.add stm pcs defs, (stm, ScgsUtils.IO.escape_label stm)
        end ("", "") StrMap.empty body in
        StrMap.iter begin fun stm -> function
          | _::_::_ as pcs ->
              List.iter begin fun pc ->
                let stm, stm' = Seq.ra names pc in
                let stm = asprintf "%s @%a" stm PC.print pc
                and stm' = asprintf "%s @%a" stm' PC.print pc in
                Seq.mutate names pc (stm, stm')
              end pcs
          | _ -> ()
        end dupls;
        names
      in
      fun ~escaped fmt ({ pc; prime } as l) ->
        begin
          if is_stm l then
            let stm = (if escaped then snd else fst) (Seq.ra names pc) in
            pp fmt "@<1>〈%s@<1>〉" stm
          else match sign with
            | Some s when pc = -1 ->
                pp fmt "@<1>〈%a〉" Sign.Decl.print_nnl s
            | _ when is_sink l ->
                pp fmt "@<1>〈sink@<1>〉"
            | _ ->
                pp fmt "@<1>〈%a@<1>〉" PC.print pc
        end;
        if prime then if not escaped then pp_print_char fmt '\'' else pp fmt "\\'"
    let print = print' ~escaped:false
    let print_unique fmt { pc; prime } =
      pp fmt "pc%a" PC.print pc;
      if prime then pp fmt "_a"

  end

  module SCFGGraph = SCFG.MakeGraph (Env) (SCFGLoc)

  (* --- *)

  module HCDR = (MakeSymbolicHCDR (Opts) (FeatureFilters) (Env)
                   (Heuristics) (UOps) (CDRs))
  module JuncStage = MakeSymbolicJS (Opts) (Env) (Heuristics) (UOps)
  type js = JuncStage.t

  open JuncStage
  open CDRs

  (* --- *)

  module SemLoc = struct
    open SCFGLoc

    type t = { loc: SCFGLoc.t;
               rel_js: js rel;
               gen_js: js;
               rel_hcdr: region option rel;
               gen_hcdr: region option; }

    let print fmt l =
      pp fmt "(%a, %a, %a)"
        print l.loc JuncStage.print l.gen_js HCDR.print l.gen_hcdr

    let pc l = pc l.loc
    let stm l = stm l.loc
    let is_stm l = is_stm l.loc
    let is_sink l = is_sink l.loc
    let is_first_stage l = is_first_stage l.loc
    let is_second_stage l = is_second_stage l.loc

    let of_scfgloc loc = { loc;
                           rel_js = Enter Js1; gen_js = Js1;
                           rel_hcdr = Same; gen_hcdr = None; }

    let init = of_scfgloc init
    let preamble s = of_scfgloc (preamble s)

    let cont l loc = { l with loc; rel_js = Enter Js1; rel_hcdr = Same }
    let jump_to_pc pc l = cont l (jump_to_pc pc l.loc)
    let jump br l = cont l (jump br l.loc)
    let succ l = cont l (succ l.loc)
    let sink l = cont l (sink l.loc)
    let second_stage l = cont l (second_stage l.loc)
    let js2_stage l = { l with rel_js = Enter Js2 }
    let enter_hcdr r l = { l with rel_hcdr = Enter (Some r) }
    let pop_hcdr l = { l with rel_hcdr = Enter None }

    let js_guard l = JuncStage.guard l.gen_js
    let js_update l = JuncStage.update l.rel_js

    let is_lcdr _ = HCDR.is_lcdr
    let hcdr_guard l = HCDR.guard l.gen_hcdr
    let hcdr_update l = HCDR.update l.rel_hcdr

    let actual_loc l = l.loc, js_guard l &&. hcdr_guard l
  end
  type semloc = SemLoc.t

  (* --- *)

  module MakeSCFGGenOverlay (Gen: SCFGGraph.GRAPH) = struct
    open SCFGGraph
    include Gen

    module GenLoc = struct
      include SemLoc
      type stm = Impl.Meth.Sem.domain Body.stm
      let semloc = ign
      let of_semloc = ign
    end
    type genloc = GenLoc.t
    type genlocs = genloc list
    open GenLoc

    (* --- *)

    let local_vars =
      VarSet.union HCDR.local_vars JuncStage.local_vars
    let init_bindings =
      VarMap.union (fun _ _ _ -> assert false)
        HCDR.init_bindings JuncStage.init_bindings

    let init data =
      let init_loc = SCFGLoc.init in
      let g = begin Gen.init data (Var.raw Opts.loc_var_name) init_loc
          |> JuncStage.decl ~decl_label ~decl:(fun v t -> decl v t State)
          |> HCDR.decl ~decl_label ~decl:(fun v t -> decl v t State)
      end in
      g, [ init ]

    let preamble g =
      let init_loc = (* GenLoc.{ l =  *)init_loc g(* ; p = JuncStage.js1_stage } *) in
      let l = preamble init_loc in
      new_init l.loc g, l

    let hcdr_juncs l = HCDR.juncs (pc l)

    (* --- *)

    module UGrd = UOps.MakeGuarding (Heuristics)
    include MakeTransMerge (Opts) (SCFGGraph) (UGrd) (Gen)

    let acc_trans (l: genloc) gul' g (rem: genlocs) : _ * genlocs =
      List.fold_left begin fun (g, rem) (grd, u, l') ->
        Log.d logger "@[%a ? %a ~> %a@]" print l Bexp.print grd SCFGLoc.print l'.loc;
        if grd = Bexp.ff then (g, rem) else begin
          let u' = u |> js_update l' |> hcdr_update l' in
          let l_new = match l'.rel_js with
            | Same -> []
            | Enter Js1 when mem_location g l'.loc -> []
            | Enter Js1 -> [{ l' with gen_js = Js1 }]
            | Enter Js2 -> [{ l' with gen_js = Js2 }]
          in
          let l_new = match l'.rel_hcdr with
            | Same -> l_new
            | Enter x -> List.map (fun l' -> { l' with gen_hcdr = x }) l_new
          in
          (add_transition l.loc (js_guard l (* &&. hcdr_guard l *) &&. grd, u') l'.loc g,
           l_new @ rem)
        end
      end (g, rem) gul'

    (* --- *)

    let gen_next ~js1 ~js2 g = function
      | l :: rem ->
          let gen = match l.gen_js with Js1 -> js1 | Js2 -> js2 in
          (gen [@tailcall]) l g rem
      | [] -> g

    (* --- *)

    let invar_in_loc l p =
      invar_in_loc l.loc (js_guard l =>. p)

    let assert_in_loc l p =
      assert_in_loc l.loc (js_guard l =>. p)

    let restrict_to_location p l =                             (* hcdr_guard? *)
      restrict_to_location (js_guard l &&. hcdr_guard l &&. p) l.loc

  end
end

(* --- *)

module type ALL_OPTIONS = sig
  include PARTITIONING_OPTIONS
  include EXPLICIT_JS_SYMBOLIC_HCDR_OPTIONS
  include EXPLICIT_JS_N_HCDR_OPTIONS
  include SYMBOLIC_JS_N_HCDR_OPTIONS
end
module DefaultOptions = struct
  include DefaultPartitioningOptions
  include DefaultExplicitJSSymbolicHCDROptions
  include DefaultExplicitJSnHCDROptions
  include DefaultSymbolicJSnHCDROptions
end

(* --- *)
