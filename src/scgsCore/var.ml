open Format
open Rutils
open Utils
open OOLang
open OOLang.Base

(* -------------------------------------------------------------------------- *)

type vfmt = (var pp -> var -> unit) PPrt.fmt

module AnyVar = struct
  include OOLang.Base.Var
  let print' ?(format: vfmt = "%a") fmt = pp fmt format print
  let external_format: vfmt = "var_%a"
  let printx = print' ~format:external_format
end

(* --- *)

module MakeVars (Fmts: sig
  val internal_format: vfmt
  val external_format: vfmt
end) = struct
  include AnyVar
  include Fmts
  let print = print' ~format:internal_format
  let printx = print' ~format:external_format
end

(* --- *)

module GuardVar = MakeVars (struct
  let internal_format: vfmt = "ok(%a)"
  let external_format: vfmt = "ok_%a"
end)

module LevelVar = MakeVars (struct
  let internal_format: vfmt = "level(%a)"
  let external_format: vfmt = "level_%a"
end)

module TaintVar = MakeVars (struct
  let internal_format: vfmt = "taint(%a)"
  let external_format: vfmt = "taint_%a"
end)

module ObjLevelVar = MakeVars (struct
  let internal_format: vfmt = "obj_level(%a)"
  let external_format: vfmt = "obj_level_%a"
end)

module ObjTaintVar = MakeVars (struct
  let internal_format: vfmt = "obj_taint(%a)"
  let external_format: vfmt = "obj_taint_%a"
end)

(* --- *)

module AliasRelVar = struct
  module T = struct
    include OrderedPairOf (AnyVar)
    let print = print' ~format:"%a~%a"
    let printx fmt (a, b) = pp fmt "alias_%i_%s_%s" (String.length a) a b
  end
  include (T: module type of T with type t := T.t)
  include FilteredValues (T) (struct
    type t = T.t
    type const = bool
    type param = unit                                    (* no parameter here *)
    let filter () x = if is_sym x then Some true else None
  end)
  let make () a b = make () (T.make a b)
  let print' ?format fmt e = print' ?format fmt (get e)
  let printx fmt e = printx fmt (get e)
  module Ops = struct
    let ( $~ ) = make ()
  end
  include Ops
end
module AliasRelVars = struct
  include Helpers.MakeSet (AliasRelVar)
  let add e s = match e with
    | AliasRelVar.Value v -> add v s
    | _ -> s
end

(* --- *)

module ShareRelVar = struct
  module T = struct
    include OrderedPairOf (AnyVar)
    let print = print' ~format:"%a><%a"
    let printx fmt (a, b) = pp fmt "share_%i_%s_%s" (String.length a) a b
  end
  include (T: module type of T with type t := T.t)
  include FilteredValues (T) (struct
    type t = T.t
    type const = bool
    type param = unit                                    (* no parameter here *)
    let filter () x = if is_sym x then Some true else None
  end)
  let make () a b = make () (T.make a b)
  let print' ?format fmt e = print' ?format fmt (get e)
  let printx fmt e = printx fmt (get e)
  module Ops = struct
    let ( $>< ) = make ()
  end
  include Ops
end
module ShareRelVars = struct
  include Helpers.MakeSet (ShareRelVar)
  let add e s = match e with
    | ShareRelVar.Value v -> add v s
    | _ -> s
end

(* --- *)

module FieldAliasRelVar = struct
  include PairOf (AnyVar)
  let print = print' ~format:"%a.~>%a"
  let printx fmt (a, b) = pp fmt "field_alias_%i_%s_%s" (String.length a) a b
  module Ops = struct
    let ( $.~> ) = make
  end
  include Ops
end
module FieldAliasRelVars = Helpers.MakeSet (FieldAliasRelVar)

(* --- *)

module FieldShareRelVar = struct
  include PairOf (AnyVar)
  let print = print' ~format:"%a.><%a"
  let printx fmt (a, b) = pp fmt "field_share_%i_%s_%s" (String.length a) a b
  module Ops = struct
    let ( $.>< ) = make
  end
  include Ops
end
module FieldShareRelVars = Helpers.MakeSet (FieldShareRelVar)

(* -------------------------------------------------------------------------- *)

type t =
  | Var of AnyVar.t
  | Guard of GuardVar.t
  | Level of LevelVar.t
  | Taint of TaintVar.t
  | HLevel of ObjLevelVar.t
  | HTaint of ObjTaintVar.t
  | AliasRel of AliasRelVar.t
  | ShareRel of ShareRelVar.t
  | FieldAliasRel of FieldAliasRelVar.t
  | FieldShareRel of FieldShareRelVar.t
  | Prime of t
  | Ret of t
  | Exc of t
let rec compare a b = match a, b with
  | Var a, Var b -> AnyVar.compare a b
  | Guard a, Guard b -> GuardVar.compare a b
  | Level a, Level b -> LevelVar.compare a b
  | Taint a, Taint b -> TaintVar.compare a b
  | HLevel a, HLevel b -> ObjLevelVar.compare a b
  | HTaint a, HTaint b -> ObjTaintVar.compare a b
  | AliasRel a, AliasRel b -> AliasRelVar.compare a b
  | ShareRel a, ShareRel b -> ShareRelVar.compare a b
  | FieldAliasRel a, FieldAliasRel b -> FieldAliasRelVar.compare a b
  | FieldShareRel a, FieldShareRel b -> FieldShareRelVar.compare a b
  | Prime a, Prime b -> compare a b
  | Ret a, Ret b -> compare a b
  | Exc a, Exc b -> compare a b
  | Var _, _ -> -1 | _, Var _ -> 1
  | Guard _, _ -> -1 | _, Guard _ -> 1
  | Level _, _ -> -1 | _, Level _ -> 1
  | Taint _, _ -> -1 | _, Taint _ -> 1
  | HLevel _, _ -> -1 | _, HLevel _ -> 1
  | HTaint _, _ -> -1 | _, HTaint _ -> 1
  | AliasRel _, _ -> -1 | _, AliasRel _ -> 1
  | ShareRel _, _ -> -1 | _, ShareRel _ -> 1
  | FieldAliasRel _, _ -> -1 | _, FieldAliasRel _ -> 1
  | FieldShareRel _, _ -> -1 | _, FieldShareRel _ -> 1
  | Prime _, _ -> -1 | _, Prime _ -> 1
  | Ret _, _ -> -1 | _, Ret _ -> 1
let equal a b = compare a b = 0
let hash = Hashtbl.hash
let rec print fmt = function
  | Var v -> AnyVar.print fmt v
  | Guard v -> GuardVar.print fmt v
  | Level v -> LevelVar.print fmt v
  | Taint v -> TaintVar.print fmt v
  | HLevel v -> ObjLevelVar.print fmt v
  | HTaint v -> ObjTaintVar.print fmt v
  | AliasRel v -> AliasRelVar.print fmt v
  | ShareRel v -> ShareRelVar.print fmt v
  | FieldAliasRel v -> FieldAliasRelVar.print fmt v
  | FieldShareRel v -> FieldShareRelVar.print fmt v
  | Prime v -> pp fmt "%a'" print v
  | Ret v -> pp fmt "ret(%a)" print v
  | Exc v -> pp fmt "exc(%a)" print v
let rec printx fmt = function
  | Var v -> AnyVar.printx fmt v
  | Guard v -> GuardVar.printx fmt v
  | Level v -> LevelVar.printx fmt v
  | Taint v -> TaintVar.printx fmt v
  | HLevel v -> ObjLevelVar.printx fmt v
  | HTaint v -> ObjTaintVar.printx fmt v
  | AliasRel v -> AliasRelVar.printx fmt v
  | ShareRel v -> ShareRelVar.printx fmt v
  | FieldAliasRel v -> FieldAliasRelVar.printx fmt v
  | FieldShareRel v -> FieldShareRelVar.printx fmt v
  | Prime v -> pp fmt "prime_%a" printx v
  | Ret v -> pp fmt "ret_%a" printx v
  | Exc v -> pp fmt "exc_%a" printx v

let subst f =
  let subst_pair (a, b) =
    match ((try Some (f a) with Not_found -> None),
           (try Some (f b) with Not_found -> None)) with
      | Some a, Some b -> a, b
      | Some a, None -> a, b
      | None, Some b -> a, b
      | None, None -> raise Not_found
  in
  let rec s v =
    try match v with
      | Var v -> Some (Var (f v))
      | Guard v -> Some (Guard (f v))
      | Level v -> Some (Level (f v))
      | Taint v -> Some (Taint (f v))
      | HLevel v -> Some (HLevel (f v))
      | HTaint v -> Some (HTaint (f v))
      | AliasRel v ->
          let a, b = subst_pair (AliasRelVar.get v) in
          (match AliasRelVar.make () a b with
            | AliasRelVar.Value v -> Some (AliasRel v)
            | AliasRelVar.Const _ -> None)                              (* XXX *)
      | ShareRel v ->
          let a, b = subst_pair (ShareRelVar.get v) in
          (match ShareRelVar.make () a b with
            | ShareRelVar.Value v -> Some (ShareRel v)
            | ShareRelVar.Const _ -> None)                            (* XXX *)
      | FieldAliasRel (a, b) ->
          let a, b = subst_pair (a, b) in
          Some (FieldAliasRel (FieldAliasRelVar.make a b))
      | FieldShareRel (a, b) ->
          let a, b = subst_pair (a, b) in
          Some (FieldShareRel (FieldShareRelVar.make a b))
      | Prime v -> (match s v with Some v -> Some (Prime v) | None -> None)
      | Ret v -> (match s v with Some v -> Some (Ret v) | None -> None)
      | Exc v -> (match s v with Some v -> Some (Exc v) | None -> None)
    with
      | Not_found -> None
  in
  s

type v = t

let raw s = Var s

(* -------------------------------------------------------------------------- *)

module Scanner = struct

  type t = v

  open Sedlexing

  exception ParseError of string
  let letter = [%sedlex.regexp? 'a' .. 'z'|'A' .. 'Z']
  let digit = [%sedlex.regexp? '0' .. '9']
  (* let var = [%sedlex.regexp? (letter | '_' | '<'), *)
  (*            Star (letter | Chars "_.:<>" | digit)] *)
  let var = [%sedlex.regexp? (letter | Chars "_$@"),
             Star (letter | Chars "_.:$@" | digit)]

  let sub_ buf prefix_length =
    Utf8.sub_lexeme buf
      (prefix_length + 1) (lexeme_length buf - prefix_length - 1)

  let subparen buf prefix_length =
    Utf8.sub_lexeme buf
      (prefix_length + 1) (lexeme_length buf - prefix_length - 2)

  let subparen' buf prefix_length =
    Utf8.sub_lexeme buf
      (prefix_length + 1) (lexeme_length buf - prefix_length - 3)

  let splitat buf length_first =
    Utf8.sub_lexeme buf 0 length_first, sub_ buf length_first

  let rec maybe_prime buf p x = match%sedlex buf with
    | "'" -> maybe_prime buf p (Prime x)
    | ")" -> (match p with
        | [] -> raise @@ ParseError (Utf8.lexeme buf)
        | `Ret :: pl -> maybe_prime buf pl (Ret x)
        | `Exc :: pl -> maybe_prime buf pl (Exc x))
    | eof -> (match p with
        | [] -> x
        | _ -> raise @@ ParseError (Utf8.lexeme buf ^ ": missing `)'"))
    | _ -> raise @@ ParseError (Utf8.lexeme buf)

  let ( $~ ) a b = match AliasRelVar.make () a b with
    | AliasRelVar.Value v -> AliasRel v
    | _ -> failwith @@ asprintf "Invalid alias variable (`%s', `%s')" a b
  let ( $>< ) a b = match ShareRelVar.make () a b with
    | ShareRelVar.Value v -> ShareRel v
    | _ -> failwith @@ asprintf "Invalid sharing variable (`%s', `%s')" a b
  let ( $.~> ) a b = FieldAliasRel (FieldAliasRelVar.make a b)
  let ( $.>< ) a b = FieldShareRel (FieldShareRelVar.make a b)

  let scan s =                     (* MUST match formats in the modules above *)
    let rec lexer p buf = match%sedlex buf with
      | "ok(", var, ")" -> Guard (subparen buf 2) |> maybe_prime buf p
      | "level(", Opt var, ")" -> Level (subparen buf 5) |> maybe_prime buf p
      | "taint(", Opt var, ")" -> Taint (subparen buf 5) |> maybe_prime buf p
      | "obj_level(", Opt var, ")" -> HLevel (subparen buf 9) |> maybe_prime buf p
      | "obj_taint(", Opt var, ")" -> HTaint (subparen buf 9) |> maybe_prime buf p
      | Opt var, '~', Opt var ->
          Scanf.sscanf (Utf8.lexeme buf) "%s@~%s" ( $~ ) |> maybe_prime buf p
      | Opt var, ".><", Opt var ->
          Scanf.sscanf (Utf8.lexeme buf) "%s@.><%s" ($.><) |> maybe_prime buf p
      | Opt var, "><", Opt var ->
          Scanf.sscanf (Utf8.lexeme buf) "%s@><%s" ( $>< ) |> maybe_prime buf p
      | Opt var, ".~>", Opt var ->
          Scanf.sscanf (Utf8.lexeme buf) "%s@.~>%s" ($.~>) |> maybe_prime buf p
      | var -> Var (Utf8.lexeme buf) |> maybe_prime buf p
      | "ret(" -> lexer (`Ret :: p) buf
      | "exc(" -> lexer (`Exc :: p) buf
      | _ -> raise @@ ParseError (Utf8.lexeme buf)
    in
    try lexer [] (Utf8.from_string s) with
      | ParseError _ -> Var s
  ;;

  let scanx s =                                                       (* ibid *)
    let rec lexer buf = match%sedlex buf with
      | "prime_" -> Prime (lexer buf)
      | "ret_" -> Ret (lexer buf)
      | "exc_" -> Exc (lexer buf)
      | "var_", var -> Var (sub_ buf 3)
      | "ok_", var -> Guard (sub_ buf 2)
      | "level_", Opt var -> Level (sub_ buf 5)
      | "taint_", Opt var -> Taint (sub_ buf 5)
      | "obj_level_", Opt var -> HLevel (sub_ buf 9)
      | "obj_taint_", Opt var -> HTaint (sub_ buf 9)
      | "alias_", Plus digit, '_' ->
          subparen buf 5 |> int_of_string |> var_pair buf ( $~ )
      | "share_", Plus digit, '_' ->
          subparen buf 5 |> int_of_string |> var_pair buf ( $>< )
      | "field_alias_", Plus digit, '_' ->
          subparen buf 11 |> int_of_string |> var_pair buf ( $.~> )
      | "field_share_", Plus digit, '_' ->
          subparen buf 11 |> int_of_string |> var_pair buf ( $.>< )
      | _ -> raise @@ ParseError (Utf8.lexeme buf)
    and var_pair buf op i = match%sedlex buf with
      | Opt var, '_', Opt var -> let a, b = splitat buf i in op a b
      | _  -> raise @@ ParseError (Utf8.lexeme buf)
    in
    try lexer (Utf8.from_string s) with
      | ParseError _ -> Var s
  ;;

  module Tests = struct
    let a_ab = match AliasRelVar.make () "a" "b" with
      | AliasRelVar.Value ab -> ab
      | _ -> assert false;;
    let a_a_ = match AliasRelVar.make () "a" "" with
      | AliasRelVar.Value a_ -> a_
      | _ -> assert false;;
    let a__ab = match ShareRelVar.make () "a" "b" with
      | ShareRelVar.Value ab -> ab
      | _ -> assert false;;
    let a__a_ = match ShareRelVar.make () "a" "" with
      | ShareRelVar.Value a_ -> a_
      | _ -> assert false;;
    let fa_ab = FieldAliasRelVar.make "a" "b_c";;
    let fa_a_ = FieldAliasRelVar.make "a" "";;
    let fa__a = FieldAliasRelVar.make "" "a";;

    let check scan print x = equal (scan (asprintf "%a" print x)) x;;

    assert (check scan print (Var "a"));;
    assert (check scan print (Guard "foo"));;
    assert (check scan print (Level "this"));;
    assert (check scan print (Taint "this"));;
    assert (check scan print (Prime (Guard "foo")));;
    assert (check scan print (Prime (Prime (HLevel "bar"))));;
    assert (check scan print (Prime (Prime (HTaint "bar"))));;
    assert (check scan print (Prime (Prime (Ret (HLevel "bar")))));;
    assert (check scan print (Prime (Prime (Ret (HTaint "taint")))));;
    assert (check scan print (AliasRel a_ab));;
    assert (check scan print (AliasRel a_a_));;
    assert (check scan print (ShareRel a__ab));;
    assert (check scan print (ShareRel a__a_));;
    assert (check scan print (Prime (FieldAliasRel fa_ab)));;
    assert (check scan print (Prime (Ret (FieldAliasRel fa_ab))));;
    assert (check scan print (Prime (Ret (FieldAliasRel fa_a_))));;
    assert (check scan print (Prime (Exc (Ret (FieldAliasRel fa_ab)))));;
    assert (check scan print (Prime (Ret (Exc (FieldAliasRel fa_a_)))));;
    assert (check scan print (Ret (FieldAliasRel fa__a)));
    assert (check scan print (Exc (FieldAliasRel fa__a)));
    (* assert (check scan print (Prime (Guard "java.lang.Object:<init>")));; *)
    assert (check scan print (Prime (Guard "java.lang.Object:java.lang.Object")));;

    assert (check scanx printx (Var "a"));;
    assert (check scanx printx (Guard "foo"));;
    assert (check scanx printx (Prime (Guard "foo")));;
    assert (check scanx printx (Prime (Prime (HLevel "bar"))));;
    assert (check scanx printx (Prime (Prime (HTaint "bar"))));;
    assert (check scanx printx (Prime (Prime (Ret (HLevel "bar")))));;
    assert (check scanx printx (Prime (Prime (Ret (HTaint "taint")))));;
    assert (check scanx printx (AliasRel a_ab));;
    assert (check scanx printx (AliasRel a_a_));;
    assert (check scanx printx (ShareRel a__ab));;
    assert (check scanx printx (ShareRel a__a_));;
    assert (check scanx printx (Prime (FieldAliasRel fa_ab)));;
    assert (check scanx printx (Prime (Ret (FieldAliasRel fa_ab))));;
    assert (check scanx printx (Prime (Ret (FieldAliasRel fa_a_))));;
    assert (check scanx printx (Prime (Exc (Ret (FieldAliasRel fa_ab)))));;
    assert (check scanx printx (Prime (Ret (Exc (FieldAliasRel fa_a_)))));;
    assert (check scanx printx (Ret (FieldAliasRel fa__a)));
    assert (check scanx printx (Exc (FieldAliasRel fa__a)));
    (* assert (check scanx printx (Prime (Guard "java.lang.Object:<init>")));; *)
    assert (check scanx printx (Prime (Guard "java.lang.Object:java.lang.Object")));;
  end
end

(* -------------------------------------------------------------------------- *)
