open Format
open Rutils
open Utils
open Var
open Base
open OOLang
open OOMeth
open OOSign

(* --- *)

let level = Log.Debug
let logger = Log.mk ~level "SecSum"

(* --- *)

module type VAR_TYPS = sig include VAR_TYPS val lpc: lexp end

(* --- *)

module Make
  (Env: SCFG.ENV with type Var.t = Var.t)
  =
struct
  module H = Heuristics.Exprs (Env) (Heuristics.DefaultOptions)
  open Env
  open H

  module CVMap = MakeMap (AnyVar)

  (* TODO: parameterize with Sign.callable | Sign.implicit *)
  type secsum =
      {
        sign: decl;
        guard: bexp;
        bindings: exp VarMap.t;
        policies: policies;
        (* custom_supp: custom_support; *)
        features: Features.Record.t;
      }
  and policies = Var.t Policy.Map.t
  type t = secsum

  let no_policies: policies = Policy.Map.empty

  exception MissingGuard of decl
  exception MalformedGuard of decl
  exception MalformedLevel of decl
  exception MalformedHLevel of decl
  exception MalformedBinding of t * Var.t

  let pp_shortname fmt { sign } =
    Sign.Decl.print_shortname fmt sign
  let throws { sign } = not (Typ.UserTyps.is_empty (Decl.throws sign))

  let methname = asprintf "%a" Sign.Decl.print_shortname
  let guardvar' s = Guard (methname s)
  let guardvar { sign } = guardvar' sign

  let guard' s gvar bindings =
    (match VarMap.find gvar bindings with
      | Bexp guard -> guard
      | _ -> raise @@ MalformedGuard s
      | exception Not_found -> raise @@ MissingGuard s)

  let guard { guard } = guard
  let features { features } = features
  let policies { policies } = policies

  let make features ?(policies = Policy.Map.empty) s bindings =
    let gvar = guardvar' s in
    {
      sign = s;
      guard = guard' s gvar bindings;
      bindings = VarMap.remove gvar bindings;
      policies;
      features;
    }

  let print_bindings =
    VarMap.print' ~empty:"" ~left:"" ~right:";" ~sep:";@\n" ~assoc:" = "
      pp_exp

  let print_policies =
    Policy.Map.print' ~empty:"" ~left:"" ~right:";" ~sep:";@\n"
      ~assoc:": " ~rev_assoc:true
      Var.print

  let print fmt { sign; guard; bindings; policies } =
    pp fmt "@[@[<2>%a {" Sign.Decl.print sign;
    if not (Policy.Map.is_empty policies)
    then pp fmt "@\n%a" print_policies policies;
    pp fmt "@\nguard = @[%a@];" pp_bexp guard;
    if not (VarMap.is_empty bindings)
    then pp fmt "@\n%a" print_bindings bindings;
    pp fmt "@]@\n}@]"

  let subst_in_exprs f s =
    { s with
      guard = Bexp.subst f s.guard;
      bindings = VarMap.map (Exp.subst f) s.bindings }

  let subst_vars_in_exprs f s =
    { s with
      guard = Bexp.subst_vars f s.guard;
      bindings = VarMap.map (Exp.subst_vars f) s.bindings }

  let fold_raw_bindings f { bindings } = VarMap.fold f bindings
  let fold_ret_bindings f t = fold_raw_bindings begin function
    | Ret v -> f v
    | Exc _ | Guard _ -> fun _ -> ign
    | x -> raise @@ MalformedBinding (t, x)
  end t
  let fold_exc_bindings f t = fold_raw_bindings begin function
    | Exc v -> f v
    | Ret _ | Guard _ -> fun _ -> ign
    | x -> raise @@ MalformedBinding (t, x)
  end t

  (* --- *)

  let remap_policies policies =
    Policy.Set.fold begin fun p (acc, i) ->
      Policy.Map.add p (Var (asprintf "p%i" i: AnyVar.t)) acc, succ i
    end policies (Policy.Map.empty, 0) |> fst

  let merge_policy_maps (lst: policies list) =
    let n = List.fold_left begin fun acc x ->
      Policy.Map.fold (fun p _ -> Policy.Set.add p) x acc
    end Policy.Set.empty lst |> remap_policies in
    let transmap m = Policy.Map.fold begin fun p v ->
      VarMap.add v (Policy.Map.find p n)
    end m VarMap.empty in
    (n : policies), List.map transmap lst

  let unify_policies (ss: t list) =
    let policies, ts = merge_policy_maps (List.map (fun s -> s.policies) ss) in
    policies,
    List.map2 (fun t -> subst_vars_in_exprs (function
      | Var _ as v -> (try Some (VarMap.find v t) with Not_found -> None)
      | _ -> None)) ts ss

  (* --- *)

  include (ScgsUtils.IO.Bin (struct
    type t = secsum
    let functional = false
  end) : ScgsUtils.IO.BINARY with type t := t)

  (* --- *)

  module type SET = sig
    include Sign.EXTENDED_SET with type elt = secsum
    include ScgsUtils.IO.BINARY with type t := t
  end

  module Set (Lookup: Sign.LOOKUP) = struct
    include Sign.MakeExtendedSet (Lookup) (struct
      type t = secsum
      module K = Typ.UserTyp
      let print = print
      let sign { sign } = Decl.sign sign
    end)
    include (ScgsUtils.IO.Bin (struct
      type t' = t
      type t = t'
      let functional = false
    end) : ScgsUtils.IO.BINARY with type t := t)
  end

  (* --- *)

  module Binding
    (HeapSubst: SymbHeap.SUBST with module Env := Env)
    =
  struct

    open Bexp.Ops
    open Lexp.Ops

    let default_merge_func _ a b = match a, b with
      | Bexp a, Bexp b -> Some (Bexp (a ||. b))
      | Lexp a, Lexp b -> Some (Lexp (a |.| b))
      | _ -> failwith "cannot merge expressions"
    let merge_bindings ?(merge_func = default_merge_func) =
      VarMap.union merge_func

    (* --- *)

    open Stm.Ops

    let subst_n_elim_in_policies map elim (policies: policies) : policies * _ =
      Policy.Map.fold (fun p v (m, t) ->
        let p' = Policy.T.subst map p in
        let p' = Policy.T.elim elim p' in
        try m, VarMap.add v (Policy.Map.find p' m, Policy.dom p') t with
          | Not_found -> Policy.Map.add p' v m, t)
        policies (Policy.Map.empty, VarMap.empty)

    let subst_guard subst v = match !^(subst v) with
      | Stm.V v -> Bexp.var (Guard v)
      | Stm.L _ -> failwith "Unexpected literal in guard variable"

    let subst_level subst v = match !^(subst v) with
      | Stm.V v -> Lexp.var (Level v)
      | Stm.L _ -> Lexp.bottom

    let subst_taint subst v = match !^(subst v) with
      | Stm.V v -> Bexp.var (Taint v)
      | Stm.L _ -> Bexp.ff

    let bool_var = function
      | Guard _ | Taint _ -> true
      | v -> HeapSubst.bool_comp v

    let level_var = function
      | Level _ -> true
      | v -> HeapSubst.level_comp v

    let typed_subst form_subst eff_subst var_subst =
      let bsubst k v = match subst form_subst v, k with
        | None, _ -> `None
        | Some v, `Ret -> `Bexp (Bexp.var (Ret v))
        | Some v, `Exc -> `Bexp (Bexp.var (Exc v))
      and lsubst k v = match subst form_subst v, k with
        | None, _ -> `None
        | Some v, `Ret -> `Expr (`Lexp (Lexp.var (Ret v)))
        | Some v, `Exc -> `Expr (`Lexp (Lexp.var (Exc v)))
      in
      function
        | Guard v -> `Bexp (subst_guard eff_subst v)
        | Level v -> `Expr (`Lexp (subst_level eff_subst v))
        | Taint v -> `Bexp (subst_taint eff_subst v)
        | Ret v when bool_var v -> bsubst `Ret v
        | Exc v when bool_var v -> bsubst `Exc v
        | Ret v when level_var v -> lsubst `Ret v
        | Exc v when level_var v -> lsubst `Exc v
        | v -> HeapSubst.subst_eff eff_subst v

    (* --- *)

    let bind: (callable_decl -> Stm.immediate argmap) -> Base.Vars.t -> t -> t = fun dmap e s ->
      let { sign; guard; bindings; policies } = s in
      let { formal2eff } = match sign with
        | Sign.C sign -> dmap sign
        | Sign.I _ -> { formal2eff = Base.VarMap.empty }
      in
      let policies, cvmd = subst_n_elim_in_policies formal2eff e policies in
      (* Log.i logger "Binding %a with %a." Sign.Decl.print s.sign *)
      (*   (Base.VarMap.print Stm.print_immediate) formal2eff; *)
      let form_arg_subst a = match !^(Base.VarMap.find a formal2eff) with
        | Stm.V v -> v
        | Stm.L l -> asprintf "%a" Base.print_lit l
      in
      let typed_subst = typed_subst form_arg_subst
        begin fun a -> try Base.VarMap.find a formal2eff with
          (* XXX: a bit hard-coded... *)
          | Not_found when Base.Var.equal a "" -> Stm.V a
          | Not_found as e ->
              Log.e logger "@[Unbound@ formal@ argument@ `%s'.@ \
                              Signature:@]@\n%a" a print s;
              raise e
        end
        begin fun v -> match VarMap.find (Var v) cvmd with
          | v', Level -> `Expr (`Lexp (Lexp.var v'))
          | v', Taint -> `Bexp (Bexp.var v')
          | exception Not_found -> `None
        end
      in
      let subst_bnd m = function
        | Ret v | Exc v as x -> (match VarMap.find x m with
            | Bexp e -> `Bexp e
            | Lexp e -> `Expr (`Lexp e)
            | Eexp e -> `Expr (`Eexp e))
        | _ -> `None
      in
      let guard = Bexp.subst typed_subst guard in
      let bindings = begin VarMap.empty
          |> fold_ret_bindings begin fun v e ->
            (* Use default merge to accomodate surjective substitutions *)
            VarMap.singleton
              (let v = Ret v in
               match subst form_arg_subst v with None -> v | Some v -> v)
              (Exp.subst typed_subst e) |> merge_bindings
          end s
          |> fold_exc_bindings begin fun v e ->
            VarMap.singleton
              (let v = Exc v in
               match subst form_arg_subst v with None -> v | Some v -> v)
              (Exp.subst typed_subst e) |> merge_bindings
          end s
      end in
      let bindings = VarMap.fold begin fun v e m ->
        VarMap.add v (Exp.subst (subst_bnd m) e) m
      end bindings bindings in
      { s with sign = Decl.subst_args form_arg_subst sign;
        guard; bindings; policies; }

    let merge ?merge_func merge_decls s s' =
      let policies, ss' = unify_policies [s; s'] in
      let s = List.hd ss' and s' = List.nth ss' 1 in
      { sign = merge_decls s.sign s'.sign;
        guard = s.guard &&. s'.guard;
        bindings = merge_bindings ?merge_func s.bindings s'.bindings;
        policies;
        features = Features.Record.merge s.features s'.features; }

    let bind_merge ?merge_func
        : (decl -> decl -> decl)
        -> (callable_decl -> Stm.immediate argmap) -> Base.Vars.t -> t list -> t =
      fun mdecls dmap elim -> function
        | [] -> raise @@ Invalid_argument "empty list"
        | s :: tl
          -> let bind = bind dmap elim and merge = merge ?merge_func mdecls in
            List.fold_left (fun s s' -> merge s (bind s')) (bind s) tl

  end

  (* --- *)

  module Forging
    (FeatureFilters: Features.FILTERS)
    (S: Semantics.S)
    (VarTyps: VAR_TYPS with type lexp := Env.lexp
                       and type texp := Env.bexp)
    (HeapForge: SymbHeap.FORGE with module Env := Env)
    =
  struct
    module Vars = Base.Vars
    open FeatureFilters
    open VarMap
    open SecStub
    open VarTyps
    open Lexp
    open Bexp
    open S
    open H

    module Heap = HeapForge.Make (FeatureFilters) (S)
    open Heap

    (* --- *)

    let lpc = if Features.track_levels = AllFlows then lpc else bottom

    (* --- *)

    let fold_vars s f = Vars.fold f s
    let fold_base_vars s f = Base.VarMap.fold f s

    let acc_all_prm_args (prms, _) f = fold_base_vars prms f
    let acc_all_ref_args (_, refs) f = fold_base_vars refs f

    let acc_all_arg_levels args = bottom
      |>~ acc_all_prm_args args (fun v _ -> (|.|) ~~v)
      |>~ acc_all_ref_args args (fun r _ l -> l |.| ~~r |.| ~^r)
    let acc_all_arg_taints args = ff
      |>? acc_all_prm_args args (fun v _ -> (||.) ?~v)
      |>? acc_all_ref_args args (fun r _ t -> t ||. ?~r ||. ?^r)
    let guard_all_args ?(level = bottom) ?(taint = ff) args = tt
      |>~ (&&.) (acc_all_arg_levels args <=.~ level)
      |>? (&&.) (acc_all_arg_taints args =>. taint)

    let toplevel _r = Lexp.top
    let idlevel r = ~^r
    and instlevel d l r = if Base.Var.equal r instvar then l else d r

    let toptaint _r = tt
    let idtaint r = ?^r
    and insttaint d l r = if Base.Var.equal r instvar then l else d r

    let guard_output args = function
      | Some level when Features.track_levels <> NoFlows
          -> (lpc <=.~ level) &&. guard_all_args ~level args
      | _ -> tt

    let args = function
      | C decl -> FCall.arg_prms_refs (FCall.call decl)
      | I decl -> Base.VarMap.empty, Base.VarMap.empty

    let make decl guard
        ?(policies = Policy.Map.empty)
        ?(level = bottom) (* base level/taint of returned primitive/reference *)
        ?(taint = ff)
        heap_bindings =
      let bind_ret_level = VarMap.add (Ret (Level "")) (Lexp level)
      and bind_exc_level = VarMap.add (Exc (Level "")) (Lexp level)
      and bind_ret_taint = VarMap.add (Ret (Taint "")) (Bexp taint)
      and bind_exc_taint = VarMap.add (Exc (Taint "")) (Bexp taint) in
      let bindings = begin heap_bindings
          |>~ (if ret_some decl then bind_ret_level else ign)
          |>!~ bind_exc_level
          |>? (if ret_some decl then bind_ret_taint else ign)
          |>!? bind_exc_taint
      end in
      { sign = decl; guard; bindings; policies; features = FeatureFilters.record; }

    type _ pol =
      | L : Lexp.t pol
      | T : Bexp.t pol

    let genpol:
    type k. _ -> k pol -> _ -> 'a -> k * 'a = fun decl ->
      let mkgenpol dom name (pols, i) =
        let p = Policy.make (Decl.clazz decl) name dom
          (List.map (fun t -> Policy.Formal t) (Decl.args decl)) in
        let v = Var (asprintf "p%i" i) in
        v, (Policy.Map.add p v pols, succ i)
      in
      fun d p pols -> match d, p with
        | L, (`Level name | `Both (name, _))
              when Features.track_levels <> NoFlows ->
            let var, pols = mkgenpol Level name pols in Lexp.var var, pols
        | L, _ -> bottom, pols
        | T, (`Taint name | `Both (_, name))
              when Features.track_taints ->
            let var, pols = mkgenpol Taint name pols in Bexp.var var, pols
        | T, _ -> ff, pols

    let of_secstub ms =
      let sign = sign ms and throws = throws ms in
      let decl = C (Decl.of_sign ~throws sign) in
      let args = args decl in
      let args_excpt_base = app_pair
        (Base.VarMap.remove instvar) (Base.VarMap.remove instvar) args in
      let retdom = ret_refdom decl in
      let arg_levels = acc_all_arg_levels args in
      let arg_taints = acc_all_arg_taints args in
      let arg_levels' = acc_all_arg_levels args_excpt_base in
      let arg_taints' = acc_all_arg_taints args_excpt_base in
      let guard = guard_output args (app_opt of_raw ms.output) in
      let taints_guard = match ms.kind with
        | Constructor { trst = `None }
        | VirtMeth { trst = `None }
        | StaticMeth { trst = `None }
          -> tt
        | Constructor { trst = `Args }
        | StaticMeth { trst = `Args }
        | VirtMeth { trst = `Args }
          -> !. arg_taints
        | VirtMeth { trst = `Fields }
          -> !. ?^instvar
      in
      let guard = guard &&. taints_guard in
      let may_mut_arg_fields = ms.mutarg <> None in
      let pols = no_policies, 0 in
      let ref_arg_level_res, pols = match ms.mutarg with
        | Some (`FromArgs | `Tag TT) -> (fun _ -> arg_levels |.| lpc), pols
        | Some (`Tag (TL l | TLT l)) -> (fun _ -> of_raw l |.| lpc), pols
        | Some (`Policy p) -> let x, pols = genpol decl L p pols in
                             (fun _ -> x |.| lpc), pols
        | None -> idlevel, pols
      in
      let ref_arg_taint_res, pols = match ms.mutarg with
        | Some (`FromArgs | `Tag (TL _)) -> (fun _ -> arg_taints), pols
        | Some (`Tag (TT | TLT _)) -> toptaint, pols
        | Some (`Policy p) -> let x, pols = genpol decl T p pols in
                             (fun _ -> x), pols
        | None -> idtaint, pols
      in
      let ref_arg_level_res, pols = match ms.kind with
        | Constructor { mutobj = Some `FromArgs }
        | VirtMeth { mutobj = Some `FromArgs }
          -> instlevel ref_arg_level_res (arg_levels |.| lpc), pols
        | Constructor { mutobj = Some (`Tag (TL l | TLT l)) }
        | VirtMeth { mutobj = Some (`Tag (TL l | TLT l)) }
          -> instlevel ref_arg_level_res (of_raw l |.| lpc), pols
        | Constructor { mutobj = None }
        | VirtMeth { mutobj = None }
          -> instlevel ref_arg_level_res ~^instvar, pols
        | Constructor { mutobj = Some (`Policy p) }
        | VirtMeth { mutobj = Some (`Policy p) }
          -> let x, pols = genpol decl L p pols in
            instlevel ref_arg_level_res (x |.| lpc), pols
        | _
          -> ref_arg_level_res, pols
      in
      let ref_arg_taint_res, pols = match ms.kind with
        | Constructor { mutobj = Some `FromArgs }
        | VirtMeth { mutobj = Some `FromArgs }
          -> insttaint ref_arg_taint_res arg_taints, pols
        | Constructor { mutobj = Some (`Tag (TT | TLT _)) }
        | VirtMeth { mutobj = Some (`Tag (TT | TLT _)) }
          -> insttaint ref_arg_taint_res tt, pols
        | Constructor { mutobj = None }
        | VirtMeth { mutobj = None }
          -> insttaint ref_arg_taint_res ?^instvar, pols
        | Constructor { mutobj = Some (`Policy p) }
        | VirtMeth { mutobj = Some (`Policy p) }
          -> let x, pols = genpol decl T p pols in
            insttaint ref_arg_taint_res x, pols
        | _
          -> ref_arg_taint_res, pols
      in
      let level_n_taint = function
        | TT -> lpc, tt
        | TL l -> of_raw l |.| lpc, ff
        | TLT l -> of_raw l |.| lpc, tt
      in
      let level_n_taint_from_deps ~virt pols = function
        | `None -> (bottom, ff), pols
        | `Args -> (arg_levels', arg_taints'), pols
        | `DotStar -> (~^instvar, ?^instvar), pols
        | `Any -> (arg_levels, arg_taints), pols
        | `Policy p -> let l, pols = genpol decl L p pols in
                      let t, pols = genpol decl T p pols in
                      (l, t), pols
      in
      let level_n_taint_from_args tagging args_only =
        if args_only
        then
          (lpc |.| arg_levels, arg_taints)
        else
          let l, t = level_n_taint tagging in
          (l  |> acc_all_prm_args args (fun v _ -> (|.|) ~~v)
              |> acc_all_ref_args args (fun v _ -> (|.|) (ref_arg_level_res v))),
          (t  |> acc_all_prm_args args (fun v _ -> (||.) ?~v)
              |> acc_all_ref_args args (fun v _ -> (||.) (ref_arg_taint_res v)))
      in
      let (dlevel, dtaint), pols = match ms.kind with
        | VirtMeth { rslt = (`FreshPrim (_, _, deps) | `New (_, _, deps)) }
          -> level_n_taint_from_deps ~virt:true pols deps
        | StaticMeth { rslt = (`FreshPrim (_, _, deps) | `New (_, _, deps)) }
          -> level_n_taint_from_deps ~virt:false pols deps
        | _
          -> (bottom, ff), pols
      in
      let make = make decl guard ~policies:(fst pols) in
      let setup_heap =
        Heap.setup ~ref_arg_level_res ~ref_arg_taint_res ~may_mut_arg_fields in
      match ms.kind with
        | Constructor _
          -> make (setup_heap args
                    ~exc_arg_alias: false)
        | VirtMeth { rslt = `Unit }
        | StaticMeth { rslt = `Unit }
          -> make (setup_heap args)
        | VirtMeth { rslt = `FreshPrim (_, tagging, _) }
        | StaticMeth { rslt = `FreshPrim (_, tagging, _) }
          -> let level, taint = level_n_taint tagging in
            let level = level |.| dlevel and taint = taint ||. dtaint in
            make ~level ~taint (setup_heap ?retdom args)
        | VirtMeth { rslt = `Prim (_, tagging, args_only) }
        | StaticMeth { rslt = `Prim (_, tagging, args_only) }
          -> let level, taint = level_n_taint_from_args tagging args_only in
            make ~level ~taint (setup_heap ?retdom args)
        | VirtMeth { rslt = `New (_, tagging, _) }
        | StaticMeth { rslt = `New (_, tagging, _) }
          -> let level, taint = level_n_taint tagging in
            make ~level ~taint
              (setup_heap ?retdom args
                 ~hlevel:(level |.| dlevel) ~htaint:(taint ||. dtaint)
                 ~ret_arg_field_alias_to: may_mut_arg_fields
                 ~exc_arg_field_alias_to: may_mut_arg_fields
                 ~ret_arg_field_share_to: may_mut_arg_fields
                 ~exc_arg_field_share_to: may_mut_arg_fields
                 ~ret_arg_alias: false
                 ~exc_arg_alias: false)
        | VirtMeth { rslt = `Any (_, tagging) }
        | StaticMeth { rslt = `Any (_, tagging) }
          -> let level, taint = level_n_taint_from_args tagging false in
            make ~level ~taint
              (setup_heap ?retdom args
                 ~hlevel:level ~htaint:taint
                 ~ret_arg_field_alias_to: may_mut_arg_fields
                 ~exc_arg_field_alias_to: may_mut_arg_fields
                 ~ret_arg_field_share_to: may_mut_arg_fields
                 ~exc_arg_field_share_to: may_mut_arg_fields)
        | VirtMeth { rslt = `Arg _ }
        | StaticMeth { rslt = `Arg _ }
          -> let level, taint = arg_levels |.| lpc, arg_taints in
            make ~level ~taint
              (setup_heap ?retdom args
                 ~hlevel:level ~htaint:taint
                 ~ret_arg_field_alias_to: may_mut_arg_fields
                 ~exc_arg_field_alias_to: may_mut_arg_fields
                 ~ret_arg_field_share_to: may_mut_arg_fields
                 ~exc_arg_field_share_to: may_mut_arg_fields)
        | VirtMeth { rslt = (`RField (_, tagging) | `PField (_, tagging)) }
          -> let level, taint = level_n_taint tagging in
            let level = ref_arg_level_res instvar |.| level
            and taint = ref_arg_taint_res instvar ||. taint in
            make ~level ~taint
              (setup_heap ?retdom args
                 ~hlevel:level ~htaint:taint
                 ~ret_arg_alias: false
                 ~exc_arg_alias: false
                 (* ~ret_arg_field_alias_to: true *)
                 (* ~exc_arg_field_alias_to: true *)
                 (* ~ret_arg_field_share_to: true *)
                 (* ~exc_arg_field_share_to: true *))

    let third_party decl =
      let decl = add_throw decl thrbltyp in
      let args = args decl in
      let guard = guard_output args (Some bottom) in
      make decl guard ~level:top ~taint:tt
        (Heap.setup ?retdom:(ret_refdom decl) args
           ~hlevel:top ~htaint:tt
           ~ref_arg_level_res:toplevel
           ~ref_arg_taint_res:toptaint
           ~may_mut_arg_fields: true)

    let nice decl =
      let args = args decl in
      let level = acc_all_arg_levels args in
      let taint = acc_all_arg_taints args in
      make decl tt ~level ~taint
        (Heap.setup ?retdom:(ret_refdom decl) args
           ~hlevel:level ~htaint:taint
           ~ret_arg_alias: false
           ~ret_arg_field_alias_to: false
           ~ret_arg_field_alias_from: false
           ~ret_arg_field_share_to: false
           ~ret_arg_field_share_from: false
           ~exc_arg_alias: false
           ~exc_arg_field_alias_to: false
           ~exc_arg_field_alias_from: false
           ~exc_arg_field_share_to: false
           ~exc_arg_field_share_from: false
           ~ref_arg_level_res: idlevel
           ~ref_arg_taint_res: idtaint
           ~may_mut_arg_fields: false)

    let bottom decl =
      make decl tt ~level:bottom ~taint:ff
        (Heap.setup ?retdom:(ret_refdom decl) (args decl)
           ~hlevel:bottom ~htaint:ff
           ~ret_arg_alias: false
           ~ret_arg_field_alias_to: false
           ~ret_arg_field_alias_from: false
           ~ret_arg_field_share_to: false
           ~ret_arg_field_share_from: false
           ~exc_arg_alias: false
           ~exc_arg_field_alias_to: false
           ~exc_arg_field_alias_from: false
           ~exc_arg_field_share_to: false
           ~exc_arg_field_share_from: false
           ~ref_arg_level_res: idlevel
           ~ref_arg_taint_res: idtaint
           ~may_mut_arg_fields: false)

    module OfStubs (SecSums: SET) = struct
      let secsums methsums =
        Ok (List.fold_left (fun s ms -> SecSums.insert (of_secstub ms) s)
              SecSums.empty methsums)
    end

  end

end

(* -------------------------------------------------------------------------- *)
