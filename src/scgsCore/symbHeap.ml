open Format
open Rutils
open Utils
open Base
module V = Var                                                       (* alias *)
open OOLang
open OOBase
open V

(* --- *)

let level = Log.Debug
let logger = Log.mk ~level "AH"

(* -------------------------------------------------------------------------- *)

module Stats = struct
  open BasicStats.Outputs

  type 'n cardinals =
      {
        nb_typ_vars: 'n;
        nb_mut_refs: 'n;
        nb_const_refs: 'n;
        nb_mut_rel_vars: 'n;
        nb_const_rel_vars: 'n;       (* typically: args related, i.e, const w.r.t
                                       method, not in the model *)
      }

  let print_cardinals ?(pk = ps) ?k0 pe fmt
      { nb_typ_vars; nb_mut_refs; nb_const_refs;
        nb_mut_rel_vars; nb_const_rel_vars } =
    let pk = pk ?k0 in
    pp fmt "@[<2>%a: %a@],@;\
            @[<2>%a: %a@],@;@[<2>%a: %a@],@;\
            @[<2>%a: %a@],@;@[<2>%a: %a@]"
      pk "nb_symb_heap_typ_vars" pe nb_typ_vars
      pk "nb_symb_heap_mut_refs" pe nb_mut_refs
      pk "nb_symb_heap_const_refs" pe nb_const_refs
      pk "nb_symb_heap_mut_rel_vars" pe nb_mut_rel_vars
      pk "nb_symb_heap_const_rel_vars" pe nb_const_rel_vars

  let map_cardinals f { nb_typ_vars; nb_mut_refs; nb_const_refs;
                        nb_mut_rel_vars; nb_const_rel_vars } =
    { nb_typ_vars       = f nb_typ_vars;
      nb_mut_refs       = f nb_mut_refs;
      nb_const_refs     = f nb_const_refs;
      nb_mut_rel_vars   = f nb_mut_rel_vars;
      nb_const_rel_vars = f nb_const_rel_vars; }

  let acc_cardinals f a b =
    { nb_typ_vars       = f a.nb_typ_vars       b.nb_typ_vars;
      nb_mut_refs       = f a.nb_mut_refs       b.nb_mut_refs;
      nb_const_refs     = f a.nb_const_refs     b.nb_const_refs;
      nb_mut_rel_vars   = f a.nb_mut_rel_vars   b.nb_mut_rel_vars;
      nb_const_rel_vars = f a.nb_const_rel_vars b.nb_const_rel_vars; }

end
open Stats

(* -------------------------------------------------------------------------- *)

module type SUPPORT_VARS = sig
  module Sem: Semantics.S
  val refs: Sem.refdom VarMap.t
  val args: Sem.domain VarMap.t
  val retdom: Sem.refdom option
  val ( ~@? ) : ('a -> 'a) -> 'a -> 'a                       (* app if Regions_m ≠ ∅ *)
end

module type SUBST = sig
  module Env: SCFG.ENV
  val bool_comp: V.t -> bool
  val level_comp: V.t -> bool
  val captured_comp: V.t -> bool
  val subst_eff
    : (V.AnyVar.t -> OOLang.Stm.immediate)
    -> (Env.Var.t, Env.Bexp.t, [>`Lexp of Env.Lexp.t]) Exprs.subst_func
end

module type T = sig
  module Env: SCFG.ENV
  open Typ
  open Env
  open SCFG
  open Updates

  include OBJ_TYPS with type texp = bexp and type lexp := lexp
  val mutable_support: typ VarMap.t
  val const_support: typ VarMap.t
  val local_support: VarSet.t
  val footprint: typ VarMap.t
  val fold_footprint_comps: (V.t -> 'a -> 'a) -> 'a -> 'a
  val acc_init_bindings: exp VarMap.t -> exp VarMap.t
  val arg_alias_invariant: bexp
  val arg_alias_invariant_support_size: int
  include SUBST with module Env := Env

  val acc_arg_related_footprint_constraints
    : comp:(V.t -> V.t) -> bexp -> bexp
  val acc_ret_related_footprint_constraints
    : comp:(V.t -> V.t) -> var option -> lexp option -> texp option -> bexp -> bexp
  val acc_exc_related_footprint_constraints
    : comp:(V.t -> V.t) -> var -> lexp -> texp -> bexp -> bexp

  val copyr: var -> var -> u -> u
  val loadr: var -> var -> field -> u -> u
  val sloadr: var -> user typname -> field -> u -> u
  val aloadr: var -> var -> u -> u
  val storep: var -> field -> lexp -> texp -> u -> u
  val storer: var -> field -> ?s:var -> lexp -> texp -> u -> u
  val astorep: var -> lexp -> texp -> u -> u
  val astorer: var -> ?r:var -> lexp -> texp -> u -> u
  val null: var -> u -> u
  val newr: var -> ?c:user typname -> lexp -> texp -> u -> u
  val newa: var -> ?c:any typname -> lexp -> texp -> u -> u
  val bulk_upgrade: from_comp:(Var.t -> Var.t) -> to_comp:(Var.t -> Var.t) -> u -> u
  val apply_comp_effect: ctxtrefs:vars -> lcap:lexp -> Var.t -> exp -> u -> u

  val stats: int cardinals
end

(* -------------------------------------------------------------------------- *)

module type FINITE_COMPS_ENV = SCFG.ENV with type Var.t = V.t

module type REF_DOM = sig
  module Env: SCFG.ENV
  open OOBase
  open Env
  open Updates
  val null_ref: var -> u -> u
  val copy_ref: var -> var -> u -> u
  val load_ref: var -> var -> field -> u -> u
  val aload_ref: var -> var -> u -> u
  val store_ref: var -> field -> var -> u -> u
  val astore_ref: var -> var -> u -> u
  val cond_deep_join_refs: bexp -> var * var -> u -> u
  val cond_deep_store_ref: bexp -> var -> var -> u -> u
end

module type OBJ_DOM = sig
  module Env: SCFG.ENV
  open OOBase
  open Env
  open Updates
  include OBJ_TYPS with type texp = bexp and type lexp := lexp
  val ( <~^ ): var -> lexp -> u -> u
  val ( <?^ ): var -> texp -> u -> u
  val tite: bexp * texp * texp -> texp
  val sett: Var.t -> bexp -> u -> u
  val upgrade_obj_level: var -> lexp -> u -> u
  val upgrade_obj_taint: var -> texp -> u -> u
  val upgrade_reachable_obj_levels: ctxtrefs: vars -> var -> lexp -> lexp -> u -> u
  val upgrade_reachable_obj_taints: ctxtrefs: vars -> var -> texp -> u -> u
end

module type FINITE_COMPS_HEAP =
  functor (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
    (Heuristics: Env.HEURISTICS) (SupportVars: SUPPORT_VARS) ->
      T with module Env := Env

(* -------------------------------------------------------------------------- *)

module CombineHeapDomains
  (Env: SCFG.ENV)
  (Heuristics: Env.HEURISTICS)
  (FeatureFilters: Features.FILTERS)
  (RefDom: REF_DOM with module Env := Env)
  (ObjDom: OBJ_DOM with module Env := Env)
  =
struct
  open FeatureFilters
  open Features
  open Env
  open Lexp
  open Bexp
  open Heuristics
  include RefDom
  include ObjDom

  let copyr r s u = u
    |>~ (r <~^ ~^s)
    |>? (r <?^ ?^s)
    |> copy_ref r s

  let loadr r s f u = u
    |>~ (r <~^ ~^s)
    |>? (r <?^ ?^s)
    |> load_ref r s f

  let sloadr r c f u = u
    |>~ (r <~^ if evil_static_fields then top else bottom)
    |>? (r <?^ if evil_static_fields then tt else ff)
    |> null_ref r

  let aloadr r a u = u
    |>~ (r <~^ ~^a)
    |>? (r <?^ ?^a)
    |> aload_ref r a

  let storep r f l t u = u
    |>~ upgrade_obj_level r l
    |>? upgrade_obj_taint r t

  let storer r f ?s l t u = u
    |>~ upgrade_obj_level r l
    |>? upgrade_obj_taint r t
    |> app' (store_ref r f) s

  let astorep a l t u = u
    |>~ upgrade_obj_level a l
    |>~ upgrade_obj_taint a t

  let astorer a ?r l t u = u
    |>~ upgrade_obj_level a l
    |>? upgrade_obj_taint a t
    |> app' (astore_ref a) r

  let null r u = u
    |>~ (r <~^ bottom)                                             (* really? *)
    |>? (r <?^ ff)
    |> null_ref r

  let newr r ?c l t u = u
    |>~ (r <~^ l)
    |>? (r <?^ t)
    |> null_ref r

  let newa a ?c l t u = u
    |>~ (a <~^ l)
    |>? (a <?^ t)
    |> null_ref a
end

(* -------------------------------------------------------------------------- *)

module ObjTypResVars = struct
  let res_obj_level = HLevel ""
  let res_obj_taint = HTaint ""
end

module ShareRelResArgVars = struct
  open ShareRelVar
  open ShareRelVar.Ops
  let res_arg_share (v: var) = match v $>< "" with
    | Value v -> ShareRel v
    | Const _ ->
        failwith @@ asprintf "Invalid result sharing: %a" OOBase.Var.print v
  let is_res_arg_share x = match get x with "", _ | _, "" -> true | _ -> false
end

module AliasRelResArgVars = struct
  open AliasRelVar
  open AliasRelVar.Ops
  let res_arg_alias (v: var) = match v $~ "" with
    | Value v -> AliasRel v
    | Const _ ->
        failwith @@ asprintf "Invalid result alias: %a" OOBase.Var.print v
  let is_res_arg_alias x = match get x with "", _ | _, "" -> true | _ -> false
end

module FieldAliasRelResArgVars = struct
  open FieldAliasRelVar.Ops
  let res_arg_field_alias_to v = FieldAliasRel (v $.~> "")
  let res_arg_field_alias_from v = FieldAliasRel ("" $.~> v)
  let is_res_arg_field_alias_to = function (_, "") -> true | _ -> false
  let is_res_arg_field_alias_from = function ("", _) -> true | _ -> false
end

module FieldShareRelResArgVars = struct
  open FieldShareRelVar.Ops
  let res_arg_field_share_to v = FieldShareRel (v $.>< "")
  let res_arg_field_share_from v = FieldShareRel ("" $.>< v)
  let is_res_arg_field_share_to = function (_, "") -> true | _ -> false
  let is_res_arg_field_share_from = function ("", _) -> true | _ -> false
end

module ConnectRelResArgVars = struct
  open AliasRelVar
  open AliasRelVar.Ops
  let res_arg_connect (v: var) = match v $~ "" with
    | Value v -> AliasRel v
    | Const _ ->
        failwith @@ asprintf "Invalid result connect: %a" OOBase.Var.print v
  let is_res_arg_connect x = match get x with "", _ | _, "" -> true | _ -> false
end

(* -------------------------------------------------------------------------- *)

(** {2 Model-specific Transformers} *)

(** {3 Heap-related Relations} *)

module ShareRefDom
  (Env: FINITE_COMPS_ENV)
  (Heuristics: Env.HEURISTICS)
  (UOps: Env.Updates.UOPS)
  (RefHelpers: RefRels.SHARE_MODEL_HELPERS with module Env := Env)
  :
  (REF_DOM with module Env := Env)
  =
struct
  open RefHelpers
  open Env.Updates
  open Env.Bexp
  open OOBase
  open UOps
  open Heuristics
  open S.Ops

  let ( <-.>< ) a = setb (ShareRel a)
  let ( <.>< ) v = v @@><? (<-.><)

  let null_ref: var -> u -> u = fun d ->
    fold_all_refs begin fun (r: var) u -> u
      |> (r $>< d  <.><  ff)
    end

  let strong_share: var -> var -> u -> u = fun d s ->
    fold_all_refs begin fun (r: var) u -> u
      |> (r $>< d  <.><  (r $><. s))
    end
  and weak_share: var -> var -> u -> u = fun d s ->
    fold_all_refs begin fun (r: var) u -> u
      |> (r $>< d  <.><  ((r $><. d) ||. (r $><. s)))
    end

  let copy_ref: var -> var -> u -> u = strong_share
  let _load_ref: var -> var -> u -> u = strong_share
  let load_ref: var -> var -> field -> u -> u = fun d s _f -> _load_ref d s
  let aload_ref: var -> var -> u -> u = fun d s -> _load_ref d s

  let _store_ref: var -> var -> u -> u = weak_share
  let store_ref: var -> field -> var -> u -> u = fun d _f s -> _store_ref d s
  let astore_ref: var -> var -> u -> u = fun d s -> _store_ref d s

  let ( <.><! ) = function
    | S.Value v when ShareRelResArgVars.is_res_arg_share v -> (<-.><) v
    | v -> (<.><) v

  let cond_deep_join_refs: _ -> var * var -> u -> u = fun cond (d, s) ->
    fold_all_refs begin fun (r: var) u -> u
      |> (r $>< d   <.><!  ((r $><. d) ||. (cond &&. (r $><. s))))
      |> (r $>< s   <.><!  ((r $><. s) ||. (cond &&. (r $><. d))))
    end

  (* XXX: only used by deep-alias domains: *)
  let cond_deep_store_ref: _ -> var -> var -> u -> u = fun cond d s ->
    cond_deep_join_refs cond (d, s)

end

module DeepAliasRefDom
  (Env: FINITE_COMPS_ENV)
  (Heuristics: Env.HEURISTICS)
  (UOps: Env.Updates.UOPS)
  (RefHelpers: RefRels.DEEP_ALIAS_MODEL_HELPERS with module Env := Env)
  :
  (REF_DOM with module Env := Env)
  =
struct
  open RefHelpers
  open Env.Updates
  open Env.Bexp
  open OOBase
  open UOps
  open Heuristics
  open AliasRelVar.Ops
  open FieldAliasRelVar.Ops
  open ShareRelVar.Ops

  include ShareRefDom (Env) (Heuristics) (UOps) (RefHelpers)

  let ( <-.~ ) a = setb (AliasRel a)
  let ( <.~ ) v = v @@~? (<-.~)

  let ( <-..~> ) fa = setb (FieldAliasRel fa)
  let ( <..~> ) v = v @@.~>? (<-..~>)

  let null_ref: var -> u -> u = fun d u -> u
    |> null_ref d
    |> fold_all_refs begin fun (r: var) u -> u
      |> (r $~ d   <.~   ff)
      |> (d $.~> r <..~> ff)
      |> (r $.~> d <..~> ff)
    end

  let copy_ref: var -> var -> u -> u = fun d s u -> u
    |> copy_ref d s
    |> fold_all_refs begin fun (r: var) u -> u
      |> (r $~ d   <.~   (r $~. s))
      |> (d $.~> r <..~> (s $.~>. r))
      |> (r $.~> d <..~> (r $.~>. s))
    end

  let _load_ref: var -> var -> u -> u = fun d s ->
    fold_all_refs begin fun (r: var) u -> u
      |> (r $~ d   <.~   (s $.~>. r))
      |> (d $.~> r <..~> (s $.~>. r))
      |> (r $.~> d <..~> ((r $~. s) ||. (r $.~>. s)))
    end
  let load_ref: var -> var -> field -> u -> u = fun d s f u -> u
    |> load_ref d s f
    |> _load_ref d s
  let aload_ref: var -> var -> u -> u = fun d s u -> u
    |> aload_ref d s
    |> _load_ref d s

  let _store_ref: var -> var -> u -> u = fun d s ->
    fold_all_ref_pairs begin fun (d': var) (s': var) u -> u
      |> (d' $.~> s' <..~> ((d' $.~>. s') ||. (((d' $~. d) ||. (d' $.~>. d)) &&.
                                                 ((s' $~. s) ||. (s $.~>. s')))))
    end
  let store_ref: var -> field -> var -> u -> u = fun d f s u -> u
    |> store_ref d f s
    |> _store_ref d s
  let astore_ref: var -> var -> u -> u = fun d s u -> u
    |> astore_ref d s
    |> _store_ref d s

  let ( <.~! ) = function
    | AliasRelVar.Value v when AliasRelResArgVars.is_res_arg_alias v -> (<-.~) v
    | v -> (<.~) v

  let ( <..~>! ) = function
    | v when FieldAliasRelResArgVars.is_res_arg_field_alias_from v -> (<-..~>) v
    | v when FieldAliasRelResArgVars.is_res_arg_field_alias_to v -> (<-..~>) v
    | v -> (<..~>) v

  let cond_deep_join_refs: _ -> var * var -> u -> u = fun cond (d, s) u -> u
    |> cond_deep_join_refs cond (d, s)
    |> fold_all_refs begin fun (r: var) u -> u
      |> (r $~ d   <.~!   bite (cond, r $~. s, r $~. d))
      |> (r $~ s   <.~!   bite (cond, r $~. d, r $~. s))
      |> (d $.~> r <..~>! bite (cond, s $.~>. r, d $.~>. r))
      |> (s $.~> r <..~>! bite (cond, d $.~>. r, s $.~>. r))
      |> (r $.~> d <..~>! bite (cond, r $.~>. s, r $.~>. d))
      |> (r $.~> s <..~>! bite (cond, r $.~>. d, r $.~>. s))
    end

  let cond_deep_store_ref: _ -> var -> var -> u -> u = fun cond d s u -> u
    |> cond_deep_store_ref cond s d
    |> fold_all_ref_pairs begin fun (d': var) (s': var) u -> u
      |> (d' $.~> s' <..~>! bite (cond,
                                  ((d' $.~>. s') ||.
                                      (((d' $~. d) ||. (d' $.~>. d)) &&.
                                          ((s' $~. s) ||. (s $.~>. s')))),
                                  (d' $.~>. s')))
    end

end

module FieldShareRefDom
  (Env: FINITE_COMPS_ENV)
  (Heuristics: Env.HEURISTICS)
  (UOps: Env.Updates.UOPS)
  (RefHelpers: RefRels.FIELD_SHARE_MODEL_HELPERS with module Env := Env)
  :
  (REF_DOM with module Env := Env)
  =
struct
  open RefHelpers
  open Env.Updates
  open Env.Bexp
  open OOBase
  open UOps
  open Heuristics
  open AliasRelVar.Ops
  open FieldShareRelVar.Ops

  let ( <-.~ ) a = setb (AliasRel a)
  let ( <.~ ) v = v @@~? (<-.~)

  let ( <-..>< ) fs = setb (FieldShareRel fs)
  let ( <..>< ) v = v @@.><? (<-..><)

  let null_ref: var -> u -> u = fun d ->
    fold_all_refs begin fun (r: var) u -> u
      |> (r $~ d   <.~   ff)
      |> (d $.>< r <..>< ff)
      |> (r $.>< d <..>< ff)
    end

  let copy_ref: var -> var -> u -> u = fun d s ->
    fold_all_refs begin fun (r: var) u -> u
      |> (r $~ d   <.~   (r $~. s))
      |> (d $.>< r <..>< (s $.><. r))
      |> (r $.>< d <..>< (r $.><. s))
    end

  let _load_ref: var -> var -> u -> u = fun d s ->
    fold_all_refs begin fun (r: var) u -> u
      |> (r $~ d   <.~   (s $.><. r))
      |> (d $.>< r <..>< (s $.><. r))
      |> (r $.>< d <..>< ((r $~. s) ||. (r $.><. s)))
    end
  let load_ref: var -> var -> field -> u -> u = fun d s _f -> _load_ref d s
  let aload_ref: var -> var -> u -> u = _load_ref

  let _store_ref: var -> var -> u -> u = fun d s ->
    fold_all_ref_pairs begin fun (d': var) (s': var) u -> u
      |> (d' $.>< s' <..><
            ((d' $.><. s') ||. (((d' $~. d) ||. (d' $.><. d)) &&.
                                  ((s' $~. s) ||. (s $.><. s') ||. (s' $.><. s)))))
    end
  let store_ref: var -> field -> var -> u -> u = fun d _f s -> _store_ref d s
  let astore_ref: var -> var -> u -> u = _store_ref

  let ( <.~! ) = function
    | AliasRelVar.Value v when AliasRelResArgVars.is_res_arg_alias v -> (<-.~) v
    | v -> (<.~) v

  let ( <..><! ) = function
    | v when FieldShareRelResArgVars.is_res_arg_field_share_from v -> (<-..><) v
    | v when FieldShareRelResArgVars.is_res_arg_field_share_to v -> (<-..><) v
    | v -> (<..><) v

  let cond_deep_join_refs: _ -> var * var -> u -> u = fun cond (d, s) ->
    fold_all_refs begin fun (r: var) u -> u
      |> (r $~ d   <.~!   bite (cond, r $~. s, r $~. d))
      |> (r $~ s   <.~!   bite (cond, r $~. d, r $~. s))
      |> (d $.>< r <..><! bite (cond, s $.><. r, d $.><. r))
      |> (s $.>< r <..><! bite (cond, d $.><. r, s $.><. r))
      |> (r $.>< d <..><! bite (cond, r $.><. s, r $.><. d))
      |> (r $.>< s <..><! bite (cond, r $.><. d, r $.><. s))
    end

  let cond_deep_store_ref: _ -> var -> var -> u -> u = fun cond d s ->
    fold_all_ref_pairs begin fun (d': var) (s': var) u -> u
      |> (d' $.>< s' <..><!
            bite (cond, ((d' $.><. s') ||.
                            (((d' $~. d) ||. (d' $.><. d)) &&.
                                ((s' $~. s) ||. (s $.><. s') ||. (s' $.><. s)))),
                  (d' $.><. s')))
    end

end

module ConnectRefDom
  (Env: FINITE_COMPS_ENV)
  (Heuristics: Env.HEURISTICS)
  (UOps: Env.Updates.UOPS)
  (RefHelpers: RefRels.CONNECT_MODEL_HELPERS with module Env := Env)
  :
  (REF_DOM with module Env := Env)
  =
struct
  open RefHelpers
  open Env.Updates
  open Env.Bexp
  open OOBase
  open UOps
  open Heuristics
  open AliasRelVar.Ops

  include ShareRefDom (Env) (Heuristics) (UOps) (RefHelpers)

  let ( <-.~ ) a = setb (AliasRel a)
  let ( <.~ ) v = v @@~? (<-.~)

  let null_ref: var -> u -> u = fun d u -> u
    |> null_ref d
    |> fold_all_refs begin fun (r: var) u -> u
      |> (r $~ d  <.~   ff)
    end

  let connect: var -> var -> u -> u = fun d s ->
    fold_all_refs begin fun (r: var) u -> u
      |> (r $~ d  <.~  (r $~. s))
    end

  let copy_ref: var -> var -> u -> u = fun d s u -> u
    |> copy_ref d s
    |> connect d s

  let _load_ref: var -> var -> u -> u = connect
  let load_ref: var -> var -> field -> u -> u = fun d s f u -> u
    |> load_ref d s f
    |> _load_ref d s
  let aload_ref: var -> var -> u -> u = fun d s u -> u
    |> aload_ref d s
    |> _load_ref d s

  let _store_ref: var -> var -> u -> u = connect
  let store_ref: var -> field -> var -> u -> u = fun d f s u -> u
    |> store_ref d f s
    |> _store_ref d s
  let astore_ref: var -> var -> u -> u = fun d s u -> u
    |> astore_ref d s
    |> _store_ref d s

  let ( <.~! ) = function
    | A.Value v when ConnectRelResArgVars.is_res_arg_connect v -> (<-.~) v
    | v -> (<.~) v

  let cond_deep_join_refs: _ -> var * var -> u -> u = fun cond (d, s) u -> u
    |> cond_deep_join_refs cond (d, s)
    |> fold_all_refs begin fun (r: var) u -> u
      |> (r $~ d   <.~!  bite (cond, r $~. s, r $~. d))
      |> (r $~ s   <.~!  bite (cond, r $~. d, r $~. s))
    end

  let cond_deep_store_ref: _ -> var -> var -> u -> u = fun cond d s u -> u
    |> cond_deep_store_ref cond s d
    |> fold_all_refs begin fun (r: var) u -> u
      |> (r $~ d   <.~!  bite (cond, r $~. s, r $~. d))
    end

end

(* -------------------------------------------------------------------------- *)

(** {3 Object Types} *)

module ObjDomUtils
  (Env: FINITE_COMPS_ENV)
  (UOps: Env.Updates.UOPS)
  =
struct
  open Env
  open UOps

  type var = OOBase.var
  type u = Updates.u
  type texp = Bexp.t

  let tite = Bexp.ite
  let sett = setb

  let ( ~^ ) (r: var) = Lexp.var (HLevel r)
  let ( ?^ ) (r: var) = Bexp.var (HTaint r)

  let ( <~^ ) (r: var) = setl (HLevel r)
  let ( <?^ ) (r: var) = sett (HTaint r)
end

module ShareObjDom
  (Env: FINITE_COMPS_ENV)
  (Heuristics: Env.HEURISTICS)
  (UOps: Env.Updates.UOPS)
  (RefHelpers: RefRels.SHARE_MODEL_HELPERS with module Env := Env)
  :
  (OBJ_DOM with module Env := Env)
  =
struct
  open RefHelpers
  open Env
  open Bexp
  open Lexp
  open Heuristics
  include ObjDomUtils (Env) (UOps)

  (* --- *)

  let upgrade_obj_level: var -> lexp -> u -> u = fun r l ->
    fold_all_refs begin fun (s: var) ->
      s <~^ (~^s |.| lite (r $><. s, l, bottom))
    end

  let upgrade_obj_taint: var -> texp -> u -> u = fun r l ->
    fold_all_refs begin fun (s: var) ->
      s <?^ (?^s ||. tite (r $><. s, l, ff))
    end

  let upgrade_reachable_obj_levels ~ctxtrefs r l cap =
    fold_all_refs begin fun (s: OOBase.var) ->
      s <~^ (~^s |.| lite (r $><. s, lite (l <=.~ cap, l, cap), bottom))
    end

  let upgrade_reachable_obj_taints ~ctxtrefs r t =
    fold_all_refs begin fun (s: var) ->
      s <?^ (?^s ||. tite (s $><. r, t, ff))
    end

end

module DeepAliasObjDom
  (Env: FINITE_COMPS_ENV)
  (Heuristics: Env.HEURISTICS)
  (UOps: Env.Updates.UOPS)
  (RefHelpers: RefRels.DEEP_ALIAS_MODEL_HELPERS with module Env := Env)
  :
  (OBJ_DOM with module Env := Env)
  =
struct
  open RefHelpers
  open Env
  open Bexp
  open Lexp
  open Heuristics
  include ObjDomUtils (Env) (UOps)

  (* --- *)

  let upgrade_obj_level: var -> lexp -> u -> u = fun r l ->
    fold_all_refs begin fun (s: var) ->
      s <~^ (~^s |.| lite ((s $~. r) ||. (s $.~>. r), l, bottom))
    end

  let upgrade_obj_taint: var -> texp -> u -> u = fun r l ->
    fold_all_refs begin fun (s: var) ->
      s <?^ (?^s ||. tite ((s $~. r) ||. (s $.~>. r), l, ff))
    end

  let reachable_obj ~ctxtrefs r s =
    (* If s is is ctxtrefs, we rely on aliasing info instead of sharing. *)
    if Vars.mem s ctxtrefs
    then (s $~. r) ||. (s $.~>. r) ||. (r $.~>. s)
    else (s $><. r)

  let upgrade_reachable_obj_levels ~ctxtrefs r l cap =
    fold_all_refs begin fun (s: OOBase.var) ->
      s <~^ (~^s |.| lite (reachable_obj ~ctxtrefs r s,
                           lite (l <=.~ cap, l, cap), bottom))
    end

  let upgrade_reachable_obj_taints ~ctxtrefs r t =
    fold_all_refs begin fun (s: var) ->
      s <?^ (?^s ||. tite (reachable_obj ~ctxtrefs r s, t, ff))
    end

end

module FieldShareObjDom
  (Env: FINITE_COMPS_ENV)
  (Heuristics: Env.HEURISTICS)
  (UOps: Env.Updates.UOPS)
  (RefHelpers: RefRels.FIELD_SHARE_MODEL_HELPERS with module Env := Env)
  :
  (OBJ_DOM with module Env := Env)
  =
struct
  open RefHelpers
  open Env
  open Bexp
  open Lexp
  open Heuristics
  include ObjDomUtils (Env) (UOps)

  (* --- *)

  let upgrade_obj_level: var -> lexp -> u -> u = fun r l ->
    fold_all_refs begin fun (s: var) ->
      s <~^ (~^s |.| lite ((s $~. r) ||. (s $.><. r), l, bottom))
    end

  let upgrade_obj_taint: var -> texp -> u -> u = fun r l ->
    fold_all_refs begin fun (s: var) ->
      s <?^ (?^s ||. tite ((s $~. r) ||. (s $.><. r), l, ff))
    end

  let reachable_obj ~ctxtrefs r s =
    (* If s is in ctxtrefs, we can omit s field-sharing r. *)
    if Vars.mem s ctxtrefs
    then (s $~. r) ||. (r $.><. s)
    else (s $~. r) ||. (r $.><. s) ||. (s $.><. r)

  let upgrade_reachable_obj_levels ~ctxtrefs r l cap =
    fold_all_refs begin fun (s: OOBase.var) ->
      s <~^ (~^s |.| lite (reachable_obj ~ctxtrefs r s,
                           lite (l <=.~ cap, l, cap), bottom))
    end

  let upgrade_reachable_obj_taints ~ctxtrefs r t =
    fold_all_refs begin fun (s: var) ->
      s <?^ (?^s ||. tite (reachable_obj ~ctxtrefs r s, t, ff))
    end

end

module ConnectObjDom
  (Env: FINITE_COMPS_ENV)
  (Heuristics: Env.HEURISTICS)
  (UOps: Env.Updates.UOPS)
  (RefHelpers: RefRels.CONNECT_MODEL_HELPERS with module Env := Env)
  :
  (OBJ_DOM with module Env := Env)
  =
struct
  open RefHelpers
  open Env
  open Bexp
  open Lexp
  open Heuristics
  include ObjDomUtils (Env) (UOps)

  (* --- *)

  let upgrade_obj_level: var -> lexp -> u -> u = fun r l ->
    fold_all_refs begin fun (s: var) ->
      s <~^ (~^s |.| lite (r $~. s, l, bottom))
    end

  let upgrade_obj_taint: var -> texp -> u -> u = fun r l ->
    fold_all_refs begin fun (s: var) ->
      s <?^ (?^s ||. tite (r $~. s, l, ff))
    end

  let reachable_obj ~ctxtrefs r s =
    (* If s is is ctxtrefs, we rely on connection info instead of sharing. *)
    if Vars.mem s ctxtrefs then (s $~. r) else (s $><. r)

  let upgrade_reachable_obj_levels ~ctxtrefs r l cap =
    fold_all_refs begin fun (s: OOBase.var) ->
      s <~^ (~^s |.| lite (reachable_obj ~ctxtrefs r s,
                           lite (l <=.~ cap, l, cap), bottom))
    end

  let upgrade_reachable_obj_taints ~ctxtrefs r t =
    fold_all_refs begin fun (s: var) ->
      s <?^ (?^s ||. tite (s $~. r, t, ff))
    end

end

(* -------------------------------------------------------------------------- *)

(** Common utilities for substitutions *)

module ObjDomSubst
  (Env: FINITE_COMPS_ENV)
  =
struct
  open Env
  open OOLang.Stm.Ops

  let subst_level subst v = match !^(subst v) with
    | Stm.V v -> Lexp.var (HLevel v)
    | Stm.L _ -> Lexp.bottom

  let subst_taint subst v = match !^(subst v) with
    | Stm.V v -> Bexp.var (HTaint v)
    | Stm.L _ -> Bexp.ff
end

module ShareRelSubst
  (Env: FINITE_COMPS_ENV)
  (Rels: RefRels.SHARE_REL)
  =
struct
  open Env
  open Rels
  open ShareRelResArgVars
  open OOLang.Stm.Ops
  open ShareRelVar.Ops

  let subst_share_rel subst x =
    let r, s = ShareRelVar.get x in
    match !^(subst r), !^(subst s) with
      | Stm.L _, _ | _, Stm.L _ -> Bexp.ff
      | Stm.V r, Stm.V s -> match r $>< s with
          | Value v when is_res_arg_share v -> Bexp.var (ShareRel v)
          | Value v when ShareRelVars.mem v share_vars -> Bexp.var (ShareRel v)
          | Value _ -> Bexp.ff
          | Const b -> Bexp.cst b
end

module AliasRelSubst
  (Env: FINITE_COMPS_ENV)
  (Rels: RefRels.ALIAS_REL)
  =
struct
  open Env
  open Rels
  open AliasRelResArgVars
  open OOLang.Stm.Ops
  open AliasRelVar.Ops

  let subst_alias_rel subst x =
    let r, s = AliasRelVar.get x in
    match !^(subst r), !^(subst s) with
      | Stm.L _, _ | _, Stm.L _ -> Bexp.ff
      | Stm.V r, Stm.V s -> match r $~ s with
          | Value v when is_res_arg_alias v -> Bexp.var (AliasRel v)
          | Value v when AliasRelVars.mem v alias_vars -> Bexp.var (AliasRel v)
          | Value _ -> Bexp.ff
          | Const b -> Bexp.cst b
end

module FieldAliasRelSubst
  (Env: FINITE_COMPS_ENV)
  (Rels: RefRels.FIELD_ALIAS_REL)
  =
struct
  open Env
  open Rels
  open FieldAliasRelResArgVars
  open OOLang.Stm.Ops
  open FieldAliasRelVar.Ops

  let subst_field_alias_rel subst (r, s) =
    match !^(subst r), !^(subst s) with
      | Stm.L _, _ | _, Stm.L _ -> Bexp.ff
      | Stm.V r, Stm.V s ->
          let v = r $.~> s in
          if is_res_arg_field_alias_to v || is_res_arg_field_alias_from v
            || FieldAliasRelVars.mem v (* Captured. *)field_alias_vars
          then Bexp.var (FieldAliasRel v)
          else Bexp.ff
end

module FieldShareRelSubst
  (Env: FINITE_COMPS_ENV)
  (Rels: RefRels.FIELD_SHARE_REL)
  =
struct
  open Env
  open Rels
  open FieldShareRelResArgVars
  open OOLang.Stm.Ops
  open FieldShareRelVar.Ops

  let subst_field_share_rel subst (r, s) =
    match !^(subst r), !^(subst s) with
      | Stm.L _, _ | _, Stm.L _ -> Bexp.ff
      | Stm.V r, Stm.V s ->
          let v = r $.>< s in
          if is_res_arg_field_share_to v || is_res_arg_field_share_from v
            || FieldShareRelVars.mem v (* Captured. *)field_share_vars
          then Bexp.var (FieldShareRel v)
          else Bexp.ff
end

(* -------------------------------------------------------------------------- *)

module type DEEP_ALIAS_MODEL_CAPTURED_RELATIONS = sig
  val capture_alias: bool
  val capture_field_alias: bool
  val capture_share: bool                                         (* always false *)
end

module DeepAliasModelSubst
  (Env: FINITE_COMPS_ENV)
  (Config: sig
    include DEEP_ALIAS_MODEL_CAPTURED_RELATIONS
    module Cache: RefRels.DEEP_ALIAS_MODEL_RELS
    module Captured: RefRels.DEEP_ALIAS_MODEL_RELS
  end)
  :
  (SUBST with module Env := Env)
  =
struct
  include ObjDomSubst (Env)
  include ShareRelSubst (Env) (Config.Cache)
  include AliasRelSubst (Env) (Config.Cache)
  include FieldAliasRelSubst (Env) (Config.Cache)
  open Config

  let bool_comp = function
    | HTaint _ -> true
    | AliasRel _ -> capture_alias
    | FieldAliasRel _ -> capture_field_alias
    | ShareRel _ -> capture_share
    | _ -> false
  let level_comp = function
    | HLevel _ -> true
    | _ -> false
  let captured_comp = function
    | HTaint _ | HLevel _ -> true
    | AliasRel x -> AliasRelVars.mem x Captured.alias_vars
    | FieldAliasRel x -> FieldAliasRelVars.mem x Captured.field_alias_vars
    | ShareRel x -> ShareRelVars.mem x Captured.share_vars
    | _ -> false

  let subst_eff eff_subst = function
    | HLevel v -> `Expr (`Lexp (subst_level eff_subst v))
    | v when not (bool_comp v) -> `None         (* early filter for optimization *)
    | HTaint v -> `Bexp (subst_taint eff_subst v)
    | AliasRel x -> `Bexp (subst_alias_rel eff_subst x)
    | FieldAliasRel x -> `Bexp (subst_field_alias_rel eff_subst x)
    | ShareRel x -> `Bexp (subst_share_rel eff_subst x)
    | _ -> `None

end

(* --- *)

module type FIELD_SHARE_MODEL_CAPTURED_RELATIONS = sig
  val capture_alias: bool
  val capture_field_share: bool
end

module FieldShareModelSubst
  (Env: FINITE_COMPS_ENV)
  (Config: sig
    include FIELD_SHARE_MODEL_CAPTURED_RELATIONS
    module Cache: RefRels.FIELD_SHARE_MODEL_RELS
    module Captured: RefRels.FIELD_SHARE_MODEL_RELS
  end)
  :
  (SUBST with module Env := Env)
  =
struct
  include ObjDomSubst (Env)
  include AliasRelSubst (Env) (Config.Cache)
  include FieldShareRelSubst (Env) (Config.Cache)
  open Config

  let bool_comp = function
    | HTaint _ -> true
    | AliasRel _ -> capture_alias
    | FieldShareRel _ -> capture_field_share
    | _ -> false
  let level_comp = function
    | HLevel _ -> true
    | _ -> false
  let captured_comp = function
    | HTaint _ | HLevel _ -> true
    | AliasRel x -> AliasRelVars.mem x Captured.alias_vars
    | FieldShareRel x -> FieldShareRelVars.mem x Captured.field_share_vars
    | _ -> false

  let subst_eff eff_subst = function
    | HLevel v -> `Expr (`Lexp (subst_level eff_subst v))
    | v when not (bool_comp v) -> `None         (* early filter for optimization *)
    | HTaint v -> `Bexp (subst_taint eff_subst v)
    | AliasRel x -> `Bexp (subst_alias_rel eff_subst x)
    | FieldShareRel x -> `Bexp (subst_field_share_rel eff_subst x)
    | _ -> `None

end

(* --- *)

module type SHARE_MODEL_CAPTURED_RELATIONS = sig
  val capture_share: bool
end

module ShareModelSubst
  (Env: FINITE_COMPS_ENV)
  (Config: sig
    include SHARE_MODEL_CAPTURED_RELATIONS
    module Cache: RefRels.SHARE_MODEL_RELS
    module Captured: RefRels.SHARE_MODEL_RELS
  end)
  :
  (SUBST with module Env := Env)
  =
struct
  include ObjDomSubst (Env)
  include ShareRelSubst (Env) (Config.Cache)
  open Config

  let bool_comp = function
    | HTaint _ -> true
    | ShareRel _ -> capture_share
    | _ -> false
  let level_comp = function
    | HLevel _ -> true
    | _ -> false
  let captured_comp = function
    | HTaint _ | HLevel _ -> true
    | ShareRel x -> ShareRelVars.mem x Captured.share_vars
    | _ -> false

  let subst_eff eff_subst = function
    | HLevel v -> `Expr (`Lexp (subst_level eff_subst v))
    | v when not (bool_comp v) -> `None         (* early filter for optimization *)
    | HTaint v -> `Bexp (subst_taint eff_subst v)
    | ShareRel x -> `Bexp (subst_share_rel eff_subst x)
    | _ -> `None
end

(* --- *)

module type CONNECT_MODEL_CAPTURED_RELATIONS = sig
  val capture_connect: bool
  val capture_share: bool
end

module ConnectModelSubst
  (Env: FINITE_COMPS_ENV)
  (Config: sig
    include CONNECT_MODEL_CAPTURED_RELATIONS
    module Cache: RefRels.CONNECT_MODEL_RELS
    module Captured: RefRels.CONNECT_MODEL_RELS
  end)
  =                                     (* Reuse shallow-alias's substitution *)
  DeepAliasModelSubst (Env) (struct
    open Config
    let capture_alias = capture_connect
    let capture_field_alias = false
    let capture_share = capture_share
    module Cache = struct
      open Cache
      let all_refs = all_refs
      let alias_vars = connect_vars
      let field_alias_vars = FieldAliasRelVars.empty
      let share_vars = share_vars
    end
    module Captured = struct
      open Captured
      let all_refs = all_refs
      let alias_vars = connect_vars
      let field_alias_vars = FieldAliasRelVars.empty
      let share_vars = share_vars
    end
  end)

(* -------------------------------------------------------------------------- *)

(** {2 Support Helpers}

    Helpers for accessing and traversing the various sets of references and
    relations. *)

(** {3 Signatures Specialized for each Relation} *)

module type __COMMON_SUPPORT_HELPERS = sig
  module Env: FINITE_COMPS_ENV
  open Base

  val ret_refp: bool
  val all_arg_refs: vars
  val fold_arg_refs: (var -> 'a -> 'a) -> 'a -> 'a
  val fold_local_refs: (var -> 'a -> 'a) -> 'a -> 'a

  open SCFG
  val obj_typs: (v -> typ -> 'a -> 'a) -> ObjLevelVar.t -> 'a -> 'a
  val add_res_obj_typs: typ Env.VarMap.t -> typ Env.VarMap.t
  val add_arg_res_obj_typs: ObjLevelVar.t -> typ Env.VarMap.t -> typ Env.VarMap.t
end

module type __SHARE_REL_SUPPORT_HELPERS = sig
  module Env: FINITE_COMPS_ENV
  open Base

  val all_arg_share: ShareRelVars.t
  val fold_arg_share: (ShareRelVar.t -> 'a -> 'a) -> 'a -> 'a

  val fold_ret_arg_share: (var -> 'a -> 'a) -> 'a -> 'a
  val fold_exc_arg_share: (var -> 'a -> 'a) -> 'a -> 'a

  open SCFG
  val share_rel: (v -> typ -> 'a -> 'a) -> ShareRelVar.t -> 'a -> 'a

  val res_arg_share: var -> v

  val add_arg_res_share: ShareRelVar.t -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_ret_arg_share: var -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_exc_arg_share: var -> typ Env.VarMap.t -> typ Env.VarMap.t
end

module type __ALIAS_REL_SUPPORT_HELPERS = sig
  module Env: FINITE_COMPS_ENV
  open Base

  val all_arg_alias: AliasRelVars.t

  val const_alias_vars: AliasRelVars.t
  val mut_alias_vars: AliasRelVars.t

  val fold_mut_alias_vars: (AliasRelVar.t -> 'a -> 'a) -> 'a -> 'a
  val fold_const_alias_vars: (AliasRelVar.t -> 'a -> 'a) -> 'a -> 'a

  val fold_ret_arg_alias: (var -> 'a -> 'a) -> 'a -> 'a
  val fold_exc_arg_alias: (var -> 'a -> 'a) -> 'a -> 'a

  open SCFG
  val alias_rel: (v -> typ -> 'a -> 'a) -> AliasRelVar.t -> 'a -> 'a

  val res_arg_alias: var -> v
  val add_ret_arg_alias: var -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_exc_arg_alias: var -> typ Env.VarMap.t -> typ Env.VarMap.t
end

module type __FIELD_ALIAS_REL_SUPPORT_HELPERS = sig
  module Env: FINITE_COMPS_ENV
  open Base

  val all_arg_field_alias: FieldAliasRelVars.t
  val fold_arg_field_alias: (FieldAliasRelVar.t -> 'a -> 'a) -> 'a -> 'a

  val fold_ret_arg_field_alias_to: (var -> 'a -> 'a) -> 'a -> 'a
  val fold_ret_arg_field_alias_from: (var -> 'a -> 'a) -> 'a -> 'a
  val fold_exc_arg_field_alias_to: (var -> 'a -> 'a) -> 'a -> 'a
  val fold_exc_arg_field_alias_from: (var -> 'a -> 'a) -> 'a -> 'a

  open SCFG
  val field_alias_rel: (v -> typ -> 'a -> 'a) -> FieldAliasRelVar.t -> 'a -> 'a

  val res_arg_field_alias_to: var -> v
  val res_arg_field_alias_from: var -> v

  val add_arg_res_field_alias: FieldAliasRelVar.t -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_ret_arg_field_alias_to: var -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_ret_arg_field_alias_from: var -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_exc_arg_field_alias_to: var -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_exc_arg_field_alias_from: var -> typ Env.VarMap.t -> typ Env.VarMap.t
end

module type __FIELD_SHARE_REL_SUPPORT_HELPERS = sig
  module Env: FINITE_COMPS_ENV
  open Base

  val all_arg_field_share: FieldShareRelVars.t
  val fold_arg_field_share: (FieldShareRelVar.t -> 'a -> 'a) -> 'a -> 'a

  val fold_ret_arg_field_share_to: (var -> 'a -> 'a) -> 'a -> 'a
  val fold_ret_arg_field_share_from: (var -> 'a -> 'a) -> 'a -> 'a
  val fold_exc_arg_field_share_to: (var -> 'a -> 'a) -> 'a -> 'a
  val fold_exc_arg_field_share_from: (var -> 'a -> 'a) -> 'a -> 'a

  open SCFG
  val field_share_rel: (v -> typ -> 'a -> 'a) -> FieldShareRelVar.t -> 'a -> 'a

  val res_arg_field_share_to: var -> v
  val res_arg_field_share_from: var -> v

  val add_arg_res_field_share: FieldShareRelVar.t -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_ret_arg_field_share_to: var -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_ret_arg_field_share_from: var -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_exc_arg_field_share_to: var -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_exc_arg_field_share_from: var -> typ Env.VarMap.t -> typ Env.VarMap.t
end

module type __CONNECT_REL_SUPPORT_HELPERS = sig
  module Env: FINITE_COMPS_ENV
  open Base

  val all_arg_connect: AliasRelVars.t
  val fold_arg_connect: (AliasRelVar.t -> 'a -> 'a) -> 'a -> 'a

  val fold_ret_arg_connect: (var -> 'a -> 'a) -> 'a -> 'a
  val fold_exc_arg_connect: (var -> 'a -> 'a) -> 'a -> 'a

  open SCFG
  val connect_rel: (v -> typ -> 'a -> 'a) -> AliasRelVar.t -> 'a -> 'a

  val res_arg_connect: var -> v

  val add_arg_res_connect: AliasRelVar.t -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_ret_arg_connect: var -> typ Env.VarMap.t -> typ Env.VarMap.t
  val add_exc_arg_connect: var -> typ Env.VarMap.t -> typ Env.VarMap.t
end

(* --- *)

(** {3 Deep-alias model} *)

module type DEEP_ALIAS_MODEL_SUPPORT_HELPERS = sig
  include __COMMON_SUPPORT_HELPERS
  include __SHARE_REL_SUPPORT_HELPERS with module Env := Env
  include __ALIAS_REL_SUPPORT_HELPERS with module Env := Env
  include __FIELD_ALIAS_REL_SUPPORT_HELPERS with module Env := Env
  include DEEP_ALIAS_MODEL_CAPTURED_RELATIONS
  module Cache: RefRels.DEEP_ALIAS_MODEL_RELS
  module Captured: RefRels.DEEP_ALIAS_MODEL_RELS
end

module DeepAliasModelSupportHelpers
  (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
  (Components: DEEP_ALIAS_MODEL_CAPTURED_RELATIONS)
  (SupportVars: SUPPORT_VARS)
  :
  (DEEP_ALIAS_MODEL_SUPPORT_HELPERS with module Env := Env)
  =
struct
  open FeatureFilters
  open SupportVars
  include Components

  (* Support sets: *)

  module RefWalks = RefRels.DeepAliasModelRefSetsWalk (Sem)
  module Cache = struct
    let all_refs = VarMap.keys refs
    let alias_vars = RefWalks.alias_vars refs
    let field_alias_vars = RefWalks.field_alias_vars refs
    let share_vars = RefWalks.share_vars refs
  end
  open Cache
  module Captured = struct
    let all_refs = all_refs
    let alias_vars =
      if capture_alias then alias_vars else AliasRelVars.empty
    let field_alias_vars =
      if capture_field_alias then field_alias_vars else FieldAliasRelVars.empty
    let share_vars =
      if capture_share then share_vars else ShareRelVars.empty
  end

  let arg_refs = VarMap.merge (fun v at rt -> match at, rt with
    | Some _, Some rt -> Some rt
    | _ -> None) args refs
  let all_args = VarMap.keys args
  let all_arg_refs = Vars.inter all_args all_refs
  let all_local_refs = Vars.diff all_refs all_arg_refs

  include struct
    open Captured

    let all_arg_alias = RefWalks.alias_vars arg_refs
    let all_arg_field_alias = RefWalks.field_alias_vars arg_refs
    let all_arg_share = RefWalks.share_vars arg_refs

    let const_alias_vars = all_arg_alias
    let mut_alias_vars = AliasRelVars.diff alias_vars const_alias_vars
    let const_share_vars = all_arg_share
    let mut_share_vars = ShareRelVars.diff share_vars const_share_vars

    let ret_refp = retdom <> None

    let ret_arg_alias_vars = match retdom with
      | _ when not capture_alias -> Vars.empty
      | None -> Vars.empty
      | Some retdom -> RefWalks.aliasing_vars retdom arg_refs
    let ret_arg_field_alias_to_vars, ret_arg_field_alias_from_vars = match retdom with
      | _ when not capture_field_alias -> Vars.empty, Vars.empty
      | None -> Vars.empty, Vars.empty
      | Some retdom -> RefWalks.field_aliasing_vars_to_from retdom arg_refs
    let ret_arg_share_vars = match retdom with
      | _ when not capture_share -> Vars.empty
      | None -> Vars.empty
      | Some retdom -> RefWalks.sharing_vars retdom arg_refs

    let exc_arg_alias_vars =
      if capture_alias && Features.track_excs
      then RefWalks.aliasing_vars Sem.excndom arg_refs
      else Vars.empty
    let exc_arg_field_alias_to_vars, exc_arg_field_alias_from_vars =
      if capture_field_alias && Features.track_excs
      then RefWalks.field_aliasing_vars_to_from Sem.excndom arg_refs
      else Vars.empty, Vars.empty
    let exc_arg_share_vars =
      if capture_share && Features.track_excs
      then RefWalks.sharing_vars Sem.excndom arg_refs
      else Vars.empty
  end

  (* Helpers: *)

  let fold_arg_refs f = Vars.fold f all_arg_refs
  let fold_local_refs f = Vars.fold f all_local_refs
  let fold_arg_field_alias f = FieldAliasRelVars.fold f all_arg_field_alias
  let fold_arg_share f = ShareRelVars.fold f all_arg_share
  let fold_mut_alias_vars f = AliasRelVars.fold f mut_alias_vars
  let fold_const_alias_vars f = AliasRelVars.fold f const_alias_vars

  let fold_ret_arg_alias f = Vars.fold f ret_arg_alias_vars
  let fold_ret_arg_field_alias_to f = Vars.fold f ret_arg_field_alias_to_vars
  let fold_ret_arg_field_alias_from f = Vars.fold f ret_arg_field_alias_from_vars
  let fold_ret_arg_share f = Vars.fold f ret_arg_share_vars
  let fold_exc_arg_alias f = Vars.fold f exc_arg_alias_vars
  let fold_exc_arg_field_alias_to f = Vars.fold f exc_arg_field_alias_to_vars
  let fold_exc_arg_field_alias_from f = Vars.fold f exc_arg_field_alias_from_vars
  let fold_exc_arg_share f = Vars.fold f exc_arg_share_vars

  (* --- *)

  module Vs = Env.VarSet
  module Vm = Env.VarMap
  open SCFG

  let obj_typs f r i = i
    |>~ f (HLevel r) Level
    |>? f (HTaint r) Bool

  let alias_rel f r i = i
    |> if capture_alias then f (AliasRel r) Bool else ign

  let field_alias_rel f r i = i
    |> if capture_field_alias then f (FieldAliasRel r) Bool else ign

  let share_rel f r i = i
    |> if capture_share then f (ShareRel r) Bool else ign

  (* --- *)

  let ret c = Vm.add (Ret c)
  let exc c = Vm.add (Exc c)

  let add_arg_res_obj_typs r s = s
    |> obj_typs ret r
    |>! obj_typs exc r

  let add_arg_res_field_alias r s = s
    |> field_alias_rel ret r
    |>! field_alias_rel exc r

  let add_arg_res_share r s = s
    |> share_rel ret r
    |>! share_rel exc r

  let add_res_obj_typs s = s
    |> (if ret_refp then obj_typs ret "" else ign)
    |>! obj_typs exc ""

  include ShareRelResArgVars
  include AliasRelResArgVars
  include FieldAliasRelResArgVars
  let add_ret_arg_alias a = ret (res_arg_alias a) Bool
  let add_ret_arg_field_alias_to a = ret (res_arg_field_alias_to a) Bool
  let add_ret_arg_field_alias_from a = ret (res_arg_field_alias_from a) Bool
  let add_ret_arg_share a = ret (res_arg_share a) Bool
  let add_exc_arg_alias a = exc (res_arg_alias a) Bool
  let add_exc_arg_field_alias_to a = exc (res_arg_field_alias_to a) Bool
  let add_exc_arg_field_alias_from a = exc (res_arg_field_alias_from a) Bool
  let add_exc_arg_share a = exc (res_arg_share a) Bool

end

(* --- *)

(** {3 Field-share model} *)

module type FIELD_SHARE_MODEL_SUPPORT_HELPERS = sig
  include __COMMON_SUPPORT_HELPERS
  include __ALIAS_REL_SUPPORT_HELPERS with module Env := Env
  include __FIELD_SHARE_REL_SUPPORT_HELPERS with module Env := Env
  include FIELD_SHARE_MODEL_CAPTURED_RELATIONS
  module Cache: RefRels.FIELD_SHARE_MODEL_RELS
  module Captured: RefRels.FIELD_SHARE_MODEL_RELS
end

module FieldShareModelSupportHelpers
  (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
  (Components: FIELD_SHARE_MODEL_CAPTURED_RELATIONS)
  (SupportVars: SUPPORT_VARS)
  :
  (FIELD_SHARE_MODEL_SUPPORT_HELPERS with module Env := Env)
  =
struct
  open FeatureFilters
  open SupportVars
  include Components

  (* Support sets: *)

  module RefWalks = RefRels.FieldShareModelRefSetsWalk (Sem)
  module Cache = struct
    let all_refs = VarMap.keys refs
    let alias_vars = RefWalks.alias_vars refs
    let field_share_vars = RefWalks.field_share_vars refs
  end
  open Cache
  module Captured = struct
    let all_refs = all_refs
    let alias_vars =
      if capture_alias then alias_vars else AliasRelVars.empty
    let field_share_vars =
      if capture_field_share then field_share_vars else FieldShareRelVars.empty
  end

  let arg_refs = VarMap.merge (fun v at rt -> match at, rt with
    | Some _, Some rt -> Some rt
    | _ -> None) args refs
  let all_args = VarMap.keys args
  let all_arg_refs = Vars.inter all_args all_refs
  let all_local_refs = Vars.diff all_refs all_arg_refs

  include struct
    open Captured

    let all_arg_alias = RefWalks.alias_vars arg_refs
    let all_arg_field_share = RefWalks.field_share_vars arg_refs

    let const_alias_vars = all_arg_alias
    let mut_alias_vars = AliasRelVars.diff alias_vars const_alias_vars

    let ret_refp = retdom <> None

    let ret_arg_alias_vars = match retdom with
      | _ when not capture_alias -> Vars.empty
      | None -> Vars.empty
      | Some retdom -> RefWalks.aliasing_vars retdom arg_refs
    let ret_arg_field_share_to_vars, ret_arg_field_share_from_vars = match retdom with
      | _ when not capture_field_share -> Vars.empty, Vars.empty
      | None -> Vars.empty, Vars.empty
      | Some retdom -> RefWalks.field_sharing_vars_to_from retdom arg_refs

    let exc_arg_alias_vars =
      if capture_alias && Features.track_excs
      then RefWalks.aliasing_vars Sem.excndom arg_refs
      else Vars.empty
    let exc_arg_field_share_to_vars, exc_arg_field_share_from_vars =
      if capture_field_share && Features.track_excs
      then RefWalks.field_sharing_vars_to_from Sem.excndom arg_refs
      else Vars.empty, Vars.empty
  end

  (* Helpers: *)

  let fold_arg_refs f = Vars.fold f all_arg_refs
  let fold_local_refs f = Vars.fold f all_local_refs
  let fold_arg_field_share f = FieldShareRelVars.fold f all_arg_field_share
  let fold_mut_alias_vars f = AliasRelVars.fold f mut_alias_vars
  let fold_const_alias_vars f = AliasRelVars.fold f const_alias_vars

  let fold_ret_arg_alias f = Vars.fold f ret_arg_alias_vars
  let fold_ret_arg_field_share_to f = Vars.fold f ret_arg_field_share_to_vars
  let fold_ret_arg_field_share_from f = Vars.fold f ret_arg_field_share_from_vars
  let fold_exc_arg_alias f = Vars.fold f exc_arg_alias_vars
  let fold_exc_arg_field_share_to f = Vars.fold f exc_arg_field_share_to_vars
  let fold_exc_arg_field_share_from f = Vars.fold f exc_arg_field_share_from_vars

  (* --- *)

  module Vs = Env.VarSet
  module Vm = Env.VarMap
  open SCFG

  let obj_typs f r i = i
    |>~ f (HLevel r) Level
    |>? f (HTaint r) Bool

  let alias_rel f r i = i
    |> if capture_alias then f (AliasRel r) Bool else ign

  let field_share_rel f r i = i
    |> if capture_field_share then f (FieldShareRel r) Bool else ign

  (* --- *)

  let ret c = Vm.add (Ret c)
  let exc c = Vm.add (Exc c)

  let add_arg_res_obj_typs r s = s
    |> obj_typs ret r
    |>! obj_typs exc r

  let add_arg_res_field_share r s = s
    |> field_share_rel ret r
    |>! field_share_rel exc r

  let add_res_obj_typs s = s
    |> (if ret_refp then obj_typs ret "" else ign)
    |>! obj_typs exc ""

  include AliasRelResArgVars
  include FieldShareRelResArgVars
  let add_ret_arg_alias a = ret (res_arg_alias a) Bool
  let add_ret_arg_field_share_to a = ret (res_arg_field_share_to a) Bool
  let add_ret_arg_field_share_from a = ret (res_arg_field_share_from a) Bool
  let add_exc_arg_alias a = exc (res_arg_alias a) Bool
  let add_exc_arg_field_share_to a = exc (res_arg_field_share_to a) Bool
  let add_exc_arg_field_share_from a = exc (res_arg_field_share_from a) Bool

end

(* --- *)

(** {3 Share model} *)

module type SHARE_MODEL_SUPPORT_HELPERS = sig
  include __COMMON_SUPPORT_HELPERS
  include __SHARE_REL_SUPPORT_HELPERS with module Env := Env
  include SHARE_MODEL_CAPTURED_RELATIONS
  module Cache: RefRels.SHARE_MODEL_RELS
  module Captured: RefRels.SHARE_MODEL_RELS
end

module ShareModelSupportHelpers
  (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
  (Components: SHARE_MODEL_CAPTURED_RELATIONS)
  (SupportVars: SUPPORT_VARS)
  :
  (SHARE_MODEL_SUPPORT_HELPERS with module Env := Env)
  =
struct
  open FeatureFilters
  open SupportVars
  include Components

  (* Support sets: *)

  module RefWalks = RefRels.ShareModelRefSetsWalk (Sem)
  module Cache = struct
    let all_refs = VarMap.keys refs
    let share_vars = RefWalks.share_vars refs
  end
  include Cache
  module Captured = struct
    let all_refs = all_refs
    let share_vars =
      if capture_share then RefWalks.share_vars refs else ShareRelVars.empty
  end

  let arg_refs = VarMap.merge (fun v at rt -> match at, rt with
    | Some _, Some rt -> Some rt
    | _ -> None) args refs
  let all_args = VarMap.keys args
  let all_arg_refs = Vars.inter all_args all_refs
  let all_local_refs = Vars.diff all_refs all_arg_refs

  include struct
    open Captured

    let all_arg_share = RefWalks.share_vars arg_refs

    let const_share_vars = all_arg_share
    let mut_share_vars = ShareRelVars.diff share_vars const_share_vars

    let ret_refp = retdom <> None

    let ret_arg_share_vars = match retdom with
      | _ when not capture_share -> Vars.empty
      | None -> Vars.empty
      | Some retdom -> RefWalks.sharing_vars retdom arg_refs

    let exc_arg_share_vars =
      if capture_share && Features.track_excs
      then RefWalks.sharing_vars Sem.excndom arg_refs
      else Vars.empty
  end

  (* Helpers: *)

  include RefRels.CommonRefHelpers (Env) (Captured)
  let fold_arg_refs f = Vars.fold f all_arg_refs
  let fold_local_refs f = Vars.fold f all_local_refs
  let fold_arg_share f = ShareRelVars.fold f all_arg_share
  let fold_mut_share_vars f = ShareRelVars.fold f mut_share_vars
  let fold_const_share_vars f = ShareRelVars.fold f const_share_vars

  let fold_locals_related_ref_pairs f =
    fold_all_ref_pairs begin fun r r' ->
      if Vars.mem r all_arg_refs && Vars.mem r' all_arg_refs
      then ign
      else f r r'
    end

  let fold_ret_arg_share f = Vars.fold f ret_arg_share_vars
  let fold_exc_arg_share f = Vars.fold f exc_arg_share_vars

  (* --- *)

  module Vs = Env.VarSet
  module Vm = Env.VarMap
  open SCFG

  let obj_typs f r i = i
    |>~ f (HLevel r) Level
    |>? f (HTaint r) Bool

  let share_rel f r i = i
    |> f (ShareRel r) Bool

  (* --- *)

  let typ_support = Vm.empty
    |> fold_all_refs (obj_typs Vm.add)

  (* --- *)

  let ret c = Vm.add (Ret c)
  let exc c = Vm.add (Exc c)

  let add_arg_res_obj_typs r s = s
    |> obj_typs ret r
    |>! obj_typs exc r

  let add_arg_res_share r s = s
    |> share_rel ret r
    |>! share_rel exc r

  let add_res_obj_typs s = s
    |> (if ret_refp then obj_typs ret "" else ign)
    |>! obj_typs exc ""

  include ShareRelResArgVars
  let add_ret_arg_share a = ret (res_arg_share a) Bool
  let add_exc_arg_share a = exc (res_arg_share a) Bool

end

(* --- *)

(** {3 Connect model} *)

module type CONNECT_MODEL_SUPPORT_HELPERS = sig
  include __COMMON_SUPPORT_HELPERS
  include __SHARE_REL_SUPPORT_HELPERS with module Env := Env
  include __CONNECT_REL_SUPPORT_HELPERS with module Env := Env
  include CONNECT_MODEL_CAPTURED_RELATIONS
  module Cache: RefRels.CONNECT_MODEL_RELS
  module Captured: RefRels.CONNECT_MODEL_RELS
end

module ConnectModelSupportHelpers
  (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
  (Components: CONNECT_MODEL_CAPTURED_RELATIONS)
  (SupportVars: SUPPORT_VARS)
  :
  (CONNECT_MODEL_SUPPORT_HELPERS with module Env := Env)
  =
struct
  open FeatureFilters
  open SupportVars
  include Components

  (* Support sets: *)

  module RefWalks = RefRels.ConnectModelRefSetsWalk (Sem)
  module Cache = struct
    let all_refs = VarMap.keys refs
    let connect_vars = RefWalks.connect_vars refs
    let share_vars = RefWalks.share_vars refs
  end
  include Cache
  module Captured = struct
    let all_refs = all_refs
    let connect_vars =
      if capture_connect then connect_vars else AliasRelVars.empty
    let share_vars =
      if capture_share then share_vars else ShareRelVars.empty
  end

  let arg_refs = VarMap.merge (fun v at rt -> match at, rt with
    | Some _, Some rt -> Some rt
    | _ -> None) args refs
  let all_args = VarMap.keys args
  let all_arg_refs = Vars.inter all_args all_refs
  let all_local_refs = Vars.diff all_refs all_arg_refs

  include struct
    open Captured

    let all_arg_connect = RefWalks.connect_vars arg_refs
    let all_arg_share = RefWalks.share_vars arg_refs

    let ret_refp = retdom <> None

    let ret_arg_connect_vars = match retdom with
      | _ when not capture_connect -> Vars.empty
      | None -> Vars.empty
      | Some retdom -> RefWalks.connecting_vars retdom arg_refs
    let ret_arg_share_vars = match retdom with
      | _ when not capture_share -> Vars.empty
      | None -> Vars.empty
      | Some retdom -> RefWalks.sharing_vars retdom arg_refs

    let exc_arg_connect_vars =
      if capture_connect && Features.track_excs
      then RefWalks.connecting_vars Sem.excndom arg_refs
      else Vars.empty
    let exc_arg_share_vars =
      if capture_share && Features.track_excs
      then RefWalks.sharing_vars Sem.excndom arg_refs
      else Vars.empty
  end

  (* Helpers *)

  include RefRels.CommonRefHelpers (Env) (struct let all_refs = all_refs end)
  let fold_arg_refs f = Vars.fold f all_arg_refs
  let fold_local_refs f = Vars.fold f all_local_refs
  let fold_arg_connect f = AliasRelVars.fold f all_arg_connect
  let fold_all_connect_vars f = AliasRelVars.fold f connect_vars
  let fold_arg_share f = ShareRelVars.fold f all_arg_share
  let fold_all_share_vars f = ShareRelVars.fold f share_vars

  let fold_locals_related_ref_pairs f =
    fold_all_ref_pairs begin fun r r' ->
      if Vars.mem r all_arg_refs && Vars.mem r' all_arg_refs
      then ign
      else f r r'
    end

  let fold_ret_arg_connect f = Vars.fold f ret_arg_connect_vars
  let fold_exc_arg_connect f = Vars.fold f exc_arg_connect_vars
  let fold_ret_arg_share f = Vars.fold f ret_arg_share_vars
  let fold_exc_arg_share f = Vars.fold f exc_arg_share_vars

  (* --- *)

  module Vs = Env.VarSet
  module Vm = Env.VarMap
  open SCFG

  let obj_typs f r i = i
    |>~ f (HLevel r) Level
    |>? f (HTaint r) Bool

  let connect_rel f r i = i
    |> if capture_connect then f (AliasRel r) Bool else ign

  let share_rel f r i = i
    |> if capture_share then f (ShareRel r) Bool else ign

  (* --- *)

  let typ_support = Vm.empty
    |> fold_all_refs (obj_typs Vm.add)

  (* --- *)

  let ret c = Vm.add (Ret c)
  let exc c = Vm.add (Exc c)

  let add_arg_res_obj_typs r s = s
    |> obj_typs ret r
    |>! obj_typs exc r

  let add_arg_res_connect r s = s
    |> connect_rel ret r
    |>! connect_rel exc r

  let add_arg_res_share r s = s
    |> share_rel ret r
    |>! share_rel exc r

  let add_res_obj_typs s = s
    |> (if ret_refp then obj_typs ret "" else ign)
    |>! obj_typs exc ""

  include ShareRelResArgVars
  include ConnectRelResArgVars
  let add_ret_arg_connect a = ret (res_arg_connect a) Bool
  let add_exc_arg_connect a = exc (res_arg_connect a) Bool
  let add_ret_arg_share a = ret (res_arg_share a) Bool
  let add_exc_arg_share a = exc (res_arg_share a) Bool

end

(* -------------------------------------------------------------------------- *)

(** {2 Generic Definitions for Symbolic Heaps}

    Common definitions for each heap model. *)

(** {3 Deep-alias model} *)

module type DEEP_ALIAS_MODEL_HELPERS_FUNCT =
  functor (Env: FINITE_COMPS_ENV) (RS: RefRels.DEEP_ALIAS_MODEL_RELS) ->
    RefRels.DEEP_ALIAS_MODEL_HELPERS with module Env := Env

module type DEEP_ALIAS_MODEL_REF_DOM_FUNCT =
  functor (Env: FINITE_COMPS_ENV) (Heuristics: Env.HEURISTICS)
    (UOps: Env.Updates.UOPS)
    (RelHelpers: RefRels.DEEP_ALIAS_MODEL_HELPERS with module Env := Env) ->
      REF_DOM with module Env := Env

module type DEEP_ALIAS_MODEL_OBJ_DOM_FUNCT =
  functor (Env: FINITE_COMPS_ENV) (Heuristics: Env.HEURISTICS)
    (UOps: Env.Updates.UOPS)
    (RelHelpers: RefRels.DEEP_ALIAS_MODEL_HELPERS with module Env := Env) ->
      OBJ_DOM with module Env := Env

module DeepAliasModelCommonDefs =
  functor (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
    (Heuristics: Env.HEURISTICS) (SupportVars: SUPPORT_VARS)
    (MakeRelHelpers: DEEP_ALIAS_MODEL_HELPERS_FUNCT)
    (MakeRefDom: DEEP_ALIAS_MODEL_REF_DOM_FUNCT)
    (MakeObjDom: DEEP_ALIAS_MODEL_OBJ_DOM_FUNCT)
    (Components: DEEP_ALIAS_MODEL_CAPTURED_RELATIONS) ->
struct
  module SupportHelpers = (DeepAliasModelSupportHelpers (Env) (FeatureFilters)
                             (Components) (SupportVars))
  module RelHelpers = MakeRelHelpers (Env) (SupportHelpers.Cache)
  module UOps = Env.Updates.MakeUOps (Heuristics)
  module Heap = (CombineHeapDomains (Env) (Heuristics) (FeatureFilters)
                   (MakeRefDom (Env) (Heuristics) (UOps) (RelHelpers))
                   (MakeObjDom (Env) (Heuristics) (UOps) (RelHelpers)))
  module Subst = DeepAliasModelSubst (Env) (SupportHelpers)
  include Heap
  include Subst

  open FeatureFilters
  open Features
  open SupportHelpers
  open RelHelpers
  open Env
  open Lexp
  open Bexp
  open Heuristics
  open UOps
  open SCFG

  module Vs = Env.VarSet
  module Vm = Env.VarMap

  let fold_locals_related_ref_pairs f =
    fold_all_ref_pairs begin fun r r' ->
      if Vars.mem r all_arg_refs && Vars.mem r' all_arg_refs
      then ign
      else f r r'
    end

  let typ_support = Vm.empty
    |> fold_all_refs (obj_typs Vm.add)

  let mutable_support = typ_support
    |> fold_mut_alias_vars (alias_rel Vm.add)
    |> fold_all_field_alias_vars (field_alias_rel Vm.add)
    |> fold_all_share_vars (share_rel Vm.add)

  let const_support = Vm.empty
    |> fold_const_alias_vars (alias_rel Vm.add)

  let local_support = Vs.empty
    |> fold_local_refs (obj_typs (fun v _ -> Vs.add v))
    |> fold_locals_related_ref_pairs begin fun r r' s -> s
      |> app' Vs.add (r $~? r')
      |> app' Vs.add (r $.~>? r')
      |> app' Vs.add (r' $.~>? r)
      |> app' Vs.add (r' $><? r)
    end

  (* --- *)

  let footprint = Vm.empty
    |> add_res_obj_typs
    |> fold_arg_refs add_arg_res_obj_typs
    |> fold_arg_field_alias add_arg_res_field_alias
    |> fold_arg_share add_arg_res_share
    |> fold_ret_arg_alias add_ret_arg_alias
    |> fold_ret_arg_field_alias_to add_ret_arg_field_alias_to
    |> fold_ret_arg_field_alias_from add_ret_arg_field_alias_from
    |> fold_ret_arg_share add_ret_arg_share
    |>! fold_exc_arg_alias add_exc_arg_alias
    |>! fold_exc_arg_field_alias_to add_exc_arg_field_alias_to
    |>! fold_exc_arg_field_alias_from add_exc_arg_field_alias_from
    |>! fold_exc_arg_share add_exc_arg_share

  let fold_footprint_comps f i =
    let f v _ = f v and ret c = f (Ret c) and exc c = f (Exc c) in i
    |> (if ret_refp then obj_typs (fun v _ -> ret v) "" else ign)
    |> fold_arg_refs (obj_typs f)
    |> fold_arg_field_alias (field_alias_rel f)
    |> fold_arg_share (share_rel f)
    |> fold_ret_arg_alias (fun a -> ret (res_arg_alias a))
    |> fold_ret_arg_field_alias_to (fun a -> ret (res_arg_field_alias_to a))
    |> fold_ret_arg_field_alias_from (fun a -> ret (res_arg_field_alias_from a))
    |> fold_ret_arg_share (fun a -> ret (res_arg_share a))
    |>! obj_typs (fun v _ -> exc v) ""
    |>! fold_exc_arg_alias (fun a -> exc (res_arg_alias a))
    |>! fold_exc_arg_field_alias_to (fun a -> exc (res_arg_field_alias_to a))
    |>! fold_exc_arg_field_alias_from (fun a -> exc (res_arg_field_alias_from a))
    |>! fold_exc_arg_share (fun a -> exc (res_arg_share a))

  let acc_init_bindings init_bindings = init_bindings
    |>~ fold_local_refs (fun r -> Vm.add (HLevel r) (Lexp bottom))
    |>? fold_local_refs (fun r -> Vm.add (HTaint r) (Bexp ff))
    |> fold_locals_related_ref_pairs begin
      let bff v = Vm.add v (Bexp ff) in fun r r' m -> m
        |> app' bff (r $~? r')
        |> app' bff (r $.~>? r')
        |> app' bff (r' $.~>? r)
        |> app' bff (r' $><? r)
    end

  let acc_arg_related_footprint_constraints ~comp i =
    let typ_over l t = typ_dispatch ~b:t ~l:l ?e:None in
    let ret_lover c _ = (&&.) (Lexp.var c <=.~ Lexp.var (comp (Ret c)))
    and ret_bover c _ = (&&.) (Bexp.var c =>. Bexp.var (comp (Ret c)))
    and exc_lover c _ = (&&.) (Lexp.var c <=.~ Lexp.var (comp (Exc c)))
    and exc_bover c _ = (&&.) (Bexp.var c =>. Bexp.var (comp (Exc c))) in i
    |> fold_arg_refs (obj_typs (typ_over ret_lover ret_bover))
    |>! fold_arg_refs (obj_typs (typ_over exc_lover exc_bover))
    |> fold_arg_field_alias (field_alias_rel ret_bover)
    |>! fold_arg_field_alias (field_alias_rel exc_bover)
    |> fold_arg_share (share_rel ret_bover)
    |>! fold_arg_share (share_rel exc_bover)

  let acc_ret_related_footprint_constraints ~comp r l t i =
    let lover x c = (&&.) (x <=.~ Lexp.var (comp (Ret c)))
    and bover x c = (&&.) (x =>. Bexp.var (comp (Ret c))) in i
    |>~ app' (fun l -> lover l (HLevel "")) l
    |>? app' (fun t -> bover t (HTaint "")) t
    |> app' begin fun r i -> i
      |> (fold_ret_arg_alias
            (fun s -> bover (r $~. s) (res_arg_alias s)))
      |> (fold_ret_arg_field_alias_to
            (fun s -> bover (r $.~>. s) (res_arg_field_alias_to s)))
      |> (fold_ret_arg_field_alias_from
            (fun s -> bover (s $.~>. r) (res_arg_field_alias_from s)))
      |> (fold_ret_arg_share
            (fun s -> bover (r $><. s) (res_arg_share s)))
    end r

  let acc_exc_related_footprint_constraints ~comp r l t i =
    let lover x c = (&&.) (x <=.~ Lexp.var (comp (Exc c)))
    and bover x c = (&&.) (x =>. Bexp.var (comp (Exc c))) in i
    |>!~ lover l (HLevel "")
    |>!? bover t (HTaint "")
    |>! (fold_exc_arg_alias
           (fun s -> bover (r $~. s) (res_arg_alias s)))
    |>! (fold_exc_arg_field_alias_to
           (fun s -> bover (r $.~>. s) (res_arg_field_alias_to s)))
    |>! (fold_exc_arg_field_alias_from
           (fun s -> bover (s $.~>. r) (res_arg_field_alias_from s)))
    |>! (fold_exc_arg_share
           (fun s -> bover (r $><. s) (res_arg_share s)))

  (* --- *)

  let bulk_upgrade ~from_comp ~to_comp =
    let ( ~^% ) r = Lexp.var (from_comp (HLevel r))
    and ( ?^% ) r = Bexp.var (from_comp (HTaint r))
    and ( ?~% ) a = Bexp.var (from_comp (AliasRel a))
    and ( ?.~>% ) fa = Bexp.var (from_comp (FieldAliasRel fa))
    and ( ?><% ) a = Bexp.var (from_comp (ShareRel a))
    and ( $~% ) = alias_var ~mk:(fun v -> Bexp.var (to_comp v))
    and ( $.~>% ) = field_alias_var ~mk:(fun v -> Bexp.var (to_comp v))
    (* and ( $><% ) = share_var ~mk:(fun v -> Bexp.var (to_comp v)) *)
    and ( <-.~^% ) r = setl (to_comp (HLevel r))
    and ( <-.?^% ) r = sett (to_comp (HTaint r))
    and ( <-.~% ) a = setb (to_comp (AliasRel a))
    and ( <-..~>% ) fa = setb (to_comp (FieldAliasRel fa))
    and ( <-.><% ) a = setb (to_comp (ShareRel a)) in
    let restore_obj_level r =
      fold_all_refs begin fun (s: OOBase.var) ->
        s <-.~^% (~^s |.| lite ((s $~% r) ||. (s $.~>% r), ~^%r, bottom))
      end
    and restore_obj_taint r =
      fold_all_refs begin fun (s: OOBase.var) ->
        s <-.?^% (?^s ||. bite ((s $~% r) ||. (s $.~>% r), ?^%r, ff))
      end
    in
    fun u -> begin u
        |> fold_mut_alias_vars (fun a -> a <-.~% ?~%a)
        |> fold_all_field_alias_vars (fun a -> a <-..~>% ?.~>%a)
        |> fold_all_share_vars (fun a -> a <-.><% ?><%a)
        |>~ fold_all_refs restore_obj_level
        |>? fold_all_refs restore_obj_taint
    end

  (* --- *)

  let apply_comp_effect ~ctxtrefs ~lcap v e = match v, e with
    | HLevel _, Lexp _ when Features.track_levels = NoFlows -> ign
    | HTaint _, Bexp _ when not Features.track_taints -> ign
    | HLevel "" as v, Lexp e ->
        setl v (lite (e <=.~ lcap, e, lcap))
    | HLevel _ as v, Lexp (LVar v') when V.equal v v' -> ign
    | HLevel r, Lexp l ->
        upgrade_reachable_obj_levels ~ctxtrefs r l lcap
    | HTaint "" as v, Bexp e ->
        setb v e
    | HTaint _ as v, Bexp (BVar v') when V.equal v v' -> ign
    | HTaint r, Bexp t ->
        upgrade_reachable_obj_taints ~ctxtrefs r t
    | AliasRel a, Bexp c ->
        cond_deep_join_refs c (AliasRelVar.get a)
    | FieldAliasRel (r, s), Bexp c ->
        cond_deep_store_ref c r s
    | ShareRel a, Bexp c ->
        cond_deep_join_refs c (ShareRelVar.get a)
    | _ -> ign

end

(* --- *)

(** {3 Field-share model} *)

module type FIELD_SHARE_MODEL_HELPERS_FUNCT =
  functor (Env: FINITE_COMPS_ENV) (RS: RefRels.FIELD_SHARE_MODEL_RELS) ->
    RefRels.FIELD_SHARE_MODEL_HELPERS with module Env := Env

module type FIELD_SHARE_MODEL_REF_DOM_FUNCT =
  functor (Env: FINITE_COMPS_ENV) (Heuristics: Env.HEURISTICS)
    (UOps: Env.Updates.UOPS)
    (RelHelpers: RefRels.FIELD_SHARE_MODEL_HELPERS with module Env := Env) ->
      REF_DOM with module Env := Env

module type FIELD_SHARE_MODEL_OBJ_DOM_FUNCT =
  functor (Env: FINITE_COMPS_ENV) (Heuristics: Env.HEURISTICS)
    (UOps: Env.Updates.UOPS)
    (RelHelpers: RefRels.FIELD_SHARE_MODEL_HELPERS with module Env := Env) ->
      OBJ_DOM with module Env := Env

module FieldShareModelCommonDefs =
  functor (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
    (Heuristics: Env.HEURISTICS) (SupportVars: SUPPORT_VARS)
    (MakeRelHelpers: FIELD_SHARE_MODEL_HELPERS_FUNCT)
    (MakeRefDom: FIELD_SHARE_MODEL_REF_DOM_FUNCT)
    (MakeObjDom: FIELD_SHARE_MODEL_OBJ_DOM_FUNCT)
    (Components: FIELD_SHARE_MODEL_CAPTURED_RELATIONS) ->
struct
  module SupportHelpers = (FieldShareModelSupportHelpers (Env) (FeatureFilters)
                             (Components) (SupportVars))
  module RelHelpers = MakeRelHelpers (Env) (SupportHelpers.Cache)
  module UOps = Env.Updates.MakeUOps (Heuristics)
  module Heap = (CombineHeapDomains (Env) (Heuristics) (FeatureFilters)
                   (MakeRefDom (Env) (Heuristics) (UOps) (RelHelpers))
                   (MakeObjDom (Env) (Heuristics) (UOps) (RelHelpers)))
  module Subst = FieldShareModelSubst (Env) (SupportHelpers)
  include Heap
  include Subst

  open FeatureFilters
  open Features
  open SupportHelpers
  open RelHelpers
  open Env
  open Lexp
  open Bexp
  open Heuristics
  open UOps
  open SCFG

  module Vs = Env.VarSet
  module Vm = Env.VarMap

  let fold_locals_related_ref_pairs f =
    fold_all_ref_pairs begin fun r r' ->
      if Vars.mem r all_arg_refs && Vars.mem r' all_arg_refs
      then ign
      else f r r'
    end

  let typ_support = Vm.empty
    |> fold_all_refs (obj_typs Vm.add)

  let mutable_support = typ_support
    |> fold_mut_alias_vars (alias_rel Vm.add)
    |> fold_all_field_share_vars (field_share_rel Vm.add)

  let const_support = Vm.empty
    |> fold_const_alias_vars (alias_rel Vm.add)

  let local_support = Vs.empty
    |> fold_local_refs (obj_typs (fun v _ -> Vs.add v))
    |> fold_locals_related_ref_pairs begin fun r r' s -> s
      |> app' Vs.add (r $~? r')
      |> app' Vs.add (r $.><? r')
      |> app' Vs.add (r' $.><? r)
    end

  (* --- *)

  let footprint = Vm.empty
    |> add_res_obj_typs
    |> fold_arg_refs add_arg_res_obj_typs
    |> fold_arg_field_share add_arg_res_field_share
    |> fold_ret_arg_alias add_ret_arg_alias
    |> fold_ret_arg_field_share_to add_ret_arg_field_share_to
    |> fold_ret_arg_field_share_from add_ret_arg_field_share_from
    |>! fold_exc_arg_alias add_exc_arg_alias
    |>! fold_exc_arg_field_share_to add_exc_arg_field_share_to
    |>! fold_exc_arg_field_share_from add_exc_arg_field_share_from

  let fold_footprint_comps f i =
    let f v _ = f v and ret c = f (Ret c) and exc c = f (Exc c) in i
    |> (if ret_refp then obj_typs (fun v _ -> ret v) "" else ign)
    |> fold_arg_refs (obj_typs f)
    |> fold_arg_field_share (field_share_rel f)
    |> fold_ret_arg_alias (fun a -> ret (res_arg_alias a))
    |> fold_ret_arg_field_share_to (fun a -> ret (res_arg_field_share_to a))
    |> fold_ret_arg_field_share_from (fun a -> ret (res_arg_field_share_from a))
    |>! obj_typs (fun v _ -> exc v) ""
    |>! fold_exc_arg_alias (fun a -> exc (res_arg_alias a))
    |>! fold_exc_arg_field_share_to (fun a -> exc (res_arg_field_share_to a))
    |>! fold_exc_arg_field_share_from (fun a -> exc (res_arg_field_share_from a))

  let acc_init_bindings init_bindings = init_bindings
    |>~ fold_local_refs (fun r -> Vm.add (HLevel r) (Lexp bottom))
    |>? fold_local_refs (fun r -> Vm.add (HTaint r) (Bexp ff))
    |> fold_locals_related_ref_pairs begin
      let bff v = Vm.add v (Bexp ff) in fun r r' m -> m
        |> app' bff (r $~? r')
        |> app' bff (r $.><? r')
        |> app' bff (r' $.><? r)
    end

  let acc_arg_related_footprint_constraints ~comp i =
    let typ_over l t = typ_dispatch ~b:t ~l:l ?e:None in
    let ret_lover c _ = (&&.) (Lexp.var c <=.~ Lexp.var (comp (Ret c)))
    and ret_bover c _ = (&&.) (Bexp.var c =>. Bexp.var (comp (Ret c)))
    and exc_lover c _ = (&&.) (Lexp.var c <=.~ Lexp.var (comp (Exc c)))
    and exc_bover c _ = (&&.) (Bexp.var c =>. Bexp.var (comp (Exc c))) in i
    |> fold_arg_refs (obj_typs (typ_over ret_lover ret_bover))
    |>! fold_arg_refs (obj_typs (typ_over exc_lover exc_bover))
    |> fold_arg_field_share (field_share_rel ret_bover)
    |>! fold_arg_field_share (field_share_rel exc_bover)

  let acc_ret_related_footprint_constraints ~comp r l t i =
    let lover x c = (&&.) (x <=.~ Lexp.var (comp (Ret c)))
    and bover x c = (&&.) (x =>. Bexp.var (comp (Ret c))) in i
    |>~ app' (fun l -> lover l (HLevel "")) l
    |>? app' (fun t -> bover t (HTaint "")) t
    |> app' begin fun r i -> i
      |> (fold_ret_arg_alias
            (fun s -> bover (r $~. s) (res_arg_alias s)))
      |> (fold_ret_arg_field_share_to
            (fun s -> bover (r $.><. s) (res_arg_field_share_to s)))
      |> (fold_ret_arg_field_share_from
            (fun s -> bover (s $.><. r) (res_arg_field_share_from s)))
    end r

  let acc_exc_related_footprint_constraints ~comp r l t i =
    let lover x c = (&&.) (x <=.~ Lexp.var (comp (Exc c)))
    and bover x c = (&&.) (x =>. Bexp.var (comp (Exc c))) in i
    |>!~ lover l (HLevel "")
    |>!? bover t (HTaint "")
    |>! (fold_exc_arg_alias
           (fun s -> bover (r $~. s) (res_arg_alias s)))
    |>! (fold_exc_arg_field_share_to
           (fun s -> bover (r $.><. s) (res_arg_field_share_to s)))
    |>! (fold_exc_arg_field_share_from
           (fun s -> bover (s $.><. r) (res_arg_field_share_from s)))

  (* --- *)

  let bulk_upgrade ~from_comp ~to_comp =
    let ( ~^% ) r = Lexp.var (from_comp (HLevel r))
    and ( ?^% ) r = Bexp.var (from_comp (HTaint r))
    and ( ?~% ) a = Bexp.var (from_comp (AliasRel a))
    and ( ?.><% ) fs = Bexp.var (from_comp (FieldShareRel fs))
    and ( $~% ) = alias_var ~mk:(fun v -> Bexp.var (to_comp v))
    and ( $.><% ) = field_share_var ~mk:(fun v -> Bexp.var (to_comp v))
    and ( <-.~^% ) r = setl (to_comp (HLevel r))
    and ( <-.?^% ) r = sett (to_comp (HTaint r))
    and ( <-.~% ) a = setb (to_comp (AliasRel a))
    and ( <-..><% ) fs = setb (to_comp (FieldShareRel fs)) in
    let restore_obj_level r =
      fold_all_refs begin fun (s: OOBase.var) ->
        s <-.~^% (~^s |.| lite ((s $~% r) ||. (s $.><% r), ~^%r, bottom))
      end
    and restore_obj_taint r =
      fold_all_refs begin fun (s: OOBase.var) ->
        s <-.?^% (?^s ||. bite ((s $~% r) ||. (s $.><% r), ?^%r, ff))
      end
    in
    fun u -> begin u
        |> fold_mut_alias_vars (fun a -> a <-.~% ?~%a)
        |> fold_all_field_share_vars (fun a -> a <-..><% ?.><%a)
        |>~ fold_all_refs restore_obj_level
        |>? fold_all_refs restore_obj_taint
    end

  (* --- *)

  let apply_comp_effect ~ctxtrefs ~lcap v e = match v, e with
    | HLevel _, Lexp _ when Features.track_levels = NoFlows -> ign
    | HTaint _, Bexp _ when not Features.track_taints -> ign
    | HLevel "" as v, Lexp e ->
        setl v (lite (e <=.~ lcap, e, lcap))
    | HLevel _ as v, Lexp (LVar v') when V.equal v v' -> ign
    | HLevel r, Lexp l ->
        upgrade_reachable_obj_levels ~ctxtrefs r l lcap
    | HTaint "" as v, Bexp e ->
        setb v e
    | HTaint _ as v, Bexp (BVar v') when V.equal v v' -> ign
    | HTaint r, Bexp t ->
        upgrade_reachable_obj_taints ~ctxtrefs r t
    | AliasRel a, Bexp c ->
        cond_deep_join_refs c (AliasRelVar.get a)
    | FieldShareRel (r, s), Bexp c ->
        cond_deep_store_ref c r s
    | _ -> ign

end

(* --- *)

(** {3 Share model} *)

module type SHARE_MODEL_HELPERS_FUNCT =
  functor (Env: FINITE_COMPS_ENV) (RS: RefRels.SHARE_MODEL_RELS) ->
    RefRels.SHARE_MODEL_HELPERS with module Env := Env

module type SHARE_MODEL_REF_DOM_FUNCT =
  functor (Env: FINITE_COMPS_ENV) (Heuristics: Env.HEURISTICS)
    (UOps: Env.Updates.UOPS)
    (RelHelpers: RefRels.SHARE_MODEL_HELPERS with module Env := Env) ->
      REF_DOM with module Env := Env

module type SHARE_MODEL_OBJ_DOM_FUNCT =
  functor (Env: FINITE_COMPS_ENV) (Heuristics: Env.HEURISTICS)
    (UOps: Env.Updates.UOPS)
    (RelHelpers: RefRels.SHARE_MODEL_HELPERS with module Env := Env) ->
      OBJ_DOM with module Env := Env

module ShareModelCommonDefs =
  functor (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
    (Heuristics: Env.HEURISTICS) (SupportVars: SUPPORT_VARS)
    (MakeRelHelpers: SHARE_MODEL_HELPERS_FUNCT)
    (MakeRefDom: SHARE_MODEL_REF_DOM_FUNCT)
    (MakeObjDom: SHARE_MODEL_OBJ_DOM_FUNCT)
    (Components: SHARE_MODEL_CAPTURED_RELATIONS) ->
struct
  module SupportHelpers = (ShareModelSupportHelpers (Env) (FeatureFilters)
                             (Components) (SupportVars))
  module RelHelpers = MakeRelHelpers (Env) (SupportHelpers.Cache)
  module UOps = Env.Updates.MakeUOps (Heuristics)
  module Heap = (CombineHeapDomains (Env) (Heuristics) (FeatureFilters)
                   (MakeRefDom (Env) (Heuristics) (UOps) (RelHelpers))
                   (MakeObjDom (Env) (Heuristics) (UOps) (RelHelpers)))
  module Subst = ShareModelSubst (Env) (SupportHelpers)
  include Heap
  include Subst

  open FeatureFilters
  open Features
  open SupportHelpers
  open RelHelpers
  open Env
  open Lexp
  open Bexp
  open Heuristics
  open UOps
  open SCFG

  module Vs = Env.VarSet
  module Vm = Env.VarMap

  let fold_locals_related_ref_pairs f =
    fold_all_ref_pairs begin fun r r' ->
      if Vars.mem r all_arg_refs && Vars.mem r' all_arg_refs
      then ign
      else f r r'
    end

  let typ_support = Vm.empty
    |> fold_all_refs (obj_typs Vm.add)

  let mutable_support = typ_support
    |> fold_all_share_vars (share_rel Vm.add)

  let const_support = Vm.empty

  let local_support = Vs.empty
    |> fold_local_refs (obj_typs (fun v _ -> Vs.add v))
    |> fold_locals_related_ref_pairs begin fun r r' s -> s
      |> app' Vs.add (r $><? r')
    end

  let footprint = Vm.empty
    |> add_res_obj_typs
    |> fold_arg_refs add_arg_res_obj_typs
    |> fold_arg_share add_arg_res_share
    |> fold_ret_arg_share add_ret_arg_share
    |>! fold_exc_arg_share add_exc_arg_share

  let fold_footprint_comps f i =
    let f v _ = f v and ret c = f (Ret c) and exc c = f (Exc c) in i
    |> (if ret_refp then obj_typs (fun v _ -> ret v) "" else ign)
    |> fold_arg_refs (obj_typs f)
    |> fold_arg_share (share_rel f)
    |> fold_ret_arg_share (fun a -> ret (res_arg_share a))
    |>! obj_typs (fun v _ -> exc v) ""
    |>! fold_exc_arg_share (fun a -> exc (res_arg_share a))

  (* --- *)

  let acc_init_bindings init_bindings = let open Env.VarMap in init_bindings
    |>~ fold_local_refs (fun r -> add (HLevel r) (Lexp bottom))
    |>? fold_local_refs (fun r -> add (HTaint r) (Bexp ff))
    |> fold_locals_related_ref_pairs begin
      let bff v = add v (Bexp ff) in fun r r' m -> m
        |> app' bff (r $><? r')
    end

  (* --- *)

  let acc_arg_related_footprint_constraints ~comp i =
    let typ_over l t = typ_dispatch ~b:t ~l:l ?e:None in
    let ret_lover c _ = (&&.) (Lexp.var c <=.~ Lexp.var (comp (Ret c)))
    and ret_bover c _ = (&&.) (Bexp.var c =>. Bexp.var (comp (Ret c)))
    and exc_lover c _ = (&&.) (Lexp.var c <=.~ Lexp.var (comp (Exc c)))
    and exc_bover c _ = (&&.) (Bexp.var c =>. Bexp.var (comp (Exc c))) in i
    |> fold_arg_refs (obj_typs (typ_over ret_lover ret_bover))
    |>! fold_arg_refs (obj_typs (typ_over exc_lover exc_bover))
    |> fold_arg_share (share_rel ret_bover)
    |>! fold_arg_share (share_rel exc_bover)

  let acc_ret_related_footprint_constraints ~comp r l t i =
    let lover x c = (&&.) (x <=.~ Lexp.var (comp (Ret c)))
    and bover x c = (&&.) (x =>. Bexp.var (comp (Ret c))) in i
    |>~ app' (fun l -> lover l (HLevel "")) l
    |>? app' (fun t -> bover t (HTaint "")) t
    |> app' begin fun r i -> i
      |> fold_ret_arg_share (fun s -> bover (r $><. s) (res_arg_share s))
    end r

  let acc_exc_related_footprint_constraints ~comp r l t i =
    let lover x c = (&&.) (x <=.~ Lexp.var (comp (Exc c)))
    and bover x c = (&&.) (x =>. Bexp.var (comp (Exc c))) in i
    |>!~ lover l (HLevel "")
    |>!? bover t (HTaint "")
    |>! fold_exc_arg_share (fun s -> bover (r $><. s) (res_arg_share s))

  (* --- *)

  let bulk_upgrade ~from_comp ~to_comp =
    let ( ~^% ) r = Lexp.var (from_comp (HLevel r))
    and ( ?^% ) r = Bexp.var (from_comp (HTaint r))
    and ( ?><% ) a = Bexp.var (from_comp (ShareRel a))
    and ( $><% ) = share_var ~mk:(fun v -> Bexp.var (to_comp v))
    and ( <-.~^% ) r = setl (to_comp (HLevel r))
    and ( <-.?^% ) r = sett (to_comp (HTaint r))
    and ( <-.><% ) a = setb (to_comp (ShareRel a)) in
    let restore_obj_level r =
      fold_all_refs begin fun (s: OOBase.var) ->
        s <-.~^% (~^s |.| lite (r $><% s, ~^%r, bottom))
      end
    and restore_obj_taint r =
      fold_all_refs begin fun (s: OOBase.var) ->
        s <-.?^% (?^s ||. bite (r $><% s, ?^%r, ff))
      end
    in
    fun u -> begin u
        |> fold_all_share_vars (fun a -> a <-.><% ?><%a)
        |>~ fold_all_refs restore_obj_level
        |>? fold_all_refs restore_obj_taint
    end

  (* --- *)

  let apply_comp_effect ~ctxtrefs ~lcap v e = match v, e with
    | HLevel _, Lexp _ when Features.track_levels = NoFlows -> ign
    | HTaint _, Bexp _ when not Features.track_taints -> ign
    | HLevel "" as v, Lexp e ->
        setl v (lite (e <=.~ lcap, e, lcap))
    | HLevel _ as v, Lexp (LVar v') when V.equal v v' -> ign
    | HLevel r, Lexp l ->
        upgrade_reachable_obj_levels ~ctxtrefs r l lcap
    | HTaint "" as v, Bexp e ->
        setb v e
    | HTaint _ as v, Bexp (BVar v') when V.equal v v' -> ign
    | HTaint r, Bexp t ->
        upgrade_reachable_obj_taints ~ctxtrefs r t
    | ShareRel a, Bexp c ->
        cond_deep_join_refs c (ShareRelVar.get a)
    | _ -> ign

end

(* --- *)

(** {3 Connect model} *)

module type CONNECT_MODEL_HELPERS_FUNCT =
  functor (Env: FINITE_COMPS_ENV) (RS: RefRels.CONNECT_MODEL_RELS) ->
    RefRels.CONNECT_MODEL_HELPERS with module Env := Env

module type CONNECT_MODEL_REF_DOM_FUNCT =
  functor (Env: FINITE_COMPS_ENV) (Heuristics: Env.HEURISTICS)
    (UOps: Env.Updates.UOPS)
    (RelHelpers: RefRels.CONNECT_MODEL_HELPERS with module Env := Env) ->
      REF_DOM with module Env := Env

module type CONNECT_MODEL_OBJ_DOM_FUNCT =
  functor (Env: FINITE_COMPS_ENV) (Heuristics: Env.HEURISTICS)
    (UOps: Env.Updates.UOPS)
    (RelHelpers: RefRels.CONNECT_MODEL_HELPERS with module Env := Env) ->
      OBJ_DOM with module Env := Env

module ConnectModelCommonDefs =
  functor (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
    (Heuristics: Env.HEURISTICS) (SupportVars: SUPPORT_VARS)
    (MakeRelHelpers: CONNECT_MODEL_HELPERS_FUNCT)
    (MakeRefDom: CONNECT_MODEL_REF_DOM_FUNCT)
    (MakeObjDom: CONNECT_MODEL_OBJ_DOM_FUNCT)
    (Components: CONNECT_MODEL_CAPTURED_RELATIONS) ->
struct
  module SupportHelpers = (ConnectModelSupportHelpers (Env) (FeatureFilters)
                             (Components) (SupportVars))
  module RelHelpers = MakeRelHelpers (Env) (SupportHelpers.Cache)
  module UOps = Env.Updates.MakeUOps (Heuristics)
  module Heap = (CombineHeapDomains (Env) (Heuristics) (FeatureFilters)
                   (MakeRefDom (Env) (Heuristics) (UOps) (RelHelpers))
                   (MakeObjDom (Env) (Heuristics) (UOps) (RelHelpers)))
  module Subst = ConnectModelSubst (Env) (SupportHelpers)
  include Heap
  include Subst

  open FeatureFilters
  open Features
  open SupportHelpers
  open RelHelpers
  open Env
  open Lexp
  open Bexp
  open Heuristics
  open UOps
  open SCFG

  module Vs = Env.VarSet
  module Vm = Env.VarMap

  let fold_locals_related_ref_pairs f =
    fold_all_ref_pairs begin fun r r' ->
      if Vars.mem r all_arg_refs && Vars.mem r' all_arg_refs
      then ign
      else f r r'
    end

  let typ_support = Vm.empty
    |> fold_all_refs (obj_typs Vm.add)

  let mutable_support = typ_support
    |> fold_all_connect_vars (connect_rel Vm.add)

  let const_support = Vm.empty

  let local_support = Vs.empty
    |> fold_local_refs (obj_typs (fun v _ -> Vs.add v))
    |> fold_locals_related_ref_pairs begin fun r r' s -> s
      |> app' Vs.add (r $~? r')
    end

  let footprint = Vm.empty
    |> add_res_obj_typs
    |> fold_arg_refs add_arg_res_obj_typs
    |> fold_arg_connect add_arg_res_connect
    |> fold_ret_arg_connect add_ret_arg_connect
    |>! fold_exc_arg_connect add_exc_arg_connect

  let fold_footprint_comps f i =
    let f v _ = f v and ret c = f (Ret c) and exc c = f (Exc c) in i
    |> (if ret_refp then obj_typs (fun v _ -> ret v) "" else ign)
    |> fold_arg_refs (obj_typs f)
    |> fold_arg_connect (connect_rel f)
    |> fold_ret_arg_connect (fun a -> ret (res_arg_connect a))
    |>! obj_typs (fun v _ -> exc v) ""
    |>! fold_exc_arg_connect (fun a -> exc (res_arg_connect a))

  (* --- *)

  let acc_init_bindings init_bindings = let open Env.VarMap in init_bindings
    |>~ fold_local_refs (fun r -> add (HLevel r) (Lexp bottom))
    |>? fold_local_refs (fun r -> add (HTaint r) (Bexp ff))
    |> fold_locals_related_ref_pairs begin
      let bff v = add v (Bexp ff) in fun r r' m -> m
        |> app' bff (r $~? r')
    end

  (* --- *)

  let acc_arg_related_footprint_constraints ~comp i =
    let typ_over l t = typ_dispatch ~b:t ~l:l ?e:None in
    let ret_lover c _ = (&&.) (Lexp.var c <=.~ Lexp.var (comp (Ret c)))
    and ret_bover c _ = (&&.) (Bexp.var c =>. Bexp.var (comp (Ret c)))
    and exc_lover c _ = (&&.) (Lexp.var c <=.~ Lexp.var (comp (Exc c)))
    and exc_bover c _ = (&&.) (Bexp.var c =>. Bexp.var (comp (Exc c))) in i
    |> fold_arg_refs (obj_typs (typ_over ret_lover ret_bover))
    |>! fold_arg_refs (obj_typs (typ_over exc_lover exc_bover))
    |> fold_arg_connect (connect_rel ret_bover)
    |>! fold_arg_connect (connect_rel exc_bover)

  let acc_ret_related_footprint_constraints ~comp r l t i =
    let lover x c = (&&.) (x <=.~ Lexp.var (comp (Ret c)))
    and bover x c = (&&.) (x =>. Bexp.var (comp (Ret c))) in i
    |>~ app' (fun l -> lover l (HLevel "")) l
    |>? app' (fun t -> bover t (HTaint "")) t
    |> app' begin fun r i -> i
      |> fold_ret_arg_connect (fun s -> bover (r $~. s) (res_arg_connect s))
    end r

  let acc_exc_related_footprint_constraints ~comp r l t i =
    let lover x c = (&&.) (x <=.~ Lexp.var (comp (Exc c)))
    and bover x c = (&&.) (x =>. Bexp.var (comp (Exc c))) in i
    |>!~ lover l (HLevel "")
    |>!? bover t (HTaint "")
    |>! fold_exc_arg_connect (fun s -> bover (r $~. s) (res_arg_connect s))

  (* --- *)

  let bulk_upgrade ~from_comp ~to_comp =
    let ( ~^% ) r = Lexp.var (from_comp (HLevel r))
    and ( ?^% ) r = Bexp.var (from_comp (HTaint r))
    and ( ?~% ) a = Bexp.var (from_comp (AliasRel a))
    and ( $~% ) = connect_var ~mk:(fun v -> Bexp.var (to_comp v))
    and ( <-.~^% ) r = setl (to_comp (HLevel r))
    and ( <-.?^% ) r = sett (to_comp (HTaint r))
    and ( <-.~% ) a = setb (to_comp (AliasRel a)) in
    let restore_obj_level r =
      fold_all_refs begin fun (s: OOBase.var) ->
        s <-.~^% (~^s |.| lite (r $~% s, ~^%r, bottom))
      end
    and restore_obj_taint r =
      fold_all_refs begin fun (s: OOBase.var) ->
        s <-.?^% (?^s ||. bite (r $~% s, ?^%r, ff))
      end
    in
    fun u -> begin u
        |> fold_all_connect_vars (fun a -> a <-.~% ?~%a)
        |>~ fold_all_refs restore_obj_level
        |>? fold_all_refs restore_obj_taint
    end

  (* --- *)

  let apply_comp_effect ~ctxtrefs ~lcap v e = match v, e with
    | HLevel _, Lexp _ when Features.track_levels = NoFlows -> ign
    | HTaint _, Bexp _ when not Features.track_taints -> ign
    | HLevel "" as v, Lexp e ->
        setl v (lite (e <=.~ lcap, e, lcap))
    | HLevel _ as v, Lexp (LVar v') when V.equal v v' -> ign
    | HLevel r, Lexp l ->
        upgrade_reachable_obj_levels ~ctxtrefs r l lcap
    | HTaint "" as v, Bexp e ->
        setb v e
    | HTaint _ as v, Bexp (BVar v') when V.equal v v' -> ign
    | HTaint r, Bexp t ->
        upgrade_reachable_obj_taints ~ctxtrefs r t
    | AliasRel a, Bexp c ->
        cond_deep_join_refs c (AliasRelVar.get a)
    | _ -> ign

end

(* -------------------------------------------------------------------------- *)

(** {2 Definitions of Symbolic Heap Domains} *)

(** {3 Deep-alias} *)

module DeepAliasRelations = struct
  let capture_alias = true
  let capture_field_alias = true
  let capture_share = false
end

module DeepAlias: FINITE_COMPS_HEAP =
  functor (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
    (Heuristics: Env.HEURISTICS) (SupportVars: SUPPORT_VARS) ->
struct

  include DeepAliasModelCommonDefs (Env) (FeatureFilters) (Heuristics)
    (SupportVars) (RefRels.DeepAliasHelpers) (DeepAliasRefDom)
    (DeepAliasObjDom) (DeepAliasRelations)

  (* --- *)

  open FeatureFilters
  open SupportHelpers
  open Env
  open Bexp
  let arg_alias_invariant, arg_alias_invariant_support_size =
    let module Inv = RefRels.DeepAliasInvariant (Env) (Heap) (struct
      let all_refs = all_arg_refs
      let alias_vars = all_arg_alias
      let field_alias_vars = all_arg_field_alias
      let share_vars = all_arg_share
    end) in
    let inv = begin tt                             (* TODO: check w.r.t share *)
        |> Inv.alias_rel_is_eqrel
        |> Inv.field_alias_rel_is_transitive
        |> Inv.field_alias_rel_transitivity_carries_over_alias_rel
        |>~ Inv.sound_levels_wrt_alias_rel
        |>~ Inv.sound_levels_wrt_field_alias_rel
        |>? Inv.sound_taints_wrt_alias_rel
        |>? Inv.sound_taints_wrt_field_alias_rel
    end in
    let supp_size = Vs.cardinal (Bexp.fold_supp Vs.add inv Vs.empty) in
    (* Log.d3 logger "Arg prms: %a" Vars.print all_arg_prms; *)
    Log.d3 logger "Arg refs: %a" Vars.print all_arg_refs;
    Log.d3 logger "Arg alias invariant:@\n%a" Env.Bexp.print inv;
    Log.d logger "Arg alias invariant has %u variables" supp_size;
    inv, supp_size;;

  (* let heap_invariant i = *)
  (*   let module Inv = MakeAliasInv (Body) in *)
  (*   let inv = begin tt *)
  (*       |> Inv.alias_rel_is_eqrel *)
  (*       |> Inv.field_alias_rel_is_transitive *)
  (*       |> Inv.field_alias_rel_transitivity_carries_over_alias_rel *)
  (*       |>~ Inv.sound_levels_wrt_alias_rel *)
  (*       |>~ Inv.sound_levels_wrt_field_alias_rel *)
  (*       |>? Inv.sound_taints_wrt_alias_rel *)
  (*       |>? Inv.sound_taints_wrt_field_alias_rel *)
  (*   end in *)
  (*   Log.d logger "Heap invariant:@\n%a" SCFGEnv.Bexp.print inv; *)
  (*   i &&. inv *)

  open Captured
  let stats: int cardinals =
    let cr = Vars.cardinal all_arg_refs in
    let mr = Vars.cardinal all_refs - cr in
    let na = AliasRelVars.cardinal alias_vars
    and cna = AliasRelVars.cardinal const_alias_vars
    and nfa = FieldAliasRelVars.cardinal field_alias_vars in
    Log.i logger "@[Heap@ abstraction@ features@ %u@ alias@ variable%(%)@ \
                    (%u@ of@ which@ %(%))@ and@ %u@ field-alias@ variable%(%).\
                    @]"
      na (if na <= 1 then "" else "s")
      cna (if cna <= 1 then "is@ constant" else "are@ constants")
      nfa (if nfa <= 1 then "" else "s");
    {
      nb_typ_vars = Vm.cardinal typ_support;
      nb_mut_refs = mr;
      nb_const_refs = cr;
      nb_mut_rel_vars = na - cna + nfa;
      nb_const_rel_vars = cna;
    }

end

(* ----------------------------------------------------------------------------- *)

(** {3 Shallow-alias} *)

module ShallowAliasRelations = struct
  let capture_alias = true
  let capture_field_alias = false
  let capture_share = false
end

module ShallowAlias: FINITE_COMPS_HEAP =
  functor (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
    (Heuristics: Env.HEURISTICS) (SupportVars: SUPPORT_VARS) ->
struct

  include DeepAliasModelCommonDefs (Env) (FeatureFilters) (Heuristics)
    (SupportVars) (RefRels.ShallowAliasHelpers) (DeepAliasRefDom)
    (DeepAliasObjDom) (ShallowAliasRelations)

  (* --- *)

  open FeatureFilters
  open SupportHelpers
  open Env
  open Bexp

  let arg_alias_invariant, arg_alias_invariant_support_size =
    let module Inv = RefRels.ShallowAliasInvariant (Env) (Heap) (struct
      let all_refs = all_arg_refs
      let alias_vars = all_arg_alias
      let field_alias_vars = all_arg_field_alias
      let share_vars = all_arg_share
    end) in
    let inv = begin tt                             (* TODO: check w.r.t share *)
        |> Inv.alias_rel_is_eqrel
        |>~ Inv.sound_levels_wrt_alias_rel
        |>? Inv.sound_taints_wrt_alias_rel
    end in
    let supp_size = Vs.cardinal (Bexp.fold_supp Vs.add inv Vs.empty) in
    (* Log.d3 logger "Arg prms: %a" Vars.print all_arg_prms; *)
    Log.d3 logger "Arg refs: %a" Vars.print all_arg_refs;
    Log.d3 logger "Arg alias invariant:@\n%a" Env.Bexp.print inv;
    Log.d logger "Arg alias invariant has %u variables" supp_size;
    inv, supp_size;;

  (* let heap_invariant i = *)
  (*   let module Inv = MakeAliasInv (Body) in *)
  (*   let inv = begin tt *)
  (*       |> Inv.alias_rel_is_eqrel *)
  (*       |> Inv.field_alias_rel_is_transitive *)
  (*       |> Inv.field_alias_rel_transitivity_carries_over_alias_rel *)
  (*       |>~ Inv.sound_levels_wrt_alias_rel *)
  (*       |>~ Inv.sound_levels_wrt_field_alias_rel *)
  (*       |>? Inv.sound_taints_wrt_alias_rel *)
  (*       |>? Inv.sound_taints_wrt_field_alias_rel *)
  (*   end in *)
  (*   Log.d logger "Heap invariant:@\n%a" SCFGEnv.Bexp.print inv; *)
  (*   i &&. inv *)

  open Captured
  let stats: int cardinals =
    let cr = Vars.cardinal all_arg_refs in
    let mr = Vars.cardinal all_refs - cr in
    let na = AliasRelVars.cardinal alias_vars
    and cna = AliasRelVars.cardinal const_alias_vars in
    Log.i logger "@[Heap@ abstraction@ features@ %u@ alias@ variable%(%)@ \
                    (%u@ of@ which@ %(%)).@]"
      na (if na <= 1 then "" else "s")
      cna (if cna <= 1 then "is@ constant" else "are@ constants");
    {
      nb_typ_vars = Vm.cardinal typ_support;
      nb_mut_refs = mr;
      nb_const_refs = cr;
      nb_mut_rel_vars = na - cna;
      nb_const_rel_vars = cna;
    }

end

(* -------------------------------------------------------------------------- *)

(** {3 Dumb-alias}

    Fully flow-insensitive variant. *)

module DumbAliasRelations = struct
  let capture_alias = false
  let capture_field_alias = false
  let capture_share = false
end

module DumbAlias: FINITE_COMPS_HEAP =
  functor (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
    (Heuristics: Env.HEURISTICS) (SupportVars: SUPPORT_VARS) ->
struct

  include DeepAliasModelCommonDefs (Env) (FeatureFilters) (Heuristics)
    (SupportVars) (RefRels.DumbAliasHelpers) (DeepAliasRefDom)
    (DeepAliasObjDom) (DumbAliasRelations)

  (* --- *)

  open FeatureFilters
  open SupportHelpers
  open Env
  open Bexp
  let arg_alias_invariant, arg_alias_invariant_support_size =
    let module Inv = RefRels.DumbAliasInvariant (Env) (Heap) (struct
      let all_refs = all_arg_refs
      let alias_vars = all_arg_alias
      let field_alias_vars = all_arg_field_alias
      let share_vars = all_arg_share
    end) in
    let inv = begin tt                                         (* TODO: check *)
        |>~ Inv.sound_levels_wrt_alias_rel
        |>? Inv.sound_taints_wrt_alias_rel
    end in
    let supp_size = Vs.cardinal (Bexp.fold_supp Vs.add inv Vs.empty) in
    (* Log.d3 logger "Arg prms: %a" Vars.print all_arg_prms; *)
    Log.d3 logger "Arg refs: %a" Vars.print all_arg_refs;
    Log.d3 logger "Arg alias invariant:@\n%a" Env.Bexp.print inv;
    Log.d logger "Arg alias invariant has %u variables" supp_size;
    inv, supp_size;;

  (* let heap_invariant i = *)
  (*   let module Inv = MakeAliasInv (Body) in *)
  (*   let inv = begin tt *)
  (*       |> Inv.alias_rel_is_eqrel *)
  (*       |> Inv.field_alias_rel_is_transitive *)
  (*       |> Inv.field_alias_rel_transitivity_carries_over_alias_rel *)
  (*       |>~ Inv.sound_levels_wrt_alias_rel *)
  (*       |>~ Inv.sound_levels_wrt_field_alias_rel *)
  (*       |>? Inv.sound_taints_wrt_alias_rel *)
  (*       |>? Inv.sound_taints_wrt_field_alias_rel *)
  (*   end in *)
  (*   Log.d logger "Heap invariant:@\n%a" SCFGEnv.Bexp.print inv; *)
  (*   i &&. inv *)

  open Captured
  let stats: int cardinals =
    let cr = Vars.cardinal all_arg_refs in
    let mr = Vars.cardinal all_refs - cr in
    let na = 0 and cna = 0 in
    Log.i logger "@[Heap@ abstraction@ features@ %u@ alias@ variable%(%)@ \
                    (%u@ of@ which@ %(%)).@]"
      na (if na <= 1 then "" else "s")
      cna (if cna <= 1 then "is@ constant" else "are@ constants");
    {
      nb_typ_vars = Vm.cardinal typ_support;
      nb_mut_refs = mr;
      nb_const_refs = cr;
      nb_mut_rel_vars = na - cna;
      nb_const_rel_vars = cna;
    }

end

(* -------------------------------------------------------------------------- *)

(** {3 Field-share} *)

module FieldShareRelations = struct
  let capture_alias = true
  let capture_field_share = true
end

module FieldShare: FINITE_COMPS_HEAP =
  functor (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
    (Heuristics: Env.HEURISTICS) (SupportVars: SUPPORT_VARS) ->
struct

  include FieldShareModelCommonDefs (Env) (FeatureFilters) (Heuristics)
    (SupportVars) (RefRels.FieldShareHelpers) (FieldShareRefDom)
    (FieldShareObjDom) (FieldShareRelations)

  (* --- *)

  open FeatureFilters
  open SupportHelpers
  open Env
  open Bexp
  let arg_alias_invariant, arg_alias_invariant_support_size =
    let module Inv = RefRels.FieldShareInvariant (Env) (Heap) (struct
      let all_refs = all_arg_refs
      let alias_vars = all_arg_alias
      let field_share_vars = all_arg_field_share
    end) in
    let inv = begin tt
        |> Inv.alias_rel_is_eqrel
        |> Inv.field_share_rel_is_transitive
        |> Inv.field_share_rel_transitivity_carries_over_alias_rel
        |>~ Inv.sound_levels_wrt_alias_rel
        |>~ Inv.sound_levels_wrt_field_share_rel
        |>? Inv.sound_taints_wrt_alias_rel
        |>? Inv.sound_taints_wrt_field_share_rel
    end in
    let supp_size = Vs.cardinal (Bexp.fold_supp Vs.add inv Vs.empty) in
    (* Log.d3 logger "Arg prms: %a" Vars.print all_arg_prms; *)
    Log.d3 logger "Arg refs: %a" Vars.print all_arg_refs;
    Log.d3 logger "Arg alias invariant:@\n%a" Env.Bexp.print inv;
    Log.d logger "Arg alias invariant has %u variables" supp_size;
    inv, supp_size;;

  open Captured
  let stats: int cardinals =
    let cr = Vars.cardinal all_arg_refs in
    let mr = Vars.cardinal all_refs - cr in
    let na = AliasRelVars.cardinal alias_vars
    and cna = AliasRelVars.cardinal const_alias_vars
    and nfs = FieldShareRelVars.cardinal field_share_vars in
    Log.i logger "@[Heap@ abstraction@ features@ %u@ alias@ variable%(%)@ \
                    (%u@ of@ which@ %(%))@ and@ %u@ field-share@ variable%(%).\
                    @]"
      na (if na <= 1 then "" else "s")
      cna (if cna <= 1 then "is@ constant" else "are@ constants")
      nfs (if nfs <= 1 then "" else "s");
    {
      nb_typ_vars = Vm.cardinal typ_support;
      nb_mut_refs = mr;
      nb_const_refs = cr;
      nb_mut_rel_vars = na - cna + nfs;
      nb_const_rel_vars = cna;
    }

end

(* -------------------------------------------------------------------------- *)

(** {3 Share} *)

module ShareRelations = struct
  let capture_share = true
end

module Share: FINITE_COMPS_HEAP =
  functor (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
    (Heuristics: Env.HEURISTICS) (SupportVars: SUPPORT_VARS) ->
struct

  include ShareModelCommonDefs (Env) (FeatureFilters) (Heuristics)
    (SupportVars) (RefRels.ShareHelpers) (ShareRefDom)
    (ShareObjDom) (ShareRelations)

  (* --- *)

  open FeatureFilters
  open SupportHelpers
  open Env
  open Bexp
  let arg_alias_invariant, arg_alias_invariant_support_size =
    let module Inv = RefRels.ShareInvariant (Env) (Heap) (struct
      let all_refs = all_arg_refs
      let share_vars = all_arg_share
    end) in
    let inv = begin tt                                         (* TODO: check *)
        |> Inv.share_rel_is_eqrel
        |>~ Inv.sound_levels_wrt_share_rel
        |>? Inv.sound_taints_wrt_share_rel
    end in
    let supp_size = Vs.cardinal (Bexp.fold_supp Vs.add inv Vs.empty) in
    (* Log.d3 logger "Arg prms: %a" Vars.print all_arg_prms; *)
    Log.d3 logger "Arg refs: %a" Vars.print all_arg_refs;
    Log.d3 logger "Arg share invariant:@\n%a" Env.Bexp.print inv;
    Log.d logger "Arg share invariant has %u variables" supp_size;
    inv, supp_size;;

  open Captured
  let stats: int cardinals =
    let cr = Vars.cardinal all_arg_refs in
    let mr = Vars.cardinal all_refs - cr in
    let nc = ShareRelVars.cardinal share_vars in
    Log.i logger "@[Heap@ abstraction@ features@ %u@ share@ variable%(%).@]"
      nc (if nc <= 1 then "" else "s");
    {
      nb_typ_vars = Vm.cardinal typ_support;
      nb_mut_refs = mr;
      nb_const_refs = cr;
      nb_mut_rel_vars = nc;
      nb_const_rel_vars = 0;
    }

end

(* -------------------------------------------------------------------------- *)

(** {3 Connect} *)

module ConnectRelations = struct
  let capture_connect = true
  let capture_share = false
end

module Connect: FINITE_COMPS_HEAP =
  functor (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
    (Heuristics: Env.HEURISTICS) (SupportVars: SUPPORT_VARS) ->
struct

  include ConnectModelCommonDefs (Env) (FeatureFilters) (Heuristics)
    (SupportVars) (RefRels.ConnectHelpers) (ConnectRefDom)
    (ConnectObjDom) (ConnectRelations)

  (* --- *)

  open FeatureFilters
  open SupportHelpers
  open Env
  open Bexp
  let arg_alias_invariant, arg_alias_invariant_support_size =
    let module Inv = RefRels.ConnectInvariant (Env) (Heap) (struct
      let all_refs = all_arg_refs
      let connect_vars = all_arg_connect
      let share_vars = all_arg_share
    end) in
    let inv = begin tt                                         (* TODO: check *)
        |> Inv.connect_rel_is_eqrel
        |>~ Inv.sound_levels_wrt_connect_rel
        |>? Inv.sound_taints_wrt_connect_rel
    end in
    let supp_size = Vs.cardinal (Bexp.fold_supp Vs.add inv Vs.empty) in
    (* Log.d3 logger "Arg prms: %a" Vars.print all_arg_prms; *)
    Log.d3 logger "Arg refs: %a" Vars.print all_arg_refs;
    Log.d3 logger "Arg connect invariant:@\n%a" Env.Bexp.print inv;
    Log.d logger "Arg connect invariant has %u variables" supp_size;
    inv, supp_size;;

  open Captured
  let stats: int cardinals =
    let cr = Vars.cardinal all_arg_refs in
    let mr = Vars.cardinal all_refs - cr in
    let nc = AliasRelVars.cardinal connect_vars in
    Log.i logger "@[Heap@ abstraction@ features@ %u@ connection@ \
                    variable%(%).@]"
      nc (if nc <= 1 then "" else "s");
    {
      nb_typ_vars = Vm.cardinal typ_support;
      nb_mut_refs = mr;
      nb_const_refs = cr;
      nb_mut_rel_vars = nc;
      nb_const_rel_vars = 0;
    }

end

(* ----------------------------------------------------------------------------- *)

(** {3 Parameterized Domain Definition} *)

module FiniteCompHeapFromFeature: FINITE_COMPS_HEAP =
  functor (Env: FINITE_COMPS_ENV) (FeatureFilters: Features.FILTERS)
    (Heuristics: Env.HEURISTICS) (SupportVars: SUPPORT_VARS) ->
struct
  module Make: FINITE_COMPS_HEAP =
    (val begin match FeatureFilters.Features.heapdom with
      | DeepAlias -> (module DeepAlias: FINITE_COMPS_HEAP)
      | ShallowAlias -> (module ShallowAlias: FINITE_COMPS_HEAP)
      | DumbAlias -> (module DumbAlias: FINITE_COMPS_HEAP)
      | Connect -> (module Connect: FINITE_COMPS_HEAP)
      | Share -> (module Share: FINITE_COMPS_HEAP)
      | FieldShare -> (module FieldShare: FINITE_COMPS_HEAP)
    end)
  include Make (Env) (FeatureFilters) (Heuristics) (SupportVars)
end

(* -------------------------------------------------------------------------- *)

(** {2 Forging (from summary stubs)} *)

module type HEAP_DOM_FORGE = sig
  module Env: SCFG.ENV
  module S: Semantics.S
  open Env
  include OBJ_TYPS with type lexp := lexp and type texp := bexp
  val setup:
    ?retdom:S.refdom ->
    ?hlevel:lexp ->
    ?htaint:bexp ->
    ?ret_arg_alias:bool ->
    ?ret_arg_field_alias_to:bool ->
    ?ret_arg_field_alias_from:bool ->
    ?ret_arg_field_share_to:bool ->
    ?ret_arg_field_share_from:bool ->
    ?exc_arg_alias:bool ->
    ?exc_arg_field_alias_to:bool ->
    ?exc_arg_field_alias_from:bool ->
    ?exc_arg_field_share_to:bool ->
    ?exc_arg_field_share_from:bool ->
    ?ref_arg_level_res:(OOBase.VarMap.key -> lexp) ->
    ?ref_arg_taint_res:(OOBase.VarMap.key -> bexp) ->
    ?may_mut_arg_fields:bool ->
    S.prmdom OOBase.VarMap.t * S.refdom OOBase.VarMap.t -> exp VarMap.t
end

(* --- *)

module CommonForgeUtils
  (Env: FINITE_COMPS_ENV)
  (FeatureFilters: Features.FILTERS)
  =
struct
  open Env.Bexp
  open FeatureFilters

  let fold_pvars s f = Base.VarMap.fold f s

  let toplevel _r = Env.Lexp.top
  let toptaint _r = tt

  let ( ~^ ) r = Env.Lexp.var (HLevel r)
  let ( ?^ ) r = Env.Bexp.var (HTaint r)

  let ret_obj_level r = Ret (HLevel r)
  let exc_obj_level r = Exc (HLevel r)
  let ret_obj_taint r = Ret (HTaint r)
  let exc_obj_taint r = Exc (HTaint r)

  let (?..) = function
    | true -> Env.Bexp tt
    | false -> Env.Bexp ff

  open Env
  open VarMap
  open ObjTypResVars
  let bind_ret_n_exc_typs ?retdom hlevel htaint =
    let bind_ret_obj_level = add (Ret res_obj_level) (Lexp hlevel)
    and bind_exc_obj_level = add (Exc res_obj_level) (Lexp hlevel)
    and bind_ret_obj_taint = add (Ret res_obj_taint) (Bexp htaint)
    and bind_exc_obj_taint = add (Exc res_obj_taint) (Bexp htaint) in
    begin fun u -> u
      |>~ (if retdom <> None then bind_ret_obj_level else ign)
      |>!~ bind_exc_obj_level
      |>? (if retdom <> None then bind_ret_obj_taint else ign)
      |>!? bind_exc_obj_taint
    end

end

module ForgeShareUtils
  (Config: sig val capture_share: bool end)
  (Env: FINITE_COMPS_ENV)
  (FeatureFilters: Features.FILTERS)
  (S: Semantics.S)
  (A: RefRels.SHARE_REL_INSTANCIATIONS with type refdom := S.refdom)
  =
struct
  open Env
  open FeatureFilters
  open ShareRelResArgVars
  open Bexp

  let ret_arg_share_vars refs = function
    | _ when not Config.capture_share -> Vars.empty
    | None -> Vars.empty
    | Some retdom -> A.sharing_vars retdom refs

  let exc_arg_share_vars =
    if Config.capture_share && Features.track_excs
    then A.sharing_vars S.excndom
    else fun _ -> Vars.empty

  let fold_svars s f =
    if Config.capture_share then ShareRelVars.fold f s else ign
  let fold_if_s s f =
    if Config.capture_share then Base.Vars.fold f s else ign

  let tts v = tt
  and ids v = var (ShareRel v)

  let ret_share v = Ret (ShareRel v)
  let exc_share v = Exc (ShareRel v)
  let ret_arg_share a = Ret (res_arg_share a)
  let exc_arg_share a = Exc (res_arg_share a)
end

module ForgeAliasUtils
  (Config: sig val capture_alias: bool end)
  (Env: FINITE_COMPS_ENV)
  (FeatureFilters: Features.FILTERS)
  (S: Semantics.S)
  (A: RefRels.ALIAS_REL_INSTANCIATIONS with type refdom := S.refdom)
  =
struct
  open Env
  open FeatureFilters
  open AliasRelResArgVars

  let ret_arg_alias_vars refs = function
    | _ when not Config.capture_alias -> Vars.empty
    | None -> Vars.empty
    | Some retdom -> A.aliasing_vars retdom refs

  let exc_arg_alias_vars =
    if Config.capture_alias && Features.track_excs
    then A.aliasing_vars S.excndom
    else fun _ -> Vars.empty

  let fold_if_a s f =
    if Config.capture_alias then Base.Vars.fold f s else ign

  let ret_arg_alias a = Ret (res_arg_alias a)
  let exc_arg_alias a = Exc (res_arg_alias a)
end

module ForgeFieldAliasUtils
  (Config: sig val capture_field_alias: bool end)
  (Env: FINITE_COMPS_ENV)
  (FeatureFilters: Features.FILTERS)
  (S: Semantics.S)
  (A: RefRels.FIELD_ALIAS_REL_INSTANCIATIONS with type refdom := S.refdom)
  =
struct
  open Env
  open FeatureFilters
  open FieldAliasRelResArgVars
  open Bexp

  let ret_arg_field_alias_vars refs = function
    | _ when not Config.capture_field_alias -> Vars.empty, Vars.empty
    | None -> Vars.empty, Vars.empty
    | Some retdom -> A.field_aliasing_vars_to_from retdom refs

  let exc_arg_field_alias_vars =
    if Config.capture_field_alias && Features.track_excs
    then A.field_aliasing_vars_to_from S.excndom
    else fun _ -> Vars.empty, Vars.empty

  let fold_favars s f =
    if Config.capture_field_alias then FieldAliasRelVars.fold f s else ign
  let fold_if_fa s f =
    if Config.capture_field_alias then Base.Vars.fold f s else ign

  let ttfa v = tt
  and idfa v = var (FieldAliasRel v)

  let ret_field_alias v = Ret (FieldAliasRel v)
  let exc_field_alias v = Exc (FieldAliasRel v)
  let ret_arg_field_alias_to a = Ret (res_arg_field_alias_to a)
  let exc_arg_field_alias_to a = Exc (res_arg_field_alias_to a)
  let ret_arg_field_alias_from a = Ret (res_arg_field_alias_from a)
  let exc_arg_field_alias_from a = Exc (res_arg_field_alias_from a)
end

module ForgeFieldShareUtils
  (Config: sig val capture_field_share: bool end)
  (Env: FINITE_COMPS_ENV)
  (FeatureFilters: Features.FILTERS)
  (S: Semantics.S)
  (A: RefRels.FIELD_SHARE_REL_INSTANCIATIONS with type refdom := S.refdom)
  =
struct
  open Env
  open FeatureFilters
  open FieldShareRelResArgVars
  open Bexp

  let ret_arg_field_share_vars refs = function
    | _ when not Config.capture_field_share -> Vars.empty, Vars.empty
    | None -> Vars.empty, Vars.empty
    | Some retdom -> A.field_sharing_vars_to_from retdom refs

  let exc_arg_field_share_vars =
    if Config.capture_field_share && Features.track_excs
    then A.field_sharing_vars_to_from S.excndom
    else fun _ -> Vars.empty, Vars.empty

  let fold_fsvars s f =
    if Config.capture_field_share then FieldShareRelVars.fold f s else ign
  let fold_if_fs s f =
    if Config.capture_field_share then Base.Vars.fold f s else ign

  let ttfs v = tt
  and idfs v = var (FieldShareRel v)

  let ret_field_share v = Ret (FieldShareRel v)
  let exc_field_share v = Exc (FieldShareRel v)
  let ret_arg_field_share_to a = Ret (res_arg_field_share_to a)
  let exc_arg_field_share_to a = Exc (res_arg_field_share_to a)
  let ret_arg_field_share_from a = Ret (res_arg_field_share_from a)
  let exc_arg_field_share_from a = Exc (res_arg_field_share_from a)
end

module ForgeConnectUtils
  (Config: sig val capture_connect: bool end)
  (Env: FINITE_COMPS_ENV)
  (FeatureFilters: Features.FILTERS)
  (S: Semantics.S)
  (A: RefRels.CONNECT_REL_INSTANCIATIONS with type refdom := S.refdom)
  =
struct
  open Env
  open FeatureFilters
  open ConnectRelResArgVars
  open Bexp

  let ret_arg_connect_vars refs = function
    | _ when not Config.capture_connect -> Vars.empty
    | None -> Vars.empty
    | Some retdom -> A.connecting_vars retdom refs

  let exc_arg_connect_vars =
    if Config.capture_connect && Features.track_excs
    then A.connecting_vars S.excndom
    else fun _ -> Vars.empty

  let fold_cvars s f =
    if Config.capture_connect then AliasRelVars.fold f s else ign
  let fold_if_c s f =
    if Config.capture_connect then Base.Vars.fold f s else ign

  let ttc v = tt
  and idc v = var (AliasRel v)

  let ret_connect v = Ret (AliasRel v)
  let exc_connect v = Exc (AliasRel v)
  let ret_arg_connect a = Ret (res_arg_connect a)
  let exc_arg_connect a = Exc (res_arg_connect a)
end

(* --- *)

module DeepAliasModelForgeCommonDefs
  (Config: DEEP_ALIAS_MODEL_CAPTURED_RELATIONS)
  (Env: FINITE_COMPS_ENV)
  (FeatureFilters: Features.FILTERS)
  (S: Semantics.S)
  :
  (HEAP_DOM_FORGE with module Env := Env and module S := S)
  =
struct
  module A = RefRels.DeepAliasModelRefSetsWalk (S)
  module H = Heuristics.Exprs (Env) (Heuristics.DefaultOptions)
  module U = struct
    include ForgeAliasUtils (Config) (Env) (FeatureFilters) (S) (A)
    include ForgeFieldAliasUtils (Config) (Env) (FeatureFilters) (S) (A)
  end

  ;;assert (not Config.capture_share);;                              (* for now *)

  open Env
  open FeatureFilters
  open VarMap
  open Lexp
  open Bexp
  open H

  include CommonForgeUtils (Env) (FeatureFilters)
  let tite = bite

  (** Symbolic heap setup *)
  let setup
      ?retdom
      ?(hlevel = bottom)                   (* base level of returned object *)
      ?(htaint = ff)
      ?(ret_arg_alias = true)
      ?(ret_arg_field_alias_to = true)
      ?(ret_arg_field_alias_from = true)
      ?(ret_arg_field_share_to = true)
      ?(ret_arg_field_share_from = true)
      ?(exc_arg_alias = true)
      ?(exc_arg_field_alias_to = true)
      ?(exc_arg_field_alias_from = true)
      ?(exc_arg_field_share_to = true)
      ?(exc_arg_field_share_from = true)
      ?(ref_arg_level_res = toplevel)
      ?(ref_arg_taint_res = toptaint)
      ?(may_mut_arg_fields = true)
      (arg_prms, arg_refs)
      =
    let       ra = ?..ret_arg_alias
    and   rfa_to = ?..ret_arg_field_alias_to
    and rfa_from = ?..ret_arg_field_alias_from
    and       ea = ?..exc_arg_alias
    and   efa_to = ?..exc_arg_field_alias_to
    and efa_from = ?..exc_arg_field_alias_from in
    let open U in
    let field_alias_vars = A.field_alias_vars arg_refs
    and field_alias_res = if may_mut_arg_fields then ttfa else idfa
    and ret_arg_alias_vars = ret_arg_alias_vars arg_refs retdom
    and exc_arg_alias_vars = exc_arg_alias_vars arg_refs
    and ret_arg_field_alias_to_vars, ret_arg_field_alias_from_vars
      = ret_arg_field_alias_vars arg_refs retdom
    and exc_arg_field_alias_to_vars, exc_arg_field_alias_from_vars
      = exc_arg_field_alias_vars arg_refs in
    let hlevel = if hlevel = top then hlevel else begin hlevel
        |>~ fold_if_a ret_arg_alias_vars (fun a ->
          (|.|) (lite (var (ret_arg_alias a), ~^a, bottom)))
        |>~ fold_if_fa ret_arg_field_alias_to_vars (fun a ->
          (|.|) (lite (var (ret_arg_field_alias_to a), ~^a, bottom)))
        |>!~ fold_if_a exc_arg_alias_vars (fun a ->
          (|.|) (lite (var (exc_arg_alias a), ~^a, bottom)))
        |>!~ fold_if_fa exc_arg_field_alias_to_vars (fun a ->
          (|.|) (lite (var (exc_arg_field_alias_to a), ~^a, bottom)))
    end in
    let htaint = if htaint = tt then htaint else begin htaint
        |>? fold_if_a ret_arg_alias_vars (fun a ->
          (||.) (tite (var (ret_arg_alias a), ?^a, ff)))
        |>? fold_if_fa ret_arg_field_alias_to_vars (fun a ->
          (||.) (tite (var (ret_arg_field_alias_to a), ?^a, ff)))
        |>!? fold_if_a exc_arg_alias_vars (fun a ->
          (||.) (tite (var (exc_arg_alias a), ?^a, ff)))
        |>!? fold_if_fa exc_arg_field_alias_to_vars (fun a ->
          (||.) (tite (var (exc_arg_field_alias_to a), ?^a, ff)))
    end in
    empty
    |> fold_if_a ret_arg_alias_vars
        (fun a -> add (ret_arg_alias a) ra)
    |> fold_if_fa ret_arg_field_alias_to_vars
        (fun a -> add (ret_arg_field_alias_to a) rfa_to)
    |> fold_if_fa ret_arg_field_alias_from_vars
        (fun a -> add (ret_arg_field_alias_from a) rfa_from)
    |> fold_favars field_alias_vars
        (fun v -> add (ret_field_alias v) (Bexp (field_alias_res v)))
    |>~ fold_pvars arg_refs
        (fun a _ -> add (ret_obj_level a) (Lexp (ref_arg_level_res a)))
    |>? fold_pvars arg_refs
        (fun a _ -> add (ret_obj_taint a) (Bexp (ref_arg_taint_res a)))
    |>! fold_if_a exc_arg_alias_vars
        (fun a -> add (exc_arg_alias a) ea)
    |>! fold_if_fa exc_arg_field_alias_to_vars
        (fun a -> add (exc_arg_field_alias_to a) efa_to)
    |>! fold_if_fa exc_arg_field_alias_from_vars
        (fun a -> add (exc_arg_field_alias_from a) efa_from)
    |>! fold_favars field_alias_vars
        (fun v -> add (exc_field_alias v) (Bexp (field_alias_res v)))
    |>!~ fold_pvars arg_refs
        (fun a _ -> add (exc_obj_level a) (Lexp (ref_arg_level_res a)))
    |>!? fold_pvars arg_refs
        (fun a _ -> add (exc_obj_taint a) (Bexp (ref_arg_taint_res a)))
    |> bind_ret_n_exc_typs ?retdom hlevel htaint

end

module DeepAliasForge = DeepAliasModelForgeCommonDefs (DeepAliasRelations)
module ShallowAliasForge = DeepAliasModelForgeCommonDefs (ShallowAliasRelations)
module DumbAliasForge = DeepAliasModelForgeCommonDefs (DumbAliasRelations)

(* --- *)

module FieldShareModelForgeCommonDefs
  (Config: FIELD_SHARE_MODEL_CAPTURED_RELATIONS)
  (Env: FINITE_COMPS_ENV)
  (FeatureFilters: Features.FILTERS)
  (S: Semantics.S)
  :
  (HEAP_DOM_FORGE with module Env := Env and module S := S)
  =
struct
  module A = RefRels.FieldShareModelRefSetsWalk (S)
  module H = Heuristics.Exprs (Env) (Heuristics.DefaultOptions)
  module U = struct
    include ForgeAliasUtils (Config) (Env) (FeatureFilters) (S) (A)
    include ForgeFieldShareUtils (Config) (Env) (FeatureFilters) (S) (A)
  end

  open Env
  open FeatureFilters
  open VarMap
  open Lexp
  open Bexp
  open H

  include CommonForgeUtils (Env) (FeatureFilters)
  let tite = bite

  (** Symbolic heap setup *)
  let setup
      ?retdom
      ?(hlevel = bottom)                   (* base level of returned object *)
      ?(htaint = ff)
      ?(ret_arg_alias = true)
      ?(ret_arg_field_alias_to = true)
      ?(ret_arg_field_alias_from = true)
      ?(ret_arg_field_share_to = true)
      ?(ret_arg_field_share_from = true)
      ?(exc_arg_alias = true)
      ?(exc_arg_field_alias_to = true)
      ?(exc_arg_field_alias_from = true)
      ?(exc_arg_field_share_to = true)
      ?(exc_arg_field_share_from = true)
      ?(ref_arg_level_res = toplevel)
      ?(ref_arg_taint_res = toptaint)
      ?(may_mut_arg_fields = true)
      (arg_prms, arg_refs)
      =
    let       ra = ?..ret_arg_alias
    and   rfs_to = ?..ret_arg_field_share_to
    and rfs_from = ?..ret_arg_field_share_from
    and       ea = ?..exc_arg_alias
    and   efs_to = ?..exc_arg_field_share_to
    and efs_from = ?..exc_arg_field_share_from in
    let open U in
    let field_share_vars = A.field_share_vars arg_refs
    and field_share_res = if may_mut_arg_fields then ttfs else idfs
    and ret_arg_alias_vars = ret_arg_alias_vars arg_refs retdom
    and exc_arg_alias_vars = exc_arg_alias_vars arg_refs
    and ret_arg_field_share_to_vars, ret_arg_field_share_from_vars
      = ret_arg_field_share_vars arg_refs retdom
    and exc_arg_field_share_to_vars, exc_arg_field_share_from_vars
      = exc_arg_field_share_vars arg_refs in
    let hlevel = if hlevel = top then hlevel else begin hlevel
        |>~ fold_if_a ret_arg_alias_vars (fun a ->
          (|.|) (lite (var (ret_arg_alias a), ~^a, bottom)))
        |>~ fold_if_fs ret_arg_field_share_to_vars (fun a ->
          (|.|) (lite (var (ret_arg_field_share_to a), ~^a, bottom)))
        |>!~ fold_if_a exc_arg_alias_vars (fun a ->
          (|.|) (lite (var (exc_arg_alias a), ~^a, bottom)))
        |>!~ fold_if_fs exc_arg_field_share_to_vars (fun a ->
          (|.|) (lite (var (exc_arg_field_share_to a), ~^a, bottom)))
    end in
    let htaint = if htaint = tt then htaint else begin htaint
        |>? fold_if_a ret_arg_alias_vars (fun a ->
          (||.) (tite (var (ret_arg_alias a), ?^a, ff)))
        |>? fold_if_fs ret_arg_field_share_to_vars (fun a ->
          (||.) (tite (var (ret_arg_field_share_to a), ?^a, ff)))
        |>!? fold_if_a exc_arg_alias_vars (fun a ->
          (||.) (tite (var (exc_arg_alias a), ?^a, ff)))
        |>!? fold_if_fs exc_arg_field_share_to_vars (fun a ->
          (||.) (tite (var (exc_arg_field_share_to a), ?^a, ff)))
    end in
    empty
    |> fold_if_a ret_arg_alias_vars
        (fun a -> add (ret_arg_alias a) ra)
    |> fold_if_fs ret_arg_field_share_to_vars
        (fun a -> add (ret_arg_field_share_to a) rfs_to)
    |> fold_if_fs ret_arg_field_share_from_vars
        (fun a -> add (ret_arg_field_share_from a) rfs_from)
    |> fold_fsvars field_share_vars
        (fun v -> add (ret_field_share v) (Bexp (field_share_res v)))
    |>~ fold_pvars arg_refs
        (fun a _ -> add (ret_obj_level a) (Lexp (ref_arg_level_res a)))
    |>? fold_pvars arg_refs
        (fun a _ -> add (ret_obj_taint a) (Bexp (ref_arg_taint_res a)))
    |>! fold_if_a exc_arg_alias_vars
        (fun a -> add (exc_arg_alias a) ea)
    |>! fold_if_fs exc_arg_field_share_to_vars
        (fun a -> add (exc_arg_field_share_to a) efs_to)
    |>! fold_if_fs exc_arg_field_share_from_vars
        (fun a -> add (exc_arg_field_share_from a) efs_from)
    |>! fold_fsvars field_share_vars
        (fun v -> add (exc_field_share v) (Bexp (field_share_res v)))
    |>!~ fold_pvars arg_refs
        (fun a _ -> add (exc_obj_level a) (Lexp (ref_arg_level_res a)))
    |>!? fold_pvars arg_refs
        (fun a _ -> add (exc_obj_taint a) (Bexp (ref_arg_taint_res a)))
    |> bind_ret_n_exc_typs ?retdom hlevel htaint

end

module FieldShareForge = FieldShareModelForgeCommonDefs (FieldShareRelations)

(* --- *)

module ShareModelForgeCommonDefs
  (Config: SHARE_MODEL_CAPTURED_RELATIONS)
  (Env: FINITE_COMPS_ENV)
  (FeatureFilters: Features.FILTERS)
  (S: Semantics.S)
  :
  (HEAP_DOM_FORGE with module Env := Env and module S := S)
  =
struct
  module A = RefRels.ShareModelRefSetsWalk (S)
  module H = Heuristics.Exprs (Env) (Heuristics.DefaultOptions)
  module U = ForgeShareUtils (Config) (Env) (FeatureFilters) (S) (A)

  open Env
  open FeatureFilters
  open VarMap
  open Lexp
  open Bexp
  open H

  include CommonForgeUtils (Env) (FeatureFilters)
  let tite = bite

  (** Symbolic heap setup *)
  let setup
      ?retdom
      ?(hlevel = bottom)                     (* base level of returned object *)
      ?(htaint = ff)
      ?(ret_arg_alias = true)
      ?(ret_arg_field_alias_to = true)
      ?(ret_arg_field_alias_from = true)
      ?(ret_arg_field_share_to = true)
      ?(ret_arg_field_share_from = true)
      ?(exc_arg_alias = true)
      ?(exc_arg_field_alias_to = true)
      ?(exc_arg_field_alias_from = true)
      ?(exc_arg_field_share_to = true)
      ?(exc_arg_field_share_from = true)
      ?(ref_arg_level_res = toplevel)
      ?(ref_arg_taint_res = toptaint)
      ?(may_mut_arg_fields = true)
      (arg_prms, arg_refs)
      =
    let rs = ?..(ret_arg_alias
                 || ret_arg_field_alias_to
                 || ret_arg_field_alias_from
                 || ret_arg_field_share_to
                 || ret_arg_field_share_from)
    and es = ?..(exc_arg_alias
                 || exc_arg_field_alias_to
                 || exc_arg_field_alias_from
                 || exc_arg_field_share_to
                 || exc_arg_field_share_from) in
    let open U in
    let share_vars = A.share_vars arg_refs
    and share_res = if may_mut_arg_fields then tts else ids
    and ret_arg_share_vars = ret_arg_share_vars arg_refs retdom
    and exc_arg_share_vars = exc_arg_share_vars arg_refs in
    let hlevel = if hlevel = top then hlevel else begin hlevel
        |>~ fold_if_s ret_arg_share_vars (fun a ->
          (|.|) (lite (var (ret_arg_share a), ~^a, bottom)))
        |>!~ fold_if_s exc_arg_share_vars (fun a ->
          (|.|) (lite (var (exc_arg_share a), ~^a, bottom)))
    end in
    let htaint = if htaint = tt then htaint else begin htaint
        |>? fold_if_s ret_arg_share_vars (fun a ->
          (||.) (tite (var (ret_arg_share a), ?^a, ff)))
        |>!? fold_if_s exc_arg_share_vars (fun a ->
          (||.) (tite (var (exc_arg_share a), ?^a, ff)))
    end in
    empty
    |> fold_if_s ret_arg_share_vars
        (fun a -> add (ret_arg_share a) rs)
    |> fold_svars share_vars
        (fun v -> add (ret_share v) (Bexp (share_res v)))
    |>~ fold_pvars arg_refs
        (fun a _ -> add (ret_obj_level a) (Lexp (ref_arg_level_res a)))
    |>? fold_pvars arg_refs
        (fun a _ -> add (ret_obj_taint a) (Bexp (ref_arg_taint_res a)))
    |>! fold_if_s exc_arg_share_vars
        (fun a -> add (exc_arg_share a) es)
    |>! fold_svars share_vars
        (fun v -> add (exc_share v) (Bexp (share_res v)))
    |>!~ fold_pvars arg_refs
        (fun a _ -> add (exc_obj_level a) (Lexp (ref_arg_level_res a)))
    |>!? fold_pvars arg_refs
        (fun a _ -> add (exc_obj_taint a) (Bexp (ref_arg_taint_res a)))
    |> bind_ret_n_exc_typs ?retdom hlevel htaint

end

module ShareForge = ShareModelForgeCommonDefs (ShareRelations)

(* --- *)

module ConnectModelForgeCommonDefs
  (Config: CONNECT_MODEL_CAPTURED_RELATIONS)
  (Env: FINITE_COMPS_ENV)
  (FeatureFilters: Features.FILTERS)
  (S: Semantics.S)
  :
  (HEAP_DOM_FORGE with module Env := Env and module S := S)
  =
struct
  module A = RefRels.ConnectModelRefSetsWalk (S)
  module H = Heuristics.Exprs (Env) (Heuristics.DefaultOptions)
  module U = struct
    include ForgeShareUtils (Config) (Env) (FeatureFilters) (S) (A)
    include ForgeConnectUtils (Config) (Env) (FeatureFilters) (S) (A)
  end

  ;;assert (not Config.capture_share);;                              (* for now *)

  open Env
  open FeatureFilters
  open VarMap
  open Lexp
  open Bexp
  open H

  include CommonForgeUtils (Env) (FeatureFilters)
  let tite = bite

  (** Symbolic heap setup *)
  let setup
      ?retdom
      ?(hlevel = bottom)                     (* base level of returned object *)
      ?(htaint = ff)
      ?(ret_arg_alias = true)
      ?(ret_arg_field_alias_to = true)
      ?(ret_arg_field_alias_from = true)
      ?(ret_arg_field_share_to = true)
      ?(ret_arg_field_share_from = true)
      ?(exc_arg_alias = true)
      ?(exc_arg_field_alias_to = true)
      ?(exc_arg_field_alias_from = true)
      ?(exc_arg_field_share_to = true)
      ?(exc_arg_field_share_from = true)
      ?(ref_arg_level_res = toplevel)
      ?(ref_arg_taint_res = toptaint)
      ?(may_mut_arg_fields = true)
      (arg_prms, arg_refs)
      =
    let rc = ?..(ret_arg_alias
                 || ret_arg_field_alias_to
                 || ret_arg_field_alias_from)
    and ec = ?..(exc_arg_alias
                 || exc_arg_field_alias_to
                 || exc_arg_field_alias_from) in
    let open U in
    let connect_vars = A.connect_vars arg_refs
    and connect_res = if may_mut_arg_fields then ttc else idc in
    let ret_arg_connect_vars = ret_arg_connect_vars arg_refs retdom
    and exc_arg_connect_vars = exc_arg_connect_vars arg_refs in
    let arg_refs f = fold_pvars arg_refs (fun a _ -> f a)
    and connect_vars = fold_cvars connect_vars
    and ret_arg_connect_vars f = fold_if_c ret_arg_connect_vars f
    and exc_arg_connect_vars f = fold_if_c exc_arg_connect_vars f in
    let hlevel = if hlevel = top then hlevel else begin hlevel
        |>~ ret_arg_connect_vars
            (fun a -> (|.|) (lite (var (ret_arg_connect a), ~^a, bottom)))
        |>!~ exc_arg_connect_vars
            (fun a -> (|.|) (lite (var (exc_arg_connect a), ~^a, bottom)))
    end in
    let htaint = if htaint = tt then htaint else begin htaint
        |>? ret_arg_connect_vars
            (fun a -> (||.) (tite (var (ret_arg_connect a), ?^a, ff)))
        |>!? exc_arg_connect_vars
            (fun a -> (||.) (tite (var (ret_arg_connect a), ?^a, ff)))
    end in
    empty
    |> ret_arg_connect_vars (fun a -> add (ret_arg_connect a) rc)
    |> connect_vars (fun v -> add (ret_connect v) (Bexp (connect_res v)))
    |>~ arg_refs (fun a -> add (ret_obj_level a) (Lexp (ref_arg_level_res a)))
    |>? arg_refs (fun a -> add (ret_obj_taint a) (Bexp (ref_arg_taint_res a)))
    |>! exc_arg_connect_vars (fun a -> add (exc_arg_connect a) ec)
    |>! connect_vars (fun v -> add (exc_connect v) (Bexp (connect_res v)))
    |>!~ arg_refs (fun a -> add (exc_obj_level a) (Lexp (ref_arg_level_res a)))
    |>!? arg_refs (fun a -> add (exc_obj_taint a) (Bexp (ref_arg_taint_res a)))
    |> bind_ret_n_exc_typs ?retdom hlevel htaint

end

module ConnectForge = ConnectModelForgeCommonDefs (ConnectRelations)

(* -------------------------------------------------------------------------- *)

(** {3 Parameterized Forging} *)

module type FORGE = sig
  module Env: SCFG.ENV
  module Make: functor (FeatureFilters: Features.FILTERS) (S: Semantics.S) ->
    HEAP_DOM_FORGE with module Env := Env and module S := S
end

module ForgeFromFeature
  (Env: FINITE_COMPS_ENV)
  :
  (FORGE with module Env := Env)
  =
struct
  module type FORGER = functor (Env: FINITE_COMPS_ENV)
      (FeatureFilters: Features.FILTERS) (S: Semantics.S) ->
        HEAP_DOM_FORGE with module Env := Env and module S := S
  module Make (FeatureFilters: Features.FILTERS) (S: Semantics.S) = struct
    module Forger: FORGER =
      (val begin match FeatureFilters.Features.heapdom with
        | DeepAlias -> (module DeepAliasForge: FORGER)
        | ShallowAlias -> (module ShallowAliasForge: FORGER)
        | DumbAlias -> (module DumbAliasForge: FORGER)
        | Connect -> (module ConnectForge: FORGER)
        | Share -> (module ShareForge: FORGER)
        | FieldShare -> (module FieldShareForge: FORGER)
      end)
    include Forger (Env) (FeatureFilters) (S)
  end
end

(* -------------------------------------------------------------------------- *)
