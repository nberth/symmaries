open Format

(* --- *)

module type OPTIONS = sig
  val eager: bool
end

module DefaultOptions: OPTIONS = struct
  let eager = false
end

(* --- *)

module type T = sig
  include Exprs.CONDITIONALS
  include Exprs.BOPS with type bexp := bexp
end

(** Implements some basic heuristics for simplifying expressions that are
    extracted from Realib's internal representation; specialized for a
    binary domain for security levels. *)
module Exprs
  (X: Exprs.ALL)
  (O: OPTIONS)
  :
  (T with type bexp = X.bexp
     and type lexp = X.lexp
     and type eexp = X.eexp) =
struct
  open X
  type bexp = Bexp.t
  type lexp = Lexp.t
  type eexp = Eexp.t

  include Conditionals
  include BOps

  open Cond
  open Lexp.Cond
  open Lexp
  open Eexp
  open Bexp
  open O

  let partition_clauses = List.partition
  let filtered_clauses f =
    let rec aux acc = function
      | [] -> None
      | c :: tl -> match f c with
          | Some x -> Some (x, List.rev_append acc tl)
          | None -> aux (c :: acc) tl
    in
    aux []

  let extract_leqs = partition_clauses (function
    | BExt (LExt (LEq (LIte _, _)))
    | BExt (LExt (LEq (_, LIte _))) -> false
    | BExt (LExt (LEq (_, (LBot | LTop))))
    | BExt (LExt (LEq ((LBot | LTop), _))) -> true
    | _ -> false)

  (* --- *)

  let lopps a b = match a, b with
    | LEq (a, LTop), LEq (a', LBot)
    | LEq (LTop, a), LEq (a', LBot)
    | LEq (a, LTop), LEq (LBot, a')
    | LEq (LTop, a), LEq (LBot, a')
    | LEq (a, LBot), LEq (a', LTop)
    | LEq (LBot, a), LEq (a', LTop)
    | LEq (a, LBot), LEq (LTop, a')
    | LEq (LBot, a), LEq (LTop, a') -> Lexp.compare a a' = 0
    | _ -> false

  let conflicts lc = filtered_clauses (function
    | BExt (LExt lc') when lopps lc' lc -> Some lc
    | _ -> None)

  (* --- *)

  let ltops a1 a2 b1 b2 = match a1, a2, b1, b2 with
    | a, LTop, b, LTop
    | LTop, a, b, LTop
    | a, LTop, LTop, b
    | LTop, a, LTop, b -> Some (a, b)
    | _ -> None

  let lbots a1 a2 b1 b2 = match a1, a2, b1, b2 with
    | a, LBot, b, LBot
    | LBot, a, b, LBot
    | a, LBot, LBot, b
    | LBot, a, LBot, b -> Some (a, b)
    | _ -> None

  (* --- *)

  let ( ||. ) a b = match a, b with
    | BExt (LExt (LEq (a1, a2))), BExt (LExt (LEq (b1, b2)))
      -> begin match ltops a1 a2 b1 b2 with
        | Some (a, b) -> BExt (LExt (LEq (a |.| b, LTop)))
        | _ -> match lbots a1 a2 b1 b2 with
            | Some (a, b) -> BExt (LExt (LEq (a |^| b, LBot)))
            | _ -> a ||. b
      end
    | _ -> a ||. b

  let ( ||. ) a b = match a, b with
    | (BExt (LExt lc) as a), (BCnj (_ :: _ as l) as cnj)
    | (BCnj (_ :: _ as l) as cnj), (BExt (LExt lc) as a)
      -> begin match conflicts lc l with
        | Some (_, l) when eager -> conj (List.map ((||.) a) l)
        | None when eager -> conj (List.map ((||.) a) l)
        | Some (_, l) -> a ||. conj l
        | None -> a ||. cnj
      end
    | _ -> a ||. b

  (* --- *)

  let ( &&. ) a b = match a, b with
    | BExt (LExt (LEq (a1, a2))), BExt (LExt (LEq (b1, b2)))
      -> begin match ltops a1 a2 b1 b2 with
        | Some (a, b) -> BExt (LExt (LEq (a |^| b, LTop)))
        | _ -> match lbots a1 a2 b1 b2 with
            | Some (a, b) -> BExt (LExt (LEq (a |.| b, LBot)))
            | _ -> a &&. b
      end
    | _ -> a &&. b

  let ( &&. ) a b = match a, b with
    | (BExt (LExt lc) as a), (BDsj (_ :: _ as l) as dsj)
    | (BDsj (_ :: _ as l) as dsj), (BExt (LExt lc) as a)
      -> begin match conflicts lc l with
        | Some (_, l) when eager -> disj (List.map ((&&.) a) l)
        | None when eager -> disj (List.map ((&&.) a) l)
        | Some (_, l) -> a &&. disj l
        | None -> a &&. dsj
      end
    | _ -> a &&. b

  (* --- *)

  let rec lite0 ?(recur = lite0 ?recur:None) tpl =
    Lexp.mk_ite0 ~conj ~disj ~recur tpl

  and lite1 = function
    | BExt (LExt lc) as c, t, e
      -> begin match lc, t, e with
        | (LEq (v, LTop) | LEq (LTop, v)), LTop, LBot -> v
        | (LEq (v, LBot) | LEq (LBot, v)), LBot, LTop -> v
        | LEq (v, LBot), w, v' when Lexp.compare v v' = 0 -> v |.| w
        | LEq (LBot, v), w, v' when Lexp.compare v v' = 0 -> v |.| w
        | LEq (v, LTop), w, v' when Lexp.compare v v' = 0 -> v |^| w
        | LEq (LTop, v), w, v' when Lexp.compare v v' = 0 -> v |^| w
        | _ -> lite0 (c, t, e)
      end
    | BCnj l, t, e as cte when eager ->
        begin match extract_leqs l, t, e with
          |([], _ | _, []), _, _
          | _,(LIte _ | LLub _ | LGlb _), _
          | _, _, (LIte _ | LLub _ | LGlb _)
            -> lite0 cte
          | ((leq0 :: leqs), (c0 :: cs)), _, _
            -> lite0 (conj (c0 :: cs), lite (conj (leq0 :: leqs), t, e), e)
        end
    | BDsj l, t, e as cte when eager ->
        begin match extract_leqs (List.map (!.) l), t, e with
          |([], _ | _, []), _, _
          | _,(LIte _ | LLub _ | LGlb _), _
          | _, _, (LIte _ | LLub _ | LGlb _)
            -> lite0 cte
          | ((leq0 :: leqs), (c0 :: cs)), _, _
            -> lite0 (conj (c0 :: cs), lite (conj (leq0 :: leqs), e, t), t)
        end
    | cte
      -> lite0 cte

  and lite = function
    | BIte (c, p1, p2), LIte (c1, t1, e1), e
          when Bexp.compare c c1 = 0 ->
        lite (c, lite (p1, t1, e), lite (p2, e1, e))
    | BIte (c, p1, p2), t, LIte (c2, t2, e2)
          when Bexp.compare c c2 = 0 ->
        lite (c, lite (p1, t, t2), lite (p2, t, e2))
    | BIte (c, p1, p2), t, LIte (BIte (cp2, p21, p22), t2, e2)
          when Bexp.compare c cp2 = 0 ->
        lite (c,
              lite (p1, t, lite (p21, t2, e2)),
              lite (p2, t, lite (p22, t2, e2)))
    |(BExt _ | BCnj _ | BDsj _ | BIte _ as c), (LTop | LVar _ as t), LBot
          when eager ->
        (try tope c |^| t with Exit -> lite1 (c, t, LBot))
    |(BExt _ | BCnj _ | BDsj _ | BIte _ as c), (LBot | LVar _ as t), LTop
          when eager ->
        (try bote c |.| t with Exit -> lite1 (c, t, LTop))
    | tpl
      -> lite1 tpl

  and tope = function
    | BExt (LExt (LEq (x, LTop)))
    | BExt (LExt (LEq (LTop, x))) -> x
    | BIte (c, p1, p2) -> lite (c, tope p1, tope p2)
    | BDsj [a] | BCnj [a] -> tope a
    | BDsj (a :: tl) -> tope a |.| tope (BDsj tl)
    | BCnj l
      -> begin match extract_leqs l with
        | [], _ -> raise Exit
        | leqs, [] -> List.fold_left (fun e x -> e |^| tope x) top leqs
        | leqs, (c0 :: cs)
          -> let l = List.fold_left (fun e x -> e |^| tope x) top leqs in
            lite0 ~recur:lite0 (conj (c0 :: cs), l, bottom)
      end
    | _ -> raise Exit

  and bote = function
    | BExt (LExt (LEq (x, LBot)))
    | BExt (LExt (LEq (LBot, x))) -> x
    | BIte (c, p1, p2) -> lite (c, bote p1, bote p2)
    | BDsj [a] | BCnj [a] -> bote a
    | BDsj (a :: tl) -> bote a |^| bote (BCnj tl)
    | BCnj l
      -> begin match extract_leqs l with
        | [], _ -> raise Exit
        | leqs, [] -> List.fold_left (fun e x -> e |.| bote x) bottom leqs
        | leqs, (c0 :: cs)
          -> let l = List.fold_left (fun e x -> e |.| bote x) bottom leqs in
            lite0 ~recur:lite0 (conj (c0 :: cs), l, top)
      end
    | _ -> raise Exit

  let rec bite0 tpl =
    Bexp.mk_ite0 ~conj ~disj ~recur:bite tpl

  and bite = function
    |(c,
      BExt (LExt (LEq (v, LTop) | LEq (LTop, v))),
      BExt (LExt (LEq (w, LTop) | LEq (LTop, w))))
      -> lite (c, v, w) ==.~ top
    |(c,
      BExt (LExt (LEq (v, LBot) | LEq (LBot, v))),
      BExt (LExt (LEq (w, LBot) | LEq (LBot, w))))
      -> lite (c, v, w) ==.~ bottom
    | tpl
      -> bite0 tpl

end
