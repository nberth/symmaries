(** {1 Representation of symbolic control flow graphs} *)

open Format
open Rutils
open Utils

(* --- *)

type typ =
  | Bool
  | Enum of String.t
  | Level
type pos = Z | S of pos

let pp_typ fmt = function
  | Bool -> pp_print_string fmt "bool"
  | Enum s -> pp_print_string fmt s
  | Level -> pp_print_string fmt "level"

let typ_dispatch
    ?(b = begin fun _ _ ->
      failwith "Invalid dispatch on boolean type"
    end)
    ?(l = begin fun _ _ ->
      failwith "Invalid dispatch on level type"
    end)
    ?(e = begin fun _ _ ->
      failwith "Invalid dispatch on enumerated type"
    end)
    c
    =
  function
    | Bool -> b c Bool
    | Enum _ as t -> e c t
    | Level -> l c Level

(* --- *)

type kind =
  | State
  | Input
  | Controllable of aim * group * rank
and aim = Min | Max
and rank = pos
and group = pos

(* --- *)

module type VAR = sig
  include ORDERED_HASHABLE_TYPE
  val raw: string -> t
end

module type ENV = sig
  module Var: VAR
  include Exprs.ALL with module Var := Var

  (* --- *)

  module type BOPS = Exprs.BOPS
    with type bexp := bexp

  module type CONDITIONALS = Exprs.CONDITIONALS
    with type bexp := bexp
    and type lexp := lexp
    and type eexp := eexp

  module type HEURISTICS = Heuristics.T
    with type bexp := bexp
    and type lexp := lexp
    and type eexp := eexp

  (* --- *)

  module VarSet: SET with type elt = Var.t
  module VarMap: sig
    include MAP with type key = Var.t
    val keys: _ t -> VarSet.t
  end
  type bindings = exp VarMap.t

  (* --- *)

  module Updates: sig
    type b = bexp VarMap.t
    type l = lexp VarMap.t
    type e = eexp VarMap.t
    type u
    include Helpers.ORDERED_TYPE with type t := u
    val id: u
    val ( <~ ): Var.t -> 'a -> 'a VarMap.t
    val memb: Var.t -> u -> bool
    val meml: Var.t -> u -> bool
    val meme: Var.t -> u -> bool
    val findb: Var.t -> u -> bexp
    val findl: Var.t -> u -> lexp
    val finde: Var.t -> u -> eexp
    val find: Var.t -> u -> exp
    val replb: Var.t -> bexp -> u -> u
    val repll: Var.t -> lexp -> u -> u
    val reple: Var.t -> eexp -> u -> u
    val foldb: (Var.t -> bexp -> 'a -> 'a) -> u -> 'a -> 'a
    val foldl: (Var.t -> lexp -> 'a -> 'a) -> u -> 'a -> 'a
    val folde: (Var.t -> eexp -> 'a -> 'a) -> u -> 'a -> 'a
    val bindings: u -> bindings
    val acc_subst: subst -> u -> u -> u
    val make_subst: u -> subst
    val compose: u -> u -> u
    module type GUARDING = sig
      val guard: bexp -> u -> u
      val ite: bexp -> u -> u -> u
    end
    module type UOPS = sig
      val unionb: b -> u -> u
      val unionl: l -> u -> u
      val unione: e -> u -> u
      val union: u -> u -> u
      val repl: u -> u -> u
      val setb: Var.t -> bexp -> u -> u
      val setl: Var.t -> lexp -> u -> u
      val sete: Var.t -> eexp -> u -> u
      val mavb: (Var.t -> bexp -> bexp) -> u -> u
      val mavl: (Var.t -> lexp -> lexp) -> u -> u
      val mave: (Var.t -> eexp -> eexp) -> u -> u
      val mapb: (bexp -> bexp) -> u -> u
      val mapl: (lexp -> lexp) -> u -> u
      val mape: (eexp -> eexp) -> u -> u
      module MakeGuarding: functor (Conds: CONDITIONALS) -> GUARDING
    end
    module MakeUOps: functor (BOps: BOPS) -> UOPS
    include UOPS
    include GUARDING
  end
  type updates = Updates.u

end

(* --- *)

module MakeEnv
  (Var: VAR)
  :
  (ENV with module Var = Var)
  =
struct

  module Var = Var
  include (Exprs.MakeAll (Var): Exprs.ALL with module Var := Var)

  (* --- *)

  module type BOPS = Exprs.BOPS
    with type bexp := bexp

  module type CONDITIONALS = Exprs.CONDITIONALS
    with type bexp := bexp
    and type lexp := lexp
    and type eexp := eexp

  module type HEURISTICS = Heuristics.T
    with type bexp := bexp
    and type lexp := lexp
    and type eexp := eexp

  (* --- *)

  module VarSet = MakeSet (Var)
  module VarMap = struct
    include MakeMap (Var)
    let keys m = fold (fun v _ -> VarSet.add v) m VarSet.empty
  end
  type bindings = exp VarMap.t

  let make_subst map : subst = fun v ->
    try match VarMap.find v map with
      | Bexp e -> `Bexp e
      | Lexp e -> `Expr (`Lexp e)
      | Eexp e -> `Expr (`Eexp e)
    with
      | Not_found -> `None

  (* --- *)

  module Updates = struct
    module Map = VarMap
    module PP = MapPP (VarMap)
    open Map
    type b = bexp Map.t
    type l = lexp Map.t
    type e = eexp Map.t
    type u = b * l * e
    let id: u = empty, empty, empty
    let compare = cmp_3
      (Map.compare Bexp.compare)
      (Map.compare Lexp.compare)
      (Map.compare Eexp.compare)
    let print: u PPrt.pp = PP.pp_3 Var.print pp_bexp pp_lexp pp_eexp
    let ( <~ ) = singleton
    let memb v ((b, _, _): u) = Map.mem v b
    let meml v ((_, l, _): u) = Map.mem v l
    let meme v ((_, _, e): u) = Map.mem v e
    let findb v ((b, _, _): u) = Map.find v b
    let findl v ((_, l, _): u) = Map.find v l
    let finde v ((_, _, e): u) = Map.find v e
    let find v u = try Bexp (findb v u) with
      | Not_found -> try Lexp (findl v u) with
          | Not_found -> Eexp (finde v u)
    let replb v x ((b, l, e): u) = Map.add v x b, l, e
    let repll v x ((b, l, e): u) = b, Map.add v x l, e
    let reple v x ((b, l, e): u) = b, l, Map.add v x e
    let foldb f ((b, _, _): u) = Map.fold f b
    let foldl f ((_, l, _): u) = Map.fold f l
    let folde f ((_, _, e): u) = Map.fold f e
    (* let mapb f ((b, l, e): u) = Map.map f b, l, e *)
    (* let mapl f ((b, l, e): u) = b, Map.map f l, e *)
    (* let mape f ((b, l, e): u) = b, l, Map.map f e *)
    let bindings: u -> bindings = fun u -> begin Map.empty
        |> foldb (fun v e -> add v (Bexp e)) u
        |> foldl (fun v e -> add v (Lexp e)) u
        |> folde (fun v e -> add v (Eexp e)) u
    end
    let acc_subst: subst -> u -> u -> u = fun sub a b -> begin a
        |> foldb (fun v e -> replb v (Bexp.subst sub e)) b
        |> foldl (fun v e -> repll v (Lexp.subst sub e)) b
        |> folde (fun v e -> reple v (Eexp.subst sub e)) b
    end
    let make_subst u = make_subst (bindings u)
    let compose: u -> u -> u = fun a -> acc_subst (make_subst a) a
    module type GUARDING = sig
      val guard: bexp -> u -> u
      val ite: bexp -> u -> u -> u
    end
    module type UOPS = sig
      val unionb: b -> u -> u
      val unionl: l -> u -> u
      val unione: e -> u -> u
      val union: u -> u -> u
      val repl: u -> u -> u
      val setb: Var.t -> bexp -> u -> u
      val setl: Var.t -> lexp -> u -> u
      val sete: Var.t -> eexp -> u -> u
      val mavb: (Var.t -> bexp -> bexp) -> u -> u
      val mavl: (Var.t -> lexp -> lexp) -> u -> u
      val mave: (Var.t -> eexp -> eexp) -> u -> u
      val mapb: (bexp -> bexp) -> u -> u
      val mapl: (lexp -> lexp) -> u -> u
      val mape: (eexp -> eexp) -> u -> u
      module MakeGuarding: functor (Conds: CONDITIONALS) -> GUARDING
    end
    module MakeUOps (BOps: BOPS) = struct
      open BOps
      let repl: u -> u -> u = fun (b, l, e) (b', l', e') ->
        let _first _ x _ = Some x in
        union _first b b', union _first l l', union _first e e'
      let unionb: b -> u -> u = fun b (b', l, e) ->
        union (fun _ a b -> Some Bexp.(a ||. b)) b b', l, e
      let unionl: l -> u -> u = fun l (b, l', e) ->
        b, union (fun _ a b -> Some Lexp.(a |.| b)) l l', e
      let unione: e -> u -> u = fun e (b, l, e') ->
        b, l, union (fun v _ _ ->
          failwith (asprintf "Multiple assignments to enumeration variable %a\
                           " Var.print v)) e e'
      let union: u -> u -> u = fun (b, l, e) u ->
        u |> unionb b |> unionl l |> unione e
      let setb v x = unionb (match x with
        | Bexp.BVar v' when Var.equal v v' -> empty | _ -> v <~ x)
      let setl v x = unionl (match x with
        | Lexp.LVar v' when Var.equal v v' -> empty | _ -> v <~ x)
      let sete v x = unione (match x with
        | Eexp.EVar v' when Var.equal v v' -> empty | _ -> v <~ x)
      let mty = empty                                    (* short local alias *)
      let mapb f ((b, l, e) as u) = foldb (fun v e -> setb v (f e)) u (mty, l, e)
      let mapl f ((b, l, e) as u) = foldl (fun v e -> setl v (f e)) u (b, mty, e)
      let mape f ((b, l, e) as u) = folde (fun v e -> sete v (f e)) u (b, l, mty)
      let mavb f ((b, l, e) as u) = foldb (fun v e -> setb v (f v e)) u (mty, l, e)
      let mavl f ((b, l, e) as u) = foldl (fun v e -> setl v (f v e)) u (b, mty, e)
      let mave f ((b, l, e) as u) = folde (fun v e -> sete v (f v e)) u (b, l, mty)
      module MakeGuarding (Conds: CONDITIONALS) = struct
        open Conds
        let guard: bexp -> u -> u = fun b u -> u
          |> mavb (fun v e -> bite (b, e, (Bexp.var v)))
          |> mavl (fun v e -> lite (b, e, (Lexp.var v)))
          |> mave (fun v e -> eite (b, e, (Eexp.var v)))
        let ite: bexp -> u -> u -> u = fun b (eb, el, ee) (fb, fl, fe) ->
          let mite ite var = Map.merge (fun v e f -> match e, f with
            | None, None -> None
            | Some e, None -> Some (ite (b, e, var v))
            | None, Some f -> Some (ite (b, var v, f))
            | Some e, Some f -> Some (ite (b, e, f)))
          in
          (mite bite Bexp.var eb fb,
           mite lite Lexp.var el fl,
           mite eite Eexp.var ee fe)
      end
    end
    include MakeUOps (BOps)
    include MakeGuarding (Conditionals)
  end
  type updates = Updates.u

end

(* -------------------------------------------------------------------------- *)

module Stats = struct
  open BasicStats.Outputs

  type 'n cardinals =
      {
        nb_locations: 'n;
        nb_transitions: 'n;
        nb_vars: 'n;
      }

  let print_cardinals ?(pk = ps) ?k0 pe fmt { nb_locations;
                                              nb_transitions;
                                              nb_vars } =
    let pk = pk ?k0 in
    pp fmt "@[<2>%a: %a@],@;\
            @[<2>%a: %a@],@;\
            @[<2>%a: %a@]"
      pk "nb_scfg_locations" pe nb_locations
      pk "nb_scfg_transitions" pe nb_transitions
      pk "nb_scfg_variables" pe nb_vars

  let map_cardinals f { nb_locations; nb_transitions; nb_vars } =
    { nb_locations   = f nb_locations;
      nb_transitions = f nb_transitions;
      nb_vars        = f nb_vars       }

  let acc_cardinals f a b =
    { nb_locations   = f a.nb_locations   b.nb_locations;
      nb_transitions = f a.nb_transitions b.nb_transitions;
      nb_vars        = f a.nb_vars        b.nb_vars;        }

  (* --- *)

  type 'n degrees =
      {
        max_in_degree: 'n;
        max_out_degree: 'n;
      }

  let print_degrees ?(pk = ps) ?k0 pe fmt { max_in_degree;
                                            max_out_degree } =
    let pk = pk ?k0 in
    pp fmt "@[<2>%a: %a@],@;\
            @[<2>%a: %a@]"
      pk "max_scfg_in_degree" pe max_in_degree
      pk "max_scfg_out_degree" pe max_out_degree

  let map_degrees f { max_in_degree; max_out_degree } =
    { max_in_degree  = f max_in_degree;
      max_out_degree = f max_out_degree; }

  let acc_degrees f a b =
    { max_in_degree  = f a.max_in_degree  b.max_in_degree;
      max_out_degree = f a.max_out_degree b.max_out_degree; }

  (* --- *)

  type 'n var_decls =
      {
        nb_state_vars: 'n;
        nb_input_vars: 'n;
        nb_contr_vars: 'n;
      }

  let print_var_decls ?(pk = ps) ?k0 pe fmt { nb_state_vars;
                                              nb_input_vars;
                                              nb_contr_vars } =
    let pk = pk ?k0 in
    pp fmt "@[<2>%a: %a@],@;\
            @[<2>%a: %a@],@;\
            @[<2>%a: %a@]"
      pk "nb_scfg_state_variables" pe nb_state_vars
      pk "nb_scfg_input_variables" pe nb_input_vars
      pk "nb_scfg_contr_variables" pe nb_contr_vars

  let map_var_decls f { nb_state_vars; nb_input_vars; nb_contr_vars } =
    { nb_state_vars = f nb_state_vars;
      nb_input_vars = f nb_input_vars;
      nb_contr_vars = f nb_contr_vars; }

  let acc_var_decls f a b =
    { nb_state_vars = f a.nb_state_vars b.nb_state_vars;
      nb_input_vars = f a.nb_input_vars b.nb_input_vars;
      nb_contr_vars = f a.nb_contr_vars b.nb_contr_vars; }

end

(* -------------------------------------------------------------------------- *)

module type LOCATION = sig
  include ORDERED_HASHABLE_TYPE
  val print': escaped: bool -> t pp
  val print_unique: t pp
end

(* --- *)

module type GRAPH = sig
  type data
  type t
  type var
  type bindings
  type loc
  type locs
  type trans
  type graph
  type edge
  type bexp
  val init: data -> var -> loc -> t
  val is_init: t -> loc -> bool
  val init_loc: t -> loc
  val new_init: loc -> t -> t
  val graph: t -> graph
  val locvar: t -> var
  val decl: var -> typ -> kind -> t -> t
  val decl_label: String.t -> String.t -> t -> t
  val locations: t -> locs
  val mem_location: t -> loc -> bool
  val is_sink: t -> loc -> bool
  val add_transition: loc -> trans -> loc -> t -> t
  val merge_transition
    : merge_trans:(trans -> trans -> trans) -> loc -> trans -> loc -> t -> t
  val fold_enums: (String.t -> Strings.t -> 'a -> 'a) -> t -> 'a -> 'a
  val fold_decls: (var -> typ * kind -> 'a -> 'a) -> t -> 'a -> 'a
  val fold_locations: (loc -> 'a -> 'a) -> t -> 'a -> 'a
  val fold_trans_from: (edge -> 'a -> 'a) -> t -> loc -> 'a -> 'a
  val mapfilter_trans_from: (edge -> trans option) -> loc -> t -> t
  val set_initial_bindings: bindings -> t -> t
  val initial_bindings: t -> bindings
  val assertion: t -> bexp
  val assert_global: bexp -> t -> t
  val assert_of_loc: t -> loc -> bexp option
  val assert_in_loc: loc -> bexp -> t -> t
  val refine_assert_of_loc: (bexp -> bexp) -> loc -> t -> t
  val fold_restrictions: (bexp -> locs -> 'a -> 'a) -> t -> 'a -> 'a
  val restrict_to_location: bexp -> loc -> t -> t
  val invar_of_loc: t -> loc -> bexp option
  val invar_in_loc: loc -> bexp -> t -> t
  val refine_invar_of_loc: (bexp -> bexp) -> loc -> t -> t
  val invar: t -> bexp
  val invar_global: bexp -> t -> t
  val set_reachable: loc -> t -> t
  val reachable: t -> loc -> bool
  val data: t -> data
  val new_data: t -> data -> t

  open Stats
  type _ sizes =
    | Cardinals : int cardinals sizes
    | CardinalsNDegrees: (int cardinals * int degrees) sizes
    | VarDecls : int var_decls sizes
  val sizes: 'a sizes -> t -> 'a
end

(* --- *)

module type FIXPOINT = sig
  type location
  type setup
  type data
  type result
  val locres: result -> location -> data
  val foldres: (location -> data -> 'a -> 'a) -> result -> 'a -> 'a
  val apply: (location -> data) -> setup -> result
end

module type FIXPOINT_CHAOTIC = sig
  include FIXPOINT
  module M: Map.S with type key = location
  val apply:
    ?widening_set: location Graph.ChaoticIteration.widening_set ->
    (location -> data) -> setup -> result
end

(* --- *)

module type T = sig
  module Env: ENV
  open Env

  (* --- *)

  module Location: LOCATION
  type loc = Location.t
  module Locations: SET with type elt = loc
  type locs = Locations.t
  module LocMap: MAP with type key = loc

  module Transition: sig
    type t = Bexp.t * Updates.u
    include Helpers.ORDERED_TYPE with type t := t
    val applyu: updates -> t -> t
    val compose: t -> t -> t
    val merge: ?uite:(bexp -> updates -> updates -> updates) -> t -> t -> t
  end
  type trans = Transition.t

  module G: Graph.Sig.P
    with type V.t = loc
    and  type V.label = loc
    and  type E.t = loc * trans * loc
    and  type E.label = trans
  module type GRAPH = GRAPH with type var := Var.t
                            and type loc := loc
                            and type locs := locs
                            and type trans := trans
                            and type bexp := bexp
                            and type bindings := bindings
                            and type graph := G.t
                            and type edge := G.edge
  include GRAPH with type data = unit
  type scfg = t                                                 (* alias type *)

  module MakeGen (Data: sig type t end) : sig
    include GRAPH with type data = Data.t
    val extract: t -> scfg
  end

  (* --- *)

  module type FIXPOINT = FIXPOINT with type location := loc
  module type FPDATA = Graph.Fixpoint.Analysis with type vertex := loc
                                               and type edge := G.edge
                                               and type g := G.t

  module Fixpoint (S: GRAPH) : sig
    type setup
    val setup: S.t -> setup
    val scfg: setup -> S.t
    module Make (D: FPDATA)
      : FIXPOINT with type setup := setup and type data := D.data
  end

  (* --- *)

  module type FIXPOINT_CHAOTIC = FIXPOINT_CHAOTIC with type location := loc
  module type FPCDATA = Graph.ChaoticIteration.Data with type edge := G.edge

  module FixpointChaotic (S: GRAPH) : sig
    type setup
    val setup: S.t -> setup
    val scfg: setup -> S.t
    module Make (D: FPCDATA)
      : FIXPOINT_CHAOTIC with type setup := setup and type data := D.t
  end

  (* --- *)

  module AsDot: sig
    val output: scfg pp
  end
end

(* -------------------------------------------------------------------------- *)

module MakeGraph
  (Env: ENV)
  (Location: LOCATION)
  :
  (T with module Env = Env
     and module Location = Location)
  =
struct
  module Env = Env
  open Env
  module BexpMap = Helpers.MakeMap (Bexp)

  (* --- *)

  module Location = Location
  module Locations = MakeSet (Location)
  module LocMap = MakeMap (Location)
  type loc = Location.t
  type locs = Locations.t

  (* --- *)

  module Transition = struct
    type t = bexp * updates
    let default = Bexp.tt, Updates.id
    let compare = cmp_pair Bexp.compare Updates.compare
    let print fmt (g, u) =
      pp fmt "%a,\\n\\@\n@[%a@]" pp_bexp g Updates.print u
    let applyu: updates -> t -> t = fun a (g, u) ->
      let sub = Updates.make_subst a in
      Bexp.subst sub g, Updates.acc_subst sub a u
    let compose (g0, u0) t1 =
      let g', u' = applyu u0 t1 in
      Bexp.(&&.) g0 g', u'
    let merge ?(uite = Updates.ite) (g0, u0) (g1, u1) =
      (* Must assume determinism: ¬(g0 ∧ g1): *)
      Bexp.(||.) g0 g1, uite g0 u0 u1
  end
  type trans = Transition.t

  (* --- *)

  module G = Graph.Persistent.Digraph.ConcreteLabeled (Location) (Transition)
  module type GRAPH = GRAPH with type var := Var.t
                            and type loc := loc
                            and type locs := locs
                            and type trans := trans
                            and type bexp := bexp
                            and type bindings := bindings
                            and type graph := G.t
                            and type edge := G.edge
  module MakeG0 (Data: sig type t end) = struct
    type t =
        {
          graph: G.t;
          initial_location: Location.t;
          assertions: bexp LocMap.t;
          assertion: bexp;
          restrictions: Locations.t BexpMap.t;
          reachable: Locations.t;
          invars: bexp LocMap.t;
          initial: exp VarMap.t;
          invar: bexp;
          enums: Strings.t StrMap.t;
          decls: (typ * kind) VarMap.t;
          locvar: Var.t;
          data: data;
        }
    and data = Data.t

    (* --- *)

    let graph { graph } = graph
    let locvar { locvar } = locvar

    (* XXX: check enum types are declared... *)
    let decl (v: Var.t) t k s =  { s with decls = VarMap.add v (t, k) s.decls }

    let decl_label e l s =
      let ls = try StrMap.find e s.enums with Not_found -> Strings.empty in
      { s with enums = StrMap.add e (Strings.add l ls) s.enums }
    let fold_enums f { enums } = StrMap.fold f enums
    let fold_decls f { decls } = VarMap.fold f decls

    (* Graph construction and traversal *)
    let add_location l s = { s with graph = G.add_vertex s.graph l }
    let add_transition l t l' s =
      { s with graph = G.add_edge_e s.graph (G.E.create l t l') }
    let merge_transition ~merge_trans l t l' s =
      try let (_, t', _) = G.find_edge s.graph l l' in
          let graph = G.remove_edge s.graph l l' in
          let t = merge_trans t t' in
          { s with graph = G.add_edge_e graph (G.E.create l t l') }
      with Not_found -> add_transition l t l' s
    let init data locvar initial_location =
      { graph = G.empty;
        initial_location;
        invars = LocMap.empty;
        assertions = LocMap.empty;
        assertion = Bexp.tt;
        initial = VarMap.empty;
        invar = Bexp.tt;
        restrictions = BexpMap.empty;
        reachable = Locations.empty;
        enums = StrMap.empty;
        decls = VarMap.empty;
        locvar;
        data;
      } |> add_location initial_location

    let is_init { initial_location } = Location.equal initial_location
    let init_loc { initial_location } = initial_location
    let new_init l s =
      let s = add_location l s in { s with initial_location = l }

    let locations { graph } =
      G.fold_vertex Locations.add graph Locations.empty
    let mem_location { graph } = G.mem_vertex graph
    let is_sink { graph } l = G.out_degree graph l = 0
    let fold_locations f { graph } = G.fold_vertex f graph
    let fold_trans_from f { graph } = G.fold_succ_e f graph

    let fold_succ_e f g l i = try G.fold_succ_e f g l i with
      | Invalid_argument _ -> i

    let mapfilter_trans_from f l ({ graph } as s) =
      let graph, maybe_unreach =
        fold_succ_e begin fun ((src, t, dest) as e) (graph, maybe_unreach) ->
          let graph = G.remove_edge_e graph e in
          match f e with
            | Some t' -> G.add_edge_e graph (src, t', dest), maybe_unreach
            | None -> graph, Locations.add dest maybe_unreach
        end graph l (graph, Locations.empty)
      in
      let has_only_loops graph l =
        G.out_degree graph l > 0
        && List.for_all (Location.equal l) (G.succ graph l)
      in
      let rec prop_unreach maybe_unreach ((graph, deleted) as acc) =
        if Locations.is_empty maybe_unreach then acc else
          let l = Locations.choose maybe_unreach in
          let acc, maybe_unreach =
            if G.in_degree graph l = 0 || has_only_loops graph l
            then ((G.remove_vertex graph l, Locations.add l deleted),
                  G.fold_succ Locations.add graph l maybe_unreach)
            else acc, maybe_unreach
          in
          let maybe_unreach = Locations.remove l maybe_unreach in
          prop_unreach maybe_unreach acc
      in
      let deleted = Locations.empty in
      let graph, deleted = prop_unreach maybe_unreach (graph, deleted) in
      let invars = Locations.fold LocMap.remove deleted s.invars in
      let assertions = Locations.fold LocMap.remove deleted s.assertions in
      let reachable = Locations.fold Locations.remove deleted s.reachable in
      let restrictions = begin s.restrictions
          |> BexpMap.map (fun s -> Locations.diff s deleted)
          |> BexpMap.filter (fun _ s -> not (Locations.is_empty s))
      end in
      { s with graph; invars; assertions; reachable; restrictions }

    (* --- *)

    let of' find k m = try Some (find k m) with Not_found -> None
    and to' find add (+) k e m = add k (try find k m + e with Not_found -> e) m

    let of_loc = of' LocMap.find
    and to_loc = to' LocMap.find LocMap.add

    let of_var k = of' VarMap.find k
    and to_var m = to' VarMap.find VarMap.add m

    let of_bexp k = of' BexpMap.find k
    and to_bexp m = to' BexpMap.find BexpMap.add m

    (* --- *)

    open Bexp.Ops

    (* Getting/setting intial constraint *)
    let initial_bindings { initial } = initial
    let set_initial_bindings initial s = { s with initial }

    (* Getting/setting global/location assertions *)
    let assertion { assertion } = assertion
    let assert_global a s = { s with assertion = a &&. s.assertion }
    let assert_of_loc { assertions } l = of_loc l assertions
    let assert_in_loc l a s = { s with assertions = to_loc ( &&. ) l a s.assertions }
    let refine_assert_of_loc f l s =
      try
        { s with assertions = LocMap.add l (f (LocMap.find l s.assertions))
            s.assertions }
      with Not_found -> s

    (* Setting variable restriction by location *)
    let fold_restrictions f { restrictions } = BexpMap.fold f restrictions
    let restrict_to_location b l s =
      { s with
        restrictions = to_bexp (Locations.union) b (Locations.singleton l)
          s.restrictions }

    (* Getting/setting location invariants *)
    let invar_of_loc { invars } l = of_loc l invars
    let invar_in_loc l i s = { s with invars = to_loc ( &&. ) l i s.invars }
    let refine_invar_of_loc f l s =
      try
        { s with invars = LocMap.add l (f (LocMap.find l s.invars)) s.invars }
      with Not_found -> s

    (* Getting/setting global invariants *)
    let invar { invar } = invar
    let invar_global i s = { s with invar = i &&. s.invar }

    (* Reachability constraints *)
    let set_reachable l s = { s with reachable = Locations.add l s.reachable }
    let reachable { reachable } l = Locations.mem l reachable

    let data { data } = data
    let new_data g data = { g with data }

    open Stats
    type _ sizes =
      | Cardinals : int cardinals sizes
      | CardinalsNDegrees: (int cardinals * int degrees) sizes
      | VarDecls : int var_decls sizes

    let sizes: type s. s sizes -> t -> s = fun s { graph = g; decls } ->
      let cardinals () = { nb_locations = G.nb_vertex g;
                           nb_transitions = G.nb_edges g;
                           nb_vars = VarMap.cardinal decls } in
      match s with
        | Cardinals ->
            cardinals ()
        | CardinalsNDegrees ->
            (cardinals (),
             G.fold_vertex begin fun l { max_in_degree; max_out_degree } ->
               let i = G.in_degree g l and o = G.out_degree g l in
               { max_in_degree = max i max_in_degree;
                 max_out_degree = max o max_out_degree; }
             end g { max_in_degree = 0; max_out_degree = 0 })
        | VarDecls ->
            VarMap.fold begin fun v -> function
              | _, State
                -> fun d -> { d with nb_state_vars = d.nb_state_vars + 1 }
              | _, Input
                -> fun d -> { d with nb_input_vars = d.nb_input_vars + 1 }
              | _, Controllable _
                -> fun d -> { d with nb_contr_vars = d.nb_contr_vars + 1 }
            end decls { nb_state_vars = 0;
                        nb_input_vars = 0;
                        nb_contr_vars = 0 }

  end

  include MakeG0 (struct type t = unit end)
  type scfg = t

  module MakeGen (Data: sig type t end) = struct
    module GD = MakeG0 (Data)
    let extract: GD.t -> scfg = function
    GD.{ graph; initial_location; assertions; assertion; restrictions;
         reachable; invars; initial; invar; enums; decls; locvar; } ->
      { graph; initial_location; assertions; assertion; restrictions;
        reachable; invars; initial; invar; enums; decls; locvar;
        data = (); }
    include GD
  end

  (* --- *)

  module type FIXPOINT = FIXPOINT with type location := loc
  module type FPDATA = Graph.Fixpoint.Analysis with type vertex := loc
                                               and type edge := G.edge
                                               and type g := G.t

  module Fixpoint (S: GRAPH) = struct
    type setup = S.t
    let setup: S.t -> setup = ign
    let scfg: setup -> S.t = ign

    module Make (D: FPDATA)
      : FIXPOINT with type setup := setup and type data := D.data =
    struct
      module F = Graph.Fixpoint.Make (G) (struct
        include D
        type vertex = loc
        type edge = G.edge
        type g = G.t
      end)
      type result = setup * (loc -> D.data)
      let locres (_, r) l = r l
      let foldres f (g, r) = S.fold_locations (fun l -> f l (r l)) g
      let apply init s : result =
        let g = S.graph s and i = S.init_loc s in
        if G.mem_vertex g i && G.out_degree g i > 0
        then s, F.analyze init g
        else s, init
    end
  end

  (* --- *)

  module type FIXPOINT_CHAOTIC = FIXPOINT_CHAOTIC with type location := loc
  module type FPCDATA = Graph.ChaoticIteration.Data with type edge := G.edge

  module FixpointChaotic (S: GRAPH) = struct
    module WTO = Graph.WeakTopological.Make (G)
    type wto =
      | Graph of loc Graph.WeakTopological.t
      | Loc of loc
    type setup = wto * S.t
    let setup s : setup =
      let g = S.graph s and i = S.init_loc s in
      (if G.mem_vertex g i && G.out_degree g i > 0
       then Graph (WTO.recursive_scc g i)
       else Loc i),
      s
    let scfg: setup -> S.t = snd

    module Make (D: FPCDATA)
      : FIXPOINT_CHAOTIC with type setup := setup and type data := D.t =
    struct
      module F = Graph.ChaoticIteration.Make (G) (struct
        include D
        type edge = G.edge
      end)
      module M = F.M
      type result = D.t M.t
      let locres m l = M.find l m
      let foldres = M.fold
      let apply ?(widening_set = Graph.ChaoticIteration.FromWto) init = function
        | Graph w, s -> F.recurse (S.graph s) w init widening_set 0
        | Loc l, s -> M.singleton l (init l)
    end
  end

  (* --- *)

  module AsDot = struct

    let output fmt scfg =
      let print_location fmt l =
        Location.print' ~escaped:true fmt l;
        match invar_of_loc scfg l with
          | None -> ()
          | Some i -> pp fmt "\\n\\@\n%a" pp_bexp i
      in
      let open Graph.Graphviz in
      let module D = Dot (struct
        include G
        let vertex_name v = asprintf "\"%a\"" Location.print_unique v
        let graph_attributes _ = []
        let default_vertex_attributes _ = [ `Shape `Box ]
        let default_edge_attributes _ = []
        let vertex_attributes v =
          [ `Label (asprintf "%a" print_location v) ]
        let edge_attributes e =
          [ `Label (asprintf "%a" Transition.print (E.label e)); ]
        let get_subgraph v = None
      end) in
      D.fprint_graph fmt scfg.graph

  end

end

(* -------------------------------------------------------------------------- *)
