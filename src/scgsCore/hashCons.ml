open Format
open Var

module Exprs (Env: SCFG.ENV) = struct
  open Env
  module BH = Hashtbl.Make (Bexp)
  module LH = Hashtbl.Make (Lexp)
  module EH = Hashtbl.Make (Eexp)

  type memo =
      {
        bh: Var.t BH.t;
        lh: Var.t LH.t;
        eh: Var.t EH.t;
        cnt: int ref;
        mutable bindings: binding list;
      }
  and binding = Var.t * exp

  let new_memo = function
    | Some par ->
        { par with bindings = [] }
    | None ->
        {
          bh = BH.create 17;
          lh = LH.create 17;
          eh = EH.create 17;
          cnt = ref 0;
          bindings = [];
        }

  let pop_bindings ({ bh; lh; eh; bindings } as m) acc =
    let acc = List.fold_left begin fun acc (v, e) ->
      begin match e with
        | Bexp e -> BH.remove bh e
        | Lexp e -> LH.remove lh e
        | Eexp e -> EH.remove eh e
      end;
      VarMap.add v e acc
    end acc bindings in
    m.bindings <- [];
    acc

  let newvar m e =
    let v = Env.Var.raw (asprintf "l%03i" !(m.cnt)) in
    incr m.cnt;
    m.bindings <- (v, e) :: m.bindings;
    v

  let rec bexp ?(toplevel = false) : memo -> bexp -> bexp = fun ({ bh = h } as m) ->
    let open Eexp.Cond in
    let open Lexp.Cond in
    let open Cond in
    let open Bexp in
    let hash e = let v = newvar m (Bexp e) in BH.add h e v; var v in
    let cons ~toplevel e = try var (BH.find h e) with
      | Not_found -> if toplevel then e else hash e in
    let pair cstr e f = if compare e f < 0 then cstr e f else cstr f e in
    let rec (~%) ?(toplevel = false) = let cons = cons ~toplevel in function
      | BCnj [] | BDsj [] | BVar _ as e -> e
      | BNot e -> cons (BNot ~%e)
      | BCnj [a] | BDsj [a] -> (~%) ~toplevel a
      | BCnj (e :: f :: []) -> cons (pair (&&.) ~%e ~%f)
      | BCnj (e :: f :: tl) -> ~%(BCnj (cons (pair (&&.) ~%e ~%f) :: tl))
      | BDsj (e :: f :: []) -> cons (pair (||.) ~%e ~%f)
      | BDsj (e :: f :: tl) -> ~%(BDsj (cons (pair (||.) ~%e ~%f) :: tl))
      | BIte (c, t, e) -> cons (BIte (~%c, ~%t, ~%e))
      | BEq (e, f) -> cons (pair (==.) ~%e ~%f)
      | BNe (e, f) -> cons (pair (<>.) ~%e ~%f)
      | BExt (LExt (LLe (e, f))) -> cons (BExt (LExt (LLe (lexp m e, lexp m f))))
      | BExt (LExt (LEq (e, f))) -> cons (BExt (LExt (LEq (lexp m e, lexp m f))))
      | BExt (EExt (EEq (e, f))) -> cons (BExt (EExt (EEq (eexp m e, eexp m f))))
    in
    (~%) ~toplevel

  and lexp ?(toplevel = false) : memo -> lexp -> lexp = fun ({ lh = h } as m) ->
    let open Lexp in
    let hash e = let v = newvar m (Lexp e) in LH.add h e v; var v in
    let cons ~toplevel e = try var (LH.find h e) with
      | Not_found -> if toplevel then e else hash e in
    let pair cstr e f = if compare e f < 0 then cstr e f else cstr f e in
    let rec (~%) ?(toplevel = false) = let cons = cons ~toplevel in function
      | LTop | LBot | LVar _ as e -> e
      | LLub (e, f, []) -> cons (pair (|.|) ~%e ~%f)
      | LLub (e, f, g::tl) -> ~%(LLub (cons (pair (|.|) ~%e ~%f), g, tl))
      | LGlb (e, f, []) -> cons (pair (|^|) ~%e ~%f)
      | LGlb (e, f, g::tl) -> ~%(LGlb (cons (pair (|^|) ~%e ~%f), g, tl))
      | LIte (c, t, e) -> cons (LIte (bexp m c, ~%t, ~%e))
    in
    (~%) ~toplevel

  and eexp ?(toplevel = false) : memo -> eexp -> eexp = fun ({ eh = h } as m) ->
    let open Eexp in
    let hash e = let v = newvar m (Eexp e) in EH.add h e v; var v in
    let cons ~toplevel e = try var (EH.find h e) with
      | Not_found -> if toplevel then e else hash e in
    let rec (~%) ?(toplevel = false) = let cons = cons ~toplevel in function
      | ECst _ | EVar _ as e -> e
      | EIte (c, t, e) -> cons (EIte (bexp m c, ~%t, ~%e))
    in
    (~%) ~toplevel

  let bexp = bexp ~toplevel:true
  let lexp = lexp ~toplevel:true
  let eexp = eexp ~toplevel:true
  let exp: memo -> exp -> exp = fun m -> function
    | Bexp e -> Bexp (bexp m e)
    | Lexp e -> Lexp (lexp m e)
    | Eexp e -> Eexp (eexp m e)

end
