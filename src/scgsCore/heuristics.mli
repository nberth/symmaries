
(* --- *)

module type OPTIONS = sig
  val eager: bool
end
module DefaultOptions: OPTIONS

(* --- *)

module type T = sig
  include Exprs.CONDITIONALS
  include Exprs.BOPS with type bexp := bexp
end

module Exprs (X: Exprs.ALL) (O: OPTIONS) :
  (T with type bexp = X.bexp
     and type lexp = X.lexp
     and type eexp = X.eexp)

(* --- *)
