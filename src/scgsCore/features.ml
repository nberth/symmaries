open Utils

type flows = NoFlows | ExplicitFlows | AllFlows
type heapdom = DeepAlias | ShallowAlias | DumbAlias | Connect | Share | FieldShare

let pp_heapdom: heapdom pp = fun fmt -> function
  | DeepAlias -> String.print fmt "deepalias"
  | ShallowAlias -> String.print fmt "shallowalias"
  | DumbAlias -> String.print fmt "dumbalias"
  | Connect -> String.print fmt "connect"
  | Share -> String.print fmt "share"
  | FieldShare -> String.print fmt "fieldshare"

type incompatible =
  | HeapDoms of heapdom * heapdom
let pp_incompatible fmt = function
  | HeapDoms (h1, h2)
    -> pp fmt "symbolic@ abstract@ heaps@ `%a'@ and@ `%a'\
             " pp_heapdom h1 pp_heapdom h2
exception IncompatibleFeatures of incompatible

module type T = sig
  val heapdom: heapdom
  val track_levels: flows
  val track_taints: bool
  val enforce_privacy: bool
  val enforce_integrity: bool
  val evil_static_fields: bool
  val sink_static_fields: bool
  include OOLang.CFG.OPTIONS
end
module Default: T = struct
  let heapdom = DeepAlias
  let track_levels = AllFlows
  let track_taints = false
  let enforce_privacy = true
  let enforce_integrity = false
  let evil_static_fields = false
  let sink_static_fields = false
  include OOLang.CFG.DefaultOptions
end

(* --- *)

module Record = struct

  type fflag =
    | Ignore
    (* TODO: ModelFlows of ftype * bool *)
    | ModelFlows of ftype
    | Enforce of ftype
  and ftype = Explicit | All

  let compare_ftypes f1 f2 = match f1, f2 with
    | Explicit, Explicit | All, All -> 0
    | Explicit, _ -> -1
    | _ -> 1

  let merge_ftypes f1 f2 = match f1, f2 with
    | All, All -> All
    | _ -> Explicit

  let compare_fflags f1 f2 = match f1, f2 with
    | Ignore, Ignore -> 0
    | ModelFlows a, ModelFlows b | Enforce a, Enforce b -> compare_ftypes a b
    | Ignore, _ -> -1 | _, Ignore -> 1
    | ModelFlows _, _ -> -1 | _, ModelFlows _ -> 1

  let merge_fflags f1 f2 = match f1, f2 with
    | Ignore, _ | _, Ignore -> Ignore
    | ModelFlows a, Enforce b | Enforce b, ModelFlows a
    | ModelFlows a, ModelFlows b -> ModelFlows (merge_ftypes a b)
    | Enforce a, Enforce b -> Enforce (merge_ftypes a b)

  let merge_heapdom a b =
    if a = b then a else raise @@ IncompatibleFeatures (HeapDoms (a, b))

  type bflag = bool

  let merge_bflags = ( || )

  type t =
      {
        heapd: heapdom;
        privacy: fflag;
        integrity: fflag;
        excs: bflag;
        sevil: bflag;
        ssink: bflag;
      }

  let compatible: t -> t -> _ = fun { heapd = h1 } { heapd = h2 } ->
    if h1 = h2 then Ok () else Error (HeapDoms (h1, h2))

  let merge
      { heapd = h1; privacy = p1; integrity = i1; excs = e1;
        sevil = n1; ssink = s1; }
      { heapd = h2; privacy = p2; integrity = i2; excs = e2;
        sevil = n2; ssink = s2; }
      =
    { heapd = merge_heapdom h1 h2;
      privacy = merge_fflags p1 p2;
      integrity = merge_fflags i1 i2;
      excs = merge_bflags e1 e2;
      sevil = merge_bflags n1 n2;
      ssink = merge_bflags s1 s2; }

  let print_heapdom fmt heapdom =
    pp fmt "heapdom=%a" pp_heapdom heapdom

  let print_fflag ~neg ~enforce ~flow fmt = function
    | Ignore when neg -> pp fmt "-%s" flow
    | ModelFlows All when neg -> pp fmt "+%s-%s" flow enforce
    | ModelFlows All -> pp fmt "+%s" flow
    | ModelFlows Explicit when neg -> pp fmt "+%s-%s,%s=explicit" flow enforce flow
    | ModelFlows Explicit -> pp fmt "+%s,%s=explicit" flow flow
    | Enforce All -> pp fmt "+%s" enforce
    | Enforce Explicit -> pp fmt "+%s,%s=explicit" enforce flow
    | _ -> ()

  let print_bflag ~neg ~name fmt = function
    | false when neg -> pp fmt "-%s" name
    | true -> pp fmt "+%s" name
    | _ -> ()

  let print: t pp = fun fmt { heapd; privacy; integrity; excs; sevil; ssink; } ->
    let pf = print_fflag ~neg:true in
    pp fmt "%a%a%a%a%a%a"
      print_heapdom heapd
      (pf ~enforce:"enforce_privacy" ~flow:"levels") privacy
      (pf ~enforce:"enforce_integrity" ~flow:"taints") integrity
      (print_bflag ~neg:true ~name:"exceptions") excs
      (print_bflag ~neg:true ~name:"evil_statics") sevil
      (print_bflag ~neg:true ~name:"sink_statics") ssink

  let print_enabled: t pp = fun fmt { heapd; privacy; integrity; excs;
                                    sevil; ssink; } ->
    let pf = print_fflag ~neg:false in
    pp fmt "%a%a%a%a%a%a"
      print_heapdom heapd
      (pf ~enforce:"enforce_privacy" ~flow:"levels") privacy
      (pf ~enforce:"enforce_integrity" ~flow:"taints") integrity
      (print_bflag ~neg:false ~name:"exceptions") excs
      (print_bflag ~neg:false ~name:"evil_statics") sevil
      (print_bflag ~neg:false ~name:"sink_statics") ssink

  let missing_bflag = function
    | true, false -> Some true
    | _ -> None

  let missing_fflag = function
    | Ignore, _ -> None
    | x, Ignore -> Some x
    | ModelFlows _, _ -> None
    | x, ModelFlows _ -> Some x
    | Enforce _, Enforce _ -> None

  type missing = string list
  let missing_flags: t -> t -> missing =
    fun { privacy = p1; integrity = i1; excs = e1; sevil = n1; ssink = s1; }
      { privacy = p2; integrity = i2; excs = e2; sevil = n2; ssink = s2; } ->
        let acc_f ~enforce ~flow xy = match missing_fflag xy with
          | Some (ModelFlows _) -> List.cons flow
          | Some (Enforce _) -> List.cons enforce
          | _ -> ign
        and acc_b ~name xy = match missing_bflag xy with
          | Some true -> List.cons name
          | _ -> ign
        in
        ([] |> acc_f ~enforce:"enforce_privacy" ~flow:"levels" (p1, p2)
            |> acc_f ~enforce:"enforce_integrity" ~flow:"taints" (i1, i2)
            |> acc_b ~name:"exceptions" (e1, e2)
            (* |> acc_b ~name:"evil_statics" (n1, n2) *)
            (* |> acc_b ~name:"sink_statics" (s1, s2) *)
            |> List.rev)

  let print_missing ~pref_unique ~pref_mult : missing pp = fun fmt -> function
    | [] -> pp fmt "<none>"
    | [f] -> pp fmt pref_unique; pp fmt "`%s'" f
    | fs ->(pp_lst ~fempty:"<none>" ~fopen:(pref_mult^^"`")
             ~fclose:"'" ~fsep:"'@ and@ `" Format.pp_print_string fmt fs)

end
type record = Record.t

let record (module T: T) : record =
  let open T in
  Record.{
    heapd = heapdom;
    privacy = (match enforce_privacy, track_levels with
      | true, AllFlows -> Enforce All
      | true, ExplicitFlows -> Enforce Explicit
      | false, AllFlows -> ModelFlows All
      | false, ExplicitFlows -> ModelFlows Explicit
      | _, NoFlows -> Ignore);
    integrity = (match enforce_integrity with
      | true -> Enforce All
      | false when track_taints -> ModelFlows All
      | false -> Ignore);
    excs = track_excs;
    sevil = evil_static_fields;
    ssink = sink_static_fields;
  }

let match_requirements (module T: T) r : bool =
  Record.missing_flags (record (module T)) r = []

(* --- *)

module type FILTERS = sig
  module Features: T
  val implicit_flows: bool
  val ( |>~ ): 'a -> ('a -> 'a) -> 'a
  val ( |>~+ ): 'a -> ('a -> 'a) -> 'a                               (* levels = all *)
  val ( |>? ): 'a -> ('a -> 'a) -> 'a
  val ( |>! ): 'a -> ('a -> 'a) -> 'a
  val ( |>!~ ): 'a -> ('a -> 'a) -> 'a
  val ( |>!? ): 'a -> ('a -> 'a) -> 'a
  val ( |>>~ ): 'a -> ('a -> 'a) -> 'a           (* enforce_privacy *)
  val ( |>>~+ ): 'a -> ('a -> 'a) -> 'a          (* enforce_privacy,levels=all *)
  val ( |>>? ): 'a -> ('a -> 'a) -> 'a           (* enforce_integrity *)
  val record: record
end

module MakeFilters (Features: T) : FILTERS = struct
  module Features = Features
  open Features
  let implicit_flows = track_levels = AllFlows
  let ( |>~ ) a f = if track_levels <> NoFlows then f a else a
  let ( |>~+ ) a f = if implicit_flows then f a else a
  let ( |>? ) a f = if track_taints then f a else a
  let ( |>! ) a f = if track_excs then f a else a
  let ( |>!~ ) a f = a |>~ fun a -> a |>! f
  let ( |>!? ) a f = a |>? fun a -> a |>! f
  let ( |>>~ ) a f = if enforce_privacy then f a else a
  let ( |>>~+ ) a f = if track_levels = AllFlows then a |>>~ f else a
  let ( |>>? ) a f = if enforce_integrity then f a else a
  let record = record (module Features)
end

(* --- *)
