(** {1 Heap models and relations} *)

open Rutils
open Utils
open Base
open OOLang
open Var

(* -------------------------------------------------------------------------- *)

(** {2 Heap models} *)

(** {3 Relations} *)

module type ALIAS_REL = sig
  val alias_vars: AliasRelVars.t                                        (* R~ *)
end

module type FIELD_ALIAS_REL = sig
  val field_alias_vars: FieldAliasRelVars.t                             (* R⤳ *)
end

module type SHARE_REL = sig
  val share_vars: ShareRelVars.t                                       (* R>< *)
end

module type FIELD_SHARE_REL = sig
  val field_share_vars: FieldShareRelVars.t                           (* R.>< *)
end

module type CONNECT_REL = sig
  val connect_vars: AliasRelVars.t                                      (* R~ *)
end

(** {3 Combinations} *)

(** Each heap model is defined based on a set of reference variables and some
    relations represented as sets of symbolic variables. *)

module type SHARE_MODEL_RELS = sig
  (** Share model *)
  val all_refs: Base.Vars.t                                             (* R *)
  include SHARE_REL
end

module type DEEP_ALIAS_MODEL_RELS = sig
  (** Deep-alias model *)
  include SHARE_MODEL_RELS
  include ALIAS_REL
  include FIELD_ALIAS_REL
end

module type FIELD_SHARE_MODEL_RELS = sig
  (** Field-share model *)
  val all_refs: Base.Vars.t                                           (* R *)
  include ALIAS_REL
  include FIELD_SHARE_REL
end

module type CONNECT_MODEL_RELS = sig
  (** Connect model *)
  include SHARE_MODEL_RELS
  include CONNECT_REL
end

(* -------------------------------------------------------------------------- *)

(** {2 Individual heap-related relations} *)

(** {3 Preliminary utilities for defining the relations and associated
    helpers} *)

module type RELATIONS_OVERAPPROX = sig
  type refdom
  module RefRels: sig
    val may_alias: refdom -> refdom -> bool
    val may_field_alias: refdom -> refdom -> bool
    val may_share_mutable: refdom -> refdom -> bool
    val may_field_share_mutable: refdom -> refdom -> bool
  end
end

module type COMMON_REL_HELPERS = sig
  module Env: SCFG.ENV
  open Base
  val fold_all_refs: (var -> 'a -> 'a) -> 'a -> 'a
  val fold_all_ref_pairs: (var -> var -> 'a -> 'a) -> 'a -> 'a
  val fold_all_ref_triples: (var -> var -> var -> 'a -> 'a) -> 'a -> 'a
end

module CommonRefHelpers
  (Env: SCFG.ENV)
  (Refs: sig val all_refs: Base.Vars.t end)
  :
  (COMMON_REL_HELPERS with module Env = Env)
  =
struct
  module Env = Env
  let fold_all_refs f = Base.Vars.fold f Refs.all_refs
  let fold_all_ref_pairs f = fold_all_refs (fun r -> fold_all_refs (f r))
  let fold_all_ref_triples f = fold_all_refs (fun r -> fold_all_ref_pairs (f r))
end

(** {3 Sharing} *)

module type SHARE_REL_CONSTR = sig
  (** Helpers to easily rule out impossible sharing cases in ref/obj model: *)
  module Env: SCFG.ENV with type Var.t = Var.t
  open Base
  module S = ShareRelVar
  val ( ?><? ): S.t -> bool
  val ( $><. ): var -> var -> Env.bexp
  val ( @@><? ): S.v -> (S.t -> 'a -> 'b -> 'b) -> 'a -> 'b -> 'b
  val fold_all_share_vars: (S.t -> 'a -> 'a) -> 'a -> 'a
  val share_var: ?mk:(Env.Bexp.var -> Env.Bexp.t) -> var -> var -> Env.bexp
  val ( $><? ): var -> var -> Env.Var.t option
end

module type SHARE_REL_INSTANCIATIONS = sig
  type refdom
  module Ss = ShareRelVars
  val share_vars: refdom Base.VarMap.t -> Ss.t
  val sharing_vars: refdom -> refdom Base.VarMap.t -> Base.Vars.t
end

module DummyShareHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: SHARE_MODEL_RELS)
  :
  (SHARE_REL_CONSTR with module Env := Env)
  =
struct
  module S = ShareRelVar
  open Env.Bexp
  open S.Ops

  let ( ?><? ) v = ShareRelVars.mem v RefSets.share_vars
  let ( @@><? ) _ _ _ = ign
  let ( $><? ) r r' = None
  let fold_all_share_vars f = ign
  let share_var ?(mk = var) r r' = match r $>< r' with
    | S.Const true -> tt
    | S.Const false -> ff
    | S.Value v when ?><? v -> tt
    | _ -> ff
  let ( $><. ) r r' = share_var r r'
end

module ActualShareHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: SHARE_MODEL_RELS)
  :
  (SHARE_REL_CONSTR with module Env := Env)
  =
struct
  include DummyShareHelpers (Env) (RefSets)
  module Ss = ShareRelVars
  open Env.Bexp
  open S.Ops

  let ( @@><? ) = function
    | S.Value v when ?><? v -> fun f x -> f v x
    | _ -> fun _ _ -> ign
  let ( $><? ) r r' = match r $>< r' with
    | S.Value v when ?><? v -> Some (ShareRel v)
    | _ -> None
  let fold_all_share_vars f = Ss.fold f RefSets.share_vars
  let share_var ?(mk = var) r r' = match r $>< r' with
    | S.Const true -> tt
    | S.Const false -> ff
    | S.Value v when ?><? v -> mk (ShareRel v)
    | _ -> ff
  let ( $><. ) r r' = share_var r r'
end

module ShareRelInvariants =
  functor (Env: SCFG.ENV with type Var.t = Var.t) ->
    functor (RH: COMMON_REL_HELPERS with module Env := Env) ->
      functor (SRH: SHARE_REL_CONSTR with module Env := Env) ->
        functor (ObjTyps: OBJ_TYPS with type lexp := Env.lexp
                                   and type texp := Env.bexp) ->
          functor (H: Env.HEURISTICS) ->
struct
  open RH
  open SRH
  open Env.Bexp
  open Env.Lexp
  open ObjTyps
  open H

  let share_rel_is_eqrel =
    fold_all_ref_triples begin fun r s t ->
      (&&.) (((r $><. s) &&. (s $><. t)) =>. (r $><. t))
    end

  let sound_taints_wrt_share_rel =
    fold_all_ref_pairs begin fun r s ->
      (&&.) ((r $><. s) =>. (?^r ==. ?^s))
    end

  let sound_levels_wrt_share_rel =
    fold_all_ref_pairs begin fun r s ->
      (&&.) ((r $><. s) =>. (~^r ==.~ ~^s))
    end
end

module ShareRefSetsWalk (I: RELATIONS_OVERAPPROX)
  : SHARE_REL_INSTANCIATIONS with type refdom := I.refdom =
struct
  open I.RefRels
  open ShareRelVar.Ops
  open Base
  module Ss = ShareRelVars

  let share_vars refs : Ss.t =
    Ss.empty |> VarMapProd.fold_unique_pairs begin fun r rt s st ->
      if may_share_mutable rt st then Ss.add (r $>< s) else ign
    end refs

  let sharing_vars (refdom: I.refdom) refs : Vars.t =
    Vars.empty |> VarMap.fold begin fun r rt ->
      if may_share_mutable rt refdom then Vars.add r else ign
    end refs
end

(* --- *)

(** {3 Aliasing} *)

module type ALIAS_REL_CONSTR = sig
  (** Helpers to easily rule out impossible shallow aliasing cases in ref/obj
      model: *)
  module Env: SCFG.ENV with type Var.t = Var.t
  open Base
  module A = AliasRelVar
  val ( ?~? ): A.t -> bool
  val ( $~. ): var -> var -> Env.bexp
  val ( @@~? ): A.v -> (A.t -> 'a -> 'b -> 'b) -> 'a -> 'b -> 'b
  val fold_all_alias_vars: (A.t -> 'a -> 'a) -> 'a -> 'a
  val alias_var: ?mk:(Env.Bexp.var -> Env.Bexp.t) -> var -> var -> Env.bexp
  val ( $~? ): var -> var -> Env.Var.t option
end with module A := AliasRelVar

module type ALIAS_REL_INSTANCIATIONS = sig
  type refdom
  module As = AliasRelVars
  val alias_vars: refdom Base.VarMap.t -> As.t
  val aliasing_vars: refdom -> refdom Base.VarMap.t -> Base.Vars.t
end

module DummyAliasHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: ALIAS_REL)
  :
  (ALIAS_REL_CONSTR with module Env := Env)
  =
struct
  module A = AliasRelVar
  open Env.Bexp
  open A.Ops

  let ( ?~? ) v = AliasRelVars.mem v RefSets.alias_vars
  let ( @@~? ) _ _ _ = ign

  let ( $~? ) r r' = None
  let fold_all_alias_vars f = ign
  let alias_var ?(mk = var) r r' = match r $~ r' with
    | A.Const true -> tt
    | A.Const false -> ff
    | A.Value v when ?~? v -> tt
    | _ -> ff
  let ( $~. ) r r' = alias_var r r'
end

module ActualAliasHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: ALIAS_REL)
  :
  (ALIAS_REL_CONSTR with module Env := Env)
  =
struct
  include DummyAliasHelpers (Env) (RefSets)
  module A = AliasRelVar
  module As = AliasRelVars
  open Env.Bexp
  open A.Ops

  let ( @@~? ) = function
    | A.Value v when ?~? v -> fun f x -> f v x
    | _ -> fun _ _ -> ign
  let ( $~? ) r r' = match r $~ r' with
    | A.Value v when ?~? v -> Some (AliasRel v)
    | _ -> None
  let fold_all_alias_vars f = As.fold f RefSets.alias_vars
  let alias_var ?(mk = var) r r' = match r $~ r' with
    | A.Const true -> tt
    | A.Const false -> ff
    | A.Value v when ?~? v -> mk (AliasRel v)
    | _ -> ff
  let ( $~. ) r r' = alias_var r r'
end

module AliasRefSetsWalk (I: RELATIONS_OVERAPPROX)
  : ALIAS_REL_INSTANCIATIONS with type refdom := I.refdom =
struct
  open I.RefRels
  open AliasRelVar.Ops
  open Base
  module As = AliasRelVars

  let alias_vars refs : As.t =
    As.empty |> VarMapProd.fold_unique_pairs begin fun r rt s st ->
      if may_alias rt st then As.add (r $~ s) else ign
    end refs

  let aliasing_vars (refdom: I.refdom) refs : Vars.t =
    Vars.empty |> VarMap.fold begin fun r rt ->
      if may_alias refdom rt then Vars.add r else ign
    end refs
end

module AliasRelInvariants =
  functor (Env: SCFG.ENV with type Var.t = Var.t) ->
    functor (RH: COMMON_REL_HELPERS with module Env := Env) ->
      functor (ARH: ALIAS_REL_CONSTR with module Env := Env) ->
        functor (ObjTyps: OBJ_TYPS with type lexp := Env.lexp
                                   and type texp := Env.bexp) ->
          functor (H: Env.HEURISTICS) ->
struct
  open RH
  open ARH
  open Env.Bexp
  open Env.Lexp
  open ObjTyps
  open H

  let alias_rel_is_eqrel =
    fold_all_ref_triples begin fun r s t ->
      (&&.) (((r $~. s) &&. (s $~. t)) =>. (r $~. t))
    end

  let sound_taints_wrt_alias_rel =
    fold_all_ref_pairs begin fun r s ->
      (&&.) ((r $~. s) =>. (?^r ==. ?^s))
    end

  let sound_levels_wrt_alias_rel =
    fold_all_ref_pairs begin fun r s ->
      (&&.) ((r $~. s) =>. (~^r ==.~ ~^s))
    end
end

(* --- *)

(** {3 Field-aliasing} *)

module type FIELD_ALIAS_REL_CONSTR = sig
  (** Helpers to easily rule out impossible field-aliasing cases in ref/obj
      model: *)
  module Env: SCFG.ENV with type Var.t = Var.t
  open Base
  module FA = FieldAliasRelVar
  val ( ?.~>? ): FA.t -> bool
  val ( $.~>. ): var -> var -> Env.bexp
  val ( @@.~>? ): FA.t -> (FA.t -> 'a -> 'b -> 'b) -> 'a -> 'b -> 'b
  val fold_all_field_alias_vars: (FA.t -> 'a -> 'a) -> 'a -> 'a
  val field_alias_var: ?mk:(Env.Bexp.var -> Env.Bexp.t) -> var -> var -> Env.bexp
  val ( $.~>? ): var -> var -> Env.Var.t option
end with module FA := FieldAliasRelVar

module type FIELD_ALIAS_REL_INSTANCIATIONS = sig
  open Base
  type refdom
  module FAs = FieldAliasRelVars
  val field_alias_vars: refdom VarMap.t -> FAs.t
  val field_aliasing_vars_to_from: refdom -> refdom VarMap.t -> Vars.t * Vars.t
end

module DummyFieldAliasHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: FIELD_ALIAS_REL)
  :
  (FIELD_ALIAS_REL_CONSTR with module Env := Env)
  =
struct
  module FA = FieldAliasRelVar
  open Env.Bexp
  open FA.Ops

  let ( ?.~>? ) v = FieldAliasRelVars.mem v RefSets.field_alias_vars
  let ( @@.~>? ) _ _ _ = ign
  let ( $.~>? ) r r' = None
  let fold_all_field_alias_vars f = ign
  let field_alias_var ?(mk = var) r r' = match r $.~> r' with
    | v when ?.~>? v -> tt
    | _ -> ff
  let ( $.~>. ) r r' = field_alias_var r r'
end

module ActualFieldAliasHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: FIELD_ALIAS_REL)
  :
  (FIELD_ALIAS_REL_CONSTR with module Env := Env)
  =
struct
  include DummyFieldAliasHelpers (Env) (RefSets)
  module FAs = FieldAliasRelVars
  open Env.Bexp
  open FieldAliasRelVar.Ops

  let ( @@.~>? ) = function
    | v when ?.~>? v -> fun f x -> f v x
    | _ -> fun _ _ -> ign
  let ( $.~>? ) r r' = match r $.~> r' with
    | v when ?.~>? v -> Some (FieldAliasRel v)
    | _ -> None
  let fold_all_field_alias_vars f = FAs.fold f RefSets.field_alias_vars
  let field_alias_var ?(mk = var) r r' = match r $.~> r' with
    | v when ?.~>? v -> mk (FieldAliasRel v)
    | _ -> ff
  let ( $.~>. ) r r' = field_alias_var r r'
end

module FieldAliasRelInvariants =
  functor (Env: SCFG.ENV with type Var.t = Var.t) ->
    functor (RH: COMMON_REL_HELPERS with module Env := Env) ->
      functor (ARH: ALIAS_REL_CONSTR with module Env := Env) ->
        functor (FARH: FIELD_ALIAS_REL_CONSTR with module Env := Env) ->
          functor (ObjTyps: OBJ_TYPS with type lexp := Env.lexp
                                     and type texp := Env.bexp) ->
            functor (H: Env.HEURISTICS) ->
struct
  open RH
  open ARH
  open FARH
  open Env.Bexp
  open Env.Lexp
  open ObjTyps
  open H

  let field_alias_rel_is_transitive =
    fold_all_ref_triples begin fun r s t ->
      (&&.) (((r $.~>. s) &&. (s $.~>. t)) =>. (r $.~>. t))
    end

  let field_alias_rel_transitivity_carries_over_alias_rel =
    fold_all_ref_triples begin fun r s t i -> i
      &&. (((r $.~>. s) &&. (s $~. t)) =>. (r $.~>. t))
      &&. (((s $.~>. t) &&. (r $~. s)) =>. (r $.~>. t))
    end

  (* --- *)

  let sound_taints_wrt_field_alias_rel =
    fold_all_ref_pairs begin fun r s ->
      (&&.) ((r $.~>. s) =>. (?^s =>. ?^r))
    end

  (* --- *)

  let sound_levels_wrt_field_alias_rel =
    fold_all_ref_pairs begin fun r s ->
      (&&.) ((r $.~>. s) =>. (~^r >=.~ ~^s))
    end

end

module FieldAliasRefSetsWalk (I: RELATIONS_OVERAPPROX)
  : FIELD_ALIAS_REL_INSTANCIATIONS with type refdom := I.refdom =
struct
  open I.RefRels
  open FieldAliasRelVar.Ops
  open Base
  module FAs = FieldAliasRelVars

  let field_alias_vars refs : FAs.t =
    FAs.empty |> VarMapProd.fold_unique_pairs begin fun r rt s st fa -> fa
      |> (if may_field_alias rt st then FAs.add (r $.~> s) else ign)
      |> (if may_field_alias st rt then FAs.add (s $.~> r) else ign)
    end refs |> VarMap.fold begin fun r rd fa -> fa
      |> (if may_field_alias rd rd then FAs.add (r $.~> r) else ign)
    end refs

  let field_aliasing_vars_to_from (refdom: I.refdom) refs : Vars.t * Vars.t =
    (Vars.empty, Vars.empty) |> VarMap.fold begin fun r rt (to_r, from_r) ->
      (if may_field_alias refdom rt then Vars.add r to_r else to_r),
      (if may_field_alias rt refdom then Vars.add r from_r else from_r)
    end refs
end

(* --- *)

(** {3 Field-sharing} *)

module type FIELD_SHARE_REL_CONSTR = sig
  (** Helpers to easily rule out impossible field-sharing cases in ref/obj
      model: *)
  module Env: SCFG.ENV with type Var.t = Var.t
  open Base
  module FS = FieldShareRelVar
  val ( ?.><? ): FS.t -> bool
  val ( $.><. ): var -> var -> Env.bexp
  val ( @@.><? ): FS.t -> (FS.t -> 'a -> 'b -> 'b) -> 'a -> 'b -> 'b
  val fold_all_field_share_vars: (FS.t -> 'a -> 'a) -> 'a -> 'a
  val field_share_var: ?mk:(Env.Bexp.var -> Env.Bexp.t) -> var -> var -> Env.bexp
  val ( $.><? ): var -> var -> Env.Var.t option
end

module type FIELD_SHARE_REL_INSTANCIATIONS = sig
  open Base
  type refdom
  module FSs = FieldShareRelVars
  val field_share_vars: refdom VarMap.t -> FSs.t
  val field_sharing_vars_to_from: refdom -> refdom VarMap.t -> Vars.t * Vars.t
end

module DummyFieldShareHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: FIELD_SHARE_REL)
  :
  (FIELD_SHARE_REL_CONSTR with module Env := Env)
  =
struct
  module FS = FieldShareRelVar
  open Env.Bexp
  open FS.Ops

  let ( ?.><? ) v = FieldShareRelVars.mem v RefSets.field_share_vars
  let ( @@.><? ) _ _ _ = ign
  let ( $.><? ) r r' = None
  let fold_all_field_share_vars f = ign
  let field_share_var ?(mk = var) r r' = match r $.>< r' with
    | v when ?.><? v -> tt
    | _ -> ff
  let ( $.><. ) r r' = field_share_var r r'
end

module ActualFieldShareHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: FIELD_SHARE_REL)
  :
  (FIELD_SHARE_REL_CONSTR with module Env := Env)
  =
struct
  include DummyFieldShareHelpers (Env) (RefSets)
  module FSs = FieldShareRelVars
  open Env.Bexp
  open FS.Ops

  let ( @@.><? ) = function
    | v when ?.><? v -> fun f x -> f v x
    | _ -> fun _ _ -> ign
  let ( $.><? ) r r' = match r $.>< r' with
    | v when ?.><? v -> Some (FieldShareRel v)
    | _ -> None
  let fold_all_field_share_vars f = FSs.fold f RefSets.field_share_vars
  let field_share_var ?(mk = var) r r' = match r $.>< r' with
    | v when ?.><? v -> mk (FieldShareRel v)
    | _ -> ff
  let ( $.><. ) r r' = field_share_var r r'
end

module FieldShareRelInvariants =
  functor (Env: SCFG.ENV with type Var.t = Var.t) ->
    functor (RH: COMMON_REL_HELPERS with module Env := Env) ->
      functor (ARH: ALIAS_REL_CONSTR with module Env := Env) ->
        functor (FSRH: FIELD_SHARE_REL_CONSTR with module Env := Env) ->
          functor (ObjTyps: OBJ_TYPS with type lexp := Env.lexp
                                     and type texp := Env.bexp) ->
            functor (H: Env.HEURISTICS) ->
struct
  open RH
  open ARH
  open FSRH
  open Env.Bexp
  open Env.Lexp
  open ObjTyps
  open H

  let field_share_rel_is_transitive =
    fold_all_ref_triples begin fun r s t ->
      (&&.) (((r $.><. s) &&. (s $.><. t)) =>. (r $.><. t))
    end

  let field_share_rel_transitivity_carries_over_alias_rel =
    fold_all_ref_triples begin fun r s t i -> i
      &&. (((r $.><. s) &&. (s $~. t)) =>. (r $.><. t))
      &&. (((s $.><. t) &&. (r $~. s)) =>. (r $.><. t))
    end

  (* --- *)

  let sound_taints_wrt_field_share_rel =
    fold_all_ref_pairs begin fun r s ->
      (&&.) ((r $.><. s) =>. (?^s =>. ?^r))
    end

  (* --- *)

  let sound_levels_wrt_field_share_rel =
    fold_all_ref_pairs begin fun r s ->
      (&&.) ((r $.><. s) =>. (~^r >=.~ ~^s))
    end

end

module FieldShareRefSetsWalk (I: RELATIONS_OVERAPPROX)
  : FIELD_SHARE_REL_INSTANCIATIONS with type refdom := I.refdom =
struct
  open I.RefRels
  open FieldShareRelVar.Ops
  open Base
  module FSs = FieldShareRelVars

  let field_share_vars refs : FSs.t =
    FSs.empty |> VarMapProd.fold_unique_pairs begin fun r rt s st fa -> fa
      |> (if may_field_share_mutable rt st then FSs.add (r $.>< s) else ign)
      |> (if may_field_share_mutable st rt then FSs.add (s $.>< r) else ign)
    end refs |> VarMap.fold begin fun r rd fa -> fa
      |> (if may_field_share_mutable rd rd then FSs.add (r $.>< r) else ign)
    end refs

  let field_sharing_vars_to_from (refdom: I.refdom) refs : Vars.t * Vars.t =
    (Vars.empty, Vars.empty) |> VarMap.fold begin fun r rt (to_r, from_r) ->
      (if may_field_share_mutable refdom rt then Vars.add r to_r else to_r),
      (if may_field_share_mutable rt refdom then Vars.add r from_r else from_r)
    end refs
end

(* --- *)

(** {3 Connecting (union of aliasing and field-aliasing)} *)

module type CONNECT_REL_CONSTR = sig
  (** Helpers to easily rule out impossible connecting cases in ref/obj
      model: *)
  module Env: SCFG.ENV with type Var.t = Var.t
  open Base
  module A = AliasRelVar
  val ( ?~? ): A.t -> bool
  val ( $~. ): var -> var -> Env.bexp
  val ( @@~? ): A.v -> (A.t -> 'a -> 'b -> 'b) -> 'a -> 'b -> 'b
  val fold_all_connect_vars: (A.t -> 'a -> 'a) -> 'a -> 'a
  val connect_var: ?mk:(Env.Bexp.var -> Env.Bexp.t) -> var -> var -> Env.bexp
  val ( $~? ): var -> var -> Env.Var.t option
end

module type CONNECT_REL_INSTANCIATIONS = sig
  type refdom
  module As = AliasRelVars
  val connect_vars: refdom Base.VarMap.t -> As.t
  val connecting_vars: refdom -> refdom Base.VarMap.t -> Base.Vars.t
end

module DummyConnectHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: CONNECT_MODEL_RELS)
  :
  (CONNECT_REL_CONSTR with module Env := Env)
  =
struct
  module A = AliasRelVar
  open Env.Bexp
  open A.Ops

  let ( ?~? ) v = AliasRelVars.mem v RefSets.connect_vars
  let ( @@~? ) _ _ _ = ign
  let ( $~? ) r r' = None
  let fold_all_connect_vars f = ign
  let connect_var ?(mk = var) r r' = match r $~ r' with
    | A.Const true -> tt
    | A.Const false -> ff
    | A.Value v when ?~? v -> tt
    | _ -> ff
  let ( $~. ) r r' = connect_var r r'
end

module ActualConnectHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: CONNECT_MODEL_RELS)
  :
  (CONNECT_REL_CONSTR with module Env := Env)
  =
struct
  include DummyConnectHelpers (Env) (RefSets)
  module As = AliasRelVars
  open Env.Bexp
  open A.Ops

  let ( @@~? ) = function
    | A.Value v when ?~? v -> fun f x -> f v x
    | _ -> fun _ _ -> ign
  let ( $~? ) r r' = match r $~ r' with
    | A.Value v when ?~? v -> Some (AliasRel v)
    | _ -> None
  let fold_all_connect_vars f = As.fold f RefSets.connect_vars
  let connect_var ?(mk = var) r r' = match r $~ r' with
    | A.Const true -> tt
    | A.Const false -> ff
    | A.Value v when ?~? v -> mk (AliasRel v)
    | _ -> ff
  let ( $~. ) r r' = connect_var r r'
end

module ConnectRefSetsWalk (I: RELATIONS_OVERAPPROX)
  : CONNECT_REL_INSTANCIATIONS with type refdom := I.refdom =
struct
  open I.RefRels
  open AliasRelVar.Ops
  open Base
  module As = AliasRelVars

  let connect_vars refs =
    As.empty |> VarMapProd.fold_unique_pairs begin fun r rt s st ->
      if may_alias rt st || may_field_alias rt st || may_field_alias st rt
      then As.add (r $~ s) else ign
    end refs

  let connecting_vars (refdom: I.refdom) refs =
    Vars.empty |> VarMap.fold begin fun r rt ->
      if (may_alias refdom rt || may_field_alias refdom rt ||
            may_field_alias rt refdom)
      then Vars.add r else ign
    end refs
end

module ConnectRelInvariants =
  functor (Env: SCFG.ENV with type Var.t = Var.t) ->
    functor (RH: COMMON_REL_HELPERS with module Env := Env) ->
      functor (CRH: CONNECT_REL_CONSTR with module Env := Env) ->
        functor (ObjTyps: OBJ_TYPS with type lexp := Env.lexp
                                   and type texp := Env.bexp) ->
          functor (H: Env.HEURISTICS) ->
struct
  open RH
  open CRH
  open Env.Bexp
  open Env.Lexp
  open ObjTyps
  open H

  let connect_rel_is_eqrel =
    fold_all_ref_triples begin fun r s t ->
      (&&.) (((r $~. s) &&. (s $~. t)) =>. (r $~. t))
    end

  let sound_taints_wrt_connect_rel =
    fold_all_ref_pairs begin fun r s ->
      (&&.) ((r $~. s) =>. (?^r ==. ?^s))
    end

  let sound_levels_wrt_connect_rel =
    fold_all_ref_pairs begin fun r s ->
      (&&.) ((r $~. s) =>. (~^r ==.~ ~^s))
    end
end

(* -------------------------------------------------------------------------- *)

(** {2 Helpers for constructing symbolic heaps pertaining to each model} *)

(** {3 Share model} *)

module type SHARE_MODEL_HELPERS = sig
  module Env: SCFG.ENV with type Var.t = Var.t
  include COMMON_REL_HELPERS with module Env := Env
  include SHARE_REL_CONSTR with module Env := Env
end

module MakeShareModelInvariant =
  functor (MakeRefHelpers:
             functor (Env: SCFG.ENV with type Var.t = Var.t) ->
               functor (RefSets: SHARE_MODEL_RELS) ->
                 (SHARE_MODEL_HELPERS with module Env := Env)) ->
    functor (Env: SCFG.ENV with type Var.t = Var.t) ->
      functor (ObjTyps: OBJ_TYPS with type lexp := Env.lexp
                                 and type texp := Env.bexp) ->
        functor (RefSets: SHARE_MODEL_RELS) ->
struct
  module H = Heuristics.Exprs (Env) (Heuristics.DefaultOptions)
  module RH = MakeRefHelpers (Env) (RefSets)
  include ShareRelInvariants (Env) (RH) (RH) (ObjTyps) (H)
end

module ShareModelRefSetsWalk = ShareRefSetsWalk

module ShareHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: SHARE_MODEL_RELS)
  :
  (SHARE_MODEL_HELPERS with module Env := Env)
  =
struct
  include CommonRefHelpers (Env) (RefSets)
  include ActualShareHelpers (Env) (RefSets)
end

module ShareInvariant = MakeShareModelInvariant (ShareHelpers)

(* -------------------------------------------------------------------------- *)

(** {3 Deep-alias model} *)

module type DEEP_ALIAS_MODEL_HELPERS = sig
  module Env: SCFG.ENV with type Var.t = Var.t
  include COMMON_REL_HELPERS with module Env := Env
  include SHARE_REL_CONSTR with module Env := Env
  include ALIAS_REL_CONSTR with module Env := Env
  include FIELD_ALIAS_REL_CONSTR with module Env := Env
end

module MakeDeepAliasModelInvariant =
  functor (MakeRefHelpers:
             functor (Env: SCFG.ENV with type Var.t = Var.t) ->
               functor (RefSets: DEEP_ALIAS_MODEL_RELS) ->
                 (DEEP_ALIAS_MODEL_HELPERS with module Env := Env)) ->
    functor (Env: SCFG.ENV with type Var.t = Var.t) ->
      functor (ObjTyps: OBJ_TYPS with type lexp := Env.lexp
                                 and type texp := Env.bexp) ->
        functor (RefSets: DEEP_ALIAS_MODEL_RELS) ->
struct
  module H = Heuristics.Exprs (Env) (Heuristics.DefaultOptions)
  module RH = MakeRefHelpers (Env) (RefSets)
  include AliasRelInvariants (Env) (RH) (RH) (ObjTyps) (H)
  include FieldAliasRelInvariants (Env) (RH) (RH) (RH) (ObjTyps) (H)
  include ShareRelInvariants (Env) (RH) (RH) (ObjTyps) (H)
end

module DeepAliasModelRefSetsWalk (I: RELATIONS_OVERAPPROX) = struct
  include AliasRefSetsWalk (I)
  include FieldAliasRefSetsWalk (I)
  include ShareRefSetsWalk (I)
end

(* --- *)

module DumbAliasHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: DEEP_ALIAS_MODEL_RELS)
  :
  (DEEP_ALIAS_MODEL_HELPERS with module Env := Env)
  =
struct
  include CommonRefHelpers (Env) (RefSets)
  include DummyAliasHelpers (Env) (RefSets)
  include DummyFieldAliasHelpers (Env) (RefSets)
  include DummyShareHelpers (Env) (RefSets)
end

module ShallowAliasHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: DEEP_ALIAS_MODEL_RELS)
  :
  (DEEP_ALIAS_MODEL_HELPERS with module Env := Env)
  =
struct
  include DumbAliasHelpers (Env) (RefSets)
  include ActualAliasHelpers (Env) (RefSets)
end

module DeepAliasHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: DEEP_ALIAS_MODEL_RELS)
  :
  (DEEP_ALIAS_MODEL_HELPERS with module Env := Env)
  =
struct
  include ShallowAliasHelpers (Env) (RefSets)
  include ActualFieldAliasHelpers (Env) (RefSets)
end

(* --- *)

module DumbAliasInvariant = MakeDeepAliasModelInvariant (DumbAliasHelpers)
module ShallowAliasInvariant = MakeDeepAliasModelInvariant (ShallowAliasHelpers)
module DeepAliasInvariant = MakeDeepAliasModelInvariant (DeepAliasHelpers)

(* -------------------------------------------------------------------------- *)

(** {3 Field-sharing model} *)

module type FIELD_SHARE_MODEL_HELPERS = sig
  module Env: SCFG.ENV with type Var.t = Var.t
  include COMMON_REL_HELPERS with module Env := Env
  include ALIAS_REL_CONSTR with module Env := Env
  include FIELD_SHARE_REL_CONSTR with module Env := Env
end

module MakeFieldShareModelInvariant =
  functor (MakeRefHelpers:
             functor (Env: SCFG.ENV with type Var.t = Var.t) ->
               functor (RefSets: FIELD_SHARE_MODEL_RELS) ->
                 (FIELD_SHARE_MODEL_HELPERS with module Env := Env)) ->
    functor (Env: SCFG.ENV with type Var.t = Var.t) ->
      functor (ObjTyps: OBJ_TYPS with type lexp := Env.lexp
                                 and type texp := Env.bexp) ->
        functor (RefSets: FIELD_SHARE_MODEL_RELS) ->
struct
  module H = Heuristics.Exprs (Env) (Heuristics.DefaultOptions)
  module RH = MakeRefHelpers (Env) (RefSets)
  include AliasRelInvariants (Env) (RH) (RH) (ObjTyps) (H)
  include FieldShareRelInvariants (Env) (RH) (RH) (RH) (ObjTyps) (H)
end

module FieldShareModelRefSetsWalk (I: RELATIONS_OVERAPPROX) = struct
  include AliasRefSetsWalk (I)
  include FieldShareRefSetsWalk (I)
end

(* --- *)

module FieldShareHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: FIELD_SHARE_MODEL_RELS)
  :
  (FIELD_SHARE_MODEL_HELPERS with module Env := Env)
  =
struct
  include CommonRefHelpers (Env) (RefSets)
  include ActualAliasHelpers (Env) (RefSets)
  include ActualFieldShareHelpers (Env) (RefSets)
end

(* --- *)

module FieldShareInvariant = MakeFieldShareModelInvariant (FieldShareHelpers)

(* -------------------------------------------------------------------------- *)

(** {3 Connect model} *)

module type CONNECT_MODEL_HELPERS = sig
  module Env: SCFG.ENV with type Var.t = Var.t
  include COMMON_REL_HELPERS with module Env := Env
  include SHARE_REL_CONSTR with module Env := Env
  include CONNECT_REL_CONSTR with module Env := Env
end

module MakeConnectModelInvariant =
  functor (MakeRefHelpers:
             functor (Env: SCFG.ENV with type Var.t = Var.t) ->
               functor (RefSets: CONNECT_MODEL_RELS) ->
                 (CONNECT_MODEL_HELPERS with module Env := Env)) ->
    functor (Env: SCFG.ENV with type Var.t = Var.t) ->
      functor (ObjTyps: OBJ_TYPS with type lexp := Env.lexp
                                 and type texp := Env.bexp) ->
        functor (RefSets: CONNECT_MODEL_RELS) ->
struct
  module H = Heuristics.Exprs (Env) (Heuristics.DefaultOptions)
  module RH = MakeRefHelpers (Env) (RefSets)
  include ConnectRelInvariants (Env) (RH) (RH) (ObjTyps) (H)
  include ShareRelInvariants (Env) (RH) (RH) (ObjTyps) (H)
end

module ConnectModelRefSetsWalk (I: RELATIONS_OVERAPPROX) = struct
  include ShareRefSetsWalk (I)
  include ConnectRefSetsWalk (I)
end

module ConnectHelpers
  (Env: SCFG.ENV with type Var.t = Var.t)
  (RefSets: CONNECT_MODEL_RELS)
  :
  (CONNECT_MODEL_HELPERS with module Env := Env)
  =
struct
  include CommonRefHelpers (Env) (RefSets)
  include DummyShareHelpers (Env) (RefSets)
  include ActualConnectHelpers (Env) (RefSets)
end

module ConnectInvariant = MakeConnectModelInvariant (ConnectHelpers)

(* -------------------------------------------------------------------------- *)
