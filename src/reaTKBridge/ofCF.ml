open Utils
open Format
open CtrlNbac
open ScgsCore

(* --- *)

module type SCANNER = sig
  type t
  val scan: string -> t
  val scanx: string -> t
end

(* --- *)

module type PARAMS = sig
  val use_external_symbs: bool
end

(* --- *)

module type T = sig
  module Env: SCFG.ENV
  open CtrlNbac
  open AST
  val var: Symb.t -> Env.Var.t

  val pred: 'f checked_pred -> Env.bexp
  val func: 'f checked_func -> Env.exp Env.VarMap.t

  module Guards: Monitor.GUARDS
  val guards
    : ?hashcons: bool
    -> Env.Var.t Policy.Map.t
    -> 'f checked_func
    -> Guards.t

  module MonNode: Monitor.NODE
  val node
    : ?hashcons: bool
    -> enabled_locs: SSet.t
    -> Env.Var.t Policy.Map.t
    -> Env.VarSet.t
    -> 'f checked_node
    -> MonNode.t

  module MonAuto: Monitor.AUTOMATON
  type 'f guarded_updates =
    | Updates of 'f checked_node
    | Guarded of 'f checked_pred * 'f guarded_updates * 'f guarded_updates
  val auto
    : ?hashcons: bool
    -> enabled_locs: SSet.t
    -> Env.Var.t Policy.Map.t
    -> Env.VarSet.t
    -> 'f checked_node
    -> 'f guarded_updates SMap.t
    -> MonAuto.t
end

(* --- *)

module Make
  (P: PARAMS)
  (Env: SCFG.ENV)
  (VS: SCANNER with type t = Env.Var.t)
  (Heuristics: Env.HEURISTICS)
  :
  (T with module Env = Env)
  =
struct
  open AST
  open Env
  open Lexp
  open Eexp
  open Bexp
  open Heuristics

  let sc_var = if P.use_external_symbs then VS.scanx else VS.scan

  let nop op mk r e f = function
    | [] -> mk (r e) (r f)
    | l -> mk (r e) (r (op f l))

  let var s = sc_var (Symb.to_string s)

  let lnot e' = function
    | LBot -> LTop
    | LTop -> LBot
    | LVar v -> Lexp.ite (LVar v ==.~ LTop, LBot, LTop)   (* XXX: only if {⊥,⊤} *)
    | _ -> failwith (asprintf "Unsupported operation on numerical expression: %a"
                      print_nexp e')

  (* --- *)

  type td =
      {
        locals: exp SMap.t;
      }

  let new_td () =
    { locals = SMap.empty; }

  (* --- *)

  exception EEmpty
  let rec bexp td ?flag : _ AST.bexp -> bexp =
    let be = bexp td and ne = nexp td and ee = eexp td in
    function
      | `Ref r -> bref td r
      | `Bool true -> tt
      | `Bool false -> ff
      | `Buop (`Neg, e) -> !.(be e)
      | `Bbop (`Imp, e, f) -> be e =>. be f
      | `Bnop (`Conj, e, f, l) -> nop mk_conj' ( &&. ) be e f l
      | `Bnop (`Disj, e, f, l) -> nop mk_disj' ( ||. ) be e f l
      | `Bcmp (`Eq, e, f) -> be e ==. be f
      | `Bcmp (`Ne, e, f) -> be e <>. be f
      | `Ncmp (`Eq, e, f) -> ne e ==.~ ne f
      | `Ncmp (`Ne, e, f) -> ne e <>.~ ne f
      | `Ncmp (`Le, e, f) -> ne e <=.~ ne f
      | `Ncmp (`Ge, e, f) -> ne e >=.~ ne f
      | `Ncmp (`Lt, e, f) -> !.(ne e >=.~ ne f)
      | `Ncmp (`Gt, e, f) -> !.(ne e <=.~ ne f)
      | `Ecmp (`Eq, e, f) -> ee e ==.@ ee f
      | `Ein (x, e, l) -> let eqx f = let x = ee x in x ==.@ ee f in
                         List.fold_left (fun acc f -> acc ||. eqx f) (eqx e) l
      | `Ite (c, t, e) -> bite (be c, be t, be e)
      | #flag as e -> apply' be e
      | e -> failwith (asprintf "Unsupported Controllable-Nbac Boolean \
                                expression: %a" print_bexp e)
  and nexp td ?flag : _ AST.nexp -> lexp =
    let be = bexp td and ne = nexp td in
    function
      | `Ref r -> nref td r
      | `Int 0 | `Ncst (false, 1, `Int 0) -> LBot
      | `Int 1 | `Ncst (false, 1, `Int 1) -> LTop
      | `Luop (`LNot, e) as e' -> lnot e' (ne e)
      | `Ite (c, t, e) -> lite (be c, ne t, ne e)
      | #flag as e -> apply' ne e
      | e -> failwith (asprintf "Unsupported Controllable-Nbac numerical \
                                expression: %a" print_nexp e)
  and eexp td ?flag : _ AST.eexp -> eexp =
    let be = bexp td and ee = eexp td in
    function
      | `Ref r -> eref td r
      | `Enum l -> enum (Symb.to_string (label_symb l))
      | `Ite (c, t, e) -> Eexp.ite (be c, ee t, ee e)
      | #flag as e -> apply' ee e
  and bref { locals } ?flag r =
    try match SMap.find r locals with
      | Bexp e -> e
      | _ -> failwith (asprintf "Incorrect use of %a: Boolean expected\
                               " Symb.print r)
    with Not_found -> BVar (var r)
  and nref { locals } ?flag r =
    try match SMap.find r locals with
      | Lexp e -> e
      | _ -> failwith (asprintf "Incorrect use of %a: Security level expected\
                               " Symb.print r)
    with Not_found -> LVar (var r)
  and eref { locals } ?flag r =
    try match SMap.find r locals with
      | Eexp e -> e
      | _ -> failwith (asprintf "Incorrect use of %a: Location expected\
                               " Symb.print r)
    with
      | Not_found -> EVar (var r)
  and pexp td ?flag : _ AST.exp -> exp = function
    | `Bexp b -> Bexp (bexp td ?flag b)
    | `Nexp n -> Lexp (nexp td ?flag n)
    | `Eexp e -> Eexp (eexp td ?flag e)
    | #flag as e -> apply' (pexp td) e
    | e -> failwith (asprintf "Unsupported Controllable-Nbac expression: %a"
                       print_exp e)

  (* --- *)

  let tlocs locs =
    SSet.fold (fun l -> Strings.add (Symb.to_string l))
      locs Strings.empty

  let tdefs td local_defs =
    List.fold_left (fun td (l, e) -> { locals = SMap.add l (pexp td e) td.locals })
      td local_defs

  let touts td output_defs =
    List.fold_left (fun acc (v, e) -> match var v, pexp td e with
      | v, (Bexp (BVar v') | Lexp (LVar v') | Eexp (EVar v'))
            when Var.equal v v' -> acc
      | v, e -> VarMap.add v e acc)
      VarMap.empty output_defs

  (* --- *)

  let pred (pred: 'f checked_pred) : bexp =
    let ldefs = sorted_pred_definitions pred in
    bexp (tdefs (new_td ()) ldefs) (pred_desc pred).pn_value

  (* --- *)

  let func (func: 'f checked_func) : exp VarMap.t =
    let fi = gather_func_info func in
    let ldefs, odefs = sorted_func_definitions func |>
        List.partition (fun (v, _) -> SMap.mem v fi.fni_local_vars) in
    touts (tdefs (new_td ()) ldefs) odefs

  (* --- *)

  let init_bindings (type f) local_bindings locals td typdefs decls init =
    let module Eval = CtrlNbac.Eval.Make (struct type flg = f end) in
    let env = Eval.bind (fun v -> let t, _, _ = SMap.find v decls in t)
      Eval.Env.empty local_bindings in
    let labeltyp = label_typ (`Tds typdefs) in
    let init = Eval.const_assignments typdefs labeltyp env init in
    let bexp ?flag b = Bexp (bexp td ?flag (`Bool b)) in
    let eexp ?flag e = Eexp (eexp td ?flag (`Enum e)) in
    let lexp ?flag (s, w, i) =
      Lexp (nexp td ?flag (`Ncst (s, w, `Int (Mpz.get_int i)))) in
    let enumtyp tn = SCFG.Enum (Symb.to_string tn) in
    let assign v e (s, m) = s, VarMap.add v (Monitor.Expr e) m in
    let declare v t (s, m) = s, VarMap.add v (Monitor.Typ t) m in
    let param v t (s, m) = VarMap.add v t s, m in
    let input t v = match t with
      | `Bool when VarSet.mem v locals -> declare v SCFG.Bool
      | `Bint _ when VarSet.mem v locals -> declare v SCFG.Level
      | `Enum tn when VarSet.mem v locals -> declare v (enumtyp tn)
      | `Bool -> param v SCFG.Bool
      | `Bint _ -> param v SCFG.Level
      | `Enum tn -> param v (enumtyp tn)
      | t -> failwith (asprintf "Unsupported Controllable-Nbac type %a for \
                                `%a'" print_typ t Var.print v)
    in
    SMap.fold begin fun s spec (m, inputs) -> match spec with
      | t, `State _, flag ->
          let v = var s in
          begin match Eval.Env.find_typed s init with
            | None -> input t v m, (v :: inputs)
            | Some (Eval.Val.Bool b) -> assign v (bexp ?flag b) m, inputs
            | Some (Eval.Val.Enum e) -> assign v (eexp ?flag e) m, inputs
            | Some (Eval.Val.Bint l) -> assign v (lexp ?flag l) m, inputs
            | _ -> failwith (asprintf "unsupported: %a" Var.print v)
          end
      | _ -> (m, inputs)
    end decls ((VarMap.empty, VarMap.empty), [])

  (* --- *)

  let policies: Var.t Policy.Map.t -> Policy.t VarMap.t = fun pols ->
    (* Just reverse the policy map: *)
    Policy.Map.fold (fun p v -> VarMap.add v p) pols VarMap.empty

  let policy_vars: Policy.t VarMap.t -> VarSet.t = fun pols ->
    VarMap.fold (fun v _ -> VarSet.add v) pols VarSet.empty

  let filter_out_policy_vars policies =
    let polvars = policy_vars policies in
    SMap.filter (fun v _ -> not (VarSet.mem (var v) polvars))

  (* --- *)

  module Env = Env
  module Guards = Monitor.Guards (Env)

  let preds (guards: 'f checked_func) =
    func guards |> VarMap.map (function
      | Bexp b -> b
      | _ -> assert false)

  let guards ?hashcons pols (guards: 'f checked_func) : Guards.t =
    Guards.make ?hashcons (policies pols) (preds guards)

  (* --- *)

  module MonNode = Monitor.Node (Env)

  let node ?hashcons ~enabled_locs pols locals
      (node: 'f checked_node) : MonNode.t =
    let policies = policies pols in
    let td = new_td () in
    let { cn_typs; cn_decls; cn_init } = node_desc node in
    let ni = gather_node_info node in
    let local_bindings = sorted_node_definitions node in
    let td = tdefs td local_bindings in
    let cn_decls = filter_out_policy_vars policies cn_decls in
    let (params, init), _ = init_bindings local_bindings locals td
      cn_typs cn_decls cn_init in
    let trans = touts td (SMap.bindings ni.cni_trans_specs) in
    let locs = tlocs enabled_locs in
    MonNode.make ?hashcons locs params policies init trans

  (* --- *)

  (* let node' ~enabled_locs (node: 'f checked_node) : MonNode.t = *)
  (*   let td = new_td () in *)
  (*   let { cn_typs; cn_decls; cn_init } = node_desc node in *)
  (*   let ni = gather_node_info node in *)
  (*   let local_bindings = sorted_node_definitions node in *)
  (*   let td' = tdefs td local_bindings in *)
  (*   let init, _ = init_bindings local_bindings td' cn_typs cn_decls cn_init in *)
  (*   let locals = SMap.fold (fun v -> VarMap.add (var v)) td'.locals VarMap.empty in *)
  (*   let trans = touts td (SMap.bindings ni.cni_trans_specs) in *)
  (*   let locs = tlocs enabled_locs in *)
  (*   MonNode.make' locs init locals trans *)

  (* --- *)

  module MonAuto = Monitor.Automaton (Env)

  type 'f guarded_updates =
    | Updates of 'f checked_node
    | Guarded of 'f checked_pred * 'f guarded_updates * 'f guarded_updates

  let rec updates td = function
    | Updates node ->
        let ni = gather_node_info node in
        let td = tdefs td (sorted_node_definitions node) in
        MonAuto.U (touts td (SMap.bindings ni.cni_trans_specs))
    | Guarded (g, u, v) ->
        MonAuto.C (pred g, updates td u, updates td v)

  let auto ?hashcons ~enabled_locs pols locals
      (node: 'f checked_node)
      (trans_funcs: 'f guarded_updates SMap.t) : MonAuto.t =
    let policies = policies pols in
    let td = new_td () in
    let { cn_typs; cn_decls; cn_init } = node_desc node in
    let local_bindings = sorted_node_definitions node in
    let td = tdefs td local_bindings in
    let cn_decls = filter_out_policy_vars policies cn_decls in
    let (params, init), _ = init_bindings local_bindings locals td
      cn_typs cn_decls cn_init in
    let steps = SMap.fold
      (fun loc n -> StrMap.add (Symb.to_string loc) (updates td n))
      trans_funcs StrMap.empty in
    MonAuto.make ?hashcons params policies init steps

end
