open Format
open CtrlNbac
open ScgsCore
open SCFG

(* --- *)

module type PRINTER = sig
  type t
  val print: t Rutils.PPrt.pp
  val printx: t Rutils.PPrt.pp
end

(* --- *)

module type PARAMS = sig
  val use_external_symbs: bool
end

(* --- *)

module Make
  (P: PARAMS)
  (E: SCFG.ENV with type Var.t = ScgsCore.Var.t)
  (VP: PRINTER with type t = E.Var.t)
  =
struct

  open E
  open AST

  let tt = mk_bcst' true and ff = mk_bcst' false

  let pp_var = if P.use_external_symbs then VP.printx else VP.print
  let tsv v = asprintf "%a" pp_var v |> mk_symb

  (* --- *)

  (* TODO: locals for intermediate values... *)
  let lub a b = List.fold_left mk_lor' (mk_lor' a b)
  let glb a b = List.fold_left mk_land' (mk_land' a b)

  (* --- *)

  let rec tb: Bexp.t -> _ bexp = function
    | Bexp.BCnj [] -> mk_bcst' true
    | Bexp.BDsj [] -> mk_bcst' false
    | Bexp.BVar v -> mk_bref' (tsv v)
    | Bexp.BNot e -> mk_neg' (tb e)
    | Bexp.BCnj (e :: tl) -> mk_conj' (tb e) (List.map tb tl)
    | Bexp.BDsj (e :: tl) -> mk_disj' (tb e) (List.map tb tl)
    | Bexp.BIte (c, t, e) -> mk_bcond' (tb c) (tb t) (tb e)
    | Bexp.BEq (e, f) -> mk_beq' (tb e) (tb f)
    | Bexp.BNe (e, f) -> mk_bne' (tb e) (tb f)
    | Bexp.BExt (Cond.LExt e) -> tlc e
    | Bexp.BExt (Cond.EExt e) -> tec e
  and tl: Lexp.t -> _ nexp = function
    | Lexp.LTop -> mk_nbicst' false 1 1
    | Lexp.LBot -> mk_nbicst' false 1 0
    | Lexp.LVar v -> mk_nref' (tsv v)
    | Lexp.LLub (e, f, l) -> lub (tl e) (tl f) (List.map tl l)
    | Lexp.LGlb (e, f, l) -> glb (tl e) (tl f) (List.map tl l)
    | Lexp.LIte (c, t, e) -> mk_ncond' (tb c) (tl t) (tl e)
  and tlc: _ Lexp.Cond.t -> _ bexp = function
    | Lexp.Cond.LLe (e, f) -> mk_le' (tl e) (tl f)
    | Lexp.Cond.LEq (e, f) -> mk_neq' (tl e) (tl f)
  and te: Eexp.t -> _ eexp = function
    | Eexp.ECst l -> mk_ecst' (mk_symb l |> mk_label)
    | Eexp.EVar v -> mk_eref' (tsv v)
    | Eexp.EIte (c, t, e) -> mk_econd' (tb c) (te t) (te e)
  and tec: _ Eexp.Cond.t -> _ bexp = function
    | Eexp.Cond.EEq (e, f) -> mk_eeq' (te e) (te f)

  let tu u =
    let uvar v e = SMap.add (tsv v) (`State (e, None)) in
    SMap.empty
    |> Updates.foldb (fun v b -> uvar v (`Bexp (tb b))) u
    |> Updates.foldl (fun v l -> uvar v (`Nexp (tl l))) u
    |> Updates.folde (fun v e -> uvar v (`Eexp (te e))) u

  (* --- *)

  let level_typ = `Bint (false, 1)

  let rec pos = function
    | Z -> one
    | S r -> succ (pos r)

  (* --- *)

  module SCFG (S: SCFG.T with module Env = E) = struct

    open S

    module Flag = struct
      type t =
        | Location of loc
        | Transition of loc * loc
      let print fmt = function
        | Location l ->
            fprintf fmt "on@ location@ %a" Location.print l
        | Transition (l, l') ->
            fprintf fmt "on@ transition@ between@ %a@ and@ %a\
                        " Location.print l Location.print l'
    end

    (* --- *)

    module Reporting = struct

      let pp = fprintf

      (** Printer for optionally located messages. *)
      type pp_located = ?flag:Flag.t -> (formatter -> unit) -> formatter -> unit

      let message p : pp_located = fun ?flag msg fmt -> match flag with
        | Some flag -> pp fmt "%a:@\n%s@[%t@." Flag.print flag p msg
        | None -> pp fmt "%s@[%t@." p msg

      let error = message "*** Error: "
      let warning = message " ++ Warning: "
      let info = message "  - Info: "

      let report_msgs fmt =
        let pp_ls fmt pl = pp fmt "@[<v>%a@]" (fun fmt -> List.iter (pl fmt)) in
        List.iter begin function
          | `TError ((flag, m) :: t) | `MError ((flag, m) :: t) ->
              error ?flag m fmt;
              pp_ls fmt (fun fmt (flag, m) ->
                pp fmt "@[%t@]" (message "" ?flag m)) t
          | `TError [] | `MError [] -> ()
        end

    end

    (* --- *)

    open Flag

    let pp_loc = Location.(if P.use_external_symbs then print_unique else print)

    let loc_symb  l = asprintf "%a" pp_loc l |> mk_symb
    let loc_label l = loc_symb l |> mk_label

    let decl_enums scfg =
      fold_enums begin fun e labels ->
        Utils.Strings.fold (fun m l -> mk_label (mk_symb m) :: l)
          labels [] |> mk_etyp |> declare_typ (mk_symb e)
      end scfg

    (* --- *)

    let decls scfg =
      let cg_typs = decl_enums scfg empty_typdefs in
      let cg_decls, contrs = fold_decls begin fun v (t, k) ->
        let t = match t with
          | Bool -> `Bool
          | Enum t -> `Enum (mk_symb t)
          | Level -> level_typ
        in
        fun (decls, contrs) ->
          let v = tsv v in
          let decl, contrs = match k with
            | State -> (t, `State', None), contrs
            | Input -> (t, `Input one, None), contrs
            | Controllable (aim, g, r) ->
                let aim = match aim with Min -> `Ascend | Max -> `Descend in
                ((t, `Contr (pos g, pos r, aim), None), SSet.add v contrs)
          in
          SMap.add v decl decls, contrs
      end scfg (SMap.empty, SSet.empty) in
      cg_typs, cg_decls, contrs

    (* --- *)

    (* Build up global assertion to restrict locations where some Boolean
       variable may hold *)
    let acc_restrictions loc_var =
      let loc_in = let mk = mk_ein' (mk_eref' loc_var) in function
        | [] -> tt | x :: tl -> mk x tl
      and loc_label l = mk_ecst' (loc_label l) in
      S.fold_restrictions begin fun v locs ->
        let locs = Locations.fold (fun l -> List.cons (loc_label l)) locs [] in
        mk_and' (mk_imp' (tb v) (loc_in locs))
      end

    (* --- *)

    let initial_constraint scfg =
      VarMap.fold (fun v -> function
        | Bexp e -> mk_and' (mk_beq' (mk_bref' (tsv v)) (tb e))
        | Lexp e -> mk_and' (mk_neq' (mk_nref' (tsv v)) (tl e))
        | Eexp e -> mk_and' (mk_eeq' (mk_eref' (tsv v)) (te e)))
        (initial_bindings scfg) tt

    (* --- *)

    let cfg scfg :
        [ `OK of Flag.t checked_cfg | `KO of Flag.t cfg ] * Flag.t loc_msg list
        =

      let cg_typs, cg_decls, contrs = decls scfg in
      let loc_var = tsv (locvar scfg) in
      let cg_graph = fold_locations begin fun l cfg ->
        let loc = loc_label l and flag = Location l in
        let init = if is_init scfg l then Some tt else None
        and assertion = match assert_of_loc scfg l with
          | Some a -> Some (tb a) | _ -> None
        and final = match invar_of_loc scfg l with
          | Some i -> Some (mk_neg' (tb i)) | _ -> None
        and reachable = if reachable scfg l then Some tt else  None in
        let arcs = fold_trans_from begin fun (_, (grd, updates), l') arcs ->
          let dest = loc_label l' and flag = Transition (l, l') in
          CFG.append_arc arcs ~flag ~guard:(tb grd) (tu updates) ~dest
        end scfg l CFG.no_arc in
        CFG.add_location cfg ~flag loc ?init ?final ?assertion ?reachable arcs
      end scfg (CFG.empty ~loc_var:(loc_var, None) ()) in
      let cg_assertion = tb (assertion scfg) |> acc_restrictions loc_var scfg in
      let cg_invar = tb (invar scfg) in
      let cg_init = initial_constraint scfg in
      let cg =
        `Desc { cg_typs; cg_decls; cg_graph; cg_init; cg_assertion;
                cg_invariant = Some cg_invar;
                cg_reachable = None; cg_attractive = None; }
      in
      match check_cfg cg with
        | Some ccg, msgs -> `OK ccg, msgs
        | None, msgs -> `KO cg, msgs

  end

end
