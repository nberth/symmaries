(** Main interface to ReaTKBridge *)

open Format
open Rutils
open Rutils.IO
open Utils
open ScgsCore
open Base
module H = Heuristics
module PP = PreProc
open Trans

let level = Log.Debug3
let logger = Log.mk ~level "RTKB";;

(* --- *)

module SymbParams = struct let use_external_symbs = false end
module AsCG = AsCG.Make (SymbParams) (SCFGEnv) (Var)
module MakeOfCF = OfCF.Make (SymbParams) (SCFGEnv) (V.Scanner)
module CN = CtrlNbac.AST

open Realib

let enable_reorderings = ref false

(* -------------------------------------------------------------------------- *)

module type CTRLNBAC_VIEW = sig
  module SCFG: Trans.S with module Env = SCFGEnv
  val sign: OOLang.Sign.decl option
  val meth_stats: int OOLang.Method.ImplStats.cardinals
  val symb_heap_stats: int Stats.SHStats.t
  val scfg: SCFG.t
  val scfg_gen_stats: (Stats.time, int) Stats.SCFGGen.t
  val transient_ffmode_heap_vars: bool
  val policies: SecSum.policies
  module SCFGAsCG: sig
    module Flag: sig type t val print: t PPrt.pp end
    module Reporting: sig
      val report_msgs: formatter -> [< Flag.t CN.loc_msg ] list -> unit
    end
    val loc_label: SCFG.loc -> CN.label
    val loc_symb: SCFG.loc -> CN.symb
  end
  module OfCF: OfCF.T with module Env = SCFGEnv
end

(* --- *)

module CNAccess (CNV: CTRLNBAC_VIEW) = struct
  open CNV
  let loc_labels s =
    SCFG.Locations.fold (fun l -> CN.SSet.add (SCFGAsCG.loc_symb l))
      s CN.SSet.empty

  let loc_labels': CN.SSet.t -> SCFGAsCG.Flag.t CN.eexp list =
    let lbl l = CN.mk_ecst' (CN.mk_label l) in
    fun locs -> CN.SSet.fold (fun l -> List.cons (lbl l)) locs []
end

(* --- *)

module type REALIB_VIEW = sig
  module E: Env.T
  val env: Env.t
  val cs: CtrlSpec.t
  val cf: Program.cfprog_t
end

module type REALIB_MODEL = sig
  include REALIB_VIEW
  include CTRLNBAC_VIEW
  val local_defs: SCFGAsCG.Flag.t Cn2rl.local_defs
  val model_constr_stats: (Stats.time, int) Stats.Constr.t
end

(* --- *)

module ModelAccess (M: REALIB_MODEL) = struct
  module EU = BddapronUtil.EnvUtils (M.E.BEnv)
  open M
  let texp = Cn2rl.translate_exp M.env local_defs
  let tb' b = texp (`Bexp b) |> Bddapron.Expr0.Bool.of_expr
  let tn' l = texp (`Nexp l) |> Bddapron.Expr0.Bint.of_expr
  let te' e = texp (`Eexp e) |> Bddapron.Expr0.Benum.of_expr
  let tb b = tb' (AsCG.tb b)
  let tl l = tn' (AsCG.tl l)
  let te e = te' (AsCG.te e)
  let texp' = function
    | SCFGEnv.Bexp b -> tb b |> Bddapron.Expr0.Bool.to_expr
    | SCFGEnv.Lexp l -> tl l |> Bddapron.Expr0.Bint.to_expr
    | SCFGEnv.Eexp e -> te e |> Bddapron.Expr0.Benum.to_expr
  let tvar v = AsCG.tsv v |> Cn2rl.translate_symb
  let locvar = tvar (SCFG.locvar scfg)
  let nuvar = tvar SCFG.ua_var
  let oracle_var = tvar SCFG.oracle_var
  let nominal_mode = tb SCFG.ua_nom
  let vart v = OfCF.var (Realib.Env.Symb.to_string v |> CN.mk_symb)
  (* let checkpoint_asserts = *)
  (*   SCFGEnv.VarMap.fold (fun c e -> PMappe.add (tvar c) (tb env e)) *)
  (*     SCFG.checkpoint_asserts EU.empty_vmap *)
  (* let tv = OfCF.var *)

  (** Extracts the set of locations encoded by a predicate that has been
      projected on [locvar]. *)
  let loc_symbs =
    List.fold_left
      (fun s l -> CN.SSet.add (CN.label_symb (Rl2cn.Open.translate_label l)) s)
      CN.SSet.empty

  let loc_in env : CN.SSet.t -> Env.boolexpr_t =
    let module CNAccess = CNAccess (M) in
    let locvar' = SCFG.locvar scfg |> AsCG.tsv in
    fun locs ->
      let cond = match CNAccess.loc_labels' locs with
        | [] -> CN.mk_bcst' false
        | l :: l' -> CN.mk_ein' (CN.mk_eref' locvar') l l'
      in
      tb' cond

  open E.BEnvNS.BAll
  let branching_mode env =
    SCFG.LocMap.fold begin fun loc branching_cond ->
      (* Log.i logger "%a => %a" *)
      (*   SCFG.Location.print loc SCFG.Env.Bexp.print branching_cond; *)
(* ite *)
      (&&~)
        ((loc_in env (CN.SSet.singleton (SCFGAsCG.loc_symb loc))) =>~
          (tb branching_cond))
    end SCFG.actual_branching (* nominal_mode *)tt

  let footprint_state = List.rev_map (fun v -> tvar (Var.Prime v))
  let footprint' =
    SCFG.(Env.VarSet.fold (fun v -> List.cons (tvar v)) footprint') []

end

(* --- *)

module type REALIB_MODEL_WITH_SECSUM = sig
  include REALIB_MODEL
  val secsum_equs: Env.equs_t list
  val sink_locs: CN.SSet.t
  val enabled_locs: CN.SSet.t
  val unreachable_locs: CN.SSet.t
  val summ_synth_stats: (Stats.time, bool) Stats.Synth.t
end

module type CTRLNBAC_GUARDS_FUNC = sig
  include CTRLNBAC_VIEW
  val guards_func: SCFGAsCG.Flag.t CN.checked_func
end

module type FULL_STATS = sig
  open Stats
  val meth_stats: int OOLang.Method.ImplStats.cardinals
  val symb_heap_stats: int SHStats.t
  val scfg_gen_stats: (time, int) SCFGGen.t
  val model_constr_stats: (time, int) Constr.t
  val summ_synth_stats: (time, bool) Synth.t
end

module type CTRLNBAC_SECSUM_FUNC = sig
  include CTRLNBAC_VIEW
  include FULL_STATS
  val secsum_func: SCFGAsCG.Flag.t CN.checked_func
end

module type REALIB_MODEL_WITH_SECSUM_N_GUARDS_FUNC = sig
  include REALIB_MODEL_WITH_SECSUM
  include CTRLNBAC_GUARDS_FUNC with module SCFG := SCFG
                               and  module SCFGAsCG := SCFGAsCG
                               and  module OfCF := OfCF
end

(* --- *)

let cudderrp e = false
  || (e = "Cudd: a function returned a null BDD node;"^
      " ErrorCode = CUDD_MAX_MEM_EXCEEDED")
  || (e = "Cudd: a function returned a null ADD/BDD node;"^
      " ErrorCode = CUDD_MAX_MEM_EXCEEDED")

(* --- *)

let kept_cudds = 2
let keep_cudd =
  (* For now, explicit calls to [Cudd.Man.garbage_collect] (here through
     [Realib.Env.clear]) appear to be dangerous and may lead to various
     segfaults and violations of assertions in OCaml's major gc. *)
  (* let mem = ref [] in *)
  (* fun c -> mem := c :: !mem *)
  let module M = Map.Make (Integer) in
  let num = ref 0 and memo = ref M.empty in
  fun c ->
    let m =
      if M.cardinal !memo < kept_cudds then !memo
      else M.remove (fst (M.min_binding !memo)) !memo
    in
    memo := M.add !num c m;
    incr num

let clear_realib_env' env =
  keep_cudd (Realib.Env.cuddman env);
  Realib.Env.clear env

let clear_realib_env (res, env) =
  clear_realib_env' env;
  Ok res

(* --- *)

type skipped_stats = int Stats.SHStats.t * (Stats.time, int) Stats.SCFGGen.t
exception SkipSynthesis of (skipped_stats * PPrt.pu)

(* --- *)

(* let sspec = "sB";; *)
(* let sspec = "sB:rp={stop_dri=1,stop_ri=30,log}";; *)
(* let sspec = "sB:rp={stop_dri=0,stop_ri=100,log}";; *)
(* let sspec = "sRB:rp={stop_dri=1,stop_ri=30,log}";; *)
(* let sspec = "sRB:rp={stop_dri=100,stop_ri=100,log}";; *)
(* let sspec = "sS";; *)

let default_synth_algo = ref "sS:d={P:m}"
let default_arg_alias_invariant_upper_threshold = ref 32    (* XXX: arbitrary *)

type summary_computation =
  | Monolithic
  | SplitGuardNFootprint of { enforce_equivalence_to_monolithic: bool }
type model = (module REALIB_MODEL)
type secsum_with_model = (module REALIB_MODEL_WITH_SECSUM)

module Synthesis (M: REALIB_MODEL) = struct
  module Access = ModelAccess (M)
  module CNAccess = CNAccess (M)
  module POps = M.E.BEnvNS.POps
  open M
  open E.BEnvNS.BAll
  open Access
  open CNAccess
  open Realib.Supra
  open Realib.Program

  type params =
      {
        algo: summary_computation;
        synth_algo: string;
        simplify_footprint_invariant_upper_threshold: int;
      }

  let params
      ?(synth_algo = !default_synth_algo)
      ?(simplify_footprint_invariant_upper_threshold =
          !default_arg_alias_invariant_upper_threshold)
      algo
      =
    {
      algo;
      synth_algo;
      simplify_footprint_invariant_upper_threshold;
    }

  let extract_g0 init : equs -> (support * equs list) * bool * bool = function
    | (c, g) :: bindings when SCFG.effects_inference = ControllablePreamble ->
        let g = Bddapron.Expr0.Bool.of_expr g in
        let g = try cofactor g init with
          (* Resort to generalized restrict if init is not a cube: *)
          | Failure e when not (cudderrp e) -> g ^~ init in
        let okbase = (var c ==~ g) &&~ init in
        let ok = var c &&~ okbase in
        let truth, false_guard =
          if not (is_ff ok)
          then (ok, false)
          else (Log.w logger "Warning: @[%a@]" pp_bexpr !~(var c); okbase, true)
        in
        Log.d2 logger "Guard = %a" pp_bexpr g;
        let g, true_guard = Bddapron.Expr0.Bool.to_expr g, is_tt g in
        let bindings = List.map (fun (v, x) -> v, x ^~~ truth) bindings in
        (varsupp [c], [(c, g) :: bindings]), false_guard, true_guard
    | (c, g) :: bindings ->
        let ok = Bddapron.Expr0.Bool.of_expr g in
        let false_guard =
          if not (is_ff ok)
          then false
          else (Log.w logger "Warning: @[%a@]" pp_bexpr !~(var c); true)
        in
        Log.d2 logger "Guard = %a" pp_bexpr ok;
        let true_guard = is_tt ok in
        (varsupp [c], [(c, g) :: bindings]), false_guard, true_guard
    | [] ->
        (tt, [[]]), false, true

  let rec extra_checkpoints ((csupp, acc): support * (equs list as 'a)) : 'a -> 'a =
    (** Simplify additional checkpoints, that shoud come in groups 1 or higher *)
    let acc_simplify_checkpoint (csupp, cl) (c, g) =
      let g = Bddapron.Expr0.Bool.of_expr g in
      (* (\* Log.d logger "cfacts[%a] = %a" pp_var c pp_bexpr cfacts; *\) *)
      (* (\* Log.d logger "        %a = %a" pp_var c pp_bexpr g; *\) *)
      let g = Bddapron.Expr0.Bool.to_expr (exist' csupp g ^~ nominal_mode (* ^~ cfacts *)) in
      (Cudd.Bdd.support_union (varsupp [c]) csupp, (c,  g) :: cl)
    in
    function
      | [] -> List.rev acc
      | cl :: tl ->
          let csupp, cl =
            List.fold_left acc_simplify_checkpoint (csupp, []) cl in
          extra_checkpoints (csupp, cl :: acc) tl

  let filter_out_useless_policies secsum_equs policies =
    let psupp, pmap = Policy.Map.fold begin fun p v (psupp, pmap) ->
      tvar v :: psupp, SCFGEnv.VarMap.add v p pmap
    end policies ([], SCFGEnv.VarMap.empty) in
    let usupp = begin secsum_equs
        |> List.fold_left (List.fold_left (fun supp (_, e) ->
          Cudd.Bdd.support_diff supp (POps.support' e))) (varsupp psupp)
        |> support
    end in
    PSette.fold begin fun s -> match vart s with
      | Var _ as v -> fun acc -> Policy.Map.remove (SCFGEnv.VarMap.find v pmap) acc
      | _ -> ign
    end usupp policies

  let sort_first_cs_group = function
    (** Sort controllable variables of first group according to decreasing DD
        level *)
    | CtrlSpec.{ cs_groups = { c_vars = c0 :: b0;
                               bp_specs = p0 :: bp0 } as g0 :: tl } ->
        let b0, bp0 = begin List.combine b0 bp0
          |> List.rev_map
            Cudd.(fun (v, p) ->
                let vsupp = varsupp [v] in
                Man.level_of_var (Bdd.manager vsupp) (Bdd.topvar vsupp), (v, p))
          |> List.sort (fun (l0, _) (l1, _) -> l0 - l1)
          |> List.rev_map snd
          |> List.split
        end in
        CtrlSpec.make ({ g0 with c_vars = c0 :: b0; bc_vars = c0 :: b0;
                                 bp_specs = p0 :: bp0 } :: tl)
    | cs -> cs

  let simplify_with_footprint_invariant
      { simplify_footprint_invariant_upper_threshold }
      =
    let footprint_invar =
      if SCFG.arg_alias_invariant_support_size <
        simplify_footprint_invariant_upper_threshold
      then tb SCFG.arg_alias_invariant
      else (Log.i logger "@[Skipping@ simplification@ with@ \
                            footprint@ invariant@ (%u @<2>≤ %u)@]"
              simplify_footprint_invariant_upper_threshold
              SCFG.arg_alias_invariant_support_size;
            tt)
    in
    fun k -> k ^~~ footprint_invar

  let simplify_equs params =
    let simpl = simplify_with_footprint_invariant params in
    List.rev_map (fun (v, e) -> v, simpl e)

  let guard_vars = function
    | CtrlSpec.{ cs_groups =
                   { c_vars = c0 :: b0; bp_specs = _ :: bp0 } as g0 :: [] } ->
        CtrlSpec.make [{ g0 with c_vars = b0; bc_vars = b0; bp_specs = bp0 }],
        varsupp [c0]
    | _ -> failwith "no guard variable found, or badly formed cs"

  let list_partition n l =
    let rec aux i = function
      | [], acc -> acc
      | c :: tl, acc :: acctl when i < n -> aux (i + 1) (tl, (c :: acc) :: acctl)
      | c :: tl, acc -> aux 1 (tl, [c] :: acc)
    in
    aux 0 (l, [[]])

  let extract_footprint_partition ?(footprint_vars_to_keep = max_int) k b0 bp0 =
    let allfcvars = List.rev_map vart b0 |> SCFGEnv.VarSet.of_list in
    let k0 = list_partition footprint_vars_to_keep b0
    and kp0 = list_partition footprint_vars_to_keep bp0 in
    match List.nth_opt k0 k, List.nth_opt kp0 k with
      | None, _ | _, None -> None
      | Some k0, Some kp0 ->
          let b0vars = List.rev_map vart k0 |> SCFGEnv.VarSet.of_list in
          let fcignore = SCFGEnv.VarSet.(diff allfcvars b0vars |> elements) in
          Some (k0, kp0, footprint_state fcignore)

  let focus0 ?footprint_vars_to_keep (cs, cf) =
    (** Focus on guards and first footprint subset. *)
    let extract_fp = extract_footprint_partition ?footprint_vars_to_keep in
    let cs', fsignore = CtrlSpec.(match cs with
      | { cs_groups = { c_vars = c0 :: b0; bp_specs = p0 :: bp0 } as g0 :: tl } ->
          let g0, fsignore = match extract_fp 0 b0 bp0 with
            | Some (k0, kp0, fsignore) ->
                Log.i  logger "Focus(0) = %a" pp_vars k0;
                Log.d2 logger "Ignore(0) = %a" pp_vars fsignore;
                { g0 with c_vars = c0 :: k0; bc_vars = c0 :: k0;
                  bp_specs = p0 :: kp0; }, fsignore
            | None ->
                { g0 with c_vars = [c0]; bc_vars = [c0]; bp_specs = [p0] }, []
          in
          make (g0 :: tl), fsignore
      | _ -> failwith "no guard variable found, or badly formed cs")
    in
    cs', Program.{ cf with c_final = forall fsignore cf.c_final }

  let focusK ?footprint_vars_to_keep k (cs, cf) =
    (** Focus on K (> 0)th footprint subset, with guard variables already
        eliminated/substituted. *)
    let extract_fp = extract_footprint_partition ?footprint_vars_to_keep in
    CtrlSpec.(match cs with
      | { cs_groups = { c_vars = b0; bp_specs = bp0 } as g0 :: tl } ->
          (match extract_fp k b0 bp0 with
            | Some (k0, kp0, fsignore) ->
                Log.i  logger "Focus(%d) = %a" k pp_vars k0;
                Log.d2 logger "Ignore(%d) = %a" k pp_vars fsignore;
                let g0 = { g0 with c_vars = k0; bc_vars = k0; bp_specs = kp0; } in
                let cs = make (g0 :: tl)
                and cf = { cf with c_final = forall fsignore cf.c_final } in
                Some (cs, cf)
            | None -> None)
      | _ -> None)

  let substitute_controllables cf equs =
    let substs = List.flatten equs in
    let bsubsts b = vsubsts b substs
    and eqsubsts = List.map (fun (v, e) -> v, POps.vsubsts substs e) in
    Program.{ cf with
      c_cfg = PSHGraph.map cf.c_cfg
        (fun _ -> bsubsts)
        Arc.(fun _ -> function
          | Normal (b, e) -> Normal (bsubsts b, eqsubsts e)
          | Loop (b, e) -> Loop (bsubsts b, eqsubsts e)
          | Bool (b, e) -> Bool (bsubsts b, eqsubsts e)
          | Id -> Id
          | a -> Log.e logger "Unsupported arc type %a" (Arc.print env) a;
              failwith "Internal error")
        id;
    (* c_ass = bsubsts cf.c_ass; *)
    (* c_final = bsubsts cf.c_final; *)
    }

  let append_footprint_equs equs footprint_equs = match equs with
    | g0 :: tl -> (g0 @ footprint_equs) :: tl
    | [] -> [footprint_equs]

  let triangularized_controller { synth_algo } (cs, cf) =
    let tic = Timing.tic () in
    let synth_algo = synth_algo ^ ",initonly,ws=0,fwcs=0,wd=0" in
    match SCFG.effects_inference with
    | ControllablePreamble ->
        let synth_spec = parse_synth env synth_algo in
        let cs, k, cf = synth synth_spec env (cs, cf.c_ass, cf) in
        let tic' = Timing.tic () in
        Log.d3 logger "A = %a" pp_bexpr cf.Program.c_ass;
        Log.d3 logger "K = %a" pp_bexpr k;
        Log.d  logger "I'= %a" pp_bexpr cf.Program.c_init;
        (* In case, eliminate uninitialized initial footprint state *)
        let k' = forall footprint' (exist [locvar] (k ^~ nominal_mode)) in
        Log.d  logger "K'= %a" pp_bexpr k';
        let equs, _ = triang (sort_first_cs_group cs) env k' in
        equs, Timing.diff tic tic', Timing.toc tic'
    | CoreachOnly ->
        let _, k, cf =
          let cs', env' = match cs.CtrlSpec.cs_groups with
            | [] -> cs, env
            | g0 :: tl ->
                CtrlSpec.make ({ g0 with c_vars = []; bc_vars = [];
                                         bp_specs = [] } :: tl),
                let new_contr v = List.mem v g0.c_vars in
                Env.{ env with
                      s_vars = List.filter (fun v -> not (new_contr v)) env.s_vars;
                      bs_vars = List.filter (fun v -> not (new_contr v)) env.bs_vars;
                      i_vars = g0.c_vars @ env.i_vars;
                      bi_vars = g0.bc_vars @ env.bi_vars }
          in
          let synth_spec = parse_synth env' synth_algo in
          coreach synth_spec env' (cs', cf.c_ass, cf)
        in
        let k = !~k in
        let tic' = Timing.tic () in
        Log.d3 logger "A = %a" pp_bexpr cf.Program.c_ass;
        Log.d3 logger "K = %a" pp_bexpr k;
        Log.d  logger "I'= %a" pp_bexpr cf.Program.c_init;
        let k' =
          (* In case, eliminate uninitialized initial footprint state *)
          exist [locvar] (cofactor k cf.Program.c_init (* ^~ nominal_mode *))
        in
        let k' =
          (* substitute each primed state var with its controllable
             counterpart *)
          vsubst_vars k'
            (List.filter_map (fun v -> match v with
                 | Var.Prime (Ret v') -> Some (tvar v, tvar (Ret v'))
                 | Var.Prime (Exc v') -> Some (tvar v, tvar (Exc v'))
                 | _ -> None) (SCFGEnv.VarSet.elements SCFG.footprint'))
        in
        Log.d  logger "K'= %a" pp_bexpr k';
        let equs, guard = triang (sort_first_cs_group cs) env k' in
        let equs =                           (* assume we have a single guard *)
          List.map (List.mapi (fun i (v, e) -> match vart v with
              | Var.Guard _ -> v, Bddapron.Expr0.Bool.to_expr guard
              | _ -> v, e)) equs
        in
        equs, Timing.diff tic tic', Timing.toc tic'

  let monolithic params (cs, cf) =
    let equs, synth_time, triang_time =
      triangularized_controller params (cs, cf) in
    match equs with
      | [] -> failwith "Malformed triangularized controller"
      | g0 :: g1 ->
          let b0, unsat_guard, true_guard = extract_g0 cf.Program.c_init g0 in
          let secsum_equs = extra_checkpoints b0 g1 in
          let secsum_equs = List.rev_map (simplify_equs params) secsum_equs in
          secsum_equs, Stats.Synth.{ synth_time; triang_time; unsat_guard; true_guard; }

  let acc_footprint_equs
      ~footprint_vars_to_keep
      ~enforce_equivalence_to_monolithic
      params
      (cs, cf) ((equs0, _, _) as acc)
      =
    let cs', gsupp = guard_vars cs
    and cf = substitute_controllables cf equs0 in
    let rec process_footprint_partitions k cf
        ((equs, synth_time, triang_time) as acc)
        =
      Log.i logger "@[Focusing@ on@ footprint@ partition@ %d@]" k;
      (* Note: the substitution below is only required to obtain strictly
         equivalent effects compared to the monolithic algorithm.  Skipping it
         does not impair correctness.  *)
      let substitute_done_footprint =
        if enforce_equivalence_to_monolithic
        then substitute_controllables
        else fun cf _ -> cf
      in
      let focus_next = focusK ~footprint_vars_to_keep in
      match focus_next k (cs', substitute_done_footprint cf equs) with
        | None -> acc
        | Some (cs', cf') ->
            let footprint_equs, synth_time', triang_time' =
              triangularized_controller params (cs', cf') in
            let footprint_equs = match footprint_equs with
              | [b0] -> let truth = gsupp &&~ cf'.Program.c_init in
                       List.map (fun (v, x) -> v, x ^~~ truth) b0
              | _ -> failwith "Malformed triangularized controller for \
                              footprint subset"
            in
            (* Log.w logger "@\n%a" (Triang.pp_split_controller env) [footprint_equs]; *)
            process_footprint_partitions (k + 1) cf
              (append_footprint_equs equs footprint_equs,
               Timing.add synth_time synth_time',
               Timing.add triang_time triang_time')
    in
    process_footprint_partitions 1 cf acc

  let log2 n =
    int_of_float (Float.log10 (float_of_int n) /. Float.log10 2.)

  let split_g_n_f ~enforce_equivalence_to_monolithic params (cs, cf) =
    let footprint_vars_to_keep =
      let nf = SCFGEnv.VarMap.cardinal SCFG.footprint
      and nx = log2 scfg_gen_stats.cards.nb_locations +
        scfg_gen_stats.decls.nb_state_vars in
      let x = float_of_int nf +. float_of_int nx /. 10. in
      (* Empricially derived via exponential regression of:
         x, y = [5, 10, 14, 20], [12, 4, 2, 1]
         fit = np.exp (np.polyfit (x, np.log (y), 1)) *)
      let y = Float.floor (23.62089999 *. Float.pow 0.8479079 x) in
      let f = max 1 (int_of_float y) in
      (* Log.w logger "|X| = %d, |F| = %d ====> %d" nx nf f; *)
      f
    in
    let focus0, focusK =
      focus0 ~footprint_vars_to_keep,
      acc_footprint_equs ~footprint_vars_to_keep
        ~enforce_equivalence_to_monolithic params
    in
    let guards_equs, synth_time, triang_time =
      triangularized_controller params (focus0 (cs, cf)) in
    match guards_equs with
      | [] -> failwith "Malformed triangularized controller"
      | g0 :: g1 ->
          let b0, unsat_guard, true_guard = extract_g0 cf.Program.c_init g0 in
          assert (List.length (snd b0) = 1);
          let secsum_equs = extra_checkpoints b0 g1 in
          let secsum_equs, synth_time, triang_time =
            focusK (cs, cf) (secsum_equs, synth_time, triang_time) in
          let secsum_equs = List.rev_map (simplify_equs params) secsum_equs in
          secsum_equs,
          Stats.Synth.{ synth_time; triang_time;
                        unsat_guard; true_guard; }

  let run ({ algo } as params) =
    log_env_info ~cs env;
    log_cfprog_info env cf;
    let algo = match algo with
      | Monolithic -> monolithic
      | SplitGuardNFootprint { enforce_equivalence_to_monolithic }
        -> split_g_n_f ~enforce_equivalence_to_monolithic
    in
    Ok (module struct
      include M
      let secsum_equs, summ_synth_stats = algo params (cs, cf)
      let enabled_locs = SCFG.locations scfg
      let sink_locs = SCFG.Locations.filter (SCFG.is_sink scfg) enabled_locs
      let enabled_locs = loc_labels enabled_locs
      let sink_locs = loc_labels sink_locs
      let unreachable_locs = CN.SSet.empty
      let policies = filter_out_useless_policies secsum_equs SCFG.policies
    end: REALIB_MODEL_WITH_SECSUM)

end

let compute_secsum
    ?synth_algo
    ?simplify_footprint_invariant_upper_threshold
    ?(algo = SplitGuardNFootprint { enforce_equivalence_to_monolithic = false })
    (module M: REALIB_MODEL)
    :
    (secsum_with_model, [> `SynthFailure]) result
    =
  let module Synth = Synthesis (M) in
  let params = Synth.params
    ?simplify_footprint_invariant_upper_threshold
    ?synth_algo
    algo
  in
  try Synth.run params with
    | Realib.Supra.SynthesisFailure _ -> Error `SynthFailure
    | Failure e when cudderrp e ->
        (* A custom exception would be better *)
        Log.w logger "@[CUDD@ ran@ out@ of@ memory:@ skipping@<1>…@]";
        clear_realib_env' M.env;
        let stats = M.symb_heap_stats, M.scfg_gen_stats in
        raise @@ SkipSynthesis
          (stats, PPrt.pp1 "CUDD out of memory (%LuB)"
            (Cudd.Man.get_max_mem (Realib.Env.cuddman M.env)))

(* --- *)

let secsum_match ((module M): secsum_with_model) (ssum: SecSum.t) =
  let module Access = ModelAccess (M) in
  let open Access in
  let open SecSum in
  let ssumb = EU.empty_vmap
    |> PMappe.add (tvar (guardvar ssum)) (SCFGEnv.Bexp (guard ssum))
    |> fold_raw_bindings (fun v -> PMappe.add (tvar v)) ssum
  in
  List.for_all (List.for_all (fun (va, ea) ->
    try ea = texp' (PMappe.find va ssumb) with Not_found -> false))
    M.secsum_equs

(* --- *)

module PostProc (M: REALIB_MODEL_WITH_SECSUM) = struct
  module Access = ModelAccess (M)
  module TII = TransBool.MergeInputIndep (M.E)
  module TEI = TransBool.ElimInputs (M.E)
  module RP = BddapronReorderPolicy
  open Realib.Supra
  open Realib.Program
  open Access
  open M
  open E.BEnvNS
  open POps
  open BAll

  type t =
      {
        env: Env.t;
        cs: CtrlSpec.t;
        df: Program.dfprog_t;
      }

  let init = { env; cs; df = cf2df env cf }

  let contract { env; cs; df } =
    Log.i logger "@[Computing@ contracted@ monitor@ node@<1>…@]";
    let input_indep, (cs', df) =
      (* let df = cf2df env cf in *)
      (* let df = merge cs df env secsum_equs in *)
      let rp = RP.mk_config ~log:true ~stop_r:RP.never () in
      let x = cs, df in
      let pii = TransBool.make_mii_param ~dirty:true ~rp in
      let ii0, x, p0 =
        Log.i logger "@[%t@<1>…@]" (TII.descr pii);
        TII.df_trans pii x
      in
      let x =
        let p = TransBool.make_ei_param [oracle_var] in
        Log.i logger "@[%t@<1>…@]" (TEI.descr p);
        TEI.df_trans p x
      in
      let ii1, x, p1 =
        Log.i logger "@[%t@<1>…@]" (TII.descr pii);
        TII.df_trans pii x
      in
      BddapronUtil.permute_boolexpr' p1 ii0 ||~ ii1, x
    in
    assert (cs == cs');
    let enabled_locs, new_unreachable_locs =
      let ii_locs = BOps.project [locvar] !~input_indep in
      Log.d2 logger "@<11>indep[ℓ] = %a" pp_bexpr ii_locs;
      let keep = CN.SSet.union sink_locs (labels locvar ii_locs |> loc_symbs) in
      CN.SSet.partition (fun l -> CN.SSet.mem l keep) enabled_locs
    in
    Log.d logger "@[Contracted@ monitor@ locations:@ %a@]\
                 " CN.SSet.print new_unreachable_locs;
    let unreachable_locs = CN.SSet.union unreachable_locs new_unreachable_locs in
    Log.d logger "@[Enabled@ monitor@ locations:@ %a@]\
                  " CN.SSet.print enabled_locs;
    enabled_locs, unreachable_locs, { env; cs; df }

  let simplify_nu enabled_locs ({ env; df } as t) =
    let loc_orbit = loc_in env enabled_locs in
    Log.d2 logger "@[loc_orbit = %a@]" pp_bexpr loc_orbit;
    let s_vars, discard_s_vars = List.fold_left begin fun (k, d) v ->
      if Realib.Env.Symb.compare nuvar v = 0 then v :: k, d else
        match vart v with
          | Var.Prime (Var.AliasRel _ | Var.FieldAliasRel _)
                when not transient_ffmode_heap_vars -> v :: k, d
          | Var.Prime _ | Var.Ret _ -> k, v :: d
          | _ -> v :: k, d
    end ([], []) env.Realib.Env.s_vars in
    let env = Realib.Env.{ env with s_vars } in
    let enums = enums_careset () in
    let branching = branching_mode env &&~ loc_orbit in
    Log.d logger "@[branching = %a@]" pp_bexpr branching;
    let df = { df with
      d_final = (* (exist discard_s_vars df.d_final) *)(* &&~ enums *)ff;
      d_ass = (* (exist discard_s_vars df.d_ass) *)(* &&~ enums *)branching;
      d_init = exist discard_s_vars df.d_init &&~ enums &&~ nominal_mode;
      d_disc_equs = List.fold_left
        (fun acc (v, e) ->
          if List.mem v discard_s_vars then acc
          else (v, e ^^~~ branching) :: acc)
        [] df.d_disc_equs;
    } in
    { t with env; df }

  (* open Base *)
  (* open SCFG *)
  (* module Bool = Bddapron.Expr0.Bool *)
  (* let unify_branch_vars { env; cs; df } = *)
  (*   let module Vs = Env.VarSet in *)
  (*   let tau = tvar unified_branch_var in *)
  (*   let br_vars = Vs.remove unified_branch_var cond_vars in *)
  (*   let br_vars = Vs.fold (fun v -> PSette.add (tvar v)) br_vars empty_vset in *)
  (*   let substs = PSette.fold (fun v -> List.cons (v, tau)) br_vars [] in *)
  (*   let subst = POps.subst_vars substs in *)
  (*   let bsubst e = Bool.to_expr e |> subst |> Bool.of_expr in *)
  (*   let df = { df with *)
  (*     d_init = bsubst df.d_init; *)
  (*     d_disc_equs = List.map (fun (v, e) -> v, subst e) df.d_disc_equs; *)
  (*   } in *)
  (*   let nobranch v = not (PSette.mem v br_vars) in *)
  (*   let env = Realib.Env.{ env with *)
  (*     i_vars = (\* tau ::  *\)List.filter nobranch env.i_vars; *)
  (*     b_vars = (\* tau ::  *\)List.filter nobranch env.b_vars; *)
  (*     bi_vars = (\* tau ::  *\)List.filter nobranch env.bi_vars; *)
  (*   } in *)
  (*   let cs = Realib.CtrlSpec.{ cs_groups = match cs.cs_groups with *)
  (*     | g0 :: g'l -> *)
  (*         { g0 with *)
  (*           u_vars = (\* tau ::  *\)List.filter nobranch g0.u_vars; *)
  (*           bu_vars = (\* tau ::  *\)List.filter nobranch g0.bu_vars; } :: g'l *)
  (*     | [] -> [] *)
  (*   } in *)
  (*   { env; cs; df } *)

  let tsymbs locs = CN.SSet.fold (fun l x -> Cn2rl.translate_symb l :: x) locs []

  let enum_locs locs { env; cs; df } =
    CN.SSet.fold begin fun l acc ->
      let x = loc_in env (CN.SSet.singleton l) in
      let lequs: Realib.Env.equs_t =
        List.fold_left (fun acc (v, e) -> (v, e ^~~ x) :: acc) [] df.d_disc_equs in
      CN.SMap.add l lequs acc
    end locs CN.SMap.empty

  let maybe_guard_equs ?(split_prop = 0.33) { env } equs =
    let tau = BOps.var (tvar SCFG.branch_var) in
    let deps, nodeps =
      let tau = Cudd.Bdd.topvar tau in
      List.fold_left begin fun (deps, nodeps) (v, e) ->
        if POps.var v = e then deps, nodeps    (* filter-out obvious identity *)
        else if Cudd.Bdd.is_var_in tau (POps.support' e)
        then (v, e) :: deps, nodeps
        else deps, (v, e) :: nodeps
      end ([], []) equs in
    let ndeps = float_of_int (List.length deps)
    and nnodeps = float_of_int (List.length nodeps) in
    let nequs = ndeps +. nnodeps in
    if nequs = 0. then `Equs equs else     (* TODO: handling full identify??? *)
      let prop_deps = ndeps /. nequs in
      if prop_deps >= split_prop then
        let tequs, eequs = List.fold_left (fun (t, e) (v, x) ->
          (v, x ^~~ tau) :: t, (v, x ^~~ !~tau) :: e)
          (nodeps, nodeps) deps in
        `Cond (tau, tequs, eequs)
      else
        `Equs (List.rev_append nodeps deps)

  open Realib
  let make_monauto ~hashcons ~avoid_labels ~enabled_locs ({ env } as x) cn =
    let open Rl2cn.Open in
    let dd = mk_data ~avoid_labels env in
    let cn_typs = translate_typdefs dd in
    let decls = declare_input_vars dd CN.SMap.empty env.Env.i_vars CN.one in
    let open OfCF in
    let node equs =
      let n =
        `Desc CN.{
          cn_typs;
          cn_decls = begin mk_equs_map equs
              |> declare_state_vars dd decls env.Realib.Env.s_vars
              |> declare_locals dd
          end;
          cn_init = mk_bcst' true;
          cn_assertion = mk_bcst' true;
          cn_invariant = None;
          cn_reachable = None;
          cn_attractive = None;
        }
      in
      match CN.check_node n with
        | None, msgs ->
            M.SCFGAsCG.Reporting.report_msgs err_formatter msgs;
            CN.print_node err_formatter n;
            Log.w logger "@[Extraction error; continuing anyway@<1>…@]";
            raise Exit
        | Some cn, _ ->
            reset_data dd; Updates cn
    and pred p =
      let pn_value = translate_bexp dd p in
      let pn_decls = declare_input_vars dd decls env.Realib.Env.s_vars CN.one in
      let pn_decls = declare_locals dd pn_decls in
      let p = `Desc CN.{ pn_typs = cn_typs; pn_decls; pn_value } in
      match CN.check_pred p with
        | None, msgs ->
            M.SCFGAsCG.Reporting.report_msgs err_formatter msgs;
            CN.print_pred err_formatter p;
            Log.w logger "@[Extraction error; continuing anyway@<1>…@]";
            raise Exit
        | Some pn, _ ->
            reset_data dd; pn
    in
    let trans_funcs = CN.SMap.mapi
      begin fun l equs -> match maybe_guard_equs x equs with
        | `Equs equs -> node equs
        | `Cond (g, t, e) -> Guarded (pred g, node t, node e)
      end (enum_locs enabled_locs x)
    in
    OfCF.auto ~hashcons ~enabled_locs policies SCFG.local_vars cn trans_funcs

end

let hashcons = true

(** NB: Eliminates ν… *)
let make_monnode ~output_monctrln ~output_monnode ~output_monauto out
    (module M: REALIB_MODEL_WITH_SECSUM(* _N_GUARDS_FUNC *)) =
  let module PostProc = PostProc (M) in
  let x = PostProc.init in
  let enabled_locs, unreachable_locs, x = PostProc.contract x in
  let x = PostProc.simplify_nu enabled_locs x in
  let PostProc.{ env; cs; df } = x in

  let avoid_labels = PostProc.tsymbs unreachable_locs in
  if output_monnode || output_monctrln || output_monauto then begin
    try match Rl2cn.extract_ctrln ~avoid_labels env cs df |> CN.check_node with
      | None, msgs ->
          M.SCFGAsCG.Reporting.report_msgs err_formatter msgs;
          Log.w logger "@[Extraction error; continuing anyway@<1>…@]";
      | Some cn, _ ->
          if output_monctrln then begin
            let fmt, close = out.out_exec
              ~descr:"Controllable-Nbac@ monitor@ node"
              ~backup:Forget "ctrln" in
            CN.print_node fmt cn;
            close ()
          end;
          if output_monnode then begin
            let monnode = M.OfCF.node ~hashcons ~enabled_locs
              M.policies M.SCFG.local_vars cn in
            let fmt, close = out.out_exec ~descr:"monitor@ node"
              ~backup:Forget "monnode" in
            M.OfCF.MonNode.print fmt monnode;
            close ()
          end;
          if output_monauto then begin
            let monauto = PostProc.make_monauto ~hashcons ~avoid_labels
              ~enabled_locs x cn in
            let fmt, close = out.out_exec ~descr:"monitor@ automaton"
              ~backup:Forget "monauto" in
            M.OfCF.MonAuto.print fmt monauto;
            close ();
          end
    with Exit -> ()
  end;

  let module Override = struct
    let env = env
    let cs = cs
    let enabled_locs = enabled_locs
    let unreachable_locs = unreachable_locs
  end in
  Ok (module struct
    include M
    include Override
    let env, cf = Realib.Supra.df2cf "d" env df;;
  end: REALIB_MODEL_WITH_SECSUM(* _N_GUARDS_FUNC *))

(* --- *)

let compute_rmoncfg ~output_rmoncfg out (m: secsum_with_model) =
  if output_rmoncfg then begin
    let module M = (val m: REALIB_MODEL_WITH_SECSUM) in
    let open M in
    let open Realib.Env in
    let open Realib.Supra in
    let open Realib.Program in
    let open Realib.CuddUtil in
    let cf =
      cudd_make_varmap env;
      let rs = reordering_save_n_enable_if !enable_reorderings env.env in
      let cs', env', cf =
        let env, algo = parse_synth env "pE:v={ℓ};rT;rAB:d=O" in
        try let cs, _, cf = synth (env, algo) env (cs, cf.c_ass, cf) in
            cs, env, cf
        with SynthesisFailure (cs, env, cf) -> cs, env, cf
      in
      assert (cs == cs');
      assert (env == env');
      reordering_restore env.env rs;
      cf
    in
    let fmt, close = out.out_exec ~descr:"contracted@ realib@ CFG"
      ~backup:Forget ~suff:"-rcfg" "dot" in
    Realib.Cfg.print_dot env fmt cf.c_cfg true;
    close ();
    let fmt, close = out.out_exec ~descr:"contracted@ realib@ CFG"
      ~backup:Forget ~suff:"-rcfg-noarcs" "dot" in
    Realib.Cfg.print_dot env fmt cf.c_cfg false;
    close ()
  end;
  Ok m

(* --- *)

let extract_guards_func ~output_ctrlf out ((module M): secsum_with_model) =
  (* First from g0 + all of g1 *)
  let open Realib.CtrlSpec in
  let cs_groups = match M.cs.cs_groups with
    | { c_vars = g :: _; bp_specs = s :: _; } as g0 :: tl ->
        { g0 with c_vars = [ g ]; bp_specs = [ s ] } :: tl
    | l -> l
  and guards_equs = match M.secsum_equs with
    | (g :: _) :: g'l -> [g] :: g'l
    | gl -> gl
  in
  let func = Rl2cn.extract_ctrlf M.env { cs_groups } guards_equs in
  match CN.check_func func with
    | None, msgs ->
        M.SCFGAsCG.Reporting.report_msgs err_formatter msgs;
        Error `ExtractionError
    | Some cf, _ ->
        if output_ctrlf then begin
          let fmt, close = out.out_exec
            ~descr:"Controllable-Nbac@ security@ guards@ function"
            ~backup:Forget ~suff:"-guards" "ctrlf" in
          CN.print_func fmt func;
          close ()
        end;
        Ok (module struct
          let guards_func = cf
          include M
        end: REALIB_MODEL_WITH_SECSUM_N_GUARDS_FUNC)

(* --- *)

let make_guards ~output_guards ~output_ctrlf out (m: secsum_with_model) =
  if output_guards || output_ctrlf then
    match extract_guards_func ~output_ctrlf out m with
      | Ok (module Mg: REALIB_MODEL_WITH_SECSUM_N_GUARDS_FUNC) ->
          if output_guards then begin
            let guards = Mg.(OfCF.guards ~hashcons:true policies guards_func) in
            let fmt, close = out.out_exec ~descr:"guards@ function"
              ~backup:Forget "guards" in
            Mg.OfCF.Guards.print fmt guards;
            close ()
          end;
          Ok m
      | Error x -> Error x
  else
    Ok m

(* --- *)

let extract_secsum_func ~output_ctrlf out
    ((module M): secsum_with_model)
    (* (module M: REALIB_MODEL_WITH_SECSUM_N_GUARDS_FUNC) *) =
  let func = M.(Rl2cn.extract_ctrlf env cs secsum_equs) in
  match CN.check_func func with
    | None, msgs ->
        M.SCFGAsCG.Reporting.report_msgs err_formatter msgs;
        Error `ExtractionError
    | Some cf, _ ->
        if output_ctrlf then begin
          let fmt, close = out.out_exec
            ~descr:"Controllable-Nbac@ security@ summary@ function"
            ~backup:Forget ~suff:"-secsum" "ctrlf" in
          CN.print_func fmt func;
          close ()
        end;
        Ok ((module struct
          let secsum_func = cf
          include M
        end: CTRLNBAC_SECSUM_FUNC), M.env)

(* --- *)

let extract_stats (module M: FULL_STATS) =
  M.meth_stats,
  Stats.{ symb_heap_stats = M.symb_heap_stats;
          meth_scfg_gen = M.scfg_gen_stats;
          meth_handling = Done { model = M.model_constr_stats;
                                 synth = M.summ_synth_stats; }; }

let make_secsum ~output_secsum ~output_secsum_bin out out_bin
    (module M: CTRLNBAC_SECSUM_FUNC) =
  let bindings = M.OfCF.func M.secsum_func in
  Ok (extract_stats (module M), begin match M.sign with
    | Some s ->
        let policies = M.policies in
        let ssum = SecSum.make ~policies M.SCFG.features s bindings in
        if output_secsum then begin
          let fmt, close = out.out_exec ~descr:"security@ summary"
            ~backup:Forget "secsum" in
          SecSum.print fmt ssum;
          close ()
        end;
        if output_secsum_bin then begin
          let oc, close = out_bin.out_exec
            ~descr:"security@ summary@ in@ binary@ format"
            ~backup:Forget "secsum_bin" in
          SecSum.output_bin oc ssum;
          close ()
        end;
        (* pp std_formatter "%a@." SecSum.print ssum; *)
        Some ssum
    | _ ->                                          (* just output and discard *)
        pp std_formatter "%a@." SecSum.print_bindings bindings;
        None
  end)

(* -------------------------------------------------------------------------- *)

(* XXX: Move most of what's below in ScgsCore *)

module type OPTIONS = sig
  include Base.OPTIONS
  include Trans.EFFECTS_INFERENCE
  include SemLoc.ALL_OPTIONS
  val prop_in_scfg: [ `None | `Full | `Consts | `Nu ]
  val methskip_condition: [ `None | `StateVars of int ]
  val methskip_policy: [ `ThirdParty ]
  val semloc_encoding: [ `ExplicitJSHCDR
                       | `ExplicitJSSymbolicHCDR
                       | `SymbolicJSHCDR ]
end

module DefaultOptions: OPTIONS = struct
  include Base.DefaultOptions
  include Trans.DefaultEffectsInference
  include SemLoc.DefaultOptions
  let prop_in_scfg = `Full
  let methskip_condition = `None
  let methskip_policy = `ThirdParty
  let semloc_encoding = `ExplicitJSSymbolicHCDR
end

module type APP_OPTIONS = sig
  include APP_OPTIONS
  include OPTIONS
end

module SCFGBuilder (Options: APP_OPTIONS) (Input: INPUT) = struct

  module SCFG = struct
    let tic = Timing.tic ();;
    open SCFG.Stats
    module SCFG = App (Options) (Input)
    include SCFG

    let mk_stats scfg =
      let cards, degrees = sizes CardinalsNDegrees scfg in
      let decls = sizes VarDecls scfg in
      Stats.SCFGGen.{ time = Timing.tic () -. tic;
                      cards; degrees; decls; }
    ;;

    let { nb_locations; nb_transitions } = sizes Cardinals scfg;;
    Log.i logger "@[Original@ SCFG@ has@ %i@ locations@ and@ %i@ \
                    transitions.@]"
      nb_locations nb_transitions;;

    let _ = match Options.methskip_condition with
      | `StateVars thr ->
          let sn = (sizes VarDecls scfg).nb_state_vars in
          if sn >= thr then
            let stats = heap_stats, mk_stats scfg in
            raise @@ SkipSynthesis
              (stats, PPrt.pp2 "State@ variables@ threshold@ \
               exceeded@ (%i @<2>≥ %i)" sn thr)
      | _ -> ()
    ;;

    let scfg, transient_ffmode_heap_vars = match Options.prop_in_scfg with
      | `None -> scfg, true
      | `Full ->
          Log.i logger "@[Full@ propagation@ in@ SCFG…@]";
          let module Prop = PP.FullProp (SCFG) in
          Log.roll Prop.apply_on scfg, true
      | `Consts ->
          Log.i logger "@[Propagating@ constants@ in@ SCFG…@]";
          let module Prop = PP.ConstProp (SCFG) in
          Log.roll Prop.apply_on scfg, true
      | `Nu ->
          Log.i logger "@[Propagating@ `%a'@ in@ SCFG…@]" Var.print ua_var;
          let module Prop = PP.VarProp (SCFG) (struct let v = ua_var end) in
          Log.roll Prop.apply_on scfg, true
    ;;

    let scfg_gen_stats = mk_stats scfg;;
    Log.i logger "@[Generated@ SCFG@ has@ %a.@]\
                 " Stats.SCFGGen.print_scfg_infos scfg_gen_stats;;
    Log.i logger "@[Generation@ took@ %a.@]\
                 " Stats.SCFGGen.print_gen_time scfg_gen_stats;;
  end;;

  (* --- *)

  module SCFGAsCG = AsCG.SCFG (SCFG)
  open SCFGAsCG.Reporting
  open SCFG
  open CN

  type error =
    [
    | SCFGAsCG.Flag.t loc_msg
    | `TError of (SCFGAsCG.Flag.t option * (formatter -> unit)) list
    ]

  let pp_errors: error list PPrt.pp = report_msgs

  let mk_cfg ?max_cudd_mem ~allocate_primes ?(synth_algo = !default_synth_algo)
      out =
    (* let svo_make_log_fmt = Some (out.out_exec ~backup:Forget) in *)
    let strong_variable_affinities = List.rev_map begin fun aff ->
      Env.VarSet.fold (fun v -> SSet.add (AsCG.tsv v)) aff SSet.empty
    end variable_affinities in
    Cn2rl.(translate_cfg
             ?max_cudd_mem
             ~env_booking_factor: 1
             ~cdfequs_only: (String.length synth_algo > 2 &&
                               Str.first_chars synth_algo 2 = "sS")
             ~static_variable_ordering: (SVOTopoBFS { svo_make_log_fmt = None })
             ~enable_primed_state_vars: (allocate_primes = `All)
             ~enable_primed_input_vars: (allocate_primes <> `None)
             ?dynamic_variable_reordering: (Some Cudd.Man.REORDER_GROUP_SIFT)
             ~strong_variable_affinities)

  let tic = Timing.tic ();;
  let model ?max_cudd_mem ~allocate_primes ?synth_algo out cg =
    match mk_cfg ?max_cudd_mem ~allocate_primes ?synth_algo out cg with
      | `TKo msgs ->
          Error (msgs :> error list)
      | `TOk (env, cs, cf, local_defs) ->
          Log.i logger "@[Successful@ SCFG@ model@ creation.@]";
          if not !enable_reorderings then
            Realib.CuddUtil.disable_reordering env.Realib.Env.env;
          Ok (module struct
            module SCFG = SCFG
            module SCFGAsCG = SCFGAsCG
            module OfCF = MakeOfCF (MakeHeuristics (Options))
            module E = (val Realib.Env.as_module env)
            let transient_ffmode_heap_vars = transient_ffmode_heap_vars
            let env = env
            let cs = cs
            let cf = cf
            let sign = Input.Meth.sign Input.meth
            let meth_stats = Input.Meth.ImplStats.stats Cardinals Input.meth
            let symb_heap_stats = heap_stats
            let scfg = scfg
            let local_defs = local_defs
            let policies = SCFG.policies
            let scfg_gen_stats = scfg_gen_stats
            let model_constr_stats =
              let module POps =  BddapronUtil.POps (E.BEnv) in
              let vsuppsize v =
                Cudd.Bdd.supportsize
                  (POps.support' (POps.var (AsCG.tsv v
                                               |> Cn2rl.translate_symb)))
              in
              let open SCFG.Env.VarSet in
              let suppsize = fold (fun v -> (+) (vsuppsize v)) support 0
              and footsize = fold (fun v -> (+) (vsuppsize v)) footprint' 0 in
              Stats.Constr.{ time = Timing.toc tic; suppsize; footsize }
          end: REALIB_MODEL)
      | exception Failure e when cudderrp e ->
          (* A custom exception would be better *)
          Log.w logger "@[CUDD@ ran@ out@ of@ memory:@ skipping@<1>…@]";
          (* XXX: env seems in the limbs here. Don't think we can clear it or
             put in in purgatory without come cooperation from Realib. *)
          raise @@ SkipSynthesis
            ((heap_stats, scfg_gen_stats), PPrt.pp2 "CUDD out of memory%a"
              (ppa_opt " (%LuB)") max_cudd_mem)

  let output_cdrs_dot ~enable out =
    if enable then
      let fmt, close = out.out_exec ~descr:"CDR-annotated@ CFG@ as@ DOT"
        ~backup:Forget  ~suff:"-cdrs" "dot" in
      output_cdrs_dot fmt;
      close ()

  let output_scfg_dot ~enable out =
    if enable then
      let fmt, close = out.out_exec ~descr:"SCFG@ as@ DOT"
        ~backup:Forget ~suff:"-scfg" "dot" in
      AsDot.output fmt scfg;
      close ()

  let scfg_model ?max_cudd_mem ~allocate_primes ?synth_algo ~enable_output out =
    CtrlNbac.Symb.reset ();
    match SCFGAsCG.cfg scfg with
      | `KO cg, msgs ->
          Log.e logger "@[Errors@ in@ SCFG@ check.@]";
          Error (msgs :> error list)
      | `OK cg, _ ->
          Log.i logger "@[Successful@ SCFG@ check.@]";
          if enable_output then begin
            let fmt, close = out.out_exec ~descr:"SCFG"
              ~backup:Forget "ctrlg" in
            CN.print_cfg fmt cg;
            close ()
          end;
          model ?max_cudd_mem ~allocate_primes ?synth_algo out cg

end

(* -------------------------------------------------------------------------- *)

let output_cfg_dot ~enable out (module Opts: OPTIONS) (module I: INPUT) =
  if enable then
    let module CFG = (OOLang.CFG.Make (Opts) (I.Meth) (I.MethInfos)
                        (I.Resolutions) (I.ResolutionUniverse)) in
    let fmt, close = out.out_exec ~descr:"CFG@ as@ DOT"
      ~backup:Forget  ~suff:"-cfg" "dot" in
    CFG.output_dot fmt (CFG.of_meth I.meth);
    close ()

let make_scfg
    ?(output_cfg = false)
    ?(output_cdrs = false)
    ?(output_scfg = false)
    ?(output_ctrlg = false)
    out
    ~anticipate_monitor_contraction
    ~anticipate_rmonitor_construction
    ?max_cudd_mem
    ?synth_algo
    ?xxx_skip
    (module Options: OPTIONS)
    (module Input: INPUT)
    =
  output_cfg_dot ~enable:output_cfg out (module Options) (module Input);
  Log.i logger "@[Bulding@ SCFG…@]";
  let module SCFGModel = SCFGBuilder (struct
    include Options
    open SemLoc
    module SemLocEncoding = (val begin match semloc_encoding with
      | `SymbolicJSHCDR -> (module (PartitionedSymbolicJSnHCDR (Options)))
      | `ExplicitJSHCDR -> (module (PartitionedExplicitJSnHCDR (Options)))
      | `ExplicitJSSymbolicHCDR -> (module (PartitionedSymbolicHCDR (Options)))
    end: ENCODING)
  end) (Input) in
  SCFGModel.output_cdrs_dot ~enable:output_cdrs out;
  SCFGModel.output_scfg_dot ~enable:output_scfg out;
  let allocate_primes =
    if anticipate_rmonitor_construction then `All
    else if anticipate_monitor_contraction then `Inputs
    else `None
  in
  match xxx_skip with
    | Some cause
      -> let stats = SCFGModel.SCFG.heap_stats, SCFGModel.SCFG.scfg_gen_stats in
        raise @@ SkipSynthesis (stats, cause)
    | None
      -> match
          SCFGModel.scfg_model ~enable_output:output_ctrlg ?max_cudd_mem
            ~allocate_primes out ?synth_algo
        with
          | Error e -> Error (`InSCFG (fun fmt -> SCFGModel.pp_errors fmt e))
          | Ok scfg_model -> Ok scfg_model

(* -------------------------------------------------------------------------- *)
