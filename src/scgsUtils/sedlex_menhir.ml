(* Adapted from
   https://github.com/unhammer/ocaml_cg_streamparse/blob/match-singlechar-example/sedlex_menhir.ml
*)
(*
  Boilerplate for using sedlex with Menhir, based on
  https://github.com/Drup/llvm/blob/3c43000f4e86af5b9b368f50721604957d403750/test/Bindings/OCaml/kaleidoscope/src/syntax.ml
*)

(** The state of the parser, a stream and a position. *)
type lexbuf = {
  stream : Sedlexing.lexbuf ;
  mutable pos : Lexing.position ;
}

(** Initialize with the null position. *)
let make_lexbuf ?(file="") stream =
  let pos = {Lexing.
             pos_fname = file;
             pos_lnum = 1; (* Start lines at 1, not 0 *)
             pos_bol = 0;
             pos_cnum = 0;
            }
  in { pos ; stream }

(** Register a new line in the lexer's position. *)
let new_line ?(n=0) lexbuf =
  let open Lexing in
  let lcp = lexbuf.pos in
  lexbuf.pos <-
    {lcp with
      pos_lnum = lcp.pos_lnum + 1;
      pos_bol = lcp.pos_cnum;
    }

(** Update the position with the stream. *)
let update lexbuf =
  let new_pos = Sedlexing.lexeme_end lexbuf.stream in
  let p = lexbuf.pos in
  lexbuf.pos <- {p with Lexing.pos_cnum = new_pos }

(** The last matched word. *)
let lexeme { stream } = Sedlexing.Utf8.lexeme stream
let lexeme_length { stream } = Sedlexing.lexeme_length stream          (* ??? *)
let sub_lexeme { stream } = Sedlexing.Utf8.sub_lexeme stream

(* --- *)

type err_loc = (string * int * int * string)   (** [(file, line, col, token)] *)

let err_loc
    ({ pos = Lexing.{ pos_lnum; pos_cnum; pos_bol; pos_fname } } as lexbuf) =
  (pos_fname, pos_lnum, pos_cnum - pos_bol, lexeme lexbuf)

exception SyntaxError of err_loc
exception ParseError of err_loc

let pp_err_loc fmt (file, line, cnum, _) =
  (match file with
    | "" -> Format.fprintf fmt "Line@ "
    | f  -> Format.fprintf fmt "File \"%s\",@ line@ " f);
  Format.fprintf fmt "%i, character %i" line cnum

let pp_err kind fmt ((_, _, _, tok) as err_loc) =
  Format.fprintf fmt "%a:@ %(%)@ error@ at@ token@ `%s'\
                     " pp_err_loc err_loc kind tok

let pp_parse_error = pp_err "parse"
let pp_syntax_error = pp_err "syntax"

let mk_lexer lexer lexbuf () =
  let ante_position = lexbuf.pos in
  let post_position = lexbuf.pos in
  lexer lexbuf, ante_position, post_position

let sedlex_with_menhir lexer' parser' lexbuf =
  let lexer = mk_lexer lexer' lexbuf in
  let parser = MenhirLib.Convert.Simplified.traditional2revised parser' in
  try parser lexer with
    | Parsing.Parse_error -> raise @@ SyntaxError (err_loc lexbuf)
    | Sedlexing.MalFormed
    | Sedlexing.InvalidCodepoint _ -> raise @@ ParseError (err_loc lexbuf)
