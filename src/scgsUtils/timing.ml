open Utils

type time = float
(** In seconds, as returned by [Sys.time] *)

let pp_time: time pp = Rutils.PPrt.pp "%.4g"

let tic = Unix.gettimeofday
let diff tic toc = toc -. tic
let add = (+.)
let toc tic' = tic () -. tic'
