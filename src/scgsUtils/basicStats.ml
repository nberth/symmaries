(* -------------------------------------------------------------------------- *)

module type INCREMENTAL = sig
  type u and d
  val init': u
  val init: d -> u
  val add: u -> d -> u
end

(* -------------------------------------------------------------------------- *)

module type D4SUM = sig
  type t
  val zero: t
  val ( +. ): t -> t -> t
  val of_int: int -> t
end

module type INCREMENTAL_SUM = sig
  type u and d
  val sum: u -> d
end

module MakeSum (D: D4SUM) : sig
  include INCREMENTAL with type d = D.t
  include INCREMENTAL_SUM with type d := d and type u := u
end = struct
  open D
  type d = t
  type u = d

  let init' = zero
  let init x = x
  let add = (+.)
  let sum x = x
end

(* -------------------------------------------------------------------------- *)

module type D4MEAN = sig
  type t
  val zero: t
  val ( +. ): t -> t -> t
  val ( -. ): t -> t -> t
  val ( /. ): t -> t -> t
  val of_int: int -> t
end

module type INCREMENTAL_MEAN = sig
  type u and d
  val mean: u -> d
end

module MakeMean (D: D4MEAN) : sig
  include INCREMENTAL with type d = D.t
  include INCREMENTAL_MEAN with type d := d and type u := u
  val population_size: u -> int
end = struct
  open D
  type d = t
  type u =
      {
        count: int;
        mean: d;
      }

  let init' = { count = 0; mean = zero }
  let init x = { count = 1; mean = x }

  let add { count; mean } x =
    let count = succ count in
    let delta = x -. mean in
    let mean = mean +. delta /. of_int count in
    { count; mean }

  let population_size { count } = count
  let mean { mean } = mean
  (* let mean = function *)
  (*   | { count = 0 } -> raise EmptyPopulation *)
  (*   | { mean } -> mean *)
end

(* -------------------------------------------------------------------------- *)

module type D4MIN_MAX = sig
  type t
  val ( <. ): t -> t -> bool
  val infinity: t
  val neg_infinity: t
end

module type INCREMENTAL_MIN_MAX = sig
  type u and d
  val min: u -> d
  val max: u -> d
end

module MakeMinMax (D: D4MIN_MAX) : sig
  include INCREMENTAL with type d = D.t
  include INCREMENTAL_MIN_MAX with type d := d and type u := u
end = struct
  open D
  type d = t
  type u =
      {
        min: d;
        max: d;
      }

  let init' = { min = infinity; max = neg_infinity }
  let init x = { min = x; max = x }

  let add ({ min; max } as u) x =
    let u = if x <. min then { u with min = x } else u in
    let u = if max <. x then { u with max = x } else u in
    u

  let min { min } = min
  let max { max } = max
end

(* -------------------------------------------------------------------------- *)

module type D4WELFORD = sig
  include D4MEAN
  val ( *. ): t -> t -> t
  val sqrt: t -> t
  val nan: t
end

module type INCREMENTAL_STATS = sig
  include INCREMENTAL_MEAN
  val variance: u -> d
  val stddev: u -> d
  val stddev': u -> d
end

exception EmptyPopulation
exception InsufficientSample

module type WELFORD = sig
  include INCREMENTAL
  include INCREMENTAL_STATS with type d := d and type u := u
  val population_size: u -> int
  module Sample: sig
    include INCREMENTAL_STATS with type d := d and type u := u
    val sample_size: u -> int
  end
end

module MakeWelford (D: D4WELFORD) : WELFORD with type d = D.t = struct
  module Mean = MakeMean (D)

  open D
  type d = t
  type u =
      {
        m: Mean.u;
        m2: d;
      }

  let init' = { m = Mean.init'; m2 = zero }
  let init x = { m = Mean.init x; m2 = zero }

  let mean { m } = Mean.mean m
  let population_size { m } = Mean.population_size m

  let add ({ m; m2 } as u) x =
    let delta = x -. mean u in
    let m = Mean.add m x in
    let delta2 = x -. Mean.mean m in
    let m2 = m2 +. delta *. delta2 in
    { m; m2 }

  let variance ({ m2 } as u) =
    let count = population_size u in
    if count = 0
    then raise EmptyPopulation
    else m2 /. of_int count
  let stddev u = sqrt (variance u)
  let stddev' u = try stddev u with EmptyPopulation -> nan

  module Sample = struct
    let mean = mean
    let sample_size = population_size
    let variance ({ m2 } as u) =
      let count = sample_size u in
      if count < 2
      then raise InsufficientSample
      else m2 /. of_int (pred count)
    let stddev u = sqrt (variance u)
    let stddev' u = try stddev u with EmptyPopulation -> nan
  end
end

(* -------------------------------------------------------------------------- *)

module Pair (X: INCREMENTAL) (Y: INCREMENTAL with type d = X.d) : sig
  include INCREMENTAL with type d = X.d
  val fst: u -> X.u
  val snd: u -> Y.u
end = struct
  type u = X.u * Y.u
  type d = X.d
  let init' = X.init', Y.init'
  let init e = X.init e, Y.init e
  let add (x, y) e = X.add x e, Y.add y e
  let fst = fst
  let snd = snd
end

(* -------------------------------------------------------------------------- *)

module Collect (S: INCREMENTAL) = struct
  open S
  let list' f l = List.fold_left (fun acc e -> add acc (f e)) init' l
  let array' f l = List.fold_left (fun acc e -> add acc (f e)) init' l
  let list l = List.fold_left add init' l
  let array l = Array.fold_left add init' l
end

(* -------------------------------------------------------------------------- *)

(* type quantiles = (float * float) list *)

module MakeInplaceQuantiles (D: sig
  type t
  val to_float: t -> float
  (* val compare: t -> t -> int *)
end) = struct
  let of_sorted_array f a =
    let n = Array.length a in
    assert (n > 0);
    let p = float_of_int (n - 1) *. f in
    let i = floor p in
    let d = p -. i in
    let i = int_of_float i in
    if i == n - 1
    then D.to_float a.(i)
    else (1. -. d) *. D.to_float a.(i) +. d *. D.to_float a.(i+1)
end

(* -------------------------------------------------------------------------- *)

module Int = struct
  include Utils.Integer
  let to_float = float_of_int
  let of_float = int_of_float
end

(* -------------------------------------------------------------------------- *)

module Float = struct
  include Stdlib
  type t = float
  let of_int = float_of_int
  let to_float x = x
  let zero = 0.0
  let ( <. ) a b = compare a b < 0
end

module FloatSum = MakeSum (Float)
module FloatMinMax = MakeMinMax (Float)
module FloatWelford = MakeWelford (Float)
module FloatQuantiles = MakeInplaceQuantiles (Float)

(* -------------------------------------------------------------------------- *)

module Aggreg = struct

  type ('a, 'n) base_aggreg = { min: 'a; max: 'a; mean: 'n; stddev: 'n }
  type 'a sum = { sum: 'a }
  type 'n quantiles = (float * 'n) list

  type ('a, 'n) aggres =
    | Basic of ('a, 'n) base_aggreg
    | Summed of ('a, 'n) base_aggreg * 'n sum
    | Quantiled of ('a, 'n) base_aggreg * 'n quantiles

  type quantiles_spec =
    | Uniform of float
    | List of float list

  module Make (D: sig
    type t
    val compare: t -> t -> int
    val to_float: t -> float
    val of_float: float -> t
  end) = struct
    let quantiles =
      let module F = Pair (FloatMinMax) (FloatWelford) in
      let module Q = MakeInplaceQuantiles (D) in
      fun ?(sorted = false) quantiles_spec a ->             (* sorts a if needed *)
        let s = Array.fold_left (fun acc d -> F.add acc (D.to_float d)) F.init' a in
        if not sorted then
          Array.sort D.compare a;
        let aggreg = {
          min = D.of_float (FloatMinMax.min (F.fst s));
          max = D.of_float (FloatMinMax.max (F.fst s));
          mean = FloatWelford.mean (F.snd s);
          stddev = FloatWelford.stddev' (F.snd s);
        } in
        let quantiles = match quantiles_spec with
          | Uniform f -> Utils.fold_frange f f 1.0 List.cons [] |> List.rev
          | List l -> l
        in
        Quantiled (aggreg, List.map (fun f -> f, Q.of_sorted_array f a) quantiles)
  end
end

(* -------------------------------------------------------------------------- *)


module Outputs = struct
  open Format
  open Rutils.PPrt
  open Utils

  let pp = fprintf
  let pi = pp_print_int
  let ps ?k0 fmt s = match k0 with
    | None -> pp_print_string fmt s
    | Some k -> pp fmt "%s%s" k s
  let pf fmt = (* Format.pp_print_float *) pp fmt "%.5g"
  let pp_secs fmt s = pp fmt "%.3g@ second%s" s (if s < 1. then "" else "s")

  let ppq ppe fmt = pp fmt "\"%a\"" ppe

  let pp_percent fmt f = pp fmt "%.3g%%" (f *. 100.)

  let print_assoc_pairs ?(fopen: ufmt = "@[<v>") ?(fclose: ufmt = "@]") pa pb
      : ('a * 'b) list pp =
    pp_lst ~fempty:"" ~fopen ~fsep:",@;" ~fclose
      (pp_pair ~format:"%a: %a" pa pb)

  (* let print_assoc_list pp_lst fmt = *)
  (*   pp fmt "@[<2>[ %a@] ]" pp_lst *)

  let print_assoc_map pp_map fmt =
    pp fmt "@[<2>{ %a@] }" pp_map

  let print_assoc_map_v pp_map fmt =
    pp fmt "@[<v2>{ %a@] }" pp_map

  let pp_strmap pe = StrMap.print'                      (* for map of scalars *)
    ~empty:"" ~left:"" ~sep:",@;" ~right:""
    ~aleft:"@[<2>\"" ~assoc:"\": " ~aright:"@]" pe

  let pp_strmap' pe = StrMap.print'                        (* for map of maps *)
    ~empty:"" ~left:"" ~sep:",@;" ~right:""
    ~aleft:"@[<2>\"" ~assoc:"\": {@;@[" ~aright:"@]@]@;}" pe

  open Aggreg

  let print_base_aggreg ?(pk = ps) ?k0 pa pn fmt { min; max;
                                                   mean; stddev } =
    let pk = pk ?k0 in
    pp fmt "%a: %a,@;%a: %a,@;%a: %a,@;%a: %a"
      pk "min" pa min
      pk "max" pa max
      pk "mean" pn mean
      pk "stddev" pn stddev

  let print_sum ?(pk = ps) ?k0 pn fmt { sum } =
    pp fmt "%a: %a"
      (pk ?k0) "sum" pn sum

  let print_quantiles ?(pk = ps) ?k0 ?(fopen: ufmt = "") ?(fclose: ufmt = "")
      pn fmt =
    pp fmt "%a: %a"
      (pk ?k0) "quantiles"
      (print_assoc_map (print_assoc_pairs ~fopen ~fclose pp_percent pn))

  let print_aggreg ?pk ?k0 pa pn fmt = function
    | Basic agg
      -> print_base_aggreg ?pk ?k0 pa pn fmt agg
    | Summed (agg, sum)
      -> (pp fmt "%a,@;%a"
           (print_base_aggreg ?pk ?k0 pa pn) agg
           (print_sum ?pk ?k0 pn) sum)
    | Quantiled (agg, qtls)
      -> (pp fmt "%a,@;%a"
           (print_base_aggreg ?pk ?k0 pa pn) agg
           (print_quantiles ?pk ?k0 pn) qtls)

end

(* -------------------------------------------------------------------------- *)
