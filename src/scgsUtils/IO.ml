open Format

let escape_label =
  Str.global_substitute (Str.regexp "\\\\\\|\"")
    begin fun s -> match Str.matched_string s with
      | "\\" -> "\\\\"
      | "\"" -> "\\\""
      | s -> s
    end

type 'a string_parser = string -> 'a
let mk_parser spec cstr : _ string_parser = fun str -> Scanf.sscanf str spec cstr
let try_parse (specs: 'a string_parser list) str =
  let rec try_parse = function
    | [] -> None
    | f :: tl -> try Some (f str) with
        | End_of_file | Scanf.Scan_failure _ -> try_parse tl
  in
  try_parse specs

let fold_lines ?(trim_comments = true) ?comment f ic =
  let rec read_next acc =
    try match input_line ic with
      | l when l.[0] = '#' -> comm l acc |> read_next
      | l -> f l acc |> read_next
    with
      | Invalid_argument _ -> read_next acc
      | End_of_file -> acc
  and comm l acc = match comment with
    | None -> acc
    | Some f when not trim_comments -> f l acc
    | Some f -> f (String.trim (Str.string_after l 1)) acc
  in
  read_next

module type BINARY = sig
  type t
  val output_bin: out_channel -> t -> unit
  val input_bin: in_channel -> t
  val output_bytes: t -> bytes
  val input_bytes: ?offset: int -> bytes -> t
  val digest: t -> Digest.t
  val has_digest: t -> Digest.t -> bool
end

module Bin (T: sig
  type t
  val functional: bool
end) = (struct
  include T
  let flgs = if functional then [ Marshal.Closures ] else []
  let output_bin: out_channel -> t -> unit = fun oc s -> Marshal.to_channel oc s flgs
  let input_bin: in_channel -> t = Marshal.from_channel
  let output_bytes: t -> bytes = fun s -> Marshal.to_bytes s flgs
  let input_bytes ?(offset = 0) : bytes -> t = fun b -> Marshal.from_bytes b offset
  let digest: t -> Digest.t = fun s -> output_bytes s |> Digest.bytes
  let has_digest a d = Digest.compare (digest a) d = 0
end : BINARY with type t = T.t)
