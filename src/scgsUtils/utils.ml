open Format
open Rutils

(* --- *)

let id x = x
let ign x = x

(* --- *)

type 'a pp = 'a PPrt.pp
let pp = fprintf

let app_fst f (a, b) = (f a, b)
let app_snd f (a, b) = (a, f b)
let app_pair f g (a, b) = (f a, g b)
let ( ~@. ) f = fun a b -> f b a

let opt d = function None -> d | Some x -> x
let opt' d = function None -> d | a -> a
let optp d p = function None -> d | Some e -> p e
let maybe = function
  | None -> fun _ -> ()
  | Some f -> f

let app_opt f = function
  | None -> None
  | Some e -> Some (f e)

let app' f = function
  | None -> ign
  | Some e -> f e

let merge_opt m a b = match a, b with
  | None, None -> None
  | Some x, None | None, Some x -> Some x
  | Some a, Some b -> Some (m a b)

let ppa_opt f fmt = function
  | None -> ()
  | Some x -> pp fmt f x

let pp_opt f ppe fmt = function
  | None -> ()
  | Some x -> pp fmt f ppe x

let cmp_opt cmp a b = match a, b with
  | None, None -> 0
  | Some a, Some b -> cmp a b
  | None, _ -> -1 | _, None -> 1

let pp_if f fmt = function
  | false -> ()
  | true -> pp fmt f

let ppa_if b pp fmt a =
  if b then pp fmt a

let ppt_if b f fmt =
  if b then pp fmt f

let cmp_2 cmp cmp' (a1, b1) (a2, b2) =
  let r = cmp a1 a2 in if r != 0 then r else cmp' b1 b2
let cmp_3 cmp cmp' cmp'' (a1, b1, c1) (a2, b2, c2) =
  let r = cmp_2 cmp cmp' (a1, b1) (a2, b2) in if r != 0 then r else cmp'' c1 c2

let eq_2 eq eq' (a1, b1) (a2, b2) =
  eq a1 a2 && eq' b1 b2
let eq_3 eq eq' eq'' (a1, b1, c1) (a2, b2, c2) =
  eq a1 a2 && eq' b1 b2 && eq'' c1 c2

let cmp_lst cmp l1 l2 =
  let exception Res of int in
  try let c = List.length l1 - List.length l2 in
      if c <> 0 then c else
        List.fold_left2 (fun c x y -> if c <> 0 then raise (Res c) else cmp x y)
          0 l1 l2
  with
    | Res c -> c

let hash_2 h1 h2 (a, b) = (h1 a) + (h2 b lsl 3)
let hash_3 h1 h2 h3 (a, b, c) = (hash_2 h1 h2 (a, b)) + (h3 c lsl 5)
let rec hash_lst ?(i = 0) he = function
  | [] -> 0
  | l when i < 0 -> Hashtbl.hash l
  | e :: tl -> he e lsl i + hash_lst ~i:(i-1) he tl

let pp_prec curp newp fmt f = pp fmt (if curp < newp then "("^^f^^")" else f)

let pp_lst = PPrt.pp_lst
let pp_stack s = pp_lst ~fopen:"" ~fsep:"::" ~fclose:"" ~fempty:"[]" s

module Cstrs = struct
  let cstr2 sa sb mk orig a b =
    let a', b' = sa a, sb b in
    if a == a' && b == b' then orig else mk a' b'
  let cstr3 sa sb sc mk orig a b c =
    let a', b', c' = sa a, sb b, sc c in
    if a == a' && b == b' && c == c' then orig else mk a' b' c'
  let cstrl se mk orig l =
    let rec aux = function
      | [] as l -> l
      | e :: tl as l' -> let e' = se e and tl' = aux tl in
                        if e == e' && tl == tl' then l' else e' :: tl'
    in
    let l' = aux l in
    if l == l' then orig else mk l'
end

module MapPP (Map: Map.S) = struct
  let print
      ?(fopen: PPrt.ufmt = "{@[")
      ?(fmap: PPrt.ufmt = " =>@ ")
      ?(fsep: PPrt.ufmt = ",@ ")
      ?(fclose: PPrt.ufmt = "@]}")
      ?(fempty: PPrt.ufmt option)
      pk pv fmt map =
    match fempty with
      | Some f when Map.is_empty map -> fprintf fmt f
      | _ ->
          fprintf fmt "@<1>%(%)@[" fopen;
          Map.fold (fun k v first ->
            if not first then fprintf fmt fsep;
            fprintf fmt "@[<2>%a%(%)%a@]" pk k fmap pv v;
            false) map true |> ignore;
          fprintf fmt "@]@<1>%(%)" fclose

  let pp_2 ?fclose pv pe1 pe2 fmt (m1, m2) =
    let pp_map = print ~fsep:",\\n\\@\n" ~fmap:" := " in
    let pp_mapl pv = pp_map ~fclose:",\\n\\@\n" pv in
    let pp_mapr = pp_map ~fopen:"" in
    if Map.is_empty m1
    then pp_map ?fclose pv pe2 fmt m2
    else if Map.is_empty m2
    then pp_map ?fclose pv pe1 fmt m1
    else (pp_mapl pv pe1 fmt m1;
          pp_mapr ?fclose pv pe2 fmt m2)

  let pp_3 ?fclose pv pe1 pe2 pe3 fmt (m1, m2, m3) =
    let pp_map = print ~fsep:",\\n\\@\n" ~fmap:" := " in
    let pp_mapl pv = pp_2 ~fclose:",\\n\\@\n" pv in
    let pp_mapr = pp_map ~fopen:"" in
    if Map.is_empty m1 && Map.is_empty m2
    then pp_map ?fclose pv pe3 fmt m3
    else if Map.is_empty m3
    then pp_2 ?fclose pv pe1 pe2 fmt (m1, m2)
    else (pp_mapl pv pe1 pe2 fmt (m1, m2);
          pp_mapr ?fclose pv pe3 fmt m3)

end

type ('a, 'b) pair_pp = 'a pp -> 'b pp -> ('a * 'b) PPrt.pp
type ('a, 'b) pair_fmt = ('a pp -> 'a -> 'b pp -> 'b -> unit) PPrt.fmt
let pp_pair ?(format: ('a, 'b) pair_fmt = "@[(%a,@ %a)@]"): ('a, 'b) pair_pp
    = fun ppa ppb fmt (a, b) -> pp fmt format ppa a ppb b
let pp_pair_rev ?format ppb ppa fmt (a, b) = pp_pair ?format ppb ppa fmt (b, a)

let cmp_pair = cmp_2
let cmp_triple = cmp_3
let eq_pair = eq_2
let eq_triple = eq_3
let hash_pair = hash_2
let hash_triple = hash_3

(* --- *)

module SetProd (Set: Set.S) = struct
  let fold_unique_pairs f s acc =
    let rec cont s acc =
      if Set.is_empty s then acc else
        let e = Set.choose s in
        let s = Set.remove e s in
        acc |> Set.fold (f e) s |> cont s
    in
    cont s acc
end

(* --- *)

module MapProd (Map: Map.S) = struct
  let fold_unique_pairs f m acc =
    let rec cont m acc =
      if Map.is_empty m then acc else
        let k, v = Map.choose m in
        let m = Map.remove k m in
        acc |> Map.fold (f k v) m |> cont m
    in
    cont m acc
end

(* --- *)

module type SET = sig
  include Set.S
  val print: t pp
  val print':
    ?empty:PPrt.ufmt ->
    ?left:PPrt.ufmt ->
    ?sep:PPrt.ufmt ->
    ?right:PPrt.ufmt -> t pp
end

module MakeSet (E: Helpers.ORDERED_TYPE) : (SET with type elt = E.t) = struct
  include Helpers.MakeSet (E)
  let print'
      ?(empty: PPrt.ufmt option)
      ?(left: PPrt.ufmt = "@[<hv 1>{")
      ?(sep: PPrt.ufmt = ",@ ")
      ?(right: PPrt.ufmt = "}@]")
      fmt set
      =
    match empty with
      | Some e when is_empty set -> pp fmt e
      | _ -> (pp fmt left;
             fold (fun e first -> if not first then pp fmt sep; E.print fmt e; false)
               set true |> ignore;
             pp fmt right)
end

(* --- *)

module type MAP = sig
  include Map.S
  val print: 'a pp -> 'a t pp
  val print':
    ?empty:PPrt.ufmt ->
    ?left:PPrt.ufmt ->
    ?sep:PPrt.ufmt ->
    ?right:PPrt.ufmt ->
    ?skip_key:(key -> 'a -> bool) ->
    ?skip_val:(key -> 'a -> bool) ->
    ?rev_assoc:bool ->
    ?aleft:PPrt.ufmt ->
    ?assoc:PPrt.ufmt ->
    ?aright:PPrt.ufmt -> 'a pp -> 'a t pp
end
module MakeMap (E: Helpers.ORDERED_TYPE) : (MAP with type key = E.t) = struct
  include Helpers.MakeMap (E)
  let print'
      ?(empty: PPrt.ufmt option)
      ?(left: PPrt.ufmt = "@[<hv 1>@<1>⦃")
      ?(sep: PPrt.ufmt = ",@ ")
      ?(right: PPrt.ufmt = "@<1>⦄@]")
      ?(skip_key = fun _ _ -> false)
      ?(skip_val = fun _ _ -> false)
      ?(rev_assoc = false)
      ?(aleft: PPrt.ufmt = "@[<2>")
      ?(assoc: PPrt.ufmt = " =>@ ")
      ?(aright: PPrt.ufmt = "@]")
      pv fmt map
      =
    match empty with
      | Some e when is_empty map -> pp fmt e
      | _ -> (pp fmt left;
             fold begin fun k v first ->
               let skip_key = skip_key k v and skip_val = skip_val k v in
               if skip_key && skip_val then first else begin
                 if not first then fprintf fmt sep;
                 pp fmt aleft;
                 if skip_key
                 then pv fmt v
                 else if skip_val
                 then E.print fmt k
                 else if not rev_assoc
                 then pp fmt "%a%(%)%a" E.print k assoc pv v
                 else pp fmt "%a%(%)%a" pv v assoc E.print k;
                 pp fmt aright;
                 false
               end
             end map true |> ignore;
             pp fmt right)
end

(* --- *)

module type ORDERED_HASHABLE_TYPE = sig
  include Helpers.ORDERED_TYPE
  include Hashtbl.HashedType with type t := t
end

module PairOf (T: ORDERED_HASHABLE_TYPE) :
sig
  type t = T.t * T.t
  include ORDERED_HASHABLE_TYPE with type t := t
  val make: T.t -> T.t -> t
  val mem: T.t -> t -> bool
  val is_sym: t -> bool
  type fmt = (T.t, T.t) pair_fmt
  val print': ?format:fmt -> t PPrt.pp
end
  =
struct
  type t = T.t * T.t
  let compare: t -> t -> int = cmp_pair T.compare T.compare
  let equal: t -> t -> bool = fun a b -> a == b || compare a b = 0
  let hash: t -> int = hash_pair T.hash T.hash
  let make a b = a, b
  let mem x (a, b) = a == x || b == x || T.equal a x || T.equal b x
  let is_sym: t -> bool = fun (a, b) -> a == b || T.compare a b = 0
  type fmt = (T.t, T.t) pair_fmt
  let print' ?(format: fmt option) = pp_pair ?format T.print T.print
  let print fmt = print' fmt
end

module OrderedPairOf (T: ORDERED_HASHABLE_TYPE) = struct
  include PairOf (T)
  let make a b = if T.compare a b <= 0 then make a b else make b a
end

module type FILTER_TO_CONST = sig
  type t
  type param
  type const
  val filter: param -> t -> const option
end

module FilteredValues
  (T: ORDERED_HASHABLE_TYPE)
  (F: FILTER_TO_CONST with type t = T.t) :
sig
  type t = private T.t
  include ORDERED_HASHABLE_TYPE with type t := t
  type v = Value of t | Const of F.const
  val make: F.param -> T.t -> v
  val get: t -> T.t
end
  =
struct
  include T
  type v =
    | Value of t
    | Const of F.const
  let make p a = match F.filter p a with
    | None -> Value a
    | Some c -> Const c
  let get x = x
end

(* --- *)

module String = struct
  include String
  let print = Format.pp_print_string
  let hash = Hashtbl.hash
end

module Strings = struct
  include MakeSet (String)
  exception Found of elt
  let find_first_opt p s =
    try iter begin fun e -> if p e then raise (Found e) end s; None with
      | Found e -> Some e
end
module StrMap = MakeMap (String)

(* --- *)

module Integer = struct
  type t = int
  let compare = (-)
  let print = pp_print_int
  let hash = Hashtbl.hash
  let equal = (==)
end
module Integers = MakeSet (Integer)

let fold_range first first_excl f =
  let rec x i acc = if i >= first_excl then acc else f i acc |> x (succ i) in
  x first

let fold_frange first step first_excl f =
  let rec x i acc = if i >= first_excl then acc else f i acc |> x (i +. step) in
  x first

(* --- *)

module type SEQ = sig
  type dont_care
  type random_access
  type (_,_) t =
      private
    | Array : 'a array -> ('a, _) t
    | List : 'a list -> ('a, dont_care) t
  type 'a any = ('a, dont_care) t
  type 'a for_ra = ('a, random_access) t

  val of_list: 'a list -> 'a any
  val of_array: 'a array -> 'a any
  val length: ('a, _) t -> int
  val map: ('a -> 'b) -> ('a, 'k) t -> ('b, 'k) t
  val mapi: (int -> 'a -> 'b) -> ('a, 'k) t -> ('b, 'k) t
  val iter: ('a -> unit) -> ('a, _) t -> unit
  val iteri: (int -> 'a -> unit) -> ('a, _) t -> unit
  val fold: ('x -> 'a -> 'x) -> 'x -> ('a, _) t -> 'x
  val fold_left: ('x -> 'a -> 'x) -> 'x -> ('a, _) t -> 'x
  val fold_right: ('a -> 'x -> 'x) -> ('a, _) t -> 'x -> 'x
  val fold_lefti: (int -> 'x -> 'a -> 'x) -> 'x -> ('a, _) t -> 'x
  val map_for_random_access: ('a -> 'b) -> 'b -> ('a, _) t
    -> 'b for_ra
  val mapi_for_random_access: (int -> 'a -> 'b) -> 'b -> ('a, _) t
    -> 'b for_ra
  val mapi_fold_left: ('x -> int -> 'a -> 'x * 'b) -> 'b -> 'x -> ('a, _) t
    -> 'b for_ra * 'x
  val mapi_fold_right: (int -> 'a -> 'x -> 'b * 'x) -> 'b -> ('a, _) t -> 'x
    -> 'x * 'b for_ra
  val mapi_fold_left': ('x -> int -> int -> 'a -> 'x * 'b option) -> 'b -> 'x -> ('a, _) t
    -> 'b for_ra * 'x
  val random_access: 'a for_ra -> int -> 'a
  val mutate: 'a for_ra -> int -> 'a -> unit
  val ra: 'a for_ra -> int -> 'a
end

module Seq : SEQ = struct

  type dont_care
  type random_access
  type (_,_) t =
    | Array : 'a array -> ('a, _) t
    | List : 'a list -> ('a, dont_care) t
  type 'a any = ('a, dont_care) t
  type 'a for_ra = ('a, random_access) t

  let of_list l = List l
  let of_array a : _ any = Array a (* XXX: a should not be mutated afterwards *)

  let length:
  type k. ('a, k) t -> int = function
    | Array a -> Array.length a
    | List l -> List.length l

  let map:
  type k. ('a -> 'b) -> ('a, k) t -> ('b, k) t = fun f -> function
    | Array a -> Array (Array.map f a)
    | List l -> List (List.map f l)

  let mapi:
  type k. (int -> 'a -> 'b) -> ('a, k) t -> ('b, k) t = fun f -> function
    | Array a -> Array (Array.mapi f a)
    | List l -> List (List.mapi f l)

  let iter:
  type k. ('a -> unit) -> ('a, k) t -> unit = fun f -> function
    | Array a -> Array.iter f a
    | List l -> List.iter f l

  let iteri:
  type k. (int -> 'a -> unit) -> ('a, k) t -> unit = fun f -> function
    | Array a -> Array.iteri f a
    | List l -> List.iteri f l

  let fold_left:
  type k. _ -> 'x -> ('a, k) t -> 'x = fun f i -> function
    | Array a -> Array.fold_left f i a
    | List l -> List.fold_left f i l

  let fold_lefti f acc s =
    snd (fold_left (fun (i, acc) s -> succ i, f i acc s) (0, acc) s)

  let fold_right:
  type k. _ -> ('a, k) t -> 'x -> 'x = fun f -> function
    | Array a -> Array.fold_right f a
    | List l -> List.fold_right f l

  let fold = fold_left

  let map_for_random_access:
  type k. ('a -> 'b) -> 'b -> ('a, k) t -> 'b for_ra = fun f i -> function
    | Array a -> Array (Array.map f a)
    | List l -> (let a = Array.make (List.length l) i in
                List.iteri (fun i s -> a.(i) <- f s) l; Array a)

  let mapi_for_random_access:
  type k. (int -> 'a -> 'b) -> 'b -> ('a, k) t -> 'b for_ra = fun f i ->
    function
      | Array a -> Array (Array.mapi f a)
      | List l -> (let a = Array.make (List.length l) i in
                  List.iteri (fun i s -> a.(i) <- f i s) l; Array a)

  let mapi_fold_left:
  type k. ('x -> int -> 'a -> 'x * 'b) -> 'b -> 'x -> ('a, k) t -> 'b for_ra * 'x =
      fun f i acc a ->
        let a' = Array.make (length a) i in
        let f (acc, i) e = let acc, e' = f acc i e in a'.(i) <- e'; acc, i+1 in
        Array a', match a with
          | Array a -> Array.fold_left f (acc, 0) a |> fst
          | List l -> List.fold_left f (acc, 0) l |> fst

  let mapi_fold_left':
  type k. ('x -> int -> int -> 'a -> 'x * 'b option) -> 'b -> 'x -> ('a, k) t -> 'b for_ra * 'x =
      fun f i acc a ->
        let a' = Array.make (length a) i in
        let f (acc, i, i') e =
          let acc, e' = f acc i i' e in
          acc, i+1, (match e' with Some e' -> a'.(i') <- e'; i'+1 | None -> i')
        in
        let acc, _, new_len = match a with
          | Array a -> Array.fold_left f (acc, 0, 0) a
          | List l -> List.fold_left f (acc, 0, 0) l
        in
        Array (Array.sub a' 0 new_len), acc

  let mapi_fold_right:
  type k. (int -> 'a -> 'x -> 'b * 'x) -> 'b -> ('a, k) t -> 'x -> 'x * 'b for_ra =
      fun f i a acc ->
        let len = length a in
        let a' = Array.make len i in
        let f e (acc, i) = let e', acc = f i e acc in a'.(i) <- e'; acc, i-1 in
        (match a with
          | Array a -> Array.fold_right f a (acc, len) |> fst
          | List l -> List.fold_right f l (acc, len) |> fst),
        Array a'

  let random_access: 'a for_ra -> int -> 'a = function
    | Array a -> fun i -> a.(i)

  let mutate: 'a for_ra -> int -> 'a -> unit = function
    | Array a -> fun i e -> a.(i) <- e

  let ra = random_access

end

(* --- *)

module type MUTABLE_STACK = sig
  type 'a t
  val init: unit -> 'a t
  val push: 'a t -> 'a -> unit
  val is_empty: 'a t -> bool
  val save_n_restore: 'a t -> ('b -> 'c) -> 'b -> 'c
  val to_list: 'a t -> 'a list
end

module MutableStack: MUTABLE_STACK = struct
  type 'a t = 'a list ref
  let init () = ref []
  let push s a = s := a :: !s
  let is_empty s = !s == []
  let save_n_restore ({ contents = x } as s) f a = let r = f a in s := x; r
  let to_list s = List.rev !s
end

(* --- *)

type 'x expected = { got: 'x; expected: 'x; }

(* --- *)
