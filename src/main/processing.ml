open Format
open Filename
open Rutils
open Rutils.IO

(* --- *)

let pp = fprintf
let pp_filename ~logger fmt f =
  if Log.check_level logger Log.Debug
  then pp_print_string fmt f
  else match dirname f with
    | "." -> pp_print_string fmt f
    | _ -> pp fmt ".../%s" (basename f)

(* --- *)

let error ?cont f =
  let maybecont fmt =
    pp_print_newline fmt ();
    (* XXX: use an arbitrary error exit code that is different from 1 as for now
       this code is emited on out-of-mem abort by CUDD. *)
    match cont with None -> exit 42 | Some c -> c ()
  in
  pp err_formatter "Error: @[";
  kfprintf maybecont err_formatter f
let warn f =
  pp err_formatter "Warning: @[";
  kfprintf (fun fmt -> pp_print_newline fmt ()) err_formatter f
let info f =
  pp err_formatter "Info: @[";
  kfprintf (fun fmt -> pp_print_newline fmt ()) err_formatter f

let abort ?filename n msgs =
  CtrlNbac.IO.report_msgs ?filename err_formatter msgs;
  error "Aborting due to errors in %(%)" n

(* --- *)

let mk_out_from f =
  try chop_extension f |> mk_fmt with
    | Invalid_argument _ when f <> "" && f <> "-" -> mk_fmt f
    | Invalid_argument _ -> mk_std_fmt

let mk_out_direct = function
    | "" | "-" -> mk_std_fmt
    | f -> mk_fmt' f

(* --- *)

let mk_bin_out_from f =
  try chop_extension f |> mk_out_bin with
    | Invalid_argument _ when f <> "" && f <> "-" -> mk_out_bin f
    | Invalid_argument _ ->
        error "@[Unable@ to@ determine@ filename@ for@ binary@ output.@]"

let mk_bin_out_direct = mk_out_bin'

(* --- *)

let straighten_if_tty oc fo cols =
  if Unix.isatty (Unix.descr_of_out_channel oc) then
    pp_set_margin fo cols

let _ =
  let columns = try int_of_string (Sys.getenv "COLUMNS") with _ -> 180 in
  straighten_if_tty stderr err_formatter columns;
  straighten_if_tty stdout std_formatter columns;
  Realib.BddapronUtil.prime_suffix := "\""
