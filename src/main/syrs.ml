open Format
open Filename
open Rutils
open Processing
open ScgsToolChain

(* -------------------------------------------------------------------------- *)

let level = Log.Debug3
let logger = Log.mk ~level "Main";;
Log.globallevel := Log.Info;;
Printexc.record_backtrace true;;

(* Enable synchronous variable reordering by default *)
ReaTKBridge.Supra.enable_reorderings := true;;

(* Enable dynamic/asynchronous variable reordering by default *)
Realib.CuddUtil.Options.enable_dynamic_reordering := true;;

(* -------------------------------------------------------------------------- *)
(* Options: *)

let args = ref []
let show_stats = ref true

(* --- *)

(** Utility to warn about deprecated file extensions *)
let deprext f = function
  | _, (x, None) -> x
  | e, (x, Some (descr, e')) ->
      warn "@[Deprecated extension@ `.%s'@ for@ %(%):@ use@ `.%s'@ instead.\
            @]" e descr e'; x

(* --- *)

(** File extensions officially understood for input, with associated input
    types. *)
let ityps_alist = [
  "classes", (`Classes, None);
  "classes_bin", (`ClassesBin, None);
  "stms", (`Stms, None);
  "meth", (`Meth, None);
  "secstubs", (`SecStubs, None);
  "secsum_bin", (`SecSumBin, None);
  "secsums_bin", (`SecSumsBin, None);
  "meth_files", (`MethFiles, None);
  "cgwalk_bin", (`CGWalkBin, None);
]

(** Name of official input types as understood by the tool. *)
let ityps = List.map fst ityps_alist

(** Deprecated input file extensions. *)
let ityps_alist = ityps_alist @ PPrt.[
  "secsums",
  (`SecStubs, Some (("security@ summary@ stubs": ufmt), "secstubs"));
  "secsig_bin",
  (`SecSumBin, Some (("security@ summary@ in@ binary@ format": ufmt),
                     "secsum_bin"));
  "secsigs_bin",
  (`SecSumsBin, Some (("security@ summaries@ in@ binary@ format": ufmt),
                      "secsums_bin"));
]

let bad_input_type t =
  raise (Arg.Bad (asprintf "Unknown input file type: `%s'" t))
let unk_input_type_of f =
  raise (Arg.Bad (asprintf "Cannot guess type of input `%s'" f))

let input_type
    : [ `Classes | `ClassesBin | `Stms | `Meth | `SecStubs
      | `SecSumBin | `SecSumsBin | `MethFiles
      | `CGWalkBin ] option ref
    = ref None

let set_input_type t =
  try input_type := Some (fst (List.assoc t ityps_alist)) with
    | Not_found -> bad_input_type t

let guessityp f =
  try match !input_type with
    | Some t -> t
    | None -> List.find (fun (s, _) -> check_suffix f s) ityps_alist |> deprext f
  with
    | Not_found -> unk_input_type_of f

(* --- *)

(** File extensions officially understood for output, with associated output
    types. *)
let otyps_alist = [
  "classes_bin", (`ClassesBin, None);
  "secsums", (`SecSums, None);
  "secsums_bin", (`SecSumsBin, None);
  "cgwalk_bin", (`CGWalkBin, None);
  "class_stats", (`ClassStats, None);
  "meth_stats", (`MethStats, None);
]

(** Name of official input types as understood by the tool. *)
let otyps = List.map fst otyps_alist

(** Deprecated output file extensions. *)
let otyps_alist = otyps_alist @ PPrt.[
  "secsigs",
  (`SecSigs, Some (("security@ summaries": ufmt), "secsums"));
  "secsigs_bin",
  (`SecSigsBin, Some (("security@ summaries@ in@ binary@ format": ufmt),
                      "secsums_bin"));
]

let bad_output_type t =
  raise (Arg.Bad (asprintf "Unknown output file type: `%s'" t))
let unk_output_type_of = function
  | "-" -> `SecSums, Some "-"
  | f -> raise (Arg.Bad (asprintf "Cannot guess type of output `%s'" f))

type ot = [ `ClassesBin | `SecSums | `SecSumsBin | `CGWalkBin
          | `ClassStats | `MethStats ]
type ot' = [ ot | `SecSigs | `SecSigsBin ]
let output_type: ot' option ref = ref None

let set_output_type t =
  try output_type := Some (fst (List.assoc t otyps_alist)) with
    | Not_found -> bad_output_type t

let guessotyp f =
  try match !output_type with
    | Some t -> t, None
    | None ->(List.find (fun (s, _) -> check_suffix f s) otyps_alist |> deprext f,
             Some f)
  with
    | Not_found -> unk_output_type_of f

(* --- *)

let input f =
  args :=
    (match guessityp f with
      | `Classes -> `Classes f
      | `ClassesBin -> `ClassesBin f
      | `Stms -> `Stms f
      | `Meth -> `Meth f
      | `SecStubs -> `SecStubs f
      | `SecSumBin -> `SecSumBin f
      | `SecSumsBin -> `SecSumsBin f
      | `MethFiles -> `MethFiles f
      | `CGWalkBin -> `CGWalkBin f) ::
    !args

(* --- *)

let output f =
  args :=
    (match guessotyp f with
      | `ClassesBin, Some f -> `OutClassesBin (mk_bin_out_from f)
      | `ClassesBin, None -> `OutClassesBin (mk_bin_out_direct f)
      | `SecSigs, Some f -> `OutSecSigs (mk_out_from f)
      | `SecSigs, None -> `OutSecSigs (mk_out_direct f)
      | `SecSums, Some f -> `OutSecSums (mk_out_from f)
      | `SecSums, None -> `OutSecSums (mk_out_direct f)
      | `SecSigsBin, Some f -> `OutSecSigsBin (mk_bin_out_from f)
      | `SecSigsBin, None -> `OutSecSigsBin (mk_bin_out_direct f)
      | `SecSumsBin, Some f -> `OutSecSumsBin (mk_bin_out_from f)
      | `SecSumsBin, None -> `OutSecSumsBin (mk_bin_out_direct f)
      | `CGWalkBin, Some f -> `OutCGWalkBin (mk_bin_out_from f)
      | `CGWalkBin, None -> `OutCGWalkBin (mk_bin_out_direct f)
      | `ClassStats, Some f -> `OutClassStats (mk_out_from f)
      | `ClassStats, None -> `OutClassStats (mk_out_direct f)
      | `MethStats, Some f -> `OutMethStats (mk_out_from f)
      | `MethStats, None -> `OutMethStats (mk_out_direct f)) ::
    !args

(* --- *)

let compute x = args := x :: !args

(* --- *)

let bad_indents_flavor t =
  raise (Arg.Bad (asprintf "Unknown flavor for identifiers: `%s'" t))

let set_idents_flavor = function
  | "java" -> args := `LexerIdentsFlavor OOLang.Lexer.Java :: !args
  | "jimple" -> args := `LexerIdentsFlavor OOLang.Lexer.Jimple :: !args
  | f -> bad_indents_flavor f

(* --- *)

let ignored_assoc k d v acc =
  warn "Ignoring@ item@ `%s%s%s'@ in@ flags@ list." k d v; acc

let flags_delim_regexp = Str.regexp "[-+,=]"

let acc_flags ?(assoc = ignored_assoc) flag =
  let open Str in
  let rec acc_flags acc = function
    | [] -> acc
    | Text k :: Delim ("=" as d) :: Text v :: tl
      -> acc_flags (assoc (String.trim k) d (String.trim v) acc) tl
    | Delim "+" :: Text f :: tl | Text f :: tl
      -> acc_flags (flag (String.trim f) true acc) tl
    | Delim "-" :: Text f :: tl
      -> acc_flags (flag (String.trim f) false acc) tl
    | Delim "," :: tl
      -> acc_flags acc tl
    | Delim d :: tl
      -> warn "Ignoring@ delimter@ `%s'@ in@ flags@ list." d; acc_flags acc tl
  in
  fun flgs -> args := acc_flags !args (full_split flags_delim_regexp flgs)

let deprecated_flag f e ?(b = true) f' =
  warn "Deprecated@ %(%)@ flag@ `%s%s':@ use@ %(%)@ instead."
    f (if b then "" else "-") e f'

(* --- *)

let typing_flag f b = match f with
  | "unsafe_interfaces" -> List.cons (`SafeInterfaces (not b))
  | "safe_interfaces" -> List.cons (`SafeInterfaces b)
  | f -> error "Unknown@ typing@ flag@ `%s'." f

let unknown_assoc k v =
  error "Unknown@ value@ `%s'@ for@ typing@ property@ `%s'." v k

let typing_assoc k _ v = match k with
  | "interfaces"
    -> List.cons (match OptHelpers.interfaces v with
      | Ok v -> v
      | Error v -> error "Unknown@ specification@ `%s'@ for@ handling@ casts@ \
                         against@ interfaces,@ given@ for@ typing@ property@ \
                         `%s'." v k)
  | _ -> error "Unknown@ typing@ property@ `%s'." k

let typing_flags = acc_flags ~assoc:typing_assoc typing_flag

(* --- *)

let deprof e = deprecated_flag "out" e

let out_flag f b = match f with
  | "irel" -> List.cons (`OutIRel b)
  | "arel" -> List.cons (`OutARel b)
  | "farel" -> List.cons (`OutFARel b)
  | "tfa" -> List.cons (`OutTFA b)
  | "cfg" -> List.cons (`OutCFG b)
  | "cdrs" -> List.cons (`OutCDRs b)
  | "scfg" -> List.cons (`OutSCFG b)
  | "ctrlg" -> List.cons (`OutCtrlg b)
  | "ctrlf" -> List.cons (`OutCtrlf b)
  | "secsig" -> deprof ~b f "`secsum'"; List.cons (`OutSecSum b)
  | "secsig_bin" -> deprof ~b f "`secsum_bin'"; List.cons (`OutSecSumBin b)
  | "secsum" -> List.cons (`OutSecSum b)
  | "secsum_bin" -> List.cons (`OutSecSumBin b)
  | "guards" -> List.cons (`OutGuards b)
  | "monctrln" -> List.cons (`OutMonCtrln b)
  | "monnode" -> List.cons (`OutMonNode b)
  | "monauto" -> List.cons (`OutMonAuto b)
  | "rmoncfg" -> List.cons (`OutRMonCfg b)
  | "*rel" -> List.append [`OutIRel b; `OutARel b; `OutFARel b; `OutTFA b]
  | "*cfg" -> List.append [`OutCFG b; `OutSCFG b]
  | "*_bin" -> List.append [`OutSecSumBin b]
  | "secsig*" -> deprof ~b f "`secsum*'"; List.append [`OutSecSum b;
                                                      `OutSecSumBin b]
  | "secsum*" -> List.append [`OutSecSum b; `OutSecSumBin b]
  | "mon*" -> List.append [`OutMonCtrln b; `OutMonNode b; `OutMonAuto b]
  | "*" | "all" -> List.append [`OutIRel b; `OutARel b; `OutFARel b; `OutTFA b;
                               `OutCFG b; `OutCDRs b; `OutSCFG b;
                               `OutCtrlg b; `OutCtrlf b;
                               `OutSecSum b; `OutSecSumBin b; `OutGuards b;
                               `OutMonCtrln b; `OutMonNode b; `OutMonAuto b]
  | f -> error "Unknown@ out@ flag@ `%s'." f

let out_flags = acc_flags out_flag

(* --- *)

let deprtf e = deprecated_flag "translation" e
let deprprop x ?b e = deprtf e ("`propagation="^^x^^"'") ?b
let deprprop' = deprprop ~b:false

let trans_flag f b = match f with
  | "levels" when b -> List.cons (`TrackLevels `All)
  | "levels" when not b -> List.cons (`TrackLevels `None)
  | "implicit_levels" when b -> List.cons (`TrackLevels `All)
  | "implicit_levels" when not b -> List.cons (`TrackLevels `Explicit)
  | "enforce_confidentiality"                                        (* alias *)
  | "enforce_privacy" -> List.cons (`EnforcePrivacy b)
  | "taints" -> List.cons (`TrackTaints b)
  | "enforce_integrity" -> List.cons (`EnforceIntegrity b)
  | "exceptions" -> List.cons (`TrackExceptions b)
  | "evil_statics" -> List.cons (`EvilStatics b)
  | "sink_statics" -> List.cons (`SinkStatics b)
  | "nu_prop" when b -> deprprop "nu" f; List.cons (`Propagation `Nu)
  | "const_prop" when b -> deprprop "const" f; List.cons (`Propagation `Consts)
  | "full_prop" when b -> deprprop "full" f; List.cons (`Propagation `Full)
  | "nu_prop" | "const_prop"
  | "full_prop" -> deprprop' "disable" f; List.cons (`Propagation `None)
  | "*" | "all" -> List.append [`TrackLevels (if b then `All else `None);
                               `EnforcePrivacy b;
                               `EnforceIntegrity b;
                               `TrackExceptions b;
                               `EvilStatics b;
                               `SinkStatics b;]
  | f -> error "Unknown@ translation@ flag@ `%s'." f

let unknown_assoc k v =
  error "Unknown@ value@ `%s'@ for@ translation@ property@ `%s'." v k

let trans_assoc k _ v = match k with
  | "heap" | "heapdom" | "hdom"
    -> List.cons (`HeapDom (match OptHelpers.heapdom v with
      | Ok v -> v
      | Error v -> error "Unknown@ abstract@ heap@ domain@ `%s',@ given@ for@ \
                         translation@ property@ `%s'." v k))
  | "effects" | "effects_inference"
    -> List.cons (`EffectsInference (match OptHelpers.effects_inference v with
      | Ok v -> v
      | Error v -> error "Unknown@ effects@ inference@ mechanism@ `%s',@ given@ \
                         for@ translation@ property@ `%s'." v k))
  | "semloc" | "semloc_encoding"
    -> List.cons (`SemLocEncoding (match OptHelpers.semloc_encoding v with
      | Ok v -> v
      | Error v -> error "Unknown@ encoding@ `%s'@ for@ semantic@ locations,@ \
                         given@ for@ translation@ property@ `%s'." v k))
  | "trans_merge" | "merge_trans"
    -> List.cons (`TransMerge (match OptHelpers.trans_merge v with
      | Ok v -> v
      | Error v -> error "Unknown@ policy@ `%s'@ for@ merging@ transitions,@ \
                         given@ for@ translation@ property@ `%s'." v k))
  | "prop" | "propagation"
    -> List.cons (`Propagation (match OptHelpers.prop v with
      | Ok v -> v
      | Error v -> unknown_assoc k v))
  | "levels"
    -> List.cons (`TrackLevels (match v with
      | "none" | "no" -> `None
      | "implicit" | "all" -> `All
      | "explicit" -> `Explicit
      | v -> error "Unknown@ `%s'@ for@ tracked@ confidentiality@ levels,@ \
                   given@ for@ translation@ property@ `%s'." v k))
  | "heuristics" | "expr_heuristics" | "exprh"
    -> List.cons (`ExprHeuristics (match OptHelpers.expr_heuristics v with
      | Ok v -> v
      | Error v -> unknown_assoc k v))
  | _ -> error "Unknown@ translation@ property@ `%s'." k

let trans_flags = acc_flags ~assoc:trans_assoc trans_flag

(* --- *)

let set_methskip_condition i =
  args := `MethSkipCondition (if i >= 0 then `StateVars i else `None) :: !args

(* --- *)

let proc_flag f b = match f with
  | f -> error "Unknown@ process@ option@ flag@ `%s'." f

let proc_assoc k _ v = match k with
  | "sigint"
    -> List.cons (`ProcOnSigint (match OptHelpers.sigint v with
      | Ok v -> v
      | Error v -> error "Unknown@ behavior@ specification@ `%s'@ for@ manual@ \
                         interruptions,@ given@ for@ process@ option@ \
                         property@ `%s'." v k))
  | "timeout"
    -> List.cons (`ProcOnTimout (match OptHelpers.timeout v with
      | Ok v -> v
      | Error v -> error "Invalid@ timout@ specification@ `%s',@ given@ for@ \
                         process@ option@ property@ `%s'." v k))
  | _ -> error "Unknown@ process@ option@ property@ `%s'." k

let proc_flags = acc_flags ~assoc:proc_assoc proc_flag

(* --- *)

let synth_algo a =
  args := `SynthAlgoSpec (match a with
    | "" -> !ReaTKBridge.Supra.default_synth_algo
    | a -> a) :: !args

let synth_flag f b = match f with
  | f -> error "Unknown@ synthesis@ flag@ `%s'." f

let synth_assoc k _ v = match k with
  | "sumcomp" | "summary_computation"
    -> List.cons (`SumComp (match OptHelpers.sumcomp v with
      | Ok v -> v
      | Error v -> error "Invalid@ specification@ of@ summary@ computation@ \
                         algorithm@ `%s',@ given@ for@ property@ `%s'." v k))
  | _ -> error "Unknown@ process@ option@ property@ `%s'." k

let synth_flags = acc_flags ~assoc:synth_assoc synth_flag

(* --- *)

let cudd_flag f b = match f with
  | "r" when not b -> List.append [`Reorder false; `DynReorder false]
  | "dr" when b -> List.append [`Reorder true; `DynReorder true]
  | "r" -> List.cons (`Reorder true)
  | "dr" -> List.cons (`DynReorder false)
  | "*" | "all" -> List.append [`Reorder b; `DynReorder b]
  | f -> error "Unknown@ Cudd/Realib@ flag@ `%s'." f

let cudd_assoc k _ v = match k with
  | "cuddmax" | "cuddmaxmem" | "cuddmem"
    -> List.cons (`CuddMaxMem (match OptHelpers.memory v with
      | Ok v -> v
      | Error v -> error "Invalid@ memory@ limit@ specification@ `%s',@ \
                         given@ for@ CUDD@ option@ property@ `%s'." v k))
  | _ -> error "Unknown@ process@ option@ property@ `%s'." k

let cudd_flags = acc_flags ~assoc:cudd_assoc cudd_flag

(* --- *)

let log_flag f = match String.lowercase_ascii f with
  | "w" | "warn" | "warning" | "warnings" -> List.cons (`Log Log.Warn)
  | "i" | "info" -> List.cons (`Log Log.Info)
  | "d" | "d1" | "debug" | "debug1" -> List.cons (`Log Log.Debug)
  | "d2" | "debug2" -> List.cons (`Log Log.Debug2)
  | "d3" | "debug3" -> List.cons (`Log Log.Debug3)
  | _ -> error "Unknown@ Log@ level@ flag@ `%s'." f

let log_flag f = args := log_flag f !args

(* --- *)

exception Help
let usage = "Usage: syrs [options] [ -- ] [ <classes> ] { <stms> | <meth> }\
\n\
\nOptions that require a <flags> argument accept a (comma-)separated list of \
flags\
\nconsisting of an optional `+' or `-' sign followed by one of the items \
listed\
\nfor the corresponding option. Unsigned flags act as if they were prefixed\
\nwith a `+'.  Such options that additionally admit properties undestand \
\nassignments of the form `key=value' or `key:value' as part of the list of \
flags.\
\n"
let print_vers () =
  fprintf std_formatter "syrs-%s (Compiled on %s, %s)@." Version.str
    Version.compile_host Version.compile_time;
  exit 0
let anon = input
let u = Arg.Unit (fun _ -> ())
let e s = ("", u, " "^s)
let t s = ("", u, " \ro __"^s^"__")
let h = Arg.Unit (fun _ -> raise Help)
let v = Arg.Unit print_vers
let options = Arg.align
  [
    e""; t"Input/Output"; e"";

    ("-i", Arg.String anon, "<file> ");
    ("--input", Arg.String anon, "<file> Input file");
    ("--input-type", Arg.Symbol (ityps, set_input_type),
     " Type of next input files (deduced from extension otherwise)");

    ("-o", Arg.String output, "<file> ");
    ("--output", Arg.String output, "<file> Output symbolic \
      security signatures in <file>");
    ("-ot", Arg.Symbol (otyps, set_output_type), " ");
    ("--output-type", Arg.Symbol (otyps, set_output_type),
     " Type of next output files (deduced from extension otherwise)");
    ("--no-stats", Arg.Clear show_stats,
     " Do not print statistics upon termination");

    ("--", Arg.Rest anon, " Treat all remaining arguments as input files");

    e""; t"Lexing & Parsing"; e"";

    ("--idents", Arg.Symbol (["java"; "jimple"], set_idents_flavor), " \
     Select flavor of identifiers (default is `jimple')");

    e""; t"Typing Features"; e"";

    ("--typing", Arg.String typing_flags, "<flags> Enable/disable some \
      typing features, to be specified BEFORE");
    e"any `.classes' file. Accepted properties are:";
    e"- interfaces: select the policy for handling potential";
    e"  aliasing w.r.t interfaces:";
    e"  + unsafe: assume no unsafe cast against interface types;";
    e"    this may reduce the amount of variables related to";
    e"    aliasing in generated SCFGs;";
    e"  + safe: safely support casts against interface types.";

    e""; t"SCFG Generation"; e"";

    ("-tf", Arg.String trans_flags, "<flags> ");
    ("--tflags", Arg.String trans_flags, "<flags> Enable/disable some \
      features impacting SCFG generation, or");
    e"specify some properties. Accepted flags are:";
    e"- levels: enable tracking of security levels (enabled by";
    e"  default --- `-' implies `-enforce_privacy');";
    e"- enforce_privacy: confidentiality guards generation";
    e"  (enabled by default --- `+' implies `+levels');";
    e"- taints: enable tracking of taints (disabled by default ---";
    e"  `-' implies `-enforce_integrity');";
    e"- enforce_integrity: integrity guards generation (enabled by";
    e"  default --- `+' implies `+taints');";
    e"- exceptions: enable tracking of exceptions (disabled by";
    e"  default);";
    e"- evil_statics: always treat static fields as holding high";
    e"  or tainted values (disabled by default);";
    e"- sink_statics: treat static stores as sinks (disabled by;";
    e"  default);";
    e"- * all: all of `enforce_privacy', `enforce_integrity',";
    e"  `exceptions'.";
    e"Accepted properties are:";
    e"- heapdom (or hdom): symbolic abstract heap domain:";
    e"  + deepalias (the default): combination of may-alias and";
    e"    deep reference field aliasing, plus per-object types;";
    e"  + connect: (equivalent to sharing---see below);";
    e"  + shallow: may-alias equivalence relation only;";
    e"  + dumb: no heap-related relation, only object types;";
    e"  + share: may-share equivalence relation, which connects";
    e"    references if they may reach a common set of objects;";
    e"  + ashare: a combination of may-alias with a may-field-share";
    e"    relation, whcih connects r with s if a field of r";
    e"    may-share with s;";
    e"- effects (or effects_inferrence): approach for inferring";
    e"  method effects:";
    e"  + coreach_only (the default): use a standard coreachability";
    e"    analysis, and a direct triangularization on state";
    e"    variables;";
    e"  + preamble: use a preamble transition and standard";
    e"    controller triangulatization;";
    e"- semloc_encoding (or semloc): algorithm for encoding";
    e"  semantic locations:";
    e"  + explicit: explicitly include the junction step and high";
    e"    CDR as part of SCFG locations;";
    e"  + mixed (the default): explicitly include the junction step";
    e"    as part of SCFG locations, and encode the high CDR";
    e"    symbolically;";
    e"  + symbolic: symbolically encode both the junction step and";
    e"    high CDR;";
    e"- trans_merge: policy for merging transitions between";
    e"  equivalent locations in generated SCFGs:";
    e"  + always (the default): always merge transitions;";
    e"  + loops: merge self-loop transitions only;";
    e"  + nonloops: merge every but self-loop transitions;";
    e"  + never: do not merge any transition;";
    e"- propagation (or prop): policy for post-processing generated";
    e"  SCFGs:";
    e"  + full (the default): fully propagate expressions;";
    e"  + consts: propagate constants only;";
    e"  + none: disable all propagations;";
    e"- expr_heuristics (or exprh): heuristics for simplifying";
    e"  expressions:";
    e"  + lazy (the default): basic heuristics;";
    e"  + eager: enable slightly deeper simplifications.";

    e""; t"Walking Callgraphs"; e"";

    ("--full-walk", Arg.Unit (fun () -> compute `CGFullWalk),
     " Process all methods of the callgraph");
    ("--walk-step", Arg.Unit (fun () -> compute `CGWalkStep),
     " Process one more method from the callgraph");
    ("--walk-skip", Arg.Unit (fun () -> compute `CGWalkSkip),
     " Skip next method from the callgraph");
    ("--walk-info", Arg.Unit (fun () -> compute `CGWalkInfo), " \
      Show some information about the current stage of callgraph");
    e"walking process";
    ("--safe-walk", Arg.Unit (fun () -> compute `CGSafeCrawl),
     " Failsafe processing of all methods in the callgraph");

    e""; t"Method Skipping"; e"";

    ("--methskip-cond", Arg.Int set_methskip_condition,
     " Set an upper-threshold on the number of state variables in");
    e"SCFGs above which methods are considered third-party.";

    e""; t"Intermediate Outputs & Inspection"; e"";

    ("-of", Arg.String out_flags, "<flags> ");
    ("--oflags", Arg.String out_flags, "<flags> Enable/disable outputting some \
      intermediary files:");
    e"- irel: inheritance relation (without arrays);";
    e"- arel: potential aliasing relation (without arrays);";
    e"- farel: field-alias relation;";
    e"- tfa: transitive field-access relation;";
    e"- cfg: raw control-flow graph;";
    e"- cdrs: raw control-flow graph, annotated with (some) CDRs;";
    e"- scfg: symbolic control-flow graph in DOT format;";
    e"- ctrlg: symbolic control-flow graph in Controllable-Nbac";
    e"  format;";
    e"- ctrlf: symbolic security signature in Controllable-Nbac";
    e"  format;";
    e"- secsum: symbolic security summaries in readable format;";
    e"- secsum_bin: symbolic security summaries in binary format;";
    e"- guards: symbolic security guards in readable format;";
    e"- monauto: symbolic security monitor automaton in readable";
    e"  format;";
    e"- monnode: symbolic security monitor node in readable";
    e"  format;";
    e"- monctrln: symbolic security monitor node in";
    e"  Controllable-Nbac format;";
    e"- rmoncfg: symbolic security monitor automaton in internal";
    e"  Realib format;";
    e"- *rel *cfg *_bin secsum* mon*: whatever matches among the";
    e"  above flags;";
    e"- * all: all of the above (except `rmoncfg').";

    e""; t"CUDD & Realib"; e"";

    ("-rf", Arg.String cudd_flags, "<flags> ");
    ("--rflags", Arg.String cudd_flags, "<flags> Enable/disable some CUDD & \
      Realib features, or specify");
    e" customizable parameters. Acceped flags are:";
    e"- r: any variable reordering (enabled by default);";
    e"- dr: dynamic variable reordering (enabled by default);";
    e"- * all: all of the above.";
    e"Accepted properties are:";
    e"- cuddmaxmem (or cuddmem, cuddmax): amount of memory made";
    e"  available to CUDD (format is <integer><unit> where <unit>";
    e"  is one of B, K, KiB, M, MiB, G, GiB).";

    e""; t"Synthesis"; e"";

    ("-sf", Arg.String synth_flags, "<flags> ");
    ("--sflags", Arg.String synth_flags, "<flags> Specify synthesis \
      parameters. Only accepted property is:");
    e"- sumcomp (or summary_computation): specific algorithm";
    e"  to use for computing summaries:";
    e"  + monolithic (or full): do not decompose synthesis problems;";
    e"  + split (or fcsplit): decompose via partitioning of";
    e"    footprint variables;";
    e"  + default: use the default, which is `split';";
    ("-s", Arg.String synth_algo, "<algo> ");
    ("--synth", Arg.String synth_algo, asprintf "<algo> Specify synthesis \
      algorithm (default is `%s')" !ReaTKBridge.Supra.default_synth_algo);
    e"Possible options are, e.g,";
    e"- sB:rp={stop_dri=1,stop_ri=100,log}: use finite";
    e"  synthesis with given reordering-policy options;";
    e"- sS: use advanced double-fixpoint synthesis that";
    e"  operates directly on the control-flow graph, with;";
    e"  BBD-list-based power domain;";
    e"- sS:d={P:m}: use advanced double-fixpoint synthesis that";
    e"  operates directly on the control-flow graph, with";
    e"  MTBBD-based power domain.";

    e""; t"High-level Process Options & Manual Interventions"; e"";

    ("-po", Arg.String proc_flags, "<flags> ");
    ("-pf", Arg.String proc_flags, "<flags> ");
    ("--pflags", Arg.String proc_flags,
     "<flags> Tune some features pertained to interactive user");
    e"interventions on syrs's process. Accepted properties are:";
    e"- sigint: behavior upon user interrupt:";
    e"  + int, or interrupt: terminate the process;";
    e"  + skip: stop analyzing current method, and forge a";
    e"    third-party summary instead;";
    e"- timeout: none (the default), or <time specification>:";
    e"  analysis time before the method currently being analyzed";
    e"  is skipped and a third-party summary is used insead.";

    e""; t"Miscellaneous"; e"";

    ("-ll", Arg.String log_flag, "<l> ");
    ("--log-level", Arg.String log_flag, "<l> Set amount of \
      console output:");
    e"- `w' or `warn': show only warnings;";
    e"- `i' or `info': restrict logs to high-level information;";
    e"- `d1', `d2', `d3': show more and more in-depth output.";
    ("-V", v, " ");
    ("--version", v, " Display version info");
    ("-h", h, " ");
    ("-help", h, " ");
    ("--help", h, " Display this list of options");
  ]

let arror f = error ~cont:(fun () -> Arg.usage options usage; exit 1) f

(* -------------------------------------------------------------------------- *)

module ToolChain = Bundle (Processing)
open ToolChain

let handle_arg s = function
  | `Classes f -> new_classes (mk_out_from f) f s
  | `ClassesBin f -> input_classes_bin f s
  | `Stms f -> new_body Raw (mk_out_from f) (mk_bin_out_from f) f s
  | `Meth f -> new_body Meth (mk_out_from f) (mk_bin_out_from f) f s
  | `SecStubs f -> read_secstubs f s
  | `SecSumBin f -> input_secsum_bin f s
  | `SecSumsBin f -> input_secsums_bin f s
  | `MethFiles f -> read_meth_files_from (mk_out_from f) f s
  | `CGWalkBin f -> input_cgwalk_bin f s
  | `CGFullWalk -> cg_fullwalk s
  | `CGWalkStep -> cg_locksteps ~n:1 s
  | `CGWalkSkip -> cg_locksteps ~n:1 ~skip_next:`Forced s
  | `CGWalkInfo -> cg_stage_info s
  | `CGSafeCrawl -> cg_safe_crawl ?n:None s
  | `HeapDom h -> set_heapdom h s
  | `TrackLevels b -> set_track_levels b s
  | `TrackTaints b -> set_track_taints b s
  | `TrackExceptions b -> set_track_exceptions b s
  | `EnforcePrivacy b -> set_enforce_privacy b s
  | `EnforceIntegrity b -> set_enforce_integrity b s
  | `EvilStatics b -> set_evil_statics b s
  | `SinkStatics b -> set_sink_statics b s
  | `EffectsInference e -> set_effects_inference e s
  | `SemLocEncoding e -> set_semloc_encoding e s
  | `TransMerge e -> set_trans_merge e s
  | `ExprHeuristics e -> set_expr_heuristics e s
  | `MethSkipCondition c -> set_methskip_condition c s
  (* | `MethSkipPolicy p -> set_methskip_policy p s *)
  | `Propagation p -> set_prop_in_scfg p s
  | `ProcOnSigint o -> set_proc_sigint o s
  | `ProcOnTimout t -> set_proc_timeout t s
  | `CuddMaxMem m -> set_proc_cuddmaxmem m s
  | `OutIRel b -> { s with output_irel = b }
  | `OutARel b -> { s with output_arel = b }
  | `OutFARel b -> { s with output_farel = b }
  | `OutTFA b -> { s with output_tfa = b }
  | `OutCFG b -> { s with output_cfg = b }
  | `OutCDRs b -> { s with output_cdrs = b }
  | `OutSCFG b -> { s with output_scfg = b }
  | `OutCtrlg b -> { s with output_ctrlg = b }
  | `OutCtrlf b -> { s with output_ctrlf = b }
  | `OutSecSum b -> { s with output_secsum = b }
  | `OutSecSumBin b -> { s with output_secsum_bin = b }
  | `OutGuards b -> { s with output_guards = b }
  | `OutMonCtrln b -> { s with output_monctrln = b }
  | `OutMonNode b -> { s with output_monnode = b }
  | `OutMonAuto b -> { s with output_monauto = b }
  | `OutRMonCfg b -> { s with output_rmoncfg = b }
  | `OutClassesBin out_bin -> output_classes_bin out_bin s
  | `OutSecSigs out -> output_secsums ~suff:"secsigs" out s
  | `OutSecSums out -> output_secsums out s
  | `OutSecSigsBin out_bin -> output_secsums_bin ~suff:"secsigs_bin" out_bin s
  | `OutSecSumsBin out_bin -> output_secsums_bin out_bin s
  | `OutCGWalkBin out_bin -> output_cgwalk_bin (`RutilsOut out_bin) s
  | `OutClassStats out -> output_class_stats out s
  | `OutMethStats out -> output_meth_stats out s
  | `SafeInterfaces b -> set_safe_interfaces b s
  | `LexerIdentsFlavor f -> set_idents_flavor f s
  | `SynthAlgoSpec a -> { s with synth_algo = Some a }
  | `SumComp a -> { s with sumcomp_algo = a }
  | `Reorder b -> ReaTKBridge.Supra.enable_reorderings := b; s
  | `DynReorder b -> Realib.CuddUtil.Options.enable_dynamic_reordering := b; s
  | `Log l -> Log.globallevel := l; s

let handle_args = function
  | [] ->
      Log.w logger "@[No@ argument@ given@ on@ the@ command@ line:@ printing@ \
                      usage@ instructions.@]";
      Arg.usage options usage; exit 1
  | lst ->
      List.fold_left handle_arg ToolChain.init lst

(* --- *)

let main () =
  Arg.parse options anon usage;
  Log.i logger "@[This@ is@ syrs@ version@ %s.@]" Version.str;
  let s = handle_args (List.rev !args) in
  if !show_stats
  then print_stats s

let _ = try main () with
  | Help -> Arg.usage options usage; exit 0
  | Sys_error s -> error "%s" s
  | Exit -> abort "" []

(* -------------------------------------------------------------------------- *)
