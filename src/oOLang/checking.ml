open Utils
open Base
open Stm
open Typ
open Semantics
open Method

type 'l error =
  [
  | `MutatedArg of 'l * var
  ]

let pp_errors fmt =
  pp_lst ~fopen:"" ~fclose:"" ~fsep:"@\n" begin fun fmt (`MutatedArg (s, v)) ->
    pp fmt "Unsupported@ mutation@ of@ reference@ argument@ `%a' in statement@ \
            %a" Var.print v Stm.print_raw' s
  end fmt

module ArgConsts (Sem: Semantics.S) = struct

  let check sign body =
    let args = Sem.args sign in
    let rec nomut pc errs = function
      | AssignR (v, _)
      | LoadR (v, _, _)
      | SLoadR (v, _, _)
      | ALoadR (v, _, _)
      | MFCallR (v, _)
      | New (v, _)
      | ANew (v, _, _)
      | CastR (v, _, _)
      | CastA (v, _, _)
      | Catch v as s
        -> if VarMap.mem v args then `MutatedArg (s, v) :: errs else errs
      | Nop
      | AssignP _
      | LoadP _
      | SLoadP _
      | ALoadP _
      | StoreP _
      | StoreR _
      | AStoreP _
      | AStoreR _
      | SStoreP _
      | SStoreR _
      | ALen _
      | UFCall _
      | MFCallP _
      | InstOfR _
      | InstOfA _
      | CastP _
      | Goto _
      | CondGoto _
      | Switch _
      | ChkPoint _
      | OutputPR _
      | ReturnU
      | ReturnP _
      | ReturnR _
      | Throw _
      | MonEnter _
      | MonExit _
        -> errs
      | Label (_, s)
        -> nomut pc errs s
    in
    match Body.fold_stms nomut [] body with
      | [] -> Ok ()
      | errs -> Error (List.rev errs : _ Sem.stm error list)

end
