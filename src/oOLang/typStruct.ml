open Format
open Rutils
open Utils
open Typ
open Graph

(* -------------------------------------------------------------------------- *)

let logger = Log.mk ~level:Log.Debug "Typs"
(* module Log = struct include Log let d = i end *)

(* -------------------------------------------------------------------------- *)

module RefTyp = struct
  type t =
    | U : user typname -> t
    | A : arry typname -> t

  let compare a b = match a, b with
    | U a, U b -> UserTyp.compare a b
    | A a, A b -> ArryTyp.compare a b
    | U _, _ -> -1
    | _, U _ -> 1

  let equal a b = a == b || match a, b with
    | U a, U b -> UserTyp.equal a b
    | A a, A b -> ArryTyp.equal a b
    | _ -> false

  let hash = function
    | U a -> UserTyp.hash a
    | A a -> ArryTyp.hash a lsl 3

  let print fmt = function
    | U t -> UserTyp.print fmt t
    | A t -> ArryTyp.print fmt t
end
module RefTyps = MakeSet (RefTyp)
module RefTypMap = MakeMap (RefTyp)

(* -------------------------------------------------------------------------- *)

module type ALIAS_OPS = sig
  type reftyp
  val may_alias: reftyp -> reftyp -> bool
  val may_field_alias: reftyp -> reftyp -> bool
  val may_share_mutable: reftyp -> reftyp -> bool
  val may_field_share_mutable: reftyp -> reftyp -> bool
  val output_farel
    : ?backup: IO.backup_policy -> ?suff: string -> formatter IO.out -> unit
  val output_tfa
    : ?backup: IO.backup_policy -> ?suff: string -> formatter IO.out -> unit
end

(* --- *)

module type T = sig
  include OPS with type deftyp := UserTyp.t
  include FIELD_OPS with type rectyp := UserTyp.t
  include ALIAS_OPS with type reftyp := RefTyp.t
  module RefOps: OPS with type deftyp := RefTyp.t
  val mem_typ: UserTyp.t -> bool
end

module type FLAT = T

module type WALKS = sig
  type typ
  val fold_direct_suptyps: (typ -> 'a -> 'a) -> typ -> 'a -> 'a
  val fold_known_ultimate_subtyps: (typ -> 'a -> 'a) -> UserTyp.t -> 'a -> 'a
end

(* --- *)

module Stats = struct

  type ('n, 'r) tree_stats =
      {
        tc_nb_classes: 'n;
        tc_nb_interfaces: 'n;
        tc_cardinal: 'n;
        tc_alias_rel_size: 'n;
        tc_alias_rel_density: 'r;
        tc_field_alias_rel_size: 'n;
        tc_field_alias_rel_density: 'r;
        tc_field_access_rel_size: 'n;
        tc_field_access_rel_density: 'r;
      }

  let print_tree_stats pn pr fmt { tc_nb_classes;
                                   tc_nb_interfaces;
                                   (* tc_cardinal; *)
                                   tc_alias_rel_size;
                                   tc_alias_rel_density;
                                   tc_field_alias_rel_size;
                                   tc_field_alias_rel_density;
                                   tc_field_access_rel_size;
                                   tc_field_access_rel_density } =
    pp fmt "@[<2>nb_classes: %a@],@;" pn tc_nb_classes;
    pp fmt "@[<2>nb_interfaces: %a@],@;" pn tc_nb_interfaces;
    pp fmt "@[<2>alias_rel_size: %a@],@;" pn tc_alias_rel_size;
    pp fmt "@[<2>alias_rel_density: %a@],@;" pr tc_alias_rel_density;
    pp fmt "@[<2>field_alias_rel_size: %a@],@;" pn tc_field_alias_rel_size;
    pp fmt "@[<2>field_alias_rel_density: %a@],@;" pr tc_field_alias_rel_density;
    pp fmt "@[<2>field_access_rel_size: %a@],@;" pn tc_field_access_rel_size;
    pp fmt "@[<2>field_access_rel_density: %a@]" pr tc_field_access_rel_density

  open BasicStats.Aggreg

  type symmetric_rel_degree_stats =
      {
        rds_stats: (int, float) aggres;
      }

  type asymmetric_rel_degree_stats =
      {
        rds_in_stats: (int, float) aggres;
        rds_out_stats: (int, float) aggres;
      }

  type tfa_degree_stats =
      {
        tfa_in_stats: (int, float) aggres;
        tfa_out_stats: (int, float) aggres;
      }

  type tree_degree_stats =
      {
        tds_alias_rel_degree_stats: symmetric_rel_degree_stats;
        tds_field_alias_rel_degree_stats: asymmetric_rel_degree_stats;
        tds_field_access_rel_degree_stats: tfa_degree_stats;
      }

  open BasicStats.Outputs

  let print_degree_stats name fmt =
    pp fmt "%(%): %a" name (print_assoc_map (print_aggreg pi pf))

  let print_tree_degree_stats fmt { tds_alias_rel_degree_stats;
                                    tds_field_alias_rel_degree_stats;
                                    tds_field_access_rel_degree_stats } =
    pp fmt "%a,@;%a,@;%a,@;%a,@;%a"
      (print_degree_stats "alias_rel_degrees")
      tds_alias_rel_degree_stats.rds_stats
      (print_degree_stats "field_alias_rel_in_degrees")
      tds_field_alias_rel_degree_stats.rds_in_stats
      (print_degree_stats "field_alias_rel_out_degrees")
      tds_field_alias_rel_degree_stats.rds_out_stats
      (print_degree_stats "field_access_rel_in_degrees")
      tds_field_access_rel_degree_stats.tfa_in_stats
      (print_degree_stats "field_access_rel_out_degrees")
      tds_field_access_rel_degree_stats.tfa_out_stats

end

module type TREE = sig
  include T
  include WALKS with type typ := RefTyp.t
  val output_irel
    : ?backup: IO.backup_policy -> ?suff: string -> formatter IO.out -> unit
  val output_arel
    : ?backup: IO.backup_policy -> ?suff: string -> formatter IO.out -> unit
  val is_class: UserTyp.t -> bool
  val is_interface: UserTyp.t -> bool
  val field_alias_rel_indegree: RefTyp.t -> int
  val field_alias_rel_outdegree: RefTyp.t -> int
  val field_access_rel_indegree: RefTyp.t -> int
  val field_access_rel_outdegree: RefTyp.t -> int
  val stats: (int, float) Stats.tree_stats
  val alias_rel_degree_stats
    : quantiles: BasicStats.Aggreg.quantiles_spec
    -> Stats.symmetric_rel_degree_stats
  val field_alias_rel_degree_stats
    : quantiles: BasicStats.Aggreg.quantiles_spec
    -> Stats.asymmetric_rel_degree_stats
  val field_access_rel_degree_stats
    : quantiles: BasicStats.Aggreg.quantiles_spec
    -> Stats.tfa_degree_stats
  val degree_stats
    : quantiles: BasicStats.Aggreg.quantiles_spec
    -> Stats.tree_degree_stats
end

(* -------------------------------------------------------------------------- *)

module MakeIRelDotOutput
  (T: ORDERED_HASHABLE_TYPE)
  (G: Sig.G with type V.t = T.t) =
struct
  module Out = Graphviz.Dot (struct
    include G
    let vertex_name = asprintf "\"%a\"" T.print
    let graph_attributes _ = []
    let default_vertex_attributes _ = [ `Shape `Box ]
    let default_edge_attributes _ = []
    let vertex_attributes v = []
    let edge_attributes e = [ `Dir `Back ]
    let get_subgraph v = None
  end)
  open Rutils.IO
  let output g ?(backup = Forget) ?(suff = "-irel") out =
    let fmt, close =
      out.out_exec ~descr:"inheritance@ relation" ~backup ~suff "dot" in
    Out.fprint_graph fmt g;
    close ()
end

module MakeARelDotOutput
  (T: ORDERED_HASHABLE_TYPE)
  (G: Sig.G with type V.t = T.t) =
struct
  module Out = Graphviz.Dot (struct
    include G
    let vertex_name = asprintf "\"%a\"" T.print
    let graph_attributes _ = []
    let default_vertex_attributes _ = [ `Shape `Box ]
    let default_edge_attributes _ = []
    let vertex_attributes v = []
    let edge_attributes e = [ `Dir `None ]
    let get_subgraph v = None
  end)
  open Rutils.IO
  let output g ?(backup = Forget) ?(suff = "-arel") out =
    let fmt, close =
      out.out_exec ~descr:"assignability@ relation" ~backup ~suff "dot" in
    Out.fprint_graph fmt g;
    close ()
end

module MakeARelDirDotOutput
  (T: ORDERED_HASHABLE_TYPE)
  (G: Sig.G with type V.t = T.t) =
struct
  module Out = Graphviz.Dot (struct
    include G
    let vertex_name = asprintf "\"%a\"" T.print
    let graph_attributes _ = []
    let default_vertex_attributes _ = [ `Shape `Box ]
    let default_edge_attributes _ = []
    let vertex_attributes v = []
    let edge_attributes e = [ `Dir `Forward ]
    let get_subgraph v = None
  end)
  open Rutils.IO
  let output g ?(backup = Forget) ?(suff = "-arel") out =
    let fmt, close =
      out.out_exec ~descr:"assignability@ relation" ~backup ~suff "dot" in
    Out.fprint_graph fmt g;
    close ()
end

(* -------------------------------------------------------------------------- *)

module MakeFARelDotOutput
  (T: ORDERED_HASHABLE_TYPE)
  (G: Sig.G with type V.t = T.t) =
struct
  module Out = Graphviz.Dot (struct
    include G
    let vertex_name = asprintf "\"%a\"" T.print
    let graph_attributes _ = []
    let default_vertex_attributes _ = [ `Shape `Box ]
    let default_edge_attributes _ = []
    let vertex_attributes v = []
    let edge_attributes e = []
    let get_subgraph v = None
  end)
  open Rutils.IO
  let output g ?(backup = Forget) ?(suff = "-farel") out =
    let fmt, close =
      out.out_exec ~descr:"field-alias@ relation" ~backup ~suff "dot" in
    Out.fprint_graph fmt g;
    close ()
end

(* -------------------------------------------------------------------------- *)

module Flat (FieldAttrs: FIELD_ATTRS with type typspec = any) = struct

  module Ops = struct
    let ( ==: ) = UserTyp.equal
    let ( <: ) = ( ==: )                                      (* no subtyping *)
    let may_alias = ( ==: )
  end

  module RefOps = struct
    let ( ==: ) = RefTyp.equal
    let ( <: ) = ( ==: )                                             (* ibid. *)
    let may_alias = ( ==: )
  end

  module TypDecls = GeneralRecordDecls (FieldAttrs)
  open TypDecls

  (* --- *)

  module CloseAlias (DB: DB) : (ALIAS_OPS with type reftyp := RefTyp.t) = struct
    open Ops
    open RefTyp

    let may_alias = RefOps.( ==: )

    (* Only for user and array types: *)
    module FARel = Imperative.Digraph.Concrete (RefTyp)
    module FARelOps = Oper.I (FARel)
    open RefTyp
    let farel = FARel.create ~size:(cardinal DB.db) ();;
    iter begin fun tn ->
      Fields.iter begin fun _fld (ft, attrs) ->
        if FieldAttrs.instance_field attrs then begin
          if_usertyp ~recurse:true
            (fun t -> FARel.add_edge farel (U tn) (U t))
            ft;
          if_arrytyp ~recurse:true
            (fun a -> FARel.add_edge farel (U tn) (A a))
            ft;
          iter_arrysubrel
            (fun a a' -> FARel.add_edge farel (A a) (A a'))
            (fun a u -> FARel.add_edge farel (A a) (U u))
            ft;
        end
      end
    end DB.db;;
    let farel = FARelOps.add_transitive_closure ~reflexive:false farel;;
    let may_field_alias t t' = FARel.mem_edge farel t t'
    let may_share_mutable t t' = true                                 (* TODO *)
    let may_field_share_mutable = may_share_mutable

    module FARelDot = MakeFARelDotOutput (RefTyp) (FARel)
    let output_farel = FARelDot.output farel
    let output_tfa = output_farel                                      (* TMP *)
  end

  (* --- *)

  module Close (DB: DB) : FLAT with type ftypspec = Fields.s = struct
    include Ops
    include CheckKnownFieldTypes (DB)
    include (CloseFields (DB): FIELD_OPS with type rectyp := UserTyp.t
                                         and type ftypspec = Fields.s)
    include (CloseAlias (DB): ALIAS_OPS with type reftyp := RefTyp.t)
    module RefOps = RefOps
    let mem_typ t = mem t DB.db
  end

  exception FieldRedeclaration of user typname * field *
      any typname * any typname

  let typspec u flds =
    try u, FieldsSpec.list flds with
      | FieldsSpec.FieldRedeclaration (f, t, t') ->
          raise @@ FieldRedeclaration (u, f, t, t')

end

(* -------------------------------------------------------------------------- *)

module SuperTyps = struct
  include Typ.UserTyps
  let print fmt = print' ~empty:"" ~left:"<: @[" ~right:"@]" fmt
end

module type PS = sig
  type t
  val equal: t -> t -> bool
  val is_empty: t -> bool
  val print: t pp
end
module MakePair (A: PS) (B: PS) : PS with type t = A.t * B.t = struct
  type t = A.t * B.t
  let print fmt (s, f) =
    if B.is_empty f
    then A.print fmt s
    else if A.is_empty s
    then B.print fmt f
    else pp fmt "%a@ %a" A.print s B.print f
  let equal = eq_pair A.equal B.equal
  let is_empty (s, f) = A.is_empty s && B.is_empty f
end

(* -------------------------------------------------------------------------- *)

exception IncompleteTypeDecls of Typ.UserTyps.t
exception CyclicSubtypingRelation of Typ.UserTyp.t list list

module MakeIRel
  (Implicits: sig val defs: UserTyps.t end)
  (TypDecl: sig type t val supertyps: t -> SuperTyps.t end)
  (TypDecls: TYP_DECLS with type decl = TypDecl.t)
  (DB: TypDecls.DB)
  =
struct
  open TypDecls

  module IRel = Imperative.Digraph.Concrete(* Bidirectional *) (UserTyp)

  let irel = IRel.create ~size:(cardinal DB.db) ();;

  let alldef, allsup = fold begin fun tn decl (alld, alls) ->
    let s = TypDecl.supertyps decl in
    SuperTyps.add tn alld,
    SuperTyps.fold (fun s -> IRel.add_edge irel s tn; SuperTyps.add s)
      s alls
  end DB.db (Implicits.defs, SuperTyps.empty);;
  let undef = SuperTyps.diff allsup alldef;;

  if not (SuperTyps.is_empty undef) then
    raise @@ IncompleteTypeDecls undef;;

  module IRelDfs = Traverse.Dfs (IRel);;
  if IRelDfs.has_cycle irel then
    let module SCC = Components.Make (IRel) in
    let s = List.fold_left begin fun acc -> function
      | [t] when not (IRel.mem_edge irel t t) -> acc
      | lst -> lst :: acc
    end [] (SCC.scc_list irel) in
    raise @@ CyclicSubtypingRelation (s);;

  module IRelDot = MakeIRelDotOutput (UserTyp) (IRel)
  let output_irel = IRelDot.output irel
end

(* -------------------------------------------------------------------------- *)

type array_inheritance =
  | ArrayInheritNone
  | ArrayInheritByElementType

module type CUSTOM = sig
  val suptyps: RefTyps.t RefTypMap.t
  val suptyps_of: RefTyp.t -> RefTyps.t
  val array_inheritance: array_inheritance
  val immutable_typs: RefTyps.t
end

module NoCustom: CUSTOM = struct
  let suptyps = RefTypMap.empty
  let suptyps_of _ = RefTyps.empty
  let array_inheritance = ArrayInheritNone
  let immutable_typs = RefTyps.empty
end

(* --- *)

type interface_definitions =
  | AllIsClass
  | ExcludeInheriting of UserTyps.t

type interface_inheritance = UserTyps.t

module type PARAMS = sig
  val interface_definitions: interface_definitions
  val interface_inheritance: interface_inheritance
end

module DefaultParams: PARAMS = struct
  let interface_definitions = AllIsClass
  let interface_inheritance = UserTyps.empty
end

(* -------------------------------------------------------------------------- *)

module AugmentDB
  (TypDecls: sig include TYP_DECLS val subtyp_spec: UserTyps.t -> spec end)
  (DB: TypDecls.DB)
  (C: CUSTOM)
  =
struct
  open TypDecls
  open RefTyp

  let db, ignored_decls =
    let uts s = RefTyps.fold (function
      | U t -> UserTyps.add t
      | A _ -> fun s -> s) s UserTyps.empty
    in
    RefTypMap.fold begin fun t sup ((db, ignored) as acc) -> match t with
      | U t -> let spec = subtyp_spec (uts sup) in
              (respec t spec db,
               if bad_respec t spec db
               then redecl t (find t db) ignored
               else ignored)
      | A _ -> acc
    end C.suptyps (DB.db, empty);;

  if not (is_empty ignored_decls) then
    Log.w logger "@[Ignoring@ the@ following@ type@ declarations@ as@ they@ \
                    conflict@ with@ implicit@ assumptions:@]@\n\
                    @[<2>  %a@]" TypDecls.print ignored_decls;;
end

(* -------------------------------------------------------------------------- *)

module MakePropagateFieldRedefinitions
  (TypDecls: sig
    include TYP_DECLS
    module Fields: RECORD_DECL
  end)
  (TypDecl: sig
    type t = TypDecls.decl
    val inherit_fields: TypDecls.Fields.t -> t -> t
    val supertyps: t -> SuperTyps.t
    val fields: t -> TypDecls.Fields.t
  end)
  =
struct
  open TypDecls

  (** Gives two non-equivalent types *)
  exception InconsistentInheritanceOfField of user typname * field *
      Fields.s typname * Fields.s typname

  module PropagateFieldRedefinitions (C: CUSTOM) (DB: DB) (P: PARAMS) = struct
    include MakeIRel (struct
      let defs = RefTypMap.fold (fun t _ -> match t with
        | U u -> UserTyps.add u
        | _ -> ign) C.suptyps UserTyps.empty
    end) (TypDecl) (TypDecls) (DB)

    (* --- *)

    module IRelTopo = Topological.Make (IRel);;
    (* Augment db with field redefinitions. *)
    let db = IRelTopo.fold begin fun t db ->
      let decl = find t DB.db in
      let flds = TypDecl.fields decl in
      let inherited_fields = SuperTyps.fold begin fun st inhflds ->
        let stdecl = find st db in
        let stflds = TypDecl.fields stdecl in
        let stflds = Fields.diff stflds flds in
        try Fields.merge stflds (Fields.inheritable inhflds) with
          | Fields.FieldRedeclaration (f, x, x') ->
              raise @@ InconsistentInheritanceOfField (t, f, x, x')
      end (TypDecl.supertyps decl) Fields.empty in
      redecl t (TypDecl.inherit_fields inherited_fields decl) db
    end irel DB.db;;

    (* --- *)

    (* Mark interfaces/classes *)
    let classes = match P.interface_definitions with
      | AllIsClass -> alldef
      | ExcludeInheriting roots ->
          UserTyps.fold (fun root acc ->
            IRelDfs.fold_component UserTyps.add acc irel root)
            roots UserTyps.empty
    let interfaces = UserTyps.diff alldef classes
    let is_class t = UserTyps.mem t classes
    let is_interface t = not (is_class t);;

    Log.d2 logger "@[All@ known@ classes:@\n%a@]" UserTyps.print classes;;
    Log.d2 logger "@[All@ known@ interfaces:@\n%a@]" UserTyps.print interfaces;;

    UserTyps.iter (fun interface ->
      UserTyps.iter (fun t -> IRel.add_edge irel t interface)
        P.interface_inheritance) interfaces;;

    (* No need to add self loops if we test type equality before edge
       membership. *)
    module IRelOps = Oper.I (IRel)
    let irel' = IRel.copy irel;;
    let irel' = IRelOps.add_transitive_closure ~reflexive:false irel';;

    let ( ==: ) = UserTyp.equal
    let ( <: ) t t' = t ==: t' || IRel.mem_edge irel' t' t

    (* --- *)

    (* Traversal of user-defined types *)

    let iter_known_subtyps f t =
      if IRel.mem_vertex irel t then IRelDfs.prefix_component f irel t
    let fold_subtyps_td f t i =
      if IRel.mem_vertex irel t then IRelDfs.fold_component f i irel t else i
    let iter_direct_known_subtyps f t =
      if IRel.mem_vertex irel t then IRel.iter_succ f irel t
    let fold_direct_known_subtyps f t i =
      if IRel.mem_vertex irel t then IRel.fold_succ f irel t i else i

    (* --- *)

    let irel_rev = IRelOps.mirror irel
    let iter_known_suptyps f t =
      if IRel.mem_vertex irel_rev t then IRelDfs.prefix_component f irel_rev t
    let fold_direct_suptyps f t i =
      if IRel.mem_vertex irel_rev t then IRel.fold_succ f irel_rev t i else i
    let iter_direct_known_suptyps f t =
      if IRel.mem_vertex irel_rev t then IRel.iter_succ f irel_rev t
    let walkup_usertyps f =
      IRelTopo.iter (fun t -> f (RefTyp.U t)) irel_rev
    let walkdown_usertyps f =
      IRelTopo.iter (fun t -> f (RefTyp.U t)) irel

    (* --- *)

    (* Type equivalence induced by interfaces propagates w.r.t transitive
       closure of inverted inheritance relation: *)
    let teqrel = IRel.copy irel_rev;;
    let teqrel = IRelOps.add_transitive_closure ~reflexive:true teqrel;;
    UserTyps.iter begin fun interface ->
      (* XXX: could use pre/succ in transitive closure directly instead of the
         "slightly more complicated" traversals below *)
      iter_known_subtyps (iter_known_suptyps
                            (IRel.add_edge teqrel interface))
        interface
    end interfaces;;

    module ARel = Imperative.Graph.Concrete (UserTyp)
    module ARelDot = MakeARelDotOutput (UserTyp) (ARel)

    let arel = ARel.create ~size:(cardinal DB.db) ();;
    IRel.iter_edges (ARel.add_edge arel) teqrel;;

    (* module ARelDotDir = MakeARelDirDotOutput (UserTyp) (IRel) *)
    (* let output_arel = ARelDotDir.output teqrel *)
    let output_arel = ARelDot.output arel

    (* --- *)

    open RefTyp

    let fu f t = f (U t)
    let accu u = RefTyps.add (U u)

    (* --- *)

    let direct_suptyps_of t =
      let sup = C.suptyps_of t in
      match t with
        | U t -> fold_direct_suptyps accu t sup
        | A _ -> sup

    let iter_direct_suptyps f t = RefTyps.iter f (direct_suptyps_of t)
    let fold_direct_suptyps f t = RefTyps.fold f (direct_suptyps_of t)

    let iter_suptyps f =
      let rec walkup t = f t; iter_direct_suptyps walkup t in walkup
    let fold_suptyps f =
      let rec walkup t i = f t i |> fold_direct_suptyps walkup t in walkup

    (* --- *)

    let direct_known_subtyps_of = function
      | U t -> fold_direct_known_subtyps accu t RefTyps.empty
      | A _ -> RefTyps.empty

    let iter_known_subtyps f t = match t with
      | U t -> iter_known_subtyps (fu f) t
      | A _ -> f t

    let fold_known_ultimate_subtyps f t =
      let rec walkdown t acc =
        let sub = direct_known_subtyps_of t in
        if RefTyps.is_empty sub then f t acc
        else RefTyps.fold walkdown sub acc
      in
      walkdown (U t)

    (* --- *)

    module RefOps = struct
      open RefTyp

      let ( ==: ) = equal

      let rec ( <: ) t t' = false
        || (t ==: t')
        || (match t, t' with
            | U t, U t' -> IRel.mem_edge irel' t' t
            | A t, A t' when C.array_inheritance = ArrayInheritByElementType ->
                let t = array_access t and t' = array_access t' in
                acc_arrytyp ~recurse:false
                  (fun a -> acc_arrytyp ~recurse:false
                    (fun a' _ -> A a <: A a') t') t false
            | _ -> false)
        || (RefTyps.mem t' (C.suptyps_of t))

      let rec may_alias (t: RefTyp.t) (t': RefTyp.t) = false
        || (t ==: t')
        || (match t, t' with
            | U t, U t' -> ARel.mem_edge arel t t'
            | A t, A t' when C.array_inheritance = ArrayInheritByElementType ->
                let t = array_access t and t' = array_access t' in
                acc_arrytyp ~recurse:false
                  (fun a -> acc_arrytyp ~recurse:false
                    (fun a' _ -> may_alias (A a) (A a')) t') t false
            | _ -> false)
        || (t <: t' || t' <: t)
    end

    module DB = struct let db = db end                   (* redefine database *)

    (* --- *)

    let field_alias_rel_indegree: RefTyp.t -> int = fun _ -> 0
    let field_alias_rel_outdegree: RefTyp.t -> int = fun _ -> 0

    let field_access_rel_indegree: RefTyp.t -> int = fun _ -> 0
    let field_access_rel_outdegree: RefTyp.t -> int = fun _ -> 0

    let stats =
      let v = ARel.nb_vertex arel and e = ARel.nb_edges arel in
      let vf = float_of_int v and ef = float_of_int e in
      Stats.{
        tc_nb_classes = UserTyps.cardinal classes;
        tc_nb_interfaces = UserTyps.cardinal interfaces;
        tc_cardinal = v;
        tc_alias_rel_size = e;
        tc_alias_rel_density = (2. *. ef) /. (vf *. vf);
        tc_field_alias_rel_size = 0;
        tc_field_alias_rel_density = nan;
        tc_field_access_rel_size = 0;
        tc_field_access_rel_density = nan;
      }

    module DegreeStats = BasicStats.Aggreg.Make (BasicStats.Int)

    let alias_rel_degree_stats ~quantiles =
      let d = Array.make (ARel.nb_vertex arel) 0 in
      ignore (ARel.fold_vertex
                (fun v i -> d.(i) <- ARel.in_degree arel v; succ i) arel 0);
      Stats.{
        rds_stats = DegreeStats.quantiles quantiles d;
      }

    let field_alias_rel_degree_stats ~quantiles =
      let open BasicStats.Aggreg in
      Stats.{
        rds_in_stats = Basic { min = 0; max = 0; mean = 0.; stddev = nan };
        rds_out_stats = Basic { min = 0; max = 0; mean = 0.; stddev = nan };
      }

    let field_access_rel_degree_stats ~quantiles =
      let open BasicStats.Aggreg in
      Stats.{
        tfa_in_stats = Basic { min = 0; max = 0; mean = 0.; stddev = nan };
        tfa_out_stats = Basic { min = 0; max = 0; mean = 0.; stddev = nan };
      }

    let degree_stats ~quantiles =
      Stats.{
        tds_alias_rel_degree_stats =
          alias_rel_degree_stats ~quantiles;
        tds_field_alias_rel_degree_stats =
          field_alias_rel_degree_stats ~quantiles;
        tds_field_access_rel_degree_stats =
          field_access_rel_degree_stats ~quantiles;
      }
  end
end

(* -------------------------------------------------------------------------- *)

module ClassOfPrimFields
  (FieldAttrs: FIELD_ATTRS with type typspec = prim)
  (C: CUSTOM)
  =
struct

  module PrimFieldsSpec = MakeFieldsDecl (PrimTyp) (FieldAttrs.Spec)
  module PrimFields = MakeFieldsDecl (PrimTyp) (FieldAttrs)

  module TypSpec = MakePair (SuperTyps) (PrimFieldsSpec)
  module TypDecl = struct
    include MakePair (SuperTyps) (PrimFields)
    let supertyps = fst
    let fields = snd
    let inherit_fields inherited_fields (s, f) =
      (s, PrimFields.inherit_fields inherited_fields f)
    let valid_redecl (sa, fa) (sb, fb) =
      SuperTyps.equal sa sb || SuperTyps.subset sb sa && PrimFields.valid_redecl fa fb
  end

  (* --- *)

  module TypDecls = struct
    module FieldsSpec = PrimFieldsSpec
    module Fields = PrimFields

    include MakeTypDecls (TypSpec) (struct
      include TypDecl
      let make: user typname -> TypSpec.t -> t = fun u (sup, flds) ->
        (sup, FieldMap.map (fun (t, a) -> t, FieldAttrs.from_spec u t a) flds)
    end)
    let subtyp_spec sup = sup, FieldsSpec.empty

    let dot_f: t -> user typname -> field -> prim typname * _ = fun db ut f ->
      match find' ut db with
        | None -> raise @@ UnknownType ut
        | Some (_, flds) -> try FieldMap.find f flds with
            | Not_found -> raise @@ UnknownFieldForType (ut, f)
  end
  open TypDecls

  (* --- *)

  include MakePropagateFieldRedefinitions (TypDecls) (TypDecl)

  module CloseSubtypOps (DB: DB) (P: PARAMS) = struct
    include PropagateFieldRedefinitions (C) (AugmentDB (TypDecls) (DB) (C)) (P)
  end

  (* --- *)

  module CloseSubtypsNAlias (DB: DB) (P: PARAMS) = struct
    include CloseSubtypOps (DB) (P)
    open RefOps
    let may_alias = may_alias
    let may_field_alias _ _ = false
    let may_share_mutable _ _ = false
    let may_field_share_mutable = may_share_mutable
    let output_farel ?backup ?suff out =                              (* skip *)
      Log.d logger "@[Skipping@ output@ of@ empty@ field-alias@ relation:@ \
                      primitive@ fields@ only.@]"
    let output_tfa ?backup ?suff out =                                (* skip *)
      Log.d logger "@[Skipping@ output@ of@ transitive@ field-access@ \
                      relation:@ primitive@ fields@ only.@]"
  end

  (* --- *)

  module CloseFields (DB: DB) = struct
    type ftypspec = Fields.s
    type fattrs = Fields.attrs
    let dot_f: user typname -> field -> ftypspec typname * fattrs = function
      | UserTyp _ as t -> fun f -> dot_f DB.db t f
  end

  (* --- *)

  module Close (DB: DB) (P: PARAMS) : TREE with type ftypspec = prim = struct
    include CloseSubtypsNAlias (DB) (P)
    include (CloseFields (DB): FIELD_OPS with type rectyp := UserTyp.t
                                         and type ftypspec = prim)
    let mem_typ t = mem t DB.db
  end

  (* --- *)

  exception FieldRedeclaration of user typname * field *
      prim typname * prim typname

  let typspec u sups flds =
    try u, (sups, FieldsSpec.list flds) with
      | FieldsSpec.FieldRedeclaration (f, t, t') ->
          raise @@ FieldRedeclaration (u, f, t, t')

end

(* -------------------------------------------------------------------------- *)

module Heap
  (FieldAttrs: FIELD_ATTRS with type typspec = any)
  (C: CUSTOM)
  :
sig
  module FieldsSpec: RECORD_DECL with type s = any
                                 and type attrs = FieldAttrs.Spec.t
  module TypSpec: sig type t = SuperTyps.t * FieldsSpec.t end
  module TypDecls: TYP_DECLS with type spec = TypSpec.t
  module Close (DB: TypDecls.DB) (P: PARAMS)
    : TREE with type ftypspec = any and type fattrs = FieldAttrs.t

  exception InconsistentInheritanceOfField of user typname * field *
      any typname * any typname

  exception FieldRedeclaration of user typname * field *
      any typname * any typname

  val typspec: user typname -> SuperTyps.t -> FieldsSpec.t list
    -> user typname * TypSpec.t
end
  =
struct

  module FieldsSpec = MakeFieldsDecl (struct type s = any end) (FieldAttrs.Spec)
  module Fields = MakeFieldsDecl (struct type s = any end) (FieldAttrs)

  module TypSpec = MakePair (SuperTyps) (FieldsSpec)
  module TypDecl = struct
    include MakePair (SuperTyps) (Fields)
    let supertyps = fst
    let fields = snd
    let inherit_fields inherited_fields (s, f) =
      (s, Fields.inherit_fields inherited_fields f)
    let valid_redecl (sa, fa) (sb, fb) =
      SuperTyps.equal sa sb || SuperTyps.subset sb sa && Fields.valid_redecl fa fb
  end

  (* --- *)

  module TypDecls = struct
    module FieldsSpec = FieldsSpec
    module Fields = Fields

    include MakeTypDecls (TypSpec) (struct
      include TypDecl
      let make: user typname -> TypSpec.t -> t = fun u (sup, flds) ->
        (sup, FieldMap.map (fun (t, a) -> t, FieldAttrs.from_spec u t a) flds)
    end)
    let subtyp_spec sup = sup, FieldsSpec.empty

    let dot_f: t -> user typname -> field -> any typname * _ = fun db ut f ->
      match find' ut db with
        | None -> raise @@ UnknownType ut
        | Some (_, flds) -> try FieldMap.find f flds with
            | Not_found -> raise @@ UnknownFieldForType (ut, f)
  end
  open TypDecls

  (* --- *)

  module CheckKnownFieldTypes = MakeCheckKnownFieldTypes (struct
    type t = TypDecls.decl
    let fields typ_spec = FieldMap.map fst (snd typ_spec)
  end) (TypDecls)

  (* --- *)

  include MakePropagateFieldRedefinitions (TypDecls) (TypDecl)

  module CloseSubtypOps (DB: DB) (P: PARAMS) = struct
    include PropagateFieldRedefinitions (C) (AugmentDB (TypDecls) (DB) (C)) (P)
  end

  (* --- *)

  module CloseSubtypsNAlias (DB: DB) (P: PARAMS) = struct
    include CloseSubtypOps (DB) (P)
    open RefTyp

    let may_alias = RefOps.may_alias

    (* --- *)

    module FARel = Imperative.Matrix.Digraph
    module FARelOps = Oper.I (FARel)

    let farel, tfa, t2i, i2t =

      let addtyp t ((m, i) as acc) =
        if RefTypMap.mem t m then acc else RefTypMap.add t i m, i + 1
      in
      let t2im, numtyps = (RefTypMap.empty, 0)
        |> fold begin fun u decl acc ->
          let acc = addtyp (U u) acc in
          let acc = Fields.fold begin fun _fld (ft, _) acc -> acc
            |> acc_usertyp ~recurse:true (fun u -> addtyp (U u)) ft
            |> acc_arrytyp ~recurse:true (fun a -> addtyp (A a)) ft
          end (TypDecl.fields decl) acc in
          acc
        end DB.db
      in
      (* Log.w logger "%a" (RefTypMap.print pp_print_int) t2im; *)
      let t2i t = RefTypMap.find t t2im in
      let i2t =
        if numtyps > 0 then
          let i2ta = Array.make numtyps (RefTypMap.choose t2im |> fst) in
          RefTypMap.iter (fun t i -> i2ta.(i) <- t) t2im;
          Array.get i2ta
        else
          fun i -> raise Not_found
      in

      let open FARel in
      let tfa = make numtyps in

      let access a b =
        let ai, bi = t2i a, t2i b in
        if Log.check_level logger Log.Debug && not (mem_edge tfa ai bi) then
          Log.d3 logger "  Caching %a .~> %a" RefTyp.print a RefTyp.print b;
        add_edge tfa ai bi
      and link_suptyps_to_succ g t  =
        let ti = t2i t in
        iter_direct_suptyps (fun s -> iter_succ (add_edge g (t2i s)) g ti) t
      and link_pred_to_subtyps g = function
        | U u as t ->
            let ti = t2i t in
            iter_direct_known_subtyps
              (fun u -> iter_pred (fun p -> add_edge g p (t2i (U u))) g ti) u
        | A _ -> ()
      and link_pred_to_suptyps g t  =
        let ti = t2i t in
        iter_direct_suptyps (fun s -> iter_pred (fun p -> add_edge g p (t2i s)) g ti) t
      in

      (* Add declared field accesses: *)
      let link_field_sub t t' =
        if_usertyp ~recurse:false (fun u -> access t (U u)) t';
        if_arrytyp ~recurse:false (fun a -> access t (A a)) t'
      in
      let link_cell_sub a = link_field_sub (A a) in
      iter begin fun u decl ->
        Fields.iter begin fun _fld (ft, fattrs) ->
          if FieldAttrs.instance_field fattrs then begin
            link_field_sub (U u) ft;
            iter_arrysubrel link_cell_sub link_cell_sub ft;
          end
        end (TypDecl.fields decl)
      end DB.db;

      (* Propagating accessed objects down to inheriting types *)
      walkdown_usertyps (link_pred_to_subtyps tfa);

      (* Propagating field accesses up to inherited types *)
      RefTypMap.iter begin fun t _ -> match t with
        | A a -> link_suptyps_to_succ tfa (A a)
        | _ -> ()
      end t2im;
      walkup_usertyps (link_suptyps_to_succ tfa);

      Log.i logger "@[Computing@ transitive@ closure@ of@ raw@ field-access@ \
                      relation@<1>…@]";
      let tfa =
        Log.roll (FARelOps.add_transitive_closure ~reflexive:false) tfa in

      (* --- *)

      (* Propagating accessed objects up to inherited types *)
      let farel = FARel.copy tfa in
      RefTypMap.iter begin fun t _ -> match t with
        | A a -> link_pred_to_suptyps farel (A a)
        | _ -> ()
      end t2im;
      walkup_usertyps (link_pred_to_suptyps farel);

      (* --- *)

      farel, tfa, t2i, i2t

    (* --- *)

    (* let mem_typ t = RefTypMap.mem t t2i *)
    let mem_edge t t' =
      try FARel.mem_edge farel (t2i t) (t2i t') with Not_found -> false

    let arrytyp_reach_arrytyp a a' =
      acc_arrytyp ~recurse:true
        (fun a -> ( || ) (ArryTyp.equal a' a)) a false
      || acc_usertyp ~recurse:true
        (fun u -> ( || ) (mem_edge (U u) (A a'))) a false
    let arrytyp_reach_usertyp a u' =
      acc_usertyp ~recurse:true
        (fun u -> ( || ) (u ==: u' || mem_edge (U u) (U u'))) a false
      || acc_arrytyp ~recurse:false
        (fun a -> ( || ) (RefTyps.mem (U u') (C.suptyps_of (A a)))) a false
    let may_field_alias t t' = mem_edge t t' || begin
      let b = match t, t' with
        | U _, U _ | U _, A _ -> false
        | A a, U u' -> arrytyp_reach_usertyp (array_access a) u'
        | A a, A a' -> arrytyp_reach_arrytyp (array_access a) a'
      in
      (* Log.i logger "%a.~>%a? %B" RefTyp.print t RefTyp.print t' b; *)
      b
    end

    let rec may_share_mutable =
      let module Z = Integers in
      let addt t s =
        try Z.add (t2i t) s with Not_found -> s
      and addt' t (s, ss) =
        try Z.add (t2i t) s, ss with Not_found -> s, RefTyps.add t ss
      and reach ~ignore_root t =
        try let i = t2i t in
            let s = if ignore_root then Z.empty else Z.singleton i in
            FARel.fold_succ Z.add tfa i s
        with Not_found -> Z.empty
      in
      let arry_reach ~ignore_root =
        let reach1 ~ignore_root t acc =
          let s, ss = if not ignore_root then addt' t acc else acc in
          Z.union (reach ~ignore_root t) s, ss
        in function
          | U _ -> ign
          | A t -> begin fun s -> (s, ignore_root)
            |> acc_arrytyp ~recurse:true
                (fun a (s, ignore_root) -> reach1 ~ignore_root (A a) s, false) t
            |> fst
            |> acc_usertyp ~recurse:true
                (fun u -> reach1 ~ignore_root:false (U u)) t
          end
      in
      let immutables = RefTyps.fold addt C.immutable_typs Z.empty in
      fun ~ignore_left_fields t t' ->
        (if ignore_left_fields then false else may_alias t t') || begin
          let reach = reach ~ignore_root:ignore_left_fields
          and reach' = reach ~ignore_root:false
          and arry_reach = arry_reach ~ignore_root:ignore_left_fields
          and arry_reach' = arry_reach ~ignore_root:false in
          let tri, trx = arry_reach t (reach t, RefTyps.empty)
          and t'ri, t'rx = arry_reach' t' (reach' t', RefTyps.empty) in
          (* Log.w logger "%a      %a" Z.print tri Z.print t'ri; *)
          (* Log.w logger "%a" Z.print (Z.diff (Z.inter tri t'ri) immutables); *)
          (* Log.w logger "%a      %a" RefTyps.print trx RefTyps.print t'rx; *)
          (* Log.w logger "%a" RefTyps.print (RefTyps.inter trx t'rx); *)
          let exists_arry_suptyp f trx =
            RefTyps.(exists (function
              | U _ -> false
              | A _ as t -> RefTyps.exists f (C.suptyps_of t)) trx)
          and may_share_mutable = may_share_mutable ~ignore_left_fields:false in
          not Z.(is_empty (diff (inter tri t'ri) immutables)) ||
            not RefTyps.(is_empty (inter trx t'rx)) ||
            exists_arry_suptyp (fun t' -> may_share_mutable t t') t'rx ||
            exists_arry_suptyp (fun t  -> may_share_mutable t t') trx
        end

    let may_share_mutable, may_field_share_mutable =
      may_share_mutable ~ignore_left_fields:false,        (* equivalence rel. *)
      may_share_mutable ~ignore_left_fields:true          (* transitive rel. *)

    (* ;; *)
    (* let int_a = array (prim "int") in *)
    (* let int_aa = array int_a in *)
    (* let obj = UserTyp "java.lang.Object" in *)
    (* assert (may_field_alias (A int_aa) (A int_a)); *)
    (* assert (may_field_alias (A int_aa) (U obj)); *)
    (* assert (not (may_field_alias (A int_a) (A int_aa))); *)
    (* assert (not (may_field_alias (U obj) (A int_aa))); *)
    (* assert (may_share_mutable (A int_a) (A int_aa)); *)
    (* assert (may_share_mutable (A int_aa) (A int_a)); *)
    (* assert (may_share_mutable (A int_aa) (U obj)); *)
    (* assert (may_share_mutable (U obj) (A int_aa)); *)
    (* assert (may_share_mutable (A int_aa) (U obj)); *)
    (* ;; *)

    (* let a = UserTyp "A" and b = UserTyp "B" and c = UserTyp "C" in *)
    (* Log.w logger "%B" (may_share_mutable (U b) (U c)); *)
    (* (\* (\\* Log.w logger "%B" (may_share_mutable (U b) (U a)); *\\) *\) *)
    (* (\* (\\* Log.w logger "%B" (may_share_mutable (U c) (U a)); *\\) *\) *)
    (* (\* (\\* Log.w logger "%B" (may_share_mutable (A (array (c))) (U a)); *\\) *\) *)
    (* (\* (\\* Log.w logger "%B" (may_share_mutable' (U b) (U c)); *\\) *\) *)
    (* (\* (\\* Log.w logger "%B" (may_share_mutable' (U b) (U a)); *\\) *\) *)
    (* (\* (\\* Log.w logger "%B" (may_share_mutable  (U c) (U a)); *\\) *\) *)
    (* (\* Log.w logger "%B" (may_share_mutable' (U c) (U a)); *\) *)
    (* (\* (\\* Log.w logger "%d" (RefTypMap.find (U c) t2i); *\\) *\) *)
    (* (\* (\\* Log.w logger "%a" (RefTypMap.print pp_print_int) t2i; *\\) *\) *)
    (* (\* (\\* Log.w logger "%B" (may_share_mutable' (A (array (c))) (U a)); *\\) *\) *)
    (* (\* Log.w logger "%B" (may_share_mutable' (U a) (U c)); *\) *)
    (* ;; *)

    (* ---- *)

    let output_farel ?backup ?suff out =
      let module FARelDot = MakeFARelDotOutput (struct
        include FARel.V
        let print fmt i = RefTyp.print fmt (i2t i)
      end) (FARel) in
      FARelDot.output farel ?backup ?suff out

    let output_tfa ?backup ?suff out =
      let module FARelDot = MakeFARelDotOutput (struct
        include FARel.V
        let print fmt i = RefTyp.print fmt (i2t i)
      end) (FARel) in
      FARelDot.output tfa ?backup ?suff out

    (* --- *)

    let field_alias_rel_indegree: RefTyp.t -> int = fun t ->
      try FARel.in_degree farel (t2i t) with Not_found -> 0

    let field_alias_rel_outdegree: RefTyp.t -> int = fun t ->
      try FARel.out_degree farel (t2i t) with Not_found -> 0

    let field_access_rel_indegree: RefTyp.t -> int = fun t ->
      try FARel.in_degree tfa (t2i t) with Not_found -> 0

    let field_access_rel_outdegree: RefTyp.t -> int = fun t ->
      try FARel.out_degree tfa (t2i t) with Not_found -> 0

    let stats =
      let v = FARel.nb_vertex farel and e = FARel.nb_edges farel in
      let vf = float_of_int v and ef = float_of_int e in
      let v' = FARel.nb_vertex tfa and e' = FARel.nb_edges tfa in
      let v'f = float_of_int v' and e'f = float_of_int e' in
      Stats.{ stats with
        tc_field_alias_rel_size = e;
        tc_field_alias_rel_density = ef /. (vf *. vf);
        tc_field_access_rel_size = e';
        tc_field_access_rel_density = e'f /. (v'f *. v'f);
      }

    let field_alias_rel_degree_stats ~quantiles =
      let n = FARel.nb_vertex farel in
      if n > 0 then
        let in_degrees = Array.init n (FARel.in_degree farel)
        and out_degrees = Array.init n (FARel.out_degree farel) in
        Stats.{
          rds_in_stats = DegreeStats.quantiles quantiles in_degrees;
          rds_out_stats = DegreeStats.quantiles quantiles out_degrees;
        }
      else
        Stats.{
          rds_in_stats = Basic { min = 0; max = 0; mean = 0.; stddev = nan };
          rds_out_stats = Basic { min = 0; max = 0; mean = 0.; stddev = nan };
        }

    let field_access_rel_degree_stats ~quantiles =
      let n = FARel.nb_vertex tfa in
      if n > 0 then
        let in_degrees = Array.init n (FARel.in_degree tfa)
        and out_degrees = Array.init n (FARel.out_degree tfa) in
        Stats.{
          tfa_in_stats = DegreeStats.quantiles quantiles in_degrees;
          tfa_out_stats = DegreeStats.quantiles quantiles out_degrees;
        }
      else
        Stats.{
          tfa_in_stats = Basic { min = 0; max = 0; mean = 0.; stddev = nan };
          tfa_out_stats = Basic { min = 0; max = 0; mean = 0.; stddev = nan };
        }

    let degree_stats ~quantiles =
      Stats.{
        tds_alias_rel_degree_stats =
          alias_rel_degree_stats ~quantiles;
        tds_field_alias_rel_degree_stats =
          field_alias_rel_degree_stats ~quantiles;
        tds_field_access_rel_degree_stats =
          field_access_rel_degree_stats ~quantiles;
      }

  end

  (* --- *)

  module CloseFields (DB: DB) = struct
    type ftypspec = Fields.s
    type fattrs = FieldAttrs.t
    let dot_f: user typname -> field -> ftypspec typname * fattrs = function
      | UserTyp _ as t -> fun f -> dot_f DB.db t f
  end

  (* --- *)

  module Close (DB: DB) (P: PARAMS) : TREE with type ftypspec = Fields.s
                                           and type fattrs = Fields.attrs =
  struct
    include CheckKnownFieldTypes (DB)
    include CloseSubtypsNAlias (DB) (P)                           (* Redef DB *)
    include (CloseFields (DB): FIELD_OPS with type rectyp := UserTyp.t
                                         and type fattrs = Fields.attrs
                                         and type ftypspec = Fields.s)
    let mem_typ t = mem t DB.db
  end

  (* --- *)

  exception FieldRedeclaration of user typname * field *
      any typname * any typname

  let typspec u sups flds =
    try u, (sups, FieldsSpec.list flds) with
      | FieldsSpec.FieldRedeclaration (f, t, t') ->
          raise @@ FieldRedeclaration (u, f, t, t')

end

(* -------------------------------------------------------------------------- *)
