open Rutils
open Utils
open Base
open Stm

(* --- *)

open Seq

type 'l error =
  [
  | `UndefLabel of 'l * label
  | `RedefLabel of 'l * label
  | `MissplacedCatch of 'l
  ]

let pp_error: 'l pp -> [> 'l error] pp = fun pp_loc fmt -> function
  | `UndefLabel (loc, l) ->
      pp fmt "Undefined@ label@ `%a'%a" pp_label l pp_loc loc
  | `RedefLabel (loc, l) ->
      pp fmt "Forbiden@ redefinition@ of@ label@ `%a'%a" pp_label l pp_loc loc
  | `MissplacedCatch loc ->
      pp fmt "Missplaced@ catch@ statement%a" pp_loc loc

type ('stm, 'loc, 'errs) t =
    {
      l2pc: pc StrMap.t;
      errs: 'errs MutableStack.t;
      exc_handlers: PCs.t;
    }

let make errs i : _ t =
  let rec map_labels_to pc m : _ Stm.unlinked -> _ = function
    | Label (l, _) when StrMap.mem l m ->
        MutableStack.push errs (`RedefLabel (Some (`Stm (pc, None)), l)); m
    | Label (l, e) -> map_labels_to pc (StrMap.add l pc m) e
    | _ -> m
  in
  let acc_labels (m, pc) s = map_labels_to pc m s, pc + 1 in
  {
    errs;
    l2pc = fold acc_labels (StrMap.empty, 0) i |> fst;
    exc_handlers = PCs.empty;
  }

exception UndefinedLabel of label
exception MissplacedCatch of pc

let undeflabel { errs } ?loc l =
  MutableStack.push errs (`UndefLabel (loc, l))

let missplaced_catch { errs } ?loc () =
  MutableStack.push errs (`MissplacedCatch loc)

let branch: _ t -> label -> nominal branch = fun { l2pc } l ->
  try Jump (l, StrMap.find l l2pc)
  with Not_found -> raise (UndefinedLabel l)

let link_stm linker pc =
  let branch = branch linker in
  let switch { swch_cases; swch_default } =
    { swch_cases = List.map (fun (c, l) -> (c, branch l)) swch_cases;
      swch_default = branch swch_default; } in
  let rec link: type k. ('s, k) unlinked -> ('s, k) linked = function
    | Nop -> Nop
    | Decl (c, rl) -> Decl (c, rl)
    | Assign _ as s -> s
    | AssignP (v, e) -> AssignP (v, e)
    | AssignR (r, s) -> AssignR (r, s)
    | LoadP (v, r, f) -> LoadP (v, r, f)
    | LoadR (r, s, f) -> LoadR (r, s, f)
    | StoreP (r, f, e) -> StoreP (r, f, e)
    | StoreR (r, f, s) -> StoreR (r, f, s)
    | SLoadP _ as s -> s
    | SLoadR _ as s -> s
    | SStoreP _ as s -> s
    | SStoreR _ as s -> s
    | ALoad (v, a, e) -> ALoad (v, a, e)
    | ALoadP (v, a, e) -> ALoadP (v, a, e)
    | ALoadR (r, a, e) -> ALoadR (r, a, e)
    | AStore (a, e, v) -> AStore (a, e, v)
    | AStoreP (a, e, v) -> AStoreP (a, e, v)
    | AStoreR (a, e, r) -> AStoreR (a, e, r)
    | ALen (v, a) -> ALen (v, a)
    | UCall (r, m, vl) -> UCall (r, m, vl)
    | USCall _ as s -> s
    | UFCall ec -> UFCall ec
    | MCall _ as s -> s
    | MSCall _ as s -> s
    | MFCallP (v, ec) -> MFCallP (v, ec)
    | MFCallR (r, ec) -> MFCallR (r, ec)
    | New (r, c) -> New (r, c)
    | ANew (r, t, d) -> ANew (r, t, d)
    | InstOf _ as s -> s
    | InstOfR _ as s -> s
    | InstOfA _ as s -> s
    | Cast (r, t, s) -> Cast (r, t, s)
    | CastP (v, t, w) -> CastP (v, t, w)
    | CastR (r, t, s) -> CastR (r, t, s)
    | CastA _ as s -> s
    | Output (l, vl) -> Output (l, vl)
    | OutputPR (l, prl) -> OutputPR (l, prl)
    | ReturnU -> ReturnU
    | Return x -> Return x
    | ReturnP x -> ReturnP x
    | ReturnR x -> ReturnR x
    | ChkPoint v -> ChkPoint v
    | Goto l -> Goto (branch l)
    | CondGoto (e, l) -> CondGoto (e, branch l)
    | Switch (e, se) -> Switch (e, switch se)
    | Throw r -> Throw r
    | Catch _ when not (PCs.mem pc linker.exc_handlers) ->
        raise @@ MissplacedCatch pc
    | Catch r -> Catch r
    | MonEnter _ | MonExit _ as s -> s
    | Label (l, s) -> Label (l, link s)
  in
  fun s -> try link s with
    | UndefinedLabel l
      -> undeflabel linker ~loc:(`Stm (pc, Some s)) l; Nop
    | MissplacedCatch pc
      -> missplaced_catch linker ~loc:(`Stm (pc, Some s)) (); Nop

let link_catch_entry i ({ catch_from; catch_to; catch_at } as e) linker =
  try let link = branch linker in
      let catch_at = link catch_at in
      Some { e with catch_at;
        catch_from = link catch_from;
        catch_to = link catch_to; },
      { linker with
        exc_handlers = PCs.add (target catch_at) linker.exc_handlers; }
  with
    | UndefinedLabel l -> undeflabel linker ~loc:(`InCatchTbl i) l; None, linker

let link_catch_table linker ct =
  CatchTable.fold_mapi link_catch_entry ct
    { linker with exc_handlers = PCs.empty }

let offset_above pc offset : _ t -> _ t = fun ({ l2pc } as t) ->
  { t with l2pc = StrMap.map (fun pc' -> if pc' > pc then pc' + offset else pc')
      l2pc }
