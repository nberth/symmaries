open Format
open Rutils
open Utils
open Graph
open Base
open Method
open Stm

let level = Log.Debug
let logger = Log.mk ~level "CFG"

(* -------------------------------------------------------------------------- *)

module type FIXPOINT = sig
  module M: Map.S with type key = pc
  type setup
  type data
  val apply:
    ?widening_set:pc Graph.ChaoticIteration.widening_set ->
    (pc -> data) -> setup -> data M.t
end

(* --- *)

module type RESOLUTION_UNIVERSE = sig
  type t
  val u: t
end

(* --- *)

type flow = Nominal | Exceptional
module Flow = struct
  type t = flow
  let compare: t -> t -> int = Stdlib.compare
  let default = Nominal
end

(* --- *)

module type T = sig
  module Meth: Method.T
  module G: Graph.Sig.I
    with type V.t = pc
    and type E.t = pc * flow * pc
  type cfg
  val of_meth: ?size: int -> ?rev: bool -> Meth.t -> cfg
  val graph: cfg -> G.t
  val meth: cfg -> Meth.t
  val sink: cfg -> pc
  val pp_loc: cfg -> escaped: bool -> pc pp
  val is_branching: cfg -> pc -> bool
  val fold_branching: (pc -> 'a -> 'a) -> cfg -> 'a -> 'a
  val fold_nom_branching: (pc -> out_degree:int -> 'a -> 'a) -> cfg -> 'a -> 'a
  val fold_sink_pred: (pc -> 'a -> 'a) -> cfg -> 'a -> 'a
  val is_branching_on_np: cfg -> pc -> bool
  val is_branching_on_bad_cast: cfg -> pc -> bool
  val is_branching_on_negsz_alloc: cfg -> pc -> bool
  val is_branching_on_aindx_error: cfg -> pc -> bool
  module Fixpoint: sig
    type setup
    val setup: cfg -> setup
    val cfg: setup -> cfg
    module type T = FIXPOINT with type setup := setup
    module Make
      (D: Graph.ChaoticIteration.Data with type edge := G.edge)
      : T with type data := D.t
  end
  val output_dot: cfg pp
  module ResolutionUniverse: RESOLUTION_UNIVERSE
end

(* --- *)

module type DEREF_INFOS = sig
  module Meth: Method.T
  type stm = (Security.t, nominal branch) Meth.Sem.stm
  val may_np: pc -> stm -> bool
end

(* --- *)

module type CAST_INFOS = sig
  module Meth: Method.T
  type stm = (Security.t, nominal branch) Meth.Sem.stm
  val unsafe_cast: Meth.Sem.decls -> pc -> stm -> bool
end

(* --- *)

module type ARRAY_ALLOC_INFOS = sig
  module Meth: Method.T
  type stm = (Security.t, nominal branch) Meth.Sem.stm
  val may_negsz_alloc: pc -> stm -> bool
end

(* --- *)

module type METH_INFOS = sig
  module Meth: Method.T
  type stm = (Security.t, nominal branch) Meth.Sem.stm
  include DEREF_INFOS with module Meth := Meth and type stm := stm
  include CAST_INFOS with module Meth := Meth and type stm := stm
  include ARRAY_ALLOC_INFOS with module Meth := Meth and type stm := stm
  val may_aindx_error: pc -> stm -> bool
end

(* --- *)

module type OPTIONS = sig
  val track_excs: bool
end

module DefaultOptions: OPTIONS = struct
  let track_excs = false
end

(* --- *)

module Make
  (Options: OPTIONS)
  (Meth: Method.T)
  (MethInfos: METH_INFOS with module Meth := Meth)
  (Resolutions: Method.RESOLUTIONS with module Meth := Meth)
  (ResolutionUniverse: sig
    include RESOLUTION_UNIVERSE with type t = Resolutions.universe
    val filter: Resolutions.e -> Sign.decl
  end)
  :
  (T with module Meth = Meth
     and module ResolutionUniverse = ResolutionUniverse)
  =
struct
  module Meth = Meth
  module Sem = Meth.Sem
  module ResolutionUniverse = ResolutionUniverse
  module G = Imperative.Digraph.ConcreteBidirectionalLabeled (PC) (Flow)
  open MethInfos
  open Options

  type body = Sem.domain Body.t
  type cfg =
      {
        g: G.t;
        meth: Meth.t;
      }

  let unsafe_goto () =
    failwith "Unsafe conditional branching instruction at the end of the given \
              sequence of statements"

  open Typ

  module Decls = Resolutions.Filtered (struct
    type t = Sign.decl
    let filter = ResolutionUniverse.filter
    let merge _ ?res _ = List.fold_left Meth.Sem.merge_throws
  end)

  let npexctyps = UserTyps.singleton Sem.npexctyp
  let ccexctyps = UserTyps.singleton Sem.ccexctyp
  let negsztyps = UserTyps.singleton Sem.negsztyp
  let aindxtyps = UserTyps.singleton Sem.aindxtyp
  let uncheckedexctyps = UserTyps.empty                            (* for now *)
    |> UserTyps.union npexctyps
    |> UserTyps.union ccexctyps
    |> UserTyps.union negsztyps

  let of_meth ?size ?(rev = false) : Meth.t -> cfg = fun meth ->
    let pc2excs =
      if track_excs
      then let pc2decls, _ = Decls.lookup_all meth ResolutionUniverse.u in
           fun pc -> Sign.Decl.throws (PCMap.find pc pc2decls)
      else fun _ -> UserTyps.empty
    in
    let decls = Meth.decls meth and body = Meth.body meth in
    let size = match size with Some s -> s | None -> Body.length body + 1 in
    let g = G.create ~size () in
    let sink = Body.first_invalid_pc body in
    let nomfl pc pc' =
      if rev then G.add_edge g pc' pc else G.add_edge g pc pc'
    and excfl pc pc' =
      let src, dest = if rev then pc', pc else pc, pc' in
      G.add_edge_e g (G.E.create src Exceptional dest)
    in
    let rec link pc = function
      | Goto br -> nomfl pc (target br)
      | CondGoto (_, br) when pc < sink -> nomfl pc (pc+1); nomfl pc (target br)
      | CondGoto _ -> unsafe_goto ()
      | Switch (_, sw) -> switch pc sw
      | Throw r when not track_excs -> throw_no_excs pc r
      | Throw r -> throw ~exctyps:(UserTyps.singleton (Sem.excdom decls r)) pc
      | Label (_, s) -> link pc s
      | s when is_return s -> nomfl pc sink
      | s when pc < sink ->
          if track_excs && is_methcall s then
            throw ~exctyps:((* UserTyps.union uncheckedexctyps *) (pc2excs pc)) pc;
          nomfl pc (pc+1)
      | _ -> ()
    and switch pc { swch_cases; swch_default } =
      List.iter (fun (_, l) -> nomfl pc (target l)) swch_cases;
      nomfl pc (target swch_default)
    and throw ?exctyps pc =
      let handlers, full = Sem.on_exc (Body.catch_table body) ?exctyps pc in
      List.iter (fun (_, h) -> excfl pc (target h)) handlers;
      if not full then excfl pc sink
    and throw_no_excs pc r =
      Log.d logger "@[Treating@ `%a'@ statement@ as@ a@ nominal@ return@ \
                      since@ the@ tracking@ of@ exceptional@ flow@ is@ \
                      disabled.@]"
        (print_linked ?pad:None ~pp_lvl:Security.print) (Throw r);
      nomfl pc sink
    and link0 pc stm =
      if track_excs then UserTyps.(begin
        let exctyps = empty
          |> (if may_np pc stm then union npexctyps else ign)
          |> (if unsafe_cast decls pc stm then union ccexctyps else ign)
          |> (if may_negsz_alloc pc stm then union negsztyps else ign)
          |> (if may_aindx_error pc stm then union aindxtyps else ign)
        in
        if not (is_empty exctyps) then
          throw ~exctyps pc
      end);
      link pc stm
    in
    Body.iter_stms link0 body;
    { g; meth }

  let graph { g } = g
  let meth { meth } = meth
  let sink { meth } = Body.first_invalid_pc (Meth.body meth)

  let pp_loc { g; meth } =
    let open ScgsUtils.IO in
    let body = Meth.body meth in
    let pp_stm fmt pc =
      Stm.print_linked ~pp_lvl:Security.print fmt (Body.stm_at_pc body pc)
    in
    fun ~escaped fmt pc ->
      if Body.valid_pc body pc                              (* sink otherwise *)
      then
        if escaped
        then let s = escape_label (asprintf "%a" pp_stm pc) in
             pp fmt "%i: @<1>〈%s@<1>〉" pc s
        else pp fmt "%i: @<1>〈%a@<1>〉" pc pp_stm pc
      else if pc = Body.first_invalid_pc body
      then pp fmt "return sink"
      else pp fmt "exception sink"

  let is_branching { g } pc =
    G.mem_vertex g pc && G.out_degree g pc > 1

  let fold_branching f ({ g } as t) =
    G.fold_vertex (fun pc acc -> if is_branching t pc then f pc acc else acc) g

  let fold_nom_branching f { g; meth } i =
    Body.fold_stms (fun pc acc stm ->
      if Stm.is_branching stm && G.out_degree g pc > 1
      then f pc ~out_degree:(G.out_degree g pc) acc
      else acc) i (Meth.body meth)

  let fold_sink_pred f ({ g; meth } as t) i =
    try G.fold_pred f g (sink t) i with
      | Invalid_argument _ -> i                         (* <- unreachable sink *)

  (* XXX: factorize all those functions: *)

  let is_branching_on_np ({ meth } as t) pc =
    let body = Meth.body meth in
    assert (Body.valid_pc body pc);
    track_excs && is_branching t pc && may_np pc (Body.stm_at_pc body pc)

  let is_branching_on_bad_cast ({ meth } as t) pc =
    let body = Meth.body meth in
    assert (Body.valid_pc body pc);
    track_excs && is_branching t pc &&
      unsafe_cast (Meth.decls meth) pc (Body.stm_at_pc body pc)

  let is_branching_on_negsz_alloc ({ meth } as t) pc =
    let body = Meth.body meth in
    assert (Body.valid_pc body pc);
    track_excs && is_branching t pc && may_negsz_alloc pc (Body.stm_at_pc body pc)

  let is_branching_on_aindx_error ({ meth } as t) pc =
    let body = Meth.body meth in
    assert (Body.valid_pc body pc);
    track_excs && is_branching t pc && may_aindx_error pc (Body.stm_at_pc body pc)

  (* --- *)

  module Fixpoint = struct
    module WTO = Graph.WeakTopological.Make (G)
    type wto =
      | Graph of pc WeakTopological.t
      | Vertex of pc
    type setup = wto * cfg
    let setup ({ g } as cfg) : setup =
      (if G.mem_vertex g PC.start && G.out_degree g PC.start > 0
       then Graph (WTO.recursive_scc g PC.start)
       else Vertex PC.start),
      cfg
    let cfg: setup -> cfg = snd

    module type T = FIXPOINT with type setup := setup

    module Make
      (D: Graph.ChaoticIteration.Data with type edge := G.edge)
      :
      (T with type data := D.t)
      =
    struct
      module F = Graph.ChaoticIteration.Make (G) (struct
        include D
        type edge = G.edge
      end)
      module M = F.M
      let apply ?(widening_set = Graph.ChaoticIteration.FromWto) init = function
        | Graph wto, g -> F.recurse (graph g) wto init widening_set 0
        | Vertex v, g -> M.singleton v (init v)
    end
  end

  open Graphviz
  open DotAttributes
  let output_dot fmt ({ g; meth } as cfg) =
    let pp_loc = pp_loc cfg ~escaped:true in
    let module D = Dot (struct
      include G
      let vertex_name = asprintf "v%i"
      let graph_attributes _ = []
      let default_vertex_attributes _ = [ `Shape `Box ]
      let default_edge_attributes _ = []
      let vertex_attributes pc = [ `Label (asprintf "%a" pp_loc pc) ]
      let edge_attributes (_, flow, _) = match flow with
        | Nominal -> []
        | Exceptional -> [ `Style `Dotted ]
      let get_subgraph pc = None
    end) in
    D.fprint_graph fmt g
end

(* -------------------------------------------------------------------------- *)

module Initialization (CFG: T) = struct
  open CFG
  let fold f fp_setup =
    let body = Meth.body (meth (Fixpoint.cfg fp_setup)) in
    let module Data = struct
      type t = Vars.t
      let join = Vars.union
      let equal = Vars.equal
      let analyze (pc, _, _) =
        assert (Body.valid_pc body pc);
        Vars.union (assigned_by (Body.stm_at_pc body pc))
      let widening = Vars.union
    end
    in
    let module FP = Fixpoint.Make (Data) in
    FP.apply (fun _ -> Vars.empty) fp_setup |> FP.M.fold f
end

(* -------------------------------------------------------------------------- *)

exception ForbiddenInstanceVariableAssignment of pc

(** Basic pointer-access analysis *)
module PointerAccess (CFG: T) = struct
  open CFG
  open Stm

  type color = NonNull | MaybeNull

  let fold_ref_accesses f fp_setup refs  =
    let meth = meth (Fixpoint.cfg fp_setup) in
    let body = Meth.body meth and decls = Meth.decls meth in
    let module Data = struct
      type t = color VarMap.t * Vars.t VarMap.t
      let join_colors = VarMap.union (fun _ a b -> match a, b with
        | NonNull, NonNull -> Some NonNull
        | _ -> Some MaybeNull)
      let join_classes = VarMap.union (fun _ a b -> Some (Vars.inter a b))
      let join (a, ac) (b, bc) = join_colors a b, join_classes ac bc
      let equal (a, _) (b, _) = VarMap.equal (==) a b
      open Expr
      open Expr.Cond
      let analyze ((pc, flow, pc'): G.edge) =
        let rec update_with = function
          | AssignR (r, Rlit Null)
            -> reset_ref pc r
          | AssignR (_, Rlit _)
          | LoadR (_, _, _) | ALoadR (_, _, _)
          | LoadP (_, _, _) | ALoadP (_, _, _)
          | StoreR (_, _, _) | AStoreR (_, _, _)
          | StoreP (_, _, _) | AStoreP (_, _, _)
          | CastR (_, _, _) | CastA (_, _, _)                            (* ? *)
          | New (_, _) | ANew (_, _, _)
          | ALen (_, _)
          | UFCall _
          | MFCallP (_, _)
          | MFCallR (_, _) when flow <> Nominal
              -> ign
          | AssignR (r, Rlit _)
          | New (r, _) | ANew (r, _, _)
          | Catch r
            -> nonnull_ref r
          | AssignR (r, R s)
          | CastR (r, _, s) | CastA (r, _, s)
            -> copy_ref r s
          | LoadP (_, r, _) | ALoadP (_, r, _)
          | StoreR (r, _, _) | AStoreR (r, _, _)
          | StoreP (r, _, _) | AStoreP (r, _, _)
          | ALen (_, r)
          | Throw r
            -> access_ref r
          | LoadR (s, r, _) | ALoadR (s, r, _)
            -> fun m -> m |> access_ref r |> reset_ref pc s
          | SLoadR (s, _, _)
            -> reset_ref pc s
          | UFCall ec
          | MFCallP (_, ec)
          | MFCallR (_, ec)
            -> if_methcall_deref access_ref ec
          | MonEnter r
          | MonExit r
            -> if_immediate_deref access_ref r
          | CondGoto (Rel (Var r, "==", Lit Null), l)
          | CondGoto (Rel (Lit Null, "==", Var r), l)
                when target l <> pc' -> nonnull_ref r
          | CondGoto (Rel (Var r, "==", Lit Null), l)
          | CondGoto (Rel (Lit Null, "==", Var r), l)
                when target l = pc' && pc' <> PC.succ pc -> reset_ref pc r
          | CondGoto (Rel (Var r, "==", Var r'), l)
                when target l = pc' && pc' <> PC.succ pc -> equal_refs r r'
          | CondGoto (Rel (Var r, "!=", Lit Null), l)
          | CondGoto (Rel (Lit Null, "!=", Var r), l)
                when target l <> pc' -> reset_ref pc r
          | CondGoto (Rel (Var r, "!=", Lit Null), l)
          | CondGoto (Rel (Lit Null, "!=", Var r), l)
                when target l = pc' && pc' <> PC.succ pc -> nonnull_ref r
          | CondGoto (Rel (Var r, "!=", Var r'), l)
                when target l <> pc' -> equal_refs r r'
          | Nop
          | AssignP _
          | SLoadP _
          | SStoreP _
          | SStoreR _
          | InstOfR _ | InstOfA _
          | CastP _
          | OutputPR _
          | ReturnU | ReturnR _ | ReturnP _
          | ChkPoint _
          | Goto _
          | CondGoto _
          | Switch _
            -> ign
          | Label (_, s)
            -> update_with s
        and aliases_of r c = try VarMap.find r c with Not_found -> Vars.empty
        and app_to_aliases_of f r d c = c
            |> VarMap.add d (f r (aliases_of d c))
        and rem_from_all_alias_classes r c = c
            |> Vars.fold (app_to_aliases_of Vars.remove r) (aliases_of r c)
            |> VarMap.add r Vars.empty
        and add_to_aliases_of r d c = c
            |> rem_from_all_alias_classes r
            |> app_to_aliases_of Vars.add r d
            |> VarMap.add r (Vars.singleton d)
        and fold_alias_class f r c i = i
            |> f r |> Vars.fold f (aliases_of r c)
        and reset_ref pc r (a, c) =
          if Var.equal r Meth.Sem.instvar
          then raise @@ ForbiddenInstanceVariableAssignment pc
          else VarMap.add r MaybeNull a, rem_from_all_alias_classes r c
        and equal_refs r r' ((a, c) as m) =
          if Var.equal r Meth.Sem.instvar then nonnull_ref r' m
          else if Var.equal r' Meth.Sem.instvar then nonnull_ref r m
          else try VarMap.add r (VarMap.find r' a) a, c with Not_found ->
            try VarMap.add r' (VarMap.find r a) a, c with Not_found -> m
        and nonnull_ref r (a, c) =
          fold_alias_class (fun r -> VarMap.add r NonNull) r c a, c
        and copy_ref r s (a, c) =
          (try VarMap.add r (VarMap.find s a) a with
            | Not_found -> VarMap.add r MaybeNull a),
          add_to_aliases_of r s c
        and access_ref r = nonnull_ref r in
        update_with (Body.stm_at_pc body pc)
      let widening = join
    end in
    let module FP = Fixpoint.Make (Data) in
    let init pc =
      let base_color = if PC.equal PC.start pc then MaybeNull else NonNull in
      let colors = VarMap.of_keys refs base_color in
      (if Meth.Sem.Decls.mem Meth.Sem.instvar decls
       then VarMap.add Meth.Sem.instvar NonNull colors
       else colors), VarMap.empty
    in
    FP.apply init fp_setup |> FP.M.fold begin fun pc (colors, _) ->
      if Body.valid_pc body pc then
        if_ref_access
          (fun stm r -> f pc stm r (VarMap.find r colors))
          (Body.stm_at_pc body pc)
      else ign
    end
end

(* -------------------------------------------------------------------------- *)

module PessimisticDerefInfos (Meth: Method.T) = struct
  module Meth = Meth
  type stm = (Security.t, nominal branch) Meth.Sem.stm
  let may_np pc stm = if_ref_access
    (fun _ r _ -> not (Var.equal r Meth.Sem.instvar)) stm false
end

(* -------------------------------------------------------------------------- *)

module PessimisticCastInfos (Meth: Method.T) = struct
  module Meth = Meth
  type stm = (Security.t, nominal branch) Meth.Sem.stm
  (* type decls = Meth.Sem.decls *)
  let unsafe_cast decls pc stm = if_unsafe_cast
    (fun stm _ _ -> match stm with
      | CastR (_, t, v) -> Meth.Sem.unsafe_cast decls (Typ.any t) v
      | CastA (_, t, v) -> Meth.Sem.unsafe_cast decls (Typ.any t) v
      | _ -> false) stm false
end

(* -------------------------------------------------------------------------- *)

module PessimisticArrayAllocInfos (Meth: Method.T) = struct
  module Meth = Meth
  type stm = (Security.t, nominal branch) Meth.Sem.stm
  let may_negsz_alloc pc stm = if_array_alloc
    (fun stm _ -> match stm with
      | ANew (_, _, idxs) when not (Vars.is_empty (vars_in_exprs idxs)) -> true
      | ANew (_, _, idxs)
        -> not (List.for_all (function | Expr.Lit (Int _) -> true | _ -> false) idxs)
      | _ -> false) stm false
end

(* -------------------------------------------------------------------------- *)

module PessimisticMethInfos (Meth: Method.T) = struct
  module Meth = Meth
  type stm = (Security.t, nominal branch) Meth.Sem.stm
  include (PessimisticDerefInfos (Meth) :
             DEREF_INFOS with module Meth := Meth and type stm := stm)
  include (PessimisticCastInfos (Meth) :
             CAST_INFOS with module Meth := Meth and type stm := stm)
  include (PessimisticArrayAllocInfos (Meth) :
             ARRAY_ALLOC_INFOS with module Meth := Meth and type stm := stm)
  let may_aindx_error pc stm = if_array_access (fun stm _ -> true) stm false
end

(* -------------------------------------------------------------------------- *)
