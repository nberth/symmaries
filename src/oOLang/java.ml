open Utils
open Typ
open TypStruct
open Base
open Semantics

(* --- *)

module Domain = struct

  (** Types with primitive domain and references to user-defined types only *)
  type t =
    | Val : prim typname -> t
    | Ref : user typname -> t
    | Arr : arry typname -> t

  type reftyp = RefTyp.t

  let to_typ: t -> any typname = function
    | Val t -> any t
    | Ref t -> any t
    | Arr t -> any t

  let rec of_typ:
  type k. k typname -> t = function
    | PrimTyp _ as t -> Val t                 (* primitive types become values *)
    | UserTyp _ as t -> Ref t                 (* user types become references  *)
    | ArryTyp _ as t -> Arr t
    | Any (_, t) -> of_typ t

  let boolean = PrimTyp "boolean"
  let byte = PrimTyp "byte"
  let int = PrimTyp "int"
  let char = PrimTyp "char"
  let long = PrimTyp "long"
  let float = PrimTyp "float"
  let double = PrimTyp "double"
  let objct = UserTyp "java.lang.Object"
  let string = UserTyp "java.lang.String"
  let clazz = UserTyp "java.lang.Class"
  let thrbl = UserTyp "java.lang.Throwable"
  let npexc = UserTyp "java.lang.NullPointerException"
  let rtexc = UserTyp "java.lang.RuntimeException"
  let ccexc = UserTyp "java.lang.ClassCastException"
  let negsz_exc = UserTyp "java.lang.NegativeArraySizeException"
  let aindx_exc = UserTyp "java.lang.ArrayIndexOutOfBoundsException"

  let anyref = UserTyp "<any reference>"

  (* convertibility of primitive types *)
  (* https://docs.oracle.com/javase/specs/jls/se7/html/jls-5.html#jls-5.1 *)
  let widens_to : prim typname -> prim typname -> bool = fun t t' ->
    PrimTyp.equal t t' || match t, t' with
      | PrimTyp "byte", (PrimTyp "short" | PrimTyp "int" | PrimTyp "long" | PrimTyp "float" | PrimTyp "double")
      | PrimTyp "short", (PrimTyp "int" | PrimTyp "long" | PrimTyp "float" | PrimTyp "double")
      | PrimTyp "char", (PrimTyp "int" | PrimTyp "long" | PrimTyp "float" | PrimTyp "double")
      | PrimTyp "int", (PrimTyp "long" | PrimTyp "float" | PrimTyp "double")
      | PrimTyp "long", (PrimTyp "float" | PrimTyp "double")
      | PrimTyp "float", PrimTyp "double" -> true
      | _ -> false

  let narrows_to : prim typname -> prim typname -> bool = fun t t' ->
    PrimTyp.equal t t' || match t, t' with
      | PrimTyp "short", (PrimTyp "byte" | PrimTyp "char")
      | PrimTyp "char", (PrimTyp "byte" | PrimTyp "short")
      | PrimTyp "int", (PrimTyp "byte" | PrimTyp "short" | PrimTyp "char")
      | PrimTyp "long", (PrimTyp "byte" | PrimTyp "short" | PrimTyp "char" | PrimTyp "int")
      | PrimTyp "float", (PrimTyp "byte" | PrimTyp "short" | PrimTyp "char" | PrimTyp "int" | PrimTyp "long")
      | PrimTyp "double", (PrimTyp "byte" | PrimTyp "short" | PrimTyp "int" | PrimTyp "long" | PrimTyp "float") -> true
      | _ -> false

  (* == narrows_or_widens_to *)
  let converts_to : prim typname -> prim typname -> bool = fun t t' ->
    t == t' || PrimTyp.equal t t' || match t, t' with
      | ((PrimTyp "byte" | PrimTyp "double"),
         (PrimTyp "byte" | PrimTyp "short" | PrimTyp "int"
             | PrimTyp "long" | PrimTyp "float" | PrimTyp "double"))
      | ((PrimTyp "short" | PrimTyp "char" | PrimTyp "int" | PrimTyp "long" | PrimTyp "float"),
         (PrimTyp "byte" | PrimTyp "short" | PrimTyp "char"
             | PrimTyp "int" | PrimTyp "long" | PrimTyp "float" | PrimTyp "double")) -> true
      | _ -> false

  let boolean_as_int : prim typname -> prim typname = function
    | PrimTyp "boolean" -> int
    | t -> t

  let ( !! ) = boolean_as_int
  let ( !!~ ) (type k) : k typname -> k typname = function
    | PrimTyp _ as t -> !!t
    | Any (Prim, t) -> Any (Prim, !!t)
    | t -> t

  (* numeric promotions: *)
  (* https://docs.oracle.com/javase/specs/jls/se7/html/jls-5.html#jls-5.6 *)

  let unary_promotion t = match t with
    | PrimTyp "byte" | PrimTyp "short" | PrimTyp "char" -> Some int
    | PrimTyp "int" | PrimTyp "long" | PrimTyp "float" | PrimTyp "double" -> Some t
    | _ -> None

  let binary_promotion t t' = match t, t' with
    | (PrimTyp "double" as t), t' when converts_to t' t -> Some t
    | t', (PrimTyp "double" as t) when converts_to t' t -> Some t
    | (PrimTyp "float" as t), t' when converts_to t' t -> Some t
    | t', (PrimTyp "float" as t) when converts_to t' t -> Some t
    | (PrimTyp "long" as t), t' when converts_to t' t -> Some t
    | t', (PrimTyp "long" as t) when converts_to t' t -> Some t
    | t, t' when converts_to t int && converts_to t' int  -> Some int
    | _ -> None

end

(* --- *)

module Decls = Decls.Make (Var) (AnyTyp)

(* --- *)

module FieldAttrs = struct
  module Spec = struct
    type t =
      | Instance
      | Static
    let compare = Stdlib.compare
    let equal a b = compare a b = 0
    let hash = Hashtbl.hash
    let print fmt = function
      | Instance -> pp fmt ""
      | Static -> pp fmt "static@ "
    let inheritable = function
      | Instance -> true
      | Static -> false
  end
  type t =
      {
        static_context: user typname option;
      }
  let compare { static_context = s1 } { static_context = s2 } =
    match s1, s2 with
      | None, None -> 0
      | Some t1, Some t2 -> UserTyp.compare t1 t2
      | Some _, None -> 1
      | None, Some _ -> -1
  let equal a b = compare a b = 0
  let hash { static_context = s } = Hashtbl.hash s
  let print fmt = function
    | { static_context = None } -> pp fmt ""
    | { static_context = Some _ } -> pp fmt "static@ "
  let from_spec t _ = function
    | Spec.Instance -> { static_context = None }
    | Spec.Static -> { static_context = Some t }
  let instance_field { static_context = s } = s = None
  let inheritable = instance_field
end

(* --- *)

module MethodLookup = Sign.CustomLookup (struct
  open Domain
  let rec compare:
  type x y. x typname -> y typname -> int = fun t t' -> match !!~t, !!~t' with
    | UserTyp _, UserTyp _ when (UserTyp.equal t anyref
                                 || UserTyp.equal t' anyref) -> 0
    | UserTyp _, ArryTyp _ when UserTyp.equal t anyref -> 0
    | ArryTyp _, UserTyp _ when UserTyp.equal t' anyref -> 0
    | Any (_, t), t' -> compare t t'
    | t, Any (_, t') -> compare t t'
    | t, t' -> TypName.compare t t'
end)

(* --- *)

module type TYPING = TypStruct.TREE with type fattrs = FieldAttrs.t

open RefTyp

module Semantics
  (Typing: TYPING)
  :
  (Semantics.S with module Decls = Decls
               and module FCall.Lookup = MethodLookup
               and type domain = Domain.t
               and type prmdom = prim typname
               and type refdom = Domain.reftyp) =
struct

  open Typing
  open Domain
  open Sign
  open Expr
  open Stm.Ops
  open Stm
  module Decls = Decls
  type var = Decls.var
  type decls = Decls.t
  type domain = Domain.t
  type prmdom = prim typname
  type refdom = Domain.reftyp
  type ('security, 'link) stm = ('security, 'link, domain typed) Stm.t

  (* --- *)

  let anyprim = prim "<primitive type>"
  let anyobj = user "java.lang.Object"

  (* --- *)

  let ( <: ) t t' = t <: t' || TypName.equal t' objct
  let ( <:@. ) a r = RefOps.( <: ) (A a) (U r) || UserTyp.equal r objct
  let ( <:.@ ) r a = RefOps.( <: ) (U r) (A a)

  let rec ( <:@ ):
  type k. arry typname -> arry typname -> bool = fun a a' -> match a, a' with
    | ArryTyp (Prim, t), ArryTyp (Prim, t') -> PrimTyp.equal t t'
    | ArryTyp (User, t), ArryTyp (User, t') -> t <: t'
    | ArryTyp (Arry, t), ArryTyp (Arry, t') -> t <:@ t'
    | _ -> false

  (* --- *)

  let dot_f_typ t f = fst (dot_f t f)
  let known_typ: any typname -> bool = fun t ->
    acc_usertyp ~recurse:true (fun t -> (&&) (mem_typ t)) t true

  (* --- *)

  type typ' =
    | PTyp : prim typname -> typ'
    | RTyp : user typname * imref -> typ'
    | Null : typ'
    | ATyp : arry typname * imref -> typ'

  let littyp: lit -> typ' = function
    | Int s -> (match Str.last_chars s 1 with
        | "l" | "L" -> PTyp long
        | _ -> PTyp int)
    | Flt s -> (match Str.last_chars s 1 with
        | "f" | "F" -> PTyp float
        | _ -> PTyp double)
    | Bool s -> PTyp boolean
    | Char s -> PTyp char
    | Str s as l -> RTyp (string, Rlit l)
    | Type t as l -> RTyp (clazz, Rlit l)
    | Null -> Null

  let typ': typ' -> any typname = function
    | Null -> any Domain.anyref
    | PTyp t -> any t
    | RTyp (t, _) -> any t
    | ATyp (t, _) -> any t

  let comparable: typ' -> typ' -> bool = fun t t' -> match t, t' with
    | Null, Null | Null, (RTyp _ | ATyp _) | (RTyp _ | ATyp _), Null
      -> true
    | PTyp t, PTyp t'
      -> (widens_to t t' || widens_to t' t || PrimTyp.equal (!!t) (!!t'))
    | RTyp (t, _), RTyp (t', _)
      -> (TypName.equal t t' || t <: t' || t' <: t)
    | ATyp (t, _), ATyp (t', _)
      -> (TypName.equal t t' || t <:@ t' || t' <:@ t)
    | ATyp _, RTyp (t, _) | RTyp (t, _), ATyp _
      -> (UserTyp.equal t objct)
    | _
      -> false

  (* --- *)

  let thisvar: var = "this"
  let excnvar: var = "throws"
  let excndom = U thrbl

  let decl v (t: any typname) =
    Decls.(merge ~redecl:(fun v _ _ -> raise (Redeclared v)) (var v t empty))

  let static_void = Decls.empty

  let init_decls = function
    | Sign.C (Method { sign = { clazz; kind }; arg_decls })
      -> let d = Decls.empty in
        let d = match kind with
          | Sign.Meth { static = true } -> d
          | Sign.Meth _ | Sign.Cstr -> decl thisvar (any clazz) d
        in
        let d = decl excnvar (any thrbl) d in
        List.fold_left (fun d (t, a) -> decl a t d) d arg_decls
    | Sign.I (Clinit _)
      -> decl excnvar (any thrbl) Decls.empty

  let args = function
    | Sign.C (Method { sign = { clazz; kind }; arg_decls }) ->
        let a = match kind with
          | Sign.Meth { static = true } -> VarMap.empty
          | Sign.Meth _ | Sign.Cstr -> VarMap.singleton thisvar (of_typ clazz)
        in
        List.fold_left (fun s (t, a) -> VarMap.add a (of_typ t) s) a arg_decls
    | Sign.I (Clinit _)
      -> VarMap.empty

  let prms decls =
    Decls.fold (fun v t m -> match of_typ t with
      | Val t -> VarMap.add v t m
      | _ -> m)
      decls VarMap.empty

  let refs decls =
    Decls.fold (fun v t m -> match of_typ t with
      | Ref t -> VarMap.add v (U t) m
      | Arr t -> VarMap.add v (A t) m
      | _ -> m)
      decls VarMap.empty

  module DeclStats: Semantics.STATS with type decls := decls = struct
    include Semantics.DeclStats
    let stats: type s. s stats -> decls -> s = fun s decls -> match s with
      | Cardinals -> { nb_prims = VarMap.cardinal (prms decls);
                      nb_refs = VarMap.cardinal (refs decls); }
  end

  let ret_some = Sign.Decl.ret_some
  let ret_refp s = match Sign.Decl.ret_typ s with
    | Some t -> (match of_typ t with Ref _ | Arr _ -> true | _ -> false)
    | _ -> false
  let ret_refdom s = match Sign.Decl.ret_typ s with
    | Some t -> (match of_typ t with
        | Ref t -> Some (U t)
        | Arr t -> Some (A t)
        | _ -> None)
    | _ -> None

  (* let nolits' = *)
  (*   List.fold_left (fun (pl, rl) -> function *)
  (*     | v, Domain.Val _ -> v :: pl, rl *)
  (*     | v, (Domain.Ref _ | Domain.Arr _) -> pl, v :: rl) ([], []) *)

  let nolits =
    List.fold_left (fun (pl, rl) (a, t) -> match !^a, t with
      | V v, Domain.Val _ -> v :: pl, rl
      | V v, (Domain.Ref _ | Domain.Arr _) -> pl, v :: rl
      | L _, _ -> pl, rl) ([], [])

  let vardom d v =
    of_typ (try Decls.find v d with Not_found -> raise (Undeclared v))

  (* --- *)

  let numeric_unary_promotion' pp_op t = match unary_promotion t with
    | Some t -> PTyp t
    | None -> raise @@ CustomError (fun fmt ->
      pp fmt "Cannot@ convert@ type@ %a@ in@ %t" PrimTyp.print t pp_op)

  let numeric_unary_promotion op =
    numeric_unary_promotion'
      (Rutils.PPrt.pp1 "application@ of numeric@ unary@ operation@ %s" op)

  let numeric_index_promotion =
    numeric_unary_promotion' (Rutils.PPrt.pp "index expression")

  (* --- *)

  let numeric_binary_promotion' pp_op t t' = match binary_promotion t t' with
    | Some t -> PTyp t
    | None -> raise @@ CustomError (fun fmt ->
      pp fmt "Cannot@ promote@ types@ %a@ and@ %a@ in@ %t\
             " PrimTyp.print t PrimTyp.print t' pp_op)

  let numeric_binary_promotion op =
    numeric_binary_promotion'
      (Rutils.PPrt.pp1 "application@ of numeric@ binary@ operation@ %s" op)

  (* --- *)

  let rec exprtyp d : unqual expr -> typ' = function
    | Lit l -> littyp l
    | Var v -> (match vardom d v with
        | Val t -> PTyp t
        | Ref t -> RTyp (t, R v)
        | Arr t -> ATyp (t, R v))
    | Unop (op, e) -> (match op, exprtyp d e with
        | ("+" | "-" | "~"), PTyp te ->
            numeric_unary_promotion op te
        | _, te -> te)
    | Binop (e, op, e') -> (match op, exprtyp d e, exprtyp d e' with
        |(("*" | "/" | "%" | "+" | "-" | "&" | "^" | "|"),
          PTyp te, PTyp te') ->
            numeric_binary_promotion op !!te !!te'
        |((">>" | ">>>" | "<<"),
          PTyp te, PTyp te') ->
            ignore (numeric_unary_promotion op te');
            numeric_unary_promotion op te
        | _, te, te' ->
            raise (TypeError (typ' te, typ' te')))
    | Relop (e, op, e') -> (match op, exprtyp d e, exprtyp d e' with
        |(("<" | "<=" | ">" | ">=" | "cmp" | "cmpl" | "cmpg" | "==" | "!="),
          PTyp te, PTyp te') ->
            ignore (numeric_binary_promotion op !!te !!te');
            PTyp byte
        | ("==" | "!="), te, te' when comparable te te' ->
            PTyp byte
        | _, te, te' ->
            raise (TypeError (typ' te, typ' te')))
    | Cast (t, e) -> (match t, exprtyp d e with
        | t, PTyp te when converts_to te t || converts_to !!te t ->
            PTyp t
        | t, te ->
            raise (TypeError (any t, typ' te)))

  let condtyp d : cond -> typ' = function
    | Cond.Rel (e, op, e') -> (match op, exprtyp d e, exprtyp d e' with
        |(("<" | "<=" | ">" | ">=" | "cmp" | "cmpl" | "cmpg" | "==" | "!="),
          PTyp te, PTyp te') ->
            ignore (numeric_binary_promotion op !!te !!te');
            PTyp boolean
        | ("==" | "!="), te, te' when comparable te te' ->
            PTyp boolean
        | _, te, te' ->
            raise (TypeError (typ' te, typ' te')))
    | Cond.Expr e -> exprtyp d e

  (* --- *)

  let check_int d e = match exprtyp d e with
    | PTyp t -> ignore (numeric_index_promotion !!t)
    | RTyp (t, r) -> raise @@ UnexpectedRef (r, Some (any t))
    | ATyp (t, r) -> raise @@ UnexpectedArr (r, t)
    | Null -> raise @@ UnexpectedRef (Rlit Null, None)

  type assign_expr = P | R of imref
  let assign_expr x x' = match x, x' with
    | Val t, PTyp t' when converts_to !!t' !!t -> P
    | Ref t, RTyp (t', r) when t' <: t -> R r
    | Arr t, ATyp (t', r) when t' <:@ t -> R r
    | Ref t, ATyp (t', r) when t' <:@. t -> R r
    | Ref _, Null | Arr _, Null -> R (Rlit Null)
    | tv, te -> raise @@ TypeError (to_typ tv, typ' te)
  let ( <-~ ) = assign_expr

  type assign_typ = P' of prim typname | R' of reftyp
  let assign_typ ?(allow_convert = false) ~strict_prim x x' = match x, x' with
    | Val t, Val t' when strict_prim && PrimTyp.equal t' t -> P' t'
    | Val t, Val t' when not strict_prim &&
        (if allow_convert then converts_to else widens_to) !!t' !!t -> P' t'
    | Ref t, Ref t' when t' <: t -> R' (U t')
    | Arr t, Arr t' when t' <:@ t -> R' (A t')
    | Ref t, Arr t' when t' <:@. t -> R' (A t')
    (* MISSING? Arr t, Ref t' *)
    | t, t' -> raise @@ TypeError (to_typ t, to_typ t')
  let ( <-? ) = assign_typ ~strict_prim:false
  let ( <-?~ ) = assign_typ ~strict_prim:false ~allow_convert:true
  let ( <-! ) = assign_typ ~strict_prim:true

  type assign_typ'= P | R
  let ( <-?. ) x x' : assign_typ' = match x <-? x' with P' _ -> P | _ -> R
  let ( <-!. ) x x' : assign_typ' = match x <-! x' with P' _ -> P | _ -> R

  (* TODO: For arrays, check
     https://docs.oracle.com/javase/specs/jvms/se7/html/jvms-6.html#jvms-6.5.checkcast
     (and maybe
     https://docs.oracle.com/javase/specs/jvms/se7/html/jvms-5.html#jvms-5.4.3.3) *)
  let casts_to x x' = match x, x' with
    | Val t, Val t' -> converts_to t t' || converts_to !!t t'
    | Ref t, Ref t' -> (* t <: t' || t' <: t *)may_alias (U t) (U t')
    | Arr t, Arr t' -> t <:@ t' || t' <:@ t || may_alias (A t) (A t')
    | Ref t, Arr t' -> t <:.@ t' || t' <:@. t || may_alias (U t) (A t')
    | Arr t, Ref t' -> t <:@. t' || t' <:.@ t || may_alias (A t) (U t')
    | _ -> false

  let safe_casts_to x x' = match x, x' with
    | Val _, Val _ -> true                        (* primtive casts never fail *)
    | Ref t, Ref t' -> t <: t'
    | Arr t, Arr t' -> t <:@ t'
    | Ref t, Arr t' -> t <:.@ t'
    | Arr t, Ref t' -> t <:@. t'
    | _ -> false

  (* --- *)

  let rec imdom:
  type k. decls -> k imm -> domain = fun d -> function
    | V v -> vardom d v
    | L l -> of_typ (typ' (littyp l))
    | C (t, l) -> ignore (of_typ t <-?~ imdom d l); of_typ t

  let instvar = thisvar

  module FCall = struct

    module Lookup = MethodLookup
    type t = (immediate, domain) effcall

    let invokevirt d fclazz name rtyp ?inst al =
      let inst = match inst with
        | None -> None
        | Some v -> Some (v, imdom d v)
      in
      { fclazz;
        fkind = VirtCall { inst; name; rtyp; };
        eff_args = List.map (fun v -> v, imdom d v) al }

    let invokecstr d fclazz this al =
      { fclazz;
        fkind = CstrCall { this };
        eff_args = List.map (fun v -> v, imdom d v) al }

    let invokespecial d fclazz this name rtyp al =
      { fclazz;
        fkind = SpecCall { this; name; rtyp; };
        eff_args = List.map (fun v -> v, imdom d v) al }

    let typsign { fclazz = clazz; fkind; eff_args } =
      let arg_typs = List.map (fun (_, t) -> to_typ t) eff_args in
      let kind = Sign.(match fkind with
        | VirtCall { inst = None; name; rtyp } ->
            Meth { static = true; name; rtyp }
        | VirtCall { inst = Some _; name; rtyp }
        | SpecCall { name; rtyp } ->
            Meth { static = false; name; rtyp }
        | CstrCall _ ->
            Cstr)
      in
      Sign.{ clazz; kind; arg_typs }

    type callsign = { methsign: sign; special: bool; }

    let methsign { methsign } = methsign

    let callsign ({ fkind } as ec) : callsign =
      let methsign = typsign ec in
      (match fkind with
        | SpecCall _ | CstrCall _ -> { methsign; special = true }
        | _ -> { methsign; special = false })

    let virtualdispatchp { methsign; special } =
      Sign.T.virtp methsign && not special

    let acc_lookupvirt f (Sign.{ clazz; arg_typs } as callsign) map init =
      let rec lookupwards t =
        fold_direct_suptyps lookup_once t
      and lookup_once t (hit, s, cache) =
        try hit || RefTypMap.find t cache, s, cache with Not_found ->
          let hit', s, cache = lookup_once' t (false, s, cache) in
          hit || hit', s, RefTypMap.add t hit' cache
      and lookup_once' = function
        | A _ as a -> lookupwards a
        | U clazz as u
          -> fun (hit, s, cache) ->
            let sign = Sign.{ callsign with clazz } in
            let hit, acc = try true, f (Lookup.find sign map) s with
              | Not_found -> hit, s in
            lookupwards u (hit, acc, cache)
      in
      fold_known_ultimate_subtyps
        begin fun t ((s, missing, n), cache) ->
          match lookup_once t (false, s, cache), t with
            | (false, s, che), U t when is_class t ->
                ((s, UserTyps.add t missing, succ n), che)
            | (_, s, che), _ -> ((s, missing, succ n), che)
        end
        clazz ((init, UserTyps.empty, 0), RefTypMap.empty) |> fst

    let acc_lookup f ({ methsign } as callsign) map init =
      if virtualdispatchp callsign
      then acc_lookupvirt f methsign map init
      else ((try f (Lookup.find methsign map) init with Not_found -> init),
            UserTyps.empty, 1)

    let lookup c map = acc_lookup List.cons c map []

    let print_typsign fmt ec = Sign.T.print' ~ret:false fmt (typsign ec)

    let concrete_methsign { methsign = Sign.{ clazz; kind; arg_typs } } =
      let utyp t = if UserTyp.equal t anyref then objct else t
      and ttyp = let anyref = any anyref and objct = any objct in
                 fun t -> if AnyTyp.equal t anyref then objct else t in
      let arg_typs = List.map ttyp arg_typs in
      Sign.{ clazz = utyp clazz; kind; arg_typs }

    let specialize clazz' ({ methsign = Sign.{ clazz } as ms } as cs) =
      { cs with methsign = { ms with clazz = clazz' } }

    let argmap: (immediate, domain) effcall -> callable declaration -> immediate argmap =
      Sign.argmap ~instvar:thisvar

    let call: callable declaration -> (var, domain) effcall =
      Sign.call ~instvar:thisvar of_typ

    let arg_prms_refs: (var, domain) effcall -> prmdom varmap * refdom varmap =
      fun { fclazz; fkind; eff_args } ->
        let r = match fkind with
          | VirtCall { inst = Some (t, Ref td) } -> VarMap.singleton t (U td)
          | VirtCall { inst = Some (t, Arr td) } -> VarMap.singleton t (A td)
          | CstrCall { this } -> VarMap.singleton this (U fclazz)
          | _ -> VarMap.empty
        in
        List.fold_left (fun (p, r) -> function
          | v, Val t -> VarMap.add v t p, r
          | v, Ref t -> p, VarMap.add v (U t) r
          | v, Arr t -> p, VarMap.add v (A t) r)
          (VarMap.empty, r)
          eff_args

    let dispatch_support_prms_refs = function
      | { fkind = VirtCall { inst = Some (this, _) } }
      | { fkind = CstrCall { this } }                (* for constructors too? *)
        -> [], [this]
      | _
        -> [], []

  end

  (* --- *)

  module FCalls
    : (SET with module Map = FCall.Lookup
           and type elt = FCall.callsign) =
  struct
    open FCall
    include MakeSet (Lookup) (struct
      type t = callsign
      let print fmt { methsign } = Sign.T.print' ~ret:false fmt methsign
      let sign = methsign
    end)
    let insert = function
      | { methsign; special = false } as x -> fun s ->
        (try if (find methsign s).special then insert x s else s
         with Not_found -> insert x s)
      | x -> insert x
  end

  (* --- *)

  let assign d (v, e) = match vardom d v <-~ exprtyp d e with
    | P -> AssignP (v, e)
    | R r -> AssignR (v, r)

  let load d (v, r, f) = match vardom d r with
    | Val _ | Arr _ -> raise @@ ExpectedRef r
    | Ref t -> match vardom d v <-?. of_typ (dot_f_typ t f) with
        | P -> LoadP (v, r, f)
        | R -> LoadR (v, r, f)

  let store d (r, f, e) = match vardom d r with
    | Val _ | Arr _ -> raise @@ ExpectedRef r
    | Ref t -> match of_typ (dot_f_typ t f) <-~ exprtyp d e with
        | P -> StoreP (r, f, e)
        | R r' -> StoreR (r, f, r')

  let sload d (v, c, f) = match vardom d v <-?. of_typ (dot_f_typ c f) with
    | P -> SLoadP (v, c, f)
    | R -> SLoadR (v, c, f)

  let sstore d (c, f, e) = match of_typ (dot_f_typ c f) <-~ exprtyp d e with
    | P -> SStoreP (c, f, e)
    | R r' -> SStoreR (c, f, r')

  let aload d (v, a, e) = match vardom d a with
    | Val _ | Ref _ -> raise @@ ExpectedArr a
    | Arr t -> check_int d e; match vardom d v <-?. of_typ (array_access t) with
        | P -> ALoadP (v, a, e)
        | R -> ALoadR (v, a, e)

  let astore d (a, e, e') = match vardom d a with
    | Val _ | Ref _ -> raise @@ ExpectedArr a
    | Arr t -> check_int d e; match of_typ (array_access t) <-~ exprtyp d e' with
        | P -> AStoreP (a, e, e')
        | R r' -> AStoreR (a, e, r')

  let alen d (v, a) = match vardom d v, vardom d a with
    | Val tv, _ when not (widens_to int tv) -> raise @@ TypeError (any tv, any int)
    | Val tv, Arr _ -> ALen (v, a)
    | Val tv, Ref t when array objct <:@. t -> ALen (v, a)
    | Ref tv, Arr _ -> raise @@ UnexpectedRef (R v, Some (any tv))
    | Arr tv, Arr _ -> raise @@ UnexpectedArr (R v, tv)
    | _, (Val _ | Ref _) -> raise @@ ExpectedArr a

  let instof d (v, r, u) = match vardom d v with
    | Ref tv -> raise @@ UnexpectedRef (R v, Some (any tv))
    | Arr tv -> raise @@ UnexpectedArr (R v, tv)
    | Val t when not (widens_to (boolean_as_int t) int) ->
        raise @@ TypeError (any boolean, any t)
    | Val _ -> let tr = vardom d r in match of_typ u with
        | Ref u as tu when casts_to tr tu -> InstOfR (v, r, u)
        | Arr u as tu when casts_to tr tu -> InstOfA (v, r, u)
        | tu -> raise @@ TypeError (to_typ tr, u)

  let cast d (v, t, u) =
    let tu = vardom d u in match vardom d v <-? of_typ t with
      | P' t when casts_to tu (of_typ t) -> CastP (v, t, u)
      | R' (U t) when casts_to tu (of_typ t) -> CastR (v, t, u)
      | R' (A t) when casts_to tu (of_typ t) -> CastA (v, t, u)
      | R' (U t) -> raise @@ DangerousCast ((v, t, u), to_typ tu)
      | _ -> raise @@ TypeError (t, to_typ tu)

  let unsafe_cast d t u =
    not (safe_casts_to (vardom d u) (of_typ t))

  let ucall d (r, m, vl) = match vardom d r with
    | Val _ | Arr _ -> raise @@ ExpectedRef r
    | Ref t -> UFCall (FCall.invokevirt d t m None ~inst:(V r) vl)

  let fcall' = FCall.invokevirt ?inst:None
  let ucall' d (c, m, vl) = UFCall (fcall' d c m None vl)

  let uscall d (r, c, m, vl) = match vardom d r, m with
    | (Val _ | Arr _), _ -> raise @@ ExpectedRef r
    | Ref t, None when t <: c -> UFCall (FCall.invokecstr d c (V r) vl)
    | Ref t, Some m when t <: c -> UFCall (FCall.invokespecial d c (V r) m None vl)
    | t, _ -> raise @@ TypeError (to_typ t, to_typ (Ref c))

  let mcall d (v, ct, r, m, vl) = match vardom d r with
    | Val _ -> raise @@ ExpectedRef r
    | Ref _ | Arr _ as t
      -> let m = FCall.invokevirt d (match t with Ref t -> t | _ -> objct) m in
        let m = m ~inst:(V r) in
        match vardom d v, ct with
          | Val rt, None ->
              MFCallP (v, m (Some (any rt)) vl)
          | Val rt, Some ct when Val rt <-?. of_typ ct = P ->           (* ??? *)
              MFCallP (v, m (Some anyprim) vl)
          | rt, None ->
              MFCallR (v, m (Some (to_typ rt)) vl)
          | rt, Some ct when rt <-?. of_typ ct <> P ->
              MFCallR (v, m (Some anyobj) vl)
          | rt, Some ct ->
              raise @@ TypeError (ct, to_typ rt)

  let mcall' d (v, ct, c, m, vl) = match vardom d v, ct with
    | Val rt, None ->
        MFCallP (v, fcall' d c m (Some (any rt)) vl)
    | Val rt, Some ct when Val rt <-?. of_typ ct = P ->                 (* ??? *)
        MFCallP (v, fcall' d c m (Some anyprim) vl)
    | rt, None ->
        MFCallR (v, fcall' d c m (Some (to_typ rt)) vl)
    | rt, Some ct when rt <-?. of_typ ct <> P ->
        MFCallR (v, fcall' d c m (Some anyobj) vl)
    | rt, Some ct ->
        raise @@ TypeError (ct, to_typ rt)

  let mscall d (v, ct, r, c, m, vl) = match vardom d r with
    | Val _ -> raise @@ ExpectedRef r
    | Ref _ | Arr _
      -> let s = FCall.invokespecial d c (V r) in match vardom d v, ct with
        | Val rt, None ->
            MFCallP (v, s m (Some (any rt)) vl)
        | Val rt, Some ct when Val rt <-?. of_typ ct = P ->             (* ??? *)
            MFCallP (v, s m (Some anyprim) vl)
        | rt, None ->
            MFCallR (v, s m (Some (to_typ rt)) vl)
        | rt, Some ct when rt <-?. of_typ ct <> P ->
            MFCallR (v, s m (Some anyobj) vl)
        | rt, Some ct ->
            raise @@ TypeError (ct, to_typ rt)

  let new_ d (r, (UserTyp _ as c)) = match vardom d r with
    | Val _ | Arr _ -> raise @@ ExpectedRef r
    | Ref t when c <: t -> New (r, c)
    | Ref _ as t -> raise @@ TypeError (to_typ t, to_typ (Ref c))

  let anew d (r, c, dims) = match vardom d r with
    | Val _ | Ref _ -> raise @@ ExpectedArr r
    | Arr t ->
        let elemtyp = List.fold_left begin fun t' e ->
          check_int d e;
          match t' with None -> None | Some t' ->
            match as_array t' with None -> None
              | Some a -> Some (array_access a)
        end (Some (any t)) dims in
        match elemtyp with
          | Some elemtyp -> (match of_typ elemtyp <-!. of_typ c with
              | P | R -> ANew (r, c, dims))
          | None -> raise @@ WrongArrayDims
              { given_array_dims = List.length dims;
                expected_array_dims = array_dims t - array_dims c }

  let output d (l, vl) =
    OutputPR (l, List.map (fun v -> v, imdom d v) vl)

  let return d rtyp e = match rtyp with
    | Some None -> raise UnexpectedRet
    | None -> (match exprtyp d e with
        | PTyp _ -> ReturnP e
        | RTyp (_, r) -> ReturnR r
        | ATyp (_, r) -> ReturnR r
        | Null -> ReturnR (Rlit Null))
    | Some (Some rt) -> match of_typ rt <-~ exprtyp d e with
        | P -> ReturnP e
        | R r -> ReturnR r

  let returnu d rtyp = match rtyp with
    | None | Some None -> ReturnU
    | Some Some t -> raise MissingRetVal

  let cond_goto d (c, l) = match condtyp d c with
    | PTyp _ -> CondGoto (c, l)
    | RTyp (t, r) -> raise @@ UnexpectedRef (r, Some (any t))
    | ATyp (t, r) -> raise @@ UnexpectedArr (r, t)
    | Null -> raise @@ UnexpectedRef (Rlit Null, None)

  let switch d (c, se) =
    check_int d c;
    Switch (c, se)

  let chkthrwbl t = if not (t <: thrbl) then raise @@ ExpectedThrwbl (None, t)
  let chkthrwbls = UserTyps.iter chkthrwbl
  let chkthrows d = chkthrwbls (Sign.Decl.throws d)

  let chkcatch d r = function
    | t when not (t <: thrbl) -> raise @@ ExpectedThrwbl (None, t)
    | exctyp -> (match vardom d r <-! of_typ exctyp with
        | P' _ -> raise @@ ExpectedRef r
        | R' (A t) -> raise @@ UnexpectedArr (R r, t)
        | R' (U t) when not (t <: thrbl) -> raise @@ ExpectedThrwbl (Some r, t)
        | R' (U t) -> ())

  let catch d r =
    let rt = vardom d r in
    if casts_to (of_typ thrbl) rt then Catch r
    else raise @@ ExpectedThrwbl (Some r, thrbl)

  let excdom d r = match vardom d r with
    | Val _ | Arr _ -> raise @@ ExpectedRef r
    | Ref t when not (t <: thrbl) -> raise @@ ExpectedThrwbl (Some r, t)
    | Ref t -> t

  let throw d r = ignore (excdom d r); Throw r

  let monitorcheck d i = match imdom d i with
    | Ref _ | Arr _ -> i
    | it -> raise @@ TypeError (any Domain.anyref, to_typ it)
  let monitorenter d i = MonEnter (monitorcheck d i)
  let monitorexit d i = MonExit (monitorcheck d i)

  (* --- *)

  (* First phase to resolve qualified identifiers: *)

  let expr d : qual expr -> unqual expr = function
    | Ident q
      -> let qs = QualIdent.to_string q in
        if QualIdent.unqual q && Decls.mem qs d
        then Var qs
        else Lit (Type (any (UserTyp qs)))
    | Lit _ as e -> e
    | Unop _ as e -> e
    | Binop _ as e -> e
    | Relop _ as e -> e
    | Cast _ as e -> e

  let assignq d (l, r) = match QualIdent.dequal l, r with
    | (Some q', f), e
      -> let r = QualIdent.to_string q' in
        if QualIdent.unqual q' && Decls.mem r d
        then store d (r, f, expr d e)
        else sstore d (UserTyp r, f, expr d e)
    | (None, v), Ident q
      -> begin match QualIdent.dequal q with
        | None, u -> assign d (v, Expr.Var u)
        | Some q', f ->
            let r = QualIdent.to_string q' in
            if QualIdent.unqual q' && Decls.mem r d
            then load d (v, r, f)
            else try sload d (v, UserTyp r, f) with
              | UnknownType (UserTyp r') when r' == r ->
                  let t = UserTyp (QualIdent.to_string q) in
                  if mem_typ t
                  then assign d (v, expr d (Lit (Type (any t))))
                  else raise @@ UndeclaredQualIdent q
      end
    | (None, v), e
      -> assign d (v, expr d e)

  let ucallq d (q, m, vl) = match QualIdent.dequal q with
    | None, r when Decls.mem r d -> ucall d (r, m, vl)
    | _ -> ucall' d (UserTyp (QualIdent.to_string q), m, vl)

  let uscallq d (v, q, m, vl) =
    let c = UserTyp (QualIdent.to_string q) in
    let c, m = match QualIdent.dequal q with
      | Some q', m' when m = None && not (mem_typ c) ->
          (* Implicitly guess that it's not a call to a constructor: *)
          (* TODO: find a way to emit a deprecability warning. *)
          UserTyp (QualIdent.to_string q'), Some m'
      | _ -> c, m
    in
    uscall d (v, c, m, vl)

  let mcallq d (v, ct, q, m, vl) = match QualIdent.dequal q with
    | None, r when Decls.mem r d -> mcall d (v, ct, r, m, vl)
    | _ -> mcall' d (v, ct, UserTyp (QualIdent.to_string q), m, vl)

  (* --- *)

  let rec type_stm ?rtyp d = function
    |(Nop | ChkPoint _ | Goto _) as s -> d, s
    | Decl (t, vl) -> List.fold_left (fun d v -> decl v t d) d vl, Nop
    | Assign (qi, e) -> d, assignq d (qi, e)
    | ALoad (v, a, e) -> d, aload d (v, a, e)
    | AStore (a, e, v) -> d, astore d (a, e, expr d v)
    | ALen (v, a) -> d, alen d (v, a)
    | UCall (q, m, vl) -> d, ucallq d (q, m, vl)
    | USCall (r, q, m, vl) -> d, uscallq d (r, q, m, vl)
    | MCall (v, t, q, m, vl) -> d, mcallq d (v, t, q, m, vl)
    | MSCall (v, t, q, c, m, vl) -> d, mscall d (v, t, q, c, m, vl)
    | New (r, c) -> d, new_ d (r, c)
    | ANew (r, t, vl) -> d, anew d (r, t, vl)
    | InstOf (v, r, t) -> d, instof d (v, r, t)
    | Cast (v, t, u) -> d, cast d (v, t, u)
    | Output (l, vl) -> d, output d (l, vl)
    | Return e -> d, return d rtyp (expr d e)
    | ReturnU -> d, returnu d rtyp
    | CondGoto (c, l) -> d, cond_goto d (c, l)
    | Switch (c, sw) -> d, switch d (c, sw)
    | Throw r -> d, throw d r
    | Catch r -> d, catch d r
    | MonEnter r -> d, monitorenter d r
    | MonExit r -> d, monitorexit d r
    | Label (l, s) -> let d, s = type_stm ?rtyp d s in d, Label (l, s)

  (* --- *)

  let on_exc ct ?(exctyps: UserTyps.t option) pc =
    let chk_catching t = optp true (UserTyps.exists (fun e -> e <: t || t <: e))
    and chk_caught t = UserTyps.exists (fun e -> e <: t || t <: e)
    and chk_all_caught caught = function
      | None -> UserTyps.mem thrbl caught
      | Some s -> UserTyps.for_all (fun e -> UserTyps.exists ((<:) e) caught) s
    in
    (match exctyps with None -> () | Some ts -> chkthrwbls ts);
    CatchTable.fold_mapi
      begin fun i { catch_typ; catch_from; catch_to; catch_at } caught ->
        if (pc >= target catch_from && pc < target catch_to
            && chk_catching catch_typ exctyps
            && not (chk_caught catch_typ caught))
        then Some (i, catch_at), UserTyps.add catch_typ caught
        else None, caught
      end ct UserTyps.empty |> begin fun (handlers, caught) ->
        handlers, chk_all_caught caught exctyps (* incomplete test, but enough… *)
      end

  let thrbltyp = thrbl
  let npexctyp = npexc
  let ccexctyp = ccexc
  let negsztyp = negsz_exc
  let aindxtyp = aindx_exc
  let unchecked_excntyps = UserTyps.singleton rtexc

  let add_throw s t =
    let throws = Sign.Decl.throws s in
    if UserTyps.exists ((<:) t) throws then s
    else Sign.Decl.add_throw s t

  let add_throws s1 t2 =
    let t1 = Sign.Decl.throws' s1 in
    let tx = UserTyps.(fold (fun t t1 ->
      if exists ((<:) t) t1 then t1 else add t t1) t1 t2) in
    UserTyps.fold (fun t s -> Sign.Decl.add_throw' s t) tx s1

  let merge_throws s1 s2 = Sign.(match s1 with
    | C s1 -> C (add_throws s1 (Sign.Decl.throws s2))
    | I s1 -> I (add_throws s1 (Sign.Decl.throws s2)))

  module RefRels = struct
    let may_alias = may_alias
    let may_field_alias = may_field_alias
    let may_share_mutable = may_share_mutable
    let may_field_share_mutable = may_field_share_mutable
    ;;

    assert (may_field_alias
              (A (array (array (PrimTyp "int"))))
              (A (array (PrimTyp "int"))));;
    assert (may_field_alias
              (A (array (array (array (PrimTyp "int")))))
              (A (array (PrimTyp "int"))));;
    assert (may_field_alias
              (A (array (array (array (PrimTyp "int")))))
              (U objct));;
    (* assert (not (may_field_alias *)
    (*              (A (array (array (array (PrimTyp "int"))))) *)
    (*              (U (UserTyp "IntArray"))));; *)
    assert (may_field_share_mutable
              (A (array (array (PrimTyp "int"))))
              (A (array (PrimTyp "int"))));;
    assert (may_field_share_mutable
              (A (array (array (array (PrimTyp "int")))))
              (A (array (PrimTyp "int"))));;
    assert (may_share_mutable
              (A (array (array (array (PrimTyp "int")))))
              (U objct));;
    assert (may_field_share_mutable
              (A (array (array (array (PrimTyp "int")))))
              (U objct));;
    assert (may_field_share_mutable
              (U objct)
              (A (array (array (array (PrimTyp "int"))))));;
    assert (may_field_share_mutable                  (* well… always assumed? *)
              (U objct)
              (A (array (PrimTyp "int"))));
  end

end

(* --- *)

module Custom: CUSTOM = struct
  let obj = U Domain.objct
  let clonable = U (UserTyp "java.lang.Cloneable")
  let serializable = U (UserTyp "java.io.Serializable")
  let exc = U (UserTyp "java.lang.Exception")
  let throwable = U Domain.thrbl
  let runtime_exc = U Domain.rtexc
  let null_pointer_exc = U Domain.npexc
  let class_cast_exc = U Domain.ccexc
  let neg_arr_size_exc = U Domain.negsz_exc
  let index_exc = U (UserTyp "java.lang.IndexOutOfBoundsException")
  let aindx_exc = U Domain.aindx_exc
  let arith_exc = U (UserTyp "java.lang.ArithmeticException")
  let array_sup = RefTyps.empty
    |> RefTyps.add obj
    |> RefTyps.add clonable
    |> RefTyps.add serializable
  let thrwbl_sup = RefTyps.empty
    |> RefTyps.add obj
    |> RefTyps.add serializable
  let exc_sup = RefTyps.singleton throwable
  let runtime_exc_sup = RefTyps.singleton exc
  let null_pointer_exc_sup = RefTyps.singleton runtime_exc
  let class_cast_exc_sup = RefTyps.singleton runtime_exc
  let neg_arr_size_exc_sup = RefTyps.singleton runtime_exc
  let index_exc_sup = RefTyps.singleton runtime_exc
  let aindx_exc_sup = RefTyps.singleton index_exc
  let arith_exc_sup = RefTyps.singleton runtime_exc
  let suptyps = RefTypMap.empty
    |> RefTyps.fold (fun t -> RefTypMap.add t RefTyps.empty) array_sup
    |> RefTypMap.add throwable thrwbl_sup
    |> RefTypMap.add exc exc_sup
    |> RefTypMap.add runtime_exc runtime_exc_sup
    |> RefTypMap.add null_pointer_exc null_pointer_exc_sup
    |> RefTypMap.add class_cast_exc class_cast_exc_sup
    |> RefTypMap.add neg_arr_size_exc neg_arr_size_exc_sup
    |> RefTypMap.add index_exc index_exc_sup
    |> RefTypMap.add aindx_exc aindx_exc_sup
    |> RefTypMap.add arith_exc arith_exc_sup
  let suptyps_of = function
    | A _ -> array_sup
    | _ -> RefTyps.empty
  let array_inheritance = ArrayInheritByElementType
  let immutable_typs = RefTyps.empty
    |> RefTyps.add (U (UserTyp "java.lang.String"))
end

module AnyFieldAttrs = struct type typspec = any include FieldAttrs end
module PrimFieldAttrs = struct type typspec = prim include FieldAttrs end

module FlatHeap = Flat (AnyFieldAttrs)
module ClassesOfPrim = ClassOfPrimFields (PrimFieldAttrs) (Custom)
module Heap = Heap (AnyFieldAttrs) (Custom)

(* --- *)

module UnsafeTypingParams = TypStruct.DefaultParams
module SafeTypingParams: PARAMS = struct
  let interface_definitions =
    ExcludeInheriting (UserTyps.singleton Domain.objct)
  let interface_inheritance =
    UserTyps.singleton Domain.objct
end

(* --- *)

module FieldAliasPeek (H: TYPING) = struct
  let peeks =
    [
      ("field_alias_in_degree(java.lang.Object)",
       H.field_alias_rel_indegree (U Domain.objct));
      ("field_alias_in_degree(java.lang.Object[])",
       H.field_alias_rel_indegree (A (array Domain.objct)));
      ("field_alias_in_degree(java.util.Collection)",
       H.field_alias_rel_indegree (U (UserTyp "java.util.Collection")));
      ("field_access_in_degree(java.lang.Object)",
       H.field_access_rel_indegree (U Domain.objct));
      ("field_access_in_degree(java.lang.Object[])",
       H.field_access_rel_indegree (A (array Domain.objct)));
      ("field_access_in_degree(java.util.Collection)",
       H.field_access_rel_indegree (U (UserTyp "java.util.Collection")));
    ]
end
