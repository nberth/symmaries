open Utils
open Typ
open Base

type sign =
    {
      kind: kind;
      clazz: user typname;
      arg_typs: any typname list;
    }
and kind =
  | Meth of
      {
        static: bool;
        name: methname;
        rtyp: any typname option;
      }
  | Cstr

module T: sig
  include ORDERED_HASHABLE_TYPE with type t = sign
  val print': ?nl: bool -> ?ret: bool -> t pp
  val print_nnl: t pp
  val print_shortname: t pp
  val meth: static:bool -> user typname -> methname -> any typname option
    -> any typname list -> sign
  val cstr: user typname -> any typname list -> sign
  val virtp: sign -> bool
  val fold_typs: (any typname -> 'a -> 'a) -> sign -> 'a -> 'a
end
module type LOOKUP = MAP with type key = sign
module Map: LOOKUP
open T

(* --- *)

module type CUSTOM_TYPE_CMP = sig
  val compare: any typname -> any typname -> int
end

module CustomLookup:
  functor (ArgTypCmp: CUSTOM_TYPE_CMP) -> LOOKUP

(* --- *)

module type SET = sig
  module Map: LOOKUP
  type elt
  type t = elt Map.t
  val empty: t
  val is_empty: t -> bool
  val insert: elt -> t -> t
  val mem: sign -> t -> bool
  val find: sign -> t -> elt
  val iter: (elt -> unit) -> t -> unit
  val fold: (elt -> 'a -> 'a) -> t -> 'a -> 'a
  val filter: (elt -> bool) -> t -> t
  val print: t pp
  val update: t -> t -> t
end

module MakeSet (L: LOOKUP) (E: sig
  type t
  val print: t pp
  val sign: t -> sign
end) : (SET with module Map = L and type elt = E.t)

module Set: (SET with type elt = sign)

(* --- *)

type ('a, 'i) xsign =
  | Callable of 'a
  | Implicit of 'i

module type EXTENDED_SIGN = sig
  type t
  module K: ORDERED_HASHABLE_TYPE
  val print: t pp
  val sign: t -> (sign, K.t) xsign
end

module type EXTENDED_SET = sig
  module Map: LOOKUP
  type elt
  type callables = elt Map.t
  type ik
  module IMap: MAP with type key = ik
  type implicits = elt IMap.t
  type t
  val empty: t
  val is_empty: t -> bool
  val insert: elt -> t -> t
  val mem: (sign, ik) xsign -> t -> bool
  val find: (sign, ik) xsign -> t -> elt
  val iter: (elt -> unit) -> t -> unit
  val fold: (elt -> 'a -> 'a) -> t -> 'a -> 'a
  val filter: (elt -> bool) -> t -> t
  val print: t pp
  val update: t -> t -> t
  val callables: t -> callables
  val implicits: t -> implicits
end

module MakeExtendedSet (L: LOOKUP) (E: EXTENDED_SIGN)
  : (EXTENDED_SET with module Map = L
                  and  type elt = E.t
                  and  type ik = E.K.t)

(* --- *)

type callable = C
type implicit = I
type _ declaration =
  | Method : { sign: sign; arg_decls: arg_decls; throws: excs } -> callable declaration
  | Clinit : { clazz: user typname; throws: excs } -> implicit declaration
and arg_decls = (any typname * var) list
and excs = UserTyps.t
and exc = user typname

type decl =
  | C : callable declaration -> decl
  | I : implicit declaration -> decl
type callable_decl = callable declaration
type implicit_decl = implicit declaration

module Decl: sig
  include ORDERED_HASHABLE_TYPE with type t = decl
  val print_nnl: t pp
  val print_shortname: t pp
  val meth: static: bool -> user typname -> methname -> any typname option
    -> (any typname * var) list -> excs -> callable declaration
  val cstr: user typname -> (any typname * var) list -> excs -> callable declaration
  val clinit: user typname -> excs -> implicit declaration
  val sign: t -> (sign, user typname) xsign
  val of_sign: ?throws: excs -> sign -> callable declaration
  val subst_args': (var -> var) -> callable declaration -> callable declaration
  val subst_args: (var -> var) -> decl -> decl
  val clazz: t -> user typname
  val args: t -> var list
  val ret_some: t -> bool
  val ret_typ: t -> any typname option
  val throws': 'a declaration -> excs
  val throws: t -> excs
  val add_throw': 'a declaration -> exc -> 'a declaration
  val add_throw: t -> exc -> t
end

module DeclMap: MAP with type key = Decl.t

(* --- *)

type ('effarg_name, 'effarg_dom) effcall =
    {
      fclazz: user typname;
      fkind: ('effarg_name, 'effarg_dom) fkind;
      eff_args: ('effarg_name * 'effarg_dom) list;
    }
and ('effarg_name, 'effarg_dom) fkind =
  | VirtCall of
      {
        inst: ('effarg_name * 'effarg_dom) option;           (* None ≡ static *)
        name: methname;
        rtyp: any typname option;
      }
  | CstrCall of
      {
        this: 'effarg_name;
      }
  | SpecCall of
      {
        this: 'effarg_name;
        name: methname;
        rtyp: any typname option;
      }

val call: instvar: var -> (any typname -> 'domain) -> callable declaration -> (var, 'domain) effcall

type 'a argmap =
    {
      formal2eff: 'a varmap;
    }
val argmap: instvar: var -> ('a, 'domain) effcall -> callable declaration -> 'a argmap
