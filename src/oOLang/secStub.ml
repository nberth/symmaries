open Format
open Utils
open Typ
open Base
open Sign

(* --- *)

type taint = bool
type tagging =
  | TL of Security.t
  | TT
  | TLT of Security.t

let default_tag = TL Security.Bot

let (!~?) = function
  | None -> default_tag
  | Some t -> t

type policy = [ `Level of var option | `Taint of var option | `Both of var option * var option ]

type static_res_deps = [ `None | `Args | `Any | `Policy of policy ]
type virtual_res_deps = [ static_res_deps | `DotStar ]

type 'r static_result =
  [
  | `Unit
  | `New of any typname * tagging * 'r
  | `Any of any typname * tagging
  | `Arg of any typname
  | `Prim of prim typname * tagging * bool                  (* bool: arg-only *)
  | `FreshPrim of prim typname * tagging * 'r
  ]
type virtual_result =
  [
  | virtual_res_deps static_result
  | `PField of prim typname * tagging
  | `RField of any typname * tagging
  ]

type static_trst = [ `None | `Args ]
type virtual_trst = [ static_trst | `Fields ]

type t =
    {
      kind: kind;
      clazz: user typname;
      arg_typs: any typname list;
      throws: excs;
      output: output option;
      mutarg: mutation option;
    }
and kind =
  | VirtMeth of
      {
        name: methname;
        rslt: virtual_result;
        mutobj: mutation option;
        trst: virtual_trst;
      }
  | StaticMeth of
      {
        name: methname;
        rslt: static_res_deps static_result;
        trst: static_trst;
      }
  | Constructor of
      {
        mutobj: mutation option;
        trst: static_trst;
      }
and output = Security.t
and mutation = [ `Tag of tagging | `FromArgs | `Policy of policy ]

(* --- *)

type virt
type static
type constr
type 'k context =
  | Static : static context
  | Virtual : virt context
  | Constr : constr context

let pp_context (type k) : k context pp = fun fmt -> function
  | Static -> pp fmt "static@ method"
  | Virtual -> pp fmt "virtual@ method"
  | Constr -> pp fmt "constructor"

(* --- *)

type static_res = [ `New of tagging option * virtual_res_deps (* XXX: lazily don't bother with GADTs *)
                  | `Star of tagging option
                  | `Arg ]
type virtual_res= [ static_res
                  | `DotStar of tagging option ]
type static_mut = [ `None
                  | `Args of mutation
                  | `Any of mutation ]
type object_mut = [ `Fields of mutation ]
type stm =
  | Output : output -> stm
  | Mutate : [ static_mut | object_mut ] -> stm
  | Result : [ static_res | virtual_res ] -> stm
  | Trust : [ static_trst | virtual_trst ] -> stm

let pp_tagging : tagging pp = fun fmt -> function
  | TL l -> Security.print fmt l
  | TT -> pp fmt "?"
  | TLT l -> pp fmt "%a?" Security.print l

let pp_tagging': tagging option pp = fun fmt -> function
  | None -> pp fmt "none"
  | Some t -> pp_tagging fmt t

let pp_policy = fun fmt -> function
  | `Level t -> pp fmt "%%(level:%a)" (pp_opt "%a" Var.print) t
  | `Taint t -> pp fmt "%%(taint:%a)" (pp_opt "%a" Var.print) t
  | `Both (l, t) when l = t -> pp fmt "%%(*:%a)" (pp_opt "%a" Var.print) t
  | `Both (l, t) -> pp fmt "%%(level:%a;taint:%a)\
                          " (pp_opt "%a" Var.print) l (pp_opt "%a" Var.print) t

let pp_res_deps fmt = function
  | `None -> ()
  | `Args -> pp fmt "(@)"
  | `DotStar -> pp fmt "(.*)"
  | `Any -> pp fmt "(*)"
  | `Policy p -> pp fmt "(%a)" pp_policy p

let pp_res fmt = function
  | `New (None, d) -> pp fmt "new%a" pp_res_deps d
  | `New (Some t, d) -> pp fmt "new_%a%a" pp_tagging t pp_res_deps d
  | `Star None -> pp_print_char fmt '*'
  | `Star (Some t) -> pp fmt "*%a" pp_tagging t
  | `Arg -> pp_print_char fmt '@'
  | `DotStar None -> pp_print_string fmt ".*"
  | `DotStar (Some t) -> pp fmt ".*%a" pp_tagging t

let pp_source = fun fmt -> function
  | `FromArgs -> pp_print_char fmt '@'
  | `Tag t -> pp_tagging fmt t
  | `Policy p -> pp_policy fmt p

let pp_mut = fun fmt -> function
  | `None -> pp fmt "- <~"
  | `Any x -> pp fmt "* <~%a" pp_source x
  | `Args x -> pp fmt "@@ <~%a" pp_source x
  | `Fields x -> pp fmt ".* <~%a" pp_source x

let pp_trst fmt = function
  | `None -> pp fmt "-!"
  | `Args -> pp fmt "@@!"
  | `Fields -> pp fmt ".*!"

let pp_stm: stm pp = fun fmt -> function
  | Output l -> pp fmt "output_%a" Security.print l
  | Result r -> pp fmt "return %a" pp_res r
  | Mutate m -> pp_mut fmt m
  | Trust t -> pp_trst fmt t

(* --- *)

let acc_tagging: tagging option -> tagging option -> tagging option =
  fun a b -> Some (match !~?a, !~?b with
    | TT, TT -> TT
    | TL a, TL b -> TL (Security.lub a b)
    | TL a, TLT b | TLT a, TL b | TLT a, TLT b -> TLT (Security.lub a b)
    | TT, (TL a | TLT a) | (TL a | TLT a), TT -> TLT a)

let acc_policies: policy -> policy -> policy =
  fun p1 p2 -> match p1, p2 with
    | `Level l, `Taint t | `Taint t, `Level l
    | `Level l, `Both (_, t) | `Taint t, `Both (l, _)
    | `Both (_, t), `Level l | `Both (l, _), `Taint t -> `Both (l, t)
    | `Level _, `Level _ | `Taint _, `Taint _ | `Both _, `Both _ -> p2

(* --- *)

type unexp =
  | Unexp : 'k context * stm -> unexp

exception UnexpectedStubDescriptor of unexp

let pp_unexpected_stub fmt (Unexp (ctxt, s)) =
  pp fmt "Unexpected@ stub@ descriptor@ `%a'@ for@ %a\
         " pp_stm s pp_context ctxt

(* --- *)

type 'k actual =
    {
      output: output option;         (** no output if None, level of outputs
                                         otherwise *)
      mutarg: mutation option;       (** does not mutate arguments if None,
                                         optional level otherwise *)
      mutobj: 'k mutate';
      sret: 'k actual';
      trust: 'k trust';
    }
and 'k actual' =
  | Sr : { ret: static_res; } -> static actual'
  | Vr : { ret: virtual_res; } -> virt actual'
  | Cr : constr actual'
and 'k mutate' =
  | Sm : static mutate'
  | Vm : mutation option -> virt mutate'
  | Cm : mutation option -> constr mutate'
and 'k trust' =
  | St : static_trst -> static trust'
  | Vt : virtual_trst -> virt trust'
  | Ct : static_trst -> constr trust'

(* --- *)

(* XXX: very ugly merging that discards overriden dependency statements *)

(* let acc_sres_deps: static_res_deps -> static_res_deps -> static_res_deps = *)
(*   fun a b -> match a, b with *)
(*     | _, `Any | `Any, _ -> `Any *)
(*     | `Policy p, `Policy p' -> `Policy (acc_policies p p') *)
(*     | `None, _ | _, `Policy _ -> b *)
(*     | _, `None -> a *)
(*     | _, `Args -> `Args *)

let acc_res_deps: virtual_res_deps -> virtual_res_deps -> virtual_res_deps =
  fun a b -> match a, b with
    | _, `None -> a
    | _, `Any | `Any, _ | `DotStar, `Args | `Args, `DotStar -> `Any
    | `Policy p, `Policy p' -> `Policy (acc_policies p p')
    | `None, _ | _, `Policy _ -> b
    | _, `Args -> `Args
    | _, `DotStar -> `DotStar

let acc_sret: static actual' -> static_res -> static actual' =
  fun (Sr { ret }) b -> Sr { ret = match ret, b with
    | `New (t, d), `New (t', d') -> `New (acc_tagging t t', acc_res_deps d d')
    | _, ret -> ret
  }

let acc_vret: virt actual' -> virtual_res -> virt actual' =
  fun (Vr { ret }) b -> Vr { ret = match ret, b with
    | `New (t, d), `New (t', d') -> `New (acc_tagging t t', acc_res_deps d d')
    | _, ret -> ret
  }

let actual (type k) : k context -> k actual -> stm -> k actual = fun t acc s -> match t, s with
  | _, Output r  -> { acc with output = Some r }
  | Static, Result (`New (_, `DotStar)) -> raise @@ UnexpectedStubDescriptor (Unexp (t, s))
  | Static, Result (#static_res as ret) -> { acc with sret = acc_sret acc.sret ret }
  | Static, Mutate `None -> { acc with mutarg = None }
  | Static, Mutate (`Any l | `Args l) -> { acc with mutarg = Some l }
  | Static, Trust (#static_trst as trst) -> { acc with trust = St trst }
  | Virtual, Result (#virtual_res as ret) -> { acc with sret = acc_vret acc.sret ret }
  | Virtual, Mutate `None -> { acc with mutarg = None; mutobj = Vm None }
  | Virtual, Mutate (`Args l) -> { acc with mutarg = Some l }
  | Virtual, Mutate (`Fields l) -> { acc with mutobj = Vm (Some l) }
  | Virtual, Mutate (`Any l) -> { acc with mutarg = Some l; mutobj = Vm (Some l) }
  | Virtual, Trust (#virtual_trst as trst) -> { acc with trust = Vt trst }
  | Constr, Mutate `None -> { acc with mutarg = None; mutobj = Cm None }
  | Constr, Mutate (`Args l) -> { acc with mutarg = Some l }
  | Constr, Mutate (`Fields l) -> { acc with mutobj = Cm (Some l) }
  | Constr, Mutate (`Any l) -> { acc with mutarg = Some l; mutobj = Cm (Some l) }
  | Constr, Trust (#static_trst as trst) -> { acc with trust = Ct trst }
  | t, d -> raise @@ UnexpectedStubDescriptor (Unexp (t, d))

let actual' ctxt = List.fold_left (actual ctxt)

(* --- *)

let top' = `Tag (TLT Security.Top)
let top = Some top'
let mutarg: mutation option = top

(* --- *)

let static { output; sret = Sr { ret };
             trust = St trst; mutarg }
    clazz name rtyp arg_typs throws =
  let rslt: static_res_deps static_result = match rtyp, ret with
    | None, _ -> `Unit
    | Some (Any (Prim, t)), `New (l, (#static_res_deps as d)) -> `FreshPrim (t, !~?l, d)
    | Some (Any (Prim, t)), `Star x -> `Prim (t, !~?x, false)
    | Some (Any (Prim, t)), `Arg -> `Prim (t, !~?None, true)
    | Some t, `New (l, (#static_res_deps as d)) -> `New (t, !~?l, d)
    | Some t, `New (l, _) -> assert false
    | Some t, `Star x  -> `Any (t, !~?x)
    | Some t, `Arg -> `Arg (t)
  in
  { kind = StaticMeth { name; rslt; trst };
    clazz; arg_typs; throws; output; mutarg }

let default_static: static actual =
  { output = None; mutarg; mutobj = Sm;
    sret = Sr { ret = `Arg }; trust = St `None }

let make_static specs = actual' Static default_static specs |> static

(* --- *)

let virt { output; sret = Vr { ret }; mutarg;
           trust = Vt trst; mutobj = Vm mutobj }
    clazz name rtyp arg_typs throws =
  let rslt: virtual_result = match rtyp, ret with
    | None, _ -> `Unit
    | Some (Any (Prim, t)), `New (x, d) -> `FreshPrim (t, !~?x, d)
    | Some (Any (Prim, t)), `Star x -> `Prim (t, !~?x, false)
    | Some (Any (Prim, t)), `Arg -> `Prim (t, !~?None, true)
    | Some (Any (Prim, t)), `DotStar x -> `PField (t, !~?x)
    | Some t, `New (l, d) -> `New (t, !~?l, d)
    | Some t, `DotStar x -> `RField (t, !~?x)
    | Some t, `Star x -> `Any (t, !~?x)
    | Some t, `Arg -> `Arg t
  in
  { kind = VirtMeth { name; rslt; mutobj; trst };
    clazz; arg_typs; throws; mutarg; output }

let default_virtual: virt actual =
  { output = None; mutarg; mutobj = Vm top;
    sret = Vr { ret = `Arg }; trust = Vt `None }

let make_virt specs = actual' Virtual default_virtual specs |> virt

(* --- *)

let cstr { output; sret = Cr; mutarg; trust = Ct trst; mutobj = Cm mutobj }
    clazz arg_typs throws =
  { kind = Constructor { mutobj; trst };
    clazz; arg_typs; throws; output; mutarg }

let default_constr: constr actual =
  { output = None; mutarg; mutobj = Cm top;
    sret = Cr; trust = Ct `None }

let make_cstr specs = actual' Constr default_constr specs |> cstr

(* --- *)

let sign { kind; clazz; arg_typs } = match kind with
  | Constructor _ ->
      Sign.T.cstr clazz arg_typs
  | VirtMeth { name; rslt = `Unit } ->
      Sign.T.meth ~static:false clazz name None arg_typs
  | VirtMeth { name; rslt = (`Prim (t, _, _) | `FreshPrim (t, _, _)
                                | `PField (t, _)) } ->
      Sign.T.meth ~static:false clazz name (Some (any t)) arg_typs
  | VirtMeth { name; rslt = (`New (t, _, _) | `Any (t, _) | `Arg t
                                | `RField (t, _)) } ->
      Sign.T.meth ~static:false clazz name (Some t) arg_typs
  | StaticMeth { name; rslt = `Unit } ->
      Sign.T.meth ~static:true clazz name None arg_typs
  | StaticMeth { name; rslt = (`Prim (t, _, _) | `FreshPrim (t, _, _)) } ->
      Sign.T.meth ~static:true clazz name (Some (any t)) arg_typs
  | StaticMeth { name; rslt = (`New (t, _, _) | `Any (t, _) | `Arg t) } ->
      Sign.T.meth ~static:true clazz name (Some t) arg_typs

let throws { throws } = throws

(* ---- *)

module Checker (S: Semantics.S) = struct
  module Checker = Typing.Checker (S)
  module MS = MutableStack
  let secstubs lst =
    let errs = MS.init () in
    List.iter begin fun ms ->
      let sign = sign ms and throws = throws ms in
      let decl = Decl.of_sign ~throws sign in
      ignore (Checker.meth_decls errs (C decl))
    end lst;
    if MS.is_empty errs then Ok lst else Error (`InSecStubs (MS.to_list errs))
  let filter_out_unknowns lst =
    Ok (List.rev @@ List.fold_left begin fun acc ms ->
      if Sign.T.fold_typs (fun t -> (&&) (S.known_typ t)) (sign ms) true
      then ms :: acc else acc
    end [] lst)
end
