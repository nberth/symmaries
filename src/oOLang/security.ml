open Format
open Utils

type level =
  | Top
  | Bot
(* | Other of string *)
(* for now... *)

type t = level                                                       (* alias *)

let print fmt = function
  | Top -> pp fmt "@<1>⊤"
  | Bot -> pp fmt "@<1>⊥"

let equal: t -> t -> bool = ( = )
let lub: t -> t -> t = fun a b -> match a, b with
  | Bot, Bot -> Bot
  | _ -> Top

type raw = string

let pp_raw = pp_print_string

exception Invalid of string
let pp_invalid fmt =
  pp fmt "Invalid@ security@ level@ `%s'"
let of_raw = function
  | "⊤" | "H" | "h" | "T" | "t" -> Top
  | "⊥" | "L" | "l" | "B" | "b" -> Bot
  | s -> raise @@ Invalid s
