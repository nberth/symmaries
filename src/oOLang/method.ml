open Format
open Rutils
open Utils
open Base
open Typ
open Sign
open Linking

(* --- *)

open Stm

module MS = MutableStack

type 'dom error =
  [
  | untyped loc Typing.error
  | 'dom typed loc Linking.error
  ]
and 'dom warning =
  [
  | untyped loc Typing.warning
  ]
and 'k loc = [ `Stm of int * 'k Stm.raw option
            | `InThrowsOf of Sign.decl
            | `InCatchTbl of int ] option

let pp_raw_srcloc: 'k loc pp = fun fmt -> function
  | None -> ()
  | Some (`Stm (i, Some s)) -> pp fmt "@ in@ statement@ %i@ (%a)" i Stm.print_raw' s
  | Some (`Stm (i, None)) -> pp fmt "@ in@ statement@ %i" i
  | Some (`InThrowsOf m) -> pp fmt "@ in@ declaration@ of@ exceptions@ thrown@ \
                                   by@ method@ `%a'" Decl.print_shortname m
  | Some (`InCatchTbl i) -> pp fmt "@ at@ entry@ %i@ of@ catch@ table" (succ i)

let pp_error fmt = function
  | #Typing.error as e -> Typing.pp_error pp_raw_srcloc fmt e
  | #Linking.error as e -> Linking.pp_error pp_raw_srcloc fmt e

let pp_warning fmt = function
  | #Typing.warning as w -> Typing.pp_warning pp_raw_srcloc fmt w

(* --- *)

module Body = struct
  open Seq

  (* --- *)

  type 'k unlinked = (Security.raw, 'k) Stm.unlinked
  type 'k linked = (Security.raw, 'k) Stm.linked
  type 'dom stm = (Security.t, 'dom typed) Stm.linked

  type raw_stms = untyped unlinked Seq.any
  type raw_catch_table = label catch_table
  type raw = raw_stms * raw_catch_table
  type 'dom t =
      {
        stms: 'dom stm Seq.for_ra;
        catch_table: (nominal branch(* , exceptional branch *)) catch_table;
      }

  (* --- *)

  let print_catch_entry ?(pad = 0) pp_link fmt
      { catch_typ; catch_from; catch_to; catch_at } =
    let pp fmt f = pp fmt ("%.*s"^^f) pad "" in
    pp fmt "catch %a: %a-%a: %a" UserTyp.print catch_typ
      pp_link catch_from pp_link catch_to pp_link catch_at

  let print_catch_table ?pad pp_link =
    pp_lst ~fopen:"@;" ~fclose:";@;" ~fsep:";@," ~fempty:"@;"
      (print_catch_entry ?pad pp_link)

  let print_unlinked fmt (stms, ct) =
    pp fmt "@[<v>";
    let pp_stm = print_raw ~pad:6 in
    iteri (fun i -> if i <> 0 then pp fmt "@,"; pp fmt "%a;" pp_stm) stms;
    print_catch_table ~pad:6 pp_label fmt ct;
    pp fmt "@]"

  let print fmt { stms; catch_table } =
    pp fmt "@[<v>";
    let pp_stm fmt = print_linked ~pad:6 ?pp_lvl:None fmt in
    iteri (fun i -> if i <> 0 then pp fmt "@,"; pp fmt "%2i: %a;" i pp_stm) stms;
    print_catch_table ~pad:4 print_branch fmt catch_table;
    pp fmt "@]"

  (* --- *)

  (* XXX: wrong place... *)
  let rec secur:
  type k. (Security.raw, 'a, k) Stm.t -> (Security.level, 'a, k) Stm.t = function
    | Nop -> Nop
    | Decl (t, rl) -> Decl (t, rl)
    | Assign _ as s -> s
    | AssignP (v, e) -> AssignP (v, e)
    | AssignR (r, s) -> AssignR (r, s)
    | LoadP (v, r, f) -> LoadP (v, r, f)
    | LoadR (r, s, f) -> LoadR (r, s, f)
    | StoreP (r, f, e) -> StoreP (r, f, e)
    | StoreR (r, f, s) -> StoreR (r, f, s)
    | SLoadP _ as s -> s
    | SLoadR _ as s -> s
    | SStoreP _ as s -> s
    | SStoreR _ as s -> s
    | ALoad (v, a, e) -> ALoad (v, a, e)
    | ALoadP (v, a, e) -> ALoadP (v, a, e)
    | ALoadR (r, a, e) -> ALoadR (r, a, e)
    | AStore (a, e, v) -> AStore (a, e, v)
    | AStoreP (a, e, v) -> AStoreP (a, e, v)
    | AStoreR (a, e, r) -> AStoreR (a, e, r)
    | ALen (v, a) -> ALen (v, a)
    | UCall (q, m, vl) -> UCall (q, m, vl)
    | USCall (r, c, m, vl) -> USCall (r, c, m, vl)
    | UFCall ec -> UFCall ec
    | MCall _ as s -> s
    | MSCall _ as s -> s
    | MFCallP (v, ec) -> MFCallP (v, ec)
    | MFCallR (r, ec) -> MFCallR (r, ec)
    | New (r, t) -> New (r, t)
    | ANew (r, t, d) -> ANew (r, t, d)
    | InstOf _ as s -> s
    | InstOfR _ as s -> s
    | InstOfA _ as s -> s
    | Cast _ as s -> s
    | CastP _ as s -> s
    | CastR _ as s -> s
    | CastA _ as s -> s
    | Output (lvl, vl) -> Output (Security.of_raw lvl, vl)
    | OutputPR (lvl, prl) -> OutputPR (Security.of_raw lvl, prl)
    | ReturnU -> ReturnU
    | Return v -> Return v
    | ReturnP v -> ReturnP v
    | ReturnR v -> ReturnR v
    | ChkPoint v -> ChkPoint v
    | Goto _ | CondGoto _ | Switch _ | Throw _ | Catch _
    | MonEnter _ | MonExit _ as s -> s
    | Label (l, s) -> Label (l, secur s)

  (* TODO: catch table... *)
  let of_typed_stm_seq
      : 'dom typed unlinked Seq.any -> ('dom t, [> 'dom error] list) result =
    fun i ->
      let errs = MS.init () in
      let linker = Linking.make errs i in
      let stms = mapi_for_random_access
        (fun i s -> s |> Linking.link_stm linker i |> secur) Nop i
      in
      let catch_table = [] in
      if MS.is_empty errs
      then Ok { stms; catch_table }
      else Error (MS.to_list errs)

  let length: 'dom t -> int = fun { stms } -> Seq.length stms
  let stm_at_pc: 'dom t -> pc -> 'dom stm = fun { stms } -> Seq.random_access stms
  let valid_pc: 'dom t -> pc -> bool = fun { stms } pc -> pc >= 0 && pc < Seq.length stms
  let last_valid_pc: 'dom t -> pc = fun t -> length t - 1
  let first_invalid_pc: 'dom t -> pc = length

  let visit_n_mutate_raw_stms f = mapi_fold_left' f Nop
  let map_raw_stms = mapi

  let make stms catch_table = { stms; catch_table }
  let iter_stms f { stms } = iteri f stms
  let fold_stms f i { stms } = fold_lefti f i stms
  let fold_map_stms f i j { stms } = mapi_fold_left f i j stms

  let catch_table { catch_table } = catch_table

end
type 'dom body = 'dom Body.t
open Body

(* --- *)

open Sign

module Impl = struct
  type t =
    | Raw of raw
    | Meth of { sign: decl; stms: raw }
  let print fmt = function
    | Raw raw ->
        pp fmt "raw: @[<v>%a@]" Body.print_unlinked raw
    | Meth { sign; stms } ->
        pp fmt "%a: @[<v>%a@]" Sign.Decl.print sign Body.print_unlinked stms
end
type impl = Impl.t
open Impl

(* --- *)

module ImplStats = struct
  type 'n cardinals =
      {
        decl_stats: 'n Semantics.DeclStats.cardinals;
        nb_stms: 'n;
      }

  open BasicStats.Outputs

  let print_cardinals ?(pk = ps) ?k0 pe fmt { decl_stats; nb_stms } =
    let pkd = pk ?k0 in
    pp fmt "%a,@;\
            @[<2>%a: %a@]"
      (Semantics.DeclStats.print_cardinals ~pk ?k0 pe) decl_stats
      pkd "nb_stms" pe nb_stms

  let map_cardinals f { decl_stats; nb_stms } =
    { decl_stats = Semantics.DeclStats.map_cardinals f decl_stats;
      nb_stms    = f nb_stms; }

  let acc_cardinals f a b =
    { decl_stats = Semantics.DeclStats.acc_cardinals f a.decl_stats b.decl_stats;
      nb_stms    = f a.nb_stms  b.nb_stms; }

end
type _ stats =
  | Cardinals : int ImplStats.cardinals stats

module type STATS = sig
  include module type of ImplStats
  type t
  val stats: 's stats -> t -> 's
end

(* --- *)

module type T = sig
  module Sem: Semantics.S
  type t
  type call = Sem.FCall.t
  val print: t pp
  val of_impl
    : impl
    -> (t, methname option * Sem.domain error list) result
    * (methname option * Sem.domain warning list)
  val sign: t -> Sign.decl option
  val body: t -> Sem.domain body
  val decls: t -> Sem.decls
  val fold_effcalls: (pc -> ?res:var -> call -> 'a -> 'a) -> t -> 'a -> 'a
  module ImplStats: STATS with type t := t
  include ScgsUtils.IO.BINARY with type t := t
end

(* --- *)

module Compiler (Sem: Semantics.S) = struct

  module Sem = Sem
  open Sem
  open Expr
  open Stm
  open Sign

  type impl =
      {
        body: domain Body.t;
        decls: Decls.t;
      }
  type t =
    | Lambda of impl
    | Method of { sign: Sign.decl; impl: impl }

  let impl = function
    | Lambda impl | Method { impl } -> impl

  let print_impl: impl pp = fun fmt { body; decls } ->
    pp fmt "decl: @[%a@]@\n" Decls.print decls;
    pp fmt "body: @[<v>%a@]" Body.print body

  let print: t pp = fun fmt -> function
    | Method { sign; impl } ->
        pp fmt "@[<2>%a {@\n%a@]}" Sign.Decl.print sign print_impl impl
    | m -> print_impl fmt (impl m)

  (* --- *)

  module Checker = Typing.Checker (Sem)

  let type_and_link errs warns decls rtyp (Raw stms | Meth { stms }) =
    let stms, catch_table = stms in
    let checker = Checker.make errs warns rtyp in
    let linker = Linking.make errs stms in
    let stms', (decls, linker, _pushed_labels) = visit_n_mutate_raw_stms
      begin fun (d, linker, pushed_labels) _old_pc pc s ->
        let d, s' = Checker.check_stm checker d pc s in
        if is_nop s'
        then ((d, Linking.offset_above pc (-1) linker,
               acc_labels pushed_labels s'),
              None)
        else ((d, linker, []),
              Some (apply_labels s' pushed_labels))
      end (decls, linker, []) stms in
    let ct, linker = Linking.link_catch_table linker catch_table in
    let stms =
      map_raw_stms (fun pc s -> s |> Linking.link_stm linker pc |> secur) stms' in
    let body = Body.make stms ct in
    let lookup_handler b = Body.stm_at_pc body (target b) in
    Checker.check_catch_table checker lookup_handler decls ct;
    body, decls

  let of_impl: _
      -> ((t, methname option * domain error list) result
         * (methname option * domain warning list)) =
    fun m ->
      let errs = MS.init () and warns = MS.init () in
      let name = match m with
        | Raw _ -> None
        | Meth { sign } -> Some (asprintf "%a" Sign.Decl.print_shortname sign)
      in
      let decls, rtyp, mk = match m with
        | Raw _
          -> (Sem.static_void, None, (fun b -> Lambda b))
        | Meth { sign = C (Method { sign = { kind = Sign.Meth { rtyp }}}) as sign }
          -> (Checker.meth_decls errs sign, Some rtyp,
             (fun impl -> Method { sign; impl }))
        | Meth { sign = C (Method { sign = { kind = Sign.Cstr }}) | I _ as sign }
          -> (Checker.meth_decls errs sign, Some None,
             (fun impl -> Method { sign; impl }))
      in
      let body, decls = type_and_link errs warns decls rtyp m in
      (if MS.is_empty errs
       then Ok (mk { body; decls })
       else Error (name, MS.to_list errs)),
      (name, MS.to_list warns)

  let body m = (impl m).body
  let decls m = (impl m).decls
  let sign = function
    | Lambda _ -> None
    | Method { sign } -> Some sign

  type call = Sem.FCall.t

  let fold_effcalls f m i =
    let rec aux pc acc = function
      | UFCall ec -> f pc ?res:None ec acc
      | MFCallP (res, ec) | MFCallR (res, ec) -> f pc ?res:(Some res) ec acc
      | Label (_, s) -> aux pc acc s
      | _ -> acc
    in
    fold_stms aux i (body m)

  (* --- *)

  module ImplStats: STATS with type t := t = struct
    include ImplStats
    let stats: type s. s stats -> t -> s = fun s m -> match s with
      | Cardinals -> { decl_stats = Sem.DeclStats.stats Cardinals (decls m);
                      nb_stms = Body.length (body m); }

  end

  (* --- *)

  include (ScgsUtils.IO.Bin (struct
    type t' = t
    type t = t'
    let functional = false
  end) : ScgsUtils.IO.BINARY with type t := t)

end

(* -------------------------------------------------------------------------- *)

module FCalls (Meth: T) = struct
  open Meth
  open Sem
  let fold f ~fallback lookup meth acc =
    fold_effcalls begin fun pc ?res ec (acc, missing) ->
      let callsign = FCall.callsign ec in
      let ms, miss, nlookups = FCall.lookup callsign lookup in
      match ms with
        | e :: tl when Typ.UserTyps.is_empty miss
            -> f pc ?res ec e tl acc, missing
        | _ -> let methsign = FCall.concrete_methsign callsign in
              fallback pc ?res ec (Sign.Decl.of_sign methsign) acc,
              if Typ.UserTyps.is_empty miss || nlookups <= 1
              then Sign.Map.add methsign (`All ec) missing
              else Sign.Map.add methsign (`Some (ec, miss)) missing
    end meth (acc, Sign.Map.empty)
end

module type RESOLVABLE = sig
  module Meth: T
  type t
  val fallback: pc -> ?res:var -> Meth.call -> Sign.callable_decl -> t
end
module type RESOLUTION = sig
  module Meth: T
  open Meth
  type elt
  type t
  type universe = elt Sem.FCall.Lookup.t
  val lookup_all: Meth.t -> universe
    -> t PCMap.t * [ `All of call | `Some of call * UserTyps.t ] Map.t
end
module type RESOLUTIONS = sig
  module Meth: T
  type e
  type universe = e Meth.Sem.FCall.Lookup.t

  module Direct (F: sig
    val merge: pc -> ?res:var -> Meth.call -> e -> e list -> e
  end) : (RESOLUTION with module Meth := Meth
                     and type elt := e
                     and type t := e)

  module Filtered (F: sig
    type t
    val filter: e -> t
    val merge: pc -> ?res:var -> Meth.call -> t -> t list -> t
  end) : (RESOLUTION with module Meth := Meth
                     and type elt := e
                     and type t := F.t)
end

module Resolutions (Meth: T) (E: RESOLVABLE with module Meth := Meth) = struct
  include FCalls (Meth)
  type e = E.t
  type universe = e Meth.Sem.FCall.Lookup.t

  module Direct (F: sig
    val merge: pc -> ?res:var -> Meth.call -> E.t -> E.t list -> E.t
  end) = struct
    open E
    open F
    type universe = E.t Meth.Sem.FCall.Lookup.t
    let acc pc ?res ec e tl =
      merge pc ?res ec e tl |> PCMap.add pc
    let lookup_all meth (u: universe) =
      fold acc ~fallback:(fun pc ?res ec decl ->
        acc pc ?res ec (fallback pc ?res ec decl) [])
        u meth PCMap.empty
  end

  module Filtered (F: sig
    type t
    val filter: E.t -> t
    val merge: pc -> ?res:var -> Meth.call -> t -> t list -> t
  end) = struct
    open E
    open F
    type universe = E.t Meth.Sem.FCall.Lookup.t
    let acc pc ?res ec e tl =
      merge pc ?res ec (filter e) (List.map filter tl) |> PCMap.add pc
    let lookup_all meth (u: universe) =
      fold acc ~fallback:(fun pc ?res ec decl ->
        acc pc ?res ec (fallback pc ec decl) [])
        u meth PCMap.empty
  end
end

(* -------------------------------------------------------------------------- *)
