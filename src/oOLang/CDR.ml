open Format
open Rutils
open Utils
open Graph
open Base
open Method

let level = Log.Debug
let logger = Log.mk ~level "CDR"

module T = String
module Ts = Strings
module M = StrMap
type t = T.t
type set = Ts.t
type region = t
type regions = set

module Stack = struct
  type t = T.t list
  let compare = Stdlib.compare                    (* XXX: Good enough I guess *)
  let equal a b = compare a b = 0
  let hash = Hashtbl.hash
  let empty: t = []
  let top: t -> T.t option = function
    | [] -> None
    | r :: _ -> Some r
  let push: T.t -> t -> t = List.cons
  let pop: t -> t = function
    | _ :: rs -> rs
    | [] -> failwith "Trying to pop from empty stack of regions"
end

module type S = sig
  type cfg and info and extended_info and cfg_fp_setup
  val of_cfg: ?fp_setup:cfg_fp_setup -> cfg -> info
  val extend: ?fp_setup:cfg_fp_setup -> cfg -> info -> extended_info
  val print: ?cdrx:extended_info -> info pp
  val output_dot: formatter -> ?cdr:info -> ?cdrx:extended_info -> cfg -> unit
  val has_regions: info -> bool
  val fold_regions: (region -> pc -> 'a -> 'a) -> info -> 'a -> 'a
  val induced_region: pc -> info -> region option
  val induces_region: pc -> info -> bool
  val pc_of_inducing: region -> info -> pc
  val is_junc: pc -> info -> bool
  val is_junc_of: region -> pc -> info -> bool
  val juncs: pc -> info -> regions
  val regions_at_pc: info -> regions PCMap.t
end

module Make
  (CFG: CFG.T)
  :
  (S with type cfg := CFG.cfg
     and type cfg_fp_setup := CFG.Fixpoint.setup)
  =
struct
  open CFG

  module Dom = Dominator.Make (G)

  let postdom_tree' cfg =
    let open Dom in
    let meth = meth cfg in
    let nstms = Body.length (Meth.body meth) in
    if nstms <= 1
    then fun _ -> S.empty
    else begin
      let cfg' = CFG.of_meth ~rev:true ~size:(nstms + 1) meth in
      let cfg' = CFG.graph cfg' in
      let sink = CFG.sink cfg in
      let sinked = G.fold_vertex begin fun v x ->
        if G.in_degree cfg' v = 0
        then (G.add_edge cfg' sink v; true)
        else x
      end cfg' false in
      if not sinked then         (* add an arbitrary edge from unreachable sink *)
        G.add_edge cfg' sink PC.start;
      let idom = compute_idom cfg' sink in
      let dominators v = idom_to_dominators idom v |> S.of_list in
      (* let dominators = dominators_to_sdominators dominators in *)
      dominators_to_dom_tree cfg' (* ~pred:G.succ *) dominators
    end

  let postdom_tree cfg =
    let postdom_tree' = postdom_tree' cfg in
    fun v -> postdom_tree' v |> Dom.S.elements

  let fold_postdom_tree f cfg acc =
    let postdom_tree' = postdom_tree' cfg in
    let visitpc pc = Dom.S.fold (f pc) (postdom_tree' pc) in
    Body.fold_stms (fun pc acc _ -> visitpc pc acc) (visitpc (sink cfg) acc)
      (Meth.body (meth cfg))

  let postdom_graph ?cfg meth =                     (* XXX: not used anywhere *)
    let cfg = match cfg with Some c -> c | None -> CFG.of_meth meth in
    let postdom_tree = postdom_tree cfg in
    let module DomG = Dominator.Make_graph (struct
      include G
      include Builder.I (G)
    end) in
    DomG.compute_dom_graph (CFG.graph cfg) postdom_tree

  (* --- *)

  type info =
      {
        junc: set PCMap.t;
        regions: regions PCMap.t;
        induced_region: t PCMap.t;          (* for region-inducing statements *)
        start: pc M.t;
      }

  type extended_info =
      {
        top_region: region PCMap.t;
        parent: t M.t;
        aliasing: (region * regions) M.t;
      }

  let empty =
    {
      junc = PCMap.empty;
      regions = PCMap.empty;
      induced_region = PCMap.empty;
      start = M.empty;
    }

  let print ?cdrx fmt { junc; regions; induced_region; start } =
    pp fmt "@[<v>junc = %a@;\
                 regions = %a@;\
                 induced_region = %a@;\
                 region_start = %a%t@]"
      (PCMap.print Ts.print) junc
      (PCMap.print Ts.print) regions
      (PCMap.print pp_print_string) induced_region
      (M.print PC.print) start
      (fun fmt -> match cdrx with None -> () | Some { top_region; parent } ->
        pp fmt "@;top_region = %a" (PCMap.print T.print) top_region)

  let has_regions { induced_region } = not (PCMap.is_empty induced_region)

  let is_junc pc { junc } = PCMap.mem pc junc

  let is_junc_of r pc { junc } =
    try Ts.mem r (PCMap.find pc junc) with Not_found -> false

  let juncs pc { junc } =
    try PCMap.find pc junc with Not_found -> Ts.empty

  let induced_region pc { induced_region } =
    try Some (PCMap.find pc induced_region) with Not_found -> None
  let induces_region pc l = induced_region pc l <> None

  let pc_of_inducing r { start } = M.find r start

  let fold_regions f { start } = M.fold f start

  let regions_at_pc: info -> regions PCMap.t = fun i -> i.regions

  (* --- *)

  let acc_regions ?fp_setup cfg (info: info) =
    let module RGFP = Fixpoint.Make (struct
      include Ts
      let join = union
      let widening = join
      let analyze (pc, _, pc') x =
        let x = match induced_region pc info with
          | None -> x
          | Some r -> Ts.add r x in
        Ts.diff x (juncs pc' info)
    end) in
    RGFP.apply (fun _ -> Ts.empty) (match fp_setup with
      | None -> Fixpoint.setup cfg
      | Some s -> s)
      |> fun m -> RGFP.M.fold PCMap.add m PCMap.empty

  (* --- *)

  let add_induced_region pc ({ induced_region; start } as t) =
    let r = asprintf "ρ%a" PC.print pc in
    { t with
      induced_region = PCMap.add pc r induced_region;
      start = M.add r pc start }, r

  let add_junc pc pc' ({ junc } as t) =
    let t, r = add_induced_region pc' t in
    let rs = try Ts.add r (PCMap.find pc junc) with
      | Not_found -> Ts.singleton r in
    { t with junc = PCMap.add pc rs junc }

  let of_cfg ?fp_setup : cfg -> info = fun cfg ->
    let t = fold_postdom_tree begin fun pc pc' ->
      if is_branching cfg pc' then add_junc pc pc' else ign
    end cfg empty in
    let infos = fold_branching begin fun pc ({ induced_region } as t) ->
      if PCMap.mem pc induced_region then t
      else add_induced_region pc t |> fst
    end cfg t in
    { infos with regions = acc_regions ?fp_setup cfg infos }

  (* --- *)

  let topo_sort: Ts.t M.t -> Ts.t list * Ts.t M.t =
    let gather set = M.fold (fun a _ -> Ts.add a) set Ts.empty in
    let forget set = M.map (fun d -> Ts.diff d set) in
    let rec srt acc deps =
      let sinks, rem_deps = M.partition (fun _ -> Ts.is_empty) deps in
      if M.is_empty sinks then
        List.rev acc, rem_deps
      else
        let sinks = gather sinks in
        srt (sinks :: acc) (forget sinks rem_deps)
    in
    srt []

  let acc_extra ?fp_setup cfg (info: info) =
    let module RGFP = Fixpoint.Make (struct
      type t = region list
      let equal a b = cmp_lst T.compare a b = 0
      let rec join a b = match a, b with
        | a, [] | [], a -> a
        | a :: atl, b :: btl when T.compare a b = 0 -> a :: join atl btl
        | a :: atl, bl -> a :: join atl (List.filter (fun r -> not (T.equal a r)) bl)
      let widening _ x = x
      let analyze (pc, t, pc') rs =
        let rec aux rs = match induced_region pc info, rs with
          | _, r :: rs when is_junc_of r pc' info -> aux rs
          | Some r, rs when is_junc_of r pc' info -> rs
          | Some r, rs when List.exists (T.equal r) rs -> rs
          | Some r, rs -> r :: rs
          | None, rs -> rs
        in aux rs
    end) in
    let rec acc_ancestors m = function
      | [] | [_] -> m
      | r :: (r' :: _ as tl) -> acc_ancestors (ancestor r r' m) tl
    and ancestor r r' m = M.add r (Ts.add r' (M.find r m)) m
    and r2pc pc m r = try M.add r (PCs.add pc (M.find r m)) m with
      | Not_found -> M.add r (PCs.singleton pc) m
    in
    RGFP.apply (fun _ -> []) (match fp_setup with
      | None -> Fixpoint.setup cfg
      | Some s -> s)
      |> fun m -> RGFP.M.fold (fun pc rs ((m, p, a) as acc) -> match rs with
          | [] -> acc
          | r :: _ -> (PCMap.add pc r m, acc_ancestors p rs,
                      List.fold_left (r2pc pc) a rs))
        m (PCMap.empty, M.map (fun _ -> Ts.empty) info.start, M.empty)

  (* --- *)


  let extend ?fp_setup cfg (cdr: info) : extended_info =
    let top_region, ancestors_relation, pcmap = acc_extra ?fp_setup cfg cdr in
    Log.d2 logger "top_region = @[%a@]" (PCMap.print T.print) top_region;
    Log.d2 logger "ancestors: %a" (M.print Ts.print) ancestors_relation;
    let parent, _ = List.fold_left (fun (m, p) r -> match p with
      | Some r' ->
          Ts.fold (fun r -> Ts.fold (fun r' -> M.add r r')
            (Ts.inter r' (M.find r ancestors_relation))) r m, Some r
      | None -> m, Some r)
      (M.empty, None) (fst (topo_sort ancestors_relation))
    in
    let module PCsMap = MakeMap (PCs) in
    let aliasing = M.fold (fun r pcs m ->
      PCsMap.add pcs (try Ts.add r (PCsMap.find pcs m) with
        | Not_found -> Ts.singleton r) m)
      pcmap PCsMap.empty
    in
    Log.d2 logger "region_aliasing: %a" (PCsMap.print Ts.print) aliasing;
    let aliasing = PCsMap.fold begin fun _ eqregs ->
      let r = Ts.choose eqregs in
      Ts.fold (fun r' -> M.add r' (r, eqregs)) eqregs
    end aliasing M.empty in
    Log.d2 logger "parent: %a" (M.print T.print) parent;
    Log.d2 logger "aliasing: %a" (M.print (pp_pair T.print Ts.print)) aliasing;
    { top_region; parent; aliasing }

  (* --- *)

  let region_family_n_parent pc = function
    | None -> None
    | Some { top_region; parent; aliasing } ->
        try
          let r = PCMap.find pc top_region in
          let r, rfamily = M.find r aliasing in
          let parent = try Some (M.find r parent) with Not_found -> None in
          Some (r, rfamily, parent)
        with Not_found -> None


  open Graphviz
  open DotAttributes
  let output_dot fmt ?cdr ?cdrx (cfg: cfg) =
    let pp_loc = pp_loc cfg ~escaped:true in
    let pp_junc_info fmt pc =
      match cdr with None -> () | Some cdr ->
        let rs = juncs pc cdr in
        if not (Ts.is_empty rs)
        then pp fmt "\\n\\@\n(junc%a)\
                    " (fun fmt -> Ts.print' ~sep:", " fmt) rs
    in
    let module D = Dot (struct
      include G
      let vertex_name = asprintf "v%i"
      let graph_attributes _ = []
      let default_vertex_attributes _ = [ `Shape `Box ]
      let default_edge_attributes _ = []
      let vertex_attributes pc =
        [ `Label (asprintf "%a%a" pp_loc pc pp_junc_info pc) ]
      let edge_attributes e = []
      let get_subgraph pc = match region_family_n_parent pc cdrx with
        | None -> None
        | Some (sg_name, rfamily, sg_parent) ->
            let fname = asprintf "%a" Ts.print rfamily in
            Some { sg_name; sg_attributes = [ `Label fname ]; sg_parent }
    end) in
    D.fprint_graph fmt (graph cfg)

end
