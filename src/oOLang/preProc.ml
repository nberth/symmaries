(** {1 Basic pre-processing modules for OOLang control flow graphs} *)

open Format
open Rutils
open Utils
open Base

let level = Log.Debug3
let logger = Log.mk ~level "OPP";;

(* -------------------------------------------------------------------------- *)

module NullPointerExceptionPruning (C: CFG.T) = struct
  module PA = CFG.PointerAccess (C)
  open PA
  let compute decls fp_setup =
    fold_ref_accesses begin fun pc stm r -> function
      | NonNull -> ign                      (* TODO: remove np exception edge? *)
      | MaybeNull ->
          Log.d logger "@[Potentially@ null@ %a@ in@ statement@ `%a'@ at@ pc=%a\
                        @]" Var.print r Stm.print_raw' stm PC.print pc;
          PCs.add pc
    end fp_setup (VarMap.keys (C.Meth.Sem.refs decls)) PCs.empty
end

(* -------------------------------------------------------------------------- *)
