open Rutils
open Utils
open Base
open Typ

(* --- *)

module type T = sig
  type var
  type s
  type t
  val mem: var -> t -> bool
  val find: var -> t -> s typname
  val fold: (var -> s typname -> 'a -> 'a) -> t -> 'a -> 'a
  val print: t pp
end

(* --- *)

module type OPEN = sig
  include T
  val empty: t
  val var: var -> s typname -> t -> t
  val merge: redecl:(var -> s typname -> s typname -> s typname option) -> t -> t -> t
end

(* --- *)

module Make
  (Var: ORDERED_HASHABLE_TYPE)
  (TSpec: TYPSPEC)
  :
  (OPEN with type var = Var.t and type s = TSpec.s)
  =
struct
  module VarMap = Helpers.MakeMap (Var)
  include TSpec
  include (VarMap: module type of VarMap with type 'a t := 'a VarMap.t)
  type var = Var.t
  type t = s typname VarMap.t
  let var = add
  let merge ~redecl = union redecl
  let print: t pp = print TypName.print
end
