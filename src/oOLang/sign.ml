open Utils
open Typ
open Base

let pp_args ?(nl = true) fmt =
  if nl then pp_lst ~fempty:"()" ~fopen:"(@[" ~fclose:")@]" ~fsep:",@ " fmt
  else pp_lst ~fempty:"()" ~fopen:"(" ~fclose:")" ~fsep:", " fmt

type sign =
    {
      kind: kind;
      clazz: user typname;
      arg_typs: any typname list;
    }
and kind =
  | Meth of
      {
        static: bool;
        name: methname;
        rtyp: any typname option;
      }
  | Cstr

module T = struct
  type t = sign

  let print_ret_typ fmt = function
    | Some t -> AnyTyp.print fmt t
    | None -> pp fmt "void"

  let print_arg_typs ?nl = pp_args ?nl TypName.print

  let print_shortname fmt = function
    | { clazz; kind = Meth { name } }
      -> pp fmt "%a:%a" UserTyp.print clazz pp_methname name
    | { clazz; kind = Cstr }
      -> pp fmt "%a" UserTyp.print clazz

  let print_nonargs' ?(ret = true) fmt ({ kind } as t) = match kind with
    | Meth { static; rtyp } when ret ->
        pp fmt "%a%a %a"
          (pp_if "static ") static
          print_ret_typ rtyp
          print_shortname t
    | Meth _ | Cstr ->
        print_shortname fmt t
  let print_nonargs fmt t = print_nonargs' fmt t

  let print fmt ({ arg_typs = at } as sign) =
    pp fmt "@[<2>%a %a@]" print_nonargs sign (print_arg_typs ~nl:true) at

  let print' ?nl ?ret fmt ({ arg_typs = at } as sign) =
    pp fmt "@[<2>%a %a@]" (print_nonargs' ?ret) sign (print_arg_typs ?nl) at

  let print_nnl fmt ({ arg_typs = at } as sign) =
    pp fmt "%a %a" print_nonargs sign (print_arg_typs ~nl:false) at

  let compare' cmp_argtyps a b = match a, b with
    | { kind = Meth _ }, { kind = Cstr }
    | { kind = Meth { static = true } }, { kind = Meth { static = false } } -> -1
    | { kind = Cstr }, { kind = Meth _ }
    | { kind = Meth { static = false } }, { kind = Meth { static = true } } -> 1
    |({ clazz = c1; kind = Cstr; arg_typs = a1 },
      { clazz = c2; kind = Cstr; arg_typs = a2 })
      -> cmp_2 UserTyp.compare (cmp_lst cmp_argtyps) (c1, a1) (c2, a2)
    |({ clazz = c1; kind = Meth { name = m1 }; arg_typs = a1 },
      { clazz = c2; kind = Meth { name = m2 }; arg_typs = a2 })
      -> cmp_2 (cmp_2 UserTyp.compare String.compare) (cmp_lst cmp_argtyps)
        ((c1, m1), a1) ((c2, m2), a2)

  let compare = compare' AnyTyp.compare

  let hash = function
    | { clazz; kind = Meth { static; name }; arg_typs }
      ->(hash_2
          (hash_2 Hashtbl.hash Hashtbl.hash)
          Hashtbl.hash
          ((clazz, name), arg_typs) + if static then 1 else 0)
    | { clazz; kind = Cstr; arg_typs }
      -> hash_2 Hashtbl.hash Hashtbl.hash (clazz, arg_typs) + 43

  let equal a b = a == b || compare a b = 0

  let meth ~static clazz name rtyp arg_typs =
    { clazz; kind = Meth { static; name; rtyp }; arg_typs }

  let cstr clazz arg_typs =
    { clazz; kind = Cstr; arg_typs }

  let virtp = function
    | { kind = Meth { static = false } } -> true
    | _ -> false

  let fold_typs f { kind; clazz; arg_typs } a =
    let a = f (any clazz) a in
    let a = List.fold_left ~@.f a arg_typs in
    match kind with
      | Cstr | Meth { rtyp = None } -> a
      | Meth { rtyp = Some t } -> f t a

end
open T
module type LOOKUP = MAP with type key = sign
module Map = MakeMap (T)

(* --- *)

module type CUSTOM_TYPE_CMP = sig
  val compare: any typname -> any typname -> int
end

module CustomLookup (ArgTypCmp: CUSTOM_TYPE_CMP) = MakeMap (struct
  include T
  let compare = compare' ArgTypCmp.compare
  let equal a b = a == b || compare a b = 0
end)

(* --- *)

module type SET = sig
  module Map: LOOKUP
  type elt
  type t = elt Map.t
  val empty: t
  val is_empty: t -> bool
  val insert: elt -> t -> t
  val mem: sign -> t -> bool
  val find: sign -> t -> elt
  val iter: (elt -> unit) -> t -> unit
  val fold: (elt -> 'a -> 'a) -> t -> 'a -> 'a
  val filter: (elt -> bool) -> t -> t
  val print: t pp
  val update: t -> t -> t
end

module MakeSet (L: LOOKUP) (E: sig
  type t
  val print: t pp
  val sign: t -> sign
end) = struct
  module Map = L
  type elt = E.t
  type t = elt Map.t
  let empty = Map.empty
  let is_empty = Map.is_empty
  let insert e = Map.add (E.sign e) e
  let mem = Map.mem
  let find = Map.find
  let iter f = Map.iter (fun _ -> f)
  let fold f = Map.fold (fun _ -> f)
  let filter p = Map.filter (fun _ -> p)
  let print: t pp =
    Map.print' ~left:"" ~right:"" ~skip_key:(fun _ _ -> true) ~sep:"@\n" E.print
  let update: t -> t -> t =
    Map.union (fun _ _ s -> Some s)
end

module Set = MakeSet (Map) (struct include T let sign s = s end)

(* --- *)

type ('a, 'i) xsign =
  | Callable of 'a
  | Implicit of 'i

module type EXTENDED_SIGN = sig
  type t
  module K: ORDERED_HASHABLE_TYPE
  val print: t pp
  val sign: t -> (sign, K.t) xsign
end

module type EXTENDED_SET = sig
  module Map: LOOKUP
  type elt
  type callables = elt Map.t
  type ik
  module IMap: MAP with type key = ik
  type implicits = elt IMap.t
  type t
  val empty: t
  val is_empty: t -> bool
  val insert: elt -> t -> t
  val mem: (sign, ik) xsign -> t -> bool
  val find: (sign, ik) xsign -> t -> elt
  val iter: (elt -> unit) -> t -> unit
  val fold: (elt -> 'a -> 'a) -> t -> 'a -> 'a
  val filter: (elt -> bool) -> t -> t
  val print: t pp
  val update: t -> t -> t
  val callables: t -> callables
  val implicits: t -> implicits
end

module MakeExtendedSet (L: LOOKUP) (E: EXTENDED_SIGN) = struct
  module Map = L
  module IMap = MakeMap (E.K)
  type elt = E.t
  type callables = elt Map.t
  type ik = E.K.t
  type implicits = elt IMap.t
  type t = callables * implicits
  let empty = Map.empty, IMap.empty
  let is_empty (e, i) = Map.is_empty e && IMap.is_empty i
  let insert e = match E.sign e with
    | Callable c -> app_fst (Map.add c e)
    | Implicit i -> app_snd (IMap.add i e)
  let mem s (e, i) = match s with
    | Callable c -> Map.mem c e
    | Implicit c -> IMap.mem c i
  let find = function
    | Callable c -> fun (m, _) -> Map.find c m
    | Implicit c -> fun (_, m) -> IMap.find c m
  let iter f (e, i) = Map.iter (fun _ -> f) e; IMap.iter (fun _ -> f) i
  let fold f (e, i) acc = acc |> Map.fold (fun _ -> f) e |> IMap.fold (fun _ -> f) i
  let filter p (e, i) = Map.filter (fun _ -> p) e, IMap.filter (fun _ -> p) i
  let print: t pp = fun fmt (e, i) ->
    Map.print' ~left:"" ~right:"" ~skip_key:(fun _ _ -> true) ~sep:"@\n"
      E.print fmt e;
    IMap.print' ~left:(if Map.is_empty e then "" else "@\n") ~right:""
      ~skip_key:(fun _ _ -> true) ~sep:"@\n" ~empty:""
      E.print fmt i
  let update: t -> t -> t = fun (e1, i1) (e2, i2) ->
    (Map.union (fun _ _ s -> Some s) e1 e2,
     IMap.union (fun _ _ s -> Some s) i1 i2)
  let callables = fst
  let implicits = snd
end

(* --- *)

type callable = C
type implicit = I
type _ declaration =
  | Method : { sign: sign; arg_decls: arg_decls; throws: excs } -> callable declaration
  | Clinit : { clazz: user typname; throws: excs } -> implicit declaration
and arg_decls = (any typname * var) list
and excs = UserTyps.t
and exc = user typname

type decl =
  | C : callable declaration -> decl
  | I : implicit declaration -> decl
type callable_decl = callable declaration
type implicit_decl = implicit declaration

let pp_excs ?(nl = true) fmt =
  if nl
  then UserTyps.print' ~empty:"" ~left:"@ @[throws @[<hv>" ~right:"@]@]" fmt
  else UserTyps.print' ~empty:"" ~left:" throws " ~right:"" ~sep:", " fmt

module Decl = struct
  type t = decl
  let compare a b = match a, b with
    | C _, I _ -> -1
    | I _, C _ -> 1
    | C (Method { sign = s1 }), C (Method { sign = s2 })
      -> T.compare s1 s2
    | I (Clinit { clazz = c1 }), I (Clinit { clazz = c2 })
      -> UserTyp.compare c1 c2
  let equal a b = a == b || compare a b = 0
  let hash = function
    | C (Method { sign }) -> T.hash sign
    | I (Clinit { clazz }) -> UserTyp.hash clazz + 37
  let sign = function
    | C (Method { sign }) -> Callable sign
    | I (Clinit { clazz }) -> Implicit clazz
  let print_arg_decls ?nl =
    let format: _ pair_fmt = match nl with
      | Some true -> "@[<2>%a@ %a@]" | _ -> "%a %a" in
    pp_args ?nl (pp_pair ~format TypName.print Var.print)
  let print fmt = function
    | C (Method { sign; arg_decls; throws }) ->
        pp fmt "@[<2>%a %a%a@]" print_nonargs sign (print_arg_decls ~nl:true)
          arg_decls (pp_excs ~nl:true) throws
    | I (Clinit { clazz; throws }) ->
        pp fmt "@[<2>static %a%a@]" UserTyp.print clazz (pp_excs ~nl:true) throws
  let print_nnl fmt = function
    | C (Method { sign; arg_decls; throws }) ->
        pp fmt "@[<2>%a %a%a@]" print_nonargs sign (print_arg_decls ~nl:false)
          arg_decls (pp_excs ~nl:false) throws
    | I (Clinit { clazz; throws }) ->
        pp fmt "@[<2>static %a%a@]" UserTyp.print clazz (pp_excs ~nl:false) throws
  let print_shortname fmt = function
    | C (Method { sign }) -> print_shortname fmt sign
    | I (Clinit { clazz }) -> UserTyp.print fmt clazz
  let meth ~static clazz name rtyp arg_decls throws =
    Method { sign = T.meth ~static clazz name rtyp (List.map fst arg_decls);
             arg_decls; throws; }
  let cstr clazz arg_decls throws =
    Method { sign = T.cstr clazz (List.map fst arg_decls);
             arg_decls; throws }
  let clinit clazz throws =
    Clinit { clazz; throws }
  let subst_args' subst = function
    | Method ({ arg_decls } as s) ->
        Method ({ s with arg_decls =
            List.map (fun (t, v) -> t, try subst v with Not_found -> v) arg_decls })
  let subst_args subst = function
    | C d -> C (subst_args' subst d)
    | d -> d
  let clazz = function
    | C (Method { sign = { clazz }}) | I (Clinit { clazz }) -> clazz
  let args = function
    | C (Method { arg_decls }) -> List.map snd arg_decls
    | I _ -> []
  let ret_some = function
    | C (Method { sign = { kind = Meth { rtyp } } }) -> rtyp <> None
    | _ -> false
  let ret_typ = function
    | C (Method { sign = { kind = Meth { rtyp } } }) -> rtyp
    | _ -> None
  let of_sign ?(throws = UserTyps.empty) ((* Method *) { arg_typs } as sign) =
    Method { sign;
             arg_decls = List.fold_left begin fun (lst, i) at ->
               (at, Format.asprintf "a%i" i) :: lst, succ i
             end ([], 0) arg_typs |> fst |> List.rev;
             throws; }
  let throws': type d. d declaration -> excs = function
    | Method { throws } -> throws
    | Clinit { throws } -> throws
  let throws: decl -> excs = function
    | C (Method { throws }) | I (Clinit { throws }) -> throws
  let add_throw': type d. d declaration -> exc -> d declaration = fun d exc ->
    match d with
      | Method ({ throws = t } as x)
        -> Method { x with throws = UserTyps.add exc t }
      | Clinit ({ throws = t } as x)
        -> Clinit { x with throws = UserTyps.add exc t }
  let add_throw d exc = match d with
    | C d -> C (add_throw' d exc)
    | I d -> I (add_throw' d exc)
end

module DeclMap = MakeMap (Decl)

(* -------------------------------------------------------------------------- *)

type ('effarg_name, 'effarg_dom) effcall =
    {
      fclazz: user typname;
      fkind: ('effarg_name, 'effarg_dom) fkind;
      eff_args: ('effarg_name * 'effarg_dom) list;
    }
and ('effarg_name, 'effarg_dom) fkind =
  | VirtCall of
      {
        inst: ('effarg_name * 'effarg_dom) option;
        name: methname;
        rtyp: any typname option;
      }
  | CstrCall of
      {
        this: 'effarg_name;
      }
  | SpecCall of
      {
        this: 'effarg_name;
        name: methname;
        rtyp: any typname option;
      }

let call ~instvar typdom (Method { sign; arg_decls }) =
  let eff_args = List.map (fun (t, v) -> v, typdom t) arg_decls in
  { fclazz = sign.clazz;
    fkind = (match sign.kind with
      | Meth { static = true; rtyp; name } ->
          VirtCall { inst = None; name; rtyp }
      | Meth { static = false; rtyp; name } ->
          VirtCall { inst = Some (instvar, typdom (any sign.clazz));
                     name; rtyp }
      | Cstr -> CstrCall { this = instvar });
    eff_args }

type 'a argmap =
    {
      formal2eff: 'a varmap;
    }

let argmap ~instvar : ('a, 'domain) effcall -> callable declaration -> 'a argmap =
  fun { fkind; eff_args } (Method { sign = { clazz }; arg_decls }) ->
    {
      formal2eff = List.fold_left2
        (fun a2e (e, _) (_, a) -> VarMap.add a e a2e)
        (match fkind with
          | VirtCall { inst = None } -> VarMap.empty
          | VirtCall { inst = Some (this, _) }
          | SpecCall { this }
          | CstrCall { this } -> VarMap.singleton instvar this)
        eff_args
        arg_decls
    }
