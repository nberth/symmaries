open Utils
open Base
open Stm
open Typ
open Semantics

type 'l error =
  [
  | `UnknownType of 'l * user typname
  | `UnknownIdent of 'l * qualident
  | `UnknownField of 'l * field * user typname
  | `Typing of 'l * any typname * any typname
  | `ExpectRef of 'l * var
  | `ExpectArr of 'l * var
  | `UnexpectRef of 'l * imref * any typname option
  | `UnexpectArr of 'l * imref * arry typname
  | `Undecl of 'l * var
  | `Redecl of 'l * var
  | `MissingRetVal of 'l
  | `UnexpectedRet of 'l
  | `WrongArrayDims of 'l * array_dims_error
  | `ExpectThrwbl of 'l * var option * user typname
  | `CustomError of 'l * Rutils.PPrt.pu
  ]

type 'l warning =
  [
  | `DangerousCast of 'l * user typname * any typname
  ]

let pp_error: 'l pp -> [> 'l error] pp = fun pp_loc fmt -> function
  | `UnknownType (loc, c) ->
      pp fmt "Undeclared@ type@ `%a',@ needed%a" UserTyp.print c pp_loc loc
  | `UnknownIdent (loc, i) ->
      pp fmt "Undeclared@ qualified@ identifier@ `%a'%a\
             " QualIdent.print i pp_loc loc
  | `UnknownField (loc, f, c) ->
      pp fmt "Undeclared@ field@ `%a'@ for@ type@ `%a'%a\
             " pp_field f UserTyp.print c pp_loc loc
  | `Typing (loc, t, t') ->
      pp fmt "Incompatible@ types%a:@ type@ %a@ is@ not@ compatible@ with@ \
              type@ %a" pp_loc loc AnyTyp.print t' AnyTyp.print t
  | `ExpectRef (loc, r) ->
      pp fmt "Reference@ expected%a:@ %a" pp_loc loc Var.print r
  | `ExpectArr (loc, r) ->
      pp fmt "Array@ expected%a:@ %a" pp_loc loc Var.print r
  | `UnexpectRef (loc, R r, _) ->
      pp fmt "Unexpected@ reference@ expression%a:@ %a" pp_loc loc Var.print r
  | `UnexpectRef (loc, Rlit l, _) ->
      pp fmt "Unexpected@ reference@ expression%a:@ %a" pp_loc loc print_lit l
  | `UnexpectArr (loc, R r, _) ->
      pp fmt "Unexpected@ array@ expression%a:@ %a" pp_loc loc Var.print r
  | `UnexpectArr (loc, Rlit l, _) ->
      pp fmt "Unexpected@ array@ expression%a:@ %a" pp_loc loc print_lit l
  | `Undecl (loc, v) ->
      pp fmt "Undeclared@ variable@ `%a'%a" Var.print v pp_loc loc
  | `Redecl (loc, v) ->
      pp fmt "Forbiden@ redeclaration@ of@ variable@ `%a'%a\
             " Var.print v pp_loc loc
  | `MissingRetVal loc ->
      pp fmt "Missing@ return@ value%a" pp_loc loc
  | `UnexpectedRet loc ->
      pp fmt "Unexpected@ returned@ expression%a" pp_loc loc
  | `WrongArrayDims (loc, { given_array_dims; expected_array_dims }) ->
      pp fmt "Wrong@ number@ of@ array@ dimentions%a:@ expected@ %i,@ got@ %i\
             " pp_loc loc expected_array_dims given_array_dims
  | `ExpectThrwbl (loc, Some r, t) ->
      pp fmt "Throwable@ reference@ expected%a:@ %a@ is@ of@ type@ %a\
             " pp_loc loc Var.print r UserTyp.print t
  | `ExpectThrwbl (loc, None, t) ->
      pp fmt "Throwable@ expected%a:@ got@ type@ %a\
             " pp_loc loc UserTyp.print t
  | `CustomError (loc, pp_msg) ->
      pp fmt "%t%a" pp_msg pp_loc loc

let pp_warning: 'l pp -> [> 'l warning] pp = fun pp_loc fmt -> function
  | `DangerousCast (loc, t, t') ->
      pp fmt "Known@ type@ hierarchy@ does@ not@ permit@ to@ validate@ \
              cast%a:@ type@ %a@ is@ not@ known@ to@ be@ compatible@ with@ \
              type@ %a\
             " pp_loc loc AnyTyp.print t' UserTyp.print t

module Checker (Sem: Semantics.S) = struct

  type ('stm, 'errs, 'warn) t =
      {
        errs: 'errs MutableStack.t;
        warn: 'warn MutableStack.t;
        rtyp: any typname option option;
      }

  let make errs warn rtyp = { errs; warn; rtyp; }

  let push_err errs e x = MutableStack.push errs e; x
  let push_warn warn e x = MutableStack.push warn e; x

  (* --- *)

  let unknown_type { errs } ?loc c =
    push_err errs (`UnknownType (loc, c))

  let unknown_qualident { errs } ?loc i =
    push_err errs (`UnknownIdent (loc, i))

  let unknown_field { errs } ?loc f c =
    push_err errs (`UnknownField (loc, f, c))

  let typing_error { errs } ?loc t t' =
    push_err errs (`Typing (loc, t, t'))

  let expected_ref { errs } ?loc v =
    push_err errs (`ExpectRef (loc, v))

  let expected_arr { errs } ?loc v =
    push_err errs (`ExpectArr (loc, v))

  let unexpected_ref { errs } ?loc r t =
    push_err errs (`UnexpectRef (loc, r, t))

  let unexpected_arr { errs } ?loc a t =
    push_err errs (`UnexpectArr (loc, a, t))

  let undeclared { errs } ?loc v =
    push_err errs (`Undecl (loc, v))

  let redeclared { errs } ?loc v =
    push_err errs (`Redecl (loc, v))

  let missing_retval { errs } ?loc =
    push_err errs (`MissingRetVal (loc))

  let unexpected_ret { errs } ?loc =
    push_err errs (`UnexpectedRet (loc))

  let wrong_array_dims { errs } ?loc e =
    push_err errs (`WrongArrayDims (loc, e))

  let expected_thrwbl { errs } ?loc r t =
    push_err errs (`ExpectThrwbl (loc, r, t))

  let custom_error { errs } ?loc pp_msg =
    push_err errs (`CustomError (loc, pp_msg))

  (* --- *)

  let dangerous_cast { warn } ?loc t t' =
    push_warn warn (`DangerousCast (loc, t, t'))

  (* --- *)

  open Sem

  let meth_decls: _ -> Sign.decl -> decls = fun errs decl ->
    let d = init_decls decl in
    let loc = `InThrowsOf decl in
    try chkthrows decl; d with
      | ExpectedThrwbl (r, t) -> push_err errs (`ExpectThrwbl (Some loc, r, t)) d

  let check_stm: _ -> decls -> int -> _ -> decls * _ = fun ({ rtyp } as chkr) d i s ->
    let loc = `Stm (i, Some s) in
    try type_stm ?rtyp d s with
      | ExpectedRef v -> d, expected_ref chkr ~loc v Nop
      | ExpectedArr v -> d, expected_arr chkr ~loc v Nop
      | UnexpectedRef (r, t) -> d, unexpected_ref chkr ~loc r t Nop
      | UnexpectedArr (a, t) -> d, unexpected_arr chkr ~loc a t Nop
      | TypeError (t, t') -> d, typing_error chkr ~loc t t' Nop
      | Undeclared v -> d, undeclared chkr ~loc v Nop
      | Redeclared v -> redeclared chkr ~loc v d, Nop
      | MissingRetVal -> d, missing_retval chkr ~loc Nop
      | UnexpectedRet -> d, unexpected_ret chkr ~loc Nop
      | WrongArrayDims e -> d, wrong_array_dims chkr ~loc e Nop
      | UnknownType c -> d, unknown_type chkr ~loc c Nop
      | UnknownFieldForType (c, f) -> d, unknown_field chkr ~loc f c Nop
      | UndeclaredQualIdent i -> d, unknown_qualident chkr ~loc i Nop
      | ExpectedThrwbl (r, t) -> d, expected_thrwbl chkr ~loc r t Nop
      | CustomError msg -> d, custom_error chkr ~loc msg Nop
      | DangerousCast ((v, t, u), t')
        -> d, dangerous_cast chkr ~loc t t' (CastR (v, t, u))

  let check_catch_entry: _ -> (nominal branch -> _ Stm.t) -> decls
    -> int -> nominal branch catch_entry -> unit =
    fun chkr lookup_handler d i { catch_typ = t; catch_at } ->
      let loc = `InCatchTbl i in
      let rec check_catch_stm = function
        | Catch r -> chkcatch d r t
        | Label (_, s) -> check_catch_stm s
        | _ -> ()
      in
      (* Already checked by `type_stm' above. *)
      ((* try chkthrwbl t with *)
       (*  | ExpectedThrwbl (r, t) -> expected_thrwbl chkr ~loc r t () *));
      let target_stm = lookup_handler catch_at in
      try check_catch_stm target_stm with
        | ExpectedRef v -> expected_ref chkr ~loc v ()
        | UnexpectedArr (a, t) -> unexpected_arr chkr ~loc a t ()
        | TypeError (t, t') -> typing_error chkr ~loc t t' ()
        | Undeclared v -> undeclared chkr ~loc v ()
        | UnknownType c -> unknown_type chkr ~loc c ()
        | ExpectedThrwbl (r, t) -> expected_thrwbl chkr ~loc r t ()

  let check_catch_table chkr lh d =
    CatchTable.iteri (check_catch_entry chkr lh d)

end
