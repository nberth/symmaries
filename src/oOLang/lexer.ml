open Sedlex_menhir

let digit = [%sedlex.regexp? nd]
let digit_nz = [%sedlex.regexp? Sub (nd, '0')]
let lc = [%sedlex.regexp? 'a' .. 'z']
let uc = [%sedlex.regexp? 'A' .. 'Z']
let letter = [%sedlex.regexp? lc | uc]
let letter_ = [%sedlex.regexp? letter | '_']
let level = [%sedlex.regexp? letter | 0x22a4 | 0x22a5]
let lcletter_ = [%sedlex.regexp? lc | '_']
let lcident = [%sedlex.regexp? lc, Star (letter_ | digit)]

(* --- *)

type flavor =                   (** flavor for qualification and identifiers  *)
  | Java                        (** Only Java for now *)
  | Jimple

let java_binop = [%sedlex.regexp? '+' | '*' | '/' | '&' | '|' | '^' | '%']
let java_ident_letter = [%sedlex.regexp? letter_]
let java_ident = [%sedlex.regexp? letter, Star (java_ident_letter | digit)]
let jimple_ident_letter = [%sedlex.regexp? letter_ | '@' | '$']
let jimple_ident = [%sedlex.regexp? jimple_ident_letter, Star (jimple_ident_letter | digit)]

(* https://docs.oracle.com/javase/specs/jls/se7/html/jls-3.html#jls-3.10.7 *)

(* Java bool literals *)
(* let java_bool = [%sedlex.regexp? "true" | "false"] *)

(* java Integer literals *)
let java_digits_n_underscores =  [%sedlex.regexp? Plus (digit | '_')]
let java_digits = [%sedlex.regexp? digit | (digit, Opt java_digits_n_underscores, digit)]
let java_underscores =  [%sedlex.regexp? Plus '_']
let java_integer_type_suffix = [%sedlex.regexp? 'l' | 'L' ]

let java_decimal_numeral = [%sedlex.regexp? '0' | digit_nz, (Opt java_digits | java_underscores, java_digits) ]
let java_decimal_integer = [%sedlex.regexp? java_decimal_numeral, Opt java_integer_type_suffix]

let java_hex_digit = [%sedlex.regexp? digit | 'a' .. 'f' | 'A' .. 'F' ]
let java_hex_digits_n_underscores =  [%sedlex.regexp? Plus (java_hex_digit | '_')]
let java_hex_digits = [%sedlex.regexp? java_hex_digit, Opt (Opt java_hex_digits_n_underscores, java_hex_digit)]
let java_hex_numeral = [%sedlex.regexp? "0x", java_hex_digits ]
let java_hex_integer = [%sedlex.regexp? java_hex_numeral, Opt java_integer_type_suffix]

let java_oct_digit = [%sedlex.regexp? '0' .. '7' ]
let java_oct_digits_n_underscores =  [%sedlex.regexp? Plus (java_oct_digit | '_')]
let java_oct_digits = [%sedlex.regexp? java_oct_digit, Opt (Opt java_oct_digits_n_underscores, java_oct_digit)]
let java_oct_numeral = [%sedlex.regexp? "0", Opt java_underscores, java_oct_digits ]
let java_oct_integer = [%sedlex.regexp? java_hex_numeral, Opt java_integer_type_suffix]

let java_bin_digit = [%sedlex.regexp? '0' | '1' ]
let java_bin_digits_n_underscores =  [%sedlex.regexp? Plus (java_bin_digit | '_')]
let java_bin_digits = [%sedlex.regexp? java_bin_digit, Opt (Opt java_bin_digits_n_underscores, java_bin_digit)]
let java_bin_numeral = [%sedlex.regexp? "0", 'b' | 'B',  java_bin_digits ]
let java_bin_integer = [%sedlex.regexp? java_bin_numeral, Opt java_integer_type_suffix]

let java_integer = [%sedlex.regexp? java_decimal_integer | java_hex_integer
                   | java_oct_integer | java_bin_integer ]

(* Java Floating point literals *)
let java_float_type_suffix = [%sedlex.regexp? 'f' | 'F' | 'd' | 'D']
let java_sign = [%sedlex.regexp? '-' | '+']
let java_signed_integer = [%sedlex.regexp? Opt java_sign, java_digits]
let java_exponent_indicator = [%sedlex.regexp? 'e' | 'E']
let java_exponent_part = [%sedlex.regexp? java_exponent_indicator, java_signed_integer]
let java_dec_float =
  [%sedlex.regexp?
      (java_digits, '.', Opt java_digits, Opt java_exponent_part, Opt java_float_type_suffix)
  |   ('.', java_digits, Opt java_exponent_part, Opt java_float_type_suffix)
  |   (java_digits, java_exponent_part, Opt java_float_type_suffix)
  |   (java_digits, Opt java_exponent_part, java_float_type_suffix) ]

let java_hex_significand =
  [%sedlex.regexp?
      (java_hex_numeral, Opt '.')
  |   ("0x" | "0X", Opt java_hex_digits, '.', java_hex_digits) ]
let java_bin_exponent_indicator = [%sedlex.regexp? 'p' | 'P']
let java_bin_exponent = [%sedlex.regexp? java_bin_exponent_indicator, java_signed_integer ]
let java_hex_float = [%sedlex.regexp? (java_hex_significand, java_bin_exponent, Opt java_float_type_suffix) ]
let java_float = [%sedlex.regexp? java_dec_float | java_hex_float]

(* Java character literals *)

(* XXX: missing weird stuffs with unicode escape leading to CR or LF here... *)
let java_raw_char_no_nl = [%sedlex.regexp? Sub (any, Chars "\r\n")]
let java_unicode_escape = [%sedlex.regexp? '\\', Plus 'u', Rep (java_hex_digit, 4)]
let java_unicode_char_no_nl = [%sedlex.regexp? java_unicode_escape | java_raw_char_no_nl]
let java_single_char_no_nl = [%sedlex.regexp? (* Sub ( *)java_unicode_char_no_nl(* , Chars "\r\n") *)]
let java_escape_seq = [%sedlex.regexp? '\\', (Chars "btnfr\"'\\"
                                                 | Rep (java_oct_digit, 1 .. 2)
                                                 | '0' .. '3', Rep (java_oct_digit, 2))]
let java_char = [%sedlex.regexp? "'", (java_single_char_no_nl | java_escape_seq), "'"]

(* Java String literals *)

let java_raw_char_no_esc = [%sedlex.regexp? Sub (any, Chars "\"\\")]
let java_unicode_char_no_esc = [%sedlex.regexp? java_unicode_escape | java_raw_char_no_esc]
let java_single_char_no_esc = [%sedlex.regexp? (* Sub ( *)java_unicode_char_no_esc(* , Chars "\"\\") *)]
let java_string_char = [%sedlex.regexp? (java_single_char_no_esc | java_escape_seq)]
let java_string = [%sedlex.regexp? '"', Star java_string_char, '"']

(* --- *)

(* let java_literal = [%sedlex.regexp? java_integer *)
(*                    | java_float *)
(*                    | java_bool *)
(*                    | java_char *)
(*                    | java_string *)
(*                    | "null" ] *)

(* --- *)

let java_comment cont buf =
  let rec comment starts ({ stream } as buf) = match%sedlex stream with
    | "*/" -> update buf; (match starts with a::b::tl -> comment (b::tl) buf | _ -> cont buf)
    | "/*" -> update buf; comment (buf.pos :: starts) buf
    | "\r\n" | "\n" | "\r" -> update buf; new_line buf; comment starts buf
    | any -> update buf; comment starts buf
    | eof -> raise @@ ParseError(* UnterminatedComments *) (err_loc buf)
    | _ -> raise @@ ParseError (err_loc buf)
  in
  comment [ buf.pos ] buf

(* --- *)

open Grammar

let rec lookup_lcident h ?(fallbacks = []) id =
  try Hashtbl.find h id with
    | Not_found -> match fallbacks with
        | [] -> Ident id
        | h :: fallbacks -> lookup_lcident h ~fallbacks id

let common_keywords, jimple_keywords =
  let open Hashtbl in
  (let k = create 100 in
   add k "void" Void;
   List.iter (fun t -> add k t (PrimTyp t))
     ["boolean"; "char"; "byte"; "short"; "int"; "long"; "float"; "double"];
   add k "true" (BoolLit "true");
   add k "false" (BoolLit "false");
   add k "null" NullLit;
   add k "static" Static;
   add k "instanceof" InstanceOf;
   add k "if" If;
   add k "goto" Goto;
   add k "switch" Switch;
   add k "new" New;
   add k "return" Return;
   add k "throw" Throw;
   add k "throws" Throws;
   add k "catch" Catch;
   add k "checkpoint" Checkpoint;
   add k "monitorenter" MonitorEnter;
   add k "monitorexit" MonitorExit;
   k),
  (let k = create 2 in
   add k "neg" (Binop "neg");
   add k "cmp" (Relop "cmp");
   add k "cmpl" (Relop "cmpl");
   add k "cmpg" (Relop "cmpg");
   k)

let lookup_java id =
  lookup_lcident common_keywords id
let lookup_jimple id =
  lookup_lcident common_keywords ~fallbacks:[jimple_keywords] id

let common_token ({ stream } as buf) = match%sedlex stream with
  (* | java_relop -> update buf; Relop (lexeme buf) *)
  | java_binop -> update buf; Binop (lexeme buf)
  | java_integer -> update buf; IntLit (lexeme buf)
  | java_float -> update buf; FltLit (lexeme buf)
  | java_char -> update buf; CharLit (lexeme buf)
  | java_string -> update buf; StrLit (lexeme buf)
  | _ -> raise @@ ParseError (err_loc buf)

let jimple_idents ({ stream } as buf) = match%sedlex stream with
  | jimple_ident -> update buf; lookup_jimple (lexeme buf)
  | _ -> common_token buf

let java_idents ({ stream } as buf) = match%sedlex stream with
  | java_ident -> update buf; lookup_java (lexeme buf)
  | _ -> common_token buf

let try_flavored = function
  | Java -> java_idents
  | Jimple -> jimple_idents

(* --- *)

let body_token ?(flavor = Java) =
  let output buf = Output (sub_lexeme buf 7 (lexeme_length buf - 7)) in
  let rec token ({ stream } as buf) = match%sedlex stream with
    | '.' -> Dot
    | '-' -> Dash
    | '!' -> Exclam
    | ',' -> Comma
    | ':' -> Colon
    | ';' -> Semicolon
    | "<<" | ">>" | ">>>" -> update buf; Binop (lexeme buf)
    | '<' | '>' | "==" | "!=" | "<=" | ">=" -> update buf; Relop (lexeme buf)
    | '=' -> Equal
    | '#' -> Hash
    | '(' -> LParen
    | ')' -> RParen
    | '{' -> LBrace
    | '}' -> RBrace
    | '[' -> LBracket
    | ']' -> RBracket
    | "output_", level -> update buf; output buf
    | Plus (Chars " \t") -> update buf; token buf
    | "\r\n" | "\n" | "\r" -> update buf; new_line buf; token buf
    | "/*" -> update buf; java_comment token buf
    | eof -> EOF
    | _ -> try_flavored flavor buf
  in
  token

(* --- *)

let taint_tag = [%sedlex.regexp? '?']
let plus_taint_tag = [%sedlex.regexp? Opt '+', taint_tag]
let secstub_token ?(flavor = Java) =
  let tag p buf = Some (sub_lexeme buf p 1) in
  let rec token ({ stream } as buf) = match%sedlex stream with
    | "<~", level, plus_taint_tag -> update buf;  LtTildaTag (tag 2 buf, true)
    | "<~", level -> update buf; LtTildaTag (tag 2 buf, false)
    | "<~", taint_tag -> update buf; LtTildaTag (None, true)
    | "<~" -> update buf; LtTilda
    | "new_", level, plus_taint_tag -> update buf; NewTag (tag 4 buf, true)
    | "new_", level -> update buf; NewTag (tag 4 buf, false)
    | "new_", taint_tag -> update buf; NewTag (None, true)
    | ".*", level, plus_taint_tag -> update buf; DotStarTag (tag 2 buf, true)
    | ".*", level -> update buf; DotStarTag (tag 2 buf, false)
    | ".*", taint_tag -> update buf; DotStarTag (None, true)
    | ".*" -> DotStar
    | "*", level, plus_taint_tag -> update buf; StarTag (tag 1 buf, true)
    | "*", level -> update buf; StarTag (tag 1 buf, false)
    | '*', taint_tag -> update buf; StarTag (None, true)
    | '*' -> Star
    | '@' -> Arobase
    | '%' -> Percent
    | Plus (Chars " \t") -> update buf; token buf
    | "\r\n" | "\n" | "\r" -> update buf; new_line buf; token buf
    | "/*" -> update buf; java_comment token buf
    | eof -> EOF
    | _ -> body_token ~flavor buf
  in
  token

(* --- *)

type types_flavor =
  | Flat
  | Class

let flat_token ~flavor ({ stream } as buf) = match%sedlex stream with
  (* no specific tokens for now *)
  | _ -> try_flavored flavor buf


let class_token ~flavor ({ stream } as buf) = match%sedlex stream with
  | "<:" -> SubTypeRel
  | _ -> try_flavored flavor buf

let types_extra = function
  | Flat -> flat_token
  | Class -> class_token

let types_token ?(style = Flat) ?(flavor = Java) =
  let rec token ({ stream } as buf) = match%sedlex stream with
    | '.' -> Dot
    | ',' -> Comma
    | ":" -> Colon
    | ';' -> Semicolon
    | '{' -> LBrace
    | '}' -> RBrace
    | '[' -> LBracket
    | ']' -> RBracket
    | Plus (Chars " \t") -> update buf; token buf
    | "\r\n" | "\n" | "\r" -> update buf; new_line buf; token buf
    | "/*" -> update buf; java_comment token buf
    | eof -> EOF
    | _ -> types_extra style ~flavor buf
  in
  token
