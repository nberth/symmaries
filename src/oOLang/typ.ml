open Rutils
open Utils

(* --- *)

type prim = Prim
type user = User
type arry = Arry
type any  = Any
type _ spec =
  | Prim: prim spec
  | User: user spec
  | Arry: arry spec
type _ typname =
  | PrimTyp : string -> prim typname
  | UserTyp : string -> user typname
  | ArryTyp : 'a spec * 'a typname -> arry typname
  | Any : 'a spec * 'a typname -> any typname

(* type (_,_) eq = Eq : ('a,'a) eq *)

(* let eq_spec (type a) (type b) : a spec -> b spec -> (a, b) eq option = *)
(*   fun a b -> match a, b with *)
(*     | Prim, Prim -> Some Eq *)
(*     | User, User -> Some Eq *)
(*     | _ -> None *)

let any (type a) : a typname -> any typname = function
  | PrimTyp _ as t -> Any (Prim, t)
  | UserTyp _ as t -> Any (User, t)
  | ArryTyp _ as t -> Any (Arry, t)
  | Any _ as t -> t

let rec array:
type a. a typname -> arry typname = function
  | PrimTyp _ as t -> (ArryTyp (Prim, t))
  | UserTyp _ as t -> (ArryTyp (User, t))
  | ArryTyp _ as t -> (ArryTyp (Arry, t))
  | Any (_, t) -> array t

let prim s = any (PrimTyp s)
let user s = any (UserTyp s)
let rec array_of:
type a. a typname -> any typname = function
  | PrimTyp _ as t -> any (ArryTyp (Prim, t))
  | UserTyp _ as t -> any (ArryTyp (User, t))
  | ArryTyp _ as t -> any (ArryTyp (Arry, t))
  | Any (_, t) -> array_of t

let rec is_prim:
type k. k typname -> bool = function
  | PrimTyp _ -> true
  | UserTyp _ -> false
  | ArryTyp _ -> false
  | Any (_, t) -> is_prim t

let rec if_usertyp:
type k. ?recurse: bool -> (user typname -> unit) -> k typname -> unit =
    fun ?(recurse = false) f -> function
      | UserTyp _ as t -> f t
      | ArryTyp _ when not recurse -> ()
      | ArryTyp (User, t) -> if_usertyp ~recurse f t
      | ArryTyp (Arry, t) -> if_usertyp ~recurse f t
      | Any (_, t) -> if_usertyp ~recurse f t
      | PrimTyp _ -> ()
      | ArryTyp _ -> ()

let rec acc_usertyp:
type k. ?recurse: bool -> (user typname -> 'a -> 'a) -> k typname -> 'a -> 'a =
    fun ?(recurse = false) f -> function
      | UserTyp _ as t -> f t
      | ArryTyp _ when not recurse -> fun a -> a
      | ArryTyp (User, t) -> acc_usertyp ~recurse f t
      | ArryTyp (Arry, t) -> acc_usertyp ~recurse f t
      | Any (_, t) -> acc_usertyp ~recurse f t
      | PrimTyp _ -> fun acc -> acc
      | ArryTyp _ -> fun acc -> acc

let rec if_arrytyp:
type k. ?recurse: bool -> (arry typname -> unit) -> k typname -> unit =
    fun ?(recurse = false) f -> function
      | ArryTyp (_, s) as t -> f t; if recurse then if_arrytyp ~recurse f s;
      | Any (_, t) -> if_arrytyp ~recurse f t
      | PrimTyp _ -> ()
      | UserTyp _ -> ()

let rec acc_arrytyp:
type k. ?recurse: bool -> (arry typname -> 'a -> 'a) -> k typname -> 'a -> 'a =
    fun ?(recurse = false) f -> function
      | ArryTyp (_, s) as t when not recurse -> fun a -> f t a
      | ArryTyp (_, s) as t -> fun a -> f t a |> acc_arrytyp ~recurse f s
      | Any (_, t) -> acc_arrytyp ~recurse f t
      | PrimTyp _ -> fun acc -> acc
      | UserTyp _ -> fun acc -> acc

let rec array_access:
type a. arry typname -> any typname = function
  | ArryTyp (_, t) -> any t

let as_array: any typname -> arry typname option = fun t ->
  acc_arrytyp ~recurse:false (fun a _ -> Some a) t None

let array_dims t =
  acc_arrytyp ~recurse:true (fun _ -> succ) t 0

let iter_arrysubrel fa fu =
  if_arrytyp ~recurse:true (fun at ->
    if_arrytyp ~recurse:false (fa at) (array_access at);
    if_usertyp ~recurse:false (fu at) (array_access at))

(* --- *)

module type TYPSPEC = sig type s end
module type TYPNAME = sig
  include TYPSPEC
  include ORDERED_HASHABLE_TYPE
end

(* --- *)

module TypName = struct
  type 'a t =
      'a typname =
    | PrimTyp : string -> prim t
    | UserTyp : string -> user t
    | ArryTyp : 'a spec * 'a t -> arry t
    | Any : 'a spec * 'a t -> any t
  let rec compare:
  type x y. x t -> y t -> int = fun t t' -> match t, t' with
    | PrimTyp t, PrimTyp t' -> Stdlib.compare t t'
    | UserTyp t, UserTyp t' -> Stdlib.compare t t'
    | ArryTyp (_, t), ArryTyp (_, t') -> compare t t'
    | Any (_, t), t' -> compare t t'
    | t, Any (_, t') -> compare t t'
    | PrimTyp _, _ -> -1
    | _, PrimTyp _ -> 1
    | ArryTyp _, _ -> -1
    | _, ArryTyp _ -> 1
  let equal t t' = t == t' || compare t t' = 0
  let hash = Hashtbl.hash
  let rec print:
  type k. k t pp = fun fmt -> function
    | PrimTyp t -> Format.pp_print_string fmt t
    | UserTyp t -> Format.pp_print_string fmt t
    | ArryTyp (_, t) -> pp fmt "%a[]" print t
    | Any (_, t) -> print fmt t

  (* let eq (type a) (type b) : a typname -> b typname -> (a, b) eq option = *)
  (*   fun a b -> match a, b with *)
  (*     | PrimTyp _, PrimTyp _ -> Some Eq *)
  (*     | UserTyp _, UserTyp _ -> Some Eq *)
  (*     | _ -> None *)

end

(* --- *)

module AnyTyp: TYPNAME with type s = any and type t = any typname = struct
  type s = any
  type t = any typname
  include (TypName: ORDERED_HASHABLE_TYPE with type t := t)
end
module AnyTyps = Helpers.MakeSet (AnyTyp)
module AnyTypMap = Helpers.MakeMap (AnyTyp)

(* --- *)

module PrimTyp: TYPNAME with type s = prim and type t = prim typname = struct
  type s = prim
  type t = prim typname
  include (TypName: ORDERED_HASHABLE_TYPE with type t := t)
end
module PrimTyps = Helpers.MakeSet (PrimTyp)
module PrimTypMap = Helpers.MakeMap (PrimTyp)

(* --- *)

module UserTyp: TYPNAME with type s = user and type t = user typname = struct
  type s = user
  type t = user typname
  include (TypName: ORDERED_HASHABLE_TYPE with type t := t)
end
module UserTyps = MakeSet (UserTyp)
module UserTypMap = MakeMap (UserTyp)

(* --- *)

module ArryTyp: sig
  include TYPNAME with type s = arry and type t = arry typname
end = struct
  type s = arry
  type t = arry typname
  include (TypName: ORDERED_HASHABLE_TYPE with type t := t)
end
module ArryTyps = MakeSet (ArryTyp)
module ArryTypMap = MakeMap (ArryTyp)

(* --- *)

module type OPS = sig
  type deftyp
  val ( <: ): deftyp -> deftyp -> bool
  val ( ==: ): deftyp -> deftyp -> bool
  val may_alias: deftyp -> deftyp -> bool
end

module type TYP_SPEC = sig
  type t
end

module type TYP_DECL = sig
  type t
  val print: t pp
  val is_empty: t -> bool
  val equal: t -> t -> bool
  val valid_redecl: t -> t -> bool
end

module type TYP_DECLS = sig
  type t
  type spec
  type decl
  val empty: t
  val cardinal: t -> int
  val is_empty: t -> bool
  val mem: user typname -> t -> bool
  val decl: user typname -> spec -> t -> t
  val respec: user typname -> spec -> t -> t
  val redecl: user typname -> decl -> t -> t
  val find: user typname -> t -> decl
  val find': user typname -> t -> decl option
  val iter: (user typname -> decl -> unit) -> t -> unit
  val fold: (user typname -> decl -> 'a -> 'a) -> t -> 'a -> 'a
  val print: t pp
  val bad_respec: user typname -> spec -> t -> bool
  type specs = (user typname * spec) list
  val of_specs: specs -> t
  module type DB = sig val db: t end
end

(* --- *)

exception UnknownType of user typname
exception TypeRedeclaration of user typname

(* --- *)

module MakeTypDecls
  (Spec: TYP_SPEC)
  (Decl: sig include TYP_DECL val make: user typname -> Spec.t -> t end)
  =
struct
  open UserTypMap

  type decl = Decl.t
  type spec = Spec.t
  type t = decl UserTypMap.t

  let empty = empty
  let mem = mem
  let cardinal = cardinal
  let is_empty = is_empty
  let iter = iter
  let fold = fold
  let find = find
  let print = print' ~left:"@[<v>" ~right:"@]" ~aright:"@];" ~sep:"@ "
    ~skip_val:(fun _ -> Decl.is_empty) ~assoc:"@ " Decl.print
  let find' t db = try Some (find t db) with Not_found -> None
  let respec t s = add t (Decl.make t s)
  let decl t s db =(if mem t db then raise @@ TypeRedeclaration t;
                    respec t s db)
  let redecl = add
  let bad_respec t s db = match find' t db with
    | None -> false
    | Some d -> not (Decl.valid_redecl (Decl.make t s) d)

  type specs = (user typname * spec) list
  let of_specs = List.fold_left (fun acc (tn, td) -> decl tn td acc) empty;;

  module type DB = sig val db: t end

end

(* --- *)

module Field = String
module FieldMap = struct
  include StrMap
  let print fmt =
    print' ~empty:""
      ~left:"{ @[" ~sep:"" ~right:"@]}"
      ~rev_assoc:true ~aleft:"" ~assoc:"@ " ~aright:";@ "
      fmt
end
type field = Field.t
let pp_field = Field.print

(* --- *)

module type FIELD_ATTRS_SPEC = sig
  include ORDERED_HASHABLE_TYPE
  val inheritable: t -> bool
end
module type FIELD_ATTRS = sig
  module Spec: FIELD_ATTRS_SPEC
  include ORDERED_HASHABLE_TYPE
  type typspec
  val instance_field: t -> bool
  val inheritable: t -> bool
  val from_spec: user typname -> typspec typname -> Spec.t -> t
end

(* --- *)

module type RECORD_DECL = sig
  type s
  type attrs
  type t = (s typname * attrs) FieldMap.t
  exception FieldRedeclaration of field * s typname * s typname
  exception FieldAttributesDiffer of field * s typname * attrs * attrs
  val empty: t
  val none: t
  val is_empty: t -> bool
  val equal: t -> t -> bool
  val merge: t -> t -> t
  val inheritable: t -> t
  val inherit_fields: t -> t -> t
  val diff: t -> t -> t
  val valid_redecl: t -> t -> bool                    (** [valid_redecl n old]  *)
  val one: field -> s typname * attrs -> t
  val mult: field list -> s typname * attrs -> t
  val list: t list -> t
  val iter: (field -> s typname * attrs -> unit) -> t -> unit
  val fold: (field -> s typname * attrs -> 'a -> 'a) -> t -> 'a -> 'a
  val print: t pp
end

module MakeFieldsDecl
  (FTSpec: TYPSPEC)
  (FAttrs: FIELD_ATTRS_SPEC)
  :
  (RECORD_DECL with type s = FTSpec.s and type attrs = FAttrs.t) =
struct
  include FTSpec
  type attrs = FAttrs.t

  type t = (s typname * attrs) FieldMap.t

  exception FieldRedeclaration of field * s typname * s typname
  exception FieldAttributesDiffer of field * s typname * attrs * attrs

  let empty: t = FieldMap.empty
  let none: t = empty
  let is_empty: t -> bool = FieldMap.is_empty
  let equal: t -> t -> bool = FieldMap.equal (eq_pair TypName.equal FAttrs.equal)
  let merge: t -> t -> t = fun x ->
    FieldMap.union (fun f (t, a) (t', a') ->
      if TypName.equal t t' then
        if FAttrs.equal a a' then Some (t, a)
        else raise @@ FieldAttributesDiffer (f, t, a, a')
      else raise @@ FieldRedeclaration (f, t, t')) x
  let inheritable = FieldMap.filter (fun _ (_, a) -> FAttrs.inheritable a)
  let inherit_fields fields = FieldMap.union (fun _ o n -> Some n) fields
  let diff a b = FieldMap.merge (fun _ a -> function
    | None -> a
    | Some _ -> None) a b
  let valid_redecl n old = FieldMap.is_empty @@ FieldMap.merge
    (fun _ fn fold -> match fn, fold with
      | Some (tn, an), Some (told, aold) when (TypName.equal tn told
                                               && FAttrs.equal an aold) -> None
      | _, old -> old) n old             (* b may only have more fields than a *)
  let one: field -> s typname * attrs -> t = FieldMap.singleton
  let of_list fl t = List.fold_left (fun acc f -> merge (one f t) acc) none fl
  let mult: field list -> s typname * attrs -> t = of_list
  let list = List.fold_left merge none
  let iter = FieldMap.iter
  let fold = FieldMap.fold
  let print = FieldMap.print (pp_pair_rev
                                ~format:"%a%a" FAttrs.print TypName.print)
end

(* --- *)

exception UnknownFieldForType of user typname * field
exception IncorrectFieldAccess of any typname * field

module type FIELD_OPS = sig
  type rectyp
  type ftypspec
  type fattrs
  val dot_f: rectyp -> field -> ftypspec typname * fattrs
end

module type RECORD_DECLS = sig
  module FieldsSpec: RECORD_DECL
  module Fields: RECORD_DECL
  type spec = FieldsSpec.t
  type decl = Fields.t
  include TYP_DECLS with type spec := spec and type decl := decl
  module CloseFields: functor (DB: DB) ->
    FIELD_OPS with type rectyp = user typname
              and type ftypspec = Fields.s
              and type fattrs = Fields.attrs
end

module MakeRecordDecls
  (FieldTypSpec: TYPSPEC)
  (FieldAttrs: FIELD_ATTRS with type typspec = FieldTypSpec.s)
  :
  (RECORD_DECLS with type FieldsSpec.s = FieldTypSpec.s
                and type Fields.s = FieldTypSpec.s
                and type FieldsSpec.attrs = FieldAttrs.Spec.t
                and type Fields.attrs = FieldAttrs.t) =
struct
  module FieldsSpec = MakeFieldsDecl (FieldTypSpec) (FieldAttrs.Spec)
  module Fields = MakeFieldsDecl (FieldTypSpec) (FieldAttrs)
  type ftyp = Fields.s typname
  type fattrs = Fields.attrs

  include MakeTypDecls (FieldsSpec) (struct
    include Fields
    let make: user typname -> FieldsSpec.t -> t = fun r ->
      FieldMap.map (fun (t, a) -> t, FieldAttrs.from_spec r t a)
  end)

  let dot_f': t -> user typname -> field -> ftyp * fattrs = fun db ut f ->
    match find' ut db with
      | None -> raise @@ UnknownType ut
      | Some flds -> try FieldMap.find f flds with
          | Not_found -> raise @@ UnknownFieldForType (ut, f)

  module CloseFields (DB: DB) = struct
    type rectyp = user typname
    type ftypspec = Fields.s
    type fattrs = Fields.attrs
    let rec dot_f:
    type k. k typname -> field -> ftyp * fattrs = function
      | PrimTyp _ as t -> fun f -> raise @@ IncorrectFieldAccess (any t, f)
      | ArryTyp _ as t -> fun f -> raise @@ IncorrectFieldAccess (any t, f)
      | UserTyp _ as t -> dot_f' DB.db t
      | Any (_, t) -> dot_f t
  end
end

(* --- *)

exception IncompleteRecordDecls of (user typname * user typname FieldMap.t) list

module MakeCheckKnownFieldTypes
  (TypDecl: sig type t val fields: t -> any typname FieldMap.t end)
  (TypDecls: TYP_DECLS with type decl = TypDecl.t)
  (DB: TypDecls.DB)
  =
struct
  open TypDecls

  let acc_undef f = acc_usertyp ~recurse:true
    (fun t ferrs -> if not (mem t DB.db) then FieldMap.add f t ferrs else ferrs)

  (* check all field types are known *)
  let errs = fold begin fun t fdefs errs ->
    let ferrs = FieldMap.fold acc_undef (TypDecl.fields fdefs) FieldMap.empty in
    if FieldMap.is_empty ferrs then errs else (t, ferrs) ::errs
  end DB.db [];;
  if errs <> [] then raise @@ IncompleteRecordDecls errs;;
end

(* --- *)

module type GENERAL_RECORD_DECLS = sig
  include RECORD_DECLS
  module CheckKnownFieldTypes: functor (DB: DB) -> sig end
end

module GeneralRecordDecls
  (FieldAttrs: FIELD_ATTRS with type typspec = any)
  :
  (GENERAL_RECORD_DECLS with type FieldsSpec.s = any
                        and type FieldsSpec.attrs = FieldAttrs.Spec.t
                        and type Fields.s = any
                        and type Fields.attrs = FieldAttrs.t) =
struct
  module TypDecls = MakeRecordDecls (AnyTyp) (FieldAttrs)
  include TypDecls

  module CheckKnownFieldTypes = MakeCheckKnownFieldTypes (struct
    type t = TypDecls.decl
    let fields x = FieldMap.map fst x
  end) (TypDecls)
end

(* --- *)

module NoFieldAttrsSpec: FIELD_ATTRS_SPEC with type t = unit = struct
  type t = unit
  let compare () () = 0
  let equal _ _ = true
  let hash () = 0
  let print fmt a = pp fmt ""
  let instance_field _ = true
  let inheritable = instance_field
end

module NoFieldAttrs = struct
  include NoFieldAttrsSpec
  let from_spec _ _ s = s
end

(* --- *)
