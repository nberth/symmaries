open Format
open Rutils
open Utils
open Base
open Typ
open Sign

(* --- *)

type 'd reference = Ref : 'd -> 'd reference
type 'd value = Value : 'd -> 'd value
type defined = Defined
type untyped = var

type ('access, 'def) variable =
  | P : var (* * 'dom *) -> ('dom value, defined) variable
  | R : var (* * 'dom *) -> ('dom reference, defined) variable

type uncasted = Uncasted
type _ imm =
  | V : var -> _ imm
  | L : lit -> _ imm
  | C : any typname * uncasted imm -> unit imm
type immediate = unit imm

type imref =
  | R : var -> imref
  | Rlit : lit -> imref

let rec print_immediate:
type k. k imm pp = fun fmt -> function
  | V v -> Var.print fmt v
  | L l -> print_lit fmt l
  | C (t, i) -> pp fmt "(%a) %a" AnyTyp.print t print_immediate i

(* --- *)

type 'dom typed = ('dom, defined) variable
type ('security, 'link, 'typing) t =
  | Nop : _ t
  | Decl : any typname * var list -> (_, _, untyped) t
  | Assign : qualident * qual expr -> (_, _, untyped) t
  | AssignP : var * unqual expr -> (_, _, _ typed) t
  | AssignR : var * imref -> (_, _, _ typed) t
  | LoadP : var * var * field -> (_, _, _ typed) t
  | LoadR : var * var * field -> (_, _, _ typed) t
  | StoreP : var * field * unqual expr -> (_, _, _ typed) t
  | StoreR : var * field * imref -> (_, _, _ typed) t
  | SLoadP : var * user typname * field -> (_, _, _ typed) t
  | SLoadR : var * user typname * field -> (_, _, _ typed) t
  | SStoreP : user typname * field * unqual expr -> (_, _, _ typed) t
  | SStoreR : user typname * field * imref -> (_, _, _ typed) t
  | ALoad : var * var * unqual expr -> (_, _, untyped) t
  | ALoadP : var * var * unqual expr -> (_, _, _ typed) t
  | ALoadR : var * var * unqual expr -> (_, _, _ typed) t
  | AStore : var * unqual expr * qual expr -> (_, _, untyped) t
  | AStoreP : var * unqual expr * unqual expr -> (_, _, _ typed) t
  | AStoreR : var * unqual expr * imref -> (_, _, _ typed) t
  | ALen : var * var -> _ t
  | UCall : qualident * methname * immediate list -> (_, _, untyped) t
  | USCall : var * qualident * methname option * immediate list -> (_, _, untyped) t
  | UFCall : (immediate, 'dom) Sign.effcall -> (_, _, 'dom typed) t
  | MCall : var * any typname option * qualident * methname * immediate list -> (_, _, untyped) t
  | MSCall : var * any typname option * var * user typname * methname * immediate list -> (_, _, untyped) t
  | MFCallP : var * (immediate, 'dom) Sign.effcall -> (_, _, 'dom typed) t
  | MFCallR : var * (immediate, 'dom) Sign.effcall -> (_, _, 'dom typed) t
  | New : var * user typname -> _ t
  | ANew : var * any typname * unqual expr list -> _ t
  | InstOf : var * var * any typname -> (_, _, untyped) t
  | InstOfR : var * var * user typname -> (_, _, 'dom typed) t
  | InstOfA : var * var * arry typname -> (_, _, 'dom typed) t
  | Cast : var * any typname * var -> (_, _, untyped) t
  | CastP : var * prim typname * var -> (_, _, 'dom typed) t
  | CastR : var * user typname * var -> (_, _, 'dom typed) t
  | CastA : var * arry typname * var -> (_, _, 'dom typed) t
  | Output : 'security * immediate list -> ('security, _, untyped) t
  | OutputPR : 'security * (immediate * 'dom) list -> ('security, _, 'dom typed) t
  | ReturnU : _ t
  | Return : qual expr -> (_, _, untyped) t
  | ReturnP : unqual expr -> (_, _, _ typed) t
  | ReturnR : imref -> (_, _, _ typed) t
  | ChkPoint : var -> _ t
  | Goto : 'link -> (_, 'link, _) t
  | CondGoto : cond * 'link -> (_, 'link, _) t
  | Switch: unqual expr * 'link switch_entries -> (_, 'link, _) t
  | Throw : var -> _ t
  | Catch : var -> _ t
  | MonEnter : immediate -> _ t
  | MonExit : immediate -> _ t
  | Label : label * ('security, 'link, 'typing) t -> ('security, 'link, 'typing) t
and 'link switch_entries =
    {
      swch_cases: (lit * 'link) list;
      swch_default: 'link;
    }

type ('s, 'a, 'k) pprt = ?pad:int -> ?pp_lvl:'s pp -> 'a pp -> ('s, 'a, 'k) t pp

let rec print:
type k. ('s, 'a, k) pprt = fun ?(pad = 0) ?pp_lvl pp_dest ->
  let pv = Var.print and pe = Expr.print and pc = Expr.Cond.print
  and pl = pp_label and pf = Field.print and pT = TypName.print
  and pQ = QualIdent.print and pM = pp_methname and pI = print_immediate in
  let pp_vl = pp_lst ~fopen:"" ~fclose:"" ~fsep:", " pv in
  let pp_svl pe = pp_lst ~fopen:"(" ~fclose:")" ~fsep:", " pe in
  let pAI = pp_svl pI and pAI' fmt lst = pp_svl pI fmt (List.map fst lst) in
  let pCT fmt = function None -> () | Some t -> pp fmt "(%a) " pT t in
  let pEC fmt = function
    | { fkind = VirtCall { inst = Some (this, _); name }; eff_args } ->
        pp fmt "%a.%a %a" pI this pM name pAI' eff_args
    | { fkind = VirtCall { name }; fclazz; eff_args } ->
        pp fmt "%a.%a %a" pT fclazz pM name pAI' eff_args
    | { fkind = SpecCall { this; name }; fclazz; eff_args } ->
        pp fmt "%a#%a.%a %a" pI this pT fclazz pM name pAI' eff_args
    | { fkind = CstrCall { this }; fclazz; eff_args } ->
        pp fmt "%a#%a %a" pI this pT fclazz pAI' eff_args
  in
  let rec pAD fmt = function
    | [] -> ()
    | e :: tl -> pp fmt "[%a]" pe e; pAD fmt tl in
  let pIR fmt = function R r -> pv fmt r | Rlit l -> print_lit fmt l in
  let pL fmt l = match pp_lvl with Some p -> pp fmt "_%a" p l | None -> () in
  let pp_padded fmt f = pp fmt ("%.*s"^^f) pad in
  let pp fmt f = pp_padded fmt f ""
  and pplabeled fmt l f = pp_padded fmt f (asprintf "%a: " pl l) in
  let pp_swtch_entries fmt { swch_cases; swch_default } =
    pp fmt "{@[<h>%i@ cases@ &@ %i@ default@]}" (List.length swch_cases) 1
  in
  fun fmt -> function
    | Nop -> pp fmt "nop"
    | Decl (t, rl) -> pp fmt "%a %a" pT t pp_vl rl
    | Assign (qi, e) -> pp fmt "%a = %a" pQ qi pe e
    | AssignP (v, e) -> pp fmt "%a = %a" pv v pe e
    | AssignR (r, s) -> pp fmt "%a = %a" pv r pIR s
    | LoadP (v, r, f) -> pp fmt "%a = %a.%a" pv v pv r pf f
    | LoadR (r, s, f) -> pp fmt "%a = %a.%a" pv r pv s pf f
    | StoreP (r, f, e) -> pp fmt "%a.%a = %a" pv r pf f pe e
    | StoreR (r, f, s) -> pp fmt "%a.%a = %a" pv r pf f pIR s
    | SLoadP (v, c, f) -> pp fmt "%a = %a.%a" pv v pT c pf f
    | SLoadR (r, c, f) -> pp fmt "%a = %a.%a" pv r pT c pf f
    | SStoreP (c, f, e) -> pp fmt "%a.%a = %a" pT c pf f pe e
    | SStoreR (c, f, r) -> pp fmt "%a.%a = %a" pT c pf f pIR r
    | ALoad (v, a, e) -> pp fmt "%a = %a[%a]" pv v pv a pe e
    | ALoadP (v, a, e) -> pp fmt "%a = %a[%a]" pv v pv a pe e
    | ALoadR (r, a, e) -> pp fmt "%a = %a[%a]" pv r pv a pe e
    | AStore (a, e, e') -> pp fmt "%a[%a] = %a" pv a pe e pe e'
    | AStoreP (a, e, e') -> pp fmt "%a[%a] = %a" pv a pe e pe e'
    | AStoreR (a, e, r) -> pp fmt "%a[%a] = %a" pv a pe e pIR r
    | ALen (v, a) -> pp fmt "%a = #%a" pv v pv a
    | UCall (q, m, il) -> pp fmt "%a.%a %a" pQ q pM m pAI il
    | USCall (r, c, None, il) -> pp fmt "%a#%a %a" pv r pQ c pAI il
    | USCall (r, c, Some m, il) -> pp fmt "%a#%a:%a %a" pv r pQ c pM m pAI il
    | UFCall ec -> pEC fmt ec
    | MCall (v, ct, q, m, il) -> pp fmt "%a = %a%a.%a %a\
                                       " pv v pCT ct pQ q pM m pAI il
    | MSCall (v, ct, r, c, m, il) -> pp fmt "%a = %a%a#%a:%a %a\
                                           " pv v pCT ct pv r pT c pM m pAI il
    | MFCallP (v, ec) -> pp fmt "%a = %a" pv v pEC ec
    | MFCallR (r, ec) -> pp fmt "%a = %a" pv r pEC ec
    | New (r, t) -> pp fmt "%a = new %a" pv r pT t
    | ANew (r, t, d) -> pp fmt "%a = new (%a)%a" pv r pT t pAD d
    | InstOf (v, r, t) -> pp fmt "%a = %a instanceof %a" pv v pv r pT t
    | InstOfR (v, r, t) -> pp fmt "%a = %a instanceof %a" pv v pv r pT t
    | InstOfA (v, r, t) -> pp fmt "%a = %a instanceof %a" pv v pv r pT t
    | Cast (r, t, s) -> pp fmt "%a = (%a) %a" pv r pT t pv s
    | CastP (v, t, w) -> pp fmt "%a = (%a) %a" pv v pT t pv w
    | CastR (r, t, s) -> pp fmt "%a = (%a) %a" pv r pT t pv s
    | CastA (r, t, s) -> pp fmt "%a = (%a) %a" pv r pT t pv s
    | Output (l, vl) -> pp fmt "output%a %a" pL l pAI vl
    | OutputPR (l, prl) -> pp fmt "output%a %a" pL l pAI' prl
    | ReturnU -> pp fmt "return"
    | Return v -> pp fmt "return %a" pe v
    | ReturnP v -> pp fmt "return %a" pe v
    | ReturnR v -> pp fmt "return %a" pIR v
    | ChkPoint v -> pp fmt "checkpoint %a" pv v
    | Goto l -> pp fmt "goto %a" pp_dest l
    | CondGoto (c, l) -> pp fmt "if (%a) goto %a" pc c pp_dest l
    | Switch (e, se) -> pp fmt "switch [%a] %a" pe e pp_swtch_entries se
    | Throw r -> pp fmt "throw %a" pv r
    | Catch r -> pp fmt "%a = catch" pv r
    | MonEnter r -> pp fmt "monitorenter %a" pI r
    | MonExit r -> pp fmt "monitorexit %a" pI r
    | Label (l, s) -> pplabeled fmt l "%a" (print ~pad:0 ?pp_lvl pp_dest) s

(* --- *)

type ('a(* , 'x *)) catch_entry =
    {
      catch_typ: user typname;
      catch_from: 'a;
      catch_to: 'a;
      catch_at: (* 'x *)'a;
    }

module CatchTable = struct
  type 'a t = 'a catch_entry list
  let mapi f (ct: 'a t) : 'b list =
    List.fold_left
      (fun (i, ct') ce -> succ i, match f i ce with
        | None -> ct'
        | Some e -> e :: ct')
      (0, []) ct |> snd |> List.rev
  let fold_mapi f (ct: 'a t) (acc: 'x) : 'b list * 'x =
    List.fold_left
      (fun (i, (ct', acc)) ce -> succ i, match f i ce acc with
        | None, acc -> ct', acc
        | Some e, acc -> e :: ct', acc)
      (0, ([], acc)) ct |> (fun (_, (ct', acc)) -> List.rev ct', acc)
  let foldi f ct acc =
    List.fold_left (fun (i, acc) ce -> succ i, f i ce acc) (0, acc) ct |> snd
  let iteri: _ -> 'a t -> unit = List.iteri
  let is_empty: 'a t -> bool = fun ct -> ct = []
end

type 'a catch_table = 'a CatchTable.t

(* --- *)

type nominal
(* type exceptional *)
type _ branch =
  | Jump : label * pc -> nominal branch

let print_branch fmt = function
  | Jump (l, _) -> pp_label fmt l

let target:
type k. k branch -> pc = function
  | Jump (_, pc) -> pc

type ('s, 'k) unlinked = ('s, label, 'k) t
type ('s, 'k) linked = ('s, nominal branch, 'k) t

(* --- *)

let rec delabel = function
  | Label (_, s) -> delabel s
  | s -> s

let rec is_branching = function
  | CondGoto _ -> true
  | Switch _ -> true
  | Label (_, s) -> is_branching s
  | _ -> false

let rec is_return:
type k. ('s, 'a, k) t -> bool = function
  | Label (_, s) -> is_return s
  | ReturnU  -> true
  | Return _ -> true
  | ReturnP _ -> true
  | ReturnR _ -> true
  | _ -> false

let rec is_methcall:
type k. ('s, 'a, k) t -> bool = function
  | Label (_, s) -> is_methcall s
  | UCall _ -> true
  | USCall _ -> true
  | UFCall _ -> true
  | MCall _ -> true
  | MSCall _ -> true
  | MFCallP _ -> true
  | MFCallR _ -> true
  | _ -> false

let rec is_nop = function
  | Label (_, s) -> is_nop s
  | Nop -> true
  | _ -> false

let rec acc_labels acc = function
  | Label (l, s) -> acc_labels (l :: acc) s
  | _ -> acc

let rec apply_labels s = function
  | [] -> s
  | l :: tl -> apply_labels (Label (l, s)) tl

(* --- *)

type 'typing raw = (Security.raw, label, 'typing) t
let print_raw ?pad = print ?pad ~pp_lvl:Security.pp_raw pp_label
let print_raw' fmt = print_raw fmt
let print_linked ?pad ?pp_lvl = print ?pad ?pp_lvl print_branch

(* --- *)

module Ops = struct
  let ( !^ ) : immediate -> uncasted imm = function
    | V _ as i -> i
    | L _ as i -> i
    | C (_, i) -> i
end

(* --- *)

let rec assigned_by = function
  | AssignP (v, _) | CastP (v, _, _)
  | LoadP (v, _, _) | SLoadP (v, _, _) | ALoadP (v, _, _)
  | MFCallP (v, _) | ALen (v, _) ->
      Vars.singleton v
  | AssignR (r, _) | CastR (r, _, _) | CastA (r, _, _)
  | StoreP (r, _, _) | AStoreP (r, _, _)
  | LoadR (r, _, _) | SLoadR (r, _, _) | ALoadR (r, _, _)
  | StoreR (r, _, _) | AStoreR (r, _, _)
  | New (r, _) | ANew (r, _, _)
  | Catch r
  | MFCallR (r, _) ->
      Vars.singleton r
  | Label (_, stm) ->
      assigned_by stm
  | _ ->
      Vars.empty

(* --- *)

open Ops

let if_immediate_deref f i = match !^i with
  | V r -> f r
  | L _ -> ign

let if_methcall_deref f = Sign.(function
  | { fkind = VirtCall { inst = Some (this, _) }; }
  | { fkind = CstrCall { this } } ->
      (match !^this with V r -> f r | L _ -> ign)
  | _ -> ign)

let if_ref_access on_access =                         (* if 'must' ref access *)
  let rec access_ref = function
    | LoadP (_, r, _) | ALoadP (_, r, _)
    | LoadR (_, r, _) | ALoadR (_, r, _)
    | StoreP (r, _, _) | AStoreP (r, _, _)
    | StoreR (r, _, _) | AStoreR (r, _, _)
    | ALen (_, r)
    | Throw r as stm
      -> on_access stm r
    | UFCall ec
    | MFCallP (_, ec)
    | MFCallR (_, ec) as stm
      -> if_methcall_deref (on_access stm) ec
    | MonEnter i
    | MonExit i as stm
      -> if_immediate_deref (on_access stm) i
    | Nop
    | AssignR _
    | AssignP _
    | SLoadP _
    | SLoadR _
    | SStoreP _
    | SStoreR _
    | New _ | ANew _
    | InstOfR _ | InstOfA _
    | CastP _ | CastR _ | CastA _               (* casts accept null operands *)
    | OutputPR _
    | ReturnU | ReturnR _ | ReturnP _
    | ChkPoint _
    | Goto _
    | CondGoto _
    | Switch _
    | Catch _
      -> ign
    | Label (_, s)
      -> access_ref s
  in
  access_ref

let if_unsafe_cast on_unsafe_cast =
  let rec unsafe_cast: _ t -> 'a -> 'a = function
    | CastR (_, _, r) | CastA (_, _, r) as stm
      -> on_unsafe_cast stm r
    | Label (_, s)
      -> unsafe_cast s
    | _
      -> ign
  in
  unsafe_cast

let if_array_alloc on_array_alloc =
  let rec aux: _ t -> 'a -> 'a = function
    | ANew (r, c, idxs) as stm
      -> on_array_alloc stm
    | Label (_, s)
      -> aux s
    | _
      -> ign
  in
  aux

let if_array_access on_array_access =
  let rec aux: _ t -> 'a -> 'a = function
    | ALoadP _ | ALoadR _
    | AStoreP _ | AStoreR _ as stm
      -> on_array_access stm
    | Label (_, s)
      -> aux s
    | _
      -> ign
  in
  aux

let array_access_idx f = if_array_access (function
  | ALoadP (_, _, e) | ALoadR (_, _, e)
  | AStoreP (_, e, _) | AStoreR (_, e, _) -> f e
  | _ -> ign)
