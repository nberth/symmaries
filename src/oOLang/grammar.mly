%{
  open Typ
  open Base
  open Method
  let tagging = function
    | Some l, false -> SecStub.TL (Security.of_raw l)
    | Some l, true -> SecStub.TLT (Security.of_raw l)
    | None, true -> SecStub.TT
    | None, false -> SecStub.default_tag
  let tagging' t = Some (tagging t)
  let atagging = function
    | Some l, false -> `Tag (SecStub.TL (Security.of_raw l))
    | Some l, true -> `Tag (SecStub.TLT (Security.of_raw l))
    | None, true -> `Tag SecStub.TT
    | None, false -> `FromArgs
%}
%token <string> Ident
%token <string> IntLit
%token <string> FltLit
%token <string> BoolLit
%token <string> CharLit
%token <string> StrLit
%token NullLit
%token <string> PrimTyp
%token <string> Binop
%token <string> Relop
%token <string> Output
%token Static
%token InstanceOf
%token Comma
%token Dot
%token Colon
%token Hash
%token Semicolon
%token Equal
%token LParen
%token RParen
%token New
%token If
%token Goto
%token Switch
%token Void
%token Return
%token Checkpoint
%token Throw
%token Throws
%token Catch
%token MonitorEnter
%token MonitorExit

%token Dash
%token Exclam
%token SubTypeRel
%token LBracket
%token RBracket
%token LBrace
%token RBrace

%token EOF

(* secstub-specific *)
%token Star
%token DotStar
%token Arobase
%token Percent
%token LtTilda
%token <string option * bool> StarTag
%token <string option * bool> DotStarTag
%token <string option * bool> LtTildaTag
%token <string option * bool> NewTag

(* --- *)

%left Binop Relop
%nonassoc Dash
%right unary_op

(* --- *)

%start <Method.Impl.t> raw_meth
%start <Method.Impl.t> meth
%start <SecStub.t list> meth_secstubs
%start <Java.FlatHeap.TypDecls.specs> java_flat_typ_specs
%start <Java.ClassesOfPrim.TypDecls.specs> java_classes_of_prim_specs
%start <Java.Heap.TypDecls.specs> java_classes_specs

%type <Stm.immediate> direct_immediate
%type <Stm.uncasted Stm.imm> uncasted_immediate
%type <Base.qual Base.Expr.t> qual_expr

%%
meth:
| meth_sign_decl LBrace stms catch_table RBrace EOF
    { Impl.Meth { sign = $1;
                  stms = Utils.Seq.of_list $3, $4; } }
  ;

  meth_sign_decl:
| usertyp LParen var_decls RParen throws {
  Sign.(C (Decl.cstr $1 $3 $5))
}
| rettyp usertyp Colon ident LParen var_decls RParen throws {
  Sign.(C (Decl.meth ~static:false $2 $4 $1 $6 $8))
}
| Static rettyp usertyp Colon ident LParen var_decls RParen throws {
  Sign.(C (Decl.meth ~static:true $3 $5 $2 $7 $9))
}
| ioption (Static) usertyp throws {
  Sign.(I (Decl.clinit $2 $3))
}
  ;

  throws:
| { UserTyps.empty }
| preceded (Throws, separated_nonempty_list (Comma, usertyp)) {
  UserTyps.of_list $1
}
  ;

  (* ---- *)

  meth_secstubs:
| list (terminated (meth_secstub, option (Semicolon))) EOF { $1 }
  ;

  arg_typs:
| separated_list (Comma, typ) { $1 }
  ;

  meth_secstub:
| usertyp LParen arg_typs RParen throws secstub_stms {
  SecStub.make_cstr $6 $1 $3 $5
}
| rettyp usertyp Colon ident LParen arg_typs RParen throws secstub_stms {
  SecStub.make_virt $9 $2 $4 $1 $6 $8
}
| Static rettyp usertyp Colon ident LParen arg_typs RParen throws secstub_stms {
  SecStub.make_static $10 $3 $5 $2 $7 $9
}
  ;

  rettyp:
| typ { Some $1 }
| Void { None }
  ;

var_decls:
| separated_list (Comma, var_decl) { $1 }
;

var_decl:
| typ ident { $1, $2 }
;

(* Summaries of security signatures *)

secstub_stms:
| delimited (LBrace,
             list (terminated (secstub_stm, option (Semicolon))),
             RBrace) { $1 }
| { [] }
  ;

%inline secstub_stm:
| Output { SecStub.Output (Security.of_raw $1) }
| Return secstub_res_base { SecStub.Result $2 }
| Dash LtTilda { SecStub.Mutate `None }
| Star LtTilda secstub_source { SecStub.Mutate (`Any $3) }
| Star LtTildaTag { SecStub.Mutate (`Any (atagging $2)) }
| Arobase LtTilda secstub_source { SecStub.Mutate (`Args $3) }
| Arobase LtTildaTag { SecStub.Mutate (`Args (atagging $2)) }
| DotStar LtTilda secstub_source { SecStub.Mutate (`Fields $3) }
| DotStar LtTildaTag { SecStub.Mutate (`Fields (atagging $2)) }
| Dash Exclam { SecStub.Trust `None }
| Arobase Exclam { SecStub.Trust `Args }
| DotStar Exclam { SecStub.Trust `Fields }
;

secstub_source:
| Arobase { `FromArgs }
| Percent LParen secstub_source_policy RParen { `Policy $3 }
;

secstub_source_policy:
| Star option(preceded(Colon, ident)) { `Both ($2, $2) }
| ident option(preceded(Colon, ident)) { match $1 with
    | "level" -> `Level $2
    | "taint" -> `Taint $2
    | x -> raise Parsing.Parse_error
}
;

secstub_res_base:
| DotStarTag { `DotStar (tagging' $1) }
| DotStar { `DotStar None }
| StarTag { `Star (tagging' $1) }
| Star { `Star None }
| Arobase { `Arg }
| NewTag option(delimited(LParen, secstub_res_deps, RParen)) {
  `New (tagging' $1, match $2 with None -> `None | Some x -> x) }
| New option(delimited(LParen, secstub_res_deps, RParen)) {
  `New (None, match $2 with None -> `None | Some x -> x) }
;

secstub_res_deps:
| Arobase { `Args }
| DotStar { `DotStar }
| Star { `Any }
| Percent LParen secstub_source_policy RParen { `Policy $3 }
;

(* ------------------------------------------------------------------------ *)

(* Identifiers *)

%inline ident:
| Ident { $1 }
;

%inline idents:
| separated_nonempty_list (Comma, ident) { $1 }
;

qual:
| qual ident Dot { QualIdent.append $2 $1 }
| ident Dot { QualIdent.init $1 }
;

%inline qualident:
| qual ident { QualIdent.append $2 $1 }
;

%inline any_ident:
| qualident { $1 }
| ident { QualIdent.init $1 }
;

(* Types *)

%inline typ:
| basetyp { any $1 }
| arrytyp { any $1 }
;

%inline basetyp:
| primtyp { any $1 }
| usertyp { any $1 }
;

%inline primtyp:
| PrimTyp { PrimTyp $1 }
;

%inline usertyp:
| any_ident { UserTyp (QualIdent.to_string $1) }
;

%inline qual_usertyp:
| qual ident { UserTyp (QualIdent.to_string (QualIdent.append $2 $1)) }
;

arrytypdims:
| LBracket RBracket { array }
| arrytypdims LBracket RBracket { (fun x -> $1 (any (array x))) }
;

%inline arrytyp:
| basetyp arrytypdims { $2 $1 }
;

(* Literals *)

%inline typcast:
| delimited (LParen, typ, RParen) { $1 }
;

%inline primtypcast:
| delimited (LParen, primtyp, RParen) { $1 }
;

%inline nonprimtypcast:
| delimited (LParen, usertyp, RParen) { any $1 }
| delimited (LParen, arrytyp, RParen) { any $1 }
;

%inline extended_lit:
| direct_lit { $1 }
| delimited (LParen, typlit, RParen) { $1 }
;

%inline typlit:
| usertyp { Type (any $1) }
| arrytyp { Type (any $1) }
;

%inline arrytyplit:
| arrytyp { Type (any $1) }
;

%inline direct_lit:
| lit { $1 }
| right_liberal_lit { $1 }
| Dash IntLit { Int ("-"^$2) }
| Dash FltLit { Flt ("-"^$2) }
| Exclam BoolLit { Bool ("!"^$2) }
;

%inline right_liberal_lit:
| arrytyp { Type (any $1) }
| qual_usertyp { Type (any $1) }
;

lit:
| IntLit { Int $1 }
| FltLit { Flt $1 }
| BoolLit { Bool $1 }
| CharLit { Char $1 }
| StrLit { Str $1 }
| NullLit { Null }
;

(* Expressions *)

%inline prefixop:
| Dash { "-" }
| Exclam { "!" }
;

%inline binop:
| Dash { "-" }
| Binop { $1 }
;

unqual_expr_noident:
| lit { Expr.Lit $1 }
| prefixop unqual_expr %prec unary_op { Expr.Unop ($1, $2) }
| unqual_expr binop unqual_expr { Expr.Binop ($1, $2, $3) }
| unqual_expr Relop unqual_expr { Expr.Relop ($1, $2, $3) }
| unqual_expr Relop right_liberal_lit { Expr.Relop ($1, $2, Expr.Lit $3) }
| LParen primtypcast unqual_expr RParen { Expr.Cast ($2, $3) }
| LParen unqual_expr_noident RParen { $2 }
;

unqual_expr:
| ident { Expr.Var $1 }
| LParen ident RParen  { Expr.Var $2 }
| unqual_expr_noident { $1 }
;

qual_expr:
| any_ident { Expr.Ident $1 }
| lit { Expr.Lit $1 }
| arrytyplit { Expr.Lit $1 }
| prefixop unqual_expr { Expr.Unop ($1, $2) }
| unqual_expr binop unqual_expr { Expr.Binop ($1, $2, $3) }
| unqual_expr Relop unqual_expr { Expr.Relop ($1, $2, $3) }
| nonprimtypcast NullLit { Expr.Lit Null }    (* XXX: ignore casts of null *)
| nonprimtypcast StrLit { Expr.Lit (Str $2) } (* XXX: ignore casts of strings *)
| primtypcast unqual_expr { Expr.Cast ($1, $2) }
| LParen unqual_expr_noident RParen { Expr.qual $2 }
;

cond:
| unqual_expr { Expr.Cond.Expr $1 }
;

(* Array access *)

arrydims:
| nonempty_list (delimited (LBracket, unqual_expr, RBracket)) { $1 }
;

(* Immediates *)

%inline direct_immediate:
| ident { Stm.V $1 }
| direct_lit { Stm.L $1 }
| primtyp { Stm.L (Type (any $1)) }
;

%inline uncasted_immediate:
| ident { Stm.V $1 }
| extended_lit { Stm.L $1 }
;

immediate:
| direct_immediate { $1 }
| typcast uncasted_immediate { Stm.C ($1, $2) }
;

immediates:
| separated_nonempty_list (Comma, immediate) { $1 }
;

maybe_immediates:
| separated_list (Comma, immediate) { $1 }
;

(* Statements *)

stms:
| nonempty_list (terminated (stm, Semicolon)) { $1 }
;

stm:
| typ idents { Stm.Decl ($1, $2) }
| any_ident Equal qual_expr { Stm.Assign ($1, $3) }
| ident Equal ident LBracket unqual_expr RBracket { Stm.ALoad ($1, $3, $5) }
| ident LBracket unqual_expr RBracket Equal qual_expr { Stm.AStore ($1, $3, $6) }
| ident Equal Hash ident { Stm.ALen ($1, $4) }
| qual ident LParen maybe_immediates RParen { Stm.UCall ($1, $2, $4) }
| ident Hash any_ident LParen maybe_immediates RParen { Stm.USCall ($1, $3, None, $5) }
| ident Hash any_ident Colon ident LParen maybe_immediates RParen { Stm.USCall ($1, $3, Some $5, $7) }
| ident Equal qual ident LParen maybe_immediates RParen { Stm.MCall ($1, None, $3, $4, $6) }
| ident Equal typcast qual ident LParen maybe_immediates RParen { Stm.MCall ($1, Some $3, $4, $5, $7) }
| ident Equal ident Hash usertyp Colon ident LParen maybe_immediates RParen { Stm.MSCall ($1, None, $3, $5, $7, $9) }
| ident Equal typcast ident Hash usertyp Colon ident LParen maybe_immediates RParen { Stm.MSCall ($1, Some $3, $4, $6, $8, $10) }
| ident Equal New usertyp { Stm.New ($1, $4) }
| ident Equal New LParen typ RParen arrydims { Stm.ANew ($1, $5, $7) }
| ident Equal New typ arrydims { Stm.ANew ($1, $4, $5) }
| ident Equal ident InstanceOf typ { Stm.InstOf ($1, $3, $5) }
| ident Equal nonprimtypcast ident { Stm.Cast ($1, $3, $4) }
| Colon { Stm.Nop }
| Output direct_immediate { Stm.Output ($1, [$2]) }
| Output LParen immediates RParen { Stm.Output ($1, $3) }
| Return qual_expr { Stm.Return $2 }
| Return { Stm.ReturnU }
| Checkpoint ident { Stm.ChkPoint ($2) }
| Goto ident { Stm.Goto ($2) }
| If cond Goto ident { Stm.CondGoto ($2, $4) }
| switch_stm { $1 }
| Throw ident { Stm.Throw ($2) }
| ident Equal Catch { Stm.Catch ($1) }
| MonitorEnter immediate { Stm.MonEnter ($2) }
| MonitorExit immediate { Stm.MonExit ($2) }
| ident Colon stm { Stm.Label ($1, $3) }
  ;

switch_stm:
| Switch LBracket unqual_expr RBracket switch_entries0 { Stm.Switch ($3, $5) };
switch_entries0:
| delimited (LBrace, terminated (switch_entries, Semicolon), RBrace) { $1 }
| switch_entries { $1 }
switch_entries:
| list (terminated (switch_entry, Semicolon)) Goto ident {
  Stm.{ swch_cases = $1; swch_default = $3 }
};
switch_entry:
| If lit Goto ident { $2, $4 }
;

catch_table: list (terminated (catch_entry, Semicolon)) { $1 };
catch_entry:
| Catch usertyp Colon ident Dash ident Colon ident {
  Stm.{ catch_typ = $2; catch_from = $4; catch_to = $6; catch_at = $8; }
}
;

raw_meth:
| stms catch_table EOF { Impl.Raw (Utils.Seq.of_list $1, $2) }
;

(* -------------------------------------------------------------------------- *)

(* Common Java rules *)

%inline java_class_name:
| usertyp { $1 }
;

java_classes:
| separated_nonempty_list (Comma, java_class_name) { $1 }
;

java_inheritance:
| { UserTyps.empty }
| SubTypeRel java_classes { UserTyps.of_list $2 }
;

java_fields_attr:
| { Java.FieldAttrs.Spec.Instance }
| Static { Java.FieldAttrs.Spec.Static }
;

(* Java-style flat type declarations: *)

java_flat_typ_specs:
| list (java_flat_typ_spec) EOF { $1 }
;

java_flat_typ_spec:
| java_class_name java_fields_specs option(Semicolon)
    { Java.FlatHeap.typspec $1 $2 }
;

java_fields_specs:
| { [] }
| delimited (LBrace, java_fields_specs_list, RBrace) { $1 }
;

java_fields_specs_list:
| list (terminated (java_fields_decl, option(Semicolon))) { $1 }
;

java_fields_decl:
| java_fields_attr typ idents {
  Java.FlatHeap.TypDecls.FieldsSpec.mult $3 ($2, $1) }
;

(* Java-style hierarchy of classes wiht primitive fields only: *)

java_classes_of_prim_specs:
| list (java_classes_of_prim_spec) EOF { $1 }
;

java_classes_of_prim_spec:
| java_class_name java_inheritance java_prim_fields_specs option(Semicolon)
    { Java.ClassesOfPrim.typspec $1 $2 $3 }
;

java_prim_fields_specs:
| { [] }
| delimited (LBrace, java_prim_fields_specs_list, RBrace) { $1 }
;

java_prim_fields_specs_list:
| list (terminated (java_prim_fields_decl, option(Semicolon))) { $1 }
;

java_prim_fields_decl:
| java_fields_attr PrimTyp idents {
  Java.ClassesOfPrim.TypDecls.FieldsSpec.mult $3 (PrimTyp $2, $1) }
;

(* Full Java-style classes hierarchy: *)

java_classes_specs:
| list (terminated (java_class_spec, option(Semicolon))) EOF { $1 }
;

java_class_spec:
| java_class_name java_inheritance java_classes_fields_specs
    { Java.Heap.typspec $1 $2 $3 }
;

java_classes_fields_specs:
| { [] }
| delimited (LBrace, java_classes_fields_specs_list, RBrace) { $1 }
;

java_classes_fields_specs_list:
| list (terminated (java_classes_fields_decl, option(Semicolon))) { $1 }
;

java_classes_fields_decl:
| java_fields_attr typ idents {
  Java.Heap.FieldsSpec.mult $3 ($2, $1) }
;
