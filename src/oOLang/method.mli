open Rutils
open Utils
open Base
open Sign
open Linking
open Stm

(* --- *)

type 'dom error =
  [
  | untyped loc Typing.error
  | 'dom typed loc Linking.error
  ]
and 'dom warning =
  [
  | untyped loc Typing.warning
  ]
and 'typing loc = [ `Stm of int * 'typing Stm.raw option
                  | `InThrowsOf of Sign.decl
                  | `InCatchTbl of int ] option
val pp_error: 'dom error pp
val pp_warning: 'dom warning pp

(* --- *)

module Body: sig
  type 'typing unlinked = (Security.raw, 'typing) Stm.unlinked
  type 'typing linked = (Security.raw, 'typing) Stm.linked
  type 'dom stm = (Security.t, 'dom typed) Stm.linked

  type raw_stms = untyped unlinked Seq.any
  type raw_catch_table = label catch_table
  type raw = raw_stms * raw_catch_table
  type 'dom t

  val print_unlinked: ('typing unlinked Seq.any * raw_catch_table) pp
  val print: 'dom t pp

  val length: 'dom t -> int
  val stm_at_pc: 'dom t -> pc -> 'dom stm
  val valid_pc: 'dom t -> pc -> bool
  val last_valid_pc: 'dom t -> pc
  val first_invalid_pc: 'dom t -> pc

  val iter_stms: (pc -> 'dom stm -> unit) -> 'dom t -> unit
  val fold_stms: (pc -> 'a -> 'dom stm -> 'a) -> 'a -> 'dom t -> 'a
  val fold_map_stms: ('a -> pc -> 'dom stm -> 'a * 'b) -> 'b -> 'a -> 'dom t
    -> 'b Seq.for_ra * 'a

  val catch_table: 'dom t -> nominal branch catch_table

  val of_typed_stm_seq: 'dom typed unlinked Seq.any
    -> ('dom t, [> 'dom error ] list) result
end
type 'dom body = 'dom Body.t
open Body

(* --- *)

open Sign

module Impl: sig
  type t =
    | Raw of raw
    | Meth of { sign: decl; stms: raw; }
  val print: t pp
end
type impl = Impl.t

(* --- *)

module ImplStats: sig
  type 'n cardinals =
      {
        decl_stats: 'n Semantics.DeclStats.cardinals;
        nb_stms: 'n;
      }
  val print_cardinals
    : ?pk:(?k0:string -> string pp) -> ?k0:string -> 'n pp -> 'n cardinals pp
  val map_cardinals: ('a -> 'b) -> 'a cardinals -> 'b cardinals
  val acc_cardinals: ('a -> 'b -> 'c) -> 'a cardinals -> 'b cardinals -> 'c cardinals
end
type _ stats =
  | Cardinals : int ImplStats.cardinals stats

module type STATS = sig
  include module type of ImplStats
  type t
  val stats: 's stats -> t -> 's
end

(* --- *)

open Typ

module type T = sig
  module Sem: Semantics.S
  type t
  type call = Sem.FCall.t
  val print: t pp
  val of_impl
    : impl
    -> (t, methname option * Sem.domain error list) result
    * (methname option * Sem.domain warning list)
  val sign: t -> Sign.decl option
  val body: t -> Sem.domain body
  val decls: t -> Sem.decls
  val fold_effcalls: (pc -> ?res:var -> call -> 'a -> 'a) -> t -> 'a -> 'a
  module ImplStats: STATS with type t := t
  include ScgsUtils.IO.BINARY with type t := t
end

module Compiler (Sem: Semantics.S) : (T with module Sem = Sem)

(* --- *)

module type RESOLVABLE = sig
  module Meth: T
  type t
  val fallback: pc -> ?res:var -> Meth.call -> Sign.callable_decl -> t
end

module type RESOLUTION = sig
  module Meth: T
  open Meth
  type elt
  type t
  type universe = elt Sem.FCall.Lookup.t
  val lookup_all: Meth.t -> universe
    -> t PCMap.t * [ `All of call | `Some of call * UserTyps.t ] Map.t
end

module type RESOLUTIONS = sig
  module Meth: T
  type e
  type universe = e Meth.Sem.FCall.Lookup.t

  module Direct (F: sig
    val merge: pc -> ?res:var -> Meth.call -> e -> e list -> e
  end) : (RESOLUTION with module Meth := Meth
                     and type elt := e
                     and type t := e)

  module Filtered (F: sig
    type t
    val filter: e -> t
    val merge: pc -> ?res:var -> Meth.call -> t -> t list -> t
  end) : (RESOLUTION with module Meth := Meth
                     and type elt := e
                     and type t := F.t)
end

module Resolutions (Meth: T) (E: RESOLVABLE with module Meth := Meth) :
  (RESOLUTIONS with module Meth := Meth and type e = E.t)

(* --- *)
