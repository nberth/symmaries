open Format
open Rutils
open Utils
open Typ

(* --- *)

module Var = String
module Vars = struct
  include Strings
  module Ops = struct
    let ( ++ ) s r = add r s
    let ( +++ ) = union
  end
end
module VarMap = struct
  include MakeMap (Var)
  let keys m = fold (fun v _ -> Vars.add v) m Vars.empty
  let of_keys s d = Vars.fold (fun v -> add v d) s empty
end
type var = Var.t
type vars = Vars.t
type +'a varmap = 'a VarMap.t

module VarsProd = SetProd (Vars)
module VarMapProd = MapProd (VarMap)

(* --- *)

module type QUAL_IDENT = sig
  include ORDERED_HASHABLE_TYPE
  val make: string list -> string -> t
  val to_string: t -> string
  val init: string -> t
  val append: string -> t -> t
  val unqual: t -> bool
  val dequal: t -> t option * string
end
module QualIdent: QUAL_IDENT = struct
  type t = string list
  let compare = cmp_lst String.compare
  let equal a b = compare a b = 0
  let hash: t -> int = Hashtbl.hash
  let print: t pp = fun fmt x ->
    pp_lst ~fopen:"" ~fsep:"." ~fclose:"" String.print fmt (List.rev x)
  let to_string: t -> string = asprintf "%a" print
  let make qual id = id :: qual
  let init id = [ id ]
  let append = List.cons
  let unqual = function [ _ ] -> true | _ -> false
  let dequal = function
    | [] -> assert false                                               (* temp *)
    | [x] -> None, x
    | x :: tl -> Some tl, x
end
type qualident = QualIdent.t

(* --- *)

type lit =
  | Int of string
  | Flt of string
  | Str of string
  | Bool of string
  | Char of string
  | Type of any typname
  | Null

let print_lit fmt = function
  | Int s | Flt s | Bool s | Char s | Str s -> pp fmt "%s" s
  | Type t -> AnyTyp.print fmt t
  | Null -> pp fmt "null"

(* --- *)

type unqual = UnQual
type qual = Qual
module Expr = struct

  type relop = string
  type op = string
  type _ t =
    | Lit : lit -> _ t
    | Var : var -> unqual t
    | Ident : qualident -> qual t
    | Unop : op * unqual t -> _ t
    | Binop : unqual t * op * unqual t -> _ t
    | Relop : unqual t * relop * unqual t -> _ t
    | Cast : prim typname * unqual t -> _ t

  let qual: unqual t -> qual t = function
    | Lit _ as e -> e
    | Var v -> Ident (QualIdent.init v)
    | Unop _ as e -> e
    | Binop _ as e -> e
    | Relop _ as e -> e
    | Cast _ as e -> e

  let rec print:
  type k. k t pp = fun fmt -> function
    | Lit l -> print_lit fmt l
    | Var v -> Var.print fmt v
    | Ident q -> QualIdent.print fmt q
    | Unop (o, x) -> pp fmt "%s %a" o print x
    | Binop (x, o, x') -> pp fmt "%a %s %a" print x o print x'
    | Relop (x, o, x') -> pp fmt "%a %s %a" print x o print x'
    | Cast (t, x) -> pp fmt "(%a) %a" PrimTyp.print t print x

  open Vars.Ops

  let rec vars = function
    | Var v -> Vars.singleton v
    | Unop (_, a) | Cast (_, a) -> vars a
    | Binop (a, _, b)
    | Relop (a, _, b) -> vars a +++ vars b
    | _ -> Vars.empty

  let fold_vars f =
    let rec fold = function
      | Var v -> f v
      | Unop (_, a) | Cast (_, a) -> fold a
      | Binop (a, _, b)
      | Relop (a, _, b) -> fun acc -> acc |> fold a |> fold b
      | _ -> fun acc -> acc
    in fold

  module Cond = struct

    type e = unqual t
    type t =
      | Expr of e
      | Rel of e * relop * e

    let var v = Expr (Var v)

    let printe = print
    let rec print fmt = function
      | Expr e -> printe fmt e
      | Rel (x, o, x') -> pp fmt "%a %s %a" printe x o printe x'

    let expr_vars = vars
    let rec vars = function
      | Expr e ->  expr_vars e
      | Rel (a, _, b) -> expr_vars a +++ expr_vars b

    let fold_vars f =
      let fev = fold_vars f in
      let rec fold = function
        | Expr e -> fev e
        | Rel (a, _, b) -> fun acc -> acc |> fev a |> fev b
      in fold

  end
end
type 'a expr = 'a Expr.t
type cond = Expr.Cond.t

let vars_in_exprs =
  List.fold_left (fun acc e -> Vars.union (Expr.vars e) acc) Vars.empty

(* --- *)

type label = string
let pp_label = pp_print_string

(* --- *)

type methname = string
let pp_methname = pp_print_string

(* --- *)

module PC = struct
  include Integer
  let start = 0
  let succ = succ
  let is_valid pc = pc >= 0
  let neg i = assert (i > 0); -i
  let is_neg i = i < 0
  let next_neg i = assert (i <= 0); pred i
end
module PCs = Helpers.MakeSet (PC)                   (* TODO: bit-vector-based *)
module PCMap = Helpers.MakeMap (PC)
type pc = PC.t

(* --- *)

type field = Typ.field
