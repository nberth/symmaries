open Rutils
open Utils
open Base
open Typ
open Stm

(* --- *)

module DeclStats = struct
  open BasicStats.Outputs

  type 'n cardinals =
      {
        nb_prims: 'n;
        nb_refs: 'n;
      }

  let print_cardinals ?(pk = ps) ?k0 pe fmt { nb_prims; nb_refs } =
    let pk = pk ?k0 in
    pp fmt "@[<2>%a: %a@],@;\
            @[<2>%a: %a@]"
      pk "nb_prims" pe nb_prims
      pk "nb_refs" pe nb_refs

  let map_cardinals f { nb_prims; nb_refs } =
    { nb_prims = f nb_prims;
      nb_refs  = f nb_refs; }

  let acc_cardinals f a b =
    { nb_prims = f a.nb_prims b.nb_prims;
      nb_refs  = f a.nb_refs  b.nb_refs; }

end
type _ stats =
  | Cardinals : int DeclStats.cardinals stats

module type STATS = sig
  include module type of DeclStats
  type decls
  val stats: 's stats -> decls -> 's
end

(* --- *)

exception Redeclared of var
exception Undeclared of var
exception UndeclaredQualIdent of qualident
exception ExpectedRef of var
exception ExpectedArr of var
exception UnexpectedRef of imref * any typname option
exception UnexpectedArr of imref * arry typname
exception TypeError of any typname * any typname
exception MissingRetVal
exception UnexpectedRet
exception ExpectedThrwbl of var option * user typname
exception CustomError of PPrt.pu
exception DangerousCast of (var * user typname * var) * any typname

type array_dims_error = { expected_array_dims: int;
                          given_array_dims: int; }
exception WrongArrayDims of array_dims_error

module type S = sig
  module Decls: Decls.T with type var = Var.t and type s = any
  type var = Decls.var
  type decls = Decls.t
  type prmdom
  type refdom
  type domain
  type ('security, 'link) stm = ('security, 'link, domain typed) Stm.t
  val static_void: decls
  val init_decls: Sign.decl -> decls
  val ret_some: Sign.decl -> bool
  val ret_refp: Sign.decl -> bool
  val ret_refdom: Sign.decl -> refdom option
  val args: Sign.decl -> domain varmap
  val refs: decls -> refdom varmap
  val prms: decls -> prmdom varmap
  module DeclStats: STATS with type decls := decls
  val instvar: var
  val excnvar: var
  val excndom: refdom
  val vardom: decls -> var -> domain
  val excdom: decls -> var -> user typname
  val nolits: (immediate * domain) list -> var list * var list
  val type_stm
    : ?rtyp: any typname option
    -> decls
    -> ('security, 'link, untyped) Stm.t
    -> decls * ('security, 'link) stm
  val unsafe_cast: decls -> any typname -> var -> bool
  val chkcatch: decls -> var -> Sign.exc -> unit
  val chkthrwbl: Sign.exc -> unit
  val chkthrows: Sign.decl -> unit
  val add_throw: Sign.decl -> Sign.exc -> Sign.decl
  val merge_throws: Sign.decl -> Sign.decl -> Sign.decl
  module FCall: sig
    open Sign
    module Lookup: LOOKUP
    type t = (immediate, domain) effcall
    val print_typsign: t pp
    type callsign

    (** Return an exact signature descriptor, which may involve undefined
        types. *)
    val methsign: callsign -> sign

    (** Returns the most general actual method signature. *)
    val concrete_methsign: callsign -> sign

    (** Construct a call signature that is specialized for the given class
        type. *)
    val specialize: UserTyp.t -> callsign -> callsign

    (** Returned user-defined types are all eligible classes for which no entry
        was found for the requested call; returned Integer is the number of
        classes explored during method lookup *)
    val lookup: callsign -> 'a Lookup.t -> 'a list * UserTyps.t * int
    val acc_lookup: ('a -> 'b -> 'b) -> callsign -> 'a Lookup.t -> 'b -> 'b * UserTyps.t * int

    val argmap: t -> callable declaration -> immediate argmap
    val typsign: ('a, domain) effcall -> sign
    val callsign: ('a, domain) effcall -> callsign
    val call: callable declaration -> (var, domain) effcall
    val arg_prms_refs: (var, domain) effcall -> prmdom varmap * refdom varmap
    val dispatch_support_prms_refs: ('a, domain) effcall -> 'a list * 'a list
  end
  module FCalls: Sign.SET with module Map = FCall.Lookup
                          and type elt = FCall.callsign
  val on_exc: 'a branch catch_table -> ?exctyps: UserTyps.t -> pc
    -> (int * 'a branch) list * bool (* true => full catch for sure *)
  val thrbltyp: Sign.exc
  val npexctyp: Sign.exc
  val ccexctyp: Sign.exc
  val negsztyp: Sign.exc
  val aindxtyp: Sign.exc
  val unchecked_excntyps: Sign.excs
  module RefRels: sig
    val may_alias: refdom -> refdom -> bool
    val may_field_alias: refdom -> refdom -> bool
    val may_share_mutable: refdom -> refdom -> bool
    val may_field_share_mutable: refdom -> refdom -> bool
  end
  val known_typ: Typ.any Typ.typname -> bool
end
