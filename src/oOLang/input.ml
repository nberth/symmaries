open Sedlex_menhir
open Sedlexing

(* --- *)

module Make (P: sig
  type t
  type options
  val default: options
  val parse: options -> Sedlex_menhir.lexbuf -> t
end) = struct
  open P
  let from_string ?(options = default) s =
    make_lexbuf (Utf8.from_string s) |> parse options
  let from_ic ?(options = default) ?file ic =
    make_lexbuf ?file (Utf8.from_channel ic) |> parse options
  let from_file ?options file =
    let ic = open_in file in
    let res = from_ic ?options ~file ic in
    close_in ic; res
end

(* --- *)

module LexerOptions = struct
  type options = { flavor: Lexer.flavor }
  let default = { flavor = Lexer.Jimple }
end

(* --- *)

module RawMeth = Make (struct
  type t = Method.Impl.t
  include LexerOptions
  let parse { flavor } = sedlex_with_menhir
    (Lexer.body_token ~flavor)
    Grammar.raw_meth
end)

(* --- *)

module Method = Make (struct
  type t = Method.Impl.t
  include LexerOptions
  let parse { flavor } = sedlex_with_menhir
    (Lexer.body_token ~flavor)
    Grammar.meth
end)

(* --- *)

module SecStubs = Make (struct
  type t = SecStub.t list
  include LexerOptions
  let parse { flavor } = sedlex_with_menhir
    (Lexer.secstub_token ~flavor)
    Grammar.meth_secstubs
end)

(* --- *)

module Java = struct
  open Java

  module Flat = Make (struct
    type t = FlatHeap.TypDecls.specs
    include LexerOptions
    let parse { flavor } = sedlex_with_menhir
      (Lexer.types_token ~flavor ~style:Lexer.Flat)
      Grammar.java_flat_typ_specs
  end)

  module ClassesOfPrim = Make (struct
    type t = ClassesOfPrim.TypDecls.specs
    include LexerOptions
    let parse { flavor } = sedlex_with_menhir
      (Lexer.types_token ~flavor ~style:Lexer.Class)
      Grammar.java_classes_of_prim_specs
  end)

  module Classes = Make (struct
    type t = Heap.TypDecls.specs
    include LexerOptions
    let parse { flavor } = sedlex_with_menhir
      (Lexer.types_token ~flavor ~style:Lexer.Class)
      Grammar.java_classes_specs
  end)

end

(* --- *)
