open Format
open Rutils
open Utils
open Graph
open Base

let level = Log.Debug
let logger = Log.mk ~level "CG"

module type S = sig
  module Compiler: Method.T
  module Entry: sig
    type t =
        {
          decl: Sign.decl;
          filename: string;
          deps: Compiler.Sem.FCalls.t;
          impl: Method.impl;
        }
    include ORDERED_HASHABLE_TYPE with type t := t
    include Sign.EXTENDED_SIGN with type t := t
                               and module K = Typ.UserTyp
  end
  module G: Sig.P with type V.t = Entry.t
  type t = G.t
  val from_filenames
    : (string -> (Method.impl, 'a) result)
    -> string list
    -> (t * Compiler.Sem.FCalls.t,
       [> `InFile of string * 'a
       |  `InMeth of string * Compiler.Sem.domain Method.error list
       |  `InMeth' of string * _ Compiler.Sem.stm Checking.error list ]) result
    * ([> `InFile of string * Compiler.Sem.domain Method.warning list
       |  `InMeth of string * Compiler.Sem.domain Method.warning list ] list)
  val output_dot: t pp
end

module Make (Sem: Semantics.S) : S with module Compiler.Sem = Sem = struct
  module Compiler = Method.Compiler (Sem)
  module ArgConsts = Checking.ArgConsts (Sem)
  module FCall = Compiler.Sem.FCall
  module FCalls = Compiler.Sem.FCalls

  module Entry = struct
    type t =
        {
          decl: Sign.decl;
          filename: string;
          deps: FCalls.t;
          impl: Method.impl;
        }
    let print fmt { decl } = Sign.Decl.print_shortname fmt decl
    let compare { decl = d1 } { decl = d2 } = Sign.Decl.compare d1 d2
    let equal { decl = d1 } { decl = d2 } = Sign.Decl.equal d1 d2
    let hash { decl } = Sign.Decl.hash decl
    let sign { decl } = Sign.Decl.sign decl
    module K = Typ.UserTyp
  end
  type entry = Entry.t

  module G = Persistent.Digraph.Concrete(* Bidirectional *) (Entry)
  type t = G.t

  module Entries = Sign.MakeExtendedSet (FCall.Lookup) (Entry)
  let build entries =
    let open FCall in
    let open FCalls in
    let g = Entries.fold (fun e g -> G.add_vertex g e) entries G.empty in
    Entries.fold begin fun (Entry.{ deps } as e) ->
      fold begin fun callsign (g, unkn) ->
        let (g', hit), missing, _nlookups =
          acc_lookup (fun e' (g, _) -> G.add_edge g e e', true)
            callsign (Entries.callables entries) (g, false)
        in
        let unkn = unkn
          |> (if not hit && Typ.UserTyps.is_empty missing then
              insert callsign else ign)
          |> Typ.UserTyps.fold (fun t -> insert (specialize t callsign)) missing
        in
        g', unkn
      end deps
    end entries (g, empty)

  let from_filenames read_impl fl =
    let rec next (s, w) = function
      | [] -> Ok s, w
      | filename :: tl -> match read_impl filename with
          | Error e -> Error (`InFile (filename, e)), w
          | Ok impl -> compile (s, w) filename impl tl

    and acc_warnings filename w = function
      | _, [] -> w
      | Some m, w' -> `InMeth (m, w') :: w
      | None, w' -> `InFile (filename, w') :: w

    and compile (s, w) filename impl tl = match Compiler.of_impl impl with
      | Error (Some filename, e), w'
        -> Error (`InMeth (filename, e)), acc_warnings filename w w'
      | Error (None, e), w'
        -> Error (`InMeth (filename, e)), acc_warnings filename w w'
      | Ok meth, w'
        -> check (s, acc_warnings filename w w') filename meth impl tl

    and check (s, w) filename meth impl tl = match Compiler.sign meth with
      | None -> insert (s, w) filename meth impl tl
      | Some decl -> match ArgConsts.check decl (Compiler.body meth) with
          | Ok () -> insert (s, w) filename meth impl tl
          | Error es -> Error (`InMeth' (filename, es)), w

    and insert (s, w) filename meth impl tl = match Compiler.sign meth with
      | None -> next (s, w) tl
      | Some decl ->
          let deps = acc_efcalls meth FCalls.empty in
          next (Entries.insert Entry.{ decl; filename; deps; impl } s, w) tl

    and acc_efcalls =
      Compiler.fold_effcalls (fun _ ?res ec -> FCalls.insert (FCall.callsign ec))

    in
    match next (Entries.empty, []) fl with
      | Error e, w -> Error e, w
      | Ok entries, w -> Ok (build entries), w

  module D = Graphviz.Dot (struct
    open Entry
    include G
    let vertex_name { filename } = asprintf "\"%s\"" filename
    let graph_attributes _ = []
    let default_vertex_attributes _ = [ `Shape `Ellipse ]
    let default_edge_attributes _ = []
    let vertex_attributes pc = []
    let edge_attributes e = []
    let get_subgraph pc = None
  end)
  let output_dot = D.fprint_graph
end
