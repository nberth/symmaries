open Format
open Rutils
open Utils
open Timing
open BasicStats
open BasicStats.Outputs
open ScgsCore.Stats

(* --- *)

type class_stats = (int, float) OOLang.TypStruct.Stats.tree_stats
type class_degree_stats = OOLang.TypStruct.Stats.tree_degree_stats
      (* type class_rel_stats = OOLang.TypStruct.Stats.rel_degree_stats *)
let print_class_stats
    ?(degree_stats: class_degree_stats option)
    ?(peeks: (string * int) list = [])
    : class_stats pp =
  fun fmt cs ->
    pp fmt "@[<v>%a%a%a@]"
      (OOLang.TypStruct.Stats.print_tree_stats pi pf) cs
      (print_assoc_pairs ~fopen:",@;" ~fclose:"" pp_print_string pi) peeks
      (pp_opt ",@;%a" OOLang.TypStruct.Stats.print_tree_degree_stats)
      degree_stats

(* --- *)

module Aggreg = struct
  open BasicStats.Aggreg
  open ScgsCore.SCFG.Stats
  open SCFGGen

  module F = Pair (FloatMinMax) (FloatWelford)
  module T = Pair (F) (FloatSum)

  let print_time = print_assoc_map (print_aggreg pp_time pp_time)
  let print_int = print_assoc_map (print_aggreg pi pf)
  let print_bool = pi
  let print_skipped_causes = print_assoc_map_v (pp_strmap pi)

  type ('t, 'n) extra =
      {
        agg_scfg_gen_done: ('t, 'n) SCFGGen.t;
        agg_scfg_gen_skipped: ('t, 'n) SCFGGen.t;
        agg_skipped: int;
        agg_skipped_causes: int StrMap.t;
        agg_total_time: time;
        agg_effective_time: time;
      }

  let print fmt (u, { agg_skipped; agg_skipped_causes;
                      agg_scfg_gen_done; agg_scfg_gen_skipped;
                      agg_total_time; agg_effective_time }) =
    let pk0 = ps ?k0:None in
    pp fmt "%a,@;\
            %a: %i,@;\
            %a: %a,@;\
            %a,@;\
            %a,@;\
            %a: %a,@;\
            %a: %a"
      (* pk0 "summarized_scfgs" *)
      (* (print_assoc_map_v (ScgsCore.Stats.SCFGGen.print *)
      (*                       print_time print_int)) agg_scfg_gen_done *)
      (* pk0 "skipped_scfgs" *)
      (* (print_assoc_map_v (ScgsCore.Stats.SCFGGen.print *)
      (*                       print_time print_int)) agg_scfg_gen_skipped *)
      (ScgsCore.Stats.print print_time print_int print_bool
         (ppq pp_print_string)) u
      pk0 "nb_skipped_summarizations" agg_skipped
      pk0 "skipped_summarization_causes" print_skipped_causes agg_skipped_causes
      (ScgsCore.Stats.SCFGGen.print ~k0:"summarized_"
         print_time print_int) agg_scfg_gen_done
      (ScgsCore.Stats.SCFGGen.print ~k0:"skipped_"
         print_time print_int) agg_scfg_gen_skipped
      pk0 "total_accounted_time" pp_time agg_total_time
      pk0 "total_effective_time" pp_time agg_effective_time

  (* --- *)

  let symb_heap_init =
    ScgsCore.SymbHeap.Stats.{
      nb_typ_vars = F.init';
      nb_mut_refs = F.init';
      nb_const_refs = F.init';
      nb_mut_rel_vars = F.init';
      nb_const_rel_vars = F.init';
    }

  let scfg_gen_init =
    {
      time = T.init';
      cards = { nb_locations = F.init';
                nb_transitions = F.init';
                nb_vars = F.init'; };
      degrees = { max_in_degree = F.init';
                  max_out_degree = F.init'; };
      decls = { nb_state_vars = F.init';
                nb_input_vars = F.init';
                nb_contr_vars = F.init'; };
    }

  let init =
    {
      symb_heap_stats = symb_heap_init;
      meth_scfg_gen = scfg_gen_init;
      meth_handling = Skipped "";
    },
    {
      agg_scfg_gen_done = scfg_gen_init;
      agg_scfg_gen_skipped = scfg_gen_init;
      agg_skipped = 0;
      agg_skipped_causes = StrMap.empty;
      agg_total_time = 0.0;
      agg_effective_time = 0.0;
    }

  (* --- *)

  let decompf u = F.fst (T.fst u), F.snd (T.fst u), T.snd u
  let decompt u = F.fst u, F.snd u

  let aggreg_time u =
    let min_n_max, stats, sum = decompf u in
    Summed ({ min = FloatMinMax.min min_n_max;
              max = FloatMinMax.max min_n_max;
              mean = FloatWelford.mean stats;
              stddev = FloatWelford.stddev' stats; },
            { sum = FloatSum.sum sum; })

  let aggreg_int u =
    let min_n_max, stats = decompt u in
    Basic ({ min = int_of_float (FloatMinMax.min min_n_max);
             max = int_of_float (FloatMinMax.max min_n_max);
             mean = FloatWelford.mean stats;
             stddev = FloatWelford.stddev' stats })

  let init_int b = F.init (float_of_int b)
  let int a b = F.add a (float_of_int b)
  let bool a b = if b then succ a else a

  let acc_symb_heap = SHStats.acc int
  let acc_scfg_gen = SCFGGen.acc T.add int

  let acc_meth_handling u mh = match u, mh with
    | Done { model; synth }, Done { model = m; synth = s }
      -> Done
        { model = Constr.{ time = T.add model.time m.time;
                           suppsize = int model.suppsize m.suppsize;
                           footsize = int model.footsize m.footsize };
          synth = Synth.{ synth_time = T.add synth.synth_time s.synth_time;
                          triang_time = T.add synth.triang_time s.triang_time;
                          unsat_guard = bool synth.unsat_guard s.unsat_guard;
                          true_guard = bool synth.true_guard s.true_guard; } }
    | Skipped _, Done { model; synth }
      -> Done
        { model = Constr.{ time = T.init model.time;
                           suppsize = init_int model.suppsize;
                           footsize = init_int model.footsize };
          synth = Synth.{ synth_time = T.init synth.synth_time;
                          triang_time = T.init synth.triang_time;
                          unsat_guard = if synth.unsat_guard then 1 else 0;
                          true_guard = if synth.true_guard then 1 else 0; }; }
    | h, Skipped _ ->
        h

  let acc_summarization_stats (u, agg)
      { symb_heap_stats; meth_scfg_gen; meth_handling } =
    { symb_heap_stats = acc_symb_heap     u.symb_heap_stats symb_heap_stats;
      meth_scfg_gen   = acc_scfg_gen      u.meth_scfg_gen meth_scfg_gen;
      meth_handling   = acc_meth_handling u.meth_handling meth_handling; },
    (let t = meth_scfg_gen.time in
     let t, et, skipped, cause = match meth_handling with
       | Skipped "" -> t, 0.0, 1, StrMap.empty
       | Skipped c -> t, 0.0, 1, StrMap.singleton c 1
       | Done d -> let t = t +. (d.model.time +. d.synth.synth_time +.
                                  d.synth.triang_time) in
                  t, t, 0, StrMap.empty
     in
     let agg_scfg_gen_done, agg_scfg_gen_skipped = match meth_handling with
       | Skipped _
         -> agg.agg_scfg_gen_done,
           acc_scfg_gen agg.agg_scfg_gen_skipped meth_scfg_gen
       | Done _
         -> acc_scfg_gen agg.agg_scfg_gen_done meth_scfg_gen,
           agg.agg_scfg_gen_skipped
     in
     { agg_scfg_gen_done;
       agg_scfg_gen_skipped;
       agg_skipped = agg.agg_skipped + skipped;
       agg_skipped_causes = (StrMap.union (fun _ x y -> Some (x + y))
                               agg.agg_skipped_causes cause);
       agg_total_time = agg.agg_total_time +. t;
       agg_effective_time = agg.agg_effective_time +. et; })

  (* --- *)

  let aggreg_symb_heap_stats = SHStats.map aggreg_int
  let aggreg_meth_scfg_gen = SCFGGen.map aggreg_time aggreg_int

  let finalize ({ symb_heap_stats; meth_scfg_gen; meth_handling }, agg) =
    {
      symb_heap_stats = aggreg_symb_heap_stats symb_heap_stats;
      meth_scfg_gen = aggreg_meth_scfg_gen meth_scfg_gen;
      meth_handling = match meth_handling with
        | Skipped _ as s -> s
        | Done { model; synth } -> Done
            { model = Constr.{ time = aggreg_time model.time;
                               suppsize = aggreg_int model.suppsize;
                               footsize = aggreg_int model.footsize; };
              synth = Synth.{ synth_time = aggreg_time synth.synth_time;
                              triang_time = aggreg_time synth.triang_time;
                              unsat_guard = synth.unsat_guard;
                              true_guard = synth.true_guard; }; };
    },
    { agg with
      agg_scfg_gen_done = aggreg_meth_scfg_gen agg.agg_scfg_gen_done;
      agg_scfg_gen_skipped = aggreg_meth_scfg_gen agg.agg_scfg_gen_skipped; }

  (* --- *)

  let summarization_stats lst =
    List.fold_left acc_summarization_stats init lst |> finalize

  (* --- *)

  module ImplStats = struct
    open OOLang.Method.ImplStats
    let init =
      {
        decl_stats =
          OOLang.Semantics.DeclStats.{
            nb_prims = F.init';
            nb_refs = F.init';
          };
        nb_stms = F.init';
      }
    let acc = acc_cardinals int
    let finalize = map_cardinals aggreg_int
    let print = print_cardinals (* ~k0:"meth_impls_" *) print_int
  end

end

(* --- *)

type impl_stats = int OOLang.Method.ImplStats.cardinals
type summarization_stat = (time, int, bool, string) ScgsCore.Stats.t
type meth_stats =
    {
      impl_stats: impl_stats;
      sum_stats: summarization_stat list
    }
type map = meth_stats StrMap.t
type t = map

let append f impl_stats s : t -> t = fun m ->
  let { sum_stats } as ss = try StrMap.find f m with
    | Not_found -> { impl_stats; sum_stats = [] }
  in
  StrMap.add f { ss with sum_stats = sum_stats @ [s] } m

(* --- *)

let print_impl_stats: impl_stats pp = fun fmt ->
  pp fmt "@[<hov>%a@]" (OOLang.Method.ImplStats.print_cardinals pi)

let print_summarization_stat: summarization_stat pp = fun fmt ->
  pp fmt "@[<hov>%a@]" (print pp_time pi pp_print_bool
                          (ppq pp_print_string))

let print_meth_full_stats: meth_stats pp = fun fmt -> function
  | { sum_stats = [] }
    -> assert false                                             (* unreachable *)
  | { impl_stats; sum_stats = [t] }
    ->(pp fmt "@[<v>processed: 1,@;\
                @[<2>impl: {@;%a@;@]},@;\
                @[<2>infos: {@;%a@]@;}\
              @]" print_impl_stats impl_stats print_summarization_stat t)
  | { impl_stats; sum_stats = lst }
    ->(pp fmt "@[<v>processed: %i,@;\
                @[<2>impl: {@;%a@;@]},@;\
                @[<2>infos: [@;%a@]@;],@;\
                @[<2>stats: {@;@[<hv>%a@]@]@;}\
              @]" (List.length lst)
        print_impl_stats impl_stats
        (pp_lst ~fopen:"@[<2>{ " ~fsep:"@;@]},@;@[<2>{ " ~fclose:"@;@]}"
           ~fempty:"[]" print_summarization_stat) lst
        Aggreg.print (Aggreg.summarization_stats lst))

type glb =
    {
      unsat: Strings.t;
      true_: Strings.t;
    }

let glb_init =
  {
    unsat = Strings.empty;
    true_ = Strings.empty;
  }

let print_overall_stats fmt m =
  let mu = Aggreg.ImplStats.init and u = Aggreg.init, glb_init in
  let mu, (agg, { unsat; true_ }) = StrMap.fold
    begin fun m { impl_stats; sum_stats } (mu, u) ->
      Aggreg.ImplStats.acc mu impl_stats,
      List.fold_left begin fun (agg, glb) ({ meth_scfg_gen; meth_handling } as s) ->
        let agg = Aggreg.acc_summarization_stats agg s in
        let glb = match meth_handling with
          | Done { synth = { unsat_guard = true } }
            -> { glb with unsat = Strings.add m glb.unsat; }
          | Done { synth = { true_guard = true } }
            -> { glb with true_ = Strings.add m glb.true_; }
          | _ -> glb
        in
        agg, glb
      end u sum_stats
    end m (mu, u) in
  let nb_meths = StrMap.cardinal m in
  let nb_unsafe_meths = Strings.cardinal unsat in
  let nb_totsafe_meths = Strings.cardinal true_ in
  (* let effective_time = snd u in *)
  pp fmt "@[<hv>%a,@;%a,@;\
            nb_meths: %i,@;nb_unsafe_meths: %i,@;nb_totsafe_meths: %i\
          @]"
    Aggreg.ImplStats.print (Aggreg.ImplStats.finalize mu)
    Aggreg.print (Aggreg.finalize agg)
    nb_meths nb_unsafe_meths nb_totsafe_meths

let print_full fmt m =
  pp fmt "@[<2>{@;@[@[<2>methods: {@;@[<v>%a@]@]@;},@;\
                    @[<2>stats: {@;%a@]@;}@]@]@;}@."
    (pp_strmap' print_meth_full_stats) m
    print_overall_stats m

(* --- *)

module IO = ScgsUtils.IO.Bin (struct
  type t = map
  let functional = false
end)

(* --- *)
