open Format
open Rutils
open Utils
open OOLang
open Graph

module type S = sig
  module CG: CallGraph.S
  open CG
  module WeakTopo: sig
    type t
    val of_callgraph: CG.t -> t
    val print: t pp
    type component
    type 'a visit =
      | V of 'a
      | Ch of 'a * component                              (* Component's entry *)
      | Ct of 'a * component                              (* Component's exit *)
    val fold: (CG.Entry.t visit -> 'a -> 'a * bool) -> t -> 'a -> 'a
    type v = private int
    val look: t -> v visit -> CG.Entry.t visit
    val top_comp_visits: t -> component -> v visit list
    val main_comp_visits: t -> v visit list
    val output_dot: formatter IO.out -> t -> unit
  end
  val output_cg_dot: formatter IO.out -> CG.t -> unit
end

module Make (CG: CallGraph.S) : S with module CG = CG = struct
  module CG = CG
  open CG

  module G = Persistent.Digraph.ConcreteBidirectional (Integer)

  let output_cg_dot out cg =
    let fmt, close = out.Rutils.IO.out_exec ~descr:"Callgraph@ as@ DOT"
      ~backup:Rutils.IO.Forget ~suff:"-cg" "dot" in
    CG.output_dot fmt cg;
    close ()

  (* --- *)

  module WeakTopo = struct
    open Graph
    open WeakTopological

    module WTopo = Make (G)
    type t = Entry.t array * G.t * G.V.t WeakTopological.t
    type component = G.V.t WeakTopological.t

    let of_callgraph cg : t =
      let module EMap = MakeMap(Entry) in
      let e2i, el, cg', _ = CG.G.fold_vertex
        begin fun e ((e2i, el, g, i) as acc) ->
          if EMap.mem e e2i then acc else
            let i = succ i in
            EMap.add e i e2i, e :: el, G.add_edge g (-1) i, i
        end cg (EMap.empty, [], G.add_vertex G.empty (-1), -1) in
      let i2e = Array.of_list (List.rev el) in
      let cg' = CG.G.fold_edges (fun e e' g ->
        G.add_edge g (EMap.find e' e2i) (EMap.find e e2i)) cg cg' in
      i2e, cg', WTopo.recursive_scc cg' (-1)

    type 'a visit =
      | V of 'a
      | Ch of 'a * component                              (* Component's entry *)
      | Ct of 'a * component                               (* Component's exit *)

    type v = int

    let look (i2e, _, _) = function
      | V v -> V i2e.(v)
      | Ch (v, c) -> Ch (i2e.(v), c)
      | Ct (v, c) -> Ct (i2e.(v), c)

    let top_comp_visits ((i2e, g', wtopo): t) c =
      let rec aux c acc = fold_left begin fun acc -> function
        | Vertex -1 -> acc
        | Vertex v -> V v :: acc
        | Component (-1, c) -> aux c acc
        | Component (v, c) -> Ct (v, c) :: Ch (v, c) :: acc
      end acc c in
      aux c [] |> List.rev

    let main_comp_visits ((_, _, wtopo) as t) = top_comp_visits t wtopo

    let fold f ((i2e, g', wtopo): t) =
      let rec aux c acc = fold_left begin fun acc -> function
        | Vertex -1 -> acc
        | Vertex v -> f (V (i2e.(v))) acc |> fst
        | Component (-1, c) -> aux c acc
        | Component (v, c) ->
            let acc, _ = f (Ch (i2e.(v), c)) acc in
            let rec comp v c acc =
              let acc = aux c acc in
              let acc, conv = f (Ct (i2e.(v), c)) acc in
              if conv then acc else comp v c acc
            in
            comp v c acc
      end acc c in
      aux wtopo

    let rec print fmt ((i2e, _, _) as wtopo) =
      fold (fun v () -> printv fmt v, true) wtopo ()
    and printv fmt = function
      | V v -> pp fmt "%a,@ " Entry.print v
      | Ch (v, c) -> pp fmt "(@[{%a}:@ " Entry.print v
      | Ct (v, _) -> pp fmt ")@],@ "

    (* --- *)

    let output_dot out (i2e, cg', _) =
      let module D = Graphviz.Dot (struct
        open Entry
        include G
        let vertex_name = function
          | i when i >= 0 -> asprintf "\"%s\"" i2e.(i).filename
          | _ -> "\"-\""
        let graph_attributes _ = []
        let default_vertex_attributes _ = [ `Shape `Ellipse ]
        let default_edge_attributes _ = []
        let vertex_attributes pc = []
        let edge_attributes e = []
        let get_subgraph pc = None
      end) in
      let fmt, close = out.Rutils.IO.out_exec
        ~descr:"Revered@ Callgraph@ as@ DOT"
        ~backup:Rutils.IO.Forget ~suff:"-rcg" "dot"
      in
      D.fprint_graph fmt cg';
      close ()
  end

end

module type BASE = sig
  module Typs: TypStruct.T
  module CG: CallGraph.S
  module WeakTopo: sig
    type t
    type component
    type 'a visit =
      | V of 'a
      | Ch of 'a * component                              (* Component's entry *)
      | Ct of 'a * component                              (* Component's exit *)
    val print: t pp
  end
end

module type T = sig
  include S
  include BASE with module CG := CG and module WeakTopo := WeakTopo
  val wtopo: WeakTopo.t
end

module type W = sig
  include BASE
  type steps
  val nosteps: steps
  val visits: steps
  val try_visit: steps -> (CG.Entry.t WeakTopo.visit * (bool -> steps)) option
  module IO: ScgsUtils.IO.BINARY with type t = steps
end

(* --- *)

module type J = sig
  module Typs: Java.TYPING
  include T with module Typs := Typs
end

module type JW = sig
  module Typs: Java.TYPING
  include W with module Typs := Typs
end

(* --- *)

module IO = struct
  module J = ScgsUtils.IO.Bin (struct
    type t = (module J)
    let functional = true
  end)
  module JW = ScgsUtils.IO.Bin (struct
    type t = (module JW)
    let functional = true
  end)
  type poly =
    | Full of (module J)
    | Stage of (module JW)
  include ScgsUtils.IO.Bin (struct
    type t = poly
    let functional = true
  end)
end

(* --- *)

(* XXX? J ~> T ? *)
module LockSteps (CGWalk: J)
  : (W with module Typs = CGWalk.Typs
       and  module CG = CGWalk.CG)
  =
struct
  open CGWalk
  open WeakTopo
  module Typs = Typs
  module CG = CG
  module WeakTopo = WeakTopo
  type steps = WeakTopo.v WeakTopo.visit list
  let nosteps: steps = []
  let visits: steps = main_comp_visits wtopo
  let try_visit: steps -> (CG.Entry.t visit * (bool -> steps)) option =
    let open_comp: _ visit -> steps -> steps = function
      | V _ | Ct _ -> ign
      | Ch (v, c) -> (@) (top_comp_visits wtopo c)
    in
    let next v vl conv = match v with
      | Ct _ when conv -> open_comp v vl
      | Ct (_, c) when not conv -> top_comp_visits wtopo c @ vl
      | v -> open_comp v vl
    in
    function
      | [] -> None
      | v :: vl -> Some (look wtopo v, next v vl)
  module IO = ScgsUtils.IO.Bin (struct
    type t = steps
    let functional = false
  end)
end
