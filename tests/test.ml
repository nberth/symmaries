open Format
open Rutils
open Utils
open OOLang
open Base
open Base.Expr
open Method.Stm
open Method.Stms
open Typ

(* --- *)

(* module Memory = Java.FlatHeap *)
(* module Memory = Java.ClassesOfPrim *)
module Memory = Java.Heap

module Fields = Memory.TypDecls.Fields

(* --- *)

module type TEST = sig
  val db: Memory.TypDecls.t
end

(* --- *)

module type DIRECT_TEST = sig
  include TEST
  val ustms: raw
end

module type RAW_TEST = sig
  include TEST
  val meth_body: Method.Impl.t
end

(* --- *)

module Default = struct
  let db = Memory.TypDecls.empty
  let decls = Java.Decls.empty
end

(* --- *)

module T0 = struct
  include Default
  open Memory
  open TypName
  let c0 = UserTyp "C0"
  and c1 = UserTyp "C1"
  (* open TypDecls *)
  (* FlatRecords *)
  (* let c0_decl = Fields.list [ Fields.one "f" (primtyp "int") ] *)
  (* let c1_decl = Fields.list [ Fields.one' "f" c0 ] *)
  (* ClassesOfPrim *)
  (* let c0_decl = (UserTyps.empty, *)
  (*                Fields.list [ Fields.one "f" (PrimTyp "int") ]) *)
  (* let c1_decl = (UserTyps.singleton c0, *)
  (*                Fields.list [ Fields.one "f" (PrimTyp "int"); ]) *)
  (* Heap *)
  let c0_decl = (UserTyps.empty,
                 Fields.list [ Fields.one "f" (prim "int") ])
  let c1_decl = (UserTyps.singleton c0,
                 Fields.list [ Fields.one "f" (any c0) ])
  let db = begin db
      |> TypDecls.decl c0 c0_decl
      |> TypDecls.decl c1 c1_decl
  end
  let decls = Java.Decls.(begin decls
      |> var "r" (any c0)
      |> var "s" (any c0)
      |> var "t" (any c0)
      |> var "u" (any c1)
  end)
end

(* --- *)

module T0': DIRECT_TEST = struct
  include T0
  let ustms = Seq.of_array
    [|
      (            ChkPoint ("c"));
      (            Decl     (user "C0", ["r"; "s"; "t"]));
      (            Decl     (user "C1", ["u"]));
      (            Decl     (prim "int", ["h"]));
      (            Decl     (prim "int", ["a"; "b"]));
      (            Assign   ("a", Var "b"));
      (            Load     ("s", "u", "f"));
      (            CondGoto (Cond.var "h", "l1"));                      (* ρ1 *)
      (            Store    ("s", "f", Const "cst"));
      (            Assign   ("t", Var "r"));
      (            Assign   ("r", Var "s"));
      (            Assign   ("s", Var "t"));
      (            Store    ("s", "f", Var "a"));
      (            Assign   ("t", Var "r"));
      (            Assign   ("r", Var "s"));
      (            Assign   ("s", Var "t"));
      Label ("l1", Load     ("a", "s", "f"));                     (* junc(ρ1) *)
      (            Null     ("r"));
      (            Output   ("L", ["a"]));
      (            Return   (Var "a"));
    |]
end

(* --- *)

module T0'': DIRECT_TEST = struct
  include T0                                               (* for `can_alias' *)
  let ustms = Seq.of_array
    [|
      (            ChkPoint ("c"));
      (            Decl     (user "C0", ["r"; "s"; "t"]));
      (            Decl     (user "C1", ["u"]));
      (            Decl     (prim "int", ["h"]));
      (            Decl     (prim "int", ["a"; "b"]));
      (            Assign   ("a", Var "b"));
      (            Assign   ("s", Var "u"));
      (            CondGoto (Cond.var "h", "l1"));                      (* ρ1 *)
      (            Store    ("s", "f", Const "cst"));
      (            Assign   ("t", Var "r"));
      (            Assign   ("r", Var "s"));
      (            Assign   ("s", Var "t"));
      (            Store    ("s", "f", Var "a"));
      (            Assign   ("t", Var "r"));
      (            Assign   ("r", Var "s"));
      (            Assign   ("s", Var "t"));
      Label ("l1", Load     ("a", "s", "f"));                     (* junc(ρ1) *)
      (            Null     ("r"));
      (            Output   ("L", ["a"]));
      (            Return   (Var "a"));
    |]
end

(* --- *)

module T5': DIRECT_TEST = struct
  include Default
  open Memory
  open TypName
  (* FlatRecords *)
  (* let obj_decl = Fields.one "f" (primtyp "int") *)
  (* ClassesOfPrim *)
  (* let obj_decl = (UserTyps.empty, *)
  (*                 Fields.one "f" (PrimTyp "int")) *)
  (* Heap *)
  let obj_decl = (UserTyps.empty,
                  Fields.one "f" (prim "int"))
  let db = db
    |> TypDecls.decl (UserTyp "Object") obj_decl
  let ustms = Seq.of_array
    [|
      (            Decl     (user "Object", ["r"]));
      (            Decl     (prim "int", ["i"; "j"]));
      (            ChkPoint ("c"));
      (            Goto     "l1");
      (            Load     ("i", "r", "f"));
      Label ("l2", CondGoto (Cond.var "j", "l3"));
      (            Assign   ("i", Binop (Var "i", "-", Var "j")));
      (            Goto     "l1");
      Label ("l3", Assign   ("i", Binop (Var "i", "+", Var "j")));
      Label ("l1", CondGoto (Cond.var "i", "l2"));                (* junc(ρ2) *)
      (            CondGoto (Cond.var "j", "l4"));                (* junc(ρ1) *)
      (            Output   ("L", ["i"]));
      (            Return   (Var "i"));
      Label ("l4", Return   (Var "j"));
    |]
end

(* --- *)

let level = Log.Debug3
let logger = Log.mk ~level "Test"
let () =
  let columns = try int_of_string (Sys.getenv "COLUMNS") - 1 with _ -> 80 in
  Format.pp_set_margin Format.err_formatter columns;
  Format.pp_set_margin Format.std_formatter columns;
  Log.globallevel := Log.Debug;
  Realib.BddapronUtil.prime_suffix := "\""
;;

open ReaTKBridge

module AsCG = AsCG.Make (struct
  let use_external_symbs = false
end)

let sspec = "sB";;

let error errs =
  List.iter (eprintf "*** Error: @[%a@]@." Method.pp_error) errs;
  exit 1;;

let abort_on_error = function
  | Ok r -> r
  | Error errs -> error errs

module Raw (T: RAW_TEST) = struct
  include T;;
  let secsigs = ReaTKBridge.Trans.SecSig.Set.empty;;
  Log.i logger "Type decarations:@\n%a" Memory.TypDecls.print db;;
  module AliasOps = Memory.Close (struct let db = db end)
  module Impl = Method.Compiler (Java.Semantics (AliasOps))
  let meth = match Impl.of_impl meth_body with
    | Ok m -> m
    | Error (_, errs) -> error errs;;
  Log.i logger "Typed and linked method:@\n%a" Impl.print meth;;
end

module Direct (T: DIRECT_TEST) = Raw (struct
  include T
  let meth_body = Method.Impl.Raw ustms;;
  Log.i logger "Raw method:@\n%a" Method.Impl.print meth_body;;
end)

module Make (Input: Trans.INPUT) = struct

  module SCFG = Trans.App (Input)
  module AsCG = AsCG (SCFG) (Trans.V)

  let extract env cs f =
    Log.d3 logger "Skipping controller extraction"
    (* let cf = Rl2cn.extract_ctrlf env cs f in *)
    (* match CtrlNbac.AST.check_func cf with *)
    (*   | None, msgs -> *)
    (*       AsCG.Reporting.report_msgs err_formatter msgs; *)
    (*       CtrlNbac.AST.print_func err_formatter cf *)
    (*   | Some cf, _ -> *)
    (*       CtrlNbac.AST.print_func err_formatter cf *)

  let synth env cs cf =
    let open Realib.Supra in
    log_env_info ~cs env;
    log_cfprog_info env cf;
    let a = cf.Realib.Program.c_ass in
    let cs, k, cf = synth (parse_synth env sspec) env (cs, a, cf) in
    let f = triang cs env k in
    extract env cs f

  let model cg =
    match Cn2rl.translate_cfg cg with
      | `TKo msgs ->
          AsCG.Reporting.report_msgs err_formatter msgs
      | `TOk (env, cs, cf, _) ->
          Log.i logger "Successful creation of SCFG model.";
          synth env cs cf

  let build scfg =
    let cg, msgs = AsCG.cfg scfg in
    AsCG.Reporting.report_msgs err_formatter msgs;
    match cg with
      | `KO cg ->
          Log.e logger "Errors in SCFG check.";
          CtrlNbac.AST.print_cfg err_formatter cg
      | `OK cg ->
          Log.i logger "Successful SCFG check.";
          CtrlNbac.AST.print_cfg err_formatter cg;
          model cg

  open SCFG

  let () =
    CtrlNbac.Symb.reset ();
    let fmt = open_out "/tmp/x.dot" |> formatter_of_out_channel in
    AsDot.output fmt scfg;
    pp_print_flush fmt ();
    build scfg

end;;

try

  let module M0' = Make (Direct (T0')) in
  let module M5' = Make (Direct (T5')) in
  let module M0'' = Make (Direct (T0'')) in

  let module MX = Make (Raw (struct
    include Default
    let meth_body = Input.RawMeth.from_string
      "      checkpoint c;
             int h, v;
             v = 1;
             if (h == 0) goto skip;
             v = 0;
       skip: output_L v;"
  end)) in

  let module MAllPrims = Make (Raw (struct
    include Default
    let meth_body = Input.RawMeth.from_string
      "      checkpoint c;
             int v, w;
             goto l1;
         l1: v = w;
             output_B (v, w);
             return;"
  end)) in

  let module MNoPrimsDecls = Make (Raw (struct
    include Default
    open Memory
    open TypName
    let db = db
      (* |> TypDecls.decl (UserTyp "Object") (Classes.SuperTyps.empty, *)
      (*                                      Fields.one "f" (PrimTyp "int")) *)
      |> TypDecls.decl (UserTyp "Object") (TypStruct.SuperTyps.empty,
                                           Fields.one "f" (prim "int"))
    let meth_body = Input.RawMeth.from_string
      "      Object v;
             int w, cst;
             cst = 0xfeefe2;
             checkpoint c;
             goto l1;
         l1: v.f = w;
             output_B (v, w);
             return;"
  end)) in

  let module TypOps = Java.FlatHeap.Close (struct
    let typ_specs = Input.Java.Flat.from_string
      "     C0 { int f, g, h }
            C1 { C0 f; }
            java.lang.Object
            java.lang.System
      "
    let db = Java.FlatHeap.TypDecls.of_specs typ_specs;;
    Log.i logger "Typdecls: %a" Java.FlatHeap.TypDecls.print db;;
  end) in

  let module TypOps = Java.ClassesOfPrim.Close (struct
    let typ_specs = Input.Java.ClassesOfPrim.from_string
      "     C0 { int f, g, h }
            C1 <: C0 { int f; };
            java.lang.Object
            java.lang.System <: java.lang.Object
      "
    let db = Java.ClassesOfPrim.TypDecls.of_specs typ_specs;;
    Log.i logger "Typdecls: %a" Java.ClassesOfPrim.TypDecls.print db;;
  end) in

  let module TypOps = Java.Heap.Close (struct
    let typ_specs = Input.Java.Classes.from_string
      "     C0 { int f, g, h }
            C1 <: C0, java.lang.Object { C0 f; };
            java.lang.Object
            java.lang.System <: java.lang.Object
      "
    let db = Java.Heap.TypDecls.of_specs typ_specs;;
    Log.i logger "Typdecls: %a" Java.Heap.TypDecls.print db;;
  end) in

  ()
with
  | Sedlex_menhir.ParseError e ->
      eprintf "@[%a@]@." Sedlex_menhir.pp_parse_error e
  | Sedlex_menhir.SyntaxError e ->
      eprintf "@[%a@]@." Sedlex_menhir.pp_syntax_error e
;;

pp_print_flush err_formatter ();;
