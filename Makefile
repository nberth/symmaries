# -*- makefile-gmake -*-
# ----------------------------------------------------------------------
#
# Example Makefile for OPAM package contruction.
#
# Copyright (C) 2015-2018 Nicolas Berthier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------
#
# Package maintainers can use the `opam-dist' utility [1] for OPAM
# package creation and distribution.
#
# [1] https://github.com/nberth/opam-dist
#
# ----------------------------------------------------------------------

# Customizable part.

# OPAM package name.
PKGNAME = symmaries

# Lists below can be left empty when needed.

# OCaml executable to be compiled.
EXECS = syrs
# OCaml libraries to be compiled, accompanied with their interfaces.
AVAILABLE_LIBs = scgsUtils oOLang scgsCore reaTKBridge scgsToolChain
AVAILABLE_LIB_ITFs = $(AVAILABLE_LIBs)

USE_PER_LIB_INSTALL_DOC = no
DOC_TARGETS = $(SRC)/symmaries.docdir/index.html

# Files to be installed in the binary directory, that do not need to
# be compiled yet.
EXTRA_EXECS =
# Ibid in the case of libraries.
EXTRA_LIBs =

# Further flags.
INSTALL_LIBS = yes
INSTALL_DOCS = no
ENABLE_BYTE = yes
ENABLE_NATIVE = yes
ENABLE_DEBUG = no
ENABLE_PROFILING = no

# Executables built and run by `check' target:
TEST_EXECS = test

# This file should define PREFIX and DOCDIR variables, using a
# configure script for instance; they could also be setup directly.
-include config.mk

# ---

QUIET =
JOBS ?= 8

MENHIRFLAGS = --fixed-exception
OCAMLBUILDFLAGS += $(if $(JOBS),-j $(JOBS),)
OCAMLBUILDFLAGS += -use-menhir -menhir "menhir $(MENHIRFLAGS)"
OCAMLDOCFLAGS = -charset iso-10646-1
NO_PREFIX_ERROR_MSG = Missing prefix: execute configure script first

# ---

# The git submodule directory containing `generic.mk':
OPAM_PKGDEV_DIR ?= opam-pkgdev

# OPAM package descriptors, and files to include in the source
# distribution archive:
OPAM_DIR = opam
OPAM_FILES = descr opam
DIST_FILES = configure LICENSE Makefile myocamlbuild.ml src _tags	\
    version.ml.in etc META.in

# ----------------------------------------------------------------------

# Leave this part as is:

-include generic.mk

GENERIC_MK = $(OPAM_PKGDEV_DIR)/generic.mk
generic.mk:
	@if test -f $(GENERIC_MK); then ln -s $(GENERIC_MK) $@;		\
	 elif test \! -f generic.mk; then echo				\
"To build from this development tree, you first need to retrieve the"	\
"$(OPAM_PKGDEV_DIR) submodule using \`git submodule update'."		\
	 >/dev/stderr; exit 1; fi;

# -----------------------------------------------------------------------

# Insert further rules here:

ifeq ($(INSTALL_DOCS),yes)
  install-doc: force
	test -d "_build/$(SRC)/symmaries.docdir" &&			\
	  cp -r "_build/$(SRC)/symmaries.docdir" "$(DOCDIR)/$(PKGNAME)/";
endif

# To be used in combination with a configure script, for instance.
.PHONY: distclean
distclean: force clean clean-version
	$(QUIET)rm -f config.mk
